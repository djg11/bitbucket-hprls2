//(*
// * $Id: hpr.cpp,v 1.1 2008/07/08 08:55:54 djg11 Exp $
// * hpr.cpp   Orangepath: C and C++ output
// * (C) 2006-8 DJ Greaves, University of Cambridge Computer Laboratory.
// *
// *)
//(*
// * Shim code for using HPR L/S library (and Kiwi) with SystemC.
// *)


#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>


#include "hprls.h"


void hpr_KppMark(int wp)
{
  g_manual_waypoint = wp;
  if (g_vd) printf("Waypoint %i\n", wp); 
}


void hpr_KppMark(int wp, const char *s1)
{
  g_manual_waypoint = wp;
  if (g_vd)   printf("Waypoint %i\n", wp); 
}

void hpr_KppMark(int wp, const char *s1, const char *s2)
{
  g_manual_waypoint = wp;
  if (g_vd)   printf("Waypoint %i\n", wp); 
}

void hpr_sysexit(int code)
{
  if (g_vd) printf("hpr_sysexit invoked. rc=%i\n", code);
  g_manual_waypoint = 0;
  g_abend_syndrome = code;
  g_exiting = true;
}


#define INT64 long long int


void hpr_pause()
{ 
  hpr_barrier();
}


void hpr_threadspawn()
{ 
  assert(0); // Not implemented.
}


void hpr_barrier()
{ 
#ifdef HPR_USE_SYSTEMC
  wait(sc_time(0, SC_NS)); 
#endif
}


void hpr_writeln()
{ 
  printf("\n");
}


void hpr_writeln(const char *fmt, ...)
{
  char buffer[4096];
  va_list ap;
  va_start(ap, fmt);
  vsnprintf(buffer, sizeof(buffer), fmt, ap);
  printf("%s\n", buffer);
  va_end(ap);
}


void hpr_write(const char *fmt, ...)
{
  char buffer[4096];
  va_list ap;
  va_start(ap, fmt);
  vsnprintf(buffer, sizeof(buffer), fmt, ap);
  printf("%s", buffer);
  va_end(ap);
}


bool hpr_testandset(unsigned char &mutex, int newval)
{ 
  //This is non-premptive, so no special constructs are needed in the implementation.
  bool old = mutex;
  mutex = newval;
  return old;
}


char* hpr_concat(char *a, char *b)
{  
  char *line = new char [strlen(a)+strlen(b)+1];
  strcpy(line, a);
  strcat(line, b);
  return line;
}









// eof

