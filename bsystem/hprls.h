#ifndef HPRLS_H
#define HPRLS_H


//(*
// * $Id: hprls.h,v 1.1 2008/07/08 08:55:54 djg11 Exp $
// * hprls.h   Orangepath: C and C++ output
// * (C) 2006-8 DJ Greaves, University of Cambridge Computer Laboratory.
// *
// *)

#ifdef HPR_USE_SYSTEMC
#include "systemc.h"
#endif



extern bool g_vd;
extern bool g_exiting;
extern unsigned char g_abend_syndrome;
extern unsigned char g_manual_waypoint;


extern void hpr_pause();
extern void hpr_threadspawn();

#define INT64 long long int


extern void hpr_barrier();
extern void hpr_writeln();
extern void hpr_writeln(const char *, ...);
extern void hpr_write(const char *, ...);
extern bool hpr_testandset(unsigned char &mutex, int newval);
extern char* hpr_concat(char *a, char *b);


extern void hpr_KppMark(int);
extern void hpr_KppMark(int, const char *);
extern void hpr_KppMark(int, const char *, const char *);
extern void hpr_sysexit(int);







#endif
