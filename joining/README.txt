
Greaves/Nam Transactor and Glue Logic Generator.  Automatic synthesis of bus protocol adaptors.
(C) 2010-16 D J Greaves, University of Cambridge, Computer Laboratory.


As of March 2016 I started copying this code into the git repo and fixing the software rot.  This is because I am writing some protocol bridges by hand
at the moment and the whole point of this was work to avoid such manual coding in future!

Based on 'Synthesis of glue logic, transactors, multiplexors and serialisors from protocol specifications'. By D. J. Greaves, M. J. Nam. At Forum on Specification & Design Languages (FDL 2010). 14th-16th Sept. 2010 Southampton.


Compilation:
  ... missing instructions



