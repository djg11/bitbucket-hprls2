(*
 * $Id: isa.fs,v 1.8 2010/09/13 07:46:49 djg11 Exp $  
 * (C) 2010 MJ Nam and DJ Greaves.
 *
 * Here is the ISA bus
 *
 *)
module isa

open hprls_hdr
open meox
open joindefs

let x_net = gec_X_net

// The net directions are as seen on the joiner for the initiator side (data source for write operations) but the protocol specification is unsexed.
// See protocols.fs for new preferred canned/internal datastructure.

let isa_nets = 
  [
    (simplenet "isa_iown",       Ndi OUTPUT, false, gen_Concrete_enum [xi_num 1; xi_num 0]);  // This coding has these four strobes active high.
    (simplenet "isa_iorn",       Ndi OUTPUT, false, gen_Concrete_enum [xi_num 1; xi_num 0]) 
    (simplenet "isa_memwn",      Ndi OUTPUT, false, gen_Concrete_enum [xi_num 1; xi_num 0]) 
    (simplenet "isa_memrn",      Ndi OUTPUT, false, gen_Concrete_enum [xi_num 1; xi_num 0]) 
    (vectornet_w("isa_addr", 20),     Ndi OUTPUT, true, gen_Symbolic_bitvec "" true 16)
    (vectornet_w("isa_rdata", 16),    Ndi INPUT,  true, gen_Symbolic_bitvec "" true 16)
    (vectornet_w("isa_wdata", 16),    Ndi OUTPUT, true, gen_Symbolic_bitvec "" true 16)    
  ]


let isa_legacy_idle =  
 [
  (g_deadval, x_net "isa_addr");
  (g_deadval, x_net "isa_rdata");
  (g_deadval, x_net "isa_wdata");  

  (xi_num 0, x_net "isa_iown");  
  (xi_num 0, x_net "isa_iorn");  
  (xi_num 0, x_net "isa_memwn");  
  (xi_num 0, x_net "isa_memrn");  
 ]


let isa_legacy_initial = isa_legacy_idle



let ad_hi = vectornet_w("AD_hi", 12)
let ad_lo = vectornet_w("AD_lo", 20)

// This is a memory map decoder.
let   ioaddr = ix_bitor ad_lo (lshift(predicate(kill ad_hi, xi_deqd(ad_hi, xi_num 17)), 20))
let  memaddr = ix_bitor ad_lo ( lshift(predicate(kill ad_hi, xi_deqd(ad_hi, xi_num 16)), 20))


let data_hi = vectornet_w("DA_hi", 16)
let data_lo = vectornet_w("DA_lo", 16)

let data32 = ix_bitor data_lo (lshift(kill data_hi, 16)) // Form a 32-bit data word by mapping the isa data bus into the bottom 16 bits.


// Define each of the four cycle types.
let isa_mem_read =  
 PS_seq[  Set(memaddr, x_net "isa_addr");     
       Set(xi_num 1, x_net "isa_memrn");
       Set(data32, x_net "isa_rdata");
       Set(xi_num 0, x_net "isa_memrn");
       Setl [ (g_deadval, x_net "isa_addr"); (g_deadval, x_net "isa_rdata"); ];
  ]

let isa_io_read =  
 PS_seq[  Set(ioaddr, x_net "isa_addr");     
       Set(xi_num 1, x_net "isa_iorn");
       Set(data32, x_net "isa_rdata");
       Set(xi_num 0, x_net "isa_iorn");
       Setl [ (g_deadval, x_net "isa_addr"); (g_deadval, x_net "isa_rdata"); ];
  ]

let isa_mem_write =  
 PS_seq[  Setl [ (memaddr, x_net "isa_addr");  (data32, x_net "isa_wdata"); ];
       Set(xi_num 1, x_net "isa_memwn");
       Set(xi_num 0, x_net "isa_memwn");
       Setl [ (g_deadval, x_net "isa_addr"); (g_deadval, x_net "isa_wdata"); ];
  ]

let isa_io_write =  
 PS_seq[  Setl [ (ioaddr, x_net "isa_addr");  (data32, x_net "isa_wdata"); ];
       Set(xi_num 1, x_net "isa_iown");
       Set(xi_num 0, x_net "isa_iown");
       Setl [ (g_deadval, x_net "isa_addr"); (g_deadval, x_net "isa_wdata"); ];
  ]


// Make an alternation of four basic cycles:
let isa_legacy_protocol_ =
    PS_alt[
      isa_mem_read; isa_mem_write;
      isa_io_read; isa_io_write;
 ]  

let isa_legacy_protocol =      isa_mem_read; // for now only one



(* Overall composition of the protocol in a direction-agnostic form *)
let g_canned_isa1620 = ("ISA1620", isa_nets,  isa_legacy_initial, isa_legacy_idle, Synch isa_legacy_protocol)


// END OF PROTOCOL AND INTERFACE DEF

// The following equations are canned in this file at the moment, but we would
// expect to parse them from the bvcisa.xml file in future.
// Some glue equations to connect 32 bit isa to 20/16 isa

let bnet s = x_net s // for now


let g_bvisa_glue =
    let isanet s = x_net ("isa1620a" + "." + s)
    let bvnet s =  x_net ("bv32" + "." + s)  // ad32 rd32 wd32 are the 'parameters' of the bvci bus - and we are tacitly assuming they are concurrently live and that there's only one data-conserving command sequence, so we don't need to overtly set the commands.
    [

        (isanet "ISADATA",
            ix_bitor (kill(ix_rshift Unsigned (bvnet "ad32") (xi_num 12))) (ix_bitand (bvnet "ad32") (xi_num 0xFFFF)));

        // Connect our address bus to bvci ad32
        (isanet "ISAADDR",
         ix_bitor(kill(ix_rshift Unsigned (bvnet "ad32") (xi_num 12))) (ix_bitand(bvnet "ad32") (xi_num 0xFffff)));

    ] // A list of pairs


// EOF

