(*
 * $Id: twophase.fs,v 1.3 2010/09/13 07:46:49 djg11 Exp $  
 * (C) 2010 MJ Nam and DJ Greaves.
 *
 * Here is the two phase handshaked defined over the following nets.
 *
 *)
module twophase
open hprls_hdr
open meox
open joindefs

let x_net = gec_X_net

(* The net directions are as seen on the joiner for the male (data sending) side but the protocol specification is unsexed. *)

let two_phase_handshake_nets = 
  [ (simplenet "Strobe2",       Ndi OUTPUT, gen_Concrete_enum [xi_num 1; xi_num 0]) 
    (simplenet "Ack2",          Ndi INPUT,  gen_Concrete_enum [xi_num 1; xi_num 0]) 
    (vectornet_w("DL", 8),     Ndi OUTPUT, gen_Symbolic_bitvec "" true 8)
  ]


// Idle when the req==ack.
let two_phase_handshake_idle =  
 [
  (g_deadval,    x_net "DL");
  (x_net "Ack2",   x_net "Strobe2");
 ]
;


// Initial is not idle for the two phase handshake!

let two_phase_handshake_initial =
 [
  (g_deadval,    x_net "DL");
  (xi_num 0,   x_net "Ack2");
  (xi_num 0,   x_net "Strobe2");
 ]


let xi_n v = xi_blift(xi_not(xi_orred v))
let two_phase_handshake_protocol(pred) =  
 PS_seq[ 
  Set(pred,                   x_net "DL");
  Set(xi_n(x_net "Strobe2"),  x_net "Strobe2");
  Set(g_deadval,                x_net "DL");
  Set(xi_n(x_net "Ack2"),     x_net "Ack2");
 ]


(* Overall composition of the protocol in a direction-agnostic form *)
let twophase pred = ("2P", two_phase_handshake_nets,
                     two_phase_handshake_initial,
                     two_phase_handshake_idle, Asynch(two_phase_handshake_protocol pred))

