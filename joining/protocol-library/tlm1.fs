(*
 * $Id: tlm1.fs,v 1.15 2010/09/13 07:46:49 djg11 Exp $  
 * (C) 2010 MJ Nam and DJ Greaves.
 * Here is the simplest TLM protocol. 
 *
 *)
module tlm1
open meox
open hprls_hdr
open joindefs

let x_net = gec_X_net

(* The net directions are as seen on the joiner for a target (entry point) style joiner. *)

let tlm1_writer_handshake_nets = // DH is an input
  [ (simplenet "call",        Ndi INPUT,  gen_Concrete_enum [xi_uqstring "idle"; xi_uqstring "active"]) 
    (vectornet_w("DH", 8),    Ndi INPUT, gen_Symbolic_bitvec "" true 8)
  ]

let tlm1_reader_handshake_nets = // DH is an output
  [ (simplenet "call",        Ndi INPUT,  gen_Concrete_enum [xi_uqstring "idle"; xi_uqstring "active"]) 
    (vectornet_w("DH", 8),    Ndi OUTPUT, gen_Symbolic_bitvec "" true 8)
  ]


let tlm1_handshake_idle =  
 [
  (g_deadval,             x_net "DH");
  (xi_uqstring "idle",  x_net "call");
 ]


// This is for a TLM entry point that is a data sink, such as write(d, a)
// Live and active occur at once from our point of view.
let tlm_writer_handshake_protocol =  
 PS_seq[
  Setl [ (xi_uqstring "DHdata",    x_net "DH");
         (xi_uqstring "active",        x_net "call");
       ];
  Set(g_deadval,                     x_net "DH");
  Set(xi_uqstring "idle",          x_net "call");
  ]


// This if for TLM entry point that is a data source, such as getchar()
// Dead and idle occur at once from the converter's point of view
let tlm_reader_handshake_protocol =  
 PS_seq[
  Set(xi_uqstring "active",        x_net "call");
  Set(vectornet_w("DHread", 8),    x_net "DH");
  Setl [
         (xi_uqstring "idle",          x_net "call");
         (g_deadval,                     x_net "DH");
       ];

  ]



(*

The product machine has a state transition when the TLM caller puts
its data live, but of course the glue logic will not see this until
the call state goes live, so it cannot really make a transition at
that point.  Therefore a pre-processing of the participating state
machines is live that elides the two operations into one is
appropriate.

This is for a TLM entry point that is a data sink.  If it were a
source, the data would be live as the call goes idle.

In the example live and active occur at once, whereas the dead
transition is not elided, since the glue will make an active
transition to read out the contents of the register.

In a complementary transactor where the TLM is a write upcall, that
has the same overall pattern, it is the going dead transition that
cannot be seen by the glue, and the going dead and call going idle are
elided in the preprocessing.  *)

(* Overall composition of the protocol in a direction-agnostic form *)

let tlm_writer8 = ("tlwriter8", tlm1_writer_handshake_nets, tlm1_handshake_idle, tlm1_handshake_idle, Asynch tlm_writer_handshake_protocol)


let tlm_reader8 = ("tlreader8", tlm1_reader_handshake_nets, tlm1_handshake_idle, tlm1_handshake_idle, Asynch tlm_reader_handshake_protocol)


(* eof *)
