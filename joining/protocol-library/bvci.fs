(*
 * $Id: bvci.fs,v 1.11 2010/09/13 07:46:49 djg11 Exp $  
 * (C) 2010 MJ Nam and DJ Greaves.
 *
 * Here is the BVCI bus
 *
 *)
module bvci

open hprls_hdr
open meox
open joindefs

let x_net = gec_X_net

(* The net directions are as seen on the joiner for the male (data sending) side but the protocol specification is unsexed. *)

// Unfortunately just part of bvci for now : no response port even !
// TODO
// rdata will be in response port ... please finish
let bvci_nets = 
  [ (simplenet "cmdreq",       Ndi OUTPUT,     false, gen_Concrete_enum [xi_num 1; xi_num 0]); 
    (simplenet "cmdack",       Ndi INPUT,      false, gen_Concrete_enum [xi_num 1; xi_num 0]);
    (vectornet_w("addr", 32),     Ndi OUTPUT,  true, gen_Symbolic_bitvec "" true (32));
    (vectornet_w("rdata", 32),     Ndi INPUT,  true, gen_Symbolic_bitvec "" true (32));
    (vectornet_w("wdata", 32),     Ndi OUTPUT, true, gen_Symbolic_bitvec "" true (32));    
    (vectornet_w("bvcmd", 2),     Ndi OUTPUT,  false, gen_Concrete_enum [xi_string "Idle"; xi_string "Write"; xi_string "Read"]); 
  ]


let bvci_ocp_idle =  
 [
  (g_deadval, x_net "addr");
  (g_deadval, x_net "rdata");
  (g_deadval, x_net "wdata");  
  (xi_string "Idle", x_net "bvcmd"); // dont care really...
  (xi_num 0, x_net "cmdreq");  
  (xi_num 0, x_net "cmdack")
 ]


let bvci_ocp_initial = bvci_ocp_idle




let ad32 = xi_string "ad32"


let bvci_write_protocol =  
 PS_seq[
  Setl [(g_deadval, x_net "rdata");
        (xi_string "wd32", x_net "wdata");
        (ad32, x_net "addr");
        (xi_string "Write", x_net "bvcmd");
        (xi_num 1, x_net "cmdreq");
      ];

   Set  (xi_num 1, x_net "cmdack");
  
   Setl [
          (g_deadval, x_net "wdata");
          (g_deadval, x_net "addr");
          (xi_string "Idle", x_net "bvcmd");
          (xi_num 0, x_net "cmdreq");
      ];

    Set  (xi_num 0, x_net "cmdack");
  ]


let bvci_read_protocol =  
 PS_seq[
  Setl [(g_deadval, x_net "rdata");
        (g_deadval, x_net "wdata");
        (ad32, x_net "addr");
        (xi_string "Read", x_net "bvcmd");
        (xi_num 1, x_net "cmdreq");
      ];

 
    Set  (xi_num 1, x_net "cmdack"); 

    Set (g_deadval, x_net "addr");

    Set  (xi_string "rd32", x_net "rdata");

    Setl [
          (xi_string "Idle", x_net "bvcmd");
          (xi_num 0, x_net "cmdreq");
      ];
    Setl [  (xi_num 0, x_net "cmdack");
            (g_deadval, x_net "rdata");
         ]
  ]


let bvci_ocp_protocol =
       bvci_read_protocol
//    Disjunction [ bvci_write_protocol; bvci_read_protocol ]



(* Overall composition of the protocol in a direction-agnostic form *)
let g_canned_bvci32 = ("bvci32", bvci_nets,  bvci_ocp_initial, bvci_ocp_idle, Synch bvci_ocp_protocol)



// eof
