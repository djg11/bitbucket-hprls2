(*
 * $Id: fourphase.fs,v 1.10 2010/09/13 07:46:49 djg11 Exp $  
 * (C) 2010 MJ Nam and DJ Greaves.
 *
 * Here is the four phase handshaked defined over the following nets.
 *
 *)
module fourphase
open hprls_hdr
open meox
open joindefs

(* The net directions are as seen on the joiner for the male (data sending) side but the protocol specification is unsexed. *)

let x_net = gec_X_net

let four_phase_handshake_nets = 
  [ (simplenet "Strobe",       Ndi OUTPUT, concrete_bool())
    (simplenet "Ack",          Ndi INPUT,  concrete_bool()) 
    (vectornet_w("DL", 8),     Ndi OUTPUT, gen_Symbolic_bitvec "" true 8)
  ]


let four_phase_handshake_idle =  
 [
  (g_deadval,    x_net "DL");
  (xi_num 0,   x_net "Strobe");
  (xi_num 0,   x_net "Ack")
 ]


let four_phase_handshake_initial = four_phase_handshake_idle


let four_phase_handshake_protocol(pred) =  
   PS_seq[  Set(pred,         x_net "DL");
         Set(xi_num 1,     x_net "Strobe");
         Set(xi_num 1,     x_net "Ack");
         Set(g_deadval,    x_net "DL");
         Set(xi_num 0,     x_net "Strobe");
         Set(xi_num 0,     x_net "Ack")
    ]


(* Overall composition of the protocol in a direction-agnostic form *)
let fourphase pred = ("4P", four_phase_handshake_nets,  four_phase_handshake_initial, four_phase_handshake_idle, Asynch(four_phase_handshake_protocol pred))

// eof
