(*
 * $Id: stdsynch.fs,v 1.11 2010/09/13 07:46:49 djg11 Exp $  
 * (C) 2010 MJ Nam and DJ Greaves.
 *
 * Here is the STDSYNCH bus
 // The standard synchronous protocol is a strict subset of hfast with only one data direction.
 *)
module stdsynch

open hprls_hdr
open meox
open joindefs

let x_net  ss = gec_X_net ss
let xb_net ss = xi_orred(gec_X_net ss)

(* The net directions are as seen on the joiner for the male/initiating side but the protocol specification is unsexed. The male side is the data source in most cases. *)


// NOTE: The net-level and TLM form of a port share the same data netport_pattern_schema, but nearly always differ in the associated handshake schema (e.g. blocking TLM has none), so these are kept separate and joined (just appended quite often) as necessary.

let g_standard_synchronous_netport_schema =  // Standard synchronous protocol... the need to have an associated director is not clear.
 [ // The order of listed the nets here may be relied on below. Sdir is the direction on the slave/target/completer.
  { g_null_netport_pattern_schema with lname="VALID";    width="1";           sdir="i";    idleval="0"; ats=[(g_control_net_marked, "true")] };
  { g_null_netport_pattern_schema with lname="RDY";      width="1";           sdir="o";    idleval="0"; ats=[(g_control_net_marked, "true")] };
  { g_null_netport_pattern_schema with lname="DATA";     width="DATA_WIDTH";   sdir="i";     };
 ]

// Going forward, this needs to use, pass-on or hydrate the canned g_standard_synchronous_netport_schema
let stdsynch_nets burstf dwidth = 
    let dname = sprintf "ddata%i" dwidth
    let evergreenf = true
    let nets = 
      [ (simplenet "dvalid",          Ndi OUTPUT, false, concrete_bool()) 
        (simplenet "drdy",            Ndi INPUT,  false, concrete_bool())
        (vectornet_w(dname, dwidth),  Ndi OUTPUT, evergreenf, gen_Symbolic_bitvec "" evergreenf dwidth)
      ]

    if burstf then
        let ll =         (simplenet "dlast",           Ndi OUTPUT,  true, concrete_bool()) // This bool is evergreen, hence having that flag inside Symbolic_bitvec is a poor type. Hence replicated explicitly is good.
        nets  @ [ll]
    else nets // Also return schema please.


// The idle predicate is a logic expression that is not a simple conjunction. Indeed it is that drdy and dvalid are not both asserted.
let stdsynch_generic_idle burstf dwidth =  
    let dname = sprintf "ddata%i" dwidth
    let pred = 
     [
        (g_deadval, x_net dname);
        (xi_num 1, x_net "drdy");   // Start READY but not valid.
        (xi_num 0, x_net "dvalid")
     ]
    if burstf then (xi_num 0, x_net "dlast"):: pred else pred

let stdsynch_generic_initial = stdsynch_generic_idle

// There are two states: busy and idle, with no restrictions on which follows which.


// Back-to-back cycle problem as always - reflexive edges needed, or epsilion transitions.  Holding reg design is easier to consider?
// PS_next should be used here since synchronous


/// The single arc approach if labelled with the conjunction will reflect a dependency, hence it is CORRECT TO ADD the soft arc asserting valid only for outside.  For inside, such an arc will become a guard but we should be ok using the 'default' value of inside.drdy.

// TODO burstf missing here ... uses DLAST
let stdsynch_write_protocol burstf_ dwidth =   // generic actually
   let dbus = x_net (sprintf "ddata%i" dwidth)
   let zig_and a b = xi_blift(ix_and (xi_orred a) (xi_orred b))
   let clkpos = ("clk", "^")

   PS_alt
       [
          PS_setlq(xi_not(ix_and (xb_net "dvalid") (xb_net "drdy")),
                    //(g_deadval, dbus)
                   [], [clkpos]) // These first two are better phrasesd as a ccon/ disjunction.
          PS_seq [
//                   PS_setlq(xb_net "dvalid", [], []) 
                   PS_setlq(ix_and (xb_net "dvalid") (xb_net "drdy"),
                            [
                             (g_powerval, dbus)
                            ], [clkpos]) // These first two are better phrasesd as a ccon/ disjunction.
                 ]
       ]

  
let stdsynch_write_protocol__ burstf_ dwidth =  // This is very declarative!
    let dbus = x_net (sprintf "ddata%i" dwidth)
    let lhs = ix_and (xi_orred (x_net "drdy")) (xi_orred (x_net "dvalid"))
    let p1 = ix_deqd (xi_string "xd32") dbus
    let p2 = ix_deqd g_deadval  dbus    
    Par [ Always (ix_or p1 p2);   Always(ix_xnor p1 lhs) ]


let stdsynch_generic_protocol burstf dwidth = stdsynch_write_protocol burstf dwidth


(* Overall composition of the protocol in a direction-agnostic form *)
let gec_canned_stdsynch burstf dwidth =
    let name = sprintf "stdsynch%s%i" (if burstf then "_b" else "") dwidth
    (name, stdsynch_nets burstf dwidth,  stdsynch_generic_initial burstf dwidth, stdsynch_generic_idle burstf dwidth, Synch(stdsynch_generic_protocol burstf dwidth))



// eof

