(*
 * $Id: credit_channel.fs,v 1.11 2010/09/13 07:46:49 djg11 Exp $  
 * (C) 2010 MJ Nam and DJ Greaves.
 *
 * Here is a simplex credit-controlled channel.  An example where initial is not the same as idle! Like two-phase.
 *
 *)
module credit_channel

open hprls_hdr
open meox
open yout
open joindefs
open moscow

let x_net  ss = gec_X_net ss
let xb_net ss = xi_orred(gec_X_net ss)

let gen_Counter_bitvec maxval initval =

    if initval <> 0 then sf "initval not zero L19"
    let ss = [0..maxval]
    gen_Concrete_enum (List.map xi_num ss) // need fresh ones owing to ref inside.
    
(* The net directions are as seen on the joiner for the male/initiating side but the protocol specification is unsexed. The male side is the data source in most cases. *)

let gec_canned_credit_channel burstf dwidth max_credit initial_credit = 
    let maxval = max_credit:int32
    let initval = initial_credit
    let cwidth = bound_log2(System.Numerics.BigInteger maxval)    
    let credit_counter = vectornet_w("credits", cwidth)
    if initial_credit > max_credit then sf "initial credit exceeds max credit"
    let dname = sprintf "ddata%i" dwidth
    let dbus = x_net dname

    let credit_channel_nets =  //  ODD MIX OF simplenet and x_net here!
        let evergreenf = true
        let nets = 
          [ (simplenet "VALID",           Ndi OUTPUT, false, concrete_bool())  //
            (simplenet "LCRDV",           Ndi INPUT,  false, concrete_bool())  // Credit return
            (vectornet_w(dname, dwidth),  Ndi OUTPUT, evergreenf, gen_Symbolic_bitvec "" evergreenf dwidth)
            (credit_counter,              Ndi LOCAL,  false, gen_Counter_bitvec maxval initval) 
          ]

        if burstf then
            let ll =         (simplenet "dlast",           Ndi OUTPUT,  true, concrete_bool()) // This bool is evergreen, hence having that flag inside Symbolic_bitvec is a poor type. Hence replicated explicitly is good.
            nets  @ [ll]
        else nets

    let lcrdv = x_net "LCRDV"
    let valid_net = x_net "VALID"
    let credit_channel_generic_idle =  
        let pred = 
         [
            (g_deadval, dbus)
            (xi_num 0, lcrdv)
            (xi_num 0, valid_net)
         ]
        if burstf then (xi_num 0, x_net "dlast"):: pred else pred

    let credit_channel_generic_initial =
        let init = (xi_num initial_credit, credit_counter) 
        init :: credit_channel_generic_idle

/// The single arc approach if labelled with the conjunction will reflect a dependency, hence it is CORRECT TO ADD the soft arc asserting valid only for outside.  For inside, such an arc will become a guard but we should be ok using the 'default' value of inside.drdy.

    let protocol =
       let clkpos = ("clk", "^")

       // Some non-blocking semantics here are not properly respected within the Seq elaboration are they?
       // Also, left/right symmetric semantics dont distinguish an inc from a dec !
       let dec_credit = (credit_counter, ix_minus (xi_num 1) credit_counter)
       let inc_credit = (credit_counter, ix_plus  (xi_num 1) credit_counter)           

       let powerstroke = (g_powerval, dbus)
       let assert_valid = (xi_num 1, valid_net)
       let deassert_valid = (xi_num 0, valid_net)        
       // Commanded nets must stay the same if not changed.
       // Powerstroke nets are idle if not commanded as powerstrokes.

       let creditize0 = PS_setlq(xi_not (xi_orred lcrdv), [], []) // nop
       let creditize1 = PS_setlq(ix_and (xi_orred lcrdv) (ix_dltd credit_counter (xi_num max_credit)), [inc_credit], [])        
       let creditize = PS_alt [creditize1; creditize0]

        // Credit received when full?

       let sendize0 = PS_setlq(ix_dled credit_counter (xi_num 0),   [deassert_valid], [])
       let sendize1 = PS_setlq(ix_dgtd credit_counter (xi_num 0),   [dec_credit; powerstroke; assert_valid], [])
       let sendize = PS_alt [sendize1; sendize0]
        
       let clockize = PS_setlq(X_true, [], [clkpos])

       PS_seq [ creditize; sendize; clockize]

    let credit_channel_generic_protocol = protocol


    (* Overall composition of the protocol in a direction-agnostic form *)

    let name = sprintf "credit_channel%s_%i_%i_%i" (if burstf then "_b" else "") dwidth initial_credit max_credit
    (name, credit_channel_nets,  credit_channel_generic_initial, Synch credit_channel_generic_protocol)



// eof

