(*
 * $Id: tlm16.fs,v 1.5 2010/09/13 07:46:49 djg11 Exp $  
 * (C) 2010 MJ Nam and DJ Greaves.
 *
 * Here's a sixteen-bit TLM protocol that handles a pair of bytes placed in parallel form.
 *)
module tlm16
open meox
open hprls_hdr
open joindefs

let x_net = gec_X_net

(* The net directions are as seen on the joiner for a target (entry point) style joiner. *)

let tlm16_handshake_nets = 
  [ (simplenet "call",           Ndi INPUT,  gen_Concrete_enum [xi_uqstring "idle"; xi_uqstring "active"]) 
    (vectornet_w("DH16", 16),    Ndi INPUT, gen_Symbolic_bitvec "" true 16)
  ]


let tlm16_handshake_idle =  
 [
  (g_deadval,          x_net "DH16");
  (xi_uqstring "idle", x_net "call");
 ]


(* This is for a TLM entry point that is a data sink.  If it were a source, the data would be live as the call goes idle. *)
let tlm16_handshake_protocol =  
    let dh_hi = vectornet_w("DH_hi", 8)
    let dh_lo = vectornet_w("DH_lo", 8)
    let dataexp = ix_bitor (ix_lshift dh_hi (xi_num 8)) dh_lo
    PS_seq[
           Set(dataexp,  x_net "DH16");
           Set(xi_uqstring "active", x_net "call");
           Set(g_deadval,            x_net "DH16");
           Set(xi_uqstring "idle",   x_net "call");
       ]



(* Overall composition of the protocol in a direction-agnostic form *)
let tlm16 = ("TLM16", tlm16_handshake_nets,
               tlm16_handshake_idle, tlm16_handshake_idle, Asynch tlm16_handshake_protocol)


(* eof *)
