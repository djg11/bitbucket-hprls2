//
// This wrapper makes the jinjoin code into a plugin for HPR Orangepath.
//
// Greaves/Nam Transactor and Glue Logic Generator.  Automatic synthesis of bus protocol adaptors.
// (C) 2010-16 D J Greaves, University of Cambridge, Computer Laboratory.
//   
// Based on 'Synthesis of glue logic, transactors, multiplexors and serialisors from protocol specifications'. By D. J. Greaves, M. J. Nam. At Forum on Specification & Design Languages (FDL 2010). 14th-16th Sept. 2010 Southampton.


// Cross-product glue logic and transactor generator.


// Need to do auto plugin scanning but load some manually here.
open jinjoin
open conerefine
open bevelab
open verilog_gen
open cpp_render
open opath
open hprxml

[<EntryPoint>]
let main (argv : string array) =
    yout.g_version_string := joiner_banner()
    yout.set_opath_report_banner_and_name([joiner_banner()], "jinjoin.rpt")
    let _ = jinjoin_used ()
    let _ = install_coneref()
    let _ = install_bevelab()
    let _ = install_verilog()
//    let _ = install_diosim()
    let _ = install_c_output_modes()
    let rc = opath_main ("joiner", argv)
    rc

