(*
 * Greaves/Nam Transactor and Glue Logic Generator.  Automatic synthesis of bus protocol adaptors.
 * (C) 2010-16 DJ Greaves, University of Cambridge, Computer Laboratory.
 * (C) 2010 MJ Nam and DJ Greaves.
 *
 * Here is an eight-bit holding register and others.
 *
 *)
module holder8

open hprls_hdr
open meox
open joindefs

(* The net directions are for the male (data sending) side but the protocol specification is unsexed. *)

let holder_w_nets w = 
  [ 
    (vectornet_w(sprintf "H%i" w, 8),     Ndi LOCAL, gen_Symbolic_bitvec w )
  ]


let holder_w_idle w =  
 [
  (g_deadval, gec_X_net (sprintf "H%i" w ));
 ]


let holder_w_initial w  = holder_w_idle w


// Holder registers should not really need a protocol !
let holder_w_protocol w =  
  PS_seq[
      Set(vectornet_w(sprintf "LIVEH%i" w, w), gec_X_net (sprintf "H%i" w));
      Set(g_deadval, gec_X_net (sprintf "H%i" w));
    ]

let holder_w_gen w = (sprintf "HOLDER%i" w, holder_w_nets w, holder_w_initial w, holder_w_idle w, Either (holder_w_protocol w))


let holder8  = holder_w_gen 8
let holder16 = holder_w_gen 16
let holder32 = holder_w_gen 32
let holder64 = holder_w_gen 64
(* eof *)
