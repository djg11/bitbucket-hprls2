(*
 * Greaves/Nam Transactor and Glue Logic Generator.  Automatic synthesis of bus protocol adaptors.
 * (C) 2010-16 D J Greaves, University of Cambridge, Computer Laboratory.
   
 * Based on 'Synthesis of glue logic, transactors, multiplexors and serialisors from protocol specifications'. By D. J. Greaves, M. J. Nam. At Forum on Specification & Design Languages (FDL 2010). 14th-16th Sept. 2010 Southampton.

 * Cross-product glue logic and transactor generator.
 *
 * Base on $Id: jinjoin.fs,v 1.52 2012/11/26 08:48:27 djg11 Exp $
 *
 * (C) 2010 MJ Nam and DJ Greaves.
 * 2016 Updated.
 *)

module jinjoin

open System.Numerics
open System.Collections.Generic


open opath_hdr
open holder8
open hprls_hdr
open joindefs
open abstract_hdr
open dotreport
open abstracte

// canned protocols
open twophase
open fourphase
open tlm1
open tlm16
open bvci
open holder8
open isa
open stdsynch

open moscow
open meox
open yout
open cpp_render
open hprxml

let g_green_limit = 2
let g_very_different = 12345678

let g_jinjoin_banner = "CBG JinJoin Version 2.01 (Dec 2019)"

let joiner_banner() =
    g_jinjoin_banner

// Active pattern that can be used to assign values to symbols in a pattern
let (|Let|) value input = (value, input)


(*For a specific xactor we need to specify which pins are in and which are out, so may need to map swap_net_sex over the net list *)


// e.g. use dot -Tpng bv32.dot | xv -
// Write a dot file for the dot graphviz visualisation tool.



// Convert abstract net names from protocol spec equations to concrete net names.
// Not used ... we use recode assoc_exp instead.
let net_retrieve nets = function
    | X_bnet ff ->
        X_bnet ff

    | X_net(ss, _) ->
        muddy (sprintf "Nets in scope for %s %A" ss nets)


    | other -> sf(sprintf "Net retrieve other form %A" other)

//(netname:string) = muddy (sprintf "net retrieve invoked %s" netname)



let write_dotfile ww iname m contents = 
    let dotname = iname + ".dot"
    let dotfd =  yout_open_out dotname
    dotout dotfd (DOT_DIGRAPH(iname, contents))
    yout_close dotfd
    vprintln 0 ("Wrote " + m + " dot file '" + iname + "' with " + i2s(length contents) + " arcs+nodes")
    ()


type djc_t = netdir_t * jspec_t

type back_address_t = string // TODO better called a forward_link_address_t

type svitem_t =
    | SV_4 of int option * hexp_t * dcx_life_t * djc_t // aka a segment: subscript_o, variable, value, coding.
    | SV_cre of back_address_t * conservation_regexp_t * bool // bool denotes donef, tainted mechanism, in which case regexp can be ignored and must be ignored in back-edge matching.  But we are instead using CRE_end and not setting the flag, which can be deleted.


type deltas_t = (hexp_t * dcx_life_t * dcx_life_t * djc_t) list // Net, ov, nv, dc.

type unisymb_cache_t = Dictionary<string, ((svitem_t list * string) * deltas_t) list>

type m_busses_t = OptionStore<string, djc_t>

type jinjoin_settings_t =
    {
        unisymb_cache:           unisymb_cache_t
        integer_pcvals:          bool
        generate_bevcode:        bool
        m_busses:                m_busses_t
        opfname:                 string
        op_kindname:             string
        pc_name:            string
        discard_participant_pcs: bool  // These may be kept for debugging/monitoring.
        dotplot_glue:            bool
        dotplot_participants:    bool
        m_report_file_contents:  string list ref
        m_states_report_lines:   string list ref
        prod_vd:                 int   // loging level for main product procedure
        newrender_vd:            int   // loging level for output VM construction
    }


// Verbosity control:
let g_xvd = ref 4 // nominally 3 - higher for more output.
         
let report (x:string) = vprintln 0 x

let g_m_next_arc_no = ref 800

let get_next_arc_no iname arcname =
    let rr = !g_m_next_arc_no
    vprintln 3 (sprintf "Allocate arc number %i for %s %s" rr iname arcname)
    mutinc  g_m_next_arc_no 1
    sprintf "%s%i" arcname rr


type clockprops_t = bool // for now. Flag set for synchrnous edge.
    
(*
 * We use a part_t record for each participating interface:
 *)
type part_t = { kind:         string 
                pi_name:      string
                aliasnames:   string list
                pnets:        (hexp_t * netdir_t * bool * jspec_t) list  // bool is old site of commandedf - deprecated
                Pc:           hexp_t option
                Initial_pred: (hexp_t * hexp_t) list
                Idle_pred:    hbexp_t                 

                databus:      (hexp_t * netdir_t * bool * jspec_t) option

                // old arcs
                arcs:         (string * clockprops_t * (hexp_t * hexp_t) * hbexp_t * hbev_t list * hexp_t list) list  // (arc_uid, from_state, condition, actions, to_states)
                states:       hexp_t list
                
              }

let g_bs = (xi_string "++unspecified") 


let ndiToStr = function
    | Always_input -> "Always_input"
    | Ndi other -> varmodeToStr other
   
    
let rec codingToStr = function
    | NS_unused          -> "NS_unused"
    | Concrete_enum lst  -> "Concrete:[" + sfold (fun (a,b) -> (if !a then "+" else "") + xToStr b) lst + "]"
    | Symbolic_bitvec v  when v.evergreenf2 -> sprintf "Evergreen(%i)" v.bits
    | Symbolic_bitvec v  -> "Symbolic:(" + i2s(v.bits) + ")[" + sfold (fun (a,b)-> (if !a then "+" else "") + lifeToStr b) !v.life + "]"

let jspecToStr jspec =
    (sprintf "anon=%A e/green=%A " jspec.anonj jspec.evergreenf)  +  codingToStr jspec.coding

let is_symbolic = function
    | Symbolic_bitvec _ -> true
    | _                 -> false

let is_evergreen = function
    | Symbolic_bitvec q -> q.evergreenf2
    | _                 -> false

    
let djToStr (dir, jspec) = ndiToStr dir + " " + codingToStr jspec.coding

let m_next_arc_idx = ref 1
let next_arc_idx() =
    let rr = !m_next_arc_idx
    mutinc m_next_arc_idx 1
    rr


//
// We only need to data conserve on our internal transfers.  
// These defintions of golive and godead detect operations inside the joining converter
// that require data conservation.   OLD?: The external clients also drive inputs live and outputs
// dead, but we should not capture those.  External data busses are commanded in both directions now, but the internal conveyence would not be commanded
// and want to be detected by golive and godead for data conservation.
// Note: If we are commanding both ways externally, we can still 'see' which direction of data is going: the godead on an input needs conserving and the golive on an output needs conserving and both need conserving on a local.



// 1/2  -  inputs go live by protocol commands, but go dead unilaterally by data conserving evictions.... OLD
// For powerstrokes it is an admit or evict depending on bus direction. We only have power strokes on i/o busses.
// For non-i/o registers, assign of DL_dead is an eviction and assign of anything other than DL_dead is an admission (because for assembly it is an admission and for disassembly there is special code needed SAY WHERE IT IS).    
let isadmit_golive var (vale, (dir, jspec)) = // An output makes an admission as its receives the data.
    let ans =
        if is_symbolic jspec.coding then
            match dir with
                | Ndi INPUT
                | Always_input -> false // Inputs do mesh with admissions, they only ever evict into our generated glue.
                | Ndi OUTPUT   -> vale<>DL_dead // This includes g_DL_powerstroke
                | _            -> vale<>DL_dead
        else false
        // was vale<>DL_dead && (dir<>Ndi INPUT && dir<>Always_input) 
    //dev_println(sprintf "PMM isadmit_golive %s := %s       ans=%A" (netToStr var) (lifeToStr vale) ans)
    ans

// Outputs go live unilaterally as our glue copies values in, and they go dead when commanded by the output port protocol. 
// So the protocol compiler needs to implement the command or the guard according to the direction of movement.
let isevict_godead var (vale, (dir, jspec)) = // An input makes an eviction as it supplies data.
    let ans =
        if is_symbolic jspec.coding then
            match dir with
                | Ndi INPUT
                | Always_input -> vale=DL_dead || vale=g_DL_powerstroke // Power stroke denoted with DL_active even if an eviction
                | Ndi OUTPUT   -> false // Outputs do mesh with evictions, they only ever admit data as it leaves our generated glue.
                | _            -> vale=DL_dead 

            // was: (vale=DL_dead || vale=g_DL_active) && dir<>Ndi OUTPUT

        else false
    //dev_println(sprintf "PMM isevict_godead %s := %s       ans=%A" (netToStr var) (xToStr vale) ans)
    ans 


let g_jin_concise_set = new HashSet<xidx_t>()
let g_jin_concise_min = ref -1


let xb2str g0 =
    let g1 = abs g0
    sprintf "xb%s%i" (if g0<0 then "!" else "") (g1 + 10 - !g_jin_concise_min)


let concise_xbToStr gg =
    if xi_istrue gg then "T"
    elif xi_isfalse gg then "F"
    else
        let g0 = xb2nn gg 
        let g1 = abs g0 
        if not (g_jin_concise_set.Contains g1) then 
            if !g_jin_concise_min < 0 then g_jin_concise_min := g1
            ignore(g_jin_concise_set.Add g1)
            vprintln 3 (sprintf "Add %s to concise guard set for %s" (xb2str g1) (xbToStr(deblit g1)))
        xb2str g0

let jin_concise_report vd =
    let rl g1 = 
        vprintln vd (sprintf "Report concise:  %s denotes %s" (xb2str g1) (xbToStr(deblit g1)))    
    for g1 in g_jin_concise_set do rl g1 done

//
//
//
let arcToStr (uid, clkprops, g1, g2, cmds, dests) = 
    let cmdf cmd = hbevToStr_cleaned "" cmd 
    let show_deqd (r,l) = xToStr l + "==" + xToStr r  + ", "
    (sprintf "  :ARC-%s clk=%A " uid clkprops + show_deqd g1 + concise_xbToStr g2 + " / " + (sfold cmdf cmds) + " --> " + sfold xToStr dests)



let participant_report ww settings iname kind participant = 
    // Log details of the participant to a file and also write it as dot
    let reportname = iname + ".rpt"
    let banner = sprintf ": Report on participant iname=%s   kind=%s  reportname=%s" iname kind reportname
    let ww = WF 2 "participant_report" ww banner
    let fd = yout_open_out reportname
    let report ss =
        vprintln 2 ss
        youtln fd ss
    report banner
    report ""
    report (sprintf "Participant Nets for %s:" iname)    
    let netrep (id, dir, _, jspec) =
        let k = (if is_symbolic jspec.coding then " so far" else "")
        report (sprintf " netrep:  %s  %s commandedf=%A" (netToStr id) (djToStr(dir, jspec)) jspec.commandedf + " and has " + i2s(n_inuse_coding_states jspec.coding) + " states" + k) 
    app netrep participant.pnets
    report ""
    report (sprintf "Protocol arcs (%i of them) for %s:" (length participant.arcs) iname)
    app (fun arc -> report(arcToStr arc)) participant.arcs
    let nodename x = string_escape_generic [  ] (xToStr x)
    let (ns, na) = (length participant.states, length participant.arcs)
    report (banner + sprintf ": Protocol with %i states and %i arcs"  ns na)
    report ""
    report (sprintf "Participant Initial pred for %s:" iname)
    app (fun (a, b) -> report ( "   "  + xToStr a + " === " + xToStr b)) participant.Initial_pred
    report ""
    report (sprintf "Participant Idle pred for %s:" iname)
    report (xbToStr participant.Idle_pred)
    report "EOF"    
    yout_close fd
    if settings.dotplot_participants then
        if na + ns > 5000 then
            vprintln 1 (sprintf "Skip writing of overly large dot file for participant.")
        else
            let dot2 =
               let m_dot2 = ref []
               let kx g1 s d = mutadd m_dot2 (DARC(gec_DNODE_plain(nodename (fst g1)), gec_DNODE_plain(nodename d), s))
               let garc (uid, clockprops, g1, g2, cmds, dlst) =
                   let color = if clockprops then "red" else "black"
                   let s = [ ("color", color); ("label", (* string_can *) string_autoinsert_newlines 55 (uid + "\n" + xbToStr g2 + "/\n" + sfoldcr (hbevToStr_cleaned "") cmds)) ]
                   app (kx g1 s) dlst
               app garc participant.arcs
               !m_dot2

            write_dotfile ww iname "participant" dot2
    // end of participant_report


let clkvalof = function
    | Synch x -> x
    | Asynch x -> x
    | Either x -> x

// Add in a symbolic value to the state list of a symbolic net if it is not there already.
let ensure_symb_state mut net v state =
    match nassoc net 0 state (!v.life) with
        | Some ov -> ov
        | None ->
            let ol = length !v.life
            let _ = mutaddtail (v.life) (ref mut, state)
            let _ = if ol > v.maxcount then cleanexit (sprintf "Too many values symbo values for %s: Please run again with higher g_maxg_symbo_maxcount than %i" (netToStr net) !g_symbo_maxcount)  // TODO add command line setting
            vprintln 3 ("Added new value " + lifeToStr state + ", no=" + i2s(ol) +  "  to " + xToStr net + " mut=" + boolToStr mut)
            ol

let gec_DL_lift arg =
    match arg with
        | W_string _ when arg = g_deadval -> DL_dead
        | other -> DL_lift arg
        
let update_states mut net coding rhs =
    match coding with
        | Concrete_enum lst -> assert(nassoc net 0 rhs lst <> None)
        | Symbolic_bitvec v ->
            ignore(ensure_symb_state mut net v (gec_DL_lift rhs))


// A more convenient expression structure for algebraic manips that hexp_t/hbexp_t
        
type dmemo_t =
    | DM_sconst of hexp_t * string
    | DM_iconst of hexp_t * int64
    | DM_var of hexp_t * djc_t
    | DM_andl of dmemo_t list
    | DM_orl of dmemo_t list 
    | DM_deqd of dmemo_t * dmemo_t
    | DM_not of dmemo_t

let rec dm_isconst = function
    | DM_sconst _
    | DM_iconst _   -> true
    | DM_var _      -> false
    | DM_andl lst
    | DM_orl lst    -> conjunctionate dm_isconst lst
    | DM_deqd(l, r) -> conjunctionate dm_isconst [l;r]
    | DM_not arg    -> dm_isconst arg

let dm_val msg = function
    | DM_sconst(xit, _) -> xit
    | DM_iconst(xit, _) -> xit
    | other -> sf(sprintf "dm_val: %s other form %A" msg other)
        
let gec_DM_orl = function
    | [item] -> item
    | lst -> DM_orl lst

let gec_DM_andl = function
    | [item] -> item
    | lst -> DM_andl lst

let gec_DM_not = function
    | DM_not x -> x
    | DM_orl [] -> DM_andl []
    | DM_andl [] -> DM_orl []
    | other -> DM_not other

    
let rec dememo opq arg =
    let (ww, settings, nets, mx) = opq
    let _:(hexp_t * netdir_t * jspec_t) list = nets
    vprintln 3 (sprintf "dememo %s" (xToStr arg))
    if int_constantp arg then DM_iconst(arg, xi_manifest64 mx arg)
    else
        match arg with
            | W_string(ss, _, _) -> DM_sconst(arg, ss)
            | X_bnet ff ->
                let rec findcoding = function  // Lookup in nets
                    | [] -> cleanexit (mx + ": Net " + xToStr arg + " was not defined in " + sfold (f1o3 >> xToStr) nets)
                    | (id, dir, coding)::tt   when id=arg -> (dir, coding)
                    | _::tt  -> findcoding tt
                let coding = findcoding nets 
                DM_var(arg, coding)
            | X_blift xb -> dememo_b opq xb
                
            | other -> sf(sprintf "dememo %s: other form %s" mx (netToStr other))

and dememo_b opq arg =
    let (ww, settings, nets, mx) = opq
    vprintln 3 (sprintf "dememo_b %s" (xbToStr arg))
    match arg with
        | W_bdiop(V_orred, [item], invf, _) ->
            let a0 = dememo opq item
            if invf then gec_DM_not a0 else a0

        | W_bdiop(V_deqd, [l;r], invf, _) ->
            let a0 = DM_deqd(dememo opq l, dememo opq r)
            if invf then gec_DM_not a0 else a0

        | W_cover(cubes, _) ->
            let g3 xidx =
                if xidx = 1 then DM_andl []   // True is the empty conjunction
                elif xidx = -1 then DM_orl [] // False is the empty disjunction
                else
                    let a0 = dememo_b opq (deblit (abs xidx))
                    if xidx > 0  then a0 else gec_DM_not(a0)
            let g2 cube = gec_DM_andl(map g3 cube)
            gec_DM_orl(map g2 cubes)
        | other -> sf(sprintf "dememo_b: %s other form %s" mx (xbToStr other))
            


// We return a list of ways of satisfying the input term.
let satisfy_and_split md master_term =

    let want_i64 ivale = function
        //| DM_andl arg =
            
        | other -> sf (sprintf "want_i64 ivale=%i other form %A" ivale other)

    let rec factorise = function
        | DM_deqd(exp, DM_iconst(_, ivale))
        | DM_deqd(DM_iconst(_, ivale), exp) -> want_i64 ivale exp // unfinished ...
        | DM_andl lst ->
            let rl = map factorise lst
            (list_flatten (map fst rl), list_flatten(map snd rl))

        | DM_not(DM_deqd _) ->
            hpr_yikes(sprintf "Ignore DM_not(DM_deqd ...) for now!") 
            ([], [])

        // nnf should mean not applied to nets only and deqd only?
        | DM_not(DM_var(xit, (dir, coding))) -> bfnet true  xit (dir, coding)
        | DM_var(xit, (dir, coding))         -> bfnet false xit (dir, coding)

        | DM_deqd(cvale, exp)
        | DM_deqd(exp, cvale) when dm_isconst cvale -> // Bool comparions should not come this way owing to strict segregation in meo.
            match exp with
                | DM_var(xit, (dir, coding)) ->
                    fnet (dm_val "L398" cvale) xit (dir, coding)
                | _ -> sf (sprintf "factorise DM_deqd other form.")

        | other -> sf (sprintf "factorise other form %A" other)

    and fnet cvale xit (dir, coding) =
        match dir with
            | Ndi INPUT
            | Always_input ->
                hpr_yikes(sprintf "For now removing guard on symbolic inputs - needed for filters of course")
                ([], [(cvale, xit)]) // Keep as a command since evergreen data event must be flagged some how for now.
                //let grd = ix_deqd cvale xit
                //([grd], []) 
            | Ndi OUTPUT ->
                ([], [(cvale, xit)]) // Commands go (rhs, lhs) oddly?

    and bfnet deszero xit (dir, coding) =
        match dir with
            | Ndi INPUT
            | Always_input ->
                let grd = if deszero then xi_not(xi_orred xit) else xi_orred(xit)
                ([grd], []) 
            | Ndi OUTPUT ->
                ([], [((if deszero then xi_zero else xi_one), xit)]) // Commands go (rhs, lhs) oddly?

    let dnf = function
        | DM_orl lst -> lst
        | other -> [other]

    let disjuncts = dnf master_term
    let guard_cmds_pairs = map factorise disjuncts
    guard_cmds_pairs // end of satisfy_and_split 

// Participant compiler.
// Compile a protocol from our input language into a set of arcs of an LTS.
// TODO unify with hprls.hpr.protocols
// Net directions already rehanded.
// CNF will give clauses that all need to be satisfied, some as guards and some commanded to hold.
let precompile_participant ww settings (iname, (kind, nets0, initial_pred0, idle_pred0, protocol, aliasnames)) =
    let prefix = iname + "."
    let ww = WF 1 "precompile_participant" ww ("Start " + iname)
    let suffix = ""
    let m_nets = ref []
    let vd = 4 // get from settings please
    
    // Where we have a disjunction we want to list each separately - that's part of the clause form anyway
    // Where we have a comparison against unity for a bool we want to just orred it.
    // Where we have a comparison against zeo for a bool we want to negate its or reduction.
    // Where we have a negation applied to a conjunction (very common for stdsynch) we de-morgan it.
    // Where we have a negation applied to a disjunction we ... not normally encountered ... we ?

    let dnf = function // DNF disjunctive-normal-form aka sop form, separate terms so each term is its own arc.
        | W_cover(cubes, _) when length cubes > 1 -> map (fun cube -> xi_cover [cube]) cubes
        | other ->

            vprintln 0 (sprintf "dnf: other %s   %s" (xbkey other) (xbToStr other))
            [other]

    let gup_deqd cc (a, b) =
        let g1 = gup_ix_deqd a b // Form the equality comparison predicate (==), converting to or_reduce (|x|) when operands are testing a boolean equality
        //dev_println(sprintf "gup_deqd  g1=%s" (xbToStr g1))
        ix_and cc g1

        
    let uf1 initval (x, dir, coding) = // net generation, with uniquify
        let nets = !m_nets
        let df = function
            | Always_input -> INPUT
            | Ndi x -> x
        let vm = df dir
        match x with
            | X_bnet ff ->
                let f2 = lookup_net2 ff.n
                let ats = if nonep initval then f2.ats else nap_update f2.ats g_resetval (valOf initval)
                xgen_bnet(iogen_serf_ff(prefix+ff.id+suffix, f2.length, ff.rh, ff.width, vm, ff.signed, [], false, ats, []))
            | X_net _  -> net_retrieve nets (cloneio prefix suffix None x)

    let uf p (x) =
        let nets = !m_nets
        match x with
            | X_bnet ff -> net_retrieve nets (cloneio (p+prefix) suffix None x) // None here deletes i/o attribute
            | X_net _   -> net_retrieve nets (cloneio (p+prefix) suffix None x)
            | other -> other


    let reref = function
        | Concrete_enum lst -> gen_Concrete_enum (map snd lst)
        | Symbolic_bitvec sb -> Symbolic_bitvec{sb with life=ref(map (fun (br, vale)->(ref !br, vale)) !sb.life) }
        | other -> other
            
    let initial_vals =
        let sw (vale, net) = (xToStr net, vale)
        map sw initial_pred0
    let (nets, mm) =
        let m_nplist = ref []
        let ufx (x, dir, evergreenf, coding) =
            let initval =
                match op_assoc (xToStr x) initial_vals with
                    | None -> None
                    | Some ivale ->
                        if vd>=4 then vprintln 3 (sprintf "initial reset value for %s is %s" (xToStr x) (xToStr ivale))
                        if ivale = g_deadval then None else Some(xToStr ivale)
            let ans = uf1 initval (x, dir, coding)
            mutadd m_nplist (x, ans)
            mutadd m_nplist (gec_X_net (xToStr x), ans)            
            let jnet =
                {
                    commandedf=   false // Correct value is added later.
                    anonj=        false  
                    evergreenf=   evergreenf
                    coding=       reref coding
                }
            (ans, dir, jnet)
        let nets = map ufx nets0
        reportx 3 "Recoding" (fun (l,r) -> sprintf "  %i   %s -> %s" (x2nn l) (xToStr l) (netToStr r)) !m_nplist
        (nets, makemap !m_nplist)

    m_nets := nets

    let recode x =
        let ans = xi_rewrite_exp mm x
        //vprintln 0 (sprintf "recoded %i %s %s to %s" (x2nn x) (xkey x) (xToStr x) (xToStr ans))
        ans

    let brecode x =
        let ans = xi_brewrite mm x
        //vprintln 0 (sprintf "recoded %i %s %s to %s" (x2nn x) (xkey x) (xbToStr x) (xbToStr ans))
        ans
        
    let idle_pred = map (fun (a, b) -> (recode a, recode b)) idle_pred0 // Can apply rewriter to logic expressions please
    let initial_pred = map (fun (a, b) -> (recode a, recode b)) initial_pred0    


    // A symbolic input net is an input if is going live and a 'sort of output' if its going dead.
    // caveat
    // WRONG: A symbolic output net is an output if is going live or dead (we don't get an indication of the kill operation, we just assume it dies when its protocol says it should. HMMM : its more a matter of whether the net is connected to a client FSA.)
    // so we're feeling confident about driving outputs dead when we feel they should: why not do the same for inputs going live ?

    // A symbolic local can be assigned dead and alive freely by us. - A local with a protocol is not unilateral whereas one without is like an input.

    // This essentially always returns false!
    let symbolic_guard_pred deadopt dir = // We want an input going away from dead to be commanded, as part of the protocol ... hm for powerstrokes we want it dead values implied by absence of live
        match dir with 
        | Ndi OUTPUT -> if deadopt = g_deadval then false else false //An output going dead is not a guard either

        | Ndi INPUT  -> if deadopt = g_deadval then false else false // An input going live is not a guard: we command it as part of a protocol

        | Always_input -> if deadopt = g_deadval then false else true // ?

        | Ndi LOCAL -> false

    // Always_inputs are always inputs and locals are 'outputs' in the sense we are free to change them.
    let is_a_guard_condition deadopt nets vexp =
        let rec is_a_guard_condition_serf = function  // Lookup in nets
            | [] -> cleanexit (kind + ": Net " + xToStr vexp + " was not defined in " + sfold (f1o3 >> xToStr) nets)
            | (id, dir, Concrete_enum _)::tt   when id=vexp -> (dir <> Ndi OUTPUT && dir <> Ndi LOCAL) // An INPUT serves as a constant? What?
            | (id, dir, Symbolic_bitvec _)::tt when id=vexp -> symbolic_guard_pred deadopt dir
            | _::tt  -> is_a_guard_condition_serf tt
        match vexp with
            | X_bnet ff ->
                let ans = is_a_guard_condition_serf nets 
                vprintln 3 (sprintf "is_a_guard_condition: %s gives %A"  (netToStr vexp) ans)
                ans
            | other ->
               // Default assume it is an expression.  Its support should all be noted as part of the explorable space and ultimately outputs should be commanded and inputs monitored.
               vprintln 3 (sprintf "Protocol compiler other expression form %s  other=%s" (xkey other) (xToStr other))
               true
           
    if List.length nets <> List.length initial_pred then cleanexit(kind + ": Vague idle pred not yet implemented: initial must be all")

    let g_null_protocol = Either(PS_seq [])
    let triv = protocol = g_null_protocol

    let (pc, pc_states, startstate, commanded, nondets, arcs) = 
        let m_counter = ref 0
        let m_pcvals = ref []
        let nextstate() =
            let r = !m_counter
            mutinc m_counter 1
            //let s = xi_string(name + "_state_" + i2s r)
            let xstate = xi_stringx (XS_withval(xi_num r)) (iname + "_st_" + i2s r) 
            //vprintln (sprintf "Noting pc state %s" (netToStr xstate))
            mutadd m_pcvals xstate
            xstate


        let ensure_unary_strobes mut (exp, constval) =
            vprintln 2 ("Ensure " + xToStr exp  + " := " + xToStr constval)
            match exp with
                | X_bnet ff ->
                    //let net = uf "" lhs
                    let rec fq = function
                        | [] -> sf(sprintf "Net %s  %i " (xToStr exp) (x2nn exp) + " was not a defined net for ensuring value " + xToStr constval)
                        | (n, d, jnet)::tt -> if n=exp then update_states mut n jnet.coding constval else fq tt
                    ignore(fq nets)
                | _ -> ()
                
        let m_arclist = ref []

        let log_arc (clkf, from_state, condition, actions:hbev_t list, to_states) =
            (
                mutadd m_arclist (get_next_arc_no iname "anon", clkf, from_state, condition, actions, to_states)
                //app ensure actions
                ()
            )

        let pc_name = "PC"
        let pc_encoding_width = 10 // for now keep it constant width. // TODO bound_log2(BigInteger(length pc_states))
        let pc = uf "" (vectornet_w(pc_name, pc_encoding_width )) // These do not appear in the result anyway ...


// Where there is no PC we can have an artificial PC with one state only, or we can select any function of any variables and use its range for Shannon decomposition...
#if NOTDEF
        let guard_equation_classify1 constval var =
            let ans = is_a_guard_condition constval nets var  // This is not commutative?  Vale first, var second.
            // Also we can get differring answers per use site for a given net - some sort of wired-OR protocol could be like that.
            vprintln 3 (sprintf "Is_a_guard_condition for %s gives %A" (netToStr var) ans)
            ans

        // Equation solving (satisfying) ...

        // We have dataflow equations and guards. Both need rearranging in general.
        // Dataflow can be forced either direction by rearranging. Guard equations need converting to closed-form predicates.
        // Return true if a guard, false for a command.
        let guard_equation_classify0 (a, b) = // Check for a constant and a variable paired in either order : a closed-form equation?

            match (int_constantp a, int_constantp b) with
                | (true, false)  -> guard_equation_classify1 a b // We pass the non-constant expression to guard_equation_classify1 in the main place. The first site is a constant including deadval.
                | (false, true)  -> guard_equation_classify1 b a
                | (true, true)   -> muddy (sprintf "guard_equation_classify0: setl equation with both sides constant, hopefully decidably equivalent: compare at compile time and this becomes a guard likely arising from parameterisation of abstracted interace descriptions.")

                // The input direction should serve as a symbolic constant in this context.
                | (false, false) ->
                    false // An equation, not a guard.
                    //muddy (sprintf "Setl with neither side constant - taut? %s cf %s " (netToStr a) (netToStr b))                
#endif 
        let m_commanded = ref []
        let m_nondets = ref []  // Additional (non-deterministic) inputs - make an explicit selection of destintation - (NDA to DFA reduction by pushing the selection outside of the FSM)
                
        let rec scans current (g00, v) =   // The current_state should only be present for an FSM .... and we might have any number of FSMs in a spec
            mutaddonce m_commanded (current, pc)
            match v with // bad tabbing
            | Always bexp ->  // If we treat this as an 'imperative' always, inside an FSM state, then guarding it with pc=current and g00 makes sense.
                    current // for now

            | Par lst ->  // All of several need to hold
#if OLD
                let rlst = map (fun x -> scans current (g00, x)) lst
                let xx_net tag = uf "" (x_net tag)
                let lhs = ix_and (xi_orred (xx_net "drdy")) (xi_orred (xx_net "dvalid"))
                let p1 = ix_deqd (xi_string "xd32") (x_net "ddata")
                let p2 = ix_deqd g_deadval  (xx_net "ddata")    
                //Par [ Always (ix_or p1 p2);   Always(ix_xnor p1 lhs) ]
                let idle_cmds =   [ (g_deadval,        xx_net "ddata")  ]
                let active_cmds = [ (xi_string "xd32", xx_net "ddata")  ]  

                app (fun (r, l) -> ensure false (l, r)) (idle_cmds @ active_cmds)
                
                let cmdx = map (fun (vale, var) -> Xassign(var, vale))
                m_commanded := lst_union !m_commanded (idle_cmds @ active_cmds)
                // let (guards, cmds) = groom2 guard_equation_classify0 lst'
                //let other = nextstate()
                log_arc((current, pc), xi_not lhs,   cmdx idle_cmds,     [current])  // There is one arc in the destination set as initially logged.
                log_arc((current, pc), lhs,          cmdx active_cmds,   [other])
                log_arc((other, pc),   lhs,          cmdx active_cmds,   [other])
                log_arc((other, pc),   xi_not lhs,   cmdx idle_cmds,     [current])

                vprintln 0 (sprintf "Elaborated/Processed canned stdsynch via backdoor %s" prefix)
#endif
                current

            | PS_alt lst  // Any of several need to hold
            | Disjunction lst -> 
               let ways = length lst
               // Make a non-det unconditional transition to each of the fanouts
               let fanouts = map (fun x -> nextstate()) [ 0..(ways-1) ]
               let clkf = false
               log_arc(clkf, (current, pc), g00, [], fanouts)  // There are multiple successor dests.
               cassert (ways > 1, "one way disj")
               //let nondet = uf "" (vectornet_w(funique "disj", bound_log2 ways))
               //mutadd m_nondets (nondet, Always_input, Concrete_enum vales)
               // NO NO app (fun x-> mutadd commanded (x, nondet)) vales

               let rlst = map (fun (q, x) -> scans q (X_true, x)) (List.zip fanouts lst)
               let joinstate = nextstate()              // Make this an epsilon state please, it should consume no concrete clock cycle.  All are epsilons by default.
               vprintln 5 ("Joinstate for disjunction is " + xToStr joinstate)
               app (fun s -> log_arc(clkf, (s, pc), X_true, [], [joinstate])) rlst
               joinstate

            | Set(vale, var) -> scans current (g00, PS_setlq(X_true, [ (vale, var) ], []))

            | Setl lst -> scans current (g00, PS_setlq(X_true, lst, []))

            | PS_setlq(gg, lst, attributes) ->               

                let prams = attributes // string_to_assoc_list (sfold_colons [attribute_string]) // Parse the string, getting key/value pairs from a colon or semicolon-separated list.
                let actives = // unused.
                    match (op_assoc "activate" prams) with
                        | Some ss -> split_string_at [','] ss
                        | _       -> []

                let clkf = not_nonep(op_assoc "clk" prams) // powerstroke predicate.
                let lst = map (fun (a,b) -> (recode a, recode b)) lst
                app (fun (vale, var) -> ensure_unary_strobes false (var, vale)) lst  // TODO ensure needs to cope with logic equations and guard_equation_classify commutes. Better to do after DM form.

                let md = (ww, settings, nets, iname)
                let master_term =
                    let upconv arg = dememo_b md arg
                    let a = upconv(brecode gg)
                    let b = map (fun (a,b) -> upconv(ix_deqd a b)) lst  // A conjunction of deqd pairs.
                    gec_DM_andl(a :: b)
                // Hmm - also, with cmds, this implementation would seem a bit polarised between l/r of pair
                let guard_cmds_pairs = satisfy_and_split md master_term
                //let ldi = xi_string (sprintf "xdd") // This is going round in circles a bit in terms of coding style.  But we want the evergreen live-on-edge concept and dead by default concepts I think.  Maybe just assign dead on idle clocks?
               //vprintln 2 ( sprintf "Formed commands " + (sfold (fun xcmd ->hbevToStr_cleaned "" xcmd ) cmds))

                // The double arc should simply be replaced with a single arc that has both a guard and a command.
                
                let next = nextstate()             
                let p1 (gg99, cmds99) (gg, cmds1) =
                    m_commanded := lst_union !m_commanded cmds1
                    let cmds = map (fun (vale, var) -> Xassign(var, vale)) cmds1
                    (gg @ gg99, cmds @ cmds99)
                let (ggl, cmds) = List.fold p1 ([], []) guard_cmds_pairs
                log_arc(clkf, (current, pc), ix_andl ggl, cmds, [next])

                next

            | PS_seq(h::t) -> 
                 let current' = scans current (g00, h)
                 scans current' (X_true, PS_seq t)

            | PS_seq [] -> current

            | other -> sf(sprintf "protocol compiler: bad scan other form %A" other)


        let scantop startstate v =
            let exitv = scans startstate (X_true, v)
            mutaddonce m_commanded (exitv, pc)
            [ (exitv, startstate) ] // Form eternal loop: request rewrite of exit node as start node.
        let (startstate, redirects) = 

            if triv then (muddy "need fsm list form", []) else
                let startstate = nextstate()             
                let redirects = scantop startstate (clkvalof protocol)
                (startstate, redirects)

        // TODO do not allow a redirect that skips the idle state
        let trivial_arc_skip cc (uid, clkf, (src, pc), guard, cmds, dests) =
            if not clkf && length dests = 1 && nullp cmds && xi_istrue guard then (src, hd dests) :: cc
            else cc
        let redirects = redirects @ List.fold trivial_arc_skip [] !m_arclist // Need to transclose on this or otherwise itereate.
        reportx  2 "FSM Raw Redirects Of Trivial States"  (fun (l, r) -> sprintf " %i  %s -> %s" (x2nn l) (xToStr l) (xToStr r)) redirects                
        let removed_states = map fst redirects
        //reportx  2 "FSM Closure "  (fun (l, r) -> sprintf "  %s -> %s" (xToStr l) (xToStr r))  (transclose redirects)
        let redirects = List.filter (fun (a,b) -> not(memberp b removed_states)) (transclose redirects)
        reportx  2 "FSM Closed Redirects"  (fun (l, r) -> sprintf "  %s -> %s" (xToStr l) (xToStr r)) redirects                

        let recode (uid, clockprops, (src, pc), g1, cmds, dests) cc =
            let recode_dest dd =
                match op_assoc dd redirects with
                    | None    -> dd
                    | Some av -> av
            if memberp src removed_states then cc else 
            (uid, clockprops, (src, pc), g1, cmds, map recode_dest dests) :: cc
        
        (pc, lst_subtract (rev !m_pcvals) removed_states, startstate, !m_commanded, !m_nondets, List.foldBack recode (rev !m_arclist) [])

    let nets =
        if triv then
            vprintln 2 (sprintf "Discard PC from state vector since trivial")
            nets @ nondets
        else 
        // The participant PC is not in the final netlist, unless retained for debugging.
            let jnet =
                {
                    commandedf=   false // Added later
                    anonj=        false
                    evergreenf=   false
                    coding=       gen_Concrete_enum(pc_states)
                }
            let newnet = (pc, Ndi LOCAL, jnet) // Coding for the PC net of the participant - will not be an output variable itself.
            newnet::            nets @ nondets

    let assigned = list_once(map snd commanded)
    reportx 0 ("Participant Assigned Nets for " + iname) netToStr assigned

    let commanded_ranges = // Find the range of values assigned (commanded) on nets and then classify the commanded forms as coding.
        let compute_commanded_range v =
            let kk c (vale, var) = if var=v then singly_add vale c else c
            let range = List.fold kk [] commanded
            (v, range)
        map compute_commanded_range assigned

    let insert_commanded_range (id, dir, proto_jnet) = // For each assigned net, generate the range of values dictionary commanded (stored) in it. More importantly, set the commanded flag.
        let avo = op_assoc id commanded_ranges
        let in_flag = (dir = Always_input || dir = Ndi INPUT)
        let out_flag = (dir = Ndi OUTPUT)
        let upc (cmdd_flag, vale) =
            if in_flag then
                vprintln 3 (sprintf "No upc update for %s   a=%A" (netToStr id) (cmdd_flag))
                () // Inputs, despite being commanded by the protocol, are not commanded in the product sense - they make unilateral changes always except when going dead?
            else
                if memberp vale (valOf avo) then cmdd_flag := true // Set cmdd_flag for enumeration values that are explicitly set in the protocol spec.  
            (cmdd_flag, vale) 
        let coding =
            match proto_jnet.coding with
            | NS_unused -> proto_jnet.coding
            | Concrete_enum constvals when avo<>None -> Concrete_enum(map upc constvals)
            | Concrete_enum constvals when avo=None -> proto_jnet.coding
            | Symbolic_bitvec cd ->
                if nonep avo then
                    vprintln 0 (sprintf "+++ commanded ranges are noted for " + sfold (fst>>xToStr) commanded_ranges)
                    hpr_yikes (sprintf "No commanded_range for symbolic_bitvec %s  dir=%A.  Assuming it is unused for now ..." (netToStr id) dir)
                    NS_unused
                else
                    vprintln 0 ("Doing symb separation: cmd'd values are " + sfold xToStr (valOf avo))
                    let update_bitvec_flags_ (old, a) = 
                        if a=g_deadval then (ref in_flag, a) // For deadvals the bool ref represents an input - TODO document why. Not used now. Was a bool that was already set for an input.
                        else upc(old, a)
                    let update_bitvec_flags (old, a) = 
                        let vx = (a=DL_dead) <> in_flag // FOR NOW
                        (ref vx, a)
                    cd.life := map update_bitvec_flags !cd.life
                    proto_jnet.coding
        let commandedf = not in_flag || list_intersection_pred ([g_fullval; g_powerval; g_deadval ], valOf_or_nil avo) || memberp g_evergreen_state (valOf_or_nil avo)
        let jnet =
            { proto_jnet with
                commandedf=   commandedf
            }

        vprintln 3 (xToStr id + ": comuni separation complete. Coding=" + codingToStr coding)

        (id, dir, commandedf, jnet)

    let nets2 = map insert_commanded_range nets

    let _ =
        let pnetToStr (net, dir, commandedf__, jspec) =
            netToStr net + sprintf " commanded=%A dir=%A coding=%A"  jspec.commandedf dir (jspecToStr jspec)
        reportx 0 ("Participant Nets: final for " + iname) pnetToStr nets2
    let initialp = (if triv then [] else [(startstate, pc)]) @ initial_pred
    let idlep = List.fold gup_deqd X_true ((if triv then [] else [(startstate, pc)]) @ idle_pred)

    let participant =  // This is too high level in a sense.  We dont want shannon decomposition onto a named PC in the future - we should flatten to any number of constrainted-trajectory suttering/synch/asynch enumerations.
       { kind=         kind 
         states=       pc_states
         pi_name=      iname
         aliasnames=   aliasnames
         Pc=           Some pc
         pnets=        nets2
         Idle_pred=    idlep
         Initial_pred= initialp
         arcs=         arcs
         databus=      None // for now
       }
    participant_report ww settings iname kind participant
    participant   // end of precompile_participant


type drive_mode_t =
   | Allvalues  (* All values are driven *)
   | Onlyvalues of hexp_t list ref


type ybev_t =
    | Yassign of djc_t option * dcx_life_t * dcx_life_t // TODO the coding can go inside DL_lift
    | Ybuffer of dcx_life_t * dcx_life_t    
    | Yreturn of hexp_t
    | Ycomment of string
    | Yif of hbexp_t * ybev_t list * ybev_t list
    


let ybevToStr = function
    | Yassign(_, lhs, rhs) -> sprintf "%s <= %s" (lifeToStr lhs) (lifeToStr rhs)
    | Ybuffer(lhs, rhs)    -> sprintf "assign %s = %s" (lifeToStr lhs) (lifeToStr rhs)    
    | Yreturn rr           -> sprintf "Yreturn(%s)" (xToStr rr)
    | Ycomment ss          -> sprintf "Ycomment(%s)" ss

let gec_Yif gg a b =
    if nullp a && nullp b then []
    else
        match xbmonkey gg with
            | Some true  -> a
            | Some false -> b
            | _          -> [Yif(gg, a, b)]


    
(*
 * For concrete nets, if driven at all, we assume we drive all values on that net, but for symbolic
 * values we must carefully note whether we gen or kill them, or both:
 * a symbolic input is gen'd externally and killed by us and the other way around for an output, but.
 * rather than looking to see if it is an in or an out, we look how we drive it.
 *
 *)
let symbolic_rhs x =
    x=g_deadval || x=g_fullval || x=g_powerval ||
      match x with 
          | W_string _ -> true // Strings are symbolic - TODO watchout for strings with XS_withval ...
          | X_num _ -> false   // Manifest constants are not symbolic
          | X_bnum _ -> false
          
          | other -> // General arithmetic --- we need to look at support ? Please explain!
              vprintln 3 (sprintf "default concrete for other form in symbolic_rhs: key=%s other=%s" (xkey other) (xToStr other))
              false

let rec driven_info_update c vale var = 
    //vprintln 0 ("Driven update " + xToStr var + " with vale=" + xToStr vale)
    match c with
    | [] -> if symbolic_rhs vale then [(var, Onlyvalues(ref [vale]))]
            else [(var, Allvalues)]
    | ((id, Allvalues)::t) -> 
                  if id=var then (id, Allvalues)::t
                  else (id, Allvalues)::(driven_info_update t vale var)
    | ((id, Onlyvalues x)::t) ->
                  if id=var 
                  then (mutaddonce x vale; (id, Onlyvalues x)::t)
                  else (id, Onlyvalues x)::(driven_info_update t vale var)


type parer_t = Paper_not_used

type baseless_state_key_t = string   // index string without evergreen numbers
type full_state_key_t = string       // Full index string. 

type sv_t = svitem_t list list * (baseless_state_key_t * full_state_key_t) // The state vector is a list of segments and each segment has components: we cache a copy of the state keys with it 

type successor_t =
    int *                                              // Index number for debug tracking
    clockprops_t option *                              // Whether a clocked edge
    sv_t *                                             // Product point
    ((djc_t * ybev_t) list * hbexp_t list * int) *  // (Commands, guards, rank)
    (parer_t list * parer_t list * hbexp_t option)     // (Data conservation operations)


// The mainarray maps a product state key to its successors.
// The lookup must return data for items that are vary in basing.
type mainarray_t = ListStore<baseless_state_key_t, (sv_t * successor_t list)>

//
// Insert in mainarray.
// Mainarray is actually a tree of arrays whose depth is the number of segments in the state vector. Originally so. Still? Comment out-of-date?
let ma_insert (ma: mainarray_t) (sv, (bk, fk)) (successors:successor_t list)  =
    vprintln 0 (sprintf "doing ma_insert  bk=%s  of %s " bk fk)
    ma.add bk ((sv, (bk, fk)), successors)


type prodstate2_t = (hexp_t * (netdir_t * jspec_t)) list list    

let creidxToStr = function
    | CRE_guard(cre_idx, cre, g)      -> "^" +  cre_idx
    | CRE_alt(cre_idx, lst)           -> "^" +  cre_idx
    | CRE_catenate(cre_idx, ll, rr)   -> "^" +  cre_idx
    | CRE_star(cre_idx, cre)          -> "^" +  cre_idx
    | CRE_equation(cre_idx, eq)       -> "^" +  cre_idx + "%" + dcxToStr eq  // How does this indexing work?
    | CRE_end                         -> "CRE_end"

let rec creToStr = function
    | CRE_guard(cre_idx, cre, gg)     -> sprintf "CRE_guard(^%s, %s, %s)" cre_idx (creToStr cre) (xbToStr gg)
    | CRE_alt(cre_idx, lst)           -> sprintf "CRE_alt(^%s, [%s])"     cre_idx (sfold creToStr lst)
    | CRE_catenate(cre_idx, ll, rr)   -> sprintf "CRE_catenate(^%s, %s, %s)" cre_idx (creToStr ll) (creToStr rr)
    | CRE_star(cre_idx, cre)          -> sprintf "CRE_star(^%s, %s)" cre_idx (creToStr cre) 
    | CRE_equation(cre_idx, eq)       -> sprintf "CRE_eqn(^%s, %s)" cre_idx (dcxToStr eq)
    | CRE_end                         -> "CRE_end"

let crefullToStr = function
    | SV_cre(backtok, cre, donef) -> "X=" + backtok + ":" + creToStr cre + (if donef then "/DONE" else "")


// Generate human-readable preferred form state name.
// Also used for main array indexing in two forms.
let pcOnlyToStr (sv) =
    let pcs arg cc =
        match arg with
            | SV_4(_, id, DL_dead, dc) -> cc
            | SV_4(_, id, DL_lift vale, dc) ->
                let ss = xToStr id
                if ss.Contains "PC" then  sprintf "%A." (int32(xi_manifest64 "pcOnlyToStr" vale)) + cc
                else cc
            | SV_cre _ -> cc
            | SV_4(_, id, dl, dc) ->
                sf ("dl other" + lifeToStr dl)

    List.foldBack pcs sv ""

let mainlab1 baselessf svl =
    let groToStr = function
        | Some green_no when not baselessf -> sprintf "G%i" green_no
        | _ -> ""

    let smartspace = function // A little bit of style in presentation.
        | SV_cre _::_ -> ""
        | SV_4(_, _, DL_lift(X_num _), _)::_ -> ""
        | _                  -> " "
    match svl with
        | sv1::tt ->
            let rec scan = function
                | SV_cre(ctok, cre, donef)::tt -> "X=" + ctok + ":" + creidxToStr cre + (if donef then "/DONE" else "") + " " + scan tt
                | SV_4(gro, id, _, _)::tt when (xToStr id).Contains "PC" -> scan tt  //PCs listed earlier now
                // Do not print the word DEAD for an evergreen var that should, I think, always be dead anyway.
                | SV_4(gro, id, DL_dead, (dir, jspec))::tt when jspec.evergreenf -> groToStr gro + " " + scan tt // Only includes constant strings in this render.
                | SV_4(gro, id, DL_dead, dc)::tt  -> "DEAD" + groToStr gro + " " + scan tt // Only includes constant strings in this render.
                | SV_4(gro, id, DL_lift(W_string(ss, XS_withval(vv), dc_)), coding)::tt -> xToStr vv + groToStr gro + " " + scan tt
                | SV_4(gro, id, DL_lift(W_string(ss, _, dc_)), coding)::tt -> "Q'" + ss + groToStr gro + " " + scan tt
                | SV_4(gro, id, DL_lift(X_num v), coding)::tt -> i2s v + groToStr gro + smartspace tt + scan tt
                | SV_4(gro, id, h, dc)::tt  -> lifeToStr h + "?" + scan tt // Only includes constant strings in this render - not very nice. We want tidier enumeration/strobe group code.
                | [] -> ""
            let pfix = "||PC" + pcOnlyToStr (hd svl)
            let jj = scan sv1
            pfix + jj

   



// Generate a state_key name from a state vector position.
// The cre regexp should?? TODO not?? appear, but its back_address should appear.
let rez_svToStr_full svp =
    let ans = (* "||PC" + pcOnlyToStr (hd svp) + "||" + *) mainlab1 false svp + "||^" + i2s(length svp)  
    ans: full_state_key_t

let rez_svToStr_baseless svp =
    let ans = (* "||PC" + pcOnlyToStr (hd svp) + "||" + *) mainlab1 true svp + "||^" + i2s(length svp) 
    ans: baseless_state_key_t

let svToStr_full (segments, (bk, fk)) = fk:string // Already in string form, so just field extract.

    
//
// Rebased aliases also returned in a soft lookup.
//
let rec ma_lookup_by_both (ma : mainarray_t) (segs, (bk, fk)) =
    // Always lookup by bk, but for hard mode, then filter by fk.
    vprintln 3 (sprintf "ma_lookup bk=%s" bk)
    let bax_pred = function
        | ((sv', (bk', fk')), vale) -> fk = fk'
    let (cores, friends) = groom2 bax_pred (ma.lookup bk)
    let core =
        match cores with
            | [item] -> Some item
            | []     -> None
            | multiple -> sf ("multiple core entries")
    (core, map fst friends)
    //(not_nullp core, friends)            
    
let rec ma_lookup_by_fk (ma : mainarray_t) (segs, (bk, fk)) =

    // repeated code: Always lookup by bk, but for hard mode, then filter by fk.
    let rec bax = function
        | [] -> None
        | ((sv', (bk', fk')), vale)::tt when fk = fk'-> Some vale
        | _ :: tt -> bax tt
    bax (ma.lookup bk)


// A pair of lookups. I see only one! Move comment.
// This is just an alias for full-key lookup.
let ma_lookup_suc (ma:mainarray_t) arg =
    match ma_lookup_by_fk ma arg with
        | None       -> None
        | Some(vale) -> Some vale


type allowed_path_set_t = Dictionary<string, string list>


// Measuring hamming distance between two state vectors. Always +ve.
let sv_delta svl svl' =
    let rec diff =
        function 
        | ([], []) -> 0
        | (SV_4(_, a, b, _)::t, SV_4(_, _, b', coding)::tt) -> (if b<>b' then 1 else 0) + diff(t, tt)
        | (SV_cre(a, b, donef)::t, tt)
        | (t, SV_cre(a, b, donef)::tt) ->  diff(t, tt)        
        | _  -> sf("structure error in sv_delta")
    let rec dd c = function
        | ([], []) -> c
        | ((sv)::q, (sv')::r) -> dd (diff (sv, sv') + c) (q, r) 
        | _ -> sf "sv_delta: sv's have different structure"
    dd 0 (svl, svl')

    

//
//        
let sv_subtract svl svl' = 
    let rec subtract1 = function 
        | ([], []) -> []
        | (SV_4(gr, a, b, dc)::t, SV_4(gr', _, b', coding)::tt) ->
            let r = subtract1 (t, tt)
            if b<>b' || gr <> gr' then SV_4(gr, a, b, dc)::r else r
        | (SV_cre(a, b, donef)::t, tt) -> subtract1 (t, tt)
        | (t, SV_cre(a, b, donef)::tt) -> subtract1 (t, tt) // We can ignore cre entries in the one context that this is called from. 


    let rec dd  = function
        | ([], []) -> []
        | ((sv)::q, (sv')::r) -> subtract1 (sv, sv') @ dd (q, r)
        | _ -> sf "sv_subtract: sv's have different structure"
    dd (svl, svl')


// Find subscript offset vector of a companion with respect to svl
let compute_rebase_vector back_re_point  (svl, _) companion =
    let rec rebase_difference = function 
        | ([], []) -> []
        | (SV_4(None, var, vale, _)::t, SV_4(None, _, vale', coding)::tt) -> rebase_difference (t, tt)

        | (SV_4(Some gl, var, vale, _)::t, SV_4(Some gr, _, vale', coding)::tt) ->
            if vale<>vale' then sf (sprintf "rebase_vector: Not the same values for " + xToStr var)
            (gl-gr)::rebase_difference(t, tt)
        | (SV_cre(backtok, re_parse_point, donef)::t, tt) ->
            //if tok <> back_re_point then muddy (sprintf "temp stop on different rebase back point %s vs %s " tok back_re_point)
            [g_very_different] // Very different!
        | (t, SV_cre _::tt)   -> rebase_difference(t, tt) // This clause irrelevant?
        | _  -> sf "B L1107"

    let rec rebase_difference1  = function
        | ([], []) -> []
        | (sv::qt, sv'::rt) -> rebase_difference (sv, sv') @ rebase_difference1 (qt, rt) 
        | _ -> sf "sv_compute_rebase: sv's have different structure"

    match rebase_difference1 (svl, fst companion) with
        | [] -> sf ("Expected at least some difference in rebase vector")
        | aa::_ when aa = g_very_different -> (false, g_very_different, companion)
        | aa::lst ->
            let all_the_same = conjunctionate (fun x -> aa=x) lst
            dev_println(sprintf "rebase_delta a=%i others=%A allthesame=%A" aa lst all_the_same)
            (all_the_same, aa, companion)
    
(*
 * When considering the product we need to constrain the trajectory to conform with the participating protocols.
 * It is tempting to consider some transitions being free to occur at any time (assuming the variable is not already
 * set to that value) and others being driven by participant protocols...  but the distinction is somewhat arbitrary.  
 *
 * Variables that are not mentioned in any protocol help expand the product space are can freely change.
 *
 * Unichange_p predicate helps collate a list of external changes that can occur unilaterally: there are two sources of these:
 *  1. Fv external inputs from the participant logic
 *  2. When nets are driven by the converter.
 *
 * So we can just assume any transition made explicitly by a participant is not unilateral. no case split is needed.
 * Please explain better ...
 *   Concrete net : ?
 *   Symbolic nets : ? 
 *   For tlm active nets: ? 
 *)
let unichange_p(driven, id, (dir, jspec), ov, s) =
    let rec comf = function
        | [] -> false
        | (a, b)::t when b=s -> !a
        | _::t -> comf t
    let commanded_transition =
        match jspec.coding with
            | Concrete_enum l   -> comf l
            | Symbolic_bitvec v -> false // if evergreenf ? true // comf !v.life
            
    let ans = not commanded_transition
    //if ans then vprintln 0 ("  unichange_p " + netToStr id + " (from " + xToStr ov + ") to " + xToStr s + " ans->" +  boolToStr ans)
    ans


// cvrt_drives:
// When grooming to see for which changes our generated converter is responsible, a different distinction is needed.
// All locals are driven by us, for instance, whether associated with a protocol or not.

// isdriven(id, sv', (dir, jspec)) = dir=Ndi LOCAL || canmutate_p(driven, id, (dir, coding), sv')
let cvrt_drives_pred driven = function
    | SV_4(_, id, s, (dir, jspec)) ->
        let making_a_target = true  // Quick Kludge wont work for TLM initiators TODO DELETE THIS OR AT LEAST GET FROM SETTINGS <<-------------------------
        let ss = lifeToStr s 
        let ans =
            if ss= "DL_active" then (vprintln 0 " ++KLUDGE active"; not making_a_target) // TLM
            elif ss="idle"  then (vprintln 0 " ++KLUDGE active"; making_a_target)

            else
            match jspec.coding with
            | Concrete_enum _ ->
                match dir with
                    | Always_input -> false  // excludes TLM control above
                    | Ndi INPUT  -> false     // excludes TLM control above
                    | Ndi LOCAL  -> true
                    | Ndi OUTPUT -> true

            | Symbolic_bitvec _ ->
                let rec scan l = 
                    match l with
                    | [] -> false
                    | ((id', Allvalues)::t) -> if id'=id then true else scan t
                    | ((id', Onlyvalues x)::t) -> 
                              if id'=id then muddy "memberp s (!x)" // need to fix
                              else scan t
                (scan driven)

        //report("  cvrt_drives_pred " + netToStr id + " to " + xToStr s + " ans->" +  boolToStr ans)
        ans
    | _ -> muddy "L1202"
        
let local_logtidy ww settings ytag =
    jin_concise_report 1
    aux_report_log 1 ytag (rev !settings.m_report_file_contents)
    aux_report_log 1 "Jinjoin States Statistics." (rev !settings.m_states_report_lines)    


let local_cleanexit ww settings msg =
    let msg = "Exit message=" + msg
    mutadd settings.m_report_file_contents "Errored or early exit from compilation."
    mutadd settings.m_report_file_contents msg
    local_logtidy ww settings "Exit report on error."
    cleanexit msg
    

//--------------------------------------------------------------------

            
// Full print out of state vector.
let svToStr (_, (bk, fk)) = fk


    
// Report changes in a state vector from old to new
let rec svDeltaToStr (o1, _) (n1, _) = 
    let rec qq = function
        | ([], []) -> ""
        | (SV_4(_, id, s, coding)::t, SV_4(_, id', s', coding')::qs) ->
            let _ = assert(id' = id)
            let tailer = qq (t, qs)
            if s <> s' then "(" + lifeToStr s + "->" + lifeToStr s' + "/"+ xToStr id + ")" + tailer else "" + tailer
        | _ -> muddy "bugger"

    let rec rr = function
        | ([], []) -> ""
        | ((h, _)::hs, (b, _)::bs) -> qq (h, b) + rr(hs, bs)
    rr (o1, n1)

// Convert a state vector segment to its key string (was an enumeration int but now is a string, hence odd name)
let intval nets state_vector =
        let mut = true // Additional symbolic expressions that crop up during elaboration are commanded (we dont want the entering the unilateral space).

        (* Find numerical index for a state of a net - strobe group number normally/ideally.*)
#if TEMP_REMOVED
        let rec nassoco net state codings =
            if state = g_bs then Some 0// nondets start this way
            else
            let net = net_retrieve nets net
            match codings with
                | NS_unused          -> None
                | Concrete_enum  lst -> nassoc net 0 state lst
                | Symbolic_bitvec v  -> Some(ensure_symb_state mut net v state)
#endif
        let nassoca net state codings = // Get numeric codepoint.
            match state with
                | W_string(s, XS_withval(X_num vv), _) -> vv
                | X_num vv -> vv
                | other when state = g_evergreen_state -> 1 // Nominal value
                | other ->
                    dev_println(sprintf "nassoca: %s %s other" (xkey other) (xToStr other))
                    //valOf_or_failf (fun ()->xToStr state + " state not found for " + xToStr net) (nassoco net state codings)
                    muddy "State mo"

        let rec inta sv = 
            match sv with
            | [] -> ""
            | SV_4(_, id, DL_lift(state), (dir, coding))::tt ->
                i2s(nassoca id state coding)+ (if tt=[] then "" else "." + inta tt) // Dot-separated list of elements
                // int form: (nassoca id state coding) + max_coding_states coding * inta tt

            | SV_4(_, id, dlx, (dir, coding))::tt -> lifeToStr dlx
            | _ -> muddy "bug4"
        (inta state_vector) + ""



// datapath match: See if we can transfer any data from from to to.
// If so, we return a quad
//   1. the remainder (or dead) of the from field (as a pair),
//   2. the guard condition,
//   3. the data movement commands so produced for the output machine,
//   4. deltas generated (dc_edits to apply to state vector). 
//
let dp_canmatch eqns (id_from0, ealpha, dc_from) (id_to0, eomega, dc_to) =
    let vd = 4

    // Delete this and rationalise silly_type_drop please.
    let id_from = gec_DL_lift id_from0  // Stay in scope despite recursion inside do_match?  Double check please.
    let id_to = gec_DL_lift id_to0 // for now - waste of time.
    
    let rep ss = if vd>=4 then vprintln 4 ss
    if ealpha = DL_dead then
        hpr_yikes(sprintf "dp_canmatch step ealpha=%s" (lifeToStr ealpha))
        None // If the value being evicted is dead, this cannot be moved. Its not a real eviction. fault it.
    else
    rep "Start dp_canmatch"
    let rec do_match m (nalpha, ealpha, nomega, eomega) =
        let _:dcx_life_t = nalpha // Names and expressions need not be the same types.
        let _:dcx_life_t = nomega        
        let _:dcx_life_t = ealpha
        let _:dcx_life_t = eomega        
        rep("   goal " + m + "\n    evict from=" + lifeToStr(*_plus_key*) ealpha + "/" + lifeToStr nalpha + ",\n   admit to=" + lifeToStr(*_plus_key*) eomega + "/" + lifeToStr nomega + "\n   destination dc=" + jspecToStr(snd dc_to))
        if ealpha = DL_dead then
            rep "dead ealpha. Cannot transfer from a dead source."
            None // Cannot transfer from a dead source.
        else
        match (nalpha, ealpha, nomega, eomega) with

#if COMMENT_OUT_FOR_NOW
        | (nalpha, E_LIFT(W_node(prec_, V_bitor, [ ll; W_node(prec__, V_lshift, [W_apply(("kill", f), _,  [_], _); X_num n], _)], _)), nomega, eomega) ->
            // ignore kill arg ? may be a pred TODO
            let kw = "kill apply"
            rep kw
            let e = (nalpha, ealpha, g_deadval,  dc_from)
            let a = (nomega, X_undef, eomega, dc_to)
            let alpha'' = (id_from, g_deadval, dc_from)
            let cmd = (dc_to, Yassign(Some dc_to, nomega, nalpha))
            Some(kw, alpha'', X_true, [cmd], [ a;e ])

        
        | (nalpha, ealpha, nomega, E_LIFT(W_apply(("kill", f), _, [ll], _))) -> // kill rhs use: best flip to left
            vprintln 0 "kill rhs flip to left"
            let nf = do_match "kill flip" (nalpha, xi_apply(("kill", f), [ealpha]), nomega, ll)
            nf

        | (nalpha, E_LIFT(W_node(prec_, V_lshift, [ll; X_num sv], _)),  nomega, eomega) -> // Shift out
            let kw = "shift out"
            let sg = do_match kw (nalpha,  ll, nomega, eomega)
            if nonep sg then None
            else
                let (kw_, alpha', grd, cmds, actions) = valOf sg
                let grdd = if f2o3 alpha'<>g_deadval then X_false else grd
                let trim (id, ov, nv, coding) = id <> nalpha
                let newevict = (nalpha, ealpha, g_deadval, dc_from)
                let actions'' = newevict :: (List.filter trim actions)
                let alpha'' = (id_from, g_deadval, dc_from)
                let ans = if f2o3 alpha'<>g_deadval then None else Some(kw, alpha'', grdd, cmds, actions'')
                rep ("   subgoal (shift out) ans=" + optToStr ans)
                ans 

        | (nalpha, ealpha, nomega, E_LIFT(W_node(prec_, V_lshift, [ll; X_num sv]), _)) -> // Shift in
            let kw = " shift in"
            let sg = do_match kw (nalpha,  ealpha, nomega, ll)
            if nonep sg then None
            else
                let (kw_, alpha', grd, cmds, actions) = valOf sg 
                let grdd = if f2o3 alpha'<>g_deadval then X_false else grd
                let trim (id, ov, nv, coding) = id <> nalpha
                let newevict = (nalpha, ealpha, g_deadval, dc_from)
                let actions'' = newevict :: (List.filter trim actions)
                let alpha'' = (id_from, g_deadval, dc_from)
                let cmds' = cmds // HERE NEED TO SHIFT TODO
                let ans = if f2o3 alpha'<>g_deadval then None else Some(kw, alpha'', grdd, cmds', actions'')
                rep ("   subgoal (shift in - untested) ans=" + optToStr ans) // UNTESTED SO FAR
                ans 

        | (nalpha, W_node(prec_, V_bitor, [ll; rr], _),  nomega, eomega) -> // serialise
            let kw = "serialise"
            let sg = do_match kw (nalpha,  ll, nomega, eomega)
            if nonep sg then None
            else
                let (kw_, alpha', grd, cmds, actions) = valOf sg
                let grdd = if f2o3 alpha'<>g_deadval then X_false else grd
                let trim (id, ov, nv, coding) = id <> nalpha
                let newevict = (nalpha, ealpha, rr, dc_from)
                let actions'' = newevict :: (List.filter trim actions)
                let alpha'' = (id_from, rr, dc_from)
                let ans = if f2o3 alpha'<>g_deadval then None else Some(kw, alpha'', grdd, cmds, actions'')
                rep ("   subgoal (serialise) ans=" + optToStr ans)
                ans

        | (nalpha, W_apply(("kill", _), _, [ll], _), nomega, eomega) -> // kill lhs use
            let kw = "subgoal kill succeded"
            let sg = do_match "kill" (nalpha,  ll, nomega, eomega)
            if nonep sg then None
            else
                let (kw_, alpha_, grd, cmds, actions) = valOf sg
                rep kw
                let newevict = (nalpha, ealpha, g_deadval, dc_from)
                let alpha_dead = (id_from, g_deadval, dc_from)
                Some(kw, alpha_dead, grd, cmds, newevict :: actions)


        | (nalpha, ealpha, nomega, W_node(prec_, V_bitor, [ll; rr], _)) -> // deserialise - ordering is hardcoded
            // Alpha is placed in right-hand-side of receiving disjunction, leaving left-hand side still to fill.
            let kw = " deserialise"
            let sg = do_match kw (nalpha, ealpha, nomega, rr) // sub goal
            rep ("  subgoal deserialise " + optToStr sg)
            if nonep sg then None
            else
                let (kw_, omega', grd, cmds, actions) = valOf sg
                let grdd =
                    if f2o3 omega'<>g_deadval then
                        vprintln 3 (sprintf "%s xfer would overwrite non-dead destination" kw)
                        X_false
                    else grd     // Here we check for overwriting and return false if this would loose data
                let trim (id, ov, nv, coding) = id <> nalpha
                let newadmit = (nomega, eomega, ll, dc_to)
                let actions'' = newadmit :: (List.filter trim actions)
                let alpha'' = (id_from, rr, dc_from)
                let ans = if f2o3 omega'<>g_deadval then None else Some(kw, alpha'', grdd, cmds, actions'')
                rep ("   subgoal (deserialise +++) ans=" + optToStr ans) // UNTESTED
                ans

        | (X_bnet f, X_bnet ef, X_bnet t, X_tuple(_, [body; bv], _)) -> // Predicate
            let sg = do_match "predicate" (nalpha,  ealpha, nomega, g_deadval)
            let kw = ("  subgoal predicated " + optToStr sg)
            rep kw
            if nonep sg then None
            else
                let (kw_, alpha', grd, cmds, actions) = valOf sg
                let mMM = makemap [ (bv, nalpha) ]
                let sp = xi_rewrite_exp mMM body
                let kw = ("  subgoal predicated : hydrated pred is " + xToStr sp)
                rep kw
                let ans = Some(kw, alpha', xi_and(xi_orred sp, grd), cmds, actions)
                ans
#endif
        // Normal, straightforward data xfer - widths (and branding in future) need to match.
//| (DL_lift(X_bnet _), DL_draining(_, (E_LIFT(X_bnet _), _)),   DL_lift(X_bnet t), eomega)  
        | (DL_lift(X_bnet _), DL_draining(_, (E_LIFT(X_bnet _), _)),   DL_lift(X_bnet dest_ff), eomega)  
        | (DL_lift(X_bnet _), DL_lift(X_bnet _),                       DL_lift(X_bnet dest_ff), eomega)      // String or net can represent alpha
        | (DL_lift(X_bnet _), DL_lift(W_string _),                     DL_lift(X_bnet dest_ff), eomega) -> // 
            let kw = "straightforward, width-matching xfer"
            let lzw = function
                | DL_lift(X_bnet ff)  -> Some(xi_width_or_fail kw (X_bnet ff)) // TODO put prec inside DL_lift
                | DL_lift(W_string _) -> None
                | DL_draining(prec, (E_LIFT(X_bnet ff), sup_status)) ->
                    dev_println ("// Need to check single field only" ) // Need to check single field only
                    Some(valOf_or_fail kw prec.widtho) // option->option ?
                | other -> sf (sprintf "L1450 other=%A" other)
            let fw = lzw ealpha
            let tw = lzw nomega // perhaps e here too!

            let ans = 
                if not_nonep fw && not_nonep tw && valOf fw <> valOf tw then
                    rep (sprintf "%s width mismatch." kw) // floating point etc?
                    None // Does not fit.
                elif false && eomega <> DL_dead then // >H X4
                    rep (sprintf "%s is  overwriting non-dead destination. %s. yikes" kw (lifeToStr eomega))
                    None
                else
                   rep (" Encoding width match " + optToStr fw + " w=" + oiToStr tw + " a=" + lifeToStr ealpha + " w=" + lifeToStr eomega)
                   let alpha' = (id_from0, DL_dead, dc_from) // alpha is rhs, where data is evicted. This goes dead now.
                   let omega' = (id_to0, ealpha, dc_to)      // omega is lhs, the receiving site, which goes live with what was stored.
                   let evict_edit = (nalpha, ealpha, DL_dead, dc_from)
                   let admit_edit = (DL_lift(X_bnet dest_ff), DL_lift(X_undef), eomega, dc_to)  // Strange use of X_undef?
                   Some (kw, alpha', X_true, [(dc_to, Yassign(Some dc_to, nomega, nalpha))], [ admit_edit; evict_edit ])
            rep (sprintf "   subgoal %s: from alpha=" kw + lifeToStr(*_plus_key*) ealpha + " fw=" + oiToStr fw + " tw=" + oiToStr tw + " ans=" + optToStr ans)
            ans

        | (ff, alpha, tt, eomega) ->
            // Not a fault if eomega is live
            sf ("do_match: other: " + " alpha=" + lifeToStr alpha + "/" + lifeToStr ff + " --> to=" + lifeToStr eomega + "/" + lifeToStr tt)
            // + sprintf " vixvix %A" alpha) 


    let ans = do_match "topstart" (id_from, ealpha, id_to, eomega)

    // The ae returned is for editing the state vector as a consequence.
    // The commands returned implement the data movement.
    let _ =
        if vd>=4 then 
            let bax = function
                | None -> "None --- not conserved."
                | Some(kw, alpha, grd, cmds, ae) -> kw + ": " + xbToStr grd + "/" + sfold (snd>>ybevToStr) cmds + " ae=^" + i2s(length ae)
            vprintln 4 ("Can match? result is move " + lifeToStr id_from + " to " + lifeToStr id_to + ", Ans=" + bax ans)
    ans    // end of dp_canmatch




(*
 * The basic data conservation rule is that evictions and admissions must be matched.  Fan-out of data by simple wiring is not allowed at this level: an explicit data-replicate operator is needed that maps into simple wiring in later processing.

 For I/O busses, all of the data is transferred, but for holding registers, symbolic 
 However, for serdes operations, not all of the data is moved and a register contains an expression with both live and dead fields ...
  

 * Another phrasing, "Two machines can only jointly move if their evictions and admissions can be dp-unified."
 * The evicting variables have new custom live expressions, whereas the admiting variables go live with their
 * default structures.

 * TODO discuss 3 or more machines ...
 *)






type dcx_t =
    | DCX_as of dcx_t * ((string * dcx_t) * int) * dcx_t  // A triple: (resource/field in instance to be manipulated on update, field to use for data op, field or symbolic expression involved for reading).  Third field is always a DCX_setget?
    | DCX_data
    | DCX_setget of dcx_t * dcx_arg_t // Pair: the resource/field and its symbolic data.  ... temporary form. Now using E_EQ_LD instead.



// Add a dead/live flag for each field in the support of an expression.
let markdeadlive nets starting_value exp = 
    let saves = new ListStore<hexp_t, int>("saves")//collate occurences of free variables to various powers.

    let log nn arg =
        match arg with
            | X_bnet ff ->
                let ov = saves.lookup arg
                if memberp nn ov then sf (sprintf "Two or more mentions of (%s)^%i" (xToStr arg) nn)
                saves.add arg nn
            | _ -> sf "L3040"

    let m_patches = ref []
    let findnet (X_net(ss, m)) = // Rehydrate data conservation equation. // >H
        let rec search = function
            | [] -> sf(sprintf "Cannot find net %s to hydrate. Candidates are %s" ss (sfold (f1o4>>xToStr) nets))
            | h::tt when xToStr(f1o4 h).IndexOf ss >= 0 ->
                mutadd m_patches (ss, (X_net(ss, m), f1o4 h))
                f1o4 h // TODO somewhat loose match!
            | _ ::tt -> search tt

        match op_assoc ss !m_patches with
            | Some(old_vale, new_vale) -> new_vale
            | None -> search nets


            
    let walksup = 
        let vd = 3
        let opfun arg nx bo xo orig child_node_data_lst =
            match xo with
                | Some v ->
                    //dev_println(sprintf "opfun %s %s" (xkey v) (xToStr v))
                    match v with
                        | (Let 0 (nn, X_net(ss, m)))
                        | X_x(X_net(ss, m), nn, _)  -> log nn (findnet (X_net(ss, m)))
                        | (Let 0 (nn, X_bnet ff))
                        | X_x(X_bnet ff, nn, _)  -> log nn (X_bnet ff)
                        | X_x(other, _, _) -> sf (sprintf "unsupported X_x form in conservation equation %s  exp=" (xToStr v) + xToStr exp)
                        | _ -> ()

                | _ -> ()
            orig
        let lfun(pp, comp_gs) clkinfo arg rhs =
            ()

        let null_unitfn arg =
            //vprintln 0 ("Unitfn " + xToStr arg)
            ()

        let null_b_sonchange _ _ nn (aa, bb) =
            //vprintln 0 ("Null_b_sonchange " + xbToStr aa)
            bb

        let x_sonchange _ _ nn (aa, bb) = bb

        let (_, walker_ss) = new_walker vd false (true, opfun, (fun _ -> ()), lfun, null_unitfn, null_b_sonchange, x_sonchange)
        let tallies_o = None
        let wlk_plifun _ = ()
        let msg = "markdeadlive"
        let aux = (msg, walker_ss, tallies_o, wlk_plifun)
        let walksup xarg  = (mya_walk_it aux g_null_directorate xarg)     // We pass in null directorate
        walksup
#if NOGOOD
    let sup = xi_support [] exp // Sadly this discards X_x forms
    let sup = map fst sup // This returns in bit-field form.  We could usefully use that in future!
#endif
    let _ = walksup exp // Side-effecting.

    let sup =
        let m_ss = ref []
        let save2 key values =
            let values = List.sort values
            vprintln 3 (sprintf  "Support powers for %s are %s" (xToStr key) (sfold i2s values))
            mutadd m_ss (key, map (fun n -> (n, starting_value)) values)
        for v in saves do save2 v.Key v.Value done
        !m_ss

    let patch:meo_rw_t = makemap (map snd !m_patches)
    let exp = xi_rewrite_exp patch exp
    let ans:dcx_arg_t = (sup, exp)
    ans



// Apply an evict from somewhere else to an equation: filling in the next empty field in the equation..
let eqn_apply_evict vd (vid, alpha, omega, coding) dl = 

    //Value being evicted is the alpha, with omega elsewhere being stored in the source, but for a powerstroke input it's the omega.
    let data = if omega = g_DL_powerstroke then omega else alpha
    let is_dsimp = function
        | X_bnet _
        | X_net _
        | W_string _ -> true
        | _ -> false
    match dl with 
    | DL_filling(prec, (E_LIFT dsimp, near_support), (far_exp, far_support)) when is_dsimp dsimp ->
        
        //From a simple scalar, it becomes fully-filled straightaway. Dont bother consulting near_support which is only needed when multiple steps are needed to fill the near side. This is a get-started approach and also an optimisation on the full approach.
        dev_println(sprintf "eqn_apply_evict: from simple scalar: alpha=%A  near=%A" data (xToStr dsimp))
        match far_exp with
            | other ->
                // TODO somehow save the association
                //let new_evt = muddy(sprintf "far_exp %A" other)
                let p = DL_draining(prec, (far_exp, far_support))
                Some(vid, p, p, coding) // Only one field needed, but vague which at the moment.

    | _ ->
        dev_println(sprintf "eqn_apply_evict: alpha=%A  near=%A" data (lifeToStr(*_full*) dl))        
        muddy "L1563"


// Match up evictions with admissions under the current equation.  There could be more than one matching.
let data_conserve nets cre evictions admissions = 
    let vd = 4
    let _:(hexp_t * dcx_life_t * dcx_life_t * djc_t) list = evictions
    let _:(hexp_t * dcx_life_t * dcx_life_t * djc_t) list = admissions
    if nullp evictions && nullp admissions then (X_true, [], [], cre, [])
    elif (nullp evictions) <> (nullp admissions) then // The number needs to be non-zero on each side but the numbers do not need to match owing to packing.
        vprintln 3 (sprintf "Cannot possibly conserve data: disparity between evictions=%i and admissions=%i" (length evictions) (length admissions))
        (X_false, [], [], cre, [])
    else
    vprintln 3 (sprintf "Can possibly conserve data: using %i evictions and %i admissions." (length evictions) (length admissions))


    let ere2s (id, ov, nv_, (dir, coding)) = "ere:" + ndiToStr dir + " :  " + xToStr id + " ov=" + lifeToStr ov + " nv=" + lifeToStr nv_ // nv_ relevant for input busses at the moment. TODO.
    let era2s (id, ov, nv, (dir, coding)) = "era:" + ndiToStr dir + " :  " + xToStr id  + " ov=" + lifeToStr ov + " nv=" + lifeToStr nv

    // First scheme: for each eviction, map it through the eqns or else fail.
    // 2nd scheme: generalise, all must be tagged to show passed through an equation and equations likewise tagged to show they have been used

    // Preprocess evictions if equations are undispatched.
    // TODO the option that does not activate the equation should also be left on the table?
    
    let term_hydrate (exp, sup) = (exp, sup) // netdir not needed since hydrated earlier
        
    let cre_activate cre evt =
        //if nullp eqns then muddy "no equations for evict_preprocess"
        //else
        match cre with
            | SV_cre(backtok, CRE_equation(nodeidx, E_EQ_LD(prec, lx, rx)), _) ->  // We will move to a hydrated/flagged form on first match for now, but ideal
                dev_println(sprintf "raw_eqn_activate: Need to evict_preproc evt=%s against eqn %s" (ere2s evt) (crefullToStr cre))
                let check_raw_ere (id, ov, nv_, (dir, coding)) rflag sofar (near_exp, near_support) (far_exp, far_support) =
                    let ssl = map (fst>>xToStr) (fst near_support) // yuck ... should be able to use hexp match please, requires hydrate earlier.
                    if memberp (xToStr id) ssl then
                        dev_println (sprintf "raw_eqn_activate: Found eqn match id=%s in %s" (xToStr id) (xToStr (snd near_support)))
                        if nonep sofar then
                            let vnew = // Need to log the command too. TODO  Need also to bump a tag. Bump gets done later. Say where. Log also - this hydrate is a lightweight thing that could be precomputed/cached. No other side effects actually.
                                DL_filling(prec, term_hydrate(near_exp, near_support), term_hydrate(far_exp, far_support))
                            let new_cre = SV_cre(backtok, CRE_end, false)
                            //let new_cre = SV_cre(backtok, CRE_equation(nodeidx + "-U", E_EQ_used), false)                                
                            let powerstrokes = if nv_ = g_DL_powerstroke then [id] else []
                            Some(new_cre, powerstrokes, vnew)
                        else muddy "Two cre activate hits"
                    else sofar
                    //muddy (sprintf "Want to check %s against %A" (xToStr id) ld_tags)
                // We have found an equation that can be hydrated.  Two cases: one side fully or one side partially.
                let x1 = check_raw_ere evt false None lx rx
                let x1 = check_raw_ere evt true  x1   rx lx
                match x1 with
                    | None ->  (cre, [], evt)
                    | Some(new_cre, powerstrokes, vnew) ->
                        dev_println("Weds: potential new CRE = " + crefullToStr new_cre)
                        match eqn_apply_evict vd evt vnew with
                            | Some new_evt ->
                                dev_println("Weds: adopt potential new CRE = " + crefullToStr new_cre)
                                // Postcursor work needs to resolve into a back edge. Explain how.  When does cre index get advance?
                                (new_cre, powerstrokes, new_evt)
                            | None -> (cre, [], evt)
            | other ->
                (other, [], evt)
                //sf(sprintf "Need to evict_preproc evt=%s against eqn %s" (ere evt) (crefullToStr cre))


    // Various routes here - match without using any equations and match after activating one or more equations.
    // Without using equations we need anon resources or resources with matching names.  Names only get translated via equations.
    // We want to explore these routes, but to get the code written, typechecked and working, activate as many as possible.
                
    // Zeroth step, activate relevant equations
    // There can be more than one way of obeying a set of equations .... 
    let rec foldy ff cre = function     // TODO need to consider all permutations and also include a nop. This is really a non-det space expand.
        | [] -> (cre, [], [])
        | h::tt ->
            let (cre, powerstrokes1, tt) = foldy ff cre tt
            let (cre, powerstrokes2, h) = ff cre h
            (cre, powerstrokes1@powerstrokes2, h::tt)
    let (cre, powerstrokes, evictions) =
        if true then foldy cre_activate cre evictions
        else (cre, [], evictions) // Alt route typechecks.

    let _:(hexp_t * dcx_life_t * dcx_life_t * djc_t) list = evictions

#if SPARE                    
    let liveqs = 
        match cre with
            | SV_cre(backtok, CRE_equation(nodeidx, eq), _) ->
                let gix cc arg =
                    match arg with // no guarantee we get the filling we just created above in activate - might match another activated one ... again, app perms.
                        //| E_EQ_draining(prec, (far_exp, far_support)) -> arg::cc
                        //| E_EQ_filling(prec, (near_exp, near_support), (far_exp, far_support))  -> arg::cc
                        | _ -> cc
                     
                List.fold gix [] [eq]// For now at most one, but more in future code re-org.
            | other ->
                sf(sprintf "L1623 %s" (crefullToStr cre))

    let preprocess_evictions liveqs = function
        | _ -> sf "ldd in danger of doing it all again"

        | evt ->
            sf(sprintf "Need to evict_preproc evt=%s against eqn %s" (ere evt) (crefullToStr cre))
    let evictions = // spare step, mary evictions with pending raw equations.
        map (preprocess_evictions liveqs) evictions
#endif
   
    // Create a mutable hungry work list for omega, one entry per admission.
    // How do we check named/anon?
    let m_hw_list = map (fun (id, w_in, w_out, dc)-> ref(id, (w_out:dcx_life_t), dc)) admissions // Discard the change-from field for an admission. ... Well if we want to check dest was empty we need the change-to field really. But we also want a mutable marker to show it was done and we want ... supress check to is dead at X3  Won't be listed as an admission unless it was vaguely sensible?

    let rec alldead_pred = function // temporary ... wont work for deserialisers since we take only some of the data on an operation ... 
        | (id, v, dc) -> v=DL_dead

    // For an eviction ('from structure') we see if any hungry admission on the hw_list can suck (some of) it up.
    let chisel (alpha, g0, c0, a0) = 
        let _: hexp_t * dcx_life_t * (netdir_t * jspec_t) = alpha// new preferred?

        let rec chi (alpha, grd, cmds, actions) = function
            | [] -> (alpha, grd, cmds, actions)
            | m_hw::wt ->
                let (idname, w0, dir_and_coding) = !m_hw
                let _:hexp_t * dcx_life_t * (netdir_t * jspec_t) = alpha
                let _:dcx_life_t = w0                
                let adp = dp_canmatch cre alpha (idname, w0, dir_and_coding) // Args are alpha/from/eviction and omega/to/admission. 
                //let adp = muddy "what form result?"
                if nonep adp then chi (alpha, grd, cmds, actions) wt  (* TODO: Ideally need a maximal matching... rather than fcfs greedy. Do over permutations. *)
                else
                    let (kw_, alpha1, grd', cmds', actions') = valOf adp
                    dev_println (sprintf "dp substep reaped %s   %i dc_edits/actions" kw_ (length actions'))
                    let w1 = DL_dead (* for now *) // Won't work for serialisers. TODO.  Replace with bumper dead/deader value so cant be sucked on further.
                    m_hw := (idname, w1, dir_and_coding)
                    chi (alpha1, ix_and grd' grd, cmds'@cmds, actions'@actions) wt // alpha is refined, becoming all dead we trust.

        let (alpha, grd, cmds, aa) = chi (alpha, X_true, [], []) m_hw_list // Apply inner loop to hungry admissions list (omega).
        (alpha, ix_and grd g0, cmds @ c0, aa @ a0)
        
    // We iterate over all evictions (alpha), hoping to fill in all hungry admissions on the omega list via all possible equations.
    // This is a mini space explore that can be folded into the main chart really...
    let rec itfun (a_out, g0, c0, dc_edits0) = function
        | []           -> (rev a_out, g0, c0, dc_edits0) // dc_edits are the commands to be applied to the sv.
        | eviction::tt -> 
            let (eviction, grd, cmds, dc_edits) = chisel (eviction, X_true, [], [])
            itfun (eviction :: a_out, ix_and grd g0, cmds @ c0, dc_edits @ dc_edits0) tt

    let _:(hexp_t * dcx_life_t * dcx_life_t * djc_t) list = evictions
    let alpha =
        let daz (id, a_in, a_out, (dir, jspec)) =
            let nv =
                //if a_in = DL_active || a_out = DL_active then DL_active
                if a_in = DL_dead then /// If daz disabled for stdsynch behaviour - need to re-instate for asynch busses?
                    // Can check bus dir here ...
                    a_out // For bus inputs, the value coming in is in the a_out field actually.
                else a_in
            (id, nv, (dir, jspec))
        map daz evictions // Discard the change-to field of an eviction.  Should test over all permutations. TODO discuss.

    let (a_out, guard, cmds, dc_edits) = itfun ([], X_true, [], []) alpha
    let alldead1 = conjunctionate alldead_pred (map (!) m_hw_list) // alldead is a check that 'everything' was handled - all of the evictions have been handled and are now dead or at least deader?
    dev_println (sprintf "all dead=%A - tidy this tested please?" alldead1)
    let ans =
        if true || alldead1 then (guard, cmds, dc_edits, cre, powerstrokes)
        else (X_false, [], [], cre, []) // This would want to be the prior cre
    let okf = not (xi_isfalse(f1o5 ans)) // we are returning at most one marry, but there's many marries via various equations or even with just one equation ...
    let _ = 
        if okf then
            reportx vd "Ya Evictions" ere2s evictions // unmodified from input
            reportx vd "Ya Admissions" era2s admissions
            vprintln vd ("Ya_CRE = " + crefullToStr cre)
            //reportx vd "Actions/dc_edits" ere2s dc_edits -- type error on lifted id
            vprintln vd ("Data conservation: cmds=" + sfold (ybevToStr) (map snd cmds) + " guard=" + concise_xbToStr guard + sprintf " and alldead=%A" alldead1)
        else vprintln vd ("Data conserve failed.")
    ans // end of data_conserve



let bmonkey x =
    match xbmonkey x with
        | Some true -> true
        | Some false -> false
        | None -> sf ("bmonkey is ambivalent on " + xbToStr x) 


let monkey e x =
    match xi_monkey x with
        | Some true -> true
        | Some false -> false
        | None -> sf ("monkey is ambivalent on " + xToStr x + " in " + xbToStr e) 

let rec beval nets (segs:svitem_t list list) e =
    let ans = beval_ntr nets segs e
    //dev_println (sprintf "Beval %s gives %A" (xbToStr e) (xbToStr ans))
    ans


and beval_ntr nets  (segs:svitem_t list list) e = 
    let beval_vd = 120

    match e with
        | X_true  -> e
        | X_false -> e
        | W_cover(cubes, _) ->
            ix_orl(map (fun cube->ix_andl(map (fun nn -> beval nets segs (deblit nn)) cube)) cubes)
                           
        | W_bdiop(V_orred, [vale], inv, _) ->
            if monkey e (eval (nets) segs vale) <> inv then X_true else X_false

        | W_bmux(vv, ff, tt, _) ->
            let k = beval (nets) segs vv
            beval (nets) segs (if bmonkey k then tt else ff)

        | W_bnode(V_band, lst, inv, _) ->
            let k cc item = if cc=X_false then cc else ix_and cc (beval (nets) segs item)
            let r = List.fold k X_true lst
            if inv then xi_not r else r

        // TODO other operators such as bor may be used?
                
        | W_bdiop(V_deqd, [e1; e2], inv, _) ->
            let l = eval (nets) segs e1
            let r = eval (nets) segs e2
            let ans = if (bmonkey(ix_deqd l r)) <> inv then X_true else X_false // TODO <--- this can remain symbolic - we dont have to monkey or false here
            if beval_vd <= 10 then vprintln beval_vd ("beval: Compare " + xToStr l + " with " + xToStr r + " giving " + xbToStr ans)
            ans
        | other -> sf (xbkey other + ": Other form in beval: " + xbToStr other)
            
and eval nets (segs:svitem_t list list) e =
    match e with
        | W_string _
        | X_num _ 
        | X_bnum _ -> e                        
        | X_net(id, _) -> eval nets segs (net_retrieve nets e)
        | X_bnet ff ->                
            let rec lookup1 = function // Lookup a field inside the state vector.
                | [] -> None
                | SV_4(_, id, DL_lift state, coding)::tt when id=e ->
                    //dev_println(sprintf "eval of %s -> DL_lift %s" (xToStr id) (xToStr state))
                    Some state
                | SV_4(_, id, DL_dead, coding)::tt when id=e ->
                    hpr_yikes(sprintf "Why do we eval dead on %s" (xToStr id))
                    Some g_deadval
                | SV_4(_, id, state, coding)::tt when id=e -> sf (sprintf "not hexp pod " + lifeToStr state)
                | _::tt -> lookup1 tt

            let rec lookup0 = function
                | [] ->
                    dev_println ("variable " + xToStr e + " is not in the state vector!")
                    xi_string (funique "tempxdd") // For now
                | h::tt ->
                    let ov = lookup1 h
                    if ov<>None then valOf ov
                    else lookup0 tt
            lookup0 segs
        | W_node(_, _, _, _) -> e (* assume all other expressions can be returned symbolicly for now *)
        | X_blift bx -> xi_blift(beval nets segs bx)
        | other -> sf (xkey other + ": Other in eval: " + xToStr other)


// Sort for nicest first
let nicenessp (_, _, _, (c, g, niceness), m) (_, _, _, (c, g', niceness'), m') = -( niceness' - niceness) // temp negate!




// Put Yreturn as last command in a block.
// Conservative: We want to keep order of everything else.
// Does not look inside Yif!
let gen_sorted_Yblock lst =
    let rec ss (e, l) cmds =
        match cmds with
        | [] -> (rev e) @ (rev l)
        | (Yreturn v)::tt -> ss ((hd cmds)::l, e) tt
        | _::tt           -> ss (l, (hd cmds)::e) tt
    ss ([], []) lst


//
// Temp routine to be deleted soon.  Make a net that holds a string.
// 
let stringnet(id, states, io) =
    ionet_w(id, -1, io, Unsigned, []) // hprls convention: width is -1 for a string.
    //let f2 = { with vt= V_SPARAMETER }
    //let _ = netsetup_log (ff, f2)

#if SPARE    
    let (n, ov) = netsetup_start id
    if ov<>None
        then X_bnet(valOf ov)
        else
            let ans = netsetup_log{ 
                                    constval=[];
                                       signed=Unsigned;
                                       length=[];
                                       dir = false;
                                       pol=false;
                                       xnet_io = io; 
                                       vtype = V_SPARAMETER;
                                       h= 0I; l= 0I;
                                       ats= [];
                                       id= id;
                                       width= -1; // A string is -1, -2 is void *. 0 is not known or in h/l.
//                                       states= states;
                                       n= n 
                                  }
            X_bnet ans
#endif    

type codepoints_t = OptionStore<string, hexp_t>

let allocate_codepoints ww settings points start_state = 
    let codepoints = new codepoints_t("codepoints")
    let m_next_codept = ref 0
    let prodpoint sv =
        let ss = svToStr_full sv
        if settings.integer_pcvals then // We can use strings as our enumaration directly... but RTL output will not like that
            match codepoints.lookup ss with
                | Some vv -> vv
                | None ->
                    let xv = !m_next_codept
                    mutinc m_next_codept 1
                    let vv = xi_stringx (XS_withval(xi_num xv)) ss
                    vprintln 2 (sprintf "Allocated codepoint %i to %s" xv ss)
                    codepoints.add ss vv
                    vv
        else xi_string ss

    let starter = prodpoint start_state // Do this first to establish reset at zero.
    let iv = xi_manifest64 "starter-encoding" starter
    vprintln 3 (sprintf "Starter is %s: %i" (xToStr starter) iv)
    if iv <> 0L then sf("Starter was not at reset point zero")

    let ccvt (svl, successors) = 
        //let _:successor_t list = successors - type assertion not correct since dcon has list form by this stage
        let keys = svToStr_full svl
        let point = prodpoint svl
        ()
    app ccvt points
    let pc_values = !m_next_codept
    (codepoints, pc_values)





// New render: Returns main parts of a vm2 containing the glue logic.
let newrender ww settings (idle_pred,
                           formal_decls,
                           pcs,
                           start_state) (codepoints:codepoints_t, pc_values:int) points =
    
    let vd = settings.newrender_vd
    //reportx 3 "new render nets" (fun (x, nd, ns) -> netToStr x + " " + dirToStr nd) nets
    let _ = WF 2 "newrender" ww (sprintf "Start %i live states in product machine" (length points))

    let debug_net = stringnet("debug_info", None, LOCAL)
    let nets = [] // for now
    let rstnet = ionet_w("reset", 1, INPUT, Unsigned, [])
    let director = 
                { g_null_directorate with
                  //style=           directorate_style
                    clocks=          [E_pos g_clknet]
                    resets=          [(true, false, rstnet)]
                }
    let (formals_, locals) =
        let is_io = function
            | Always_input -> true
            | Ndi x -> varmode_is_io x
        groom2 ((fun (x, nd, ns) -> is_io nd)) nets

    let proto_statenet = stringnet("cvtrpc", None, LOCAL) // This is ok for SystemC as the final output form in fact.


    let m_arcs = ref 0

    // The original start state may not directly correspond to an actual state in the telescoped result.
    let prodpoint msg ss =
        match codepoints.lookup ss with
            | Some vv->vv
            | None ->
                for kv in codepoints do vprintln 1 (sprintf "  available point %s " kv.Key)
                sf (sprintf "L1686: no prodpoint: '%s' for %s" msg ss) 


    let cvt cvt_cc (svl, successors) = 
        //let _:successor_t list = successors - type assertion not correct since dcon has list form by this stage
        let key_s = svToStr_full svl
        let point = prodpoint "key_s" key_s
        //let point = prodpoint svl
        let gens (ppno, clockprops_, svl', (cmds, grds, rank), dcon_) cc =
            let key_d = svToStr_full svl'
            dev_println (sprintf " cvt %s     -> %s" key_s key_d)
            // The supress of active to callstates would need to be idle for an initiator of course! What? TODO = two places
            let grdss = ix_andl grds
            let cmd_keep ((dir, jspec), cmd) =
                match cmd with
                    | Yassign(dc_to, lhs, rhs) ->
                        let keep = not settings.discard_participant_pcs || not(memberp lhs pcs) (* || xiToStr r = "active"*)  // We can discard assigns to participant PCs, since these are no longer needed, but they are nice for debugging/tracing.
                        if vd>=3 then vprintln 3 (sprintf "TP%i Consider keep of assign to %s of %s  keep=%A. grds=%s keys=%s" ppno (lifeToStr lhs) (lifeToStr rhs) keep (concise_xbToStr grdss) key_s)
                        true
                        //keep
                    | _ -> true
            let cmds =
                let c1 = List.filter cmd_keep cmds
                vprintln 3 (sprintf "TP%i: rank=%i Kept %i of %i commands" ppno rank (length c1) (length cmds))
                c1
            let comments = List.map (fun (dcoding, cmd)->Ycomment(ybevToStr cmd)) (List.filter (fun((dir, jspec), cmd)->is_symbolic jspec.coding) cmds)
            let ma =
                let idlep = beval nets (fst svl') idle_pred
                key_d  + sprintf "Rank=%i"  rank + " idlep=" + xbToStr idlep
            let ma = sprintf "Start TP%i " ppno + ma
            let deb = Yassign(None, DL_lift debug_net, DL_lift(xi_string ma))
            let xx arg = xi_manifest64 "xxf" arg

            dev_println(sprintf "cvt: TP%i %s   %i -> %i   gg=%s" ppno key_s (xx point) (xx(prodpoint "key_d" key_d)) (concise_xbToStr grdss))
            let goto = Yassign(None, DL_lift proto_statenet, DL_lift(prodpoint "key_d" key_d))
            let mm = Ycomment(ma)
            mutinc m_arcs 1
            let cmdss = gec_Yif grdss (gen_sorted_Yblock(mm :: comments @ [deb] @ (map snd cmds) @ [ goto ])) cc
            cmdss
        let successors = (List.sortWith nicenessp successors) // Nicest first on rank basis. hmmm with fold left nicest will be lost? TODO
        let contents = List.foldBack gens successors []
        vprintln 3 (sprintf "Converted glue state " + key_s + " with live successors " + i2s(length successors))

        //gec_Yif(ix_deqd proto_statenet point) contents cvt_cc // This is later pattern matched to a case statement.
        ((proto_statenet, point), contents)::cvt_cc // This is later pattern matched to a case statement.
#if OLD
 // Set.fold here
    let clauses =
        hpr_yikes("start state lift missing")
        // Move start state to start...
        //let sstate = valOf_or_fail "Missing Start State" (op_assoc start_state points)
        //let points = ((start_state, sstate)::(list_delitem (start_state, sstate) points))
        List.fold cvt [] points
#endif
    let cans = List.fold cvt [] points

    let _ = 
        let mx = sprintf "New render of %i product points (%i originally) and %i arcs" (length cans) (length points) !m_arcs
        mutadd settings.m_states_report_lines mx        
        vprintln 1 mx
    let statenet =
        if not settings.integer_pcvals then proto_statenet // We can use strings as our enumaration directly... but RTL output will not like that
        else
            let pc_encoding_width = bound_log2(BigInteger pc_values)
            let pc = vectornet_w(settings.pc_name, pc_encoding_width)
            vprintln 1 (sprintf "Need %i values in product PC. %s" pc_values (netToStr pc))
            pc
            
    let mMM = makemap [ (proto_statenet, statenet) ]

    let rw_exp =  xi_assoc_exp g_sk_null mMM
    let rw_bexp = xi_assoc_bexp g_sk_null mMM     

    let rec yToHbev = function
        | Ycomment ss       -> Xcomment ss
        | Yif (gg, tl, fl)  -> gec_Xif (rw_bexp gg) (gec_Xblock (map yToHbev tl)) (gec_Xblock (map yToHbev fl))
        | Yassign(_, DL_lift lhs, DL_lift rhs) -> Xassign(rw_exp lhs, rw_exp rhs)
        | Yreturn vv        -> Xreturn(rw_exp vv)
        | other -> sf "L1193 other yToHbev"


#if SPARE
    let rec ysupport cc = function
        | Ycomment ss -> cc // Discard our comments - sad.
        | Yif (gg, tl, fl)  -> List.fold ysupport (List.fold ysupport (xi_bsupport cc gg) fl) tl
        | Yassign(lhs, rhs) -> xi_support (xi_support cc rhs) lhs
        | _ -> cc


    let support = List.fold ysupport [] ans

    let allocate_codepoints (sm:Map<hexp_t, hexp_t>) (start_state, _) =
        match start_state with
            | W_string(ss, XS_withval _, _) -> sm
            | W_string(ss, _, _) ->
                sf (sprintf "need codepoint for ss %s" ss)                   
            | other -> sm
    let mapping = List.fold allocate_codepoints Map.empty support
#endif

    let (n_goers, n_togs) = (ref 0, ref 0)
    
    let rec yToRtl pp grds  ycmd (cb, cs) =
        let addg g gs = // Forming conjunction of nested guards. Guards are often repeated: could use cons, singly_add or ix_and ...  perhaps best as ascending xidx_t list which is what ix_and would do?
            singly_add g gs

        let ybuffer lhs rhs =
            let gg = rw_bexp(ix_andl grds)
            if xi_isfalse gg then
                mutinc n_togs 1
                //dev_println(sprintf "yToRtl: buffer: discard false guard %s " (sfold concise_xbToStr grds))
                (cb, cs)
            else
                let lhs = rw_exp lhs
                let rhs = rw_exp rhs
                //dev_println(sprintf "yToRtl: buffer: arc assign %s %s" (xToStr lhs) (xToStr rhs))
                let mustsel = false // TODO for now - this introduces comb loops etc..

                // TODO the defaulty value of comb outputs.
                let gg =
                    match pp with
                        | None       -> gg
                        | Some (a,b) when mustsel -> ix_and gg (ix_deqd a b)
                        | Some (a,b) -> (ix_deqd a b)                            
                ((gg, lhs, rhs)::cb, cs)

        match ycmd with
            | Ycomment ss ->
                dev_println(sprintf "ycmd: nop %s" ss)
                (cb, XRTL_nop(ss)::cs)
            | Yif (gg, tl, fl) ->
                //dev_println(sprintf "ycmd: if %s" (concise_xbToStr gg))
                let (cb, cs) = List.foldBack (yToRtl pp (addg gg grds)) tl (cb, cs)
                List.foldBack (yToRtl pp (addg (xi_not gg) grds)) fl (cb, cs)
                //List.foldBack ( tl (List.foldBack (yToRtl pp (addg (xi_not gg) grds)) fl cc)
            | Yassign(dco, DL_lift lhs, DL_lift rhs) ->
                let combf =
                    match dco with
                        | Some(dir, coding) -> (dir = Ndi OUTPUT) // for now
                        | _                 -> false
                //dev_println(sprintf "Select comb: based on dir for %s. combf=%A" (xToStr lhs) combf)
                if combf then ybuffer lhs rhs
                else
                let gg = rw_bexp(ix_andl grds)
                if xi_isfalse gg then
                    mutinc n_togs 1
                    //dev_println(sprintf "yToRtl: discard false guard %s " (sfold concise_xbToStr grds))
                    (cb, cs)
                else
                    mutinc n_goers 1
                    let lhs = rw_exp lhs
                    let rhs = rw_exp rhs
                    dev_println(sprintf "yToRtl: arc assign %s %s" (xToStr lhs) (xToStr rhs))
                    (cb, gen_XRTL_arc(pp, gg, lhs, rhs) :: cs)
            | Ybuffer(DL_lift lhs, DL_lift rhs) -> ybuffer lhs rhs
            | other -> sf (sprintf "yToRtl: other form %A" other)

    let gen_rtl = not settings.generate_bevcode

    let yr0 ((_, pcv), work) (cb, cs) =
        let pp = Some(statenet, pcv)
        let (cb, cs) = List.foldBack (yToRtl pp []) work (cb, cs)
    //let ans = gec_Yif (xi_orred rstnet) [Yassign(proto_statenet, prodpoint start_state)] clauses // TODO: better document convention that (or whether) reset is manifest here and implicit in other hpr forms... double insertion of reset does not matter ultimately.
        (cb, cs)
        
    let (cb, xrtl) = List.foldBack yr0 cans ([], [])
    let _ =
        let mx = (sprintf "Kept %i arcs with %i discarded owing to invalid guards." !n_goers !n_togs)
        vprintln 1 mx
        mutadd settings.m_states_report_lines mx

    let bufs =
        let col = generic_collate f2o3 cb // Collate on lhs
        let genbuf (lhs, trips) =
            let backstop =
                match resetval_o lhs with
                    | Some vale -> vale
                    | _ -> sf "L1853"
            let rec qmux = function
                | []                 -> backstop
                | (g, lhs_, rhs)::tt -> ix_query g rhs (qmux tt)
            gen_XRTL_buf(X_true, lhs, qmux trips)
        map genbuf col
    let execs =
        if gen_rtl then
            let mx = (sprintf "Created %i arcs/assignments in RTL output." (length xrtl))
            mutadd settings.m_states_report_lines mx
            //[ H2BLK(director, SP_rtl xrtl) ]            
            vprintln 3 mx
            let printvd = 3
            let visit_budget = g_default_visit_budget
            let (fsm, arcs_other_machines, _, _) = project_arcs_to_fsm ww "project RTL" printvd false statenet visit_budget xrtl
            let ctl = { id= funique "jinjoin-gen" }
            let rtl = arcs_other_machines @ bufs
            let boz = if nullp rtl then [] else [ SP_rtl(ctl, rtl)]
            [ H2BLK(director, gen_SP_lockstep (boz@fsm)) ]
        else
            let ans = muddy "old way - non-FSM output style."
            let hbev = gec_Xblock(map yToHbev ans) //  Dont want to call map yr0 but then stats lines wrong order.
    // hbev_t output 
            let ast_ctrl = { abstracte.g_null_ast_ctrl with id=funique "jinjoin" }
            [ H2BLK(director, SP_l(ast_ctrl, hbev)) ]


    // let formals = rstnet :: (map f1o3 formals)

    let locfilter cc (x, _, _) = 
        if (xToStr x).Contains(".call") then cc // delete 'call' variables - crude string match for now!
        else cc //deletes all actually

    let locals = debug_net::statenet::(List.fold locfilter [] locals)
    (formal_decls, locals, execs) // end of newrender




#if OLD_RENDER_USED
// We now generate an H2 VM and let it be rendered by another stage in the orangepath.
let oldrender settigs (
               live,
               mainarray :mainarray_t,
               allowed_path_set: allowed_path_set_t,
               idle_pred,
               nets,
               pcs,
               start_state) =
    
    let yout_open_out f = System.IO. File.CreateText(f)
    let yout (fs:System.IO.StreamWriter, s:string) = fs.Write s
    let yout_close (fs:System.IO.StreamWriter)  = fs.Close()
    let fname = opfname + ".cpp"
    let fd = yout_open_out fname
    let render s = yout(fd, s)    
    let cvtToAutomaton (svl) = 
    let ov = ma_lookup_suc mainarray svl
    if nonep ov then ()
    else
        render "//----------------------------------------------\n"
        let islive (svl', _, m) = memberp svl' live
        let successors = List.filter islive (!(valOf ov))
        let keys = svToStr_full svl
        let (found, allowed) = allowed_path_set.TryGetValue keys // We only render if the allowed, live successors.
        assert(found)
        let successors =
            if found then List.filter (fun (k, _, _)-> memberp (svToStr_full k) allowed) successors  // Quadratic... avoid? 
            else []
        if successors = [] then ()
        else
            let idlep = beval nets svl idle_pred
            render (" case " + keys + ": //" + svToStr svl + " idle=" + xbToStr idlep + "\n")

            let statenet = X_net "state"

            let netfun x  =
                match x with
                    | X_bnet ff    -> (x, ([ff.id], ff.id, "", x))
                    | X_net(id, _) -> (x, ([id], id, "", x))                

            let cppf:cppToStr_t = {
                 cpp1=      gen.cpp
                 flags=     []
                 signals=   []
                 hdrflg=    false
                 netlist=   map netfun (statenet:: (map f1o3 nets))
                }

            let ii = "    "
            let gen (svl', (cmds, grds, rank), (_, _, _)) =
                let keys' = svToStr_full svl'
                let idlep = beval nets svl' idle_pred
                // The supress of active to callstates would need to be idle for an initiator of course!! TODO
                let cmds = List.filter (fun((dir, coding), Xassign(lhs, r))->not(memberp lhs pcs (* || xiToStr r = "active"*) )) cmds
                let comments = List.map (fun (dcoding, cmd)->ii + "// " + hbevToStr_cleaned "" cmd + ";") (List.filter (fun((dir, coding), cmd)->is_symbolic coding) cmds)
                let gfin = List.fold (fun c g->xi_and(c, g)) X_true grds
                let grdss = cpp_xbToStr cppf gfin
                let goto = Xassign(statenet, xi_string keys')
                let deb = Xassign(X_net "debug_state", xi_string keys')
                let goto = cpp_render_unit 0 cppf (CPP_BEV(goto))
                let cmdss = List.fold (fun c cmd-> cpp_render_unit 0 cppf (CPP_BEV cmd) (*hbevToStr cmd +  "; " *) + c) goto (deb :: (map snd cmds))
                List.iter (render)  comments
                let m = (" Rank=" + i2s rank + " idlep=" + xbToStr idlep + "\n")
                render (ii + "// " + keys' + "," + svToStr svl' + m)
                render (ii + "if (" + grdss + ") { " + cmdss + "}\n")
                ()

            let successors = List.sortWith rankp successors // nicest first.
            List.iter gen successors
            render (ii + "break;\n") 
            report("Wrote glue state " + keys + " with " + i2s(length successors) + " arcs to " + fname)
            ()

    render ("if (reset) state = " + svToStr_full start_state + "; else \n")
    render ("  switch(state)\n{")
    List.map cvtToAutomaton (start_state::(list_delitem start_state live))
    render ("}\n\n")
    report("Wrote glue machine with " + i2s(length live) + " states to " + fname)    
    yout_close fd
    ()
#endif


let inc_gro msg0 m_gf msg = function
    | Some green_no ->
        dev_println (sprintf "Increment evergreen gro %s for %s from %i" msg0 (xToStr msg) green_no)
        if green_no = g_green_limit then m_gf := true
        if green_no > g_green_limit then sf(sprintf "Over-reached arbitrary green limit temp stop %i" green_no)
        Some(green_no + 1)
    | None ->
        vprintln 3 (sprintf "Cannot increment green number %s of %s " msg0 (xToStr msg))
        None

let rec mine_cre cx = function
    | [] -> sf "mine_cre failed"
    | SV_cre(backtok_, eqns, donef) as cre :: tt -> (cre, (rev cx) @ tt)
    | ox::tt -> mine_cre (ox::cx) tt

let rec mine_backtok = function
    | [] -> sf "mine_backtok failed"
    | SV_cre(backtok, eqns, donef)::_ -> backtok
    | _::tt -> mine_backtok tt


//
// Conserve data by marrying up the evictions with the admissions or else delete the combination from cc if cannot match.
// All of this work takes place in the commanded segment of the state vector.
// The revised svl is returned as a successor
// data_compatible can return zero, one or many if there are alternative meshings.
let data_compatible ww settings nn nets svl cc ((cp_env, (clkprop, (concs, evf, adt))), arc_idx) =
    let _:sv_t = svl
    let segs = fst svl // Drop keys. Suitable keys are re-affixed when successor is stored in mainarray.

    let m_gf = ref false
    let (cre, segrest) = mine_cre [] (hd segs)
    if not_nullp evf || not_nullp adt then
        vprintln 3 (sprintf "idx=A%i data_compatible? evf=%i" arc_idx (length evf) + sprintf " adt=%i" (length adt))
    let (conserve_guard, cmds, dp_actions, cre, powerstrokes) = data_conserve nets cre evf adt
    let rep = vprintln 3

   // messy, only the evict powerstrokes come in from data_conserve ...
    let dp_settle sv (id1, ov__, proposed_nv, netspec_) = // datapath marry
        //rep("\n\n\ndp_settle marry operating on " + xToStr id1 + " proposed_nv=" + xToStr proposed_nv)

        let operate gro prior_vale netspec = // This is what makes the actual changes to the state vector
            //vprintln 3 (sprintf "    dp_settle: operate: %s  prior=%s cf nv=%s   " (xToStr id1) (lifeToStr prior_vale) (lifeToStr proposed_nv))
            let (final_gro, final_vale) =
                if memberp id1 powerstrokes then
                    (inc_gro "L2393" m_gf id1 gro, DL_dead)
                elif proposed_nv = g_DL_powerstroke || ov__ = g_DL_powerstroke then // check either dir for now! TODO check the right one.
                    (inc_gro "L2395" m_gf id1 gro, DL_dead)
                // Do not now need next clause.
                elif proposed_nv = g_DL_powerstroke then //A powerstroke does not change the lhs stored value (remains DEAD). Instead it is an increment of green number and an eviction for an input bus or an admission for an output bus.
                    dev_println (sprintf "    perform powerstroke %s  %s cf %s   " (xToStr id1) (lifeToStr prior_vale) (lifeToStr proposed_nv))
                    if isevict_godead id1 (proposed_nv, netspec) then  (inc_gro "L2398d" m_gf id1 gro, prior_vale)
                    elif isadmit_golive id1 (proposed_nv, netspec) then (inc_gro "L2398l" m_gf id1 gro, prior_vale)
                    else muddy "DL_active pattern not matched" // Here we are expecting DL_active to be strongly associated with inc_gro
                else (gro, proposed_nv)
            //rep ("dpmarry change " + xToStr id1 + " from " + xToStr prior_vale + " with rhs=" + xToStr proposed_nv + " to " + lifeToStr final_vale)
            SV_4(final_gro, id1, final_vale, netspec)

        let rec map_operate sofar = function
            | [] -> rev sofar
            | SV_4(gro, id, prior_vale, netspec)::tt ->
                //rep ("  dp_settle point: " + xToStr prior_vale + "/" + xToStr id)
                let nx = if id=id1 then operate gro prior_vale netspec else SV_4(gro, id, prior_vale, netspec)
                map_operate (nx::sofar) tt
            | SV_cre(a, b, donef) :: tt -> map_operate (SV_cre(a, b, donef)::sofar) tt
            //| _ -> muddy "bug5"
        map_operate [] sv

    let external_guard = // from cp_env
        let eg cc = function
            | SV_4(_, id, DL_lift vale, coding) ->
                let g1 = gup_ix_deqd id vale
                ix_and g1 cc
            | SV_4(_, id, vale, coding) -> sf "L2269-valed"
            | SV_cre _ -> cc
        List.fold eg X_true (f2o3 cp_env)
    let guard = ix_and conserve_guard external_guard
    if not (xi_isfalse guard) then
        //dev_println (sprintf "dp_settle on %i dp_actions and %i concs" (length dp_actions) (length concs))
        let silly_type_drop = function
            | (DL_lift id, a, o, coding) -> (id, a, o, coding)
            | _ -> sf "L2352"
        let ans = List.fold dp_settle segrest (map silly_type_drop dp_actions @ concs) // Only processes command segment here

        if !m_gf then
            vprintln 3 (sprintf "Search terminated owing to hitting green limit")
            cc
        else
            //let ans_s = intval (nets) ans
            //vprintln 3 ("dpmary - new commanded part of sv " + svToStr0 ans + " " + ans_s)
            //rep ("   root=" + nn + ": compatible ans --> " + (if ans_s=nn then "same" else ans_s) + " guard=" + xbToStr guard + " commands=" + sfold ybevToStr (map snd cmds))
            //cassert(evf=[] && adt=[], "stopnow")
            singly_add (arc_idx, clkprop, (cre::ans) :: tl segs, guard, cmds) cc
    else cc



// Convert internal rep for output plot in dot. 
let plotnodes3 ww title detailed_arcsf annotations_o opl = 
    let xxf arg = xi_manifest64 "xxf" arg

    let annotation key =
        match annotations_o with
            | None -> ""
            | Some(codepoints:codepoints_t) ->
                match codepoints.lookup key with
                    | Some ax ->
                        let ax = xxf ax
                        //dev_println (sprintf "Retrieved annotation %s -> %i" key (ax))
                        i2s64 ax + ":: "
                    | None ->
                        vprintln 3 (sprintf "L1966: No annotation key for %s" key)
                        ""
                        
    let dlab_ref svl = "NN_" + (* string_can*) (svToStr_full svl) // string_can now implemented within dot report

    let dlab_def svl =
        let key_s = svToStr_full svl
        let annos = annotation key_s
        let colour = if key_s.IndexOf "START" >= 0 then "red" else "brown"
        let colour =
            if key_s.IndexOf "START" >= 0 then "red"
            elif key_s.IndexOf "_end" >= 0 then "yellow"
            else "brown"                    
        let lab = string_autoinsert_newlines 55 (* string_can *)(annos + svToStr_full svl)
        DNODE_DEF(dlab_ref svl, [ ("label", lab); ("color", colour) ])


    let dlab (svl:sv_t) = gec_DNODE_plain(dlab_ref svl)
    let (m_arcs, m_nodes) = (ref [], ref [])
    let nodeplot svp = mutadd m_nodes (dlab_def svp)

    // The nodes already often have an index, printed before :: so perhaps use that?
    let nodes_idx = zipWithIndex (map (fun (basis_svl, _) -> dlab basis_svl) opl)
    app (fst >> nodeplot) opl
    
    let pnx bundleup first_pass_arcs (basis_svl, successors) =
        let id = dlab basis_svl
        //dev_println (sprintf "%i successors at plotnodes2: %A" (length successors) successors)
        let arcplot(_, clockprops, sv, (cmds, guards, rank), dcon_) =
            //if memberp sv opl then
                let id1 = dlab sv
                let delta = (valOf_or (op_assoc id1 nodes_idx) 0) - (valOf_or (op_assoc id nodes_idx) 0)
                let w = if delta > 0 then 1 else 2
                let summary_arc_f = bundleup && length(List.filter (fun (x, y) -> x = (id, id1)) first_pass_arcs) > 1
                let deltas =
                    match detailed_arcsf with
                        | 0 -> if nullp guards then "T" else "->"
                        | 1 -> (sfold concise_xbToStr guards)
                        | 2 -> (sfold concise_xbToStr guards) + "/" + muddy "svDeltaToStr sv basis_svl"
                        | 3 -> (sfold xbToStr guards) + "/" + muddy "svDeltaToStr sv basis_svl"

                let lab =
                    if summary_arc_f then "..."
                    else (* string_can *) string_autoinsert_newlines 55 (deltas)
                let col =
                    match clockprops with
                        | None       -> "green"
                        | Some true  -> "orange"
                        | Some false -> "black"
                let directed = [ ("label", lab); ("color", col); ("penwidth", i2s w) ] // the default?

                mutaddonce m_arcs ((id, id1), DARC(id, id1, directed))                    

                
        app arcplot successors
        ()
    app (pnx false []) opl
    let simplified_f = (length !m_arcs > 150)
    if simplified_f then
        let first_pass_arcs = !m_arcs
        m_arcs := []
        app (pnx true first_pass_arcs) opl        // Run simplified plot if too many arcs.

    vprintln 3 (sprintf "Finished plotnodes3 %s with %i nodes and %i arcs. simplified_f=%A" title (length !m_nodes) (length !m_arcs) simplified_f)
    let ans = !m_nodes @ (map snd !m_arcs)
    Some ans

// Plot in dot with live filter. 
let plotnodes2 ww mainarray title detailed_arcsf anno_o svls = 
    let rec oldy_out (svp:sv_t) = 
        let keys = svToStr_full svp
        let ov = ma_lookup_suc mainarray svp
        let successors =
            let islive (_, clockprops, svl, _, dcon) = memberp svl svls // Set membership - use a Set
            List.filter islive ((valOf_or_fail "cvt848" ov))
        (svp, successors)

    if length svls > 5000 then
        vprintln 1 (sprintf "Not plotting overly large dot file " + title)
        None
    else
        let op = map oldy_out svls
        plotnodes3 ww title detailed_arcsf anno_o op

#if SPARE
// This is the function for asynch composition: one var changes at once only. 
// A companion algorithm for synch composition: does/does not exist?
(* However, there is a  bit of semi-synchronous composition here, since when we kill a 
   symbolic register of width v we must also execute an arc immediately afterwards that
   goes live with a corresponding register of width v. The ticket and design pattern extensions will make the constraints more intricate in the future.
*)
// This is a cartesian product routine.
// Perturb each element of the state vector in turn to all of its possible alternate values - asynchronous means only one can change at a time.
let gen_stut_successors driven state = 
    sf "Not used anymore"
    let rec synch_flips sv = 
        match sv with
            | [] -> [[]]
            | SV_cre(a, b, donef)::tt -> synch_flips tt // The conservation tag is ignored here - it is passed via a different route to the successors.
            | (SV_4(gr, id, DL_lift state, (dir, coding)) as as_is)::tt -> 
                let wiggle cc s =
                      if state = s then as_is::cc // Preserve the as_is value, regardless of predicate.
                      elif unichange_p (driven, id, (dir, coding), state, s) then // Test can be lifted to per segment basis?
                          //vprintln 0 ("   FV uni wiggle " + xToStr id + " from " + lifeToStr state + " to " + xToStr s)
                          SV_4(gr, id, DL_lift state, (dir, coding))::cc
                      else cc
                let alternate_values = List.fold wiggle [] (uni_coding_states coding) // 1/3 - get stut successor
                let tailer = synch_flips tt
                if nullp alternate_values 
                    then map (fun x -> as_is::x) tailer
                    else list_flatten(map (fun x -> map(fun y->x::y) tailer) alternate_values)
            | SV_4(gr, id, tother, (dir, coding))::tt -> 
                muddy (xToStr id + " - 2387 poggle " + lifeToStr tother)
            | _ -> muddy "L2387"

    let raw_ans = synch_flips state
        //We do not strip the nochange state, since there may be deterministic execution
        //that is running in the participants while no inputs change.
        //let _ = list_subtract(rawans, [state])
    raw_ans


// Likewise cartesian product of the segments.
let gen_successors driven (sv, (bk, fk)) =
    let rec kk = function
        | [item] -> [gen_stut_successors driven item]
        | h::tt ->
            let h' = gen_stut_successors driven h
            let t' = kk tt
            cartesian_lists h' t'
    kk sv
#endif

    

let my_monkey = function // This must not be ambivalent but there are other allowable answers.
    | X_true  -> true    // Shortcuts 
    | X_false -> false  // 
    | other ->
        match xbmonkey other with
            | Some true -> true
            | Some false -> false
            | _ -> sf (sprintf "my_monkey other form %s %s" (xbkey other) (xbToStr other))



            
//=============================================================================
// machine_execute obeys the commands associated with the taken arcs of a participant machine.
// I thought it might also goes all ways allowed by non-det inputs?  We can tree walk the guard expressions to find the non-det input values that do something or we can have a list of them. But it does not.
// We return three lists of deltas: each delta has form (id, from, to, dcoding).
// The three lists are the concretes, the evictions and the admissions.
// Unilateral evictions and admissions are recorded in the concs and committed to the state.
let machine_execute ww settings vd keys name nets arcs_of_one_machine delts00 sv00 = 
    let _ :svitem_t list list = sv00 // undecorated input
    let m_gf = ref false // This site unused
    let execute (sv:svitem_t list) cmdlist =
        vprintln 2 (sprintf "Machine execute arcs=^%i keys=%s" (length cmdlist) keys + "----------------------------\n")
        let (m_concs, m_evf, m_adt) = (ref delts00, ref [], ref []) 
        let rec cmd_apply1 (new_vale, var) sv = 
            match sv with
            | [] ->
                hpr_yikes(sprintf "machine_execute: Command could not be applied. lhs not in (this part of) the statevector.  %s := %s" (xToStr var) (lifeToStr new_vale))
                sf "Following code does not need to be used if inputs are in commanded SV. L2050"
                let djc = valOf_or_fail "L2050" (settings.m_busses.lookup (xToStr var))
                // The input bus has an eviction since this must match with some resource that admits the received value from the input bus.
                let newpred_evf = is_symbolic ((snd djc).coding) && new_vale<>DL_dead && (fst djc=Ndi INPUT || fst djc = Always_input)
                // Contrarywise, an output bus makes an admission.
                // newpred is now active swipe code. tidy.
                let newpred_adt = is_symbolic (snd djc).coding && new_vale<>DL_dead && fst djc=Ndi OUTPUT 
                dev_println (sprintf "%s newpred: evf=%A adt=%A" (netToStr var) newpred_evf newpred_adt)
                let prior_state = DL_dead
                if newpred_evf then mutadd m_evf (var, prior_state, new_vale, djc)
                if newpred_adt then mutadd m_adt (var, prior_state, new_vale, djc)
                []
            | SV_cre(a, b, donef)::tt -> SV_cre(a,b, donef) :: cmd_apply1 (new_vale, var) tt
            | SV_4(gr_o, id, prior_state, coding)::tt -> 
               // dev_println (sprintf "        consider update %s cf  %s  (for := %s)   " (xToStr var) (xToStr id) (lifeToStr new_vale))
               if var=id then 
                     // DL_active not only used for active swipes? So inc_gro might report fails?
                    if new_vale = g_DL_powerstroke then //A powerstroke does not change the lhs stored value (remains DEAD). Instead it is an increment of green number and an eviction for an input bus or an admission for an output bus.  Not always - lets be clear on that going forward!
                        dev_println (sprintf "   prelim consider powerstroke update %s  %s cf %s   " (xToStr var) (lifeToStr prior_state) (lifeToStr new_vale))
                        if isevict_godead var (new_vale, coding) then
                            mutadd m_evf (id, prior_state, new_vale, coding)
                        elif isadmit_golive var (new_vale, coding) then mutadd m_adt (id, prior_state, new_vale, coding)
                        SV_4(inc_gro "L2642ignored" m_gf id gr_o, id, prior_state, coding)::tt // This increment is ignored and done again in dp
                    else
                        let vale1 = // Always the same as prior_state infact now post deferral of commit.
                            if prior_state<>new_vale then // Not the same for evergreen symbols.
                            //dev_println (sprintf "Really consider update %s  %s cf %s   " (xToStr var) (xToStr state) (xToStr vale))
                                if isevict_godead var (new_vale, coding) then
                                    (mutadd m_evf (id, prior_state, new_vale, coding); prior_state)   // Don't evict yet, so vale1=prior_state!
                                elif isadmit_golive var (new_vale, coding) then (mutadd m_adt (id, prior_state, new_vale, coding); prior_state) // Dont go live yet, but put live expression in adt.
                                else (mutadd m_concs (id, prior_state, new_vale, coding); prior_state)
                            else new_vale
                        SV_4(gr_o, id, vale1, coding)::tt
               else SV_4(gr_o, id, prior_state, coding) :: (cmd_apply1 (new_vale, var) tt)
        let cmd_apply sv = function
            | Xassign(var, new_vale) -> cmd_apply1 (gec_DL_lift new_vale, var) sv
            | other ->
                hpr_yikes(sprintf "cmd_apply ignored other command " + hbevToStr other)
                sv // Other commands are ignored, but there aren't any.
        let sv_modified_ = List.fold cmd_apply sv cmdlist // We dont return the modified state vector at this point and the code that generated it can be deleted soon. 
        (!m_concs, !m_evf, !m_adt) // We just return the deltas.


    let active_arcs =
        let ix_deqd_ a b =
            let ans = ix_deqd a b // TODO the ix_deqd expression can be rezzed in pre-compile participant. Should be faster.
            vprintln 3 (sprintf "Arc src check: %s cf %s gives %A" (xToStr a) (xToStr b) (xbToStr ans))
            ans
        let active_arc (uid, clks, (state, pc), guard, cmds, dlst) =
            let g0 = my_monkey(beval nets sv00 (ix_deqd state pc)) // TODO the ix_deqd expression can be rezzed in pre-compile participant. Should be faster.
            let g1 = my_monkey(beval nets sv00 guard)
            let ans = g0 && g1
            if g0 then
                let gmsg = if ans then sprintf "g0=%A g1=%A" (g0) (g1) else sprintf "%s evaluates to %A" (concise_xbToStr guard) ans
                vprintln 3 (sprintf "Arc check: ARC%s %s cf %s  guard=%s " uid (xToStr pc) (xToStr state) gmsg)
            ans
        List.filter active_arc arcs_of_one_machine
    let ll = length active_arcs

    if vd>=5 then reportx 5 (sprintf "machine_execute: Active arcs for '%s' are %i out of %i: " name (length active_arcs) (length arcs_of_one_machine) + keys) (fun x -> "active: " + arcToStr x) active_arcs
    else vprintln 3 (sprintf "machine_execute: '%s' %i/%i active_arcs" name ll (length arcs_of_one_machine))

    let execute1 sv = function
        | (uid, clockprops, (state, pc), g1, cmds, destlst) ->
            let kc dest = execute sv (Xassign(pc, dest)::cmds)
            map kc destlst 

    let ans = 
        match active_arcs with
            | [] -> []
            | [(uid, clks, (state, pc), guard, cmds, dlst) as arc] ->
                let addclock rr = (clks, rr)
                let r3 = execute1 (hd sv00) arc // TODO march 2020 is hd sufficient? Sort out.
                map addclock r3
            | arcs ->
                reportx 0 "Two or more arcs fired at once" arcToStr active_arcs
                let px ((uid, clks, (state, pc), guard, cmds, dlst) as arc) = 
                    let addclock rr = (clks, rr)
                    let r3 = execute1 (hd sv00) arc // Ditto
                    map addclock r3
                list_flatten(map px arcs)
                //sf (sprintf "Two arcs fired in this machine '%s'." name)
    let nothing = (false, ([], [], []))   // The NOP stutter - is here the place to add it?
    nothing :: ans
    // end of machine_execute





// Determine unilateral symbolic changes from the commanded part of the sv.
// Also save them in dp cache dictionary once computed.
// These are not simply pre-computed like the other wiggles since not all will be needed.
// Determine unilateral changes in commanded symbolic variables - inputs will go dead as we read data from them and outputs can go live as we forward data to them. 2/2 - This is noted in their + plus sign marking. Seems back to front?  Move over to commanded in both directions now.
// Additional resources wont be in the commanded section unless we back them up with an FSM
let get_commanded_unilateral_symbolic_changes ww settings nets driven (sv, key) =
    let (found, ov) = settings.unisymb_cache.TryGetValue(key)
    if found then ov
    else
        let one = function
            | SV_4(gr, net, vale, dc) ->
                let rep vd m nv = vprintln 3 ("  Rez for cache, unilateral change for commanded " + xToStr net + " " + djToStr dc + " from ov=" + lifeToStr vale + " to nv=" + lifeToStr nv + " ans=" + m)
                let wiggle ccc nv = // need its current value? plus anything it can uni change to
                    let nv' = muddy "drop lift nv"
                    if unichange_p(driven, net, dc, vale, nv') then
                        if vale<>nv then
                            ( rep 3 "different from current " nv;
                              singly_add (SV_4(gr, net, nv, dc)) ccc
                            )
                        else ( rep 3 "same as current so discard" nv; ccc )
                    else
                        ( rep 0 "not uni afterall?" nv;
                          if vale<>nv then singly_add (SV_4(gr, net, nv, dc)) ccc else ccc
                          //sf "was not uni afterall"
                        )
                let range = uni_coding_states (snd dc) // 2/3  commanded unis ... 
                let range = map (fun x -> gec_DL_lift x) range // TODO do once? This result is cached actually.
                //reportx 0 "symbolic uni change" (fun x->xToStr net + " net is " + xToStr x) range
                singly_add (SV_4(gr, net, vale, dc)) (List.fold wiggle [] range)
        let lsymbolic = function
            | SV_4(_, net, vale, dc) -> is_symbolic (snd dc).coding 
            | SV_cre _ -> false
            //| _ -> muddy "bugg"
        let rec usm = function
            | [] -> sf ("kk get_commanded_unilateral_symbolic_changes null")
            | [item] when lsymbolic item ->  map (fun x-> [x]) (one item)
            | [item] (* otherwise *) ->  [[item]]
            | h::tt when not(lsymbolic h) ->
                let tt' = usm tt
                cartesian_lists [h] tt'
            | h::tt (* otherwise *)->                            
                let h' = one h 
                let tt' = usm tt
                let ans = cartesian_lists h' tt'
                //vprintln 3 ("usm wiggle " + i2s(length h') + " " + i2s(length tt') + " = " + i2s(length ans))
                ans
        let ansl = usm sv

        let rec gdelts = function
            | ([], []) -> []
            | (SV_4(_, id, nv, dc)::t, SV_4(_, id', ov, dc')::s) ->
                            let r = gdelts (t, s)
                            if (nv<>ov) then (id, ov, nv, dc) :: r else r
            | _ -> sf "bug2"
        let nv = map (fun ans -> ((ans, intval nets ans), gdelts(ans, sv))) ansl
        reportx 3 (sprintf "Adding to cache under key=%s uni changes in commanded symbolic" key)(fun x-> intval nets x) ansl
        settings.unisymb_cache.Add(key, nv)
        nv

            
//==================================
//   Main product geneator.  doconsider starts with the global start state and considers all neighbours/successors where each component makes a changed, but some components change to themselves if that's part of their NSF ...


let product_runbatch ww settings (m_considered, m_dx_total, m_dx_conserving, m_pending) mainarray driven nets all_groups_wiggles work batchsize =

    let vd = 4 


    let progress_report keys =
        vprintln 1 ("Considering: " + keys)
        vprintln 1 ("Considered " + i2s(!m_considered) + " states so far, with " + i2s(!m_pending) + " states to go.")

    let consider_one_product_point re_point (idx_no_, x_, svl, y_, dcon_) = // node index not arc index?
            mutinc m_considered 1
            let keys = svToStr_full svl
            let svls = keys
            if !m_considered % 100 = 0 then progress_report keys// Periodic progress report

            let ml = svToStr_full svl
            vprintln 3 ("\n---------------------\n\n\n:Start-consider:" + ml)

#if SPARE_AND_SPURIOUS
            let zl0 = gen_successors driven svl // This uses site 1 which seems to double up on all_group_wiggles
            //vprintln 3 (n + " has " + i2s(length zl0) + " successor environments.")
            let zl0 = list_once zl0
            //vprintln 3 (n + " has " + i2s(length zl0) + " successor environments.")            
            //reportx 0 "Candidate Successor" svToStr zl0
#endif
#if OLD
            let livedead_monitor_ (newst, oldst) =
                let (m_evictions, m_admissions) = (ref [], ref [])
                let rec scan v =
                    match v with
                        | ([], [])  -> ()
                        | ((var, vale, dd)::ta, (old_var, old_vale, _)::tb) ->
                           (
                              vprintln 0 ("vale<>old_vale " + xToStr vale + xToStr old_vale + " " + boolToStr(vale=old_vale));
                              if vale <> old_vale && golive(vale, dd) then mutadd admissions (var, old_vale, vale, dd) else ();
                              if vale <> old_vale && godead(vale, dd) then mutadd evictions (var, vale, dd) else ();
                              if (var <> old_var) then failwith ("state vector mangled: " + xToStr var + "c f "+ xToStr old_var) else ();
                              assert(!m_evictions = []);// thse unilateral ones are not relevant
                              assert(!m_admissions = []);
                              scan (ta, tb)
                           )
                scan (newst, oldst)
                ()
            app (fun x -> livedead_monitor_(x, st)) zl0  // These are the LIVEDEAD THAT WE DONT WORRY ABOUT. ie they are in the participants and not in our glue
#endif

            let dToS1 (id, ov, nv, dc) = lifeToStr nv + "/" + xToStr id
            let dToS2 (id, ov, nv, dc) = lifeToStr ov + "->" + lifeToStr nv + "/" + xToStr id
            let dToS (cp_env, (clkprop, (coc, evf, adt))) =
                let qk = function
                    | SV_4(None, var, vale, coding) -> sprintf "%s/%s" (lifeToStr vale) (xToStr var)
                    | SV_4(Some subsc, var, vale, coding) -> sprintf "%s_%i/%s" (lifeToStr vale) subsc (xToStr var)                    
                    | SV_cre _ -> "SV_cre(...)"
                let a1 = sprintf "cp_env=%s clk=%A  coc=%s " (sfold qk (f2o3 cp_env)) clkprop (sfold dToS1 coc)
                let a2 = if nullp evf && nullp adt then " (nodata) " else sprintf "  evf=%s  adt=%s" (sfold dToS2 evf) (sfold dToS2 adt)
                a1+a2
                    
            let svToStr0 (id, vale, coding) = sprintf "%s/%s" (lifeToStr vale) (xToStr id)
            let sv_commanded:(svitem_t list) = hd (fst svl) // hd gives commanded
            let unis_00 = [] // get_commanded_unilateral_symbolic_changes ww settings driven sv_com 

            let sv_unishared = cadr (fst svl) //ignored for now
            let cre =
                let is_cre = function
                    | SV_cre _ :: _ -> false // disabled
                    | _             -> false
                List.filter is_cre (fst svl)
            let vd = 5 // for now

            // Is explore_env better called apply_wiggle?
            // Explore a candidate env for a uni-group
            // We product the changes in its range with the unisymb and shared ..?
            let explore_env (participant_o:part_t option, (unis:(hexp_t * netdir_t * bool * jspec_t) list), cp_envs) =
                let pi_name = if nonep participant_o then "NONOM" else (valOf participant_o).pi_name
                // TODO replace arcs at product stage with constrained_trajectories
                let arcs = if nonep participant_o then [] else (valOf participant_o).arcs
                let n_cp_envs = length cp_envs
                if vd>=4 then report (sprintf "Explore_env for '%s': arcs=^%i:  cp_envs=^%i" pi_name (length arcs) n_cp_envs)
                    
                let isolated_execute (idx, env, envs) =  // We have pre-generated all unilateral wiggles.
                    let jig com = // Augment a wiggle or partial sv (SAY WHICH PLEASE)  with commanded consequences (strange style free and bound vars!).
                        [ com; (* !! Remove while added to all_groups for now - sv_unishared;*) env ] @ cre
                    let ans = 

                    //First hand always taken for now
                        if nullp unis_00 then    // If we had no unilateral commanded changes (changes to the commanded variables made from outside)
                            let sv = jig sv_commanded  // The commanded component is re-inserted here  .. We should gen one empty one earlier instead of case splitting here
                            let _:svitem_t list list = sv
                            let unidelts1 = []
                            machine_execute ww settings vd keys pi_name nets arcs unidelts1 sv // Execute this machine/group in isolation - may give non-det choices
                        else
                            // otherwise we ...
                            //let ans22 = list_flatten(map (fun (unienvs1, unidelts1) -> machine_execute ww settings vd pi_name nets arcs unidelts1 (jig unienvs1)) unis_00) // TODO have consistent coding order perhaps.
                            muddy "ans22"
                    //let qaa (var , vale, _) = sprintf "%s/%s" (xToStr vale) (xToStr var)
                    //app (fun x -> dev_println (sprintf "Prepend %s  %s" envs (sfold qaa env))) ans
                    map (fun x -> ((idx, env, envs), x)) ans // Prepend relevant cp_env.

                let deltas =
                    if nullp cp_envs then
                        if vd>=4 then vprintln 4 (sprintf "%s: using empty cp env" pi_name)
                        let ans = isolated_execute (-1, [], "") // This is the single empty one - should have been provided earlier
                        //if vd>=4 then vprintln 4 (sprintf "%s: used empty cp env ans=%A" pi_name ans)
                        ans
                    else
                        if vd>=4 then
                            //let cp2str (env_idx, svitems, key) = sprintf "cp_env %i/%i key=%s  contents=%s" env_idx n_cp_envs key (sfold (fun (v, vale, coding) -> xToStr vale + "/" + xToStr v) svitems)
                            //reportx 4 "explore_env: cp_envs" cp2str cp_envs
                            ()
                        list_flatten(map isolated_execute cp_envs)
                let deltas = list_once deltas
                if vd>=5 then
                    //vprintln 5 (pi_name + sprintf ": No of deltas created=%i" (length deltas))
                    reportx 5 (sprintf  " '%s' participant deltas" pi_name) dToS deltas
                vprintln vd (sprintf ": Finished explore_env for '%s'" pi_name)
                (pi_name, deltas)
                //singly_add ((-2, [], "minustwo"), (false, ([], [], []))) deltas // Include a nop for purposes of stuttering. - not all protocols are allowed to stutter?
            let delts_ll = map explore_env all_groups_wiggles // Creates a list of triples: concretes, evictions and admissions.

// TODO nop add at end instead of start where it gets producted up.
            // Will need to check that unis_shared and unis_00 had same value! TODO
            // A synchronous product will apply all changes at once.
            // What is a stuttering product? Somewhere between an asynch and a sych where some participants move synchronously and others do not move?
            // Cartesian product of transitions, including an unclocked non-move, is considered.  
            // This is filtered immediately with only those with identical clock and shared support compatibility can rendevous and if there are any evictions there must be at least one admission and vice versa.

            // If all make an unclocked stutter we get an inevitible self edge. Reflexive. Not of interest and can be deleted.
            let stutter delts = 
                let rec stut1 = function // Not all can stutter - TODO - this code seems to be the full synchronous product of the delts_ll
                    | [(pi_name, itemset)] ->
                        //let _:(sv_, 'q list * 'q list * 'q list)  = items
                        //dev_println (sprintf "Tail item set for %s. arity=%i" pi_name (length itemset))
                        let qk = function
                            | SV_4(_, var, vale, coding) -> sprintf "%s/%s" (lifeToStr vale) (xToStr var)
                            | SV_cre _ -> "SV_cre(...)"
                        let ppos (cp_env, (clkprop, (conc, evf, adt))) = sfold qk (f2o3 cp_env)
                        //dev_println (sprintf "Tail items %s" (sfold ppos itemset))
                        itemset // need to delete sv_' or else product it properly in following clause using a fuller cp_env_unify
                    | (pi_name, itemset)::tt ->
                        let tail = stut1 tt
                        let cp_env_unify (env_no1, a1, a1s) (env_no2, b2, b2s) =
                            let _:svitem_t list = a1
                            // TODO check for any contrary pairs - will not happen if disjoint support. Its the shared support that must be checked.
                            //vprintln 0 (sprintf "cp_env_unify envs %s and %s" (sfold svToStr0 a1) (sfold svToStr0 b2))
                            (0, a1 @ b2, "_cp_env_unified_")
                        //cassert(itemset <> [], "null behaviour")
                        let pprod (cp_env, (clkprop, (coc, evf, adt))) cc =
                            let bagot (cp_env2, (clkprop_s, (cs, xs, ys))) cc =
                                //dev_println (sprintf "bagot %A %A         %A"  clkprop clkprop_s (clkprop = clkprop_s))
                                if clkprop_s <> clkprop then cc  // Only those with identical clocks can rendezvous.  What about synch to asynch join?
                                else (cp_env_unify cp_env cp_env2, (clkprop, (coc@cs, evf@xs, adt@ys)))::cc  // Cmds/Evictions/Admissions
                            List.foldBack bagot tail cc
                        let ans = List.foldBack pprod itemset [] // Fold, deleteing non-unifies on cp_env if any fail - they may all be orthogonal unless there's shared support.
                        let i_true d = (if d then 1 else 0)
                        let i_false d = (if d then 0 else 1)                        
                        let (clk_count, unclk_count) = List.fold (fun (clk_count, unclk_count) (d, (clkprop, _)) -> (clk_count+i_true clkprop, unclk_count+i_false clkprop)) (0, 0) ans
                        vprintln 3 (sprintf "Stutter product on %s counts %i x %i -> clk=%i + unclk=%i" pi_name (length tail) (length itemset) clk_count unclk_count)
                        ans
                    | [] -> sf "L2540 stut1"
                stut1 delts

            
            let futures =
                let arc_idx_add n = (n, next_arc_idx())
                map arc_idx_add (list_once(stutter delts_ll))
            let n_futures = length futures
            if vd>=3 then vprintln 3 (sprintf "Unique stuttering combs created by explore %i" n_futures)
            let ans_zl = List.fold (data_compatible ww settings keys nets svl) [] futures // data_compatible can return zero, one or many if there are alternative meshings.
            if vd>=3 then vprintln 3 (sprintf "Of which sufficiently data conserving are %i/%i" (length ans_zl) n_futures)
            mutinc m_dx_total n_futures; mutinc m_dx_conserving (length ans_zl)
#if COMMENTOUT_FOR_NOW
            if vd >=5 then
                let zfprint ((cp_env, (clkprop, (concs, evf, adt))), idx_no) =
                    let qk (var, vale, coding) = sprintf "%s/%s" (xToStr vale) (xToStr var)
                    sprintf "%i   cp_env=%s" idx_no (sfold qk (f2o3 cp_env))
                let zlprint (idx_no, clockprop_, svll, gg, cmds) =
                    //let nn = intval nets sv
                    let flag =
                        if svl = svll then "REFLEXIVE-EDGE"
                        elif (* nn<>n && *) not_nonep(lookup mainarray svl) then "CROSS/BACKEDGE" else ""
                    sprintf "idx=A%i -> %s %s : " idx_no (svToStr_full svll) (svDeltaToStr svl svll) + " G=" + concise_xbToStr gg + " cmds=" + sfold ybevToStr (map snd cmds) + flag
                    
                //reportx vd "Stuttering Successors"  zfprint futures
                reportx vd ("Stuttering Successors That Suitably Conserve Data for " + ml)  zlprint ans_zl                   

                vprintln vd (sprintf  "SX %s has %i stuttering successors." svls (length ans_zl))
                vprint vd ""
#endif            
            let gg_trim cc (arc_idx, clkprop, svll, guard, cmds) =
                //let key = intval nets sv
                // Delete completely reflexive and invalid arcs.    TODO seems brutal? reflexive arc might be good in some coding styles, although currently we dont have an pre-telescoped.
                if svll=fst svl || xi_isfalse guard then cc else (arc_idx, clkprop, svll, guard, cmds) :: cc
            let successors = List.fold gg_trim [] ans_zl

            //let vd = 5 // for now
            
            if vd>=4 then
                vprintln 4 (sprintf "SX %s has %i successors." svls (length successors)) 
                vprint 4 "" 

            let add_rank cc = function // What is the rank? It is based on state vector differences.
                                       // This also adds in the reguards for the external input unilateral changes
                    | (arc_idx, clkprop, svll, guard0, cmds0) ->
                        let concrete_only =
                            let pred = function
                              | SV_4(_, var, vale, (dir, jspec)) -> not (is_symbolic jspec.coding)
                              | SV_cre _ -> true
                            List.filter pred
                        let (cmds, grds) = groom2 (cvrt_drives_pred driven) (concrete_only(sv_subtract svll (fst svl))) // Interesting: we guard only on the deltas here --- please explain why... that's fairly fundamental.
                        let ass = if true then  "active" else "idle" // for a TLM target - Document these TLM keywords please.
                        let grdfun cc = function
                            | SV_cre _ -> cc
                            | SV_4(_, x, DL_lift y, coding) ->
                                let v = xi_deqd(x, y)
                                if xi_istrue v || xToStr y = ass then cc else v::cc
                            | _ -> muddy "bugg232"
                        let grds = List.fold grdfun [] grds
                       // We make a weighted sum heuristic.
                        let niceness = sv_delta (fst svl) svll
                        let svl' = (svll, (rez_svToStr_baseless svll, rez_svToStr_full svll))
                        let rank = -10*(length grds) + (3 * length cmds) + niceness // Add most benefit for moving data TODO
                        //if vd>=5 then vprintln 5 (sprintf "arc_idx=A%i rank = %i + %i + %i = %i" arc_idx (-10*(length grds)) (3 * length cmds) niceness rank)  // Add most benefit for moving data TODO
                        let cmdgen = function
                            | SV_4(_, var, DL_lift vale, coding) ->
                                if xToStr vale = "idle" // TLM target keyword
                                then (coding, Yreturn X_undef) // for a TLM target only
                                else (coding, Yassign(None, gec_DL_lift var, gec_DL_lift vale))
                            | _ -> muddy "gug02"
                        let ggg = (if xi_istrue guard0 then grds else guard0 :: grds)
                        if vd>=5 then vprintln 5 (sprintf " mv-> " + svToStr_full svl' + " grds=" + sfold concise_xbToStr ggg + " scmds=" + i2s(length cmds0) + " nscmds=" + i2s(length cmds) + " rank=" + i2s(rank))
                        let successor = (arc_idx, Some clkprop, svl', (cmds0 @ map cmdgen cmds, ggg, rank), ([], [], Some X_true))
                        singly_add successor cc // Singly add should remove exact duplicates.

            let successors:successor_t list = List.fold add_rank [] successors

            let uncond = List.filter (fun (_, _,  _, (cmds, grds, rank), m)->length grds = 0) successors
            if vd>=4 then
                //reportx 4 "Successors guards" (fun (arc_idx, clockprop_, _, (cmds, grds, rank), dcon_) -> (sfold xbToStr grds)) successors
                let uncondl = length uncond
                vprintln 4 ("Stop-consider: " + svToStr_full svl + (if successors = [] then " is a --DEADEND-- " else  " has " + i2s(length successors) + " distinct successors, of which " + i2s(uncondl) + " are unconditional"))
            //vprint(1, "") // cause a flush
            successors // end of consider_one_product_point

    let m_rebase_backedges = ref 0
    let rec doconsider steps  = function  // This is the main exploration function, depth-first tree exhaustively walked through the compatible product space.
        | work when steps <= 0 -> work
        | [] -> []
        | (re_point, (idx_no, grd, svl, cmds, dcon_))::tt ->
            mutinc m_pending -1
            let (found0, _) = ma_lookup_by_both mainarray svl // Lookup current point
            let newwork = 
                if nonep found0 then
                    let svls = svToStr_full svl                
                    let successors = consider_one_product_point re_point (idx_no, grd, svl, cmds, dcon_)
                    let analyse_successor (cc, newwork) successor =
                        let (arc_idx, grd, svl, cmds, dcon) = successor
                        let backtok =
                            match mine_backtok (hd(fst svl)) with
                                | backtok -> backtok
                        let (found, related) = ma_lookup_by_both mainarray svl // Lookup facilitates rebased lookup
                        if not_nonep found then // A straightforward backedge
                             vprintln 3 ("  doconsider - reached node visited already: straightforward backedge " + svls)
                             if vd>=5 then vprintln 5 ("  doconsider - reached node visited already: " + svls)
                             (successor::cc, newwork) // Does not need to be looked at again
                        else
                            let rebased_candidates =
                                if not_nullp related then dev_println(sprintf " doconsider A%i - rebase: (re_point=%A backtok=%A) related to %s are %s" arc_idx re_point backtok (svToStr svl) (sfoldcr_lite svToStr related))
                                map (compute_rebase_vector re_point svl) related
                            let rebasers = List.filter f1o3 rebased_candidates // find any suitable alternative ... TODO use a metric and prefer one

                            if not_nullp rebasers then
                                let goto = hd rebasers  // TODO ignores remainer! This would be ok if sorted into preference order.
                                if vd>=4 then vprintln 4 (sprintf "  doconsider A%i - create rebase back edge from %s to %s" arc_idx svls (svToStr_full (f3o3 goto)))
                                mutinc m_rebase_backedges 1
                                let edit (arc_idx, xx, svl, yy, dcon) = (arc_idx, xx, f3o3 goto, yy, dcon)
                                ((edit successor)::cc, newwork)
                            else
                                vprintln 3 (sprintf "  doconsider - node is new: (re_point=%A backtok=%A) (rebasers=%i) %s " re_point backtok (length related) svls)
                                (successor::cc, successor::newwork) // Save to mainarray and also add to newwork for further exploration


                    let (successors, newwork) = List.fold analyse_successor ([], []) successors
                    ma_insert mainarray svl successors // Back edges are important so all arcs saved, but not all are new ground.
                    let kk x = if not_nonep(ma_lookup_suc mainarray x) then "P" else " " // P flag for virgin ground?
                    //app (fun (sv, (c,g,rank), _) -> vprintln 3 (kk isv + ":" +  ":" + svToStr1 sv)) successors
                    app (fun (arc_idx, clockprop_, sv, (cmds, grds, rank), _) -> vprintln 3 (sprintf " succ1: idx=A%i rank=%i   %s" arc_idx rank (svToStr_full sv))) successors            
                    newwork
                else [] // Done already
            mutinc m_pending (length newwork)
            dev_println (sprintf "rebased back edges = %i" !m_rebase_backedges)
            doconsider (steps-1) (tt @ map (fun x -> (re_point, x)) newwork) // Breadthfirst. ? What order. A*-like search on good looking ones?

// Need to run more batches if not live or not hitting all fairpoints.

    let ans = doconsider batchsize work
    progress_report "At end of batch"
    ans

let add_tau_transition (mainarray:mainarray_t) from_s to_s =
    let _:(back_address_t * sv_t) = from_s
    let svl = snd from_s
    let suc = (-2, Some false(*synchf*), (snd to_s), ([], [], 0), ([], [], None))
    ma_insert mainarray svl [suc]
    
let form_form_prod ww settings nets all_groups_wiggles origin_state primed_state driven =         
    let ww = WF 2 "form_form_product" ww (sprintf "Start at the primed state=%s for product construction" (fst primed_state))
    let unisymb_cache = unisymb_cache_t() // Another cache of possible wiggles?
    let vd = settings.prod_vd
    vprintln 2 (sprintf "form_form_product vd=%i" vd)
    vprintln 2 (sprintf "Insert origin and its transition to primed.")    
    let mainarray:mainarray_t = new mainarray_t("mainarray")
    let _:(back_address_t * sv_t) = primed_state
    vprintln 2 (sprintf "Insert origin and its transition to primed state.")    
    add_tau_transition mainarray origin_state primed_state
    //let is0 = start_state 
    let m_pending = ref 1 // The initial start point
    let (m_considered, m_dx_total, m_dx_conserving, m_pending) = (ref 0, ref 0, ref 0, m_pending) // Stats

    dev_println "evergreen values must not identify - throw spanner in if overwriting a live value "

    let work = [(fst primed_state, (-1, None, snd primed_state, ([], [], 0), ([], [], (Some X_true))))]

    let batchsize = 1000


    let rec batch_iterater bn work =
        let work = product_runbatch ww settings (m_considered, m_dx_total, m_dx_conserving, m_pending) mainarray driven nets all_groups_wiggles work batchsize
        if nullp work then
            let mx = (sprintf "Finished product construction (after considering %i states).\n%i arcs considered of which %i were data conserving." !m_considered !m_dx_total !m_dx_conserving)
            let ww = WF 2 "form_form_product" ww mx
            work
        elif bn>3 then
            vprintln 1 (sprintf "Depth temp stop with %i pending" (length work))
            work
        else
            vprintln 1 (sprintf "End of batch %i with %i pending" bn (length work))
            batch_iterater (bn+1) work


    let ww = WF 2 "form_form_product" ww "Start"
    let _ = batch_iterater 1 work
    let ww = WF 2 "form_form_product" ww "All batches run"
    mutadd settings.m_states_report_lines "All batches run"
    mainarray //  End of form_form_product




type lmap_t = Map<sv_t, sv_t list>

let get_inverse_arcs ww  (mainarray:mainarray_t) srclst =
    //let m_done_set = new HashSet<sv_t>() // Mutable - dont like
    let inverse_find (dones:Set<sv_t>, cc) nx =
        if Set.contains nx dones then (dones, cc)
        else
            match ma_lookup_suc mainarray nx with
                | Some ov ->
                    let cc = List.fold (fun cc (_, _, svl, _, _) -> singly_add (nx, svl) cc) cc ov
                    (Set.add nx dones, cc)
                | None -> (dones, cc)
    let (nodes, fwd_arcs) = List.fold inverse_find (Set.empty, []) srclst
    dev_println(sprintf "get_inverse_arcs: processing: find  nodes=%i arcs=%i" (Set.count nodes) (length fwd_arcs))

    let inverse_arcs =
        let flip cc (s, d) = singly_add (d, s) cc
        List.fold flip [] fwd_arcs

    dev_println(sprintf "get_invers_arcs: inverse_find  srclist^=%i  nodes=^%i  fwdarcs=^%i  inverse_arcs=^%i" (length srclst) (Set.count nodes) (length fwd_arcs) (length inverse_arcs))

    let gmap (mm:lmap_t) (s, d) =
        let ov = valOf_or_nil(mm.TryFind s)
        mm.Add(s, singly_add d ov)
    let fwd_arcs = List.fold gmap Map.empty fwd_arcs
    let inverse_arcs = List.fold gmap Map.empty inverse_arcs
    (nodes, fwd_arcs, inverse_arcs)


// We trim to live paths by retaining states that have successors in the previous round.
// To isolate the lasso stem we do it on the reverse arcs
let live_trim ww (mainarray:mainarray_t) report srclst = 
    let ww = WF 2 "jinjoin:live_trim" ww "precompute"
    let (nodes, fwd_arcs, inverse_arcs) = get_inverse_arcs ww  (mainarray:mainarray_t) srclst
    let ww = WF 2 "jinjoin:live_trim" ww "start"

    let srcset = Set.ofList srclst
    let rec live_trim msg (mm:lmap_t) (inl:Set<sv_t>) =
        let check n =
            let dests = valOf_or_nil(mm.TryFind n)
            let rec check1 = function
                | [] -> false
                | h::tt when Set.contains h inl -> true
                | _::tt -> check1 tt
            check1 dests
        let outl = Set.filter check inl
        let inll = Set.count inl
        report(sprintf "Live trim: %s %i states in the running." msg inll)
        let changed = (inll <> Set.count outl)
        if changed then live_trim msg mm outl else inl

    if false then
        let alst1 = live_trim "first" fwd_arcs nodes
        let alst2 = live_trim "second" inverse_arcs alst1
        reportx 2 "Lasso delete" svToStr_full (Set.toList (Set.difference alst1 alst2)) // intersect also works
        alst2
    else
        let ww = WF 2 "jinjoin:live_trim" ww "old start"
        let rec live_trim inl = // Lists give quadratic cost here !  Use Set
            let check n =
                match ma_lookup_suc mainarray n with
                    | Some ov -> List.fold (fun c (_, _, svl, _, _) -> c || Set.contains svl inl) false ov
                    | None    -> false
            let outl = Set.filter check inl
            let inll = Set.count inl
            report("Old Live trim: " + i2s(inll) + " states in the running.")
            let changed = (inll <> Set.count outl)
            if changed then live_trim outl else inl
        let alst = live_trim srcset
        alst

                

// Find all paths that do not loop and which start and end at an idle state.
// The number of paths is exponential in general.  Any articulation points help reduce the reporting complexity.
// Reporting them is a useful side effect for debugging purposes.
// (Could this be essentially phrased as a fairness problem?)
// This returns its result in allowed_path_set.
let run_vbs ww nets idle_pred start_state mainarray live = 
    let ww = WF 2 "run_vbs" ww "Start"

    // Allowed_path_set maps a state to a ... and is used to supress certain (which) routes in the final machine.
    let allowed_path_set = Dictionary<string, string list>()
    let m_paths_found = ref 0

    let allowed_path_set_cons k nv = // This is a ListStore<string, string>
        let (found, ov) = allowed_path_set.TryGetValue k
        if found then (ignore(allowed_path_set.Remove k); allowed_path_set.Add(k, nv :: ov))
        else allowed_path_set.Add(k, [nv])
    let m_uzl_final = ref []

    let logp path =
        mutinc m_paths_found 1
        vprintln 0 ("logged back path length=" + i2s(length path))
        let rec pins = function
            | [] -> ()
            | [item] -> ()            
            | h::t::tt ->                
                 (
                        allowed_path_set_cons h t // This is liststore.Add
                        pins (t::tt)
                 )
        pins (map svToStr_full path)
        m_uzl_final := lst_union (list_once path) !m_uzl_final
        ()

    let idlep(sv) = xi_istrue(beval nets (fst sv) idle_pred)
    let stackreport msg svp st =
        let gr svp = (if idlep svp then "!IDLE!" else "") + (svToStr_full svp) + ":" + svToStr_full svp + "  "
        report(sprintf "Examined %s " msg + sfold gr (st))
        //report(sprintf "Examined %s summary " msg + sfold (hd>>pcOnlyToStr) (st)) 
        report(svToStr_full svp)
        //let ml = mainlab svl
        //report ("to idlep ?" + boolToStr i + "\n\n")

    let live_set =
        let live_set = new HashSet<sv_t>() // Mutable
        app (fun svp -> ignore(live_set.Add svp)) live
        live_set
    // We have many trimmed successors still stored in mainarray, so again chop them off here (better to regen mainarray perhaps)
    let trim_again (idx_no, clockinfo, svp, cgr, dcon) cc = if live_set.Contains svp then (idx_no, clockinfo, svp, cgr, dcon)::cc else cc

    let checked_array = new OptionStore<string, sv_t list list>("checked array") 
    let _:allowed_path_set_t = allowed_path_set
    let rec valbacks st bodyf of_interest svl cc =
        let report = vprintln 3
        //if not (memberp svl live) then vprintln 3 (svToStr2 svl + " was not in live set")
        let i = idlep svl
        let st' = svl::st
        if i && bodyf then
            logp (rev st')
            stackreport "closedpath-to-idle" svl (rev st')
            [svl]::cc // Expect cc to be null.
        else
        let keys = svToStr_full svl
#if SPARE
        let of_interest = of_interest // || (hd>>pcOnlyToStr) svl = "4.0."
        if of_interest then stackreport "interesting-path" svl (rev st')
#endif
        match checked_array.lookup keys with
            | Some continuations when nullp continuations -> cc
            | Some continuations ->
                let answers = map (fun x -> rev st' @ x) continuations
                app (stackreport "closedpath-mitre" svl) answers
                answers @ cc
            | None ->
                // Some ov -> fst(allowed_path_set.TryGetValue keys) then logp(rev st')
                let backedge = memberp svl st
                if (backedge) then stackreport "misc-other-backedge" svl (rev st')
                // TODO there are others to log
                if (backedge && i) then (logp(rev st'); report("Found idlep backedge: " + sfold svToStr_full (rev st')))
                if backedge then cc
                else 
                    //let ml = mainlab svl                     if (ml = "0 0 ") then vprintln 3 ("AN ORIGIN " + svToStr2 svl)
                    //vprintln 3 ("idleback test=" + boolToStr i + " " + ml  + ":" + svToStr2 svl)

                    let dest_lst =
                        match ma_lookup_suc mainarray svl with
                            | None -> []
                            | Some dest_lst ->
                                //if of_interest then dev_println(sprintf "of interest             %s" (sfold (f3o5>>svToStr_full) dest_lst))
                                List.foldBack trim_again dest_lst []
                    //if of_interest then dev_println(sprintf "of interest filtered %s" (sfold (f3o5>>svToStr2) dest_lst))
                    let completions =
                        let sq cc (_, _, svp, _, _) =
                            let ans = valbacks st' true of_interest svp cc
                            ans
                        List.fold sq [] dest_lst
                    checked_array.add keys completions
                    if nullp completions then cc
                    else
                        (map (fun x->svl::x) completions)@cc
    ignore(valbacks [] false false start_state [])
    vprintln 0 (sprintf "vbs: %i paths back to idle states found." !m_paths_found)
    if !m_paths_found = 0 then vprintln 0 "+++ NO IDLE PASSING  PATHS FOUND +++"
    if (false) then cassert (!m_paths_found > 0, "No idle passing paths found")
    let glue_ans = !m_uzl_final//for dot output only
    vprintln 0 (sprintf  "Allowed_path_set arity=%i.  States in vbs glue is %i" (allowed_path_set.Count) (length glue_ans)) 
    let ww = WF 2 "run_vbs" ww "Finished"
    let allowed_path_support = glue_ans
    (allowed_path_set, allowed_path_support, glue_ans) // end of run_vbs

    // TODO allowed_path_set is computed and then disregarded?


// For synchronous logic output, there should be no loops of unclocked edges.
// Certain product points are clocked points, meaning that at least one of the incoming edges is synchronous.  But such points can also be part of an asynchronous
// trajectory.      
let telescope_unclocked ww settings (mainarray:mainarray_t) report start_state live = 
    let ww = WF 2 "telescope_unclocked" ww (sprintf "Start.  live svps = %i" (Set.count live))
    let _:Set<sv_t> = live
    // We have many trimmed successors still stored in mainarray, so again chop them off here (better to regen mainarray perhaps)
    let trim_again (idx_no, clockinfo, svp, cgr, dcon) cc = if Set.contains svp live then (idx_no, clockinfo, svp, cgr, dcon)::cc else cc

#if OLD
    let clocked_dbase = new OptionStore<sv_t, bool>("clocked_dbase")
    let _ =
        let check_set = new HashSet<sv_t>()
        let (m_clocked, m_unclocked) = (ref 0, ref 0)
        let rec clocked_classify svp =
            if check_set.Contains svp then ()
            else
                let _ = check_set.Add svp
                let dest_checker (clockprops, svp1, _, _) =
                    let vale = valOf_or_fail "L2011" clockprops
                    match clocked_dbase.lookup svp1 with
                        | None ->
                            mutinc (if vale then m_clocked else m_unclocked) 1
                            clocked_dbase.add svp1 vale
                        | Some ov ->
                            if vale = ov then ()
                            else sf (sprintf "Cannot cope with mixed clocks leading (from %s) to %s " (svToStr svp) (svToStr svp1))
                match ma_lookup_suc mainarray svp with
                    | Some dest_lst ->
                        let dest_lst = List.foldBack trim_again !dest_lst []
                        app dest_checker dest_lst
                    | None          -> ()
        app clocked_classify live
        report(sprintf "telescope_unclocked: %i clocked dests and %i unclocked dests out of %i.  check_set.Count=%i" !m_clocked !m_unclocked (!m_clocked + !m_unclocked) check_set.Count)
        () // Answer in clocked_dbase

    reportx 3 "Live Set" (fun svp -> svToStr2 svp + sprintf "  clocked=%A" (clocked_dbase.lookup svp)) live

    let compress_path cc (kv:KeyValuePair<sv_t,bool>) =
        if not kv.Value then cc // Ignore unclocked origins - (watch out for entry point though?)
        else
            let origin = kv.Key
            let rec mypath stack svp =
                if memberp svp stack then muddy "Unclocked loop unwinder. TODO report the loop."
                match ma_lookup_suc mainarray svp with
                    | None          ->
                        sf (sprintf "1/2 hit dead end - could not have been a live path. origin=%s  svp=%s" (svToStr origin) (svToStr svp))
                        []
                    | Some dest_lst ->
                        let dest_lst = List.foldBack trim_again !dest_lst []
                        if nullp dest_lst then hpr_yikes (sprintf "2/2 hit dead end - could not have been a live path. origin=%s  svp=%s" (svToStr origin) (svToStr svp))
                        let shan (clockinfo, svp1, cgr, dcon) =
                                match clocked_dbase.lookup svp1 with
                                    | Some true -> [(clockinfo, svp1, cgr, dcon)] // Terminate at another clocked point
                                    | Some false ->
                                        let suffixes = mypath (svp::stack) svp1 
                                        list_flatten (map (fun x -> (clockinfo, svp1, cgr, dcon) :: x) suffixes) // Augment suffixes with here.
                                    | None ->
                                        sf(sprintf "missing L2060 for %s" (svToStr_full svp1))
                        map shan dest_lst

            let paths = mypath [] origin
            vprintln 3 (sprintf "telescope from %s has %i potential paths" (svToStr_full kv.Key) (length paths))
            if nullp paths then cc else (origin, paths) :: cc

    vprintln 3 (sprintf "OLD: telescope_unclocked: clocked base Count=%i" clocked_dbase.Count)
    let telescoped_paths = clocked_dbase |> Seq.fold compress_path []
    // END OF OLD 
#endif
    let done_set = new HashSet<sv_t>()

    
    let m_tp_idx = ref 10 // Start index at ten.
    let next_idx () =
        let ans = !m_tp_idx
        mutinc m_tp_idx 1
        ans
        
    let rec compute_telescoped_paths cc = function
        | [] -> cc
        | svp::tt when done_set.Contains svp -> compute_telescoped_paths cc tt
        | start_svp::tt ->
            ignore(done_set.Add start_svp)
            let rec mypath tt stack svp =
                match ma_lookup_suc mainarray svp with
                    | None          ->
                        sf (sprintf "1/2 hit dead end - could not have been a live path. origin=%s  svp=%s" (svToStr start_svp) (svToStr svp))
                        (tt, [])
                    | Some dest_lst ->
                        let dest_lst = List.foldBack trim_again dest_lst []
                        if nullp dest_lst then hpr_yikes (sprintf "2/2 hit dead end - could not have been a live path. origin=%s  svp=%s" (svToStr start_svp) (svToStr svp))
                        let rec shan tt cxc = function
                            | [] -> (tt, cxc)
                            | (arc_idx, clockinfo, svp1, cgr, dcon)::tx ->
                                let stopf = clockinfo = Some true
                                if memberp svp stack then muddy ("An unclocked loop unwinder is needed")
                                let (tt, ans) = 
                                    if stopf then
                                        //mutinc m_tp_idx 1
                                        (singly_add svp1 tt, [(arc_idx, clockinfo, svp1, cgr, dcon)]::cxc) // Terminate after a clocked edge and add that as a further dest to tt worklist.
                                    else
                                        let (tt, suffixes) = mypath tt (svp::stack) svp1 
                                        let suffixes = (map (fun x -> (arc_idx, clockinfo, svp1, cgr, dcon) :: x) suffixes)
                                        (tt, suffixes@cxc)
                                shan tt ans tx
                        let (tt, ans) = shan tt [] dest_lst

                        (tt, ans)
            let (tt, paths) = mypath tt [] start_svp
            let paths = map (fun p -> (next_idx(), p)) paths // // Add another form of index, for the telescoped path instead of the arc. 
            compute_telescoped_paths ((start_svp, paths)::cc) tt

    let telescoped_paths = compute_telescoped_paths [] [start_state]
    vprintln 3 (sprintf "telescope_unclocked: %i telplogs" (length telescoped_paths))



    let telplogToStr vbf (origin, ppno, pathsteps) =
        let reflexivef = not_nullp pathsteps && f3o5(hd(rev pathsteps)) = origin
        let yf = (if vbf then svToStr_full else fst>>hd>>pcOnlyToStr) 
        let successor_bos svp (code, gg, r) =
            //let (_, _, dc1:hbexp_t option ) = dcon
            let codeToStr((netdir, net_spec), ybev) = ybevToStr ybev
            let dcons = sfold codeToStr code
            (if nullp gg then "" else sfold concise_xbToStr gg + "/" ) + yf svp + (if vbf then "$(" + dcons + "$)" else "")

        sprintf "TP%i %s " ppno (yf origin) +  sprintf "%s  : %s" (if reflexivef then "R" else " ") (sfold (fun (idx1, clk, svp, cgr, dcon) -> successor_bos svp cgr) pathsteps)


    let gec_rank r0 (idx_no, clockprop, svp, (cmds, grd, rank), dcon) = r0 * (max rank 1)
    let gec_guard cg (idx_no, clockprop, svp, (cmds, grd, rank), dcon) = ix_andl (cg::grd)
   

    let _ =  // Flatten for temp listing...
        let reformed = list_flatten(map (fun (origin, paths) ->map (fun (ppno, p) -> (origin, ppno, p)) paths) telescoped_paths)
        let valid (origin, ppno, steps) =
            let composite_grd = List.fold gec_guard X_true steps
            (xbmonkey composite_grd <> Some false)
        let valid_plogs = List.filter valid reformed
        vprintln 2 (sprintf "%i/%i paths are valid" (length valid_plogs) (length reformed))
        reportx 2 "Valid Telplogs" (telplogToStr false) valid_plogs
        reportx 2 "Valid Telplogs/verbose" (telplogToStr true) valid_plogs        
        let tpca (origin, ppno, pathsteps) =
            sprintf "  TP%i arcs=" ppno + sfold (f1o5>>i2s) pathsteps
        reportx 2 "Valid Telplogs Component Arcs" tpca valid_plogs

    let telescoped_paths =
        let compress (origin, arcs) cc =
            let telescope_path (ppno, path) cc = 
                if nullp path then
                    hpr_yikes (sprintf "Null path from %s ignored for now!" (svToStr_full origin))
                    cc
                else
                let composite_grd = List.fold gec_guard X_true path
                if xbmonkey composite_grd = Some false then
                    //vprintln 3 (telplogToStr false (origin, ppno, path) + sprintf " Remove invalid path %s" (sfold (f4o5>>f2o3>>sfold xbToStr) path))
                    cc
                else
                    let composite_rank = List.fold gec_rank 1 path
                    let dest = f3o5(hd(rev path))
                    let reflexivef = (dest = origin)
                    if reflexivef && false then // Why would we delete them?
                        vprintln 3 (sprintf "Discard reflexive path from %s to %s has guard %s" (svToStr origin) (svToStr dest) (xbToStr composite_grd))
                        cc
                    else
                        if (reflexivef) then vprintln 3 (sprintf "Keep reflexive path from %s to %s has guard %s" (svToStr origin) (svToStr dest) (xbToStr composite_grd))
                        vprintln 3 (sprintf "Consolidate path from %s to %s has guard %s" (svToStr origin) (svToStr dest) (xbToStr composite_grd))
                        let composite_cmds = list_flatten (map (f4o5>>f1o3) path) // Need to give precedence to latter commands, WaW style.
                        let composite_control = (composite_cmds, [composite_grd], composite_rank) // 
                        let composite_dcon = map f5o5 path// (list_flatten(map (f4o4>>f1o3) path), list_flatten(map (f4o4>>f2o3) path), list_flatten(map (f4o4>>f3o3) path))
                        (ppno, f2o5 (hd path), dest, composite_control, composite_dcon)::cc
            let newarcs = List.foldBack telescope_path arcs []
            vprintln 3 (sprintf "telescope from %s has %i valid paths" (svToStr_full origin) (length newarcs))
            if nullp newarcs then
                vprintln 2 (sprintf "Removed all tplogs from %s and so not including it" (svToStr origin))
                cc
            else (origin, newarcs)::cc
        List.foldBack compress telescoped_paths []

    let msg0 = sprintf "telescope_unclocked: %i telplogs after deleting empty and invalid paths." (length telescoped_paths)
    vprintln 1 (msg0)
    if nullp telescoped_paths then local_cleanexit ww settings msg0
    let ww = WF 2 "telescope_unclocked" ww "Finished"
    telescoped_paths // end of telescope_unclocked




// Double-check an intermediate or final result for liveness etc
let check_still_live ww report title op =
    let ww = WF 2 ("check_still_live") ww ("start")
    let route_matrix = new route_matrix_t<sv_t>("route_matrix")

    let csl_insert (origin, paths) =
        let rec cslu origin = function
            | [] -> ()
            | (_, clockprops, dest_svl, _, dcon)::tt ->
                route_matrix.add origin dest_svl
                cslu dest_svl tt
        cslu origin paths
    app csl_insert op
    let _ = generic_live_trim ww route_matrix report 
    let ww = WF 2 ("check_still_live") ww ("finished")
    ()

type conservation_predicate_t = (hbexp_t * hexp_t) 


let gec_demux_canned_2_1_and_ser () = 
    let in_data = gec_X_net "left.ddata"      // 32 bits
    let out_data0 = gec_X_net "right0.ddata0" // 32 bits
    let out_data1 = gec_X_net "right1.ddata1" // 16 bits
    let o_data0 = (out_data0)
    let o_data1 = (out_data1)
    let thinwidth = 16
    //let muxpred = 
    let de = ix_bitor (ix_lshift (o_data0) (xi_num thinwidth)) (o_data1) // For now.
    // Not Correct -- canned in canned product for now
    [ (X_true, q_deqd in_data de) ]

let gec_demux_canned_2_1 () = 
    let in_data = gec_X_net "inside.ddata"
    let out_data0 = gec_X_net "outside.ddata0"
    let out_data1 = gec_X_net "outside.ddata1"    
    let o_data0 = (out_data0)
    let o_data1 = (out_data1)
    // Not Correct
    let thinwidth = 32
    let de = ix_bitor (ix_lshift (o_data0) (xi_num thinwidth)) (o_data1) // For now.
    [ (X_true, q_deqd in_data de) ]



let gec_serdes_canned2 thinwidth = // One side of, re-hydrated and re-arranged.   (data_guard, lhs) (data_guard, rhs)
    let in_data = gec_X_net "inside.ddata"
    let out_data = gec_X_net "outside.ddata"
    let o_data0 = (out_data)
    let o_data1 = (xi_X -1 (out_data))
    let de = ix_bitor (ix_lshift (o_data0) (xi_num thinwidth)) (o_data1) // For now.
    [ (X_true, q_deqd in_data de) ]

let gec_serdes_canned4 thinwidth = // One side of, re-hydrated and re-arranged.   (data_guard, lhs) (data_guard, rhs)
    let left_data = gec_X_net "left.ddata"
    let right_data = gec_X_net "right.ddata"

    let i_data0 = (right_data)
    let i_data1 = (xi_X -1 (right_data))
    let i_data2 = (xi_X -2 (right_data))
    let i_data3 = (xi_X -3 (right_data))        
    let ce = ix_bitor (ix_lshift (i_data0) (xi_num thinwidth)) (i_data1)
    let ce = ix_bitor (ix_lshift ce (xi_num thinwidth)) (i_data2)
    let ce = ix_bitor (ix_lshift ce (xi_num thinwidth)) (i_data3)                 
    [ (X_true, q_deqd left_data ce) ]


let direct_gen_stdynch ww token flip width =
    let jspec0 =
        {
            commandedf=   false
            anonj=        true
            evergreenf=   false
            coding=       gen_Symbolic_bitvec "" true 0
        }

    let abstraction_name = "std_synch_cannedL2899"
    let kind = abstraction_name
    let iname = token
    let d_in  = (if flip then OUTPUT else INPUT)
    let d_out = (if flip then INPUT else OUTPUT)
    let ats = []
    let valid =
        let suffix = "_valid"
        let jspec = { jspec0 with   coding= gen_Symbolic_bitvec "" true width }
        let dir = Ndi d_out
        (xgen_bnet(iogen_serf_ff(token+suffix, [], -1I, 1, d_out, Unsigned, [], false, ats, [])), dir, false, jspec)
    let rdy =
        let suffix = "_rdy"
        let dir = Ndi d_in
        let jspec = { jspec0 with   coding=concrete_bool() }
        (xgen_bnet(iogen_serf_ff(token+suffix, [], -1I, 1, d_in, Unsigned, [], false, ats, [])), dir, false, jspec)

    let data =
        let suffix = "_data"
        let jspec = { jspec0 with   coding= gen_Symbolic_bitvec "" true width }
        let dir = Ndi d_out
        (xgen_bnet(iogen_serf_ff(token+suffix, [], -1I, width, d_out, Unsigned, [], false, ats, [])), dir, false, jspec)

    let formals = [valid; rdy; data]

    let metaprams = [] // for now        
    let local_decls =
        let wrap (nn, netdir, commandedf_, dc) = DB_leaf(None, Some nn)
        DB_group({ g_null_db_metainfo with kind=abstraction_name; pi_name=iname; not_to_be_trimmed=true; form=DB_form_external; metaprams=metaprams }, map wrap formals)
        //DB_group({ g_null_db_metainfo with kind="internal_nets" }, map wrap locals)

    let participant =
       { kind=         kind 
         aliasnames=   []
         states=       []
         pi_name=      iname
         databus=      Some data
         Pc=           None
         pnets=        formals
         Idle_pred=    xi_not(ix_and (xi_orred (f1o4 rdy)) (xi_orred(f1o4 valid)))
         Initial_pred= [(f1o4 valid, xi_zero); (f1o4 rdy, xi_zero)]
         arcs=         [] // old arcs ... please return new allowable xitions.

       }
    (participant, local_decls, formals, [], [])






// Data conserve -
//to serialise we operate on the lowest occuring index, replacing it with dead.
// To deserialise, we need named place holders that become live in order.    
// We may not want to bump - 
//



let tagzilToStr (liveset, exp) =
    let livesetToStr (x, status) =
        let ls1 (nn, flag) = sprintf "%i=%A" nn flag
        xToStr x + "lives=" + (sfold ls1 status)
    "TAGZIL:" + xToStr exp + (sfoldcr_lite livesetToStr liveset)
    

let dcx_as_bumper msg killf which_ = function // direct hack for now but in future not used or else needs to know which var to bump and which way
    | DCX_setget(conc, (liveset, exp)) ->
        dev_println(sprintf "dcx_as_bumper: trace: killf=%A on %s" killf (tagzilToStr (liveset, exp)))
        if length liveset <> 1 then sf(sprintf "dcx_as_bumper NOT ONE! killf=%A on %s" killf (tagzilToStr (liveset, exp)))
        else
            let rec alterone1 = function
                | [] -> sf(sprintf "killf=%A Could not find one to alter for " killf + xToStr exp)
                | (nn, ss)::tt when ss <> killf -> (nn, killf) :: tt
                | h::tt                         -> h :: alterone1 tt
            let alterone2 (x, status) = (x, alterone1 status)
            DCX_setget(conc, ([alterone2 (hd liveset)], exp))
            //sf(sprintf "dcx_as_bumper killf=%A on %s" killf (sfoldcr_lite livesetToStr liveset))        
            // TODO return the one updated for noting in the DCX_as central record
    
    | other -> sf(sprintf "dcx_as_bumper other %A" other)

// Support arbitrary interleaves and invertible functions and re-arrangements.  Invertible functions include negation and bit field insert/extract. /catenation form preferred but support for shift and masks.

// Evict and admits get paired to form a dconc_t
type dc_trace_t = string // for now
type dconc_t = // replaces parer_t
    | DC_move of dc_trace_t * hbexp_t * jspec_t * (string * dcx_t) * (string * dcx_t)
    | DC_evict of jspec_t    
    | DC_admit of jspec_t
    

let gec_holder ww settings width prefix =
    muddy "not being used"
    let kind = "canned_holder"
    let iname = prefix
    let holder_net =
        let ats = []
        xgen_bnet(iogen_serf_ff(prefix, [], -1I, width, LOCAL, Unsigned, [], false, ats, []))
    let ga pname = get_next_arc_no pname 
    let holder_state = newnet_with_prec g_bool_prec (prefix + "_livef")
    let pc = holder_state
    let coding =
        {
            commandedf=   false
            anonj=        true
            evergreenf=   false
            coding=       gen_Symbolic_bitvec "" false width   // This is where we want the serdes variant >H
        }

    let (s0, s1) = (g_deadval, g_fullval)
    let holder_states = [ s0; s1 ]    
    // GL, SL, SD, GD, L2L
    let synchf = true
    let holder_allowable_xitions =
        [
            // Two arcs when dead: stay dead (SD) and go live (GL)
            (ga "GL", synchf, s0, X_true, [ DC_admit(coding) ], [s1])
            (ga "SD", synchf, s0, X_true, [], [s0]) // This could be implied
          
            // Three arcs when live: stay live (SL), live-to-live (L2L) and go dead (GD)
            (ga "SL",  synchf, s1, X_true, [], [s1]) // This also could be implied
            (ga "L2L", synchf, s1, X_true, [DC_admit coding; DC_evict coding], [s1])
            (ga "GD",  synchf, s1, X_true, [DC_evict coding],                  [s0])
        ] // replaces earlier set of participant arcs
    let netdir = Ndi LOCAL
    let databus = (holder_net, netdir, false, coding)
    let holder_nets = [ databus ]
    let metaprams = [] // for now        
    let holder_decls =
        let wrap (nn, netdir, commandedf_, dc) = DB_leaf(None, Some nn)
        //DB_group({ g_null_db_metainfo with kind=abstraction_name; pi_name=pi_name; not_to_be_trimmed=true; form=DB_form_external; metaprams=metaprams }, map wrap formals)
        DB_group({ g_null_db_metainfo with kind="internal_nets" }, map wrap holder_nets)

    let idlep = ix_deqd s0 pc
    let participant =  // This is too high level in a sense.  We dont want shannon decomposition onto a named PC in the future - we should flatten to any number of constrainted-trajectory suttering/synch/asynch enumerations.
       { kind=         kind 
         aliasnames=   []
         databus=      Some databus
         states=       [s0; s1]
         pi_name=      iname
         Pc=           Some pc
         pnets=        holder_nets
         Idle_pred=    idlep
         Initial_pred= [(s0, pc)]
         arcs=         [] // old arcs ... please return new allowable xitions.
       }

    (participant, holder_decls, holder_nets, holder_state, holder_states, holder_allowable_xitions)     


// Fake out a product of two interfaces with one holder. Canned products.
let directgen ww settings namer equations participants =
    // 
    let cb =
          {
            commandedf=   false
            anonj=        true
            evergreenf=   false
            coding=       concrete_bool()
          }

    let _:(hbexp_t * dcexp_t) list = equations
    
    // The names left and right are the default iname monicas
    let iname_in = "left"
    let iname_out = "rightside"

    let conservation_exp =
        if length equations = 1 then hd equations // for now
        else sf "L3178"

    let rec mine_part msg ss = function
        | []    -> sf (sprintf "directgen: %s: '%s' Cannot find participant %s" msg namer ss)
        | h::t when h.pi_name = ss -> h
        | h::t when memberp ss h.aliasnames -> h        
        | _::tt -> mine_part msg ss tt

    let iname_holder = "areg_one"
    let part_holder = mine_part "genx-areg" iname_holder participants
    
    let mine_net msg partname netname =
        let part = mine_part msg partname participants
        let rec mine_net = function
            | []    ->
                vprintln 0 (sprintf "Candidate nets are " + sfold (f1o4>>xToStr) part.pnets)
                sf (sprintf "Cannot find net %s in participant %s " netname partname)
            | h::tt when xToStr(f1o4 h).IndexOf netname >= 0 -> h // Somewhat loose!
            | _::tt -> mine_net tt
        mine_net part.pnets
            
    //let (_, in_decls, _, in_x1, in_x2) = direct_gen_stdynch ww wotter.intoken true  wotter.in_width
    //let (_, out_decls, _, in_x1, in_x2) = direct_gen_stdynch ww wotter.outtoken false  wotter.out_width
    //let (part_holder, holder_decls, [holder_net], holder_state, holder_states, holder_allowable_xitions) = gec_holder ww settings mwidth iname_holder
        

    let set_databus msg dbusname part =
        let net = mine_net msg part.pi_name dbusname
        let w = valOf_or_fail "databus width" (ewidth "databus width" (f1o4 net)) // Can also get from its coding field.
        dev_println(sprintf "set_databus: %s: for %i %s %s"  msg w part.pi_name dbusname)
        ({ part with databus= Some net }, w)

    let part_left = mine_part "genx" iname_in  participants
    let (part_left, in_width) = set_databus "genx" "ddata" part_left
    let (part_holder,_) = set_databus "genx" "r_shr" part_holder
    
    //let is_live net = xi_blift(ix_deqd net g_liveval)
    //let is_dead net = xi_blift(xi_not(ix_deqd net g_liveval))   

    let nets = list_flatten (map (fun p -> p.pnets) participants) 
    // Counter will be in the product and not a resource for the serdes examples
    // Directly 'can' the product for this example
    let s0 = xi_string "state_s0"
    let s1 = xi_string "state_s1"
    let s2 = xi_string "state_s2"
    let s3 = xi_string "state_s3"
    let s4 = xi_string "state_s4"

    //let ratio = max (in_width / out_width) (out_width / in_width)



    let gec_DC_move(coding, dest, src) = DC_move("serdes-1", X_true, coding, dest, src)

    // DC_move has dest first semantics.
    let cp_demux2 burstf =
        let iname_out0 = "port0"
        let iname_out1 = "port1"
        let part_out0 = mine_part  "cp_demux2" iname_out0  participants        
        let (part_out0, out_width) = set_databus "cp_demux2" "ddata" part_out0
        let part_out1 = mine_part "cp_demux2" iname_out1  participants        
        let (part_out1, out_width) = set_databus "cp_demux2" "ddata" part_out1

        let in_v = mine_net    "cp_demux2" "inside" "dvalid"
        let in_r = mine_net    "cp_demux2" "inside" "drdy"
        let out0_v = mine_net  "cp_demux2" "outside0" "dvalid"
        let out0_r = mine_net  "cp_demux2" "outside0" "drdy"
        let out1_v = mine_net  "cp_demux2" "outside1" "dvalid"
        let out1_r = mine_net  "cp_demux2" "outside1" "drdy"

        let in_coding = f4o4(valOf_or_fail "L3251" part_left.databus)      
        let out_coding = f4o4(valOf_or_fail "L3251" part_out0.databus)   
        //let mag = DCX_setget(DCX_data, markdeadlive nets true (snd conservation_exp)) // TODO do not discard guard with snd.
        // DCX_as : lhs field examined in dest context, rhs field in src context.

        let pred = xi_orred(ix_bitand (f1o4(valOf_or_fail "L3262" part_left.databus)) (xi_num 16))
        let pred_t = (xi_blift pred, Ndi LOCAL, false, cb)
        let pred_f = (xi_blift(xi_not pred), Ndi LOCAL, false, cb)        

        let dc_not(xb, _, _, _) = (xi_blift(xi_not(xi_orred xb)), Ndi LOCAL, false, cb)
        let product = 
            if burstf then
                let in_dlast = mine_net "lax" "inside" "dlast"
                let in_dcont = dc_not in_dlast
                let out0_dlast = mine_net "lax" "outside0" "dlast" // For completeness these need adding
                let out1_dlast = mine_net "lax" "outside1" "dlast"
                [
                    (s0, [in_v; in_r; pred_t],  [ gec_DC_move(in_coding, (iname_holder, DCX_data), (iname_in, DCX_data))], s1)
                    (s0, [in_v; in_r; pred_f],  [ gec_DC_move(in_coding, (iname_holder, DCX_data), (iname_in, DCX_data))], s3)                

                    (s1, [out0_v; out0_r; in_dlast; out0_dlast], [ gec_DC_move(out_coding, (iname_out0, DCX_data), (iname_holder, DCX_data))], s0)
                    (s1, [out0_v; out0_r; in_dcont], [ gec_DC_move(out_coding, (iname_out0, DCX_data), (iname_holder, DCX_data))], s2)
                    (s2, [in_v; in_r; pred_f],  [ gec_DC_move(in_coding, (iname_holder, DCX_data), (iname_in, DCX_data))], s1)

                    (s3, [out1_v; out1_r; in_dlast; out1_dlast], [ gec_DC_move(out_coding, (iname_out1, DCX_data), (iname_holder, DCX_data))], s0)
                    (s3, [out1_v; out1_r; in_dcont], [ gec_DC_move(out_coding, (iname_out1, DCX_data), (iname_holder, DCX_data))], s4)
                    (s4, [in_v; in_r; pred_f],  [ gec_DC_move(in_coding, (iname_holder, DCX_data), (iname_in, DCX_data))], s3)                
                ]
            else
              [
                (s0, [in_v; in_r; pred_t],  [ gec_DC_move(in_coding, (iname_holder, DCX_data), (iname_in, DCX_data))], s1)
                (s0, [in_v; in_r; pred_f],  [ gec_DC_move(in_coding, (iname_holder, DCX_data), (iname_in, DCX_data))], s2)                
                (s1, [out0_v; out0_r], [ gec_DC_move(out_coding, (iname_out0, DCX_data), (iname_holder, DCX_data))], s0)
                (s2, [out1_v; out1_r], [ gec_DC_move(out_coding, (iname_out1, DCX_data), (iname_holder, DCX_data))], s0)                 

              ]
        (product)

    // DC_move has dest first semantics.
    let cp_demux2_and_ser () =
        let burstf = false
        let iname_in = "left"
        let iname_out0 = "right0"
        let iname_out1 = "right1"
        let part_out0 = mine_part "cp_demux2_and_ser" iname_out0 participants        
        let (part_out0, fat_out_width) = set_databus "cp_demux2_ans_ser" "ddata" part_out0
        let part_out1 = mine_part "cp_demux2_and_ser" iname_out1  participants        
        let (part_out1, thin_out_width) = set_databus "cp_demux2_ans_ser" "ddata" part_out1

        let in_v = mine_net "cp_demux2_ans_ser" iname_in  "dvalid" // avoid in/out names for left/right ...
        let in_r = mine_net "cp_demux2_ans_ser"iname_in "drdy"
        let out0_v = mine_net "cp_demux2_ans_ser" iname_out0 "dvalid"
        let out0_r = mine_net "cp_demux2_ans_ser" iname_out0 "drdy"
        let out1_v = mine_net "cp_demux2_ans_ser" iname_out1 "dvalid"
        let out1_r = mine_net "cp_demux2_ans_ser" iname_out1 "drdy"

        let in_coding = f4o4(valOf_or_fail "L3251" part_left.databus)      
        let fat_out_coding = f4o4(valOf_or_fail "L3251" part_out0.databus)
        let thin_out_coding = f4o4(valOf_or_fail "L3251" part_out1.databus)           

        let pred = xi_orred(ix_bitand (f1o4(valOf_or_fail "L3262" part_left.databus)) (xi_num 16))
        let pred_t = (xi_blift pred, Ndi LOCAL, false, cb)
        let pred_f = (xi_blift(xi_not pred), Ndi LOCAL, false, cb)        

        let conservation_exp =
            let thinwidth = 16 // for now ... can get from out_data
            let out_data = f1o4(valOf_or_fail "L3344" part_out1.databus)
            let o_data0 = (out_data)
            let o_data1 = (xi_X -1 (out_data))
            ix_bitor (ix_lshift (o_data0) (xi_num thinwidth)) (o_data1) // For now.
        let dc_not(xb, _, _, _) = (xi_blift(xi_not(xi_orred xb)), Ndi LOCAL, false, cb)
        let mag = DCX_setget(DCX_data, markdeadlive nets true ((*snd*) conservation_exp)) // TODO do not discard guard with snd.
        let outwot = (iname_out1, DCX_data)
        let product = 
              [
                (s0, [in_v; in_r; pred_t],  [ gec_DC_move(in_coding, (iname_holder, DCX_data), (iname_in, DCX_data))], s1)
                (s0, [in_v; in_r; pred_f],  [ gec_DC_move(in_coding, (iname_holder, DCX_data), (iname_in, DCX_data))], s2)                
                (s1, [out0_v; out0_r], [ gec_DC_move(fat_out_coding, (iname_out0, DCX_data), (iname_holder, DCX_data))], s0)
                (s2, [out1_v; out1_r], [ gec_DC_move(thin_out_coding, outwot, (iname_holder, DCX_as(DCX_data, (outwot, 0), mag)))], s3) 
    // These tags opk tags are returned by bumper in reality.
    // mag' perhaps here ? using indexing?
                (s3, [out1_v; out1_r], [ gec_DC_move(thin_out_coding, outwot, (iname_holder, DCX_as(DCX_data, (outwot, -1), dcx_as_bumper "" false outwot mag)))], s0) 

             ]
        (product)

    let gsnd(a, E_EQ(p,  E_LIFT q)) = q // hack

    let canned_cp2() = // Serialise
        let part_out = mine_part  "cp2" "outside"  participants        
        let (part_out, out_width) = set_databus "cp2" "ddata" part_out    
        let in_v = mine_net "cp2" "inside" "dvalid"
        let in_r = mine_net "cp2" "inside" "drdy"
        let out_v = mine_net "cp2" "outside" "dvalid"
        let out_r = mine_net "cp2" "outside" "drdy"
        let mwidth = max in_width out_width
        let mag = DCX_setget(DCX_data, markdeadlive nets true (gsnd conservation_exp)) // TODO do not discard guard with snd.
        // DCX_as : lhs field examined in dest context, rhs field in src context.
        let in_coding = f4o4(valOf_or_fail "L3251" part_left.databus)           // Watch out - makes a fresh mutable?  mutations no longer used given tagged live fields.
        let out_coding = f4o4(valOf_or_fail "L3251" part_out.databus)           // Watch out - makes a fresh mutable?  mutations no longer used given tagged live fields.
        let outwot = (iname_out, DCX_data)
        let product =
            [
                (s0, [in_v; in_r],   [ gec_DC_move(in_coding, (iname_holder, mag), (iname_in, DCX_data))], s1) // DCX_data or DCX_exp could equally be loaded here since equal (under guard also needs loading but true in this case) but live trim has chosen DCX_serialise.

                // Do we need to make the choice on the unload of holder for a deserialiser?
                (s1, [out_v; out_r], [ gec_DC_move(out_coding, outwot, (iname_holder, DCX_as(DCX_data, (outwot, 0), mag)))], s2) 
    // These tags opk tags are returned by bumper in reality.
    // mag' perhaps here ? using indexing?
                (s2, [out_v; out_r], [ gec_DC_move(out_coding, outwot, (iname_holder, DCX_as(DCX_data, (outwot, -1), dcx_as_bumper "" false outwot mag)))], s0) 
            ]
        (product)


    let cp4() = // Deserialise 1to4
        let iname_out = "outside"
        let part_out = mine_part "cp4" iname_out  participants        
        let (part_out, out_width) = set_databus  "cp4" "ddata" part_out    
        let in_v = mine_net  "cp4" "inside" "dvalid"
        let in_r = mine_net  "cp4" "inside" "drdy"
        let out_v = mine_net  "cp4" "outside" "dvalid"
        let out_r = mine_net  "cp4" "outside" "drdy"
        let mwidth = max in_width out_width
        let mag = DCX_setget(DCX_data, markdeadlive nets false (gsnd conservation_exp))  // init to false in lmode - when in left field of DCX_as . TODO do not discard guard.
        let in_coding = f4o4(valOf_or_fail "L3251" part_left.databus)      
        let inwot = (iname_in, DCX_data)
        let a0 = mag                // Not clear whether the first field should be updated already in the first product arc.
        let a1 = dcx_as_bumper "cp4a" true inwot a0
        let a2 = dcx_as_bumper "cp4b" true inwot a1  // Bumper changes a dcon expression, here setting live the next field that matches inwot.
        let a3 = dcx_as_bumper "cp4c" true inwot a2        
        let out_coding = f4o4(valOf_or_fail "L3251" part_out.databus)      
        let aa0 = DCX_as(mag, (inwot, 0), DCX_data) 
        let aa1 = DCX_as(a0,  (inwot, -1), DCX_data)        
        let aa2 = DCX_as(a1,  (inwot, -2), DCX_data)      
        let aa3 = DCX_as(a2,  (inwot, -3), DCX_data)  
        let product = 
            [
                (s0, [in_v; in_r],   [ gec_DC_move(in_coding, (iname_holder, aa0),     inwot)], s1) 
                (s1, [in_v; in_r],   [ gec_DC_move(in_coding, (iname_holder, aa1),     inwot)], s2) 
                (s2, [in_v; in_r],   [ gec_DC_move(in_coding, (iname_holder, aa2),     inwot)], s3) 
                (s3, [in_v; in_r],   [ gec_DC_move(in_coding, (iname_holder, aa3),     inwot)], s4) 
                (s4, [out_v; out_r], [ gec_DC_move(out_coding, (iname_out, DCX_data), (iname_holder, DCX_data))], s0) 
            ]
        (product)
            
    let (canned_product) = // instances are refined with databus so need this yuck
        match namer with
            | "" when false -> canned_cp2()
            | "" when false -> cp4()            



            | ss when ss.IndexOf "DEMUX_1TO2_AND_SER" >= 0      -> cp_demux2_and_ser ()

            | ss when ss.IndexOf "BURST_DEMUX_1TO2" >= 0        -> cp_demux2 true
            | ss when ss.IndexOf "SPLITCHANNEL1.xml:AREAD" >= 0 -> cp_demux2 false


            | ss when ss.IndexOf "LITE_DEMUX_1TO2" >= 0 -> cp_demux2 false
            | other -> sf(sprintf "no canned product for '%s'" other)


    let product_states = list_once(map f1o4 canned_product)
    (product_states, canned_product)
  


let lowform_product ww settings nets participants conserve_equations = 
    let ww = WF 2 "form_product" ww ("Start")    
    let vd = !g_xvd
    let _:conservation_regexp_t list = conserve_equations
            
    let all_arcs = List.map (fun x -> x.arcs) participants
    let pcs = List.map (fun x -> x.Pc) participants

    let upd c cmd = (* cleaner if this is in the per-protocol code...! *)
        match cmd with
            | (Xassign(var, vale)) -> driven_info_update c vale var
            // | _ -> ()
    let driven = List.fold (fun c (clkprops, uid, (from_state, pc), g, cmds, to_state) ->
                            List.fold upd (singly_add (pc, Allvalues) c) cmds) [] (list_flatten all_arcs)

//    let driven = if true then assigned else driven PREFER THIS !


    //============================================================================= 
    let ww = WF 1 "form_product" ww "Making preparations by partitioning nets/state into commanded, unilateral_shared and unilateral_private."
    

    //The commanded set is variables that are functions of a participant PC or otherwise do not need to be explored since they cannot unilaterally change.
    //A possible exception is the true life of an input or output.

    //OLD: WRONG: A symbolic net  can be inferred to be live as an input in a protocol spec at the time it is used.  When it is an output we should ideally command
    //it live and dead, since the greater the dead period, the more schedulling flexibility there is, but it is ok if it remains live longer than commanded.
    //And also, all symbolic nets should be marked dead in the reset and idle predicates.

    // Symbolic vars with any commands at all are considered fully commanded, but
    // the explore function adds their unilateral events on the fly, if any. These are not the dead/live transitions that should be manifest in the protocol spec.

    let classified_nets =
        let mine_commanded (xpart:part_t) =
            let commanded_pred (net, dir, aux__, jspec) =
                let ans = jspec.commandedf
                vprintln 3 (sprintf "Commanded_pred for %s is %A  (aux__=%A)" (netToStr net) ans aux__)
                ans
            let (commanded, unis) = groom2 commanded_pred xpart.pnets
            (xpart, commanded, unis)
        map mine_commanded participants // For each participant, classify the nets as commanded or unilateral.

    let commanded_nets = List.fold (fun cc (xpart, commanded, unis) -> commanded @ cc) [] classified_nets

    let uni_shared:(hexp_t * netdir_t * bool * jspec_t) list =  // Unilaterally changing nets that are shared between machines.
        let rec us = function
            | [] -> []
            | (x, commanded, unis) :: tt ->
                let shared n = (disjunctionate (fun (x, commanded, unis) -> memberp n unis) tt) 
                let unis' = List.filter shared unis
                unis' @ us tt
        us classified_nets

    let uni_private_groups = // Find the unilateral nets that are not shared between machines
        let rec us = function
            | [] -> []
            | (xpart, commanded, unis) :: tt ->
                let privatepred n = not (disjunctionate (fun (xpart, commanded, unis) -> memberp n unis) tt) 
                let unis' = List.filter privatepred unis
                (xpart, unis') :: us tt
        us classified_nets


    // Cross-check that all nets are in one group         // Please use this ordering in all reports: commanded, shared, private
    let _ =
        let uni_private_nets  = list_flatten(map snd uni_private_groups)
        let check_membership_net (net, dir, commandedf, coding) =
            let x1 = memberp (net, dir, commandedf, coding) commanded_nets
            let x2 = not_nullp(List.filter (fun (n, _, _, _) ->n=net) uni_shared)
            let x3 = memberp (net, dir, commandedf, coding) uni_private_nets
            vprintln 2 (sprintf "Double-check membership net %s    %A %A %A %s" (xToStr net) x1 x2 x3 (if not(x1||x2||x3) then "MISSED OUT" else ""))
            ()
        let check_membership part = app check_membership_net part.pnets
        app check_membership participants
        vprintln 2 ("Cross-checking nets for membership complete.")

    // Possible values for one uni net in the x-product
    let possibilities_net (id, dir, commandedf, jspec) =
        let wiggle s =
            let gr =
                match jspec.coding with
                    | Symbolic_bitvec sv when jspec.evergreenf -> sf("Did not expect greenflag on uni wiggle" + netToStr id)
                    | _ ->
                        if jspec.evergreenf then muddy "gr_o is None L3887"
                        None
            vprintln 3 ("   FV uni wiggle " + xToStr id + " over " + xToStr s)
            SV_4(gr, id, gec_DL_lift s, (dir, jspec))
        let code_states = uni_coding_states jspec // 3/3 shared
        vprintln 0 (sprintf "Valuepoint states for net %s commandedf=%A coding=%s are %s" (netToStr id) commandedf (jspecToStr jspec) (sfold xToStr code_states))
        map wiggle code_states

    let lift lst = map (fun x-> [x]) lst
    let uni_shared_group_wiggles = // For a larger number of participants would it be logical to have one group per interaction set?
        let rec kk1 = function
            | [] -> sf "nullo shared group"
            | [ (net, vale, commandedf, dc) ] ->
                let ans = lift (possibilities_net (net, vale, commandedf, dc))
                if nullp ans then hpr_yikes (sprintf "no wiggles in shared partial product for net %s" (netToStr net))
                ans

            | net::tt ->
                let t' = kk1 tt
                let h' = possibilities_net net
                if nullp h' then hpr_yikes (sprintf "no wiggles in shared partial product for %s" (netToStr (f1o4 net)))
                if nullp t' then lift h' else cartesian_lists h' t'
        if uni_shared = [] then []
        else
            let ay = kk1 uni_shared
            let pol (x, idx) = (100+idx, x, intval nets x)// Make it stand out a little in traces by adding 100
            [(None, uni_shared, (map pol (zipWithIndex ay)))]

    let all_groups_wiggles = // Excludes commanded.
        let private_cart_wiggle (xp:part_t, unis) =
            let rec cart_product_for_unilateral_group = function
                | [] -> sf "nullo uni group - not supported"
                | [ net ] ->
                    let ans = lift (possibilities_net net)
                    if nullp ans then hpr_yikes (sprintf "no private_part_wiggles in partial product for net %s in %s" (netToStr (f1o4 net)) xp.pi_name)
                    vprintln 3 (sprintf "built %i private_part_wiggles for %s" (length ans) (xToStr (f1o4 net)))
                    ans
                | h::tt ->
                    let t' = cart_product_for_unilateral_group tt
                    let h' = possibilities_net h
                    let ans = if nullp t' then lift h' else cartesian_lists h' t' // Form cartesian product of all possible changes
                    vprintln 3 ("building private_cart_wiggle " + i2s(length h') + " " + i2s(length t') + " = " + i2s(length ans))
                    if nullp ans then hpr_yikes (sprintf "no wiggles in partial product for %s" xp.pi_name)
                    ans
            let cp_envs =
                if nullp unis then [] // better as the list containing null - then dont need special case on use site
                else
                    let ap = cart_product_for_unilateral_group unis
                    vprintln 3 (xp.kind + ": " + i2s(length ap) + " uni envs for this participant")
                    //reportx 3 (xp.kind + " uni envs") svToStr0 ap
                    map (fun (x, idx)-> (idx, x, intval nets x)) (zipWithIndex ap)

            (Some xp (*{ pi_name=xp.pi_name}*), unis, cp_envs)
        let private_wiggles = map private_cart_wiggle uni_private_groups
        //let _:(part_t option * (hexp_t * netdir_t * bool * jspec_t) list * ((int * (hexp_t * hexp_t * djc_t) list * string)) list) list = private_wiggles
        vprintln 2 (sprintf "All groups: %i groups of private_wiggles, %i uni_shared_group_wiggles." (length private_wiggles) (length uni_shared_group_wiggles))
        private_wiggles @ uni_shared_group_wiggles



    //=============================================================================
    let ww = WF 1 "form_product" ww "Composing global idle predicate"
    let idle_pred = foldl xi_and X_true (map (fun x -> x.Idle_pred) participants)


    let ww = WF 1 "form_product" ww "Composing global initial predicate"
    let initial_pred = list_flatten (List.map(fun (x:part_t) -> x.Initial_pred) participants)


    // Return starting state vector for a participant
    let starter s =
        if s <> net_retrieve nets s then failwith "unprocessed net"
        let ans = List.fold (fun c (setting, var)->if net_retrieve nets var=s then setting else c) g_bs initial_pred
        assert(ans <> g_bs)
        gec_DL_lift ans

    let segkey = function
            | 0 -> "commanded"
            | 1 -> "shared_unis"
            | nn when nn > 1 -> "private_unis" //one per participant
            | _ -> "???"
    // Form the state vector: use a special ordering for first two
    // sections which are commanded and uni_shared.
    // Our state vector is an interleaving  ?
    let state_vector =
        let combine (net, dir, commandedf_, jspec) cc =
            if jspec.coding=NS_unused then
                vprintln 2 (sprintf "Discard net %s from product state vector since flagged as unused." (netToStr net))
                cc
            elif false (* && length (inuse_coding_states__ coding) < 2 *) then
                vprintln 2 (sprintf "Discard net %s from product state vector since coding only contains %s" (netToStr net) (jspecToStr jspec))
                cc
            elif false && (dir = Ndi INPUT || dir = Ndi OUTPUT || dir = Always_input) && jspec.evergreenf then // do below
                settings.m_busses.add (xToStr net) (dir, jspec)
                vprintln 2 (sprintf "Introduce green counter for net %s from sv product since a symbolic i/o bus. coding=%s" (netToStr net) (jspecToStr jspec))
                cc

            else (net, (dir, jspec))::cc
        // Please use this ordering in all reports: commanded, shared, private
        let cmded  = List.foldBack combine commanded_nets []
        let shared_unis = List.foldBack combine uni_shared []
        let private_unis = map (fun (x, nets)-> List.foldBack combine nets []) uni_private_groups

        let sv = [ cmded; shared_unis ] @ private_unis // Flattened essentially
        vprintln 1 (sprintf "Legend: Composite state vector has %i segments" (length sv))
        let svs1 (net, dc) = xToStr net + " has dc = " + djToStr dc
        app (fun (s, seg_no) -> reportx 2 (sprintf "State Vector Segment %i %s Encoding (at start)" seg_no (segkey seg_no)) svs1 s) (zipWithIndex sv)
        sv:prodstate2_t

    
    let suffix = "_1T" // For parallel composition there can be multiple START tokens

    let gen_state back_state_name cre =
        let zg addcre_f seg =
            let seg2 (net, (dir, jspec)) cc =
                let green = 
                    if jspec.evergreenf then Some 0 // Starting value for evergreen counter.
                    else None
                SV_4(green, net, starter net, (dir, jspec)) :: cc
            let a0 = List.foldBack seg2 seg []
            if addcre_f then cre::a0 else a0
        let ssv = (zg true (hd state_vector)) :: (map (zg false) (tl state_vector))
        (back_state_name, (ssv, (rez_svToStr_baseless ssv, rez_svToStr_full ssv)))

    let origin = "START" + suffix
    let origin_cre =  SV_cre(origin, CRE_end, (*donef*)false) // Donef flag is not used.
    let primed_cre = SV_cre(origin, once "TODO one start point per eqn" conserve_equations, false)

    let origin_state =  gen_state origin origin_cre
    let primed_state =  gen_state origin primed_cre    


    
    vprintln 2 (sprintf "Origin state vector has %i segments and is %s %s" (length state_vector) (fst origin_state) (svToStr (snd origin_state)))
    vprintln 2 (sprintf "Primed state vector has %i segments and is %s %s" (length state_vector) (fst primed_state) (svToStr (snd primed_state)))    

    // cleanexit("temporary stop after participant compile")

    //=============================================================================
    let ww = WF 1 "form_product" ww ("Starting product construction")
    let mainarray = form_form_prod ww settings nets all_groups_wiggles origin_state primed_state driven
    let ww = WF 1 "form_product" ww ("Finished product construction")

    let uzl =
        let m_uzl = ref []
        let rec walkarray (a:mainarray_t) =
            //vprintln 0 ("Walkarray " + i2s(a.GetHashCode()))
            let _ =
                let dix (sv, successors) = mutadd m_uzl (sv, successors) 
                for z in a do app dix z.Value done
            ()
        walkarray mainarray
        !m_uzl

       
    let muzl0 = map fst uzl
        
    let ww =
        let msg0 = sprintf "Finished Product Construction: states encountered=%i" (length uzl)
        mutadd settings.m_states_report_lines msg0
        WF 1 "form_product" ww msg0

// This writes out if failed to find live path ... disabled for now.
//            let ww = WF 1 "form_product" ww (sprintf "Writing all states explored (incomplete graph) to %s.dot" gluename)

//            write_dotfile ww gluename "tit0" (plotnodes2 ww (valOf mainarray) "incomplete"  0 None muzl0)

    let live = live_trim ww mainarray report muzl0
    (live, idle_pred, pcs, origin_state, primed_state, mainarray)



///
let form_product_mid ww settings nets formal_decls participants conserve_equations =
   
    let prehydrate = function
        | CRE_equation(cre_idx, E_EQ(E_LIFT l, E_LIFT r)) ->
            let prec = infer_prec g_bounda l r
            //let l = hyd l
            //let r = hyd r
            // E_EQ_LD has stupid form with var replicated.
            let zf arg =
                let (sup, e) = markdeadlive nets false arg
                (E_LIFT e, (sup, e)) // Put in stupid form
            let e = E_EQ_LD(prec, zf l, zf r)
            CRE_equation(cre_idx, e)

        | other ->
            sf(sprintf "prehydrate other %A" other)
            other:conservation_regexp_t

    let conserve_equations = map prehydrate conserve_equations
                       
    let (live, idle_pred, pcs, origin_state, primed_state, mainarray) = lowform_product ww settings nets participants  conserve_equations

    //let _:sv_t = start_state
    let _ = // Textual report on the live set
        let repxf segs =
            sprintf " :  %s :"  (svToStr_full segs)
        reportx 3 "Live path states" repxf (Set.toList live)

    let ww =
        let msg1 = sprintf "Finished live path trimming: live states left=%i" (Set.count live)
        mutadd settings.m_states_report_lines msg1
        WF 1 "form_product" ww msg1

    let allowed_path_support =  live
#if SPARE
        if false then /// THIS IS DISABLED
            let (allowed_path_set_, allowed_path_support, glue_ans) = run_vbs ww nets idle_pred start_state mainarray live
            let gluename = "vbs_ans"
            let ww = WF 1 "form_product" ww (sprintf "Formatting glue result for %s.dot" gluename)
            write_dotfile ww gluename "tit2" (plotnodes2 ww (valOf mainarray) "glue-result" 2 None glue_ans)
            allowed_path_support
        else live
#endif

    let ww = WF 2 "form_product" ww ("Product formed. Starting dot form render.")    
    if settings.dotplot_glue then
        let gluename = "glue"
        if Set.count live > 0 then
            let ww = WF 1 "form_product" ww (sprintf "Formatting live path for %s.dot" gluename)
            let dot_o = (plotnodes2 ww mainarray "Final asynch result" 1 None (Set.toList live))
            if not_nonep dot_o then write_dotfile ww gluename "Live Set" (valOf dot_o)


    let _ = 
        let livename = "protolive"
        let ww = WF 1 "form_product" ww (sprintf "Proto-formatting live path for %s.dot" livename)
        let dot_o = (plotnodes2 ww mainarray "final asynch result" 1 None (Set.toList live))
        if not_nonep dot_o then write_dotfile ww livename "tit1" (valOf dot_o)

        // Allowed path set and not glue_ans goes forward...
#if SPARE
    let rec oldy_out svl = // Project from mainarray_t to transition adjacency list from: needed when other components disabled
        let keys = svToStr1 svl
        let ov = ma_lookup_suc mainarray svl
        let successors =
            let islive (_, clockprops, svl, _, dcon) = memberp svl live // Set membership - use a Set
            List.filter islive (!(valOf_or_fail "cvt848" ov))
        (svl, successors)
#endif
        
    let op =
        telescope_unclocked ww settings (mainarray:mainarray_t) report (snd origin_state) (allowed_path_support)

    let (codepoints, pc_values) = allocate_codepoints ww settings op (snd origin_state)
        
    let _ = 
        let livename = "live" // replot now we can also mark with final codepoints
        let ww = WF 1 "form_product" ww (sprintf "Formatting live path for %s.dot" livename)
        let dot_o = (plotnodes2 ww mainarray "final asynch result" 1 (Some codepoints) (Set.toList live))
        if not_nonep dot_o then write_dotfile ww livename "live-z" (valOf dot_o)

    (op, idle_pred, pcs, origin_state, codepoints, pc_values)
    //(live, idle_pred, formal_decls, pcs, start_state, Some mainarray)


// Expression re-arranger is in here for now.
let dcon_comp ww settings instances rhsvo dcoding (iname, op) =

    let destf = not_nonep rhsvo // We are processing a lhs if have been passed in the rhs as an option in rhsvo since lhs is done second.

    let find_instance iname = valOf_or_fail "find instance" (op_assoc iname instances)
    let instance = find_instance iname
    dev_println (sprintf "dcon_comp found instance destf=%A %s" destf iname)
    
    let rec rearrange truesrc ((opwot_, opbusiness_), opk) (tagged_support_, arg) = // Does not check tagged_support is live.  Does not check correct var. Just works on the power for now!!  TODO
        let rec rearr offsetter sofar = function
            | W_node(prec_, V_lshift, [arg; amount], _) when xi_constantp amount ->
                let d = xi_manifest64 "rearrange lshift" amount
                rearr (offsetter+d) sofar arg
                
            | W_node(prec_, V_bitor, lst, _) ->
                List.fold (rearr offsetter) sofar lst
            | X_x(X_bnet ff, nn, _)  
            | (Let 0 (nn, X_bnet ff)) ->
                if nn <> opk then sofar
                elif nonep sofar then Some offsetter
                else sf "Two or more targets in rearrange"

            | X_x(X_net(ss, _), nn, _)  
            | (Let 0 (nn, X_net(ss, _))) ->
                if nn <> opk then sofar
                elif nonep sofar then Some offsetter
                else sf "Two or more targets in rearrange"

            | other -> sf(sprintf "rearrange wot=%s_%i  other form %s  %s" opwot_ opk (xkey other) (xToStr other))
             
        match rearr 0L None arg with
            | Some ans ->
                ix_rshift Unsigned (f1o4 truesrc) (xi_num64 ans) // TODO Add a mask to the result.

            | None -> sf(sprintf "Failed to rearrange wot=%s_%i  arg=%s" opwot_ opk (xToStr arg))


    // Similarly, does not check tagged_support is dead.  Does not check correct var. Check ss please. Just works on the power for now!!  TODO Will need to go through the instance to connect opwot to actual identifer...
    let expression_insert truedest update_op lmode_alias rhs =
        let destwidth = // Or get from f4o4 truedest
            match ewidth "expression_insert concourse" truedest with
                | Some w -> (int)w
                | None   -> sf "L3753"

        let rhs_width =
            match ewidth "expression_insert rhs width" rhs with
                | Some w -> w
                | None -> sf (sprintf "Did not find lhs width for %s" (netToStr rhs))
        dev_println(sprintf "expression_insert: (update bit field) %A with rhs=%s in %s" update_op (xToStr rhs) (tagzilToStr lmode_alias))
        let m_didsome = ref None

        let rec ei (dd:int) arg =
            match arg with
                | (Let 0 (nn, X_net(ss, _)))
                | X_x(X_net(ss, _), nn, _)  ->
                    if nn = (snd update_op) then // TODO check ss as well please!
                        dev_println "TODO check ss as well please!"
                        //vprintln 3 (sprintf "ei: pos1 %i cf %i at nn=%i" nn (snd update_op) dd)                        
                        m_didsome := Some(rhs_width, dd)
                        rhs
                    else arg
                | (Let 0 (nn, X_bnet ff))
                | X_x(X_bnet ff, nn, _)  -> muddy "pos2 X_bnet"
                    

                | W_node(tprec, V_lshift, [ll; amount], _) ->
                    if xi_constantp amount then
                        let d = (int)(xi_manifest "expression_insert lshift" amount)
                        ei (dd  + d) ll
                    else muddy "xpants"

                | W_node(tprec, V_rshift _, [ll; amount], _) ->
                    if xi_constantp amount then
                        let d = (int)(xi_manifest "expression_insert rshift" amount)
                        ei (dd - d) ll
                    else muddy "rpants"

                | W_node(prec, oo, args, _) ->
                    let args' = map (ei dd) args
                    ix_op prec oo args'

                | other ->
                    if xi_constantp other then other
                    else
                        sf(sprintf "ei: other from %s in %s  %s" (xkey other) (xToStr other) (tagzilToStr lmode_alias))

        let _ = ei 0 (snd lmode_alias) // First should be checked for being dead.  return ignored? side effect in m_didsome
        let ans = 
            match !m_didsome with
                | None ->
                    sf(sprintf "Failed to expression_insert: (update bit field) %A with rhs=%s in %s" update_op (xToStr rhs) (tagzilToStr lmode_alias))
                | Some(width, baser) ->
                    //dev_println (sprintf "insert_field: yux %i %i" width baser)
                    let xmask = himask width
                    let topmask = if baser+width >= destwidth then 0I else (himask destwidth - himask (baser+width)) 
                    let botmask = if baser = 0 then 0I else himask baser
                    //dev_println (sprintf "Topmask={0:X}  botmask={0:X}" topmask botmask)
                    let mask = xi_bnum_n destwidth (topmask + botmask)
                    //let rhs = ix_bitand (xi_bnum_n width (himask dd)) rhs
                    ix_bitor (ix_lshift (rhs) (xi_num baser)) (ix_bitand mask truedest)
        ans
        
    // For lmode, eval returns the lhs and the value to be stored in it as a pair.
    // For rmode, the type structure is overkille with the rhs value being returned in both positions.
    let rec eval nn_ op = 
        match op with
            | DCX_data -> // This is silly coding - we are hydrating here but in the DCX hx form we assume already hydrated since raw hexp
                let ans =
                    match instance.databus with // stupid late lookup
                        | None ->
                            let rec silly_scanfor_databus = function
                                | [] -> sf ("No databus in " + instance.pi_name)
                                | (net, dir, b, d)::tt when (xToStr net).IndexOf "ddata" >= 0 ->
                                    //sf(sprintf "big %A" p)
                                    (net, dir, b, d)
                                | _::tt -> silly_scanfor_databus tt
                            silly_scanfor_databus instance.pnets
                        | Some db -> db
                (ans, valOf_or rhsvo (f1o4 ans))

            | DCX_setget(conc, _) -> // Assume conc wanted if not via DCX_as
                if conc = DCX_data then
                    let ans = valOf_or_fail "no databus 2/2" instance.databus
                    (ans, valOf_or rhsvo (f1o4 ans))
                else
                    let hx = muddy(sprintf "rearrange nn hx setget out of context? iname=%s destf=%A" iname destf)
                    ((hx, Ndi LOCAL, false, dcoding), valOf_or rhsvo hx)  // backdoor insert of coding for dest site only TODO. !!!
                    
                
            | DCX_as(resource, readout_op, DCX_setget(_, readout_alias)) -> // serialise useage
                if destf then eval nn_ resource // lmode result wanted!
                else
                    let (truesrc, _) = eval nn_ DCX_data
                    dev_println (sprintf "truesrc = %s" (xToStr (f1o4 truesrc)))
                    let hx = rearrange truesrc readout_op readout_alias
                    ((hx, Ndi LOCAL, false, dcoding), valOf_or rhsvo hx)  // backdoor insert of coding for dest site only TODO. !!! tuesrc may have the clue - no that is lmode - mask wants lmode expression width though.
                    
            | DCX_as(DCX_setget(_, lmode_alias), update_op, resource) -> // deserialise usage in lmode requires bit field insert returner.
                if destf then
                    let rhs = valOf_or_fail "L3679" rhsvo
                    let (truedest, _) = eval nn_ DCX_data
                    dev_println (sprintf "truedest = %s" (xToStr (f1o4 truedest)))
                    let composite = expression_insert (f1o4 truedest) update_op lmode_alias rhs
                    //((hx, Ndi LOCAL, false, dcoding), hx) 

                    (truedest, composite)
                else eval nn_ resource                    
    let ans = eval 0 op
    ans




//
//       
let form_product ww settings nets participants namer conserve_equations  =
    let ww = WF 2 "form_product" ww "Start-L4082"

    let formal_decls =
        let wrap(nn, netdir_, commandedf_, net_spec_) = DB_leaf(None, Some nn)
        let metaprams = [] // for now        
        let gec_decl participant =
            let abstraction_name = participant.kind
            let pi_name = participant.pi_name
            DB_group({ g_null_db_metainfo with kind=abstraction_name; pi_name=pi_name; not_to_be_trimmed=true; form=DB_form_external; metaprams=metaprams }, map wrap participant.pnets)
        map gec_decl participants

    let (op, idle_pred, pcs, start_state, codepoints, pc_values) =

        let g_canned_kludge = false // Delete this please
        if not g_canned_kludge then form_product_mid ww settings nets formal_decls participants  conserve_equations
        else
            let instances = map (fun p->(p.pi_name, p)) participants
            let equations = muddy "old way"
            let _:(hbexp_t * dcexp_t) list = equations
            let (product_states, canned_product) = directgen ww settings namer equations participants
            let ww = WF 1 "dcanned" ww "postproc"
            let phantom_part_pc = xi_string "phantom_part_pc"
            let dxr state = (ref false, state)
            let sty = (Ndi LOCAL, Concrete_enum (map dxr product_states))

            let collated = generic_collate f1o4 canned_product
            
            let encode vv =
                //let xx:sv_t = [ ([SV_4(None, phantom_part_pc, vv, sty)], xToStr vv + "PIGGER")] // TODO delete PIGGER! and all of this
                muddy "pigger xx"
            //let _ = map encode product_states

            let m_pp = ref 100
            let convirt0 ff (ff_, grds_and_cmds, dcon, tt) =
                dev_println (sprintf "convirt0: Start on %A" (xToStr ff_))
                let ppno = !m_pp
                mutinc m_pp 1
                let grd_pred (exp, _, _, _) =
                    match exp with
                        | W_node _ -> false // Assume mux predicate - but TODO is look at support to see if in or out using rearranger.
                        | X_bnet ff ->
                            match  (lookup_net2 ff.n).xnet_io with
                                | INPUT -> false
                                | OUTPUT -> true
                                | _ -> sf "L3379-io"
                        | other ->
                            dev_println(sprintf "mxpred other form a=%s  "  (netToStr other))
                            false
                let (cmds, grds) = groom2 grd_pred grds_and_cmds
                let cmds =
                    let to_cmd (arg, dir, _, coding) =
                        match arg with
                            | X_bnet ff -> ((dir, coding), Ybuffer(DL_lift arg, gec_DL_lift xi_one))
                            | other -> sf(sprintf "to_cmd: other form a=%s  "  (netToStr other))

                    map to_cmd cmds
                let grds =
                    let to_grd (arg, dir, _, coding) =
                        match arg with
                            | X_bnet ff -> xi_orred(arg)
                            | other ->
                                vprintln 3 (sprintf "to_grd: other form a=%s  "  (netToStr other))
                                xi_orred(other)


                    map to_grd grds

                let rank = 10
                let dcon_comp01 rhsvo dcoding (iname, op) = dcon_comp ww settings instances rhsvo dcoding (iname, op)

                let dcon_comp00 arg cc =
                    match arg with
                        | DC_move(tracer, gg___, coding, dest, src) ->
                            let ((rhs, _, _, _), _) = dcon_comp01 None coding src
                            let ((lhs, dir, _, coding), rhs) = dcon_comp01 (Some rhs) coding dest
                            let combf = (dir = Ndi OUTPUT) // for now
                            dev_println(sprintf "coding %A for %s. combf=%A" dir (xToStr lhs) combf)
                            // swap to buffer now done later. Need not do it here.
                            let nc = if combf then Ybuffer(DL_lift lhs, gec_DL_lift rhs) else Yassign(None, DL_lift lhs, gec_DL_lift rhs)
                            let dc = (dir, coding)
                            dev_println (sprintf "dcon_comp rez combf=%A %s" combf (ybevToStr nc))
                            (dc, nc)::cc
                        | other -> sf (sprintf "Other form dcon L3408 %A" other)
                let nc = List.foldBack dcon_comp00 dcon []
                // Compile dcon to code here now
                let old_dcon = []
                let arc = (ppno, Some true(*synchf/clockprops*), encode tt, (cmds @ nc, grds, rank), old_dcon)
                arc


            let op = 
                let convirt1 (ff, dests) =
                    let eff = encode ff // Maintain order of encoding points by let binding this first.
                    (eff, map (convirt0 ff) dests)
                map convirt1 collated

            let start_state:sv_t = encode (hd product_states)
            let idle_pred = ix_deqd phantom_part_pc (hd product_states)
            let pcs           = []

            let (codepoints, pc_values) = allocate_codepoints ww settings op start_state
            (op, idle_pred, pcs, muddy "old code path start_state", codepoints, pc_values) 


    check_still_live ww report "canned at op" op
    let _ =
        let gluename = "telescoped" // Was 'minch'
        let ww = WF 1 "form_product" ww (sprintf "Formatting machine path for %s.dot" gluename)
        let dot_o = (plotnodes3 ww "final synch result" 1 (Some codepoints) op)
        if not_nonep dot_o then write_dotfile ww gluename "Telescoped" (valOf dot_o)


    let ww = WF 2 "form_product" ww ("Product formed. Starting output VM render.")    
    let (formals, locals, execs) =
        let dex pcs =
            let dexf x cc =
                match x with
                    | None -> cc
                    | Some v -> (gec_DL_lift v)::cc
                    //| _ -> sf "L4321"
            List.foldBack dexf pcs []
        newrender ww settings (idle_pred, formal_decls, dex pcs, start_state) (codepoints, pc_values) op
    let ww = WF 2 "form_product" ww ("Finished")    
    (formals, locals, execs) // end of form_product




// We'll want to dynamically load the protocols too at some point.
// But for now they are in this table:

let ucond = vectornet_w("DHdata", 8) 
let apred = ix_pair (gec_X_net "arg") (xi_blift(xi_deqd(ix_bitand (gec_X_net "arg") (xi_num 1), xi_num 1)))
let bpred = ix_pair(gec_X_net "arg") (xi_blift(xi_deqd(ix_bitand (gec_X_net "arg") (xi_num 1), xi_num 0)))

let g_canned_protocol_table() =
    [
(*        ("tlm16", tlm16);
        ("tlwriter8", tlm_writer8);
        ("tlreader8", tlm_reader8);
        ("twophase", twophase(ucond));
        ("fourphase", fourphase(ucond));
        ("fourphase-a", fourphase(apred));
        ("fourphase-b", fourphase(bpred));         
        ("isa1620", isa1620);
        ("holder8", holder8);
        ("axi4lite", axi4lite);
        *)
        //gec_canned_credit_channel burstf dwidth max_credit initial_credit
        gec_canned_credit_channel false 32 3 0
        gec_canned_credit_channel false 32 3 3        
        gec_canned_stdsynch true 8
        gec_canned_stdsynch true 16
        gec_canned_stdsynch true 32
        gec_canned_stdsynch true 64
        gec_canned_stdsynch true 128
        gec_canned_stdsynch false 8
        gec_canned_stdsynch false 16
        gec_canned_stdsynch false 32
        gec_canned_stdsynch false 64
        gec_canned_stdsynch false 128
        g_canned_isa1620;
        g_canned_bvci32;
    ]


    
let gec_fifos1_equations () =
    let stdsynch_inet ss = x_net ("left" + "." + ss) // TODO need way to spec instance name
    let stdsynch_onet ss = x_net ("right" + "." + ss) // TODO need way to spec instance name    
    [
        (X_true, q_deqd (stdsynch_inet "ddata") (stdsynch_onet "ddata"))
    ]        

let g_stdsynch32to64_equations =    // "ddata64 == ddata32 | (ddata32 << 32)"
    let stdsynch_inet ss = x_net ("left" + "." + ss) // TODO need way to spec instance name
    let stdsynch_onet ss = x_net ("right" + "." + ss) // TODO need way to spec instance name
    
    let id32 = stdsynch_inet "xdd"
    let od64 = stdsynch_onet "xdd"
    [
        (X_true, ix_deqd od64 (ix_bitor id32 (ix_lshift id32 (xi_num 32)))) // Word ordering not specified using X_x TODO
    ]        


//    
// We dont parse the quations in the fsharp version yet ...
//
let hydrate_oldstyle_conseqn lst =
    let dix = function
        | (X_true, e) ->
            let ne = funique "NE" // A new/raw equation
            vprintln 2 (sprintf "Tag with %s RE item %s" ne (dcxToStr e)) 
            CRE_equation(ne, e)
        
        //("stdsynch32to64", g_stdsynch32to64_equations);
       // ("bvisa", g_bvisa_glue);        
       // ("fifos1", g_fifos1_equations)
    if length lst <> 1 then sf ("hydrate_oldstyle_conseqn - only hydrates singletons")
    hd(map dix lst)


let g_canned_conservation_equations =
    [
        ("demux_1to2_and_ser", hydrate_oldstyle_conseqn(gec_demux_canned_2_1_and_ser()))
        ("burst_demux_2_1",    hydrate_oldstyle_conseqn(gec_demux_canned_2_1()))
        ("lite_demux_2_1",     hydrate_oldstyle_conseqn(gec_demux_canned_2_1()))
        ("serdes_16_32",       hydrate_oldstyle_conseqn(gec_serdes_canned2 16))
        ("serdes_8_32",        hydrate_oldstyle_conseqn(gec_serdes_canned4 8))   
        ("simple_buffer",      hydrate_oldstyle_conseqn(gec_fifos1_equations()))
    ]

    
let lookup_protocol ww msg oname =
    let prototable = g_canned_protocol_table()
    let rec find = function
        | [] ->
            vprintln 0 ("Cannot find protocol called " + oname)
            vprintln 0 ("Available protocols are  " + sfold f1o5 prototable)
            sf ("Cannot find protocol called " + oname)
        | xx::_ when f1o5 xx = oname-> xx
        | _::tt-> find tt
    find prototable



// Parse a participant from the XML control file and assemble its components
// No longer hard code the experiments. Read from this XML file.
//
let f_participant ww settings atts tree =
    let msg = "f_participant"
    let rec f_id ats = function// ditto abstract and  whitespace 
        | (XML_ATOM s)::t -> s
        | _::tt -> f_id ats tt
        | [] -> sf "missing value in participant element"
    let xml_string_list msg key tree =
        let rec f_strl ats = function// ditto abstract and  whitespace 
            | (XML_ATOM ss)::tt -> ss :: f_strl ats  tt
            | _::tt -> f_strl ats tt
            | [] -> []
        let rec fsl = function
            | XML_ELEMENT(k, ats, lst)::tt when k=key -> f_strl ats lst
            | _::tt -> fsl tt
            | [] -> []
        fsl tree
    let xiname     = xml_once msg "iname"     f_id None tree
    let xprotocol  = xml_once msg "protocol"  f_id None tree    
    let xinterface = xml_once msg "interface" f_id None tree
    let xaliasnames = xml_string_list msg "aliasnames" tree    
    let xdirection = xml_insist "direction" ["forward";"reverse"] (Some 0) tree

    //vprintln 0 (sprintf " direction " + (i2s xdirection)) // 1 for reversed.
    let mx = "Parsing participant iname=" + xiname + " protocol=" + xprotocol + " inteface=" + xinterface + " direction=" + (if xdirection>0 then "reverse" else "forward") + " aliasnames=" + sfold id xaliasnames
    let ww = WF 1 "xml_f_participant" ww mx
    mutadd settings.m_report_file_contents mx
    // Want nets to be set by the 'interface' element but currently part of the protocol...
    let (oname, nets, init, idle, proto) = lookup_protocol ww msg xprotocol
    let swapo = fun net -> (if xdirection > 0 then swapsex net else net)
    let p1 = (oname, map swapo nets, init, idle, proto, xaliasnames)
    (xiname, p1)

// Parse an additional resource from the XML control file.
//
let f_resource ww settings atts tree =
    let msg = "f_resource"
    let rec f_id ats = function
        | (XML_ATOM s)::t -> s
        | _::t -> f_id ats t
        | [] -> sf "missing value in participant element"
    let xiname     = xml_once msg "iname"     f_id None tree
    let xkind      = xml_once msg "kind"      f_id None tree    
    let m = sprintf "Importing additional resource kind=%s iname=%s" xkind xiname 
    let ww = WF 1 "xml_f_resource" ww m
//    let (oname, nets, init, idle, proto) = lookup_resource ww msg xkind
//    let swapo = fun net -> (if xdirection > 0 then swapsex net else net)
    let part = 
        match xkind with
            | xkind when (strlen xkind)>3 && xkind.[1..2] = "hr" -> // eg shr32 or ahr 64  // A holding register.
                let synchf = (xkind.[0] = 's')
                let width = atoi32 xkind.[3..]
                let prefix = xiname + "_r_"
                let jspec =
                    {
                        commandedf=   true
                        anonj=        false // N/A
                        evergreenf=   false
                        coding=       gen_Symbolic_bitvec "" false width
                    }
                let reg = vectornet_w(prefix + xkind, width)
                let reg_spec = (reg,  Ndi LOCAL, jspec.commandedf, jspec)            
                let x_net ss = gec_X_net (prefix + ss)
                let live_value = g_fullval
                vprintln 1 (sprintf "rez holding register: xkind=%s synchf=%A iname=%s" xkind synchf xiname) 
                let (s0, s1, s2) = (xi_num 0, xi_num 1, xi_num 2)
                update_states true reg jspec.coding live_value
                let protocol__ =    // unused. Need to add "clk=^" for synchf
                   PS_seq[
                        Set (live_value,          reg);
                        Set (g_deadval,           reg);
                      ]
                let initial_pred = [  (g_deadval, reg) ]
                // (xiname, [reg], init, idle, protocol)
                let pc = vectornet_w(prefix + "PC", 2) // 2-bit wide for LtDtL xition. // bound_log2 (length states)
                let states = [s0; s1; s2]
                let jspecp =
                    {
                        commandedf=   true
                        anonj=        false // N/A
                        evergreenf=   false
                        coding=       gen_Concrete_enum states
                    }
                let pc_spec = (pc,  Ndi LOCAL, jspecp.commandedf, jspecp)
                let idle_pred = [(s0, pc)]
                let ga = get_next_arc_no xiname 
                let arcs = // Five-arc NDA: all arcs unconditional.
                    [
                        // Two arcs when dead: stay dead (SD) and go live (GL)
                       (ga "SD", synchf, (s0, pc), X_true, [],                                                     [s0]);
                       (ga "GL", synchf, (s0, pc), X_true, [Xassign(reg, live_value)],                             [s1]); 

                       // Three arcs when live: stay live (SL), live-to-live (L2L) and go dead (GD)
                       (ga "SL",  synchf, (s1, pc), X_true, [],                                                    [s1]);
                       (ga "GD",  synchf, (s1, pc), X_true, [Xassign(reg, g_deadval)],                             [s0]);

                       // L2L equivalent.
                       (ga "GtD",  false, (s1, pc), X_true, [Xassign(reg, g_deadval)],                             [s2]);                       
                       (ga "GtL", synchf, (s2, pc), X_true, [Xassign(reg, live_value)],                            [s1]); 

                       //(ga "L2L", synchf, (s1, pc), X_true, [Xassign(reg, g_deadval); Xassign(reg, live_value)], [s1]);
                    ]
                let arcs =
                    if synchf then arcs
                    else
                     [
                       // Next two should be intrinsic/implicit please ? Fantom held over clock edge - not likely, we assign dead.
                       (ga "SGDX", true, (xi_num 0, pc), X_true, [Xassign(reg, g_deadval)], [xi_num 0]);
                       (ga "SGDY", true, (xi_num 1, pc), X_true, [Xassign(reg, g_deadval)], [xi_num 1]);
                     ] @ arcs

                let deqd (a, b) = ix_deqd a b
                let part =
                    {
                        kind=         xkind
                        aliasnames=   []
                        pi_name=      xiname
                        pnets=        [reg_spec; pc_spec]
                        Pc=           Some pc // Holding reg has live and dead states.  0=dead, 1=live.
                        arcs=        arcs
                        Initial_pred=initial_pred @ idle_pred 
                        Idle_pred =  ix_andl(map deqd (idle_pred @ idle_pred))
                        states=      states
                        databus=      Some reg_spec
                    }
                part

            | other -> cleanexit(sprintf "Cannot find additional resource of kind %s" xkind)
    let wpx (a,b) = xToStr a + "==" + xToStr b
    vprintln 2 (sprintf "Additional resource: participant %s  %s  states=%s  inital=%s"  part.kind part.pi_name (sfold xToStr part.states) (sfold wpx part.Initial_pred))
    participant_report ww settings xiname xkind part
    part



// Read some glue equations.  Parsed in full version, but canned for now.
let f_conservation_equations ww settings atts tree =
    let rec f_id ats = function
        | (XML_ATOM s)::tt -> s
        | _::tt            -> f_id ats tt
        | [] -> sf "missing value in equations element"
    
    let id = f_id atts tree

    let ww = WF 1 "Conservation_equations equations" ww (sprintf ": Lookup of %s " id)
    
    let rec ss = function // op_assoc
        | (a, b)::tt -> if a=id then Some b else ss tt
        | [] -> sf ("Conservation_equations equations " + id + " not in canned table")

    let ans = ss g_canned_conservation_equations

    if nonep ans then sf  ("no conservation_equations equations" )
    else valOf ans



// Parse the xml control file for the job in hand.
//    
let read_control_file ww settings controlname =
        
    let readxml filename = hpr_xmlin ww filename
    let tree = readxml controlname

    let getstring arg:string =
        match arg with
            // ... skip leading whitespace please
            | XML_ATOM ss::_ when ss.Length > 0 -> ss
            | other -> sf (sprintf "Malformed driver file %s.  String %A" controlname other)

    let f_chanparts atts lst =
        let chname =
            let get_chname sofar = function
                | XML_ELEMENT("channel_name", _, lst1) -> getstring lst1
                | _ -> sofar
            List.fold get_chname "" lst
        let ch =
            (xml_multi "participant" (f_participant ww settings) lst,
             xml_multi "equations"   (f_conservation_equations ww settings) lst,
             xml_multi "resource"    (f_resource ww settings) lst
            )
        (chname, ch)


    
    let rec ff0 = function
        | (XML_ELEMENT("joining", ats, lst))::tt -> // old keyword
            [f_chanparts ats lst]
        | (XML_ELEMENT("singlechannel", ats, lst))::tt -> // new alias for old keyword
            [f_chanparts ats lst]
            // remainder of file in tt is ignored!
            
        | (XML_ELEMENT("multichannel", ats, lst))::tt ->
            let ans = xml_multi "channel" f_chanparts lst
            ans
            
        | (XML_ELEMENT(ss, ats, lst))::tt ->
            hpr_yikes(sprintf "Ignore element %s in driver file %s" ss controlname)
            ff0 tt
        | _::tt -> // Tacit ignore any other elements, whitespace or comments.
            ff0 tt
        | [] -> sf ("No recognised joining control file element: " + xmlToStr "" tree)

    let globals = [] // for now /.. need to allocate them to channels
    let channels =
        match ff0 [tree] with
            | items -> items
            //| _ -> sf "L4205"
    (channels, globals)
    


    // Compile one channel of a multi-channel design. Channels are essentially independent
    // glue entities in one wrapper but which can intercommunicate with shared resources as specified in the conservation predicates.
let compile_channel ww settings controlname globals (chname, (participants0, equations, resources)) = 
    let msg = sprintf "start. controlname=%s chname=%s" controlname chname
    let ww = WF 2 "compile channel" ww msg

    let equations =
        match equations with
            | [] -> muddy ("Provide default conservation equation please for " + chname)
            | items -> items
            //| _ -> sf ("More than one equations definition in control file " + controlname)

    let participants0 = map (precompile_participant ww settings) (rev participants0) // Process in XML input file order.

    let ww = WF 2 msg ww "participants coralled"

    let participants = participants0 @ resources

    let nets =
        let gnets cc p = lst_union (p.pnets) cc
        List.fold gnets [] participants
    // Complexity estimate report
    let _ =
        let statesl = map (fun x->length x.states) participants
        // Actual state vector has many more states possible, but we shall only examine the reachable states that conserve data.
        let cmsg = sprintf "Complexity estimate:  PC-based state space rough estimate is product of %s which is %i" (sfold i2s statesl) (List.fold (fun cc sl -> cc * (max 1 sl)) 1 statesl)
        vprintln 1 cmsg
        mutadd settings.m_states_report_lines cmsg

    let checkglue (l, r) =
        let mm = makemap []
        let l' = xi_rewrite_exp mm l
        let r' = xi_rewrite_exp mm r
        vprintln 0 ("Glue  " + xToStr l' + "  ===  " + xToStr r')
        (l', r')

    // Must call checkglue AFTER the interface nets have been entered in the net base by precompile_participant.
    //let unfinshed_ = map checkglue (list_flatten equations)


    if (length participants < 2) then hpr_warn ("Only " + i2s(length participants) + " participants in product")


    let namer = controlname + ":" + chname
    // Main work:
    let (formals, locals, execs) = form_product ww settings nets participants namer equations
    let ww = WF 2 "compile channel" ww "finished"
    (formals, locals, execs)



    
let opath_jinjoin_vm ww (op_args:op_invoker_t) vm0_ =
    let orangename = op_args.stagename
    let ban = orangename + ": " + g_jinjoin_banner + " " + get_os()
    vprintln 1 (ban + "\n" + timestamp(true) + "\n")
    g_version_string := ban
    let ww = WF 2 orangename ww "Start"
    let controlname = control_get_s orangename op_args.c3 "controlfile" None
    let opname = control_get_s orangename op_args.c3 "o" (Some "out")

    g_xvd :=  control_get_i op_args.c3 (orangename + "-loglevel") 20 //

    let settings =
        {
            opfname=                  opname
            op_kindname=              opname            
            m_busses=                 new m_busses_t("m_busses")
            discard_participant_pcs=  false
            dotplot_glue=             true
            dotplot_participants=     true
            integer_pcvals=           true
            generate_bevcode=         false
            pc_name=                  "jin"
            m_report_file_contents=   ref []
            m_states_report_lines=    ref []            
            unisymb_cache=            new unisymb_cache_t()
            newrender_vd=             3 // Get from cmdline or recipe please.
            prod_vd=                  5
        }

    mutadd settings.m_report_file_contents (joiner_banner())
    mutadd settings.m_report_file_contents (timestamp true)

    let (channels, globals) = read_control_file ww settings controlname 
    vprintln 2 (sprintf "Compiling %i channels defined in %s" (length channels) controlname)
    if nullp channels then cleanexit("No channels of joining defined in " + controlname)
    let ww = WF 2 orangename ww "Driver file read."

    let ans = map (compile_channel ww settings controlname globals) channels

    let (formals, locals, execs) = List.unzip3 ans
    let locals  = list_flatten locals
    let formals = list_flatten formals
    let execs   = list_flatten execs
    
    local_logtidy ww settings "Normal exit"

    // Form Output VM2
    let kind = settings.op_kindname
    let atts = []
    let vlnv_name = { vendor= !g_m_xact_op_vendor; version= !g_m_xact_op_version; library= !g_m_xact_op_library; kind=[kind] }
    // library="JinJoinAutoRender" 
            
    let local_decls =
        let wrap nn = DB_leaf(None, Some nn)
        DB_group({ g_null_db_metainfo with kind="internal_nets" }, map wrap locals)

    let iinfo =
        { g_null_vm2_iinfo with
            definitionf=          true
            generated_by=         orangename
            kind_vlnv=            vlnv_name
            externally_provided=  false // false, since these should not be separate modules
        }

    let vm2 = HPR_VM2({ g_null_minfo with name=vlnv_name; atts=atts }, formals @[local_decls], [], execs, [])


    [(iinfo, Some vm2)]    


let jinjoin_used () =
    let stagename = "jinjoin"
    let argpattern =
        [
            Arg_required("controlfile", -1, "Name of XML control file listing participants", "");

//          Arg_required("named-conserve-predicate", 1, "Pre-parser conservation predicate", "");
            Arg_required("o", 1, "Name of SystemC or Verilog output file", "");

            Arg_int_defaulting(stagename + "-loglevel", 20, "Verbosity level for ... (lower is more verbose)");
        ]

    install_operator (stagename,  "Greaves/Nam glue/TLM Join X Productor", opath_jinjoin_vm, [], [], argpattern)


(* eof *)
