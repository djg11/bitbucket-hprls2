(*
 * Transactor and Glue Logic Generator.  Automatic synthesis of bus protocol adaptors.
 * (C) 2010-16 D J Greaves, University of Cambridge, Computer Laboratory.
   
 * Based on 'Synthesis of glue logic, transactors, multiplexors and serialisors from protocol specifications'. By D. J. Greaves, M. J. Nam. At Forum on Specification & Design Languages (FDL 2010). 14th-16th Sept. 2010 Southampton.

 * Cross-product glue logic and transactor generator.
 *
 * $Id: joindefs.fs,v 1.13 2010/09/13 07:46:49 djg11 Exp $  
 *
 * Definitions of our data structures.
 *)


module joindefs
open yout
open moscow
open hprls_hdr
open meox

(*

In general, many different input language constructs are useful for protocol and
transactor specification. These include a wide range of commercial and
experimental temporal logic and assertion languages.  In our
experiments, we used a combination of automatic and manual conversion
from various sources to a common protocol representation
with abstract syntax tree shown in Table~\ref{tab:lts}. 


This is the
labled transition language (LTS) form where the lables perform operations
on variables. Arcs are labled with a list of pairs of expressions
which must atomically become pairwise equal when the transition is
taken. For instance, if one half of a pair is an output or local
variable and the other half is an expression that is a function of
inputs, then an assignment is created copies the expression to the
output or local.  Whether a variable is an input or output depends on
the direction of instantiation of an interface and, for symbolic
variables, whether it is going dead or live.

For synchronous edges, transitions are taken on the active edge of the clock and for asynchronous edges, at any time but can they be co-incident or strictly alternating - please say.


(The LTS is compiled into a flat form where the current state variable is just another net - is it really?)

*)
type protocol_spec_t =  // Lifted protocol spec AST for participant compiler accepts this form.
 | Set of hexp_t * hexp_t                //Place (command) a value in a var - commutative - two expressions can be unified in principle, provided no complex root finding is needed.
 | PS_setlq of hbexp_t * (hexp_t * hexp_t) list  * (string * string) list       // Parallel list of sets with attributes and guard.
 | Setl of (hexp_t * hexp_t) list          // Parallel list of sets
 | PS_seq of protocol_spec_t list          // Time sequence, simple FSM
// | Conjunction of protocol_spec_t list   // Non-det, non-lock-step fork of flow
 | Always of hbexp_t                       // Safety specification

 | PS_alt of protocol_spec_t list        //Alternatives
 | Disjunction of protocol_spec_t list   //Alternatives - deprecated alternate form. Delete?
 | Par of protocol_spec_t list           // Conjunction
 | PS_next                               // wait a clock - for synchronous protocols only - we currently use Seq with a clock qual.


type protocol_mood = // see also synchf
    | Synch of  protocol_spec_t
    | Asynch of  protocol_spec_t
    | Either of  protocol_spec_t
    
type dcx_arg_t = ((hexp_t * (int * bool) list) list) * hexp_t // Pair up an expression with its tagged support powers.

(*
 * In a reversed port, the inputs are swapped with the outputs, apart from Always_x forms
 * that do not swap direction (e.g, master clock).
 *)
type netdir_t = Always_input | Ndi of varmode_t

type net_coding_t =
    | Concrete_enum of  (bool ref * hexp_t) list // * enum_constraint_matrix_t option // Enumeration constants - the bool cmdd_flag holds for enumeration values that are explicitly set in the protocol spec.  This prints as a plus sign.
    | Symbolic_bitvec of symbolic_bitvec_t
    | NS_unused

and jspec_t =
    {
        //prec:       precision_t 
        evergreenf:   bool        // move me here?
        anonj:        bool
        commandedf:   bool
        coding:       net_coding_t
    }
and coding4_t = hexp_t * netdir_t * bool * jspec_t // (net, dir, commandedf, coding) coding should use precision_t .  commandedf, if still used, now deprecard here - use the inside jnet field now
    
and dcexp_t =  // compare with  conservation_regexp_t
   | E_LIFT  of hexp_t // just for lifting ... can be a different type.
   | E_EQ    of dcexp_t * dcexp_t
   | E_EQ_LD of precision_t * (dcexp_t * dcx_arg_t) * (dcexp_t * dcx_arg_t)
   //| E_EQ_used

// A ping/pong system, where resources are named and anon.  Transfers between any named resource need to be controlled by an equation.
// A dead holding register or other partially-fillable dest can accept a newly hydrated datacons equation. This must be fully filled
// and then fully drained before it becomes dead and ready to use a gain.
and dcx_life_t =
    | DL_dead // lift of deadval preferred TODO
//    | DL_active
    | DL_lift of hexp_t
    | DL_filling of precision_t * (dcexp_t * dcx_arg_t) * (dcexp_t * dcx_arg_t)
    | DL_draining of precision_t * (dcexp_t * dcx_arg_t)


and symbolic_bitvec_t =  // Better to embed a meox.prec_t instead of bits.
    {
         bits:        int                             // Symbolic register bit width ... should be a prec_t field really.
         maxcount:    int                             // Max number of symbolic values available ... a sanity limit
         life:      (bool ref * dcx_life_t) list ref    // Symbolic values in use: numbered from 0=dead. bool ref is set if commanded to that value (not needed anymore).
         evergreenf2:  bool                         // For an an input or output, the state includes the next transfer number on this port.
         //named_resource: bool   // aka anonj
    }


type enum_constraint_matrix_t = (hexp_t * hexp_t list) list // Only limited trajectories are allowed dictated by this.

let dcx_argToStr (alive, exp_) =
    let s2s (e, i) cc =
        let live_or_dot (n, m_b) cc = (if m_b then i2s n else ".") + cc
        List.foldBack live_or_dot i cc
    sprintf "^%i^%s^" (length alive) (List.foldBack s2s alive "")

let rec dcxToStr = function
    | E_LIFT(X_net(ss, _)) -> sprintf "E_LIFT(N:%s)" ss  // These should have gone!  Equation or participant hydration wrong if you get one.
    | E_LIFT(exp) -> sprintf "E_LIFT(%s)" (xToStr exp)    
    | E_EQ(a, b) -> sprintf "E_EQ(%s, %s)" (dcxToStr a) (dcxToStr b)
    | E_EQ_LD(prec, (l, ldc), (r, rdc)) -> sprintf "E_EQ_LD_raw(%s, %s, ...)" (dcxToStr l) (dcxToStr r)
    //| E_EQ_used -> "USED"

let lifeToStr_brief = function
    | DL_filling(prec, lx, rx)       -> "DL_filling(...)"
    | DL_draining(prec, (exp, arg))  -> sprintf "DL_draining(%s, %s)" (dcxToStr exp) (dcx_argToStr arg)
    | DL_dead         -> "DL_DEAD"
    //| DL_active     -> "DL_active"
    | DL_lift hexp    -> xToStr hexp

let lifeToStr = lifeToStr_brief 

     
let rec codingToStr = function
    | NS_unused          -> "NS_unused"
    | Concrete_enum lst  -> "Concrete:[" + sfold (fun (a,b) -> (if !a then "+" else "") + xToStr b) lst + "]"
    | Symbolic_bitvec v  -> "Symbolic:(" + i2s(v.bits) + ")[" + sfold (fun (a,b)-> (if !a then "+" else "") + lifeToStr b) (!(v.life)) + "]"


let gen_Concrete_enum l = Concrete_enum (map (fun x ->(ref false, x)) l)


let concrete_bool() = gen_Concrete_enum [ xi_num 0; xi_num 1 ] // need fresh ones owing to ref inside.
  

// Value denoted as $bot in the paper is here called deadval  DL_dead
let g_deadval = xi_string "dead"
let g_fullval = xi_string "FULLVAL"
let g_powerval = xi_string "P-STROKE"
let g_DL_full = DL_lift g_fullval        // All bits full with live data ... perhaps deprecated
let g_DL_powerstroke = DL_lift g_powerval  // For a synchronous bus, this denotes the transition where a live value is transferred (incrementing any associated green counter).

// The kill operator, for data non-conservation, is denoted here with kill(alpha)
let gis = { g_default_native_fun_sig with rv=g_default_prec; args=[g_default_prec];  }

let kill alpha = xi_apply(("kill", gis), [alpha])

let lshift(e, amount) = ix_lshift e (xi_num amount)

let predicate(bv, pred) = ix_pair bv (xi_blift pred) // A predicated data constuction - we use the X_pair form

let g_symbo_maxcount = ref 6

let gen_Symbolic_bitvec msg_ evergreenf width =
    Symbolic_bitvec
       {
         bits=             width
         maxcount=         !g_symbo_maxcount // A rather arbitrary number, but if exceeded we need to run again with a higher starting value
         life=             ref [ (ref false, DL_dead) ] // Not used for synchronous i/o busses that instead have evergreen flag set.
         evergreenf2=      evergreenf
         //named_resource= true
       }

let rec is_symbolic v =
   match v with
       | Concrete_enum l    -> false
       | Symbolic_bitvec _  -> true
       | NS_unused          -> false

let rec max_coding_states v = 
   match v with
       | Concrete_enum l    -> length l
       | Symbolic_bitvec v  ->
           if v.evergreenf2 then 1
            else v.maxcount
       | NS_unused          -> 0

let g_evergreen_state = xi_string "GREEN"

// Get the possible values of a state vector member where the cmd flag has not been set, on the
// basis that those with the flag set will always be commanded to that value, not unilaterally changing to it.       
let rec uni_coding_states jspec = 
   match jspec.coding with
       | Concrete_enum l    -> List.fold (fun cc (cmdd, b)-> if !cmdd then cc else b::cc) [] l
       | Symbolic_bitvec v  ->
           if v.evergreenf2 then [] // [g_evergreen_state] // A return of the evergreen state seems unnecessary since the guarding inputs will have the appropriate uni changes and the evergreen bus stroke is commanded by the protocol.
           else []
           // We're assuming all new dcx_ symbolic code points are commanded ...
           // List.fold (fun cc (cmdd, b)-> if !cmdd then cc else b::cc) [] !v.life
       | NS_unused          -> []


// Get all possible possible values of a state vector member where the cmd flag has not been set   - misnomer?
let n_inuse_coding_states v = 
   match v with
       | Concrete_enum lst  -> length lst
       | Symbolic_bitvec v  ->
           if v.evergreenf2 then 1 // TODO delete this ref - no longer used - its in the bumper thing.
           else length !v.life
       | NS_unused          -> 0

#if OLD       
let rec inuse_coding_states v = 
   match v with
       | Concrete_enum lst  -> lst
       | Symbolic_bitvec v  ->
           if v.evergreenf then [(ref false, g_evergreen_state)] // TODO delete this ref - no longer used - its in the bumper thing.
           else !v.values
       | NS_unused          -> []
#endif


// Swap inputs for outputs if we are instantiating a mirrored/reversed interface.
let swapsex (net, dir, evergreenf, width) =
    let f2 = function
        | Always_input -> Always_input
        | Ndi x -> Ndi (reverse_varmode x)
    (net, f2 dir, evergreenf, width)



let dirToStr =
    function
    | Always_input -> "Always_input"
    | Ndi x -> varmodeToStr x


// Find encoding 'integer' for the state of a variable (now a string infact!):
let rec nassoc id p state lst =
       match lst with
        | [] -> None
        | ((cmdd, h)::t) -> if h=state then Some p else nassoc id (p+1) state t

type cre_nodeidx_t = string



   
let q_deqd l r =
    E_EQ(E_LIFT(l), E_LIFT(r))
 

// Outer Kleene star is normally implied. 
// NO:
  // We want to use a form where a tree of catenations is right-associated (no left arg is itself a catenation)
  // Re-arrange so right-associated (list-like form).        
  // An invalid guard on one branch of an alternation allows it to be a hidden catenation, why is ... 
// YES: compile with a branch to back_address_t state once done.
type conservation_regexp_t =
    | CRE_guard of cre_nodeidx_t *  conservation_regexp_t * hbexp_t // The paper says these are lower down the AST, but they can be here for now.
    | CRE_catenate of cre_nodeidx_t *  conservation_regexp_t * conservation_regexp_t 
    | CRE_alt of cre_nodeidx_t *  conservation_regexp_t list
    | CRE_star of cre_nodeidx_t *  conservation_regexp_t
    | CRE_equation of cre_nodeidx_t * dcexp_t
    | CRE_end  


(* eof *)
