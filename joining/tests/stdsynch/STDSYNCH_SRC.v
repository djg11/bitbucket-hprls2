// HPR L/S Logic Synthesis Library
// (C) 2004, DJ Greaves University of Cambridge, Computer Laboratory.
// Standard synchronous protocol - simplex source
`timescale 1ns/1ns


module STDSYNCH_SRC
  #(parameter DATA_WIDTH=32,
    parameter SRC_NO=1)   
  (input clk,
   input 		   reset,
   input 		   drdy,
   output reg 		   dvalid,
   output [DATA_WIDTH-1:0] dout);

   wire [ 3:0] 		   src_no_4bit = SRC_NO;

   reg [14:0] 		   prbs, prbs_fordata;
   reg [12:0] 		   count_for_data;		   

   wire 		   go = drdy && dvalid;
   wire pre_valid = prbs[5] & prbs[3];

   always @(posedge clk) if (reset) begin
      prbs = 101 + SRC_NO; dvalid = 0; count_for_data = 0; prbs_fordata = 101; end
   else begin
      prbs <= { prbs[13:0], (prbs[13] ^ prbs[14]) };
      if (go) begin
	 prbs_fordata <= { prbs_fordata[13:0], (prbs_fordata[13] ^ prbs_fordata[14]) };
	 count_for_data <= count_for_data + 1;
	 end
      dvalid <= pre_valid;  // Should perhaps not deassert dvalid unless previous request has been served?
   end


   assign dout = (go) ? { prbs_fordata, count_for_data, src_no_4bit } : 0;

      

endmodule // STDSYNCH_SRC
// eof

