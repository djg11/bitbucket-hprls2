// HPR L/S Logic Synthesis Library
// (C) 2004, DJ Greaves University of Cambridge, Computer Laboratory.

// Serdes: serialiser testbench.


module STDSYNCH_BENCH202();
   parameter SRC_WIDTH=32;
   parameter SINK_WIDTH=16;   

   reg 			   clk, reset;
   
   wire 		   drdy0, dlast0, dvalid0;
   wire [SRC_WIDTH-1:0]   dbus0;


   wire 		   drdy_y, dlast_y, dvalid_y;
   wire [SINK_WIDTH-1:0]   dbus_y;



   initial begin #3000 $finish; end
   initial begin $dumpfile("vcd.vcd"); $dumpvars(); end
   initial begin reset=1; #33 reset = 0; end
   initial begin clk=0; forever #5 clk = !clk; end   


   STDSYNCH_SRC #(SRC_WIDTH, 10) src0(
			       //STDSYNCH_BURST_SRC #(32, 10) src0(
				     .reset(reset),
				     .clk(clk),
				     .drdy(drdy0),
				     //.dlast(dlast0),
				     .dvalid(dvalid0), 
				     .dout(dbus0));
  
   STDSYNCH_SINK #(SINK_WIDTH) sink0(
   //STDSYNCH_BURST_SINK sink0(
			     .reset(reset),
			     .clk(clk),
			     .drdy(drdy_y),
			     //.dlast(dlast_y),
			     .dvalid(dvalid_y),
			     .din(dbus_y));
 SERIALISER32TO16 ser32to16(    
     .reset(reset),
     .clk(clk),
     .db_inside_rdy(drdy0), .db_inside_valid(dvalid0), .db_inside_data(dbus0), // .db_insidelast(dlast0),
     .db_outside_rdy(drdy_y), .db_outside_valid(dvalid_y), .db_outside_data(dbus_y) // .db_outside_last(dlast_y)
				 );


endmodule // STDSYNCH_SRC
// eof

