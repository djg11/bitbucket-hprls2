// HPR L/S Logic Synthesis Library
// (C) 2004, DJ Greaves University of Cambridge, Computer Laboratory.

// Standard synchronous protocol - simplex connection for FIFO and buffer


module STDSYNCH_BENCH1();

   parameter DATA_WIDTH=32;
   reg 			   clk, reset;
   
   wire 		   drdy, dvalid;
   wire [DATA_WIDTH-1:0]   dbus;

   wire 		   drdy_y, dvalid_y;
   wire [DATA_WIDTH-1:0]   dbus_y;




   initial begin #1000 $finish; end
   initial begin $dumpfile("vcd.vcd"); $dumpvars(); end
   initial begin reset=1; #33 reset = 0; end
   initial begin clk=0; forever #5 clk = !clk; end   
   
   STDSYNCH_SRC #(DATA_WIDTH) src0(
		     .reset(reset),
		     .clk(clk),
		     .drdy(drdy),
		     .dvalid(dvalid), 
		     .dout(dbus));

   STDSYNCH_SINK #(DATA_WIDTH) sink0(
		       .reset(reset),
		       .clk(clk),
		       .drdy(drdy_y),
		       .dvalid(dvalid_y),
		       .din(dbus_y));

   SIMPLE_STDSYNCH_BUFFER the_buffer(
				     .reset(reset),
				     .clk(clk),
				     .left_drdy(drdy), .left_dvalid(dvalid), .left_ddata32(dbus), // .insidelast(dlast0),
				     .right_drdy(drdy_y), .right_dvalid(dvalid_y), .right_ddata32(dbus_y) // .outside_last(dlast_y)
				 );

endmodule // STDSYNCH_BENCH1
// eof

