

// HPR L/S Logic Synthesis Library
// (C) 2004, DJ Greaves University of Cambridge, Computer Laboratory.

// Standard synchronous protocol - simplex aribter, static priority, non-burst, combinational paths allowed.



module STDSYNCH_ARBITER2_STATIC_PRI_NOBURST
  #(parameter DATA_WIDTH=32)
   (input clk,
    input  reset,

    input  dvalid0, output drdy0, input [DATA_WIDTH-1:0] din0,
    input  dvalid1, output drdy1, input [DATA_WIDTH-1:0] din1,

    output dvalid_y, input drdy_y, output [DATA_WIDTH-1:0] dout_y);


   wire sel1 = dvalid1;

   assign dout_y = (sel1) ? din1: din0;
   assign drdy0 = !sel1 && drdy_y;
   assign drdy1 =  sel1 && drdy_y;
   assign dvalid_y = dvalid0 || dvalid1;

endmodule // STDSYNCH_SRC
// eof
