// HPR L/S Logic Synthesis Library
// (C) 2004, DJ Greaves University of Cambridge, Computer Laboratory.

// Arbiter is also a mux.
// Standard synchronous protocol - simplex aribter, static priority, combinational paths allowed.
// Burst arbiter can only rearbitrate after the last word of a burst.
// The last word of a burst or packet is flagged with the dlast signal.


module STDSYNCH_ARBITER2_STATIC_PRI_BURST
  #(parameter DATA_WIDTH=32)
   (input clk,
    input  reset,

    input  dvalid0, input dlast0, output drdy0, input [DATA_WIDTH-1:0] din0,
    input  dvalid1, input dlast1, output drdy1, input [DATA_WIDTH-1:0] din1,

    output dvalid_y, output dlast_y, input drdy_y, output [DATA_WIDTH-1:0] dout_y);

   reg 	   selected;
   wire    sel1;
   reg 	   idle;
   always @(posedge clk) begin
      if (reset) begin
	 selected <= 0;
	 idle <= 1;
	 end
      else begin
	 selected <= (idle) ? sel1: selected;
	 if (dvalid_y && drdy_y) idle <= dlast_y;
	 end
   end

   assign sel1 = (idle) ?  dvalid1: selected;
   assign dout_y = (sel1) ? din1: din0;
   assign dlast_y = (sel1) ? dlast1: dlast0;
   assign drdy0 = !sel1 && drdy_y;
   assign drdy1 =  sel1 && drdy_y;
   assign dvalid_y = dvalid0 || dvalid1;

endmodule // STDSYNCH_SRC
// eof
