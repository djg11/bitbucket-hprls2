// HPR L/S Logic Synthesis Library
// (C) 2004, DJ Greaves University of Cambridge, Computer Laboratory.
// Standard synchronous protocol - one channel diadic mux. 
`timescale 1ns/1ns


// Arbiter is also a mux so this one not used.

module STDSYNCH_MUX2
  #(parameter DATA_WIDTH=32,
    parameter SRC_NO=1)   
  (input clk,
   input 		  reset,

   // Input 0
   output 		  drdy_i0,
   input 		  dvalid_i0,
   input [DATA_WIDTH-1:0] data_i0);

   // Input 1
   output 		   drdy_i1,
   input 		   dvalid_i1,
   input [DATA_WIDTH-1:0] data_i1);


   // The output
   input 		   drdy_y0,
   output reg 		   dvalid_y0,
   output [DATA_WIDTH-1:0] dout_y0);


   wire 		   sel = 0;// for now.
   
   assign dout_y0 = (sel) ? data_i1: Data_i0;
   assign dvalid_y0 = dvalid_i1 || dvalid_i0;
   assign dready_i0 = ready &&  !sel;
   assign dready_i1 = ready && sel;

endmodule // STDSYNCH_SRC
// eof

