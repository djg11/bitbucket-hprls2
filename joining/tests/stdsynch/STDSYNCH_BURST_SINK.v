// HPR L/S Logic Synthesis Library
// (C) 2004, DJ Greaves University of Cambridge, Computer Laboratory.

// Standard synchronous protocol - simplex burst sink.



module STDSYNCH_BURST_SINK
  #(parameter DATA_WIDTH=32)
   (input clk,
    input 		   reset,
    output reg 		   drdy,
    input 		   dlast,
    input 		   dvalid,
    input [DATA_WIDTH-1:0] din);

   reg [14:0] 		   prbs;
   
   
   wire 		   go = drdy && dvalid;
   wire [3:0] 		   packet	   = din >> 12;
   wire [3:0] 		   word	   = din >> 4;
   wire [3:0] 		   no	   = din >> 0;
   always @(posedge clk) if (reset) begin drdy = 0; prbs = 221; end
   else begin
      prbs <= { prbs[13:0], (prbs[13] ^ prbs[14]) };
      drdy <= prbs[4];
      if (go) $display("%1t, %m: Received Word: dlast=%1d 0x%1h:  %1d %1d %1d", $time, (dlast), din, packet, word, no);
      //if (go) $display("%1t, %m: Received Word: dlast=%1d 0x%1h:  %1d %1d %1d", $time, (dlast), din, packet, word, no);
   end
   

endmodule // STDSYNCH_SRC
// eof

