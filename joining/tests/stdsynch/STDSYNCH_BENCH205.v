// HPR L/S Logic Synthesis Library
// (C) 2004, DJ Greaves University of Cambridge, Computer Laboratory.

// Serdes: demux2 testbench burst.


module STDSYNCH_BENCH205();
   parameter SRC_WIDTH=32;
   parameter SINK_WIDTH=32;   

   reg 			   clk, reset;
   
   wire 		   drdy0, dlast0, dvalid0;
   wire [SRC_WIDTH-1:0]   dbus0;


   wire 		   drdy0_y, dlast0_y, dvalid0_y;
   wire [SINK_WIDTH-1:0]   dbus0_y;

   wire 		   drdy1_y, dlast1_y, dvalid1_y;
   wire [SINK_WIDTH-1:0]   dbus1_y;



   initial begin #3000 $finish; end
   initial begin $dumpfile("vcd.vcd"); $dumpvars(); end
   initial begin reset=1; #33 reset = 0; end
   initial begin clk=0; forever #5 clk = !clk; end   



   STDSYNCH_BURST_SRC #(32, 10) src0(
				     .reset(reset),
				     .clk(clk),
				     .drdy(drdy0),
				     .dlast(dlast0),
				     .dvalid(dvalid0), 
				     .dout(dbus0));
  
   STDSYNCH_BURST_SINK sink0(
			     .reset(reset),
			     .clk(clk),
			     .drdy(drdy0_y),
			     .dlast(dlast0_y),
			     .dvalid(dvalid0_y),
			     .din(dbus0_y));

   STDSYNCH_BURST_SINK sink1(
			     .reset(reset),
			     .clk(clk),
			     .drdy(drdy1_y),
			     .dlast(dlast1_y),
			     .dvalid(dvalid1_y),
			     .din(dbus1_y));

 BURST_DEMUX_1TO2 burst_demux_1to2(    
				       .clk(clk),
				       .reset(reset),
				       .inside_drdy(drdy0), .inside_dvalid(dvalid0), .inside_ddata32(dbus0), .inside_dlast(dlast0),
				       .outside0_drdy(drdy0_y), .outside0_dvalid(dvalid0_y), .outside0_ddata32(dbus0_y), .outside0_dlast(dlast0_y),
				       .outside1_drdy(drdy1_y), .outside1_dvalid(dvalid1_y), .outside1_ddata32(dbus1_y), .outside1_dlast(dlast1_y)					     
				 );


endmodule // STDSYNCH BENCH
// eof

