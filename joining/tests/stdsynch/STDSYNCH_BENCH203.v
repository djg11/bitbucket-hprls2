// HPR L/S Logic Synthesis Library
// (C) 2004, DJ Greaves University of Cambridge, Computer Laboratory.

// Serdes: deserialiser testbench.


module STDSYNCH_BENCH202();
   parameter SRC_WIDTH=8;
   parameter SINK_WIDTH=32;   

   reg 			   clk, reset;
   
   wire 		   drdy0, dlast0, dvalid0;
   wire [SRC_WIDTH-1:0]   dbus0;


   wire 		   drdy_y, dlast_y, dvalid_y;
   wire [SINK_WIDTH-1:0]   dbus_y;



   initial begin #3000 $finish; end
   initial begin $dumpfile("vcd.vcd"); $dumpvars(); end
   initial begin reset=1; #33 reset = 0; end
   initial begin clk=0; forever #5 clk = !clk; end   


   STDSYNCH_SRC #(SRC_WIDTH, 10) src0(
			       //STDSYNCH_BURST_SRC #(32, 10) src0(
				     .reset(reset),
				     .clk(clk),
				     .drdy(drdy0),
				     //.dlast(dlast0),
				     .dvalid(dvalid0), 
				     .dout(dbus0));
  
   STDSYNCH_SINK #(SINK_WIDTH) sink0(
   //STDSYNCH_BURST_SINK sink0(
			     .reset(reset),
			     .clk(clk),
			     .drdy(drdy_y),
			     //.dlast(dlast_y),
			     .dvalid(dvalid_y),
			     .din(dbus_y));
 LITE_DESERIALISER_8TO32 lite_deser8to32(    
     .reset(reset),
     .clk(clk),
     .inside_drdy(drdy0), .inside_dvalid(dvalid0), .inside_ddata8(dbus0), // .insidelast(dlast0),
     .outside_drdy(drdy_y), .outside_dvalid(dvalid_y), .outside_ddata32(dbus_y) // .outside_last(dlast_y)
				 );


endmodule // STDSYNCH_SRC
// eof

