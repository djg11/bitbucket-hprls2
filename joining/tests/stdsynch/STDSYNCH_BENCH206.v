// HPR L/S Logic Synthesis Library
// (C) 2004, DJ Greaves University of Cambridge, Computer Laboratory.

// Serdes: demux2 with ser testbench 


module STDSYNCH_BENCH206();
   parameter SRC_WIDTH=32;
   parameter FAT_SINK_WIDTH=32;
   parameter THIN_SINK_WIDTH=16;      

   reg 			   clk, reset;
   
   wire 		   drdy0, dlast0, dvalid0;
   wire [SRC_WIDTH-1:0]   dbus0;


   wire 		   drdy0_y, dlast0_y, dvalid0_y;
   wire [FAT_SINK_WIDTH-1:0]   dbus0_y;

   wire 		   drdy1_y, dlast1_y, dvalid1_y;
   wire [THIN_SINK_WIDTH-1:0]   dbus1_y;



   initial begin #3000 $finish; end
   initial begin $dumpfile("vcd.vcd"); $dumpvars(); end
   initial begin reset=1; #33 reset = 0; end
   initial begin clk=0; forever #5 clk = !clk; end   



   STDSYNCH_SRC #(SRC_WIDTH, 10) src0(
				     .reset(reset),
				     .clk(clk),
				     .drdy(drdy0),
				     //.dlast(dlast0),
				     .dvalid(dvalid0), 
				     .dout(dbus0));
  
   STDSYNCH_SINK #(FAT_SINK_WIDTH) sink0(
			     .reset(reset),
			     .clk(clk),
			     .drdy(drdy0_y),
			     //.dlast(dlast0_y),
			     .dvalid(dvalid0_y),
			     .din(dbus0_y));

   STDSYNCH_SINK #(THIN_SINK_WIDTH) sink1(
			     .reset(reset),
			     .clk(clk),
			     .drdy(drdy1_y),
			     //.dlast(dlast1_y),
			     .dvalid(dvalid1_y),
			     .din(dbus1_y));

 DEMUX_1TO2_AND_SER burst_demux_1to2_and_ser(    
				       .clk(clk),
				       .reset(reset),
				       .left_drdy(drdy0), .left_dvalid(dvalid0), .left_ddata32(dbus0),  // .left_dlast(dlast0),
				       .right0_drdy(drdy0_y), .right0_dvalid(dvalid0_y), .right0_ddata32(dbus0_y),  //.right0_dlast(dlast0_y),
				       .right1_drdy(drdy1_y), .right1_dvalid(dvalid1_y), .right1_ddata16(dbus1_y) // .right1_dlast(dlast1_y)					     
				 );


endmodule // STDSYNCH BENCH
// eof

