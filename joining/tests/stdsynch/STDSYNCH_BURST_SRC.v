// HPR L/S Logic Synthesis Library
// (C) 2004, DJ Greaves University of Cambridge, Computer Laboratory.
// Standard synchronous protocol - simplex burst source - has a dlast/TLAST signal asserted for last word of packet/burst.



module STDSYNCH_BURST_SRC
  #(parameter DATA_WIDTH=32,
    parameter SRC_NO=1)   
  (input clk,
   input 		   reset,
   input 		   drdy,
   output reg 		   dvalid,
   output 		   dlast, 
   output [DATA_WIDTH-1:0] dout);

   wire [ 3:0] 		   src_no_4bit = SRC_NO;

   reg [14:0] 		   prbs, prbs_fordata;
   reg [7:0] 		   packet_no;
   reg [7:0] 		   count_within_data;		      

   wire 		   go = drdy && dvalid;
   wire pre_valid = prbs[5] & prbs[3];

   always @(posedge clk) if (reset) begin
      prbs = 101 + SRC_NO; dvalid = 0; packet_no = 0; count_within_data = 0; prbs_fordata = 101; end
   else begin
      prbs <= { prbs[13:0], (prbs[13] ^ prbs[14]) };
      if (go) begin
	 if (count_within_data > 0) begin
	    count_within_data = count_within_data - 1;
	 end
	 else begin 
	    prbs_fordata <= { prbs_fordata[13:0], (prbs_fordata[13] ^ prbs_fordata[14]) };
	    packet_no <= packet_no + 1;
	    count_within_data <= (prbs_fordata & 7)+1;
	    end
	 end
      dvalid <= pre_valid;  // Should perhaps not deassert dvalid unless previous request has been served?
   end

   assign dlast = (count_within_data == 0);

   assign dout = (go) ? { prbs_fordata, packet_no, count_within_data, src_no_4bit } : 0;

      

endmodule // STDSYNCH_SRC
// eof

