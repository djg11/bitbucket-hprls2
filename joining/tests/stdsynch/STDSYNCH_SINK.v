// HPR L/S Logic Synthesis Library
// (C) 2004, DJ Greaves University of Cambridge, Computer Laboratory.

// Standard synchronous protocol - simplex sink.

`timescale 1ns/1ns

module STDSYNCH_SINK
  #(parameter DATA_WIDTH=32)
   (input clk,
    input 		   reset,
    output reg 		   drdy,
    input 		   dvalid,
    input [DATA_WIDTH-1:0] din);

   reg [14:0] 		  prbs;


   wire 		   go = drdy && dvalid;


   always @(posedge clk) if (reset) begin drdy = 0; prbs = 221; end
   else begin
      prbs <= { prbs[13:0], (prbs[13] ^ prbs[14]) };
      drdy <= prbs[4];
      if (go) $display("%1t, %m: Received Word 0x%1h", $time, din);
   end
      

endmodule // STDSYNCH_SRC
// eof

