// HPR L/S Logic Synthesis Library
// (C) 2004, DJ Greaves University of Cambridge, Computer Laboratory.

// Standard synchronous protocol - simplex connection with bursts.


module STDSYNCH_BENCH3();
   parameter DATA_WIDTH=32;

   
   wire 		   drdy, dvalid, dlast;
   wire [DATA_WIDTH-1:0]   dbus;
   reg 			   clk, reset;


   initial begin #3000 $finish; end
   initial begin $dumpfile("vcd.vcd"); $dumpvars(); end
   initial begin reset=1; #33 reset = 0; end
   initial begin clk=0; forever #5 clk = !clk; end   
   
   STDSYNCH_BURST_SRC src0(
		     .reset(reset),
		     .clk(clk),
		     .dlast(dlast),
		     .drdy(drdy),
		     .dvalid(dvalid), 
		     .dout(dbus));

   STDSYNCH_BURST_SINK sink0(
		       .reset(reset),
		       .clk(clk),
		       .dlast(dlast),
		       .drdy(drdy),
		       .dvalid(dvalid),
		       .din(dbus));


endmodule 
// eof

