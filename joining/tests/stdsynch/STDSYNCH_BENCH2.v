// HPR L/S Logic Synthesis Library
// (C) 2004, DJ Greaves University of Cambridge, Computer Laboratory.

// Standard synchronous protocol - arbitrated simplex connection


module STDSYNCH_BENCH2();
   parameter DATA_WIDTH=32;

   reg 			   clk, reset;
   
   wire 		   drdy0;
   wire 		   dvalid0;
   wire [DATA_WIDTH-1:0]   dbus0;

   wire 		   drdy1;
   wire 		   dvalid1;
   wire [DATA_WIDTH-1:0]   dbus1;

      wire 		   drdy_y;
   wire 		   dvalid_y;
   wire [DATA_WIDTH-1:0]   dbus_y;



   initial begin #3000 $finish; end
   initial begin $dumpfile("vcd.vcd"); $dumpvars(); end
   initial begin reset=1; #33 reset = 0; end
   initial begin clk=0; forever #5 clk = !clk; end   

   
   STDSYNCH_SRC #(32, 10) src0(
		     .reset(reset),
		     .clk(clk),
		     .drdy(drdy0),
		     .dvalid(dvalid0), 
		     .dout(dbus0));

   STDSYNCH_SRC #(32, 11) src1(
		     .reset(reset),
		     .clk(clk),
		     .drdy(drdy1),
		     .dvalid(dvalid1), 
		     .dout(dbus1));

   STDSYNCH_SINK sink0(
		       .reset(reset),
		       .clk(clk),
		       .drdy(drdy_y),
		       .dvalid(dvalid_y),
		       .din(dbus_y));

   STDSYNCH_ARBITER2_STATIC_PRI_NOBURST arb1(
     .reset(reset),
     .clk(clk),
     .drdy0(drdy0), .dvalid0(dvalid0), .din0(dbus0),
     .drdy1(drdy1), .dvalid1(dvalid1), .din1(dbus1),
     .drdy_y(drdy_y), .dvalid_y(dvalid_y), .dout_y(dbus_y));


endmodule // STDSYNCH_SRC
// eof

