// HPR L/S Logic Synthesis Library
// (C) 2004, DJ Greaves University of Cambridge, Computer Laboratory.

// Standard synchronous protocol - simplex src to sink connection - no ip blocks.


module STDSYNCH_BENCH0();

   parameter DATA_WIDTH=32;

   
   wire 		   drdy;
   wire 		   dvalid;
   wire [DATA_WIDTH-1:0]   dbus;
   reg 			   clk, reset;


   initial begin #1000 $finish; end
   initial begin $dumpfile("vcd.vcd"); $dumpvars(); end
   initial begin reset=1; #33 reset = 0; end
   initial begin clk=0; forever #5 clk = !clk; end   
   
   STDSYNCH_SRC #(DATA_WIDTH) src0(
		     .reset(reset),
		     .clk(clk),
		     .drdy(drdy),
		     .dvalid(dvalid), 
		     .dout(dbus));

   STDSYNCH_SINK #(DATA_WIDTH) sink0(
		       .reset(reset),
		       .clk(clk),
		       .drdy(drdy),
		       .dvalid(dvalid),
		       .din(dbus));


endmodule // STDSYNCH_BENCH0
// eof

