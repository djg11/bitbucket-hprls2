// Sane 0, manual bubble FIFO.

module STDSYNCH_FIFO1(    
/* portgroup= abstractionName=stdsynch32 pi_name=inside */
    input 	  inside_dvalid,
    output 	  inside_drdy,
    input [31:0]  inside_ddata32,
    
/* portgroup= abstractionName=stdsynch32 pi_name=outside */
    output 	  outside_dvalid,
    input 	  outside_drdy,
    output reg [31:0] outside_ddata32,
    
/* portgroup= abstractionName=L2590-vg pi_name=net2batchdirectoratenets10 */
    input 	  clk,
    input 	  reset);

   reg 	    sane0;
   
 always   @(posedge clk )  begin 
      //Start structure cvtToVerilogdropped/1.0
      if (reset)  begin 
         sane0 <= 0;
      end
      else
	   if (inside_drdy && inside_dvalid) begin
	      sane0 <= 1;
	      outside_ddata32 <= inside_ddata32;
	      end
	   else if (outside_drdy && outside_dvalid) sane0 <= 1;	 
	 
       end 

    assign inside_drdy = !sane0;
    assign outside_dvalid = sane0;
endmodule
// eof (HPR L/S Verilog)
