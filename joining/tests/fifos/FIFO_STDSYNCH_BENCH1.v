// HPR L/S Logic Synthesis Library
// (C) 2004, DJ Greaves University of Cambridge, Computer Laboratory.


// Testbench



module STDSYNCH_BENCH1();
   parameter DATA_WIDTH=32;

   reg clk, reset;
   initial begin #1000 $finish; end
   initial begin $dumpfile("vcd.vcd"); $dumpvars(); end
   initial begin clk=0; forever #5 clk = !clk; end
   initial begin reset = 1; clk = 0; #33 reset = 0; end
   wire 		   inside_drdy;
   wire 		   inside_dvalid;
   wire [DATA_WIDTH-1:0]   inside_dbus;
   wire 		   outside_drdy;
   wire 		   outside_dvalid;
   wire [DATA_WIDTH-1:0]   outside_dbus;


   
   STDSYNCH_SRC src0(
		     .clk(clk),
		     .reset(reset),		     
		     .drdy(inside_drdy),
		     .dvalid(inside_dvalid), 
		     .dout(inside_dbus));

   always @(posedge clk) if (inside_drdy && inside_dvalid) $display("%1t: Input word observed %1h", $time, inside_dbus);
   always @(posedge clk) if (outside_drdy && outside_dvalid) $display("%1t: Output word observed %1h", $time, outside_dbus);
   STDSYNCH_FIFO1 fifo0(
			.clk(clk),
			.reset(reset),
			.inside_drdy(inside_drdy),
			.inside_dvalid(inside_dvalid), 
			.inside_ddata32(inside_dbus),
			.outside_drdy(outside_drdy),
			.outside_dvalid(outside_dvalid), 
			.outside_ddata32(outside_dbus));


   STDSYNCH_SINK sink0(
     .clk(clk),
     .reset(reset),		       
     .drdy(outside_drdy),
     .dvalid(outside_dvalid),
     .din(outside_dbus));


endmodule // STDSYNCH_SRC
// eof

