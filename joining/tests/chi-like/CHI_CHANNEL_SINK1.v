// HPR L/S Logic Synthesis Library
// (C) 2004, DJ Greaves University of Cambridge, Computer Laboratory.
//
// CHI-like single channel- simplex sink

`timescale 1ns/1ns


module CHI_CHANNEL_SINK1
  #(parameter DATA_WIDTH=32,
    parameter CREDIT_POOL=2,
    parameter SINK_NO=3)   
  (input clk,
   input 		  reset,
   input  		  FLITPEND, // Clock wake up
   input 		  FLITV,    // Valid
   input [DATA_WIDTH-1:0] FLIT,     // Data
   output  reg		  LCRDV     // Cedit return
   );

   integer 		  credit_to_return;
   reg 			  last_pending;
   reg [14:0] 		  sink_prbs;

   wire returnx = sink_prbs[5] & sink_prbs[3];


   always @(posedge clk) if (reset) begin
      LCRDV <= 0;
      credit_to_return = CREDIT_POOL;
      last_pending <= 0;
      sink_prbs = 101 + SINK_NO; end
   else begin
      last_pending <= FLITPEND;
      if (last_pending != FLITV) $display("%1t: %m: FLITV (%1d) does not match previous FLITPEND (%1d)", $time, FLITV, last_pending);
      if (FLITV) $display("%1t: %m: FLIT value %1x", $time, FLIT);      
      sink_prbs <= { sink_prbs[13:0], (sink_prbs[13] ^ sink_prbs[14]) };
      if (returnx && credit_to_return > 0) begin
	 LCRDV <= 1;
	 credit_to_return = credit_to_return - 1;
	 end
      else LCRDV <= 0;
      if (FLITV) credit_to_return = credit_to_return + 1;
      if (credit_to_return > CREDIT_POOL) $display("%1t: %m: Credit exceeds pool", $time, credit_to_return, CREDIT_POOL);      

   end





      

endmodule // STDSYNCH_SRC
// eof

