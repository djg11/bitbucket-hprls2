// HPR L/S Logic Synthesis Library
// (C) 2004, DJ Greaves University of Cambridge, Computer Laboratory.

// CHI-like channel - basic simplex test


module CHI_BENCH1();

   parameter DATA_WIDTH=32;
   reg 			   clk, reset;
   
   wire 		   FLITV_0, FLITPEND_0, LCRDV_0;
   wire [DATA_WIDTH-1:0]   FLIT_0;
   
   reg	 		   FLITV_S0, FLITPEND_S0, LCRDV_S0;
   reg [DATA_WIDTH-1:0]     FLIT_S0;

   reg	 		   FLITV_S1, FLITPEND_S1, LCRDV_S1;
   reg [DATA_WIDTH-1:0]     FLIT_S1;

   wire 		   FLITV_2, FLITPEND_2, LCRDV_2;
   wire [DATA_WIDTH-1:0]   FLIT_2;



   initial begin #3000 $finish; end
   initial begin $dumpfile("vcd.vcd"); $dumpvars(); end
   initial begin reset=1; #33 reset = 0; end
   initial begin clk=0; forever #5 clk = !clk; end   


   always @(posedge clk) if (reset) begin
   	  FLIT_S0 <= 0;   	  FLITV_S0 <= 0;   	  FLITPEND_S0 <= 0;   	  LCRDV_S0 <= 0;
   	  FLIT_S1 <= 0;   	  FLITV_S1 <= 0;   	  FLITPEND_S1 <= 0;   	  LCRDV_S1 <= 0;	  
   	  end
	  else begin
	     FLIT_S0 <= FLIT_0;  FLITV_S0 <= FLITV_0;  FLITPEND_S0 <= FLITPEND_0;  LCRDV_S0 <= LCRDV_2;
		  FLIT_S1 <= FLIT_S0; FLITV_S1 <= FLITV_S0; FLITPEND_S1 <= FLITPEND_S0; LCRDV_S1 <= LCRDV_S0;
          end	  

   assign FLITPEND_2 = FLITPEND_S1;
   assign FLITV_2 = FLITV_S1;
   assign FLIT_2 = FLIT_S1;
   assign LCRDV_0 = LCRDV_S1;

   CHI_CHANNEL_SRC1 #(DATA_WIDTH) src0(
		     .reset(reset),
		     .clk(clk),
		     .FLITPEND(FLITPEND_0),
		     .FLITV(FLITV_0),
		     .FLIT(FLIT_0),
		     .LCRDV(LCRDV_0));  

						


  CHI_CHANNEL_SINK1 #(DATA_WIDTH) sink0(
		     .reset(reset),
		     .clk(clk),
		     .FLITPEND(FLITPEND_2),
		     .FLITV(FLITV_2),
		     .FLIT(FLIT_2),
		     .LCRDV(LCRDV_2));  


endmodule // CHI_BENCH1

// eof

