// HPR L/S Logic Synthesis Library
// (C) 2004, DJ Greaves University of Cambridge, Computer Laboratory.
//
// Simplex,credited stage.

`timescale 1ns/1ns


module CREDITED_STAGE1
  #(parameter DATA_WIDTH=32,
    parameter INITIAL_CREDIT=0)   
  (input clk,
   input 		       reset,

   //Std synch input port.
   input [DATA_WIDTH-1:0]      ddata,
   input 		       dvalid,
   output 		       drdy, 

   //Credit port.
   output 		       FLITPEND, // Clock wake up
   output reg 		       FLITV, // Valid 
   output reg [DATA_WIDTH-1:0] FLIT, // Data
   input 		       LCRDV         // Cedit return
   );

   wire 		   i_flitv;    // Valid 
   wire [DATA_WIDTH-1:0]   i_flit;     // Data


   always @(posedge clk) if (reset) begin
      FLIT <= 0;
      FLITV <= 0;
      end
   else begin
      FLIT <= i_flit;
      FLITV <= i_flitv;
      end

   assign FLITPEND = i_flitv;
   
   STD_TO_CHI1_INNER #(DATA_WIDTH, INITIAL_CREDIT)
   src_inner(
	     .clk(clk), .reset(reset),
	     .ddata(ddata), .drdy(drdy), .dvalid(dvalid),
	     .I_FLIT(i_flit),	     .I_FLITV(i_flitv),	     .LCRDV(LCRDV)
	     );
endmodule // CHI_CHANNEL_SRC1_INNER
//


//
module STD_TO_CHI1_INNER
  #(parameter DATA_WIDTH=32,
    parameter INITIAL_CREDIT=0)   
  (input clk,
   input 		   reset,


   //Std synch input port.
   input [DATA_WIDTH-1:0]      ddata,
   input 		       dvalid,
   output 		       drdy, 

   //Credit port.
   output reg 		   I_FLITV,       // Valid 
   output reg [DATA_WIDTH-1:0] I_FLIT,    // Data
   input 		   LCRDV          // Cedit return
   );

   integer 		  bridge_credit;   


   always @(posedge clk) if (reset) begin
      bridge_credit = INITIAL_CREDIT; 
      I_FLIT <= 0; I_FLITV <= 0;
   end
   else begin

      if (dvalid && bridge_credit > 0) begin

	 I_FLIT <= ddata;
	 I_FLITV <= 1;
	 bridge_credit = bridge_credit - 1;
      end
      else begin
	 I_FLITV <= 0;
	 I_FLIT <= 0;
      end
      if (LCRDV) bridge_credit = bridge_credit + 1;
   end

   assign drdy = bridge_credit > 0;
     
endmodule // STD_T_CHI1_INNER

// eof

