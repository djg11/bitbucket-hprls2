// HPR L/S Logic Synthesis Library
// (C) 2004, DJ Greaves University of Cambridge, Computer Laboratory.
//
// Simplex,credited stage.

`timescale 1ns/1ns


module CREDITED_STAGE1
  #(parameter DATA_WIDTH=32)
  (input clk,
   input 		       reset,


   //Credit port IN.
   input 		       IN_FLITV, // Valid 
   input [DATA_WIDTH-1:0]      IN_FLIT, // Data
   output  		       IN_LCRDV, // Cedit return

   //Credit port OUT.
   output 		       FLITPEND, // Clock wake up
   output  		       OUT_FLITV, // Valid 
   output [DATA_WIDTH-1:0] OUT_FLIT, // Data
   input 		       OUT_LCRDV  // Cedit return

   );

   reg 			       p_IN_LCRDV; // Cedit return
   wire			       p_OUT_FLITV; // Valid

   assign #3 IN_LCRDV = p_IN_LCRDV;
   assign #3 OUT_FLITV = p_OUT_FLITV; 

   reg [DATA_WIDTH-1:0]        stage_data;
   reg 			       logged;
   integer 		  outside_credit;   
   integer 		  starting_credit;		  

   always @(posedge clk) if (reset) begin
      starting_credit = 1;
      outside_credit = 0;      
      p_IN_LCRDV = 0;
      logged = 0;
      //p_OUT_FLITV = 0;
   end
   else begin
      if (IN_FLITV) stage_data <= IN_FLIT;

      p_IN_LCRDV = 0;
      if (OUT_LCRDV) p_IN_LCRDV = 1;
      if (!p_IN_LCRDV && starting_credit > 0) begin
	 p_IN_LCRDV = 1;
	 starting_credit = starting_credit - 1;
	 end

      if (p_OUT_FLITV) begin
	 outside_credit = outside_credit - 1;
	 logged = 0;
	 end
	
      if (IN_FLITV) begin
	 if (logged) $display("%1t: %m: Buffer overrun with %1x", $time, IN_FLIT);
	 logged = 1;
      end
      
      if (OUT_LCRDV) outside_credit = outside_credit + 1;
   end

   assign p_OUT_FLITV = logged && outside_credit > 0;
   assign #3 OUT_FLIT = (p_OUT_FLITV) ? stage_data: 0;
   
endmodule // CREDITED_STAGE1

// eof

