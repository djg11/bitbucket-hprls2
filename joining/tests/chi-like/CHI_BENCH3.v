// HPR L/S Logic Synthesis Library
// (C) 2004, DJ Greaves University of Cambridge, Computer Laboratory.

// Exploring credited stages


module CHI_BENCH3();

   parameter DATA_WIDTH=32;
   reg 			   clk, reset;
   
   wire 		   FLITV_0, LCRDV_0;
   wire [DATA_WIDTH-1:0]   FLIT_0;
   
   wire	 		   FLITV_S0;
   reg 			   LCRDV_S0;
   wire [DATA_WIDTH-1:0]    FLIT_S0;

   reg 			    FLITV_S1;
   wire 		    LCRDV_S1;
   reg [DATA_WIDTH-1:0]     FLIT_S1;


   initial begin #3000 $finish; end
   initial begin $dumpfile("vcd.vcd"); $dumpvars(); end
   initial begin reset=1; #33 reset = 0; end
   initial begin clk=0; forever #5 clk = !clk; end   

   //wire #3 xl = LCRDV_0;   

   CHI_CHANNEL_SRC1 #(DATA_WIDTH) src0(
				       .reset(reset),
				       .clk(clk),
				       .FLITPEND(FLITPEND_0),
				       .FLITV(FLITV_0),
				       .FLIT(FLIT_0),
				       .LCRDV(LCRDV_0));  

   //always @(posedge clk) if (LCRDV_0) $display("%m Return to src %1t", $time);
   //always @(posedge clk) if (xl) $display("%m Return to src %1t xl", $time);   

   CREDITED_STAGE1 #(DATA_WIDTH) stage0(
					.reset(reset),
					.clk(clk),
					.IN_FLITV(FLITV_0),
					.IN_FLIT(FLIT_0),
					.IN_LCRDV(LCRDV_0),
					
					.OUT_FLITV(FLITV_S0),
					.OUT_FLIT(FLIT_S0),
					.OUT_LCRDV(LCRDV_S0));  

   always @(posedge clk) if (reset) begin
      FLIT_S1 <= 0;   	  FLITV_S1 <= 0;   	    	  LCRDV_S0 <= 0;	  
   end
   else begin
      FLIT_S1 <= FLIT_S0; FLITV_S1 <= FLITV_S0; LCRDV_S0 <= LCRDV_S1;
   end	  


   CHI_CHANNEL_SINK1 #(DATA_WIDTH) sink0(
					 .reset(reset),
					 .clk(clk),
					 .FLITPEND(FLITV_S0),
					 .FLITV(FLITV_S1),
					 .FLIT(FLIT_S1),
					 .LCRDV(LCRDV_S1));  

endmodule // CHI_BENCH3

// eof

