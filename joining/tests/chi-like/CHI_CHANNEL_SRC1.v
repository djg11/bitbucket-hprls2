// HPR L/S Logic Synthesis Library
// (C) 2004, DJ Greaves University of Cambridge, Computer Laboratory.
//
// CHI-like single channel- simplex source

`timescale 1ns/1ns


module CHI_CHANNEL_SRC1
  #(parameter DATA_WIDTH=32,
    parameter SRC_NO=1,
    parameter INITIAL_CREDIT=0)   
  (input clk,
   input 		   reset,
   output  		   FLITPEND,     // Clock wake up
   output  		   FLITV,        // Valid 
   output  [DATA_WIDTH-1:0] FLIT,     // Data
   input 		   LCRDV         // Cedit return
   );

   wire 		   i_flitv;    // Valid 
   wire [DATA_WIDTH-1:0]   i_flit;     // Data

   reg 			   p_FLITV;     // Valid 
   reg [DATA_WIDTH-1:0]    p_FLIT;      // Data
   
//   always @(posedge clk) if (LCRDV) $display("%m Return to src %1t", $time);
   
   always @(posedge clk) if (reset) begin
      p_FLIT <= 0;
      p_FLITV <= 0;
      end
   else begin
      p_FLIT <= i_flit;
      p_FLITV <= i_flitv;
      end

   
   assign #3 FLIT = p_FLIT;
   assign #3 FLITV = p_FLITV;   

   assign #3 FLITPEND = i_flitv;
   
   CHI_CHANNEL_SRC1_INNER #(DATA_WIDTH, SRC_NO, INITIAL_CREDIT)
   src_inner(
	     .clk(clk), .reset(reset),
	     .I_FLIT(i_flit),
	     .I_FLITV(i_flitv),
	     .LCRDV(LCRDV)
	     );
endmodule // CHI_CHANNEL_SRC1_INNER
//


//
module CHI_CHANNEL_SRC1_INNER
  #(parameter DATA_WIDTH=32,
    parameter SRC_NO=1,
    parameter INITIAL_CREDIT=0)   
  (input clk,
   input 		   reset,
   output  		   I_FLITV,       // Valid 
   output [DATA_WIDTH-1:0] I_FLIT,        // Data
   input 		   LCRDV          // Cedit return
   );


   reg 			   p_I_FLITV;     // Valid 
   reg [DATA_WIDTH-1:0]    p_I_FLIT;      // Data

   assign #3 I_FLIT = p_I_FLIT;
   assign #3 I_FLITV = p_I_FLITV;   

   integer 		  src_credit;   
   wire [ 3:0] 		  src_no_4bit = SRC_NO;
   
   reg [14:0] 		  prbs, prbs_fordata;
   reg [12:0] 		  count_for_data;		   
   
   wire pre_valid = prbs[5] & prbs[3];

   always @(posedge clk) if (reset) begin
      prbs = 101 + SRC_NO; src_credit = INITIAL_CREDIT; count_for_data = 0; prbs_fordata = 101;
      p_I_FLIT <= 0; p_I_FLITV <= 0;
   end
   else begin
      prbs <= { prbs[13:0], (prbs[13] ^ prbs[14]) };
      if (pre_valid && src_credit > 0) begin
	 prbs_fordata <= { prbs_fordata[13:0], (prbs_fordata[13] ^ prbs_fordata[14]) };
	 count_for_data <= count_for_data + 1;
	 p_I_FLIT <= { prbs_fordata, count_for_data, src_no_4bit };
	 p_I_FLITV <= 1;
	 src_credit = src_credit - 1;
      end
      else begin
	 p_I_FLITV <= 0;
	 p_I_FLIT <= 0;
      end
      if (LCRDV) src_credit = src_credit + 1;
   end


endmodule // CHI_CHANNEL_SRC1_INNER

// eof

