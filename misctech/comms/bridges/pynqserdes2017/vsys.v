//
// Kiwi Scientific Acceleration  
// University of Cambridge, Computer Laboratory.
//
// vsys.v - A test wrapper for RTLSIM execution environment
//
`timescale 1ns/1ns

module SIMSYS();
   reg clk, reset;
   initial begin reset = 1; clk = 0; #33 reset = 0; end
   always #5 clk = !clk;

   initial begin # (1 * 100 * 1000) $display("Finish HDL simulation on timeout %t.", $time); $finish(); end
   initial begin $dumpfile("vcd.vcd"); $dumpvars(); end

   wire ser1, ser2;


   reg [3:0] shifter;
   always @(posedge clk) if (reset) shifter <= 0; else shifter <= { shifter[2:0], ser1 };

   wire 	    scrambler_enable = 1;
   
   assign ser2 = shifter[1];
   parameter pwidth = 32;
   wire [pwidth-1:0] parout;
   MYSER_TX  #(.pwidth(pwidth))
   the_tx(.clk(clk), 
		   .reset(reset),
		   .scrambler_enable(scrambler_enable),
		   .serout(ser1),
		   .parin(btn[1])
		   );
 
   MYSER_RX #(.pwidth(pwidth))
   the_rx(.clk(clk), 
	  .reset(reset),
	  .scrambler_enable(scrambler_enable),
	  .serin(ser2),
	  .parout(parout)
	  );

//   always @(posedge clk) begin
//      $display("%1t,  pc=%1d, codesent=%h" , $time, the_dut.bevelab10nz, 0);
//   end
   
endmodule
// eof


