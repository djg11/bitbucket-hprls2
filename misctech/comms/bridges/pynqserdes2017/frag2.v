
   // =============================================================================================================
   // Use a PLL to create an x4 version of the serial link baud clock for local operations.

   wire 		    ck_src = pynq_sw[1] ? divx: serdes_clk_in;

   wire 		    serdes_clk_in;
   wire 		    tx1_clkfbout, tx1_clkfbin;

   // We will operate at 62.5 MHz. Baudclk will be one quarter of that.  16ns x 4 = 64ns
   
   PLL_BASE # (
	       .CLKIN_PERIOD(64),  // 64ns for BaudClk of 15.625 MHz
	       .CLKFBOUT_MULT(48),
	       .CLKOUT0_DIVIDE(12), 
	       // .CLKOUT1_DIVIDE(10), // unused
	       .COMPENSATION("SOURCE_SYNCHRONOUS")
	       ) the_pll (
			  .CLKOUT0(mainclk_raw),
			  //.CLKOUT1(pclk_raw),
			  .CLKIN(ck_src),
			  //.RST(pll_reset) 
			  .CLKFBIN(tx1_clkfbin),     .CLKFBOUT(tx1_clkfbout)
			  );

   BUFG tx1_clkfb_buf (.I(tx1_clkfbout), .O(tx1_clkfbin));
   BUFG mainclk_buf(mainclk, mainclk_raw);



   // 
   //================================================================================
   //  Serial/Deserialiser - Inter Board Link
   
   /* portgroup=LinkStatus abstractionName=AUTOMETA_DuplexInterFPGALink_LinkStatusSQ0_UNIT */
   wire 		    serdes_LinkStatus_req;
   wire 		    serdes_LinkStatus_ack;
   wire [31:0] 		    serdes_LinkStatus_return;
   
   /* portgroup=Read64 abstractionName=AUTOMETA_DuplexInterFPGALink_Read64SQ0_UNIT */
   wire 		    serdes_Read64_req;
   wire 		    serdes_Read64_ack;
   wire [63:0] 		    serdes_Read64_return;
   
   /* portgroup=Write64 abstractionName=AUTOMETA_DuplexInterFPGALink_Write64SQ1_U64 */
   wire 		    serdes_Write64_req;
   wire 		    serdes_Write64_ack;
   wire [63:0] 		    serdes_Write64_writed;
   
   /* portgroup=net batch2 abstractionName=nokind */
   //wire 		    serdes_scrambler_enable;
   wire 		    serdes_ser_data_out;
   wire 		    serdes_ser_clk_out;
   wire 		    serdes_ser_data_in;


   wire [1:0] 		    serbusout, serbusin;
   wire 		    baudclk, serout;
   wire 		    perror, in_synch, scrambler_enable;


      
   OBUFDS p0_out (.I(serdes_ser_clk_out), .O(pmod_ja_diff_out_p[0]), .OB(pmod_ja_diff_out_n[0]));
   OBUFDS p1_out (.I(serdes_ser_data_out), .O(pmod_ja_diff_out_p[1]), .OB(pmod_ja_diff_out_n[1]));

   IBUFDS #(.DIFF_TERM("false")) p0_in (.O(serdes_clk_in), .I(pmod_ja_diff_in_p[0]), .IB(pmod_ja_diff_in_n[0]));
   IBUFDS #(.DIFF_TERM("false")) p1_in (.O(serdes_ser_data_in), .I(pmod_ja_diff_in_p[1]), .IB(pmod_ja_diff_in_n[1]));      

   
   wire 		    serdes_LinkStatus_okled;  // Interesting challenge - how to hard bond this out from C#/System Integrator: Please explain.
   wire 		    serdes_RemoteStatus_okled;
   assign		    scrambler_enable = 1;

   DuplexInterFPGALink joy_side_serdes(
				       .reset(!ARESET_N),
				       .clk(mainclk),
				       
				       /* portgroup=LinkStatus abstractionName=AUTOMETA_DuplexInterFPGALink_LinkStatusSQ0_UNIT */
				       .LinkStatus_req(serdes_LinkStatus_req),
				       .LinkStatus_ack(serdes_LinkStatus_ack),
				       .LinkStatus_return(serdes_LinkStatus_return),
    
				       /* portgroup=Read64 abstractionName=AUTOMETA_DuplexInterFPGALink_Read64SQ0_UNIT */
				       .Read64_req(serdes_Read64_req),
				       .Read64_ack(serdes_Read64_ack),
				       .Read64_return(serdes_Read64_return),
				       
				       /* portgroup=Write64 abstractionName=AUTOMETA_DuplexInterFPGALink_Write64SQ1_U64 */
				       .Write64_req(serdes_Write64_req),
				       .Write64_ack(serdes_Write64_ack),
				       .Write64_writed(serdes_Write64_writed),
				       /* portgroup=net batch2 abstractionName=nokind */

				       .LinkStatus_okled(serdes_LinkStatus_okled),
				       .RemoteStatus_okled(serdes_RemoteStatus_okled),
				       .scrambler_enable(scrambler_enable),
 				       .ser_data_out(serdes_ser_data_out),
				       .ser_clk_out(serdes_ser_clk_out),
				       .ser_data_in(serdes_ser_data_in));





