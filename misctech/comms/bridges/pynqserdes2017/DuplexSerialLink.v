
// Kiwi-callable design for instantiation on fixed resources of an FPGA blade.

module DuplexInterFPGALink(    
			       input 		    clk,
			       input 		    reset,

			       

/* portgroup=LinkStatus abstractionName=AUTOMETA_DuplexInterFPGALink_LinkStatusSQ0_UNIT */
			       input 		    LinkStatus_req,
			       output reg 	    LinkStatus_ack,
			       output signed [31:0] LinkStatus_return,
    
/* portgroup=Read64 abstractionName=AUTOMETA_DuplexInterFPGALink_Read64SQ0_UNIT */
			       input 		    Read64_req,
			       output reg 	    Read64_ack,
			       output [63:0] 	    Read64_return,
    
/* portgroup=Write64 abstractionName=AUTOMETA_DuplexInterFPGALink_Write64SQ1_U64 */
			       input 		    Write64_req,
			       output reg 	    Write64_ack,
			       input [63:0] 	    Write64_writed,
    
/* portgroup=net batch2 abstractionName=nokind */
			       

			       input 		    scrambler_enable,
 			       output 		    ser_data_out,
			       output 		    ser_clk_out,
			       input 		    ser_data_in,

			       output RemoteStatus_okled,
			       output LinkStatus_okled
);
//

   parameter pwidth = 68;
   wire 			     rx_insynch;
   wire 			     folded_insynch; 			     
   assign RemoteStatus_okled = folded_insynch;
   assign LinkStatus_okled = rx_insynch;

   
// To get a FIFO paradigm we qualify each broadside word with a valid bit. The valid bit toggles each time new live data is present.
// Data format - 64+2 bits - the top extra 2 bits are a { synch_fold } and { data_valid }. 
   
//-----------------------------------------------------
// Transmitter
   wire [pwidth-1:0] parout;
   reg [63:0] 	     word0;
   reg 		     vtoggle; 
   always @(posedge clk) if (reset) begin
      //parout <= 0;
   end
   else begin
      Write64_ack <= Write64_req; // No backpressure - just drop.
      if (Write64_req) begin
	 word0 <= Write64_writed;
	 vtoggle <= !vtoggle;
      end
   end

   assign parout = { rx_insynch, vtoggle, word0 };
      
   MYSER_TX  #(.pwidth(pwidth))
   the_tx(
	  .clk(clk), 
	  .reset(reset),
	  .scrambler_enable(scrambler_enable),
	  .serout(ser_data_out),
	  .baudclk(ser_clk_out),
	  .parin(parout)
	  );

//-----------------------------------------------------
// Receiver

   wire [pwidth-1:0] parin;
   
   MYSER_RX #(.pwidth(pwidth))

   the_rx(.clk(clk), 
	  .reset(reset),
	  .scrambler_enable(scrambler_enable),
	  .serin(ser_data_in),
	  .parout(parin),
	  .in_synch(rx_insynch)
	  );


   always @(posedge clk) if (reset) begin
      Read64_ack <= 0;
   end
   else begin
      Read64_ack <= Read64_req; // No blocking for the moment.
      //if (Write64_req) begin
	 //word0 <= Write64_writed;
	// vtoggle <= !vtoggle;
   end

   assign Read64_return = parin[63:0];
   assign folded_insynch = parin[64+1];

//-----------------------------------------------------
// Status

   always @(posedge clk) if (reset) begin
      LinkStatus_ack <= 0;
   end
   else begin
      LinkStatus_ack <= LinkStatus_req; // TODO - designate 'always ready' in the IP-XACT
      //if (Write64_req) begin
      //word0 <= Write64_writed;
      // vtoggle <= !vtoggle;
   end

   assign LinkStatus_return = { folded_insynch, rx_insynch } ;

endmodule

// eof
