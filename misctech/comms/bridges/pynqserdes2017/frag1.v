   // 
   //================================================================================
   //  Serial/Deserialiser - Inter-Board Link
   
   OBUFDS p0_out (.I(serdes_ser_clk_out), .O(pmod_ja_diff_out_p[0]), .OB(pmod_ja_diff_out_n[0]));
   OBUFDS p1_out (.I(serdes_ser_data_out), .O(pmod_ja_diff_out_p[1]), .OB(pmod_ja_diff_out_n[1]));

   IBUFDS #(.DIFF_TERM("false")) p0_in (.O(serdes_clk_in), .I(pmod_ja_diff_in_p[0]), .IB(pmod_ja_diff_in_n[0]));
   IBUFDS #(.DIFF_TERM("false")) p1_in (.O(serdes_ser_data_in), .I(pmod_ja_diff_in_p[1]), .IB(pmod_ja_diff_in_n[1]));      

   
   wire 		    serdes_LinkStatus_okled;  // Interesting challenge - how to hard bond this out from C#/System Integrator: Please explain.
   wire 		    serdes_RemoteStatus_okled;
   wire scrambler_enable = 1;
   DuplexInterFPGALink joy_side_serdes(
				       .reset(!ARESET_N),
				       .clk(mainclk),
				       
				       /* portgroup=LinkStatus abstractionName=AUTOMETA_DuplexInterFPGALink_LinkStatusSQ0_UNIT */
				       .LinkStatus_req(serdes_LinkStatus_req),
				       .LinkStatus_ack(serdes_LinkStatus_ack),
				       .LinkStatus_return(serdes_LinkStatus_return),
    
				       /* portgroup=Read64 abstractionName=AUTOMETA_DuplexInterFPGALink_Read64SQ0_UNIT */
				       .Read64_req(serdes_Read64_req),
				       .Read64_ack(serdes_Read64_ack),
				       .Read64_return(serdes_Read64_return),
				       
				       /* portgroup=Write64 abstractionName=AUTOMETA_DuplexInterFPGALink_Write64SQ1_U64 */
				       .Write64_req(serdes_Write64_req),
				       .Write64_ack(serdes_Write64_ack),
				       .Write64_writed(serdes_Write64_writed),
				       /* portgroup=net batch2 abstractionName=nokind */

				       .LinkStatus_okled(serdes_LinkStatus_okled),
				       .RemoteStatus_okled(serdes_RemoteStatus_okled),
				       .scrambler_enable(scrambler_enable),
 				       .ser_data_out(serdes_ser_data_out),
				       .ser_clk_out(serdes_ser_clk_out),
				       .ser_data_in(serdes_ser_data_in));

