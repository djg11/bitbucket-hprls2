// Simulation model (partial) of LCD 1602 panel in Verilog RTL. 
//  (C) 1995 DJ Greaves, University of Cambridge, Computer Laboratory.
// May be freely copied provided this notice is retained.
module VIRTUAL_LCD_PANEL_1602(
			inout LCD_DB4_LS, inout LCD_DB5_LS, inout LCD_DB6_LS, inout LCD_DB7_LS,
			input LCD_RS_LS, input LCD_E_LS, input LCD_RW_LS);

   wire [3:0] 		      din4 = { LCD_DB7_LS, LCD_DB6_LS, LCD_DB5_LS, LCD_DB4_LS };

   reg 			      high;
   reg [3:0] 		      saved_nibble;
   reg [7:0] 		      data8;
   reg [6:0] 		      address_counter;
   wire 		      busy_flag = 1'b0; // Never go busy.		      


   reg [3:0] 		      dout4;
   reg 			      oe;
   bufif1(LCD_DB7_LS, dout4[7], oe);
   bufif1(LCD_DB6_LS, dout4[6], oe);
   bufif1(LCD_DB5_LS, dout4[5], oe);
   bufif1(LCD_DB4_LS, dout4[4], oe);


   initial begin oe = 1'd0; high = 1'd0; end

   always @(posedge LCD_E_LS) begin
      if (LCD_RW_LS == 1) begin // read  high or low
	 oe = 1'd1;
	 high <= !high;
	 data8 = { busy_flag, address_counter };
	 // Not sure which read nibble is returned first. This may be wrong.
	 dout4 = (!high) ? data8[7:4]: data8[3:0];
	 $display("%m: rs=%1d rw=%1d    readbyte=0x%2h nibble=0x%1h", LCD_RS_LS, LCD_RW_LS, data8, dout4);	 
      end
   end
   
   always @(negedge LCD_E_LS) begin


      $display("%m: rs=%1d rw=%1d d=%1h", LCD_RS_LS, LCD_RW_LS, din4);
      oe <= #5 1'd0; // disable output after some hold delay


      if (LCD_RW_LS == 0 && !high) begin // write high
	 saved_nibble <= din4;
	 high <= !high;
      end
      
      else if (LCD_RW_LS == 0 && high) begin // write low	 
	 high <= !high;
	 data8 = { saved_nibble, din4 };


	 if (LCD_RS_LS == 1) begin	    // data writes
	    $display("%m, Data byte  0x%2h %1c", data8, data8);
	 end
	 
	 if (LCD_RS_LS == 0) begin	    // command writes
	    if (data8[7:1] == 7'b0011_001) begin // reset code of 0x32 or 0x33
	       high <= 1;
	       $display("%m, Protocol Reset code 3");
	    end

	    else if (data8 == 8'h01) begin
	       $display("%m, SCREEN CLEAR");
	       address_counter <= 0;
	       end
	    else if (data8 == 8'h02)           $display("%m, CURSOR RETURN");
	    else if (data8[7:2] == 6'b0000_01) $display("%m, INPUT SET      0x%2h", data8);
	    else if (data8[7:3] == 5'b0000_1)  $display("%m, DISPLAY SWITCH 0x%2h", data8);
	    else if (data8[7:4] == 4'b0001)    $display("%m, SHIFT          0x%2h", data8);
	    else if (data8[7:5] == 4'b001)     $display("%m, FUNCTION SET   0x%2h", data8);
	    else if (data8[7:6] == 3'b01)      $display("%m, CG ADDR  SET   0x%2h", data8);
	    else if (data8[7] == 1'b1)         $display("%m, DD ADDR  SET   0x%2h", data8);
	    else                               $display("%m, unrecognised command  0x%2h", data8);	    

	 end
      end
   end
endmodule   

// eof
