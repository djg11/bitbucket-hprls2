(*
 *
 * (C) DJ Greaves 2002 Tenison Technology and University of Cambridge.
 * Import cv2.100 code from sml to FSharp for RTL reset factorisation and blocking assignment elaboration.
 *)
//
// (C) 2020, DJ Greaves, University of Cambridge, Computer Laboratory.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer;
// redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution;
// neither the name of the copyright holders nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

module cv2100

open Microsoft.FSharp.Collections
open System.Collections.Generic

open moscow
open yout
open hprls_hdr
open meox
open verilog_hdr
open abstracte

//open verilog_render
//open abstract_hdr
//open abstracte
//open plot


// (* %: bool * logic * logic *)
let igf_muxdiv_pos = function
    | (v, W_query(gg, v', r, _)) -> (v = v', r, gg)
    | (v, _)                     -> (false, xi_num 0, X_false)


let igf_muxdiv_neg = function
    | (v, W_query(gg, r, v', _)) -> (v = v', r, gg)
    | (v, _)                   -> (false, xi_num 0, X_false)

#if OLD
//(* %: bool * logic *)
let igf_and_inv p1 = function
    | binand(r, lognot(q), _) -> (q = p1, r)
    | binand(lognot(q), r, _) -> (q = p1, r)
    | lognot(q) -> (q = p1, number(1))

    | binand(r, binot(q, _), _) -> (q = p1, r)
    | binand(binot(q, _), r, _) -> (q = p1, r)
    | binot(q, _) -> (q = p1, number(1))
    | _ -> (false, number(0))


// (* %: bool * logic *)
let igf_or_inv = function
    | (p1, binor(r, lognot(q), _)) -> (q = p1, r)
    | (p1, binor(lognot(q), r, _)) -> (q = p1, r)
    | (p1, lognot(q)) -> (q = p1, number(0))
    
    | (p1, binor(r, binot(q, _), _)) -> (q = p1, r)
    | (p1, binor(binot(q, _), r, _)) -> (q = p1, r)
    | (p1, binot(q, _)) -> (q = p1, number(0))
    | (p1, _) -> (false, number (0))


//(* %: bool * logic *)
let igf_and = function
    | (p1, binand(x, y, _)) -> if (p1 = x) then (true, y) else (p1 = y, x)
    | (p1, q) -> (p1 = q, number(1))


// (* %: bool * logic *)
let igf_or = function
    | (p1, binor(x, y, _)) -> if p1 = x then (true, y) else (p1 = y, x)
    | (p1, q)               -> (p1 = q, number(0))
#endif

(*
 * This code factorises flip-flop resets using the standard Verilog synthesis templates.
 *)
type cv_reset_t = CV_ARESET of hexp_t | CV_APRESET of hexp_t


//(* %: event_control_t list *)
let rec cv_verilog_ec_flatten cc = function
    | V_EVC_OR(x, y) -> cv_verilog_ec_flatten (cv_verilog_ec_flatten cc x) y
    | other -> other :: cc

let cv2_genbinot net = xi_blift (xi_not (xi_orred net))

#if NOTINUSE
(*
 * This is called with each permutation of event control ordering until it works because we must divide in the correct order.
 * Return (rhs, resets/presets/, clock) if possible, else None.
 *)
//(* %: (logic * cv_reset_t list * logic) option *)
let cv_verilog_flop_factor_help arg r =
    match arg with
        | [V_EVC_POS clk] -> Some(r, [], E_pos clk)
        | [V_EVC_NEG clk] -> Some(r, [], E_neg clk)

        | (V_EVC_POS p1)::tt -> 
            let (t1, rem1) = igf_and_inv(p1, r)
            let (t3, rem3) = igf_or(p1, r) 
            let tailer1 = cv_verilog_flop_factor_help(tt, rem1)
            let tailer3 = cv_verilog_flop_factor_help(tt, rem3)
            if (t1 && tailer1 <> None) then
                let (r', xl, clk) = valOf(tailer1)
                Some((r', CV_ARESET(p1)::xl, clk)) 
            elif (t3 && tailer3 <> None) then
                let (r', xl, clk) = valOf(tailer3)
                Some((r', CV_APRESET(p1)::xl, clk))
            else None


        | (V_EVC_NEG p1)::tt ->
            let (t1, rem1) = igf_and(p1, r)
            let (t3, rem3) = igf_or_inv(p1, r) 
            let tailer1 = cv_verilog_flop_factor_help(tt, rem1)
            let tailer3 = cv_verilog_flop_factor_help(tt, rem3)
            if (t1 && tailer1 <> None) then
                let (r', xl, clk) = valOf(tailer1)
                Some((r', CV_ARESET(cv2_genbinot p1)::xl, clk))
            elif (t3 && tailer3 <> None) then
               let (r', xl, clk) = valOf(tailer3)
               Some((r', CV_APRESET(cv2_genbinot p1)::xl, clk))
            else None
#endif


// The memoising heap puts query trees in to a normal form that does not make the reset net necessarily sit at the top.
// Hence algebraic division is potentially needed or else pattern matching on the raw input, prior to converting the hexp_t form.
let cv_verilog_flop_factor_help dname candidate_rst rtl =
    muddy (sprintf "%s: let cv_verilog_flop_factor_help %A" dname rtl)

(*
 * Test each permutation of sensitivities in turn and return the first one that worked for all as a possible reset.
 *)
//(* %: logic * cv_reset_t list * logic *)
let rec cv_verilog_flop_factor_test dname arg r =
    match arg with
        | [] ->
            sf(dname +  sprintf ":Asynchronous set or reset cannot be factorised in %A" arg) 
            // (xval, [], r)

        | h::tt ->
            let x = cv_verilog_flop_factor_help dname h r
            if x = None then cv_verilog_flop_factor_test dname tt r
            else valOf x 


// (* %: logic * cv_reset_t list * logic *)
let cv_verilog_flop_factor dname ecf rtl = 
    //let ecf = cv_verilog_ec_flatten [] ec
    let ans = cv_verilog_flop_factor_test dname (list_permutations ecf) rtl

    // type dir_reset_t = (bool * bool * hexp_t) // (is_pos, is_asynch, net)
    let resets =  []
    let clks = muddy "clks---"
    let quotient = muddy "clks---"
    let dtor = { g_null_directorate with clocks=clks; resets=resets; duid=next_duid() }
    // now convert to directorate form
    (dtor, quotient) 



// eof       
