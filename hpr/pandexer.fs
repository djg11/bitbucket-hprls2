//
// CBG Orangepath. HPR L/S Formal Codesign System.
//
//

// This file contains code to "bit-blast" vector expressions into binary.  Hence it has to decide
// on what sort of adders, multipliers and dividers to synthesise.  A trinary variant also exists.
//
// Many other generic expression handling routines are in this file as well.
//
// (C) 2003-17, DJ Greaves, University of Cambridge, Computer Laboratory.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer;
// redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution;
// neither the name of the copyright holders nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

module pandexer

open Microsoft.FSharp.Collections
open System.Collections.Generic
open System.Numerics

open yout
open hprls_hdr
open moscow
open meox
open opath_hdr
open opath_interface
open abstract_hdr
open abstracte
open protocols

open pandex_binary
open pandex_ternary




// Apply pandex to each machine in the VM tree.
let pandex_sp ww pctrl dtor sp =
    match sp with
        | other -> sf (sprintf "pandexer: Other form sp %A" other)



// Apply pandex to each machine in the VM tree.
let rec pandex_vm ww pctrl (ii, vm2) =
// name should be taken from the iname if instantiated or from the component name otherwise ...
    let name =
        match vm2 with 
        | Some(HPR_VM2(minfo, decls, sons, input_execs, assertions)) ->    
            hptos minfo.name.kind
        | _ -> ii.iname
    let m0 = "pandex_vm " + name
    let ww = WF 2 m0 ww "start"
    let pins = new_pandex_pins name
    let agentP = { pctrl with pinopt= Some pins }
    let pandex_agent ww m0 arg = 
        match pctrl.pf_ternary with
            | None ->
                let wo = None
                let xb = pandex_binary ww agentP m0 wo arg
                map xi_blift (xb)
            | Some 3 ->
                let wo = None
                let xb = pandex_ternary ww agentP wo arg
                xb
            | other -> sf (sprintf "pandex L92 other %A" other)
             
    let pandex_agent_lmode ww m0 arg = pandex_agent ww m0 arg // Don't currently need lmode info. Get an error later on unsuitable lmode expressions as required.

    let bpandex_agent ww m0 arg =
        match pctrl.pf_ternary with
            | _ ->
                bpandex_binary ww agentP m0 arg
            //| Some 3 ->
            //    let wo = Some 1 // One trit is sufficient for one bit.
            //    let xb = bpandex_ternary ww agentP arg
            //    xi_orred xb // TODO check this ternary monkey


            //| other -> sf (sprintf "pandex L92 other %A" other)


    let rec pandexer_rtl1 ww dtor arg cc0 =
        //dev_println ("pandexer_rtl1 start " + xrtlToStr arg)
        let ans = pandexer_rtl2 ww dtor arg cc0
        //dev_println ("pandexer_rtl1 finish " + xrtlToStr arg)        
        ans
        
    and pandexer_rtl2 ww dtor arg cc0 =
        match arg with
            | XIRTL_pli(stutter, g0, (f, gis), args) ->
                arg :: cc0 // Leave unchanged for now

            | XRTL((None as stutter), ga, lst) ->
                let sq gg = if xi_istrue gg then [] else [gg]
                let qq arg cc1 =
                    match arg with
                        | Rpli(gg, ((f, gis), so), args) ->
                       //let gg = bpandex_agent ww gg
    // Rpli call form represent 3 different things:
    //  1. They are actually structural instances
    //  2. They are 'finessed' to alternative logic, like sysexit's write to the abend syndrome register.
    //  3. They are geninue PLI calls for rendering in the output RTL.
    // RTL PLI calls are never compiled to gates, but actual args to other structural instances should be. A coding style where such args are busses is suitable, since the drivers of the busses will be separately handled (even for lmode outargs). This is presumably elsewhere ensured.
                            Rpli(gg, ((f, gis), so), args)::cc1 // Leave unprocessed for now


                        | Rarc(gg, ll, rr) ->
                            let m0 = "Rarc. LHS=" + xToStr ll
                            let gg:hbexp_t = bpandex_agent ww (m0 + " guard") gg
                            let ll:hexp_t list = pandex_agent_lmode ww m0 ll            
                            let rr:hexp_t list = pandex_agent ww (m0 + " rhs") rr
                            let rec gen cc = function
                                | ([], _)    -> cc
                                | (ll, [])   -> gen cc (ll, [xi_zero])  // Missing inputs, buffer to zero.
                                | (h::tt, r::rt)  -> Rarc(gg, h, r)::(gen cc (tt, rt)) 
                            gen cc1  (ll, rr)
                        | other -> sf(sprintf "pandexer RTL L139 other %A" other)
                let bz = List.foldBack qq lst []
                XRTL(stutter, ga, bz)::cc0

            | XIRTL_buf(gg, ll, rr) -> // TODO handle X_x forms coherently
                let m0 = "XIRTL_buf. LHS=" + xToStr ll
                let gg:hbexp_t = bpandex_agent ww m0 gg
                //dev_println (sprintf "pandexer_rtl2 buf  gg done")
                let ll:hexp_t list = pandex_agent_lmode ww m0 ll            
                //dev_println (sprintf "pandexer_rtl2 buf ll done")
                let rr:hexp_t list = pandex_agent ww m0 rr
                //dev_println (sprintf "pandexer_rtl2 buf rr done")
                let rec gen cc = function
                    | ([], _)    -> cc
                    | (ll, [])   -> gen cc (ll, [xi_zero])  // Missing inputs, buffer to zero.
                    | (h::tt, r::rt)  -> XIRTL_buf(gg, h, r)::(gen cc (tt, rt)) 
                //dev_println (sprintf "pandexer_rtl2 buf laneing done")
                gen cc0 (ll, rr)

            | XRTL_nop ss -> (XRTL_nop ss)::cc0
            | other -> sf (sprintf "other from pandexer_rtl1 %A" other)

            
    let collect_pin_setups () = // Deferred work and combinational pins need processing.
        let rec iterate count cc =
            let simple_pins = !pins.m_newassigns
            let further_work = !pctrl.m_deferred_work 
            pins.m_newassigns := []
            pctrl.m_deferred_work := []
            if nullp simple_pins && nullp further_work then cc
            else
                let setup_simple_pin cc (lhs, rhs) = gen_XRTL_buf(X_true, lhs, rhs)::cc
                let cc = List.fold setup_simple_pin cc simple_pins
                let expand_further cc (key_, (lhs, rhs)) = // These subexpressions need pandexing.
                    // lhs is generally a pin for a predicate expression
                    let ll = lhs // :hexp_t list = pandex_agent_lmode ww lhs
                    let rr:hbexp_t = bpandex_agent ww m0 rhs
                    let setup_deferred_pin cc (lhs, rhs) = gen_XRTL_buf(X_true, lhs, rhs)::cc
                    // Leading zeros -
                    setup_deferred_pin cc (ll, xi_blift rr)
                let cc = List.fold expand_further cc further_work
                iterate (count+1) cc // Recurse since further work can generate even more.
        iterate 0 []

    let vm2 =
        match vm2 with
            | None -> None
            | Some(HPR_VM2(minfo, decls, children, execs, assertions)) ->
                let ww = WF 2 (sprintf "pandex_vm %s" (vlnvToStr minfo.name)) ww "start"
                let pandex_exec = function
                    | H2BLK(dtor, body) ->
                        match body with
                            | SP_rtl(rtl_ctl, xrtl) ->
                                let body' = List.foldBack (pandexer_rtl1 ww dtor) xrtl []
                                let pin_setups = collect_pin_setups()
                                    

                                let sp = SP_rtl(rtl_ctl, body' @ pin_setups)
                                H2BLK(dtor, sp)
                let execs = map pandex_exec execs
                let decls =
                    let nd = 32
                    let cpi = { g_null_db_metainfo with kind= "pandexer:" + name;  }
                    let nd = gec_DB_group(cpi, map db_netwrap_null (!pins.m_newnets))

                    nd @ decls
                let ww = WF 2 (sprintf "pandex_vm %s" (vlnvToStr minfo.name)) ww "Finished pandex execs"
                let children = map (pandex_vm ww pctrl) children
                let ww = WF 2 (sprintf "pandex_vm %s" (vlnvToStr minfo.name)) ww "Finished pandex children" 
                Some(HPR_VM2(minfo, decls, children, execs, assertions))
                
    (ii, vm2)

    
let opath_pandex_vm ww op_args vms =
    let control = op_args.c3
    let peek = control_get_s op_args.stagename control op_args.stagename (Some "disable")
    let enable = peek = "enable"
    let baser = control_get_s op_args.stagename control "pandexer-base" (Some "binary")
    let vd = control_get_i op_args.c3 "pandexer-loglevel" 3

    let ww = WF 2 "opath_pandex_vm" ww (sprintf "start peek=%A enabled=%A baser=%s" peek enable baser)
    let pf_ternary = (if baser = "ternary" then Some 3 else None)
    let pctrl = new_pandex_dp control pf_ternary 
    let pctrl = { pctrl with vd=vd }

    //let gatelib = hd(arg_assoc_or_fail stagename (shortname + "-gatelib", c2) )
    //let gatelib = if gatelib = "" || gatelib = "NONE" then None else Some gatelib


    let vms = if enable then map (pandex_vm ww pctrl) vms else vms

    let ww = WF 2 "opath_pandex_vm" ww "finished"
    vms:(vm2_iinfo_t * hpr_machine_t option) list    


    
let install_pandexer() =
    let stagename = "pandexer"
    let shortname = stagename
    let argpattern =
        [
          Arg_enum_defaulting(stagename, ["enable"; "disable"], "enable", "Enable control for this operation");
          Arg_defaulting("pandexer-gatelib", "None", "Bit or trit blast.")
          Arg_enum_defaulting("pandexer-base", ["binary"; "ternary"], "binary", "Convert to binary or base 3 logic.")
          Arg_int_defaulting("pandexer-loglevel", 3,  "Trace level: 3 is default, values closer to 10 increase verbosity.")          
        ]

    install_operator (stagename, "Bit-blast for gate-level synthesis.", opath_pandex_vm, [], [], argpattern)
    ()

// Make appear as a loadable plugin
type Pandex_gen() =
    interface IOpathPlugin with
        member this.OpathInstaller() = install_pandexer()
        member this.MethodName = "install_pandexer"



# eof
