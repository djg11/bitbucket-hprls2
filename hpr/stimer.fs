//
// CBG Orangepath.
// HPR L/S Formal Codesign System.
// (C) 2003/16 DJ Greaves. All rights reserved.
//
//  Static timing analyser.
module stimer

let stimer_banner = "d320 $Id: stimer.fs $ "


(*
 *
 * stimer.sml - Static timing analyser STA.
 *
 * (C) 2003-13 DJ Greaves. Cambridge. CBG.
 *
 * HPR L/S Core component.  
 *
 *)
open Microsoft.FSharp.Collections
open System.Collections.Generic


open hprls_hdr
open meox
open moscow
open yout
open abstract_hdr
open abstracte
open opath_hdr
open linepoint_hdr

type static_times_t = Dictionary<xidx_t, walker_nodedata_t>

type seq_flag_t =
    | STA_seq                    // Sequential but clock domain no yet stored.
    | STA_clkinfo of edge_t list // Sequential clock domain now stored.
    | STA_comb

type sequential_flags_t = Dictionary<xidx_t, seq_flag_t>

type st_t =
    {
        seqflags:   sequential_flags_t
        sta:        static_times_t
        maxtime:    int
    }


let lhs_netof lhs =
    let r0 = xi_driven [] lhs
    map fst r0

type clock_domain_residency_t = ListStore<xidx_t, duid_t>
type clock_domain_comb_candidates_t = ListStore<xidx_t, hexp_t>


type directors_club_t = OptionStore<duid_t, directorate_t> // A directors directory.

type clock_domains_info_t =
        {  
            residency:         clock_domain_residency_t
            candidates:        clock_domain_comb_candidates_t
            directors_club:    directors_club_t
        }

let stimer_rez_walker ww vd title =

    let vdp = false
    let k1 =
        {
          maxtime=   1000
          sta=       new static_times_t()
          seqflags=  new sequential_flags_t()  
        }

    let clock_domain_residency = new clock_domain_residency_t ("clock_domain_residency")
    let clock_domain_comb_candidates = new clock_domain_comb_candidates_t("clock_domain_comb_candidates")
    let directors_club = new directors_club_t("directors club")
    let cdom_info =
        {
            residency=      clock_domain_residency
            candidates=     clock_domain_comb_candidates
            directors_club= directors_club
        }
    let m_expressions = ref 0
    let histogram = Array.create k1.maxtime 0
    let m_entries = ref 0
    let m_hwm = ref 0    
    let null_unitfn arg =
        //vprintln 0 ("Unitfn " + xToStr arg)
        //let _ = rdytime arg
        ()


    let null_sonchange _ _ nn (a,b) =
        if (vdp) then vprintln 0 ("Do Null_sonchange " + xToStr a + " -> " + qToStr b)
        b

    let null_b_sonchange _ _ nn (a,b) =
        if (vdp) then vprintln 0 ("Do Null_b_sonchange " + xbToStr a)
        b

    let rec lfun_nettime_get m1 x =
        let nn = x2nn x
        let abs_nn = abs nn
        let oa =
            let (found, ov) = k1.sta.TryGetValue abs_nn
            if found then Some ov else None
        if oa <> None then
            //if (tf) then vprintln 3 ("retrieved " + xToStr x + " result " + qToStr(valOf oa))
            valOf oa
        else
            match x with
            | X_tuple(_, last_item::_, _) ->
                let ans = lfun_nettime_get (fun () -> "pair") last_item // pairs have no nn for dp
                //if (tf) then vprintln 3 ("pair " + xToStr x + " result " + (qToStr ans))
                ans
            | x when fconstantp x -> STIME_NODE(X_true, 0)
            | x ->
                sf (m1() + sprintf ": nettime_get not yet stored for expression='%s'" (xToStr x))

    let ensure_comb x =
        let n = x2nn x
        let nn = abs n
        let (found, ov) = k1.seqflags.TryGetValue nn
        if found
            then match ov with
                    | STA_comb -> ()
                    | STA_clkinfo _
                    | STA_seq -> hpr_yikes("Net is already tagged sequential when marked comb: " + xToStr x)

            else k1.seqflags.Add(nn, STA_comb)

    let ensure_seq provisf x =
        //dev_println (sprintf "ensure_seq: provisf=%A for %s" provisf (netToStr x))
        let n = x2nn x
        let nn = abs n
        let (found, ov) = k1.seqflags.TryGetValue nn
        if found
            then match ov with
                    | STA_seq -> ()
                    | STA_clkinfo _ -> ()
                    | STA_comb -> hpr_yikes("Net is already tagged combinational when want to mark sequential. net=" + netToStr x)

            else k1.seqflags.Add(nn, STA_seq)

    let log_stime bitlane_ = function
        | STIME_NODE(_, v) -> 
            if v >= k1.maxtime then sf ("stimer" + sprintf ": maxtime static time exceeded %i" k1.maxtime)
            else
                Array.set histogram v (histogram.[v]+1)
                mutinc m_entries 1
                m_hwm := max !m_hwm v
                ()

    let tf = false // Take from recipe/settings please.

    let lfun (pp, g) (dir:directorate_t) lhs rhs =
        let m1() = ("lfun " + title + " lhs=" + xToStr lhs + " g=" + xbToStr g + " rhs=" + xToStr rhs)
        directors_club.add dir.duid dir
        let recordx start_time bitlane lhs_x =
            mutinc m_expressions 1

            if tf then vprintln 0 (sprintf "stimer: recordx clocks=%s for assign to %s bitlane=%i" (sfold edgeToStr dir.clocks) (xToStr lhs_x) bitlane)
            match dir.clocks with
                | [] ->
                    //vprintln 0 (m1() + " comb logged: bnet: support ready at " + qToStr start_time)
                    //if ... if treated as zero delay then need another pass
                    // Here look at support clock domains
                    clock_domain_comb_candidates.add (abs(x2nn lhs)) rhs
                    ensure_comb lhs_x
                    ()

                | elist ->
                    //vprintln 0 (m1() + " seq logged: bnet: support ready at " + qToStr start_time)
                    log_stime bitlane start_time
                    let note xidx = 
                        if length elist = 1 then
                            clock_domain_residency.add xidx dir.duid
                        else hpr_yikes(m1() + sprintf ": Multiple clock edges for assign to %s" (netToStr lhs))

                    let record_domain lhs =
                        match lhs with
                            | X_bnet ff ->
                                let xidx = ff.n
                                note xidx

                            | X_x(X_bnet ff, -1, _) -> // This is a combinational assign, but also serves as a means of noting the clock domain for combinational logic.
                                let xidx = ff.n
                                note xidx

                            | other ->
                                hpr_yikes(m1() + sprintf ": stimer: domain: lhs other form in %s" (xToStr other))
                    app record_domain (lhs_netof lhs)
                    ensure_seq false lhs_x
                        // TODO, with gated clocks, need to eval rdy time for each elist member
                    ()

        let vl_max = function
            | (STIME_NODE(vl, tl), STIME_NODE(vr, tr)) ->if (tl=tr) then STIME_NODE(ix_and vl vr, tl) elif tl>tr then STIME_NODE(vl, tl) else STIME_NODE(vr, tr) // idiom
        let start_time = lfun_nettime_get m1 rhs
        match lhs with

                
            | W_asubsc(X_bnet f, subsc, _) ->
                let subsc_time = lfun_nettime_get m1 subsc
                let start_time = vl_max (start_time, subsc_time)
                recordx start_time -1 (X_bnet f)

                // Array assigns could use the maximum approach, but in most cases these will be synchronous writes and the array should be treated as timing destinations with set-up time.
            | W_asubsc(l, ss, _) -> sf ("stimer unsupported lhs: " + xToStr lhs)

            | X_x(X_bnet ff, -1, _) -> // Combinational assign 
                recordx start_time -1 lhs

            | X_bnet ff ->
                recordx start_time -1 lhs

            // Bit inserts could involve a max function on the bits of a vector if vectorised, or else can keep each bit of the lhs as a timed net in its own right, which is better.  eg:: lhs other blift.bitsel XSEL._byteop [0]
            | X_blift(W_bitsel(X_bnet ff, bitlane, _, _)) ->
                recordx start_time bitlane lhs

            // Further bit inserts             eg W_node.:<-maskcast- C1u(_ptos_newd)                - why this other forms
            | W_node(_, V_cast CS_maskcast, [W_node(_, V_rshift _, [lhs; bitlane], _)], _) ->
                let bitlane = xi_manifest ("bit insert on " + xToStr lhs) bitlane
                recordx start_time bitlane lhs

            | W_node(_, V_cast CS_maskcast, [lhs], _) ->
                //let start_time = lfun_nettime_get m1 rhs
                recordx start_time 0 lhs


            | other -> sf ("stimer: lhs other " + xkey lhs + " " + xToStr lhs)
                
    // Operator function:
    let opfun arg nn bo xo _ soninfo2 =
        let sx() = if not_nonep bo then xbToStr(valOf bo) elif not_nonep xo then xToStr(valOf xo) else "-one-"
        if tf then vprintln 0 ("Opfun " + sx() + " acting on " +  sfold qToStr soninfo2)
        let abs_nn = abs nn
        let (found, ov) = k1.sta.TryGetValue abs_nn
        if found then ov
        else
            let sonscan c = function 
                | STIME_NODE(guard_, q) -> max c q
                | _ -> c
            let _ =
                if nullp soninfo2
                then
                    match bo, xo with
                    | (None, Some X_undef) -> ()
                    | (None, Some (X_bnet ff)) ->
                        vprintln 3 ("Leaf sequential net: " + sx()) 
                        // There is no second pass?  Why would we assume this?
                        //ensure_seq true (valOf xo)// assume sequential on first pass
                        () //let _ = 
                    | (None, Some other) when fconstantp other->  ()
                    | (None, Some other) ->  vprintln 3 ("Stimer: ignored s node with no sons " + sx()) 
                    | (Some other, None) when bconstantp other ->  ()
                    | (Some other, None) ->  vprintln 3 ("Stimer: ignored b node with no sons " + sx())
                    | (None, None) -> () //sf (sprintf "opfun other:  bo=%A  xo=%A"  bo xo)
            let args_rdy = List.fold sonscan (0) soninfo2
            if tf then vprintln 0 ("sons=" + sfold qToStr soninfo2 + "\nArgs ready for " + sx() + sprintf "  %i " args_rdy)
            // ass read or write info in
            let output_rdy = args_rdy + 1 // Unit delay model
            if tf then vprintln 0 (sprintf "Timed nn=%i " nn + sx() + sprintf " result rdy %i " output_rdy)
            // Various bit lanes can be updated under various guards. We union them all somehow ...
            // A global histogram of delay times is kept: of more interest would be the subset histogram of set-up times? ...
            let rr = STIME_NODE(X_false, output_rdy)
            // The timeof_nodes first arg (guard) is a complete dummy when used by this static timer.
            // Bit lanes are ignored.  NB: conerefine implements bit-lane management using sd_intersection_pred and so on. Reuse here for 
            k1.sta.Add(abs_nn, rr) // For each net, a collection of arrivals is kept, corresponding to multiple guarded assigns (disjoint), bus fights (if not disjoint) or different bit lanes.
            rr


    let (_, sSS) = new_walker vd vdp (true, opfun, (fun _ -> ()), lfun, null_unitfn, null_b_sonchange, null_sonchange)
    (cdom_info, vdp, (histogram, k1, m_entries, m_hwm, m_expressions), sSS)

//
// The clock domain of a combinational net is the domain of its support, unless it is an input, in which case it is the domain of its user and the input is not tagged with a domain.  Mechanism for tagging inputs is TBD.
//
//     
let domain_of ww dP (cdom_info:clock_domains_info_t) net =
    let cbase = [g_constant_directorate.duid]
    let searches_conducted = new OptionStore<xidx_t, duid_t list>("searched_conducted_dp")
    let rec domains_of arg =
        let xidx = abs(x2nn arg)
        match searches_conducted.lookup xidx with 
            | Some(lst) -> lst
            | None ->
                let ans = 
                    match cdom_info.residency.lookup xidx with
                        | lst when not_nullp lst -> lst
                        | [] ->
                            match cdom_info.candidates.lookup xidx with
                                | [] ->
                                    vprintln 3 (sprintf "No timing information or clock domain stored for %s  %s" (xkey arg) (netToStr arg)) // This will happen for inputs ... TODO allow markup.
                                    []
                                | rhs_lst ->
                                    let support = map fst (List.fold (xi_support dP) [] rhs_lst)
                                    let doms = 
                                        if conjunctionate xi_constantp support then
                                             vprintln 3 (sprintf "Select constant domain for net since support all constant. net= " + netToStr arg)
                                             cbase
                                        else list_flatten(map domains_of support) // or union
                                    lst_subtract (list_once doms) cbase // We do not list the constant director when there are others.
                searches_conducted.add xidx ans
                ans
                

    domains_of net    
    // end of domain_of

    
// Write a timing report.
// Need to use yout to redirect output please.
let rez_timing_histogram ww m_hwm m_entries histogram =
    if !m_entries = 0 then
        hpr_yikes("rez_timing_histogram: no entries")
        ()
    else
        let _:int array = histogram
        vprintln 0 (sprintf "Maximum static time %i out of %i" !m_hwm !m_entries)
        let m0 = List.fold (fun c s -> histogram.[s]+c) 0 [0..!m_hwm]
        let m1 = List.fold (fun c s -> s*histogram.[s]+c) 0 [0..!m_hwm]       
        let fmean = float(m1) / System.Convert.ToDouble(m0)
        let mean = m1/m0
        vprintln 0 (sprintf "Mean static time %f out of %i" fmean m0)
        let mx0 = List.fold (fun c s -> histogram.[s]+c) 0 [mean+1..!m_hwm]
        let mx1 = List.fold (fun c s -> s*histogram.[s]+c) 0 [mean+1..!m_hwm]       
        let fmeanx =
            if mx0 > 0 then
                let fmeanx = float(mx1)/float(mx0)
                vprintln 0 (sprintf "Mean excess static time %f out of %i" fmeanx mx0)
                fmeanx
            else 0.0
        vprintln 0 (sprintf "CSV max,mean,excess,%i,%f,%f" !m_hwm fmean fmeanx)
        //let _:csv_report_field_t = ans // Should frame as csv_report_field_t please
        ()


// This is an entry point used by other plugins.
let vm2_anal_clock_domains ww0 vd title mc =
    let qm = "vm2_anal_clock_domains " + title
    let ww = WF 2 qm ww0 "start"

    let (cdom_info, vdp, (histogram, k1, m_entries, m_hwm, m_expressions), sSS) = stimer_rez_walker ww vd title

    let _ = walk_vm ww vdp (qm, sSS, None, g_null_pliwalk_fun) mc

    let ww = WF 3 qm ww0 "vm walked"
    vprintln 1 (sprintf "Total of %i expressions walked" !m_expressions)

    let ww = WF 2 qm ww0 "done"
    cdom_info


// Static time of one VM2 machine.
// This can also work out which clock domain(s) combinational logic is in.
let opath_stimer_core ww0 vd title mc =
    let qm = "opath_stimer_core " + title
    let ww = WF 2 qm ww0 "start"

    let (cdom_info, vdp, (histogram, k1, m_entries, m_hwm, m_expressions), sSS) = stimer_rez_walker ww vd title

    let _ = walk_vm ww vdp (qm, sSS, None, g_null_pliwalk_fun) mc

    let ww = WF 3 qm ww0 "vm walked"

    let anal() = rez_timing_histogram ww m_hwm m_entries histogram 

    anal()
    vprintln 3 ("Static time analysis done")
    let ww = WF 2 qm ww0 "done"
    mc 




let opath_stimer_vm ww op_args vms =
    let disabled = 1= cmdline_flagset_validate op_args.stagename ["enable"; "disable" ] 0 op_args.c3
    vprintln 1 (stimer_banner)
    let _ = 
        if disabled then vprintln 1 "Stage is disabled"
        else
            let vd = 3
            vprintln 1 "Starting stimer 1749";    
            let _ = map (opath_stimer_core (WN "stimer_core" ww) vd "plugin") vms
            ()
    vms // Output machine(s) same as input


let install_stimer() =
    let bev_argpattern =
        [
          Arg_enum_defaulting("stimer", ["enable"; "disable"; ], "enable", "Disable this stage");

        ]

    let _ = install_operator ("stimer",  "Static Timing Analyser", opath_stimer_vm, [], [], bev_argpattern)

    ()


(* eof *)
