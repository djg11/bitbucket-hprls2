// CBG Orangepath. HPR L/S Formal Codesign System.
//
//


// This file contains code to "trit-blast" vector expressions into base 3.
// It must decide on what sort of adders, multipliers and dividers to synthesise. 
// The code is written with half a mind to support higher bases as well in the future.
//
// Since it will not have access to the logic minimisers for bexp_t, this code can
// usefully also support base 2 code generation for a fairer comparison.
//
// (C) 2003-23, DJ Greaves, University of Cambridge, Computer Laboratory.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer;
// redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution;
// neither the name of the copyright holders nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

module pandex_ternary

open Microsoft.FSharp.Collections
open System.Collections.Generic
open System.Numerics

open yout
open hprls_hdr
open moscow
open meox

open pandex_binary




// Unsigned
let rec rshift_ternary d e = function
    | h::tt ->
        let d = d + h*3
        let vale = d/2 + e
        let d = d % 2
        let r = vale % 3
        let e = vale / 3
        r :: rshift_ternary d e tt
    | [] -> muddy "foo"

// Unsigned - is balanced the same?
let rec lshift_ternary d e = function
    | h::tt ->
        let d = d + h
        let vale = d*2 + e
        let d = 0
        let r = vale % 3
        let e = vale / 3
        r :: lshift_ternary d e tt
    | [] ->
        if (e > 0) then [e]
        else []




// tw is target width for Verilog-like expression contexts...
let rec pandex_ternary ww aP two arg = 
    let vd = aP.vd
    //dev_println(sprintf "vd was %i" vd)
    let idx = mutinco g_pandex_idx 1
    if vd>=4 then vprintln vd (sprintf "Pandex3 start ii%i "  idx + xkey arg)
    let rr = pandex3_ntr ww aP two arg 
    if vd>=4 then vprintln 4 (sprintf "Pandex3 completed ii%i (of %i) " idx (!g_pandex_idx - 1)) //+ xToStr x
    rr

and bpandex_ternary ww aP arg = 
    let vd = aP.vd
    //dev_println(sprintf "vd was %i" vd)
    let idx = mutinco g_pandex_idx 1
    if vd>=4 then vprintln vd (sprintf "BPandex3 start ii%i "  idx + xbkey arg)
    let rr = bpandex3_ntr ww aP arg 
    if vd>=4 then vprintln 4 (sprintf "BPandex3 completed ii%i (of %i) " idx (!g_pandex_idx - 1)) //+ xToStr x
    rr

    
and pandex3_ntr ww pP two arg =

    let ternary_zero = xi_zero  // TODO depends on balancedf?  Could be 1

    // Ternary sign extend a pandex'd value (lsb-first bool list)
    let pandex3_sex wix arg =  // Here wix is in trits
        match wix with
            | None -> sf(sprintf "pandex3_sex: no width specified for " + sfold xToStr arg)
            | Some width ->
                let rec prex n = function
                    | [] -> if n<=0 then [] else sf "pandex3_sex: cannot do sex on an empty list"
                    | [item] -> if n<=1 then [item] else item :: prex (n-1) [item]
                    | h::tt -> h :: (prex (n-1) tt)
                let r99 = prex width arg        
                r99:hexp_t list

    let ans =
        match arg with
            | X_bnet ff ->
                let f2 = lookup_net2 ff.n   
                if memberp g_ternary_net_attribute f2.ats then [arg] // Already a single-bit trit net: leave alone.
                else
                    let prec = mine_prec_no_float arg                     // Returns 32 bits normally?
                    let dwidth = encoding_width arg
                    if prec = g_bool_prec then
                        // install a level convertor?
                        muddy (sprintf "pandex3: net expression from %s  %s" (xkey arg) (netToStr arg))
                        else
                            let trit_width = (dwidth * 2 + 2)/3
                            let rez_trit_net no =
                                new_ternary_net pP.balancedf (ff.id + "trit_" + i2s no)             
                                //muddy (sprintf "pandex3: net expression from %s  bin_width=%i trit_width=%i" (netToStr arg) dwidth trit_width)
                            map rez_trit_net [0 .. trit_width-1 ]
            | X_net(_) -> [ arg ]
            | X_undef ->  [ arg ]
            | W_query(g, t, f, _) -> 
                let g' = bpandex_binary_with_monkey ww pP "pandex3: query guard" g
                let tt = pandex_ternary ww pP two t
                let ff = pandex_ternary ww pP two f
                let rec kq = function
                    | ([], []) -> []
                    | (a, []) -> kq(a, [ ternary_zero ])
                    | ([], b) -> kq([ ternary_zero ], b)
                    | (a::at, b::bt) -> xi_query(g', a, b) :: kq(at, bt)
                kq(tt, ff)



            | X_num n ->
                if n = 0 then [ ternary_zero ]
                else 
                    let rec k3 = function // This is unbalanced.
                        | 0 -> []  (* lsb first *)
                        | n ->
                            let quot = n/3
                            let remainder = n - 3*quot
                            xi_num remainder :: k3 quot
                    let rec q3_signed = function
                        | 0 -> [ xi_num 2]  // Final sign but: TODO check balanced and so on.
                        | n ->
                            let quot = n/3
                            let remainder = n - 3*quot
                            (xi_num quot) :: q3_signed quot

                    if pP.vd>=5 then vprintln 5 (sprintf "pandex_ternary_ntr X_num %s  wid=%s" (xToStr arg) (i2so two))
                    let ans = if n >= 0 then k3 n else pandex3_sex two (q3_signed (0-1-n))  // Is wix in binary bits or ternary trits?
                    //if pP.vd>=5 then vprintln 5 ("pandex_ternary_ntr X_num " + xToStr aA + " done")
                    ans


            | W_string(ss, XS_withval x, _) -> pandex_ternary ww pP two x
            | X_blift a2 ->
                //xsx let ans = bpandex_ternary ww pP (Some 1) a2
                muddy "L69"


            | W_node(prec, oo, lst, _) -> // Generic diadic vector operators with vector result
                let keep_trailers = (oo=V_bitor||oo=V_xor)

                // Bitwise, OR, AND and XOR need to performed base 9. This is the LCM of 2 and 3.
            //let f = if oo=V_bitand then ix_and elif oo=V_bitor then ix_or else ix_xor
                let rec diog_br = function
                    | ([], []) -> []

                    | (ll::ls, rr::rs) -> // We need two trits of input in both arguments to proceed (six wot?).
                        let fop = muddy (sprintf "need fop for %A" arg)
                        fop ll rr :: diog_br (ls, rs)
                        
                    | (x, [])
                    | ([], x) -> if keep_trailers then x else []

                let rec bitwise_fold = function // associative scalar reduction
                    | [item] -> item
                    | a::b::tt -> bitwise_fold(diog_br(a,b)::tt) 

                let lst' = map (pandex_ternary ww pP None) lst
            
                match oo with
                    | V_bitor | V_bitand | V_xor -> bitwise_fold lst'
                //| V_plus                     -> pand_add1_ternary pP (hd lst', hd(tl lst'))
                //| V_lshift -> pand_lshift_ternary(hd lst', hd(tl lst'), 1)                
                //| V_rshift ss -> pand_rshift_ternary ss prec (hd lst', hd(tl lst'), 1)
                //| V_times  -> pand_times pP prec (hd lst', hd(tl lst')) 
                    | _ -> sf ("pandex3: unsupported diadic op " + xToStr arg)
                
            | other -> muddy (sprintf "pandex3: unsupported expression from %s  %s" (xkey other) (xToStr other))

    ans:hexp_t list

    // This only needs to specially process the top-level expression node, since
    // ternary and higher treatment of nested expressions will eventually come back here via the deferred_work mechanism.



and bpandex3_ntr ww pP two arg =

    let ans =
        match arg with 
            | W_bdiop(oo, [lhs; rhs], iv, _) ->
                muddy "pooh"

            //| W_cover  // We do need to watch out for forms being handed back and forward forever without being processed!
            | other ->
                dev_println(sprintf "pandex3: handing-on expression form %s  %s" (xbkey other) (xbToStr other))
                bpandex_binary ww pP "unfinished code ..." arg


    ans:hbexp_t








// eof
