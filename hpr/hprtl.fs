(*
 * HPR L/S hprtl.fs hardware construction language.
 *
 * All rights reserved. (C) 2003-17, DJ Greaves, University of Cambridge, Computer Laboratory.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *)


//
// CBG HPR L/S HP-RTL library - handy shims for helping create VM2 modules mainly composed of RTL
// (This is a hardware constructon language: compare with HardML and Clash ...)
//

// Support IP-XACT round trip esp for black boxes, bundle wiring, directorate wiring, parameter overrides, dont-care minisimation, TLM interface integration
// NOTE: The net-level and TLM form of a port share the same data netport_pattern_schema, but nearly always differ in the associated handshake schema (e.g. blocking TLM has none), so these are kept separate and joined (just appended quite often) as necessary.


module hprtl

open System.Numerics

open yout
open meox
open moscow
open hprls_hdr
open abstract_hdr
open cvipgen
open protocols
open abstracte


// TODO move this
// NOTE: The net-level and TLM form of a port share the same data netport_pattern_schema, but nearly always differ in the associated handshake schema (e.g. blocking TLM has none), so these are kept separate and joined (just appended quite often) as necessary.


//
// NOTE: Where multiple ports have the same director nets, it is perfectly valid to list the director nets in each binding (DB_group of mgr etc)
// and rendering tools must remove the spare copies when flattening to RTL or whatever, that does not support structured ports.
//
let g_standard_synchronous_netport_schema =  // Standard synchronous protocol... 
    let snets = 
     [ // The order of listed the nets here may be relied on below. Sdir is the direction on the slave/target/completer.
       { g_null_netport_pattern_schema with lname="VALID";    width="1";           sdir="i";    idleval="0"; ats=[(g_control_net_marked, "true")] };
       { g_null_netport_pattern_schema with lname="RDY";      width="1";           sdir="o";    idleval="0"; ats=[(g_control_net_marked, "true")] };
       { g_null_netport_pattern_schema with lname="DATA";     width="DATA_WIDTH";  sdir="i";     };
    ]
    (IPB_custom "standard-synchronous-protocol101", snets)


let g_standard_cc_netport_schema =  // Standard credit-controlled port schema.
    let snets = 
     [ // The order of listed the nets here may be relied on below. Sdir is the direction on the slave/target/completer.
       { g_null_netport_pattern_schema with lname="DATA";     width="DATA_WIDTH";  sdir="i";     };
       { g_null_netport_pattern_schema with lname="VALID";    width="1";           sdir="i";    idleval="0"; ats=[(g_control_net_marked, "true")] };
       { g_null_netport_pattern_schema with lname="CRV";      width="1";           sdir="o";    idleval="0"; ats=[(g_control_net_marked, "true")] };

    ]
    (IPB_custom "standard_cc_protocol", snets)





type temp_structured_netgroup_t = (ipblk_protocol_t(*kind*) * string (*pi_name*) * bool option(*initiator=Some true*) * netport_pattern_schema_t list(*schema*)) * (string * hexp_t) list  // aka a d22

type temporary_hprtl_binding_type_t =
    | HBT_freehand_formals of (hexp_t) list  // lname could be helpful too I think, even for these? ... Plural since several can be listed.
    | HBT_structured_formal of temp_structured_netgroup_t

    | HBT_freehand_bindings of (string * hexp_t) list
    | HBT_structured_bind of string list * temp_structured_netgroup_t // No hierarch allowed here at the moment ...! But lhs arg (formal name) as string list is intended to convey a path in the future.

// The vm passed in can be the actual definition or just a signature. Only the signature is needed for instancing.
// Rides perhaps handled earlier, not here.  The rides are for customising externally provided blocks, but should have already been applied to
// the instances we have ... blah see cvipgen rides squirrelling ..
//
// We are a bit smart with our binding code, supporting automatic wiring of directors, automatic adpation between TLM and net-level interfaces and wiring of interfaces with bi-directional handshake at a single stroke.
// 


type hprtl_standard_mk_t = (WW_t -> (string * hexp_t) list -> hpr_machine_t)   

let hf_munge (key, vale) = ([key], vale)

//
// Generate actual to formal bindings for an instantiated component. Ports can be directors, freehand or structured.   
//
let vm_instance_bind ww vm actuals_rides director_actuals contact_actuals iname =
    match vm with
        | HPR_VM2(minfo, decls, _, _, _) ->
            let kinds = hptos minfo.name.kind
            let msg = (sprintf "hprtl: vm_instance_bind: iname=%s: Start instance of %s" iname kinds)
            let ww = WF 2 "vm_instance_bind" ww msg
            let vd = 3 // Normally 3 please.
            //dev_println msg
            let n_bindings = ref 0
            let (n_rides_bound, n_formals_bound) = (ref 0, ref 0)
            let (freehand_bindings, structured_bindings) = 
                let argue arg (cc, cd) =
                    match arg with
                        | HBT_freehand_bindings sp ->
                            (sp @ cc, cd)// Treat as one set of bindings - must have different names (but that's needed for RTL output (modulo logical name aliases))
                        | HBT_structured_bind(formal_path, actual_path) ->
                            let nv = (formal_path, actual_path) 
                            (cc, nv::cd) // Just flattening, throws away structure. ... Need to be smarter in future with multiple instances of a port kind perhaps? Seems to work as is!
                List.foldBack argue contact_actuals ([], [])

            let freehand_bindings = map hf_munge (director_actuals @ freehand_bindings)
            let m_freehand_bindings = add_assoc_used_flags freehand_bindings
            let m_structured_bindings = add_assoc_used_flags structured_bindings

            let m_actuals_rides = add_assoc_used_flags actuals_rides

            let rec bindate msg status arg (cc, cd) =
                match arg with
                    | DB_leaf(Some formal, _) when status=DB_form_pramor ->
                        let key = xToStr formal
                        match op_assoc_used key m_actuals_rides with
                            | None ->
                                if vd>=4 then vprintln 4 (sprintf "hprtl: iname=%s bindate status=%A no override for instance parameter arg= %s" iname status (key))
                                (cc, cd)
                            | Some net ->
                                (DB_leaf(Some formal, Some net)::cc, cd) // Create a parameter override binding.
                                    
                    | DB_leaf(Some formal, _) when status=DB_form_external ->
                        mutinc n_bindings 1
                        let key = [xToStr formal]
                        match op_assoc_used key m_freehand_bindings with
                            | None -> 
                                hpr_yikes(sprintf "hprtl: iname=%s %s bindate status=%A no binding for instance formal arg= %s" iname msg status (hptos key))
                                (cc, cd)
                            | Some net ->
                                mutinc n_formals_bound 1
                                //dev_println (sprintf "hprtl bind: iname=%s: status=%A: bind key=%s to net=%s" iname status (hptos key) (xToStr net))
                                (cc, DB_leaf(Some formal, Some net)::cd) // Create a net binding.
                                    
                    | DB_group(cpi, lst) when cpi.form=DB_form_local ->
                        //dev_println(sprintf "Delete local %i" (length lst))
                        (cc, cd) // Delete local declarations.

                    | DB_group(cpi, contacts) ->  // For a structured formal port
                        //dev_println (sprintf "hprtl bind: iname=%s: portgroup check %s" iname cpi.pi_name)
                        match op_assoc_used [cpi.pi_name] m_structured_bindings with  // see if it has a matching binding (same port instance name)
                            | None ->
                                let status = cpi.form
                                hpr_yikes(sprintf "hprtl bind: iname=%s: bindate status=%A missing structured binding for port %s ... carry on haphazard" iname status cpi.pi_name)
                                let msg = cpi.pi_name
                                let (cc, cd1) =  List.foldBack (bindate msg status) contacts ([], [])  //If not mached, then carry on (haphazard?) by recursing on the contents but with new status.
                                (cc, gec_DB_group(cpi, cd1) @ cd)

                            | Some binds -> // The formal has a port instance name that matches the name of structured actual port instance name.
                                if vd>=4 then vprintln 4 (sprintf "hprtl bind: iname=%s For pi_name=%s, found structured_bindings=%A" iname cpi.pi_name ("binds"))
                                structured_bindate cpi contacts binds (cc, cd)

                    | other ->
                        hpr_yikes(sprintf "bindate status=%A ignore arg=%A" status (simple_dbToStr other))
                        (cc, cd)
                        
            and structured_bindate cpi contacts ((kind, pi_name, sex, schema), actual_nets) (cc, cd) = // Bind individual nets of a port instance
                if vd>=4 then vprintln 4 (sprintf "structured_bindate commence for %s  %i %i" cpi.pi_name  (length contacts) (length actual_nets))
                let actual_nets = add_assoc_used_flags actual_nets
                let sb arg cd =
                    match arg with // For structured pots, the individual nets are associate on an lname (logical name) basis.
                        | DB_leaf(Some(X_net(lname, _)), Some formal) ->
                            match op_assoc_used lname actual_nets with
                                | Some actual ->
                                    if vd>=4 then vprintln 4 (sprintf "structured_bindate for %s  formal_lname=%s formal_net=%s actual=%s" cpi.pi_name lname (xToStr formal) (xToStr actual))
                                    DB_leaf(Some formal, Some actual)::cd

                                | None ->
                                    verror(sprintf "%s: structure binding for port %s, lname=%s: missing net" kinds cpi.pi_name lname)
                                    cd
                        | _ -> muddy(sprintf  "nested structure port binding found in %s" cpi.pi_name)
                let cd = List.foldBack sb contacts cd
                let unused = List.filter (fun (key, m_used) -> not !m_used) (map fst actual_nets)
                if not_nullp unused then
                    hpr_yikes(sprintf "Instance %s of %s: Port %s: The following interface nets were not connected: " iname cpi.pi_name kinds + (sfold (fst) unused))
                (cc, cd)
                    
            // Rewrite the declaration tree, deleting any locals, but otherwise preserving the structure, and binding where relevant.
            let (bound_rides, bound_actuals) = List.foldBack (bindate "top" DB_form_local) decls ([], [])  // Iterate over the formals to the component, looking for an associative-matching binding.

            
            let unused_rides = List.filter (fun (key, m_used) -> not !m_used) (map fst m_actuals_rides)
            vprintln 3  (sprintf "Instance %s of %s: Used %i (should be %i) parameter overrides." iname kinds (length actuals_rides) (length bound_rides))
            if not_nullp unused_rides then
                vprintln 2  (sprintf "Instance %s of %s: The following parameter overrides were unused: " iname kinds + (sfold (fst) unused_rides))

            let unused_contacts = List.filter (fun (key, m_used) -> not !m_used) (map fst m_freehand_bindings  @ map fst m_structured_bindings)
            if not_nullp unused_contacts then
                hpr_yikes(sprintf "Instance %s of %s: The following bindings were unused: " iname kinds + (sfold (fst>>hptos) unused_contacts))

            let bound_rides =
                let cpi = { g_null_db_metainfo with not_to_be_trimmed=true; form=DB_form_pramor; purpose="hprtl-instance-overrides" }
                gec_DB_group(cpi, bound_rides)
            

            // We can work with the meld or the vm2 to get the formal types ... 

            // Why is director_Bindings null here? Who does this?
            let director_bindings = [] // get_directorate_signature (dir:directorate_t) 
        //let (actuals_decls, contacts) =
            // Superceed the declsrez_actuals code by supporting associative binding?  Holdon, its intrinsically associative in the output form?
            // declsrez_actuals stagename pi_name ctrl "instance"  externally_instantiatedf (fu_meld.formals_rides, actuals_rides, formal_contact_list, actualssch, [])

            if !n_bindings <> !n_formals_bound then hpr_yikes(sprintf "Instance %s of %s bound %i formals out of %i" iname kinds !n_formals_bound !n_bindings)
            HPR_VM2(minfo, bound_rides @ bound_actuals, [], [], []) // All implementation details removed.

//
//               
let stagename = "hprtl"

type abstract_dir3_t = edge_t list(*clocks*) * dir_reset_t list(*resets*) * hexp_t list(*clk_enables*)
type hprtl_directors_club_t = OptionStore<duid_t, directorate_t> // A directors directory.

let g_hprtl_director_database = new OptionStore<abstract_dir3_t, string>("hprtl_director_database") // Abstract form
let g_hprtl_directors_club = new hprtl_directors_club_t("hprtl_directors_club")                     // Concrete form

let note_director ww dir =
    match g_hprtl_directors_club.lookup dir.duid with
        | None ->
            g_hprtl_directors_club.add dir.duid dir
        | Some ov_ ->
            ()

let get_managed_director_identifier ww abstract_dir3 = // Abstract form.
    let (clocks, resets, clk_enables) = abstract_dir3
    match g_hprtl_director_database.lookup abstract_dir3 with
        | Some duid -> duid
        | None ->
            let duid = "gmd_" + next_duid()            
            g_hprtl_director_database.add abstract_dir3 duid
            //let dir = { g_null_directorate with duid=duid; clocks=clocks; clk_enables=clk_enables; resets=resets; }
            duid
let hprtl_in_same_file = true // TODO do not hardwire


let g_m_hprtl_defined_kinds = new OptionStore<ip_xact_vlnv_t * squirrel_string_t, vm2_iinfo_t * hpr_machine_t option>("hprtl_defined_kinds")
let g_m_hprtl_defs = ref [] // Same inventory in flat list form.

let g_hprtl_director_formals = "hprtl-director-formals"

let save_definition ww vm =
    let externally_instantiatedf = true    
    let minfo = hpr_minfo vm
    let key = (minfo.name, minfo.squirrelled)
    match g_m_hprtl_defined_kinds.lookup key with
        | Some _ ->
            hpr_yikes(sprintf "hprtl: save_definition: DMTO for %s" (vlnvToStr minfo.name))
            ()
        | None ->
            let rez_opath1_ii stagename externally_instantiatedf i_name kind_vlnv =
                        { g_null_vm2_iinfo with
                              kind_vlnv=            kind_vlnv // { g_canned_default_iplib  with kind=kkey }
                              iname=                i_name
                              definitionf=          true
                              externally_provided=  false
                              external_instance=    externally_instantiatedf 
                              preserve_instance=    true
                              in_same_file=         hprtl_in_same_file
                              generated_by=         stagename
                          }

            let ii = rez_opath1_ii stagename externally_instantiatedf "" (hpr_minfo vm).name

            let fu_definition = (ii, Some vm)

            g_m_hprtl_defined_kinds.add key  fu_definition
            mutadd g_m_hprtl_defs fu_definition // Same inventory in flat list form.

let hx_attributes ats = map (fun (k, v) -> Nap(k, v)) ats

let hx_report_data data = [ Napnode(g_csv_atts, hx_attributes data) ]



    
// Defining a component kind starts with creating an instance of this (or else read in from IP-XACT (sans behaviour)).
// Dictionary g_m_hprtl_defined_kinds avoids kinds being DMTO (defined more than once).
// What does nbm stand for?   Some sort of module.  It's a component kind anyway.
type nbm_t(ww0, name, mirrorable, rides_and_defaults) = class
    let vd = 3
    let kinds = name
    let _ = vprintln 3 (sprintf "hprtl: Start definition of component %s" name)
    let portmeta = [] // Not being used.
    let kind_vlnv=  { vendor= !g_m_xact_op_vendor; version= !g_m_xact_op_version; library= !g_m_xact_op_library;  kind=[kinds] }

    let _:(string * hexp_t) list = rides_and_defaults // Module parameters and defaults.
    let    m_parameters=       ref []                 // aka portmeta ... internal use (within HPR L/S), not expected for RTL render.  

    let    m_freehand_contact_schema=   ref []// Formal contacts that are not part of any method group (directorate nets typically).
    let    m_mgr_melds=                 ref []// Methods in their method groups
    let    mirrorable=                  mirrorable// If any methods are non-mirrorable, the whole FU is non mirrorable (additional instances cannot be freely made owing to internal state).

    // Extensions for an implementation


    let m_directors_used=         ref []
    let m_closed=          ref None // This is none when signature still under definition.
    let m_clkdir =         ref (g_null_directorate, []) // Current clock domain for new logic
    let m_freehand_locals =      ref []
    let m_structured_locals =    ref []        
    let m_assertions=            ref []


    let set_clkdir ww abstract_dir3 = // Change clock domain for the next seqs added.
        let (clocks, resets, clk_enables) = abstract_dir3
        let duid = get_managed_director_identifier ww abstract_dir3
        let dir = { g_null_directorate with duid=duid; clocks=clocks; clk_enables=clk_enables; resets=resets; } // The director deabstract step
        note_director ww dir
        // Dont yet add to m_directors_used since could be purely comb. But then missing if passed to child? FIXME
        let director_bindings = get_directorate_signature dir |> List.unzip |> fst
        m_clkdir := (dir, director_bindings) // My current director and the director bindings to be used for subsequent instances that need them.
        ()
        
    // The default directorate:
    let _ = set_clkdir ww0 ([E_pos g_clknet], [(true, false, g_resetnet)], [])        

// This finishes definition for the schema and implementation for an FU (or any hardware component, perhaps also with TLM entry points.), ready for instantiation.
    let a_fu_end ww = // This end is not really the end since children may need additional directing and our local behaviour may too. TODO!
            let block_meld = 
                { kinds=            kinds
                  parameters=       portmeta
                  formals_rides=    rides_and_defaults
                  contact_schema=   !m_freehand_contact_schema  // Freehand ports (just ad hoc connections).
                  mgr_melds=        !m_mgr_melds                // Structured  ports.
                  mirrorable=       mirrorable
                }

            if not_nonep !m_closed then hpr_yikes(sprintf "Closed already kind=%s " (kinds))
            m_closed := Some block_meld
            // User may not need the block meld, but perhaps add an interlock to show it has been closed. Directors get added.
            block_meld

    let m_execs =        ref []            // Executable code in various forms (DIC/RTL/FSM/AST...)
    let m_seq_rtl =      ref []
    let m_comb_rtl =     ref []    
    let m_sons =         ref []

    let m_rides_used =   ref []

    let rides_or_meta_lookup additional msg defs key =
        if starts_digit key then key
        else
            mutaddonce m_rides_used key
            // c.f. port_eval_int 
            match op_assoc key (additional @ rides_and_defaults) with // Ignores portmeta (for now)
                | Some vale -> xToStr vale
                | None ->
                    hpr_yikes(sprintf "Defining %s: msg=%s. No entry found for %s.  Using %s." name msg key defs)
                    defs
                
    let flush_seq() = // Flush this on change of clock domain.
        match !m_seq_rtl with
            | [] -> () // If it is empty, the director is not saved.
            | lst ->
                let dir = fst !m_clkdir
                m_seq_rtl := []
                let gec_seq (g, lhs, rhs) = Rarc(g, lhs, rhs)
                let xrtl = XRTL(None, X_true, map gec_seq lst)
                mutaddonce m_directors_used dir.duid
                let rtl_ctl:rtl_ctrl_t = { id=kinds + "_seq" }
                mutadd m_execs (H2BLK(dir, SP_rtl(rtl_ctl, [xrtl])))

    let flush_comb() =
        match !m_comb_rtl with
            | [] -> ()
            | lst ->
                m_comb_rtl := []
                let gec_comb (g, lhs, rhs) = XIRTL_buf(g, lhs, rhs)
                let xrtl = map gec_comb lst
                let combinational_directorate = g_null_directorate  // You may refresh duid if you like.
                let rtl_ctl:rtl_ctrl_t = { id=kinds + "_comb" }
                mutadd m_execs (H2BLK(combinational_directorate, SP_rtl(rtl_ctl, xrtl)))


    let externally_instantiatedf = true // This is the 'normal' RTL situation (rather than in-line definitions).  This decision is replicated in save_definition: TODO.
                
    // Finish the definition of a component/FU. Parent (or testbench) must separately make instances of it.
    let a_finish_definition ww atts1 =
        let ww = WF 3 "hprtl_finish_definition" ww (sprintf "kind=%s" kinds)
        let fu_meld = if nonep !m_closed then sf(sprintf "hprtl: meld not closed for %s" kinds) else valOf !m_closed
        flush_comb()
        flush_seq()

        // This removes logical name annotations which we dont want (the .N() form)
        let unbind_ = function // We could instead retrieve the original from the directors directory, if there is such a thing?
            //| DB_leaf(Some formal, Some actual) -> DB_leaf(None, Some actual)
            | other -> other

        let autoformal cc cpi items =
            match cpi.autoconnect with
                | Some "director" ->
                    singly_add ("director", DB_group(cpi, items)) cc
                | other ->
                    hpr_yikes(sprintf "hprtl: kind=%s Ignoring other form autoconnect %A" kinds other)
                    cc
                    
        // Scan for autoconnected nets on child instances and handle appropriately ... which currently just means passing them up in our own formals (no local generation or modification).
        // The child bindate should make the connections as a matter of course.
        let (autoformals_on_sons, autoformal_pi_names) =
            let rec child_autoconnect cc (ii, vm_o) =
                if ii.definitionf || nonep vm_o then cc
                else
                    let rec decl_scan (cc, cd) = function
                        | DB_leaf _ -> (cc, cd)
                        | DB_group(cpi, items) when not_nonep cpi.autoconnect ->
                            (autoformal cc cpi items, cpi.pi_name::cd)
                        | DB_group(cpi, items) ->
                            List.fold (decl_scan) (cc, cd) items

                    List.fold (decl_scan) cc (hpr_decls(valOf vm_o))
            if vd>=4 then vprintln 4 (sprintf "hprtl: autoformals: Scanning %i children for autoconnect ports" (length !m_sons))
            let (rr, pi_names) = List.fold child_autoconnect ([], []) !m_sons
            if vd>4 then vprintln 4 (sprintf "hprtl: autoformals: kind=%s: Found %i different autoconnect ports present on sons" kinds (length rr))
            (rr, pi_names)
            
        let freehand_formals =
            let boz1 nps = // Freehand or loose nets
                let width = atoi32(rides_or_meta_lookup [] "freehand width" "32" nps.width)
                let io = if nps.sdir = "o" then OUTPUT else INPUT // No flip for component instance freehand formal.
                let net = ionet_prec io { g_default_prec with widtho=Some width; signed=nps.signed } (hx_attributes nps.ats) nps.lname
                ("unbound", net)
           
            map boz1 fu_meld.contact_schema

        let director_formals =  [] // Old freehand way ... no longer used.  

        let director_formals_grouped =
            let boz_dir cc duid =
                let is_present_from_autoconnect = memberp duid autoformal_pi_names
                if is_present_from_autoconnect then cc
                else
                    let dir = valOf_or_fail "L379 - missing director" (g_hprtl_directors_club.lookup duid)
                    let dirsig = get_directorate_signature dir // ((string * net) * net) list
                    let nets = map (fun ((_, net), _)-> DB_leaf(Some net, None)) dirsig // 
                    let cpi = { g_null_db_metainfo with kind= g_hprtl_director_formals; form=DB_form_external; pi_name=dir.duid; not_to_be_trimmed=true; autoconnect=Some "director" }  // This cpi form could be worthwhile pre-stored in the club?
                    gec_DB_group(cpi, nets)::cc
            if vd>=4 then vprintln 4 (sprintf "hprtl: autoformals: kind=%s: Found %i directors to be grouped." kinds (length !m_directors_used))
            list_flatten(List.fold boz_dir [] !m_directors_used)

        let _:decl_binder_t list = director_formals_grouped
            
        let _:(string * decl_binder_t) list = autoformals_on_sons // These are the bindings ...and the metainfo


        // We need to union the son autoformals directors with our local ones since all must be sourced ultimately.
        //dev_println (sprintf "Defining %s: 1/2 director_formals_grouped are " name + sfold simple_dbToStr director_formals_grouped)
        //dev_println (sprintf "Defining %s: 2/2 director_formals_grouped are " name + sfold (fun (a,b) -> a + ":" + simple_dbToStr b) autoformals_on_sons)
        let director_formals_grouped = lst_union (director_formals_grouped) (map snd autoformals_on_sons) 


        let structured_formals =
            let boz2_mgr (mgr:mgr_meld_abstraction_t) cc = // Formal nets bundled as formals method group ports (mgrs).
                let boz2 flipsex_ nps = 
                    let width = atoi32(rides_or_meta_lookup [] "struc-box2" "32" nps.width)
                    let io = if nps.sdir = "o" then OUTPUT else INPUT // No flip for component instance.
                    let netname = mgr.mgr_name + "_" + nps.lname
                    let net = ionet_prec io { g_default_prec with widtho=Some width; signed=nps.signed } (hx_attributes nps.ats) netname
                    DB_leaf(Some (gec_X_net nps.lname), Some net)

                let wrapped_nets = map (boz2 (fst mgr.contact_schema)) (snd mgr.contact_schema)
                let mgr_kind = ipbToStr mgr.hs_protocol
                let cpi = { g_null_db_metainfo with kind=mgr_kind; pi_name=mgr.mgr_name; not_to_be_trimmed=true; form=DB_form_external; purpose="structured formals" } 
                gec_DB_group(cpi, wrapped_nets) @ cc
            List.foldBack boz2_mgr fu_meld.mgr_melds []
           
        vprintln 2(sprintf "hprtl: module definition %s:  %i mgr groups, %i freehand contacts, %i explicit directorate contacts, %i freehand local nets.  %i structured local groups of nets." kinds (length fu_meld.mgr_melds) (length fu_meld.contact_schema) (length director_formals) (length !m_freehand_locals) (length !m_structured_locals))




        let (formals_and_gubbins) =
            let pi_name = kinds // Pass in the kind as the pi_name here since all of the method group nets are atomically rezzed.
            declsrez_formals stagename pi_name director_formals "hprtl-component" externally_instantiatedf (rides_and_defaults, [](*not used*), freehand_formals, []) @ structured_formals

        //dev_println(sprintf "Defining %s: formal_contact_list=^%i formals_and_gubbins=^%i" name (length freehand_formals) (length formals_and_gubbins))

        let locals = // Declare new nets
            let cpi = { g_null_db_metainfo with purpose="freehand locals" }
            gec_DB_group(cpi, map db_netwrap_null !m_freehand_locals) @ !m_structured_locals
        let decls = director_formals_grouped @ formals_and_gubbins @ locals

        let rides0fun (key, value) = Nap(key, value) // Same as re_nap

        //dev_println (sprintf "Formals_and_gubbins decls for %s are " name + sfold simple_dbToStr formals_and_gubbins)
        //dev_println (sprintf "Ultimate decls for %s are " name + sfold simple_dbToStr decls)

        // Sq contains only the used values from the rides env that are used (others present did not affect this block).
        let sq =
            let keys = List.sort !m_rides_used
            let depair cc key =
                let vale = rides_or_meta_lookup [] "sq" key key
                if vale = "" then cc else key + "=" + vale + ":" + cc
            List.fold depair "" keys

        // Definition
        let atts0 = if nullp portmeta then [] else [ Napnode("parameters", map rides0fun portmeta) ]
        let vm = HPR_VM2({g_null_minfo with name=kind_vlnv;  squirrelled=sq; atts=atts0 @ atts1 }, decls, !m_sons, !m_execs, !m_assertions) 

        vm
            
    member x.set_clock_domain ww clkv3 =
        flush_seq()
        set_clkdir ww clkv3


    member x.rides_lookup defs key = rides_or_meta_lookup [] "rides_lookup_misc" defs key 




    member x.add_freehand_locals ww nets = app (mutadd m_freehand_locals) nets

    member x.add_structured_locals ww bundels = // As locals, we don't really care about the structure, but we maintain it .. we do if we interconnect structured formals of children or export them...
        let addb22 ((port_kind, pi_name, sex, schema), nets) =  // This version deprecated for the one in rez2
            //(string * bool option * netport_pattern_schema_t list) * (string * hexp_t) list    
            let cpi = { g_null_db_metainfo with purpose="structured_locals"; portschema_franca=Some(schema, sex); pi_name=pi_name }
            mutadd m_structured_locals (DB_group(cpi, map (snd>>db_netwrap_null) nets))
            ()
            
        map addb22 bundels



        
    member x.add_seq(l, r) = mutadd m_seq_rtl (X_true, l, r)
    member x.add_seq_g g (l, r) = mutadd m_seq_rtl (xi_orred g, l, r)
    member x.add_comb (l, r) = mutadd m_comb_rtl (X_true, l, r)
    member x.add_comb_g g (l, r) = mutadd m_comb_rtl (xi_orred g, l, r)    

    member x.finish_definition ww csv_atts =
        let vmd = a_finish_definition ww csv_atts 
        save_definition  ww vmd
        vmd

    member x.fu_end = a_fu_end

    member x.add_child ww vm layout_domain iname portmeta contact_bindings =

        // The vm passed in can be the actual definition or just a signature. Only the signature is needed for instancing.
        let actuals_rides = portmeta // TODO is this correct/consistent naming with protocols.fs etc. ?
        let vm = vm_instance_bind ww vm actuals_rides (snd !m_clkdir) contact_bindings iname
        // Since the ...
        let iinfo = 
                { g_null_vm2_iinfo with
                      kind_vlnv=            (hpr_minfo vm).name
                      iname=                iname
                      definitionf=          false
                      externally_provided=  false
                      external_instance=    false
                      preserve_instance=    true
                      generated_by=         stagename
                      layout_domain=        layout_domain
                 }

        mutadd m_sons (iinfo, Some vm) 
        ()
        
    // The port gets highly abstracted from hexp_t, only to come down again. Although this looks like a waste of time, it is more
    // compatible with both reading in from IP-XACT or external RTL and with changing the names and widths of nets on an instance basis, which
    // is really handy for rehydrating with different parameters.
    member x.add_ports ww freehand_pi_name formals = // Add a port group - according to an established interface or else free-hand.

        let save_to_schema_freehand = function // Freehand case - add to misc ports rather than method groups (mgrs) for now.  TODO generate some meld on-the-fly...
           | X_bnet ff->
                let (ff, f2) = valOf_or_fail "no such net" (lookup_net_by_xidx_o ff.n)
                let nps =
                   { g_null_netport_pattern_schema with
                       lname=    ff.id
                       chan=     freehand_pi_name
                       sdir=     (if f2.xnet_io=INPUT then "i" else if f2.xnet_io=OUTPUT then "o" else "x") // x means does not flip.
                       width=    i2s(ff.width) // "dwidth" for dynamic rehydration.
                       signed=   ff.signed
                   }

                mutadd m_freehand_contact_schema nps // Freehand ones are added to the block meld.

        let save_to_schema1 = function
            | HBT_freehand_formals hx -> app (save_to_schema_freehand) hx // Freehand, no protocol.
            | HBT_structured_formal((kind, pi_name, sex, schema), nets) ->
                let mgr_meld =
                    {
                        mgr_name=         pi_name
                        method_melds=     [] // No TLM support.
                        contact_schema=   (valOf_or_fail "no upcall/target sex" sex, schema)
                        hs_protocol=      kind
                        hs_vlat=          None
                        is_hpr_cv=        false
                    }
                    
                mutadd m_mgr_melds mgr_meld
        app save_to_schema1 formals

    // Add a structured local or formal to a component, hydrating the schema, which for an I/O forms a port_instance (aka method group mgr).
    // When sex=None it adds a structure local.
    member x.busrez2 ww additional_portmeta sex schema pi_name =
        let vd = 3 //
        let port_instance_name = if pi_name = "" then [] else [ Nap(g_port_instance_name, pi_name) ]
        let gen_formal schema_entry = 
            let width = rides_or_meta_lookup additional_portmeta "structured port net entry" "32" schema_entry.width // DATA_WIDTH string should not be hardwired here?
            let prec = { g_default_prec with widtho=Some(int32 width); signed=schema_entry.signed }
            let io =
                match schema_entry.sdir with
                    | "x" -> INPUT  // Always input, such as clock and reset.
                    | "o" -> if nonep sex then LOCAL else if valOf sex then INPUT else OUTPUT
                    | "i" -> if nonep sex then LOCAL else if valOf sex then OUTPUT else INPUT
                    | "l" when nonep sex -> LOCAL
                    | ss -> sf (sprintf "busrez2 %s other mdir I/O direction string >%s<" pi_name ss)
                    // Replicated from protocols.buildportnet.fs SITE-L2172 TODO keep in step, merge or justify.
            // let fu_instance_name = if ikey = "" then [] else [ Nap(g_fu_instance_name, ikey) ]
            let ats = port_instance_name @ [Nap(g_logical_name, schema_entry.lname)] @ (map (fun (k,v) -> Nap(k, v)) schema_entry.ats)
            let netname = if pi_name = "" then schema_entry.lname else pi_name + "_" + schema_entry.lname
            let formal = ionet_prec io prec ats netname
            if vd >= 5 then vprintln 5 (sprintf "hprtl.buildportnet: sex=%A kind=%s width=%s pi_name=%s rezzed port net " sex name width pi_name + netToStr formal)
            (schema_entry.lname, formal)
        let (schema_name, snets) = schema
        let schema = map (fun (sc:netport_pattern_schema_t) -> { sc with chan=pi_name }) snets
        let formals = map gen_formal schema

        let addb22 ((port_kind, pi_name, sex, schema), nets) =  // This version deprecated for the one in rez2
            //(string * bool option * netport_pattern_schema_t list) * (string * hexp_t) list    
            let cpi = { g_null_db_metainfo with purpose="structured_locals"; portschema_franca=Some(schema, sex); kind=ipbToStr(*don't like*) schema_name; pi_name=pi_name }
            mutadd m_structured_locals (DB_group(cpi, map (snd>>db_netwrap_null) nets))
            ()

        if nonep sex then
            addb22 ((schema_name, pi_name, sex, schema), formals)
            else x.add_ports ww ""  [ HBT_structured_formal((schema_name, pi_name, sex, schema), formals) ]
        ((schema_name, pi_name, sex, schema), formals)


end


// Freehand input                                
let hx_input ww (nbm:nbm_t) (name:string) (width:int) =
    let ats = []
    let ans = ionet_w(name, width, INPUT, Unsigned, ats)
    nbm.add_ports ww "" [ HBT_freehand_formals[ ans ] ]
    ans

let hx_output ww (nbm:nbm_t) (name:string) (width:int) =
    let ats = []
    let ans = ionet_w(name, width, OUTPUT, Unsigned, ats)
    nbm.add_ports ww "" [ HBT_freehand_formals[ ans ] ]
    ans

let hx_registered_output ww (nbm:nbm_t) name bits =
    let ats = []
    let ans = ionet_w(name, bits, OUTPUT, Unsigned, ats)
    nbm.add_ports ww "" [ HBT_freehand_formals[ ans ] ]
    ans

let hx_registered_output_with_reset ww (nbm:nbm_t) (resetval:string) name bits =
    let ats = [ Nap(g_resetval, resetval) ]
    let ans = ionet_w(name, bits, OUTPUT, Unsigned, ats)
    nbm.add_ports ww "" [ HBT_freehand_formals[ ans ] ]
    ans

let hx_register_with_reset ww (nbm:nbm_t) (resetval:string) name (bits: int) =
    let ats = [ Nap(g_resetval, resetval) ]
    let ans = ionet_w(name, bits, LOCAL, Unsigned, ats)
    nbm.add_freehand_locals ww [ans]
    ans

// Create a local net. It can be assigned sequentially or combinationally, but normally not both.  Multiple assignments to it will often have disjoint guards, but priority is given to the last one in any concurrent set.
let hx_logic_s ww (nbm:nbm_t) name (signedf, (bits:int)) = // Please tend to use this instead of direct use of freehand locals in models.
    let ats = [ ]
    let signed = (if signedf then Signed else Unsigned)
    let ans = ionet_w(name, bits, LOCAL, signed, ats)
    nbm.add_freehand_locals ww [ans]
    ans

let hx_logic ww (nbm:nbm_t) name (bits:int) =  hx_logic_s ww nbm name (false, bits)
        
let hx_register ww (nbm:nbm_t) name (bits:int) = hx_logic ww nbm name bits // Just a synonym
    
// Logic functions, such as AND and OR, on single-bit hexp_t expressions.
let hx_xor a b = xi_blift (ix_xor (xi_orred a) (xi_orred b))

let hx_and a b = xi_blift (ix_and (xi_orred a) (xi_orred b))    
let hx_inv v   = xi_blift (xi_not (xi_orred v))
let hx_or a b  = xi_blift (ix_or (xi_orred a) (xi_orred b))

// Any and all operators.
let hx_orl lst = xi_blift (ix_orl (map xi_orred lst))
let hx_andl lst = xi_blift (ix_andl (map xi_orred lst))

let hx_deqd a b = xi_blift (ix_deqd a b)
let hx_dned a b = xi_blift (ix_dned a b)
let hx_dltd a b = xi_blift (ix_dltd a b)

// General lifting of all other operators in case we want to instrument IP blocks
let hx_query g a b = ix_query (xi_orred g) a b // Use this for muxes of any width.
let hx_plus l r = ix_plus l r
let hx_minus l r = ix_minus l r

// Add up the number of ones in a list.
let hx_tally nets =
    let hx_Plus lst =
        if nullp lst then xi_zero
        else List.fold (fun c d -> hx_plus c d) (hd lst) (tl lst)
    hx_Plus nets


let get_schema_name msg = function
    | ((IPB_custom schema_name, _, _, _), _) -> schema_name
    | other ->  sf(sprintf "%s: get schema_name other form %A" msg other)    

let schema00ToStr fullfatf schema00 =
    let wish (sl:netport_pattern_schema_t) =
        if fullfatf then sprintf "%s^%s" sl.lname sl.width else sl.lname
    sfold_colons (map wish schema00)

let d22_to_str d22 =
    let msg = ""
    let ((schema_name, pi_name, sex, schema00), structured_ports) = d22
    let subs_lhs = sfold_colons (map (fun (sl:netport_pattern_schema_t)->sl.lname) schema00)
    let subs_rhs = sfold_colons (map fst structured_ports)    
    let yiker = if subs_lhs = subs_rhs  then "" else sprintf "+yiker(subs_lhs is %s)" subs_lhs
    "D22_" + get_schema_name msg d22 + "/" + pi_name + "/" + subs_rhs + yiker


// Lookup net by name in a busrez (aka d22)
let hx_sformal msg d22 lname =
    let (schema_, nets) = d22
    match op_assoc lname nets with
        | None -> sf(sprintf "%s: hx_sformal: cannot find lname %s" msg lname)
        | Some net -> net

let hx_sformal2 msg (_, d22) lname = hx_sformal msg d22 lname
    


// FSM compiler.
let hx_compile_sm0 ww (nbm:nbm_t) guards arcs = // Export this to above.
    let gal = length guards
    if (gal < 1) then sf("compile_hx_sm0 : first entry must be current state")
    let m_or_array = ref []
    let pc = hd guards // PC should be first entry
    let compile_arc (grd_vec, actions, next_state) =
        if length grd_vec <> gal then sf ("fsm guard length is wrong length for " + xToStr (hd guards))
        let gterm (a, nv) = ix_deqd a (xi_num nv)
        let grd = xi_blift(ix_andl (map gterm (List.zip guards grd_vec)))
        let perform_action action = mutadd m_or_array (action, grd)
        app perform_action actions
        nbm.add_seq_g grd (pc, xi_num next_state)
        ()
    app compile_arc arcs

    let implement_action (action, whens) =
        let lower (action_, grd) = xi_orred grd
        nbm.add_comb (action, xi_blift(ix_orl (map lower whens)))

    app implement_action (generic_collate fst !m_or_array)

        
    ()


// ----------------------------------------
// An example component kind:
let mk_upcounter ww genv =
    let ww = WF 2 "mk_upcounter" ww "Start"
    let width = 16
    let nbm = new nbm_t(ww, sprintf "COUNTER%i" width, false, genv)
    
    // Formals
    let yy = hx_logic ww nbm "yy" 16 
    let meld_ctr16 = nbm.fu_end ww // Call this after last formal added.

    // Locals
    let counter_value = hx_logic_s ww nbm "counter_value" (false, 16) 

    // Behaviour
    nbm.add_seq(counter_value, ix_plus (xi_one) counter_value)
    nbm.add_comb(yy, counter_value)

    let vmd = nbm.finish_definition ww []
    let ww = WF 2 "mk_upcounter" ww "Finish"    
    vmd


    
let _:hprtl_standard_mk_t = mk_upcounter

// eof    

