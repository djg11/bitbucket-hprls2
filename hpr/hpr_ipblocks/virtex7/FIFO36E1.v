// Virtex-like RTL simulation model

// 32 Kbit FIFO. 32*1024=2^15.  For 72 bit words, 512 x72=36864, so parity byte is not included in the total I think.
module FIFO36E1 #( 
		   //.almost_empty_offset(almost_empty_offset),
 		   //.almost_full_offset(almost_full_offset), parameter integer DATA_WIDTH(72),
		   parameter integer DATA_WIDTH = 72,
		   parameter integer DO_REG = 1,
		   parameter EN_SYN ="FALSE",
		   parameter FIFO_MODE = "FIFO36_72", // Must be this currently
		   parameter FIRST_WORD_FALL_THROUGH = "TRUE"
		     //.init(init),
		     //.srval(srval)
		     ) 

   (
    output 	  almostempty,
    output 	  almostfull,
    // .dbiterr(),
    output [63:0] DO,
    output [7:0]  DOP,
    //.eccparity(),
    output 	  EMPTY,

    output 	  FULL,
    output [11:0] RDCOUNT,
    //.rdcount(tx_rdcount),
    //.rderr(rderr),
    //.sbiterr(),
    output [11:0] WRCOUNT,  // Only values 0 to 511 used we think.
    input 	  WREN,
    //.wrerr(wrerr),
    input [63:0]  DI,
    input [7:0]   DIP,
    input 	  RDEN,
    input 	  RST, 
           //.REGCE(REGCE_PATTERN),
           //.RSTREG(RSTREG_PATTERN),
    input 	  WRCLK,
    input 	  RDCLK
                 );

   // guts missing ... for now
   
endmodule

// eof
