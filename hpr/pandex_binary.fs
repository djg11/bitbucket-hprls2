//
// CBG Orangepath. HPR L/S Formal Codesign System.
//
//

// This file contains code to "bit-blast" vector expressions into binary.  Hence it has to decide
// on what sort of adders, multipliers and dividers to synthesise.  A trinary variant also exists.
//
//
// (C) 2003-17, DJ Greaves, University of Cambridge, Computer Laboratory.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer;
// redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution;
// neither the name of the copyright holders nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

module pandex_binary

open Microsoft.FSharp.Collections
open System.Collections.Generic
open System.Numerics

open yout
open hprls_hdr
open moscow
open meox

let g_pandex_idx = ref 100

// Sign extend a pandex'd value (lsb-first bool list)
let pandex_sex msg no arg =
    if nonep no then
        hpr_warn(sprintf "pandex_sex: no width specified for %s in %s" (sfold xbToStr arg) msg)
        arg
    else
        let rec prex n = function
            | [] -> if n<=0 then [] else sf "cannot do sex on an empty list"
            | [item] -> if n<=1 then [item] else item :: prex (n-1) [item]
            | h::tt -> h :: (prex (n-1) tt)
        prex (valOf no) arg        

type pandex_pins_t =
    {
        m_newnets:       hexp_t list ref
        m_newassigns:    (hexp_t * hexp_t) list ref
        stringkey:       string
    }

let new_pandex_pins stringkey =
    {
        m_newnets=        ref []
        m_newassigns=     ref []
        stringkey=        stringkey
    }



type pandexCtrl_t_ =
 {
     pf_ternary:   int option    // Binary or ternary select: value supported are None and (Some 3).
     balancedf:    bool          // For ternary logic, whether unsigned or balanced is being used.
     p_dp:         Dictionary<int, hbexp_t list>
     control: control_t
     vd:                      int // Verbal diorosity.
     ripple_adder_radix:      int
     //structures:            restr_t list ref;
     pinopt:                  pandex_pins_t option
     m_deferred_work:         (hbexp_t * (hexp_t * hbexp_t)) list ref
 }
         
let new_pandex_dp control pf_ternary =
   {
       pf_ternary=  pf_ternary
       balancedf=   false// for now
       p_dp=        new Dictionary<int, hbexp_t list>()
       control=     control
       vd=          -1 // Debugging off.
       pinopt=               None // site for new_pandex_pins()
       m_deferred_work=      ref []        
       ripple_adder_radix=   2
   }

let rec pand_k oo = function
    | ([], []) -> []
    | (a::at, b::bt) -> ix_diop oo a b :: pand_k oo (at, bt)



(* Zero pad extends the shorter arg with leading zeros. *)
let rec pand_zp = function
    | ([], [], p, q)       -> (rev p, rev q)
    | ([], a::b, p, q)     -> pand_zp([], b, X_false::p, a::q)
    | (a::b, [], p, q)     -> pand_zp(b, [], a::p, X_false::q)
    | (c::cc, d::dd, p, q) -> pand_zp(cc, dd, c::p, d::q)

let pand_zeropad(a, b) = pand_zp(a, b, [], [])

//
// Ripple-carry adder generator.
// Kogge-stone is where?
// Dividers and multipliers are always explicit instances of third-party components or those from cvipgen.fs
//
let pand_add_ripple_binary pP operand_lists =
    //let radix = 2 // From pP is better ...
    let vd = pP.vd
    let rec ripaddx lane (cin:hbexp_t) = function    
        | ([], []) -> [cin]
        | (a::at, b::bt) ->
            let cin = 
                if (lane % pP.ripple_adder_radix) = 0 && not(xi_bconstantp cin) then 
                    let m = valOf_or_fail "pandex_ripple_adder: addx: no pin-save resources" pP.pinopt 
                    let pin = newnet_with_prec g_bool_prec (funique (m.stringkey + "ripadd"))
                    mutadd m.m_newnets pin
                    mutadd m.m_newassigns (pin, xi_blift cin)
                    xi_orred pin
                else cin
            let s = ix_xor a b
            if vd>=5 then vprintln 5 ("    pand_add a = " + xbToStr a )
            if vd>=5 then vprintln 5 ("    pand_add b = " + xbToStr b )
            let c1 = ix_and a b
            if vd>=5 then vprintln 5 ("    pand_add s = " + xbToStr s )
            if vd>=5 then vprintln 5 ("    pand_add c1 = " + xbToStr c1 )
            let c2 = ix_and s cin
            if vd>=5 then
                vprintln 5 ("    pand_add c2 = ... \n")
                vprintln 5 ("    pand_add c2 got  \n")
                //vprintln 5 ("    pand_add c2 got  " + xkey k)
                //vprintln 5 ("  >> " +  xToStr k + "<<\n")
                vprintln 5 ("    pand_add c2 yeild  \n")
                vprintln 5 ("    pand_add c2 = " + (xbToStr c2) )
            (ix_xor s cin)::(ripaddx (lane+1) (ix_or c2 c1) (at, bt))
        | _ -> sf "unppand_added pand_add"

    ripaddx 0 X_false operand_lists

// Perform bitlist subtraction with carry (borrow bar) in.
// Note: subtraction is deprecated and should not occur in normal-form meox expression. Add the negation.
// Also this is missing the pins, so cannot cope with wide words.
let rec pand_subtract_equal_lengths c = function
    | ([], []) -> [c]
    | (a::at, b::bt) -> 
        let b' = xi_not b // Ones complement.
        let s = ix_xor a b' // Half-adder.
        let c1 = ix_and a b'
        // Other half of full adder:
        (ix_xor s c)::(pand_subtract_equal_lengths (ix_or c c1) (at, bt))
    | _ -> sf "pandex subtract differing lengths:  pand_sub"


let pand_add1_binary pP (a,b) = 
    //dev_println ("pand_add1_binary started")
    let two_lists = pand_zeropad(a, b)
    //dev_println (sprintf "add mid started len=%i" (length(fst two_lists)))
    let ans = pand_add_ripple_binary pP two_lists
    //dev_println (sprintf "add done and dusted.  Answer len=%i" (length ans))
    //vprintln 100 ("pand_add1_binary: pandex'd to " + (sfold xToStr ans))
    (ans:hbexp_t list)



let pand_guard g v =
    match g with
        | X_false -> [X_false]
        | X_true -> v
        | _ -> map (fun x -> ix_and g x) v


let rec pand_mux g =
    function
        | ([], []) -> []
        | (a::at, b::bt) -> xi_orred(xi_query(g, xi_blift22 a, xi_blift22 b)) :: (pand_mux g (at, bt))

let pand_muxzp g (a, b) = pand_mux g (pand_zeropad(a, b)) 


let rec pand_rshift (signed) (prec:precision_t) = function
    | (a, [], x) -> a
    | (a, h::t, x) -> 
        let r = pand_rshift signed prec (a, t, x*2)
        let unsigned = prec.signed = Unsigned || signed=Unsigned
        if not unsigned then muddy "pand_rshift signed"
        let rec pand_rshift1 a n = if n=0 || a=[] then a else pand_rshift1 (tl a) (n-1)
        pand_muxzp h (pand_rshift1 r x, r)

let rec pand_lshift = function
    | (a, [], x) -> a
    | (a, h::t, x) -> 
        let rec pand_lshift1 (a, n) = if n=0 then a else X_false::(pand_lshift1(a, n-1))
        let r = (pand_lshift(a, t, x*2))
        pand_muxzp h (pand_lshift1(r, x), r)

let rec pand_times pP prec_ = function // Precision/signed is ignored for now.
    | ([], v) -> [ X_false]
    | (h::t, v) -> pand_add1_binary pP (pand_guard h v, pand_times pP prec_ (t, X_false :: v))


let rec pand_ka oo = function
    | ([], []) -> []
    | (a::at, b::bt) -> 
        let rr = if oo=V_xor then ix_xor a b
                 else if oo=V_bitand then ix_and a b
                 else if oo=V_bitor then ix_or a b
                 else xi_orred(ix_diop g_default_prec oo (xi_blift22 a) (xi_blift22 b)) (* not used *)
        rr :: pand_ka oo (at, bt)
    | _ -> sf "unppand_added pand_ka"


// Used only for strictly binary predicates, so returns hbexp_t.
let deferr_pandex_work pP whom item = // linear search - replace with dictionary please ...
    let pin = 
        match op_assoc item !pP.m_deferred_work with
            | Some (pin, rhs) -> pin
            | None ->
                let prec = g_bool_prec //mine_prec_no_float item
                let m = valOf_or_fail "pandex pancb: no pin-save resources" pP.pinopt 
                let pin = newnet_with_prec prec (funique (m.stringkey + "pancb"))
                mutadd m.m_newnets pin
                mutadd pP.m_deferred_work (item, (pin, item)) // redundant while associng on the rhs anyway.
                pin
    xi_orred pin

// This is creating a binary subtractor that will also require pinning, but its only the carry out that' needed.
let rec pand_dltd_binary r = function
        | ([], []) -> r
        | (a::at, b::bt) ->
            let r = ix_or (ix_and (xi_not a) b) (ix_and (ix_xnor a b) r)
            //vprintln 0 ("pand_dltd_binary a=" + xbToStr a + " b=" + xbToStr b + " r=" + xbToStr r + " r'=" + xbToStr r') 
            pand_dltd_binary r (at, bt)

                
// Signed multiply has one fewer product bits than unsigned if we negate the answer.
//
// With 3 bit unsigned fields the maximum product is 49 which is 110001, needing 6 bits.
// With 3 bit signed fields, ranges are 3*-4 = -12 which is 110100 and -4*-4=16 01000. A 5 bit result almost suffices, since it can hold -16 to +15. The negated product would fit!
//
// The difference between signed and unsigned multiply can be illustrated with -2 * -2 in 3-bit fields
//  -2 is 110  and -4 is 11100
//  -2 is also 6, and 6*6=36 is 100100 - which is the same in the low bits but different above.
//
// Assuming one argument is
// To make a signed multiplier from an unsigned core we need to correct for each argument is negative. A negative argument
// is a subtracted pair: for instance -2 in 3 bits is stored as (8-2).  If we have this as both operands to an unsigned multiplier, we get
//    (8-2)*(8-2) = (64 - 16 - 16 + 4) = 36 = 100100 (as above).  To fix this, we need to add on 16 for each operand.  The 64 is discarded since it is in Bit6 and we
// just want a six-bit result.            
//
#if SPARE
let rec pand_multiplier___ ww P (x, y) = 
    let xl = length x
    let yl = length y
    let rw = max xl yl
    let _ = vprintln 3 ("Pand: multiplier " + i2s xl + "x" + i2s yl)
    if conjunctionate bconstantp x then xgen_mpxconst(x, y)
    elif conjunctionate bconstantp y then xgen_mpxconst(y, x)
    else 
        let fgis = ("MPX", { g_default_native_fun_sig with rv={g_default_prec with widtho=Some rw}; args=[xl; yl] })
        let rn = vectornet_w("o2mpx", rw)       
        let r = ix_pair (xi_apply(fgis, [xi_num rw; xi_num xl; xi_num yl] @ rn :: (map xi_blift22 (x @ y)))) rn
        pandex ww 0 P r
#endif


(*
 * Main entry point
 * Pandex  (binary) converts to bit-blasted form, returning a list of bit lanes, lsb first.  w is a target width, needed for sign extensions.
 *
 *)
and pandex_binary ww aP (msg:string) w x = 
    let vd = aP.vd
    //dev_println(sprintf "vd was %i" vd)
    let idx = mutinco g_pandex_idx 1
    if vd>=4 then vprintln vd (sprintf "Pandex start ii%i "  idx + xkey x)
    let rr = pandex_ntr ww aP msg w x 
    if vd>=4 then vprintln 4 (sprintf "Pandex completed ii%i (of %i) " idx (!g_pandex_idx - 1)) //+ xToStr x
    rr

and bpandex_binary_with_monkey = bpandex_binary // The with_monkey suffix is potentially available for where an expression is to be used as a binary predicate. Currently a NOP. For ternary, it may be more interesting.


// Where bpandex encounteres a vectored expression, it must call back to the appropriate varient of pandex according to base.
// Since ternary code is not in-scope at this point, a pin needs to be logged for the work to be done in a subsequent iteration (all relevant code is available to the iterator).
// Not doing this for binary results in fewer pins which helps with code readibility.
and bpandex_ntr ww (pP:pandexCtrl_t_) (msg:string) bA =
    match bA with
        | X_false    ->  bA 
        | X_true     ->  bA 
        | X_dontcare ->  muddy "bpandex_ntr: xi_dontcare remains" // W_cover form has don't care sets and that mechanism should be being used. 

#if NOCOVER        
        | W_bnode(V_band, [t; f], false, _)  -> [ix_and t f] // WHY FALSE HERE ?   Why no clause for false!
        | W_bnode(V_bor, [t; f], false, _)   -> [ix_or t f]
        | W_bnode(V_bxor, [t; f], false, _)  -> [ix_xor t f]
#endif
        | W_cover(lst, _) ->
            let rec pandex_orred = function
                | [] -> X_false
                | [term] -> term
                | h::t -> ix_or h (pandex_orred t)

            let lf x = (*pandex_orred*) (bpandex_binary ww pP msg (deblit x)) // This is bitwise code!! Not right.?  This should be orred here!
            let rec diog f keep = function
                | ([], []) -> []
                | (l::ls, r::rs) -> f(l, r) :: diog f keep (ls, rs)
                | (x, [])
                | ([], x) -> if keep then x else []
            let rec sqand = function
                | [] -> X_true
                | [item] -> lf item
                | h::t -> ix_and (lf h) (sqand t)
            let rec sqor = function
                | [] -> X_false
                | [term] -> sqand term
                | h::t -> ix_or (sqand h) (sqor t)
            sqor lst 

        | W_bdiop(V_orred, [item], iv, _) -> 
            let ivf x = if iv then xi_not x else x
            let pin_needed = not (is_a_net item) && not_nonep pP.pf_ternary // Do not pin boolean nets since that's just needless overhead. 
            let a0 =
                if pin_needed then
                    deferr_pandex_work pP "orred" bA
                else
                    let lst = pandex_binary ww pP msg None item
                    let rec orred = function
                        | [item] -> item
                        | h::tt -> ix_or h (orred tt)
                        | [] -> sf "orred []"
                    orred lst
            ivf a0

        | W_bdiop(oo, [lhs; rhs], iv, _) -> 
            let pandex_nf_diop_binary ll rr =
                // Not-equal predicate: disjunction of xors.
                let rec pand_dneq r =  function
                    | ([], []) -> r
                    | (a::at, b::bt) -> pand_dneq (ix_or (ix_xor a b) r) (at, bt)

                //vprintln 0 ("pandex W_bdiop start" + xbToStr bA)
                let ans =
                    match oo with
                        | V_dned -> (pand_dneq X_false (pand_zeropad(ll, rr)))
                        | V_deqd -> xi_not(pand_dneq X_false (pand_zeropad(ll, rr)))

                        | V_dltd signed__ -> (pand_dltd_binary X_false (pand_zeropad(ll, rr))) // TODO signed__ is sadly ignored!
                        // a<=b is b>=a is !(b<a)
                        | V_dled signed__ -> xi_not(pand_dltd_binary X_false (pand_zeropad(rr, ll)))

                        | other-> sf("bpandex_binary: W_bdiop: other form " + xbToStr bA)
                //vprintln 0 ("pand bdiop end")
                ans

            let pin_needed = not_nonep pP.pf_ternary
            let a000 =
                if pin_needed then // pin the +ve (non-inverted) version
                    let abs = if iv then xi_not bA else bA
                    deferr_pandex_work pP "bdiop" abs
                else
                    let a0 = pandex_nf_diop_binary (pandex_binary ww pP msg None lhs) (pandex_binary ww pP msg None rhs)
                    if iv then xi_not a0 else a0
            a000


        // 
        | W_bsubsc(l, r, iv, _) ->  // Dynamic bit extract
            let ivf x = if iv then xi_not x else x
            let pin_needed = not_nonep pP.pf_ternary // Do not pin boolean nets since that's just needless overhead. 
            let a000 =
                if pin_needed then deferr_pandex_work pP "bsubsc" bA
                else
                    match pandex_binary ww pP msg (Some 1) (ix_bitand (ix_rshift Unsigned l r) (xi_num 1)) with
                        | [] -> sf "Fewer than one bit in bsubsc"
                        | [onebit] -> ivf onebit
                        | _ -> sf "bpandex: range extract ... ?" // where is this code:
            a000
                        
        | W_bitsel(X_bnet f, bit_no, iv, _) ->  // Static bit extract.  TODO warn about 'bitsel' in base 3?
            let ivf x = if iv then xi_not x else x
            let w = encoding_width (X_bnet f)
            if bit_no < 0 || bit_no >= w then sf(sprintf "pandex: bitsel %i out of range in %s" bit_no (xbToStr bA))
            muddy "Do something with the bit"
            ivf bA


        | other -> sf("bpandex other " + xbToStr other)

and bpandex_binary ww pP (msg:string) x =
    let vd = -1 // -1 for off
    let nn = xb2nn x
    let (found, ov) = pP.p_dp.TryGetValue nn
    if found then hd ov
        else
            let idx = mutinco g_pandex_idx 1
            if vd >=4 then vprintln 4 (sprintf "Bpandex start ii%i " idx + xbToStr x)
            let rr = bpandex_ntr ww pP msg x
            if vd >=4 then vprintln 4 (sprintf "Bpandex end ii%i" idx)
            pP.p_dp.Add(nn, [rr])
            rr
            
and pandex_ntr ww pP (msg:string) (wix:int option) aA =
    let vd = pP.vd // -1 for off
    let gwidth prec =
        if not_nonep wix then valOf wix
        elif not_nonep prec.widtho then valOf prec.widtho // this ordering needs checking on a clause-by-clause basis
        else sf ("pandex_binary: need width in " + xToStr aA)

    match aA with
        | X_bnet f ->
           let dwidth = encoding_width aA
           if vd>=4 then vprintln 4 ("Pandex " + xToStr aA + " dwidth=" + i2s dwidth)
           map (fun x -> ix_bitsel aA x) [0 .. dwidth-1]

        | X_net(_) -> [ xi_orred aA ]
        | X_undef ->  [ xi_orred aA ]
        | W_query(g, t, f, _) -> 
            let g' = bpandex_binary_with_monkey ww pP msg g
            let t' = pandex_binary ww pP msg wix t
            let f' = pandex_binary ww pP msg wix f
            let rec k = function
                | ([], []) -> []
                | (a, []) -> k(a, [ X_false ])
                | ([], b) -> k([ X_false ], b)
                | (a::at, b::bt) -> xi_query(g', xi_blift22 a, xi_blift22 b) :: k(at, bt)
            map xi_orred (k(t', f'))

        | W_string(ss, XS_withval x, _) -> pandex_ntr ww pP msg wix x

        | W_string(ss, _, _) -> muddy "pandex-binary: string with value needed."

        (*| (W_mask(h, l)) ->  
             let zgen n = if n <= 0 then [] else X_false :: zgen(n-1)
             let onegen n = if n <= 0 then [] else X_true :: onegen(n-1)
             let zeros = zgen (min(h,l))
             let ones = onegen (1+abs(h-l))
             in zeros @ ones 
        *)
#if TNUM
        | X_tnum lst -> 
             let rec k n = if wide_zerop n then []
                           else (if wide_oddp n then X_true else X_false) :: (k (wide_divmod(true, n, [2])))
             let rec q n = if wide_zerop n then [X_true] (* Final sign bit *)
                           else (if wide_oddp n then X_true else X_false) :: (q (wide_divmod(true, n, [2])))
             let _ = vprintln 10 ("pandex_ntr X_tnum " + xToStr aA)
             let ans = if wide_negativep lst then q(wide_addsub(false, wide_negate lst, [1]))
                       else if wide_zerop lst then [ X_false ] else k lst
             ans
#endif
        | X_bnum (w, bn, _) -> 
            let rec kk (bn:BigInteger) =
                if bn.Sign <0 then muddy "-ve bnum"
                if bn.IsZero then []        
                else
                    let rem = ref 0I
                    let q = BigInteger.DivRem(bn, 2I, rem)
                    (if (!rem).Sign > 0 then X_true else X_false) :: kk q
            kk bn

        | X_num n ->
            if n = 0 then [ X_false ]
            else 
              //Example:  -4 in a 6 bit field is 111100
             let rec k = function
                 | 0 -> []  (* lsb first *)
                 | n -> (if (n % 2)=1 then X_true else X_false) :: k (n/2)
             let rec q = function
                 | 0 -> [X_true]  (* final negative sign bit *)
                 | n -> (if (n % 2)=0 then X_true else X_false) :: q (n/2)
             if vd>=5 then vprint 5 (i2so wix + " width pandex_ntr X_num " + xToStr aA + "\n")
             let ans = if n >= 0 then k n else pandex_sex msg wix (q (0-1-n))
             if vd>=5 then vprintln 5 ("pandex_ntr X_num " + xToStr aA + " done")
             ans


        // Monadic operator: cast.
        | W_node(prec, V_cast cvtf, [arg], _) ->
            let ans = 
                match cvtf with
                    | CS_typecast ->                  // Just a typecast that may alter a subsequent operator overload selection.
                        pandex_ntr ww pP msg (prec.widtho) arg
                    | CS_maskcast ->                  // Clipping etc to field width.
                        let width = gwidth prec
                        let mask = gec_X_bnum({widtho=Some width; signed=Unsigned}, himask width)
                        pandex_ntr ww pP msg None (ix_bitand mask arg)
                    | CS_preserve_represented_value -> // A major bit-changing convert (e.g. int to float).
                        muddy (sprintf "Unsupported pandex cast " + xToStr aA)

            ans

        // Monadic operator: one's complement.
        | W_node(prec, V_onesc, [xx], _) ->
            let bitlist = pandex_ntr ww pP msg wix xx
            // Should warn if wix has a value different from length bitlist
            map (xi_not) bitlist

        // Monadic operator: two's complement.
        | W_node(prec, V_neg, [xx], _) ->
            let rhs = pandex_ntr ww pP msg wix xx
            let cin = X_true // no borrow
            pand_subtract_equal_lengths cin (map (fun bit -> X_false) rhs, rhs)


        | W_node(prec, oo, lst, _) -> // Generic diadic vector operators with vector result
            let keep_trailers = oo=V_bitor||oo=V_xor
            let fop = if oo=V_bitand then ix_and elif oo=V_bitor then ix_or else ix_xor
            let rec diog = function
                | ([], []) -> []
                | (l::ls, r::rs) -> (fop l r) :: diog (ls, rs)
                | (x, [])
                | ([], x) -> if keep_trailers then x else []

            let rec bitwise_fold = function
                | [item] -> item
                | a::b::tt -> bitwise_fold(diog(a,b)::tt) 
            let lst' = map (pandex_binary ww pP msg None) lst
            
            match oo with
                | V_bitor | V_bitand | V_xor -> bitwise_fold lst'
                | V_plus                     -> pand_add1_binary pP (hd lst', hd(tl lst'))
                | V_lshift -> pand_lshift(hd lst', hd(tl lst'), 1)                
                | V_rshift ss -> pand_rshift ss prec (hd lst', hd(tl lst'), 1)
                | V_times  -> pand_times pP prec (hd lst', hd(tl lst')) 
                | _ -> sf ("pandex_binary: unsupported diadic op " + xToStr aA)

        | X_x(subarg, n, _) ->
            let bitlist = pandex_ntr ww pP msg wix subarg
            map (fun qbit -> xi_orred(xi_X n (xi_blift qbit))) bitlist

#if UNCLEAR
        | X_tuple(ats, lst, _) -> 
            let t' = t (* could leave as is: a void side effector no doubt. *)
            let f' = pandex_binary ww wix pP f
            if length f' < 1 then [muddy "pandeX_tuple t'" ]
            else xi_orred(ix_pair t' (muddy "pandeX_tuple f'")) :: tl f'
#endif
        | X_blift b ->
            let b' = bpandex_binary ww pP msg b
            [b]


        | other -> sf("pandex other " + xToStr other)


let pandor ww pP msg v = 
    let vd = pP.vd // -1 is off
    let v' = pandex_binary ww pP msg (Some 1) v
    if vd>=5 then vprintln 5 ("or-reduce items " + (sfold xbToStr v'))

    let rec orred = function
        | [item] -> item
        | h::tt  -> ix_or h (orred tt)
        | [] -> sf "orred []"
    orred v'



// eof (pandex_binary.fs)
    
