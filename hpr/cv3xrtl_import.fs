// cv3import.fs
// (C) 2015 DJ Greaves, University of Cambridge, Computer Laboratory.
//
// This is an opath recipe stage.
//
// cv3xrtl_import -- Import Verilog RTL - The parse tree from an external parser (csimloved and old Tentech tools) is read in as XML.
// The resultant RTL parse tree can be round-tripped to an RTL file using hpr verilog_render for debug purposes.
// A longer route via an HPR VM that can be converted to a Verilog parse tree using verilog_gen and rendered the same way.
//
// To convert to an HPR VM, reset factorisation is first needed and then csyn-style elaboration, is needed,
// converting into  'pure RTL'.  This is either as (xrtl_t assign pair/AP form) or as X_x hbev_t assigns (ie SP_rtl or SP_l).
// These steps are performed by verilog_gen.rtl_presim
// 

module cv3xrtl_import

open Microsoft.FSharp.Collections
open System.Collections.Generic


open yout
open moscow
open verilog_hdr
open meox
open linepoint_hdr
open hprls_hdr
open abstract_hdr
open verilog_gen
open verilog_render
open opath_hdr
open abstracte 

open hprxml


// Local
open cxverhdr



let denet = function
    | X_bnet ff -> ff
    | other -> sf(sprintf  "denet other %A" other)


let lid_import = function
    | "lid_wire"   -> (LOCAL, VNT_WIRE)
    | "lid_reg"    -> (LOCAL, VNT_REG)
    | "lid_input"  -> (INPUT, V_IN)
    | "lid_regout" -> (OUTPUT, V_OUT)
    | "lid_output" -> (OUTPUT, V_OUT)
    | "lid_inout"  -> (INOUT, V_INOUT)
    | other ->
        hpr_yikes(sprintf "cv3import: Treat other lid form as REG %A" other)
        (LOCAL, VNT_REG)


// Kludge: Where a net has been defined with the same name but different properties, we must autorename it, sadly. Memoising heaps no good at name space currently!  Please fix.
let autorename_net w io id =
    let (found, ff) = g_netbase_ss.TryGetValue id
    if not found then id
    else
        if ff.is_array || ff.is_fifo then sf "L86-array or FIFO"
        if ff.width <> w then sf "L86-width mismatch"
        let (found, ov) = g_netbase_nn.TryGetValue ff.n
        if (found) then
            match (snd ov).xnet_io with
                | a when a=io  -> id
                | INPUT -> "i_" + id
                | OUTPUT -> "o_" + id
                | _  -> "l_" + id                                
        else id

let delist msg = function
    | XML_ATOM "XMLNIL" -> []
    | XML_ELEMENT("ALIST", [], contents) -> contents
    | other ->
        hpr_yikes (sprintf "cv3xrtl_import: %s delist other %A" msg other)
        []


//  [XML_ELEMENT ("nsid",[],[XML_ATOM ""clk""; XML_ATOM "lid_input"]);
let cv3xrtl_import_net ww net =
    let san = string_fold_and_sanitize_no_leading_digit 65 [ '_' ]  // Remove the dots 
    match net with
        | XML_ELEMENT("nsid", [], [XML_ATOM id; XML_ATOM lid_form]) ->
        //CX_nsid(id, lid_form) -> // Scalar net
            let (io, _) = lid_import lid_form 
            let id = autorename_net 1 io id
            let id = san id
            let id = (if strlen id > 1 && not(id.[0] <> '_') then id.[1..] else id) // Remove leading '.' always added by the flattener in the front end?
            let net = ionet_w(id, 1, io, Unsigned, [])
            let ff = denet net
            //dev_println (sprintf "cv3xrtl_import: net import >%s< -> >%s<" id ff.id)
            (V_NET(ff, ff.id, -1), lid_form)

        | XML_ELEMENT("nvid", [], [XML_ATOM id; XML_ATOM h; XML_ATOM "0"; XML_ATOM lid_form; XML_ATOM is_signed]) ->
        // CX_nvid(id, hh, 0, lid_form, is_signed) -> // Register (vector) net
            let (io, _) = lid_import lid_form 
            let ss = if is_signed = "true" then Signed else Unsigned
            let id = autorename_net (atoi32 h + 1) io id
            let id = san id
            let id = (if strlen id > 1 && not(id.[0] <> '_') then id.[1..] else id) // Remove leading '.' always added by the flattener in the front end?
            let ff = denet(vectornet_ws(id, atoi32 h + 1, ss))
            (V_NET(ff, ff.id, -1), lid_form)

        | other -> sf(sprintf "Other net form %A" other)


let rec cv3_import_evc ww nets = function
    | XML_ELEMENT("K_posedge", _, [net]) -> V_EVC_POS(cv3_import_exp ww nets net)
    | XML_ELEMENT("K_negedge", _, [net]) -> V_EVC_NEG(cv3_import_exp ww nets net)
    //XML_ELEMENT("K_anyedge", _, [net]) -> V_EVC_ANY(cv3_import_exp ww nets net)        
    | XML_ELEMENT("hash", _, [number])   -> V_EVC_HASH(cv3_import_exp ww nets number)
    | XML_ELEMENT("K_anyedge", _, [net])
    | XML_ELEMENT("Anyedge", _, [net])   -> V_EVC_ANY(cv3_import_exp ww nets net)     // We want to flatten such lists, but maybe later.
    | XML_ELEMENT("K_or", _, [ll; rr]) -> // Lists of two only!
        let a = cv3_import_evc ww nets ll 
        let b = cv3_import_evc ww nets rr
        match (a, b) with
            //| (V_EVC_ANY a1, V_EVC_ANY a2)
            | _ -> V_EVC_OR(b, a) // Reverse as a hack to later 'forget' asynch reset on rtl_resynth of blif for now.
    | other -> sf(sprintf "cv3_import_evc: Other form %A" other)            

and cv3_import_exp ww nets (arg:xml_t) =
    let vd = 3 //for now
    let gprec msg = function
        | XML_ELEMENT("attr", _, [XML_ATOM width; XML_ATOM is_signed; XML_ATOM is_downto; XML_ATOM "0"]) ->
            let signed = if is_signed="true" then Signed else Unsigned
            { g_default_prec with widtho=Some(atoi32 width); signed=signed }
        | other -> sf(sprintf "cv3_import_exp: gprec: other prec %A" other)


    let diadic_p oo lhs rhs prec =
        let (lhs, rhs) =  (cv3_import_exp ww nets lhs, cv3_import_exp ww nets rhs)
        match oo with // The following forms are for i/o modes only and not supported or at least  deprecated in internal forms.
            | V_DGTD -> V_DIADIC(prec, V_DLTD, rhs, lhs) // Swap args.
            | V_DGED -> V_DIADIC(prec, V_DLED, rhs, lhs) // Again swap args.
            | V_DNED -> V_LOGNOT(V_DIADIC(prec, V_DEQD, lhs, rhs))
            | oo     -> V_DIADIC(prec, oo, lhs, rhs)

    let diadic oo lhs rhs ats =
        let prec = gprec "diadic" ats
        diadic_p oo lhs rhs prec

    let deats msg arg = arg


    match arg with
        | XML_ELEMENT("sizednumber", [], [ XML_ATOM nn; XML_ELEMENT("attr", _, [XML_ATOM width; XML_ATOM is_signed; XML_ATOM is_downto; XML_ATOM "0"])]) ->
            let truef ss = (ss = "true") // See also gprec and rationalise!
            let (width, is_signed) = (atoi32 width, truef is_signed)
            V_NUM(width, is_signed, "d", xi_bnum(atoi nn))

        | XML_ELEMENT("nvid", _, _)
        | XML_ELEMENT("nsid", _, _)     -> fst(cv3xrtl_import_net ww arg)


        | XML_ELEMENT("text", _, [XML_ATOM ss])         -> V_STRING(xi_string ss, ss)
        | XML_ELEMENT("lognot", _, [arg])     -> V_LOGNOT(cv3_import_exp ww nets arg)
        | XML_ELEMENT("simplecat", _, [items; n_]) ->
            let qf (XML_ELEMENT("ATUPLE2", _, [arg; XML_ATOM count])) =  (atoi32 count, cv3_import_exp ww nets arg)
            V_CAT(map qf (delist "CX_simplecat" items))

        | XML_ELEMENT("extract", _, [arg; XML_ATOM baser; XML_ATOM width]) ->
            let arg = cv3_import_exp ww nets arg
            let rr = atoi32 baser
            let ll = rr + atoi32 width - 1
            V_BITSEL(arg, ll, rr)
        | XML_ELEMENT("query", _, [gg; lhs; rhs; _]) ->
            let (lhs, rhs) =  (cv3_import_exp ww nets lhs, cv3_import_exp ww nets rhs)
            let gg         =  cv3_import_exp ww nets gg
            V_QUERY(gg, lhs, rhs)
            
        | XML_ELEMENT("lshift", _, [lhs; rhs; ats])    -> diadic V_LSHIFT lhs rhs (deats "Q" ats)
        | XML_ELEMENT("rshift", _, [lhs; rhs; (XML_ELEMENT("attr", [], [XML_ATOM width; XML_ATOM is_signed; is_downto_; lsb_index]) as ats)])  ->
            let opp = (if is_signed="true" then V_ArithRSHIFT else V_LogicalRSHIFT)
            diadic opp lhs rhs (deats "Q" ats)

        | XML_ELEMENT("plus", _, [lhs; rhs; ats])     -> diadic V_PLUS lhs rhs (deats "Q" ats)
        | XML_ELEMENT("minus", _, [lhs; rhs; ats])    -> diadic V_MINUS lhs rhs (deats "Q" ats)
        | XML_ELEMENT("rem", _, [lhs; rhs; ats])      -> diadic V_DMOD lhs rhs (deats "Q" ats)
        | XML_ELEMENT("multiply", _, [lhs; rhs; ats]) -> diadic V_TIMES lhs rhs (deats "Q" ats) 
        | XML_ELEMENT("divide", _, [lhs; rhs; ats])   -> diadic V_DIVIDE lhs rhs (deats "Q" ats)
        // Bitwise operators
        | XML_ELEMENT("binot", _, [arg; ats])         -> V_1sCOMPLEMENT(cv3_import_exp ww nets arg)
        | XML_ELEMENT("binand", _, [lhs; rhs; ats])   -> diadic V_BITAND lhs rhs (deats "Q" ats)
        | XML_ELEMENT("binor", _, [lhs; rhs; ats])    -> diadic V_BITOR lhs rhs (deats "Q" ats)
        | XML_ELEMENT("binxor", _, [lhs; rhs; ats])   -> diadic V_XOR lhs rhs (deats "Q" ats)                
        // Logic operators
        | XML_ELEMENT("logand", _, [lhs; rhs])        -> diadic_p V_LOGAND lhs rhs g_bool_prec
        | XML_ELEMENT("logor", _, [lhs; rhs])         -> diadic_p V_LOGOR lhs rhs g_bool_prec        
        // Arithmetic comparison operators:
        | XML_ELEMENT("deqd", _, [lhs; rhs])            -> diadic_p V_DEQD lhs rhs g_bool_prec
        | XML_ELEMENT("dgtd", _, [lhs; rhs; signed_])   -> diadic_p V_DGTD lhs rhs g_bool_prec // Signed has to be re-detected - lost in import.
        | XML_ELEMENT("dged", _, [lhs; rhs])            -> diadic_p V_DGED lhs rhs g_bool_prec

        | XML_ELEMENT("vcast", _, [XML_ATOM is_signed; XML_ATOM width; XML_ATOM is_downto; XML_ATOM right_index; arg]) ->
            // XML_ATOM "false"; XML_ATOM "32"; XML_ATOM "true"; XML_ATOM "0" -- is_downto and right_index are usually true and 0.  
            V_VCAST((is_signed = "true"), atoi32 width, (is_downto = "true"), atoi32 right_index, cv3_import_exp ww nets arg)

        | XML_ELEMENT("text", _, strings) -> // Each space in the input makes a separate XML_ATOM.  Want to re-catenate
            let recat_ss arg cc =
                match arg with
                    | XML_ATOM ss -> if cc = "" then ss else ss + " " + cc
                    | other -> sf(sprintf "cv3xml_import_exp: Other recat_ss form %A" other)
            let ss = List.foldBack recat_ss strings ""
            let vgen_STRING s = V_STRING(xi_string s, s) // A flying copy.
            vgen_STRING ss

        | XML_ELEMENT("systemfunctioncall", _, [XML_ATOM fname; args; rt_]) ->
            let args = map (cv3_import_exp ww nets) (delist "simf" args)
            let callers_flags = g_null_callers_flags
            let hpr_name = verilog_un_plimap (killquotes fname)
            if vd>=4 then vprintln 4 (sprintf "Import CX_systemfunctioncall %s (%s) with %i args."  fname hpr_name (length args))
            let def = hpr_native hpr_name
            V_CALL(callers_flags, (def, None), args) // This has the Verilog name. What? inside it?
            // xi_apply (hpr_native fname, args)

            
        | other              -> sf(sprintf "cv3xml_import_exp: Other form %A" other)                   


// Behavioural RTL import
let rec cv3_import_seq ww nets cx cc =
    match cx with

        | XML_ELEMENT("K_while", [], [gg; body; XML_ATOM ss_string_]) ->
            let gg = cv3_import_exp ww nets gg
            let body = gen_V_BLOCK(cv3_import_seq ww nets body [])
            V_WHILE(gg, body, ss_string_)::cc

        | XML_ELEMENT("event_control", _, [evc; bev]) ->
            let evc = cv3_import_evc ww nets evc
            let bev = cv3_import_seq ww nets bev cc
            (V_EVC evc) :: bev

        | XML_ELEMENT("linepoint", _,  [XML_ELEMENT("LP", _, (XML_ATOM filename)::(XML_ATOM lineno)::_)]) -> // NB: We should also see linepoints at the top level
            let (filename, lineno) = (killquotes filename, atoi32 lineno)
            //if vd>=4 then vprintln 4 (sprintf "cv3xrtl_import: Linepoint %s  %i" filename lineno)
            V_LINEPOINT(LP(filename, lineno))::cc


        | XML_ELEMENT("block", _, [id; decls; bevlst]) ->
            //if not_nullp decls then muddy "decs in block"
            let lst = List.foldBack (cv3_import_seq ww nets) (delist "block" bevlst) []
            (V_BLOCK lst)::cc

        | XML_ELEMENT("simf", _, [XML_ATOM pli_fname; args]) ->
            let args = map (cv3_import_exp ww nets) (delist "simf" args)
            let native_fun_name = verilog_un_plimap (killquotes pli_fname)
            //let def = hpr_native (verilog_un_plimap fname)
            match builtin_fungis native_fun_name with
                | None ->
                    hpr_yikes(sprintf "cv3xrtl_import: Failed to import native pli call %s with %i args."  native_fun_name (length args))
                    cc
                | Some native_fun_def ->
                    //if vd>=4 then vprintln 4 (sprintf "Import native pli call %s with %i args."  native_fun_name (length args))
                    V_EASC(V_CALL(g_null_callers_flags, ((native_fun_name, native_fun_def), None), args))::cc


        | XML_ELEMENT("K_if", _, [gg; t_body; XML_ELEMENT("SOME", _, [f_body])]) ->
            let gg = cv3_import_exp ww nets gg
            let t_body = gen_V_BLOCK(cv3_import_seq ww nets t_body [])
            let f_body = gen_V_BLOCK(cv3_import_seq ww nets f_body [])            
            V_IFE(gg, t_body, f_body)::cc

        | XML_ELEMENT("K_if", _, [gg; t_body; XML_ATOM "NONE"]) ->
            let gg = cv3_import_exp ww nets gg
            let t_body = gen_V_BLOCK(cv3_import_seq ww nets t_body [])
            V_IF(gg, t_body)::cc

        | XML_ELEMENT("K_behev_assign", _, [lhs; rhs; XML_ATOM assop; XML_ATOM "NONE"]) ->
            let (lhs, rhs) = (cv3_import_exp ww nets lhs, cv3_import_exp ww nets rhs)
            let non_blocking = (assop = "ba_equals")
            (if non_blocking then V_NBA(lhs, rhs) else V_BA(lhs, rhs))::cc

        | other -> sf(sprintf "cv3xrtl_import: Other cx seq form %A" other)

let cv3_import_ap ww nets ap =
    match ap with
        | XML_ELEMENT("AP", _, [lhs; rhs; XML_ATOM delay; pin_string]) ->
            let (lhs, rhs) = (cv3_import_exp ww nets lhs, cv3_import_exp ww nets rhs) // no lmode convert needed.   

            let delay_o = Some (atoi32 delay)
            let format = false // muddy (sprintf "biz_string=%s" biz_string)
            V_CONT((delay_o, format), lhs, rhs)

        | other -> sf(sprintf "cv3xrtl_import: Other ap lhs form %A" other)

        
let cv3xrtl_import_main ww op_args control dump_straightaway (filename) =

    if not (existsFile filename) then cleanexit(sprintf "cv3xrtl_import: cannot find src file %s" filename)
    let xml_ast = hpr_xmlin ww filename

    if (false) then
        vprintln 0 (sprintf "Printing whole ast: %A" xml_ast) 
        vprintln 0 (sprintf "Finshed printing whole ast.")   


    let (kind, ast00) = 
        match xml_ast with
            | XML_ELEMENT("module", atts, ast00) ->
                let dname = valOf_or_fail "No name attribute in XML RTL" (op_assoc "name" atts)
                (dname, ast00)
            | other -> sf(sprintf "Other top-level XML AST %A" other)
            
            
    let ww = WF 1 "cv3import" ww (sprintf "Start import '%s' from cv3 ast." kind)
    let (contacts, bev00, ap_lst00, nets00, xt00) =
        let sieve item (contacts00, bev00, ap_lst00, nets00, xt00) =
            match item with
                | XML_ELEMENT("ATUPLE5", _, co::be::ap::ne::xt::_) ->
                    (delist "co" co @ contacts00, delist "be" be @ bev00, delist "ap" ap @ ap_lst00, delist "ne" ne @ nets00, delist "xt" xt @ xt00)
                | other ->
                    hpr_yikes(sprintf "%s: Ignore other content  XML AST %A" filename other)
                    (contacts00, bev00, ap_lst00, nets00, xt00)
        List.foldBack sieve ast00 ([], [], [], [], []) 


    let nets = map (cv3xrtl_import_net ww) (contacts @ nets00) // Ignore distinction and filter them later as needed.
    let seqs = List.foldBack (cv3_import_seq ww nets) (bev00) []
    let aps = map (cv3_import_ap ww nets) (ap_lst00)

    let ww = WF 1 "cv3import" ww (sprintf "Finished translate from cv3 ast.  %i nets. %i seqs. %i combs." (length nets) (length seqs) (length aps))


    let net_decls = 
        let to_netdecl (V_NET(ff, _, _), lid) =
            let (io_, vnt) = lid_import lid
            let f2 = lookup_net2 ff.n
            let vtype = f2.vtype
            let m = f2.xnet_io
            let alias = ""
            let init_o = None
            let delay_o = None // DELETED above: please restore.
            let array_max = asize f2.length - 1L // -1L for not an array
            let bt = g_standard_binary_form //Always binary import for Verilog for now.
            //dev_println (sprintf "cv3xrtl_import: net_decl: >%s<" ff.id)
            V_NETDECL(bt, X_bnet ff, delay_o, ff, encoding_width (X_bnet ff), alias, array_max, vnt, init_o)
        map to_netdecl nets

    let code_seq = map (fun x ->V_INITIAL x) seqs // Verilog's 'always' has been converted to 'initial forever' in most cases.
    let code_comb = aps //map (fun (l, r) ->V_CONT((None, false), l, r)) aps
    let vlnv = { vendor= !g_m_xact_op_vendor; version= !g_m_xact_op_version;  library="RTL-IMPORT"; kind=[kind] }    
    let lst =
        let pf x = (x, "silly") // why?
        map pf (net_decls @ code_seq @ code_comb)
    let hbev = rtl_presim ww (vlnv, lst) ([], [])  // This performs reset factorisation and so on.
    let ww = WF 1 "cv3import" ww (sprintf "Finished import '%s' to hbev from cv3 ast." kind)

    if nonep dump_straightaway then
        let ww = WF 1 "cv3import" ww (sprintf "Roundtrip render for '%s' straightaway form is NOT ENABLED." kind)
        ()
    else 
        let (filename, file_suffix) = (filename, ".v")
        let ww = WF 1 "cv3import" ww (sprintf "Roundtrip render start for '%s' to hbev?? from cv3 AST.  filename=%s.%s" kind filename file_suffix)
        let output_module_name = valOf dump_straightaway // ie not using the -vnl=filename flag form for the simple roundtrip.
        let kandr = control_get_s op_args.stagename control "cv3import-kandr" (Some "enable") = "enable"
        let bag = { g_null_ddctrl with kandr=kandr }
        let pagewidth = 132 
        let fvv:(int * ddctrl_t) = (pagewidth, bag) // Silly place to put page width. Pass in separately please.

        let timescale = ""
        let aux_messages = []
        let add_aux_reports = true
        let items = lst
        let aliases = []
        let (gv_nets, lv_nets) =
            let pred(net, lid) =
                let (io, _) = lid_import lid // This is an unnecessary second import ... see net_decls
                isio io
            groom2 pred nets

        let gv_nets =
            let cpi = { g_null_db_metainfo with kind= "cv3import"; form= DB_form_external }
            let db_netwrap_null (net, lid) = VDB_formal(net)
            gec_VDB_group(cpi, map db_netwrap_null gv_nets)

        let lv_nets =
            let cpi = { g_null_db_metainfo with kind= "cv3import"; form= DB_form_local }
            let db_netwrap_null (net, lid) = VDB_actual(None, net)
            gec_VDB_group(cpi, map db_netwrap_null lv_nets)

        let newpramdefs = [] 
        let hh_level = 0
        //rtl_output2 ww' fv1.ddctrl filename fv1.timescale auxlist fv1.add_aux_reports
        let netinfo_dir = new Dictionary<string, netinfo_t>()

        //TODO nothing in the dict
        //app v2_make_directory (formals @ locals @ newpramdefs @ pramdefs)
        
        let vmods = [(netinfo_dir, "asrf_", output_module_name, fvv, hh_level, (aliases, newpramdefs, gv_nets, lv_nets), items)]
        rtl_output2 ww bag (filename, file_suffix) timescale aux_messages add_aux_reports vmods
        let ww = WF 1 "cv3import" ww (sprintf "Roundtrip render finish for '%s' to hbev from cv3 ast." kind)
        ()


    let ww = WF 1 "cv3import" ww (sprintf "Finished cv3xrtl_import for '%s'." kind)        

    let (decls, execs) = hbev
    let kind_vlnv = { vendor= !g_m_xact_op_vendor; version= !g_m_xact_op_version; library= !g_m_xact_op_library; kind=[kind] }
    let minfo = { g_null_minfo with name=kind_vlnv; }
    let ii = { g_null_vm2_iinfo with iname=kind; generated_by=op_args.stagename;  definitionf=true }    
    let ans = (ii, Some(HPR_VM2(minfo, decls, [], execs, [])))
    ans


    
let cv3xrtl_import_argpattern =
   [
       Arg_required("cv3xrtl-srcfile", -1, "Verilog RTL input file(s) for processing.", "") // The top-level input .dll/.exe file for compilation 
       Arg_enum_defaulting("cv3xrtl-import-kandr", ["enable"; "disable"], "enable", "Enable Kernighan and Ritchie style (pre-2001) Verilog port syntax");
       Arg_enum_defaulting("cv3xrtl-import-dump-straightaway", ["enable"; "disable"], "disable", "Write out a round-trip copy of the input.");       
   ] 
 
// This is the top-level instance of this plugin
let opath_cv3xrtl_import ww op_args vms_in =
    let c1:control_t = op_args.c3
    let stagename = op_args.stagename
    let disabled = (cmdline_flagset_validate stagename ["enable"; "disable" ] 0 c1 = 1)
    let ww = WF 1 "cv3" ww "Starting"


    //vprintln 2 (bevelab_banner)
    if disabled
    then
        vprintln 1 "cv3xrtl_import: Stage is disabled"
        vms_in
    else                
        //let zz = cmdline_flagset_validate "skip-propagate" ["false"; "true" ] 0 c1

    let dump_straightaway =
        if control_get_s stagename op_args.c3 "cv3xrtl-import-dump-straightaway" None = "enable" then Some "cv3-roundtrip.v" else None // Silly canned filename

    let src_names = rev(control_get_strings op_args.c3 "cv3xrtl-srcfile") // Reverse since returned in wrong order

    let ww = WF 2 "cv3" ww ("Start RTL in XML format import of " + sfold id src_names)
    //let dnames = names // [ ("/tmp/benchmarks-compiled/SD1.xml", "SD1") ]

    vprintln 3 (sprintf "Processing %i RTL src files in XML form." (length src_names))
    let answers = map (cv3xrtl_import_main ww op_args c1 dump_straightaway) src_names

    let ww = WF 2 "cv3" ww "Finished cv3 import."        
    answers @ vms_in
    



let install_cv3xrtl_import() =
    let op_args__ =
            { stagename=         "cv3xrtl_import"
              banner_msg=        "cv3xrtl_import"
              argpattern=        cv3xrtl_import_argpattern
              command=           None //"cv3xrtl_import"
              c3=                []
            }
    install_operator ("cv3xrtl_import",  "Import RTL parse tree from old Tentech tools", opath_cv3xrtl_import, [], [], cv3xrtl_import_argpattern); 

// eof

