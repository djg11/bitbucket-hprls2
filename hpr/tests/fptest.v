//
// Floating point test in Verilog RTL.
//
module SIMSYS;

   real vv, nn, dd;

   reg [63:0] nx, dx;
   wire [63:0] sum, diff, prod, quot;
   reg 	      clk, reset;
   
   initial begin
      $display("fptest.v");
      clk = 0;
      reset = 1;
      # 122
	reset = 0;
   end

   always #50 clk = !clk;

   reg [10:0] count, op;
   real       ideal, back;
   reg [63:0] dp_ans;
   always @(posedge clk) begin
      if (reset) count = 0; 

      else count = count + 1;
      if (count == 1000) $finish;
      vv = $bitstoreal(64'h3ff1_8000_0000_0000);

      if ((count %10) == 0) begin
	 nx = (count < 10) ? $realtobits(1025) : 64'h3001_8000_1234_0000 | (count << 45);
	 dx = (count < 10) ? $realtobits(32.0) : 64'h3df0_0000_0000_0000 | (count << 54);
	 nn = $bitstoreal(nx);
	 dd = $bitstoreal(dx);	 
      end 


      if ((count %10) == 9) begin	 
	
	 $display("fptest.v %g %g            %h  %h", nn, dd, nx, dx);

	 op = 0;
	 while (op < 4) begin
	    case(op)
	      0:
		begin
		   dp_ans = diff;
		   ideal = nn  - dd;
		   $display("    fptest diff %h %g %g",   dp_ans, $bitstoreal(dp_ans), ideal);
		end

	      1:
		begin
		   dp_ans = sum;
		   ideal = nn +dd;
		   $display("    fptest sum %h %g %g",   dp_ans, $bitstoreal(dp_ans), ideal);
		end
	      
	      2:
		begin
		   dp_ans = prod;
		   ideal = nn * dd;
		   $display("    fptest prod %h %g %g",   dp_ans, $bitstoreal(dp_ans), ideal);
		end
	      3:
		begin
		   dp_ans = quot;
		   ideal = nn / dd;
		   $display("    fptest quot %h %g %g",   dp_ans, $bitstoreal(dp_ans), ideal);
		end
	      
	    endcase // case (op)

	    back = $bitstoreal(dp_ans);
	    if (ideal != back) $display(" ************* A difference in answer! ************************** TEST FAILED ");
	    op = op + 1;
	 end // while (op < 4)
      end
      

   end

   wire mf;    CV_FP_FL5_DP_MULTIPLIER multiplier(clk, reset, prod, nx, dx, mf);
   wire df;    CV_FP_FL5_DP_DIVIDER divider(clk, reset, quot, nx, dx, df);
   wire af;    CV_FP_FL5_DP_ADDER adder(clk, reset, sum, nx, dx, af);
   wire sf;    CV_FP_FL5_DP_SUBTRACTOR subtractor(clk, reset, diff, nx, dx, sf);         

endmodule
