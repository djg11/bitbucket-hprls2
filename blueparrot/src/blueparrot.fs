//
// BlueParrot - an HLS Compiler
// (C) 2023 University of Cambridge, Computer Laboratory.
//
//
// This wrapper makes the blueparrot process a plugin for HPR Orangepath tool chain.
//
   
// Need to do auto plugin scanning but load some manually here.

open conerefine
open bevelab
open verilog_gen
open cpp_render
open opath
open hprxml

open parrot_temp


[<EntryPoint>]
let main (argv : string array) =
    yout.g_version_string := blueparrot_banner()
    yout.set_opath_report_banner_and_name([blueparrot_banner()], "blueparrot.rpt")
    let _ = blueparrot_used ()
    let _ = install_coneref()
    let _ = install_bevelab()
    let _ = install_verilog()
//    let _ = install_diosim()
    let _ = install_c_output_modes()
    let rc = opath_main ("blueparrot", argv)
    rc
