

module canned_parrot_blocks // not being used!

open System.Numerics

open yout
open meox
open moscow
open hprls_hdr
open abstract_hdr

open hprtl


//=======================================================
// 2-place FIFO with freehand contacts.
//
//  Baseline functionality:  (not empty output is occ > 0). All outputs registered.
//  The output is wired permanently to PL0. PL0 is loaded from din or PL1. PL1 is loaded from din.
//
// This is partly a bad example, since it
// 1. uses a fancy FSM compiler that's probably not worth learning about
// 2. has freehand formals instead of structured ports.
let mk_two_place_fifo ww genv =
    let ww = WF 2 "mk_two_place_fifo" ww "Start"
    let nbm = new nbm_t(ww, sprintf "TWO_PLACE_FIFO", false, genv)
    let word_width = atoi32(nbm.rides_lookup "32" "DATA_WIDTH")
    
    // Formals
    let din  =  ionet_w("din",  word_width, INPUT, Unsigned, [])
    let dout =  ionet_w("dout", word_width, OUTPUT, Unsigned, [])
    let enqueue =  ionet_w("enqueue", 1, INPUT, Unsigned, [])
    let dequeue =  ionet_w("dequeue", 1, INPUT, Unsigned, [])
    let not_empty =  ionet_w("not_empty", 1, OUTPUT, Unsigned, [])

    nbm.add_ports ww "" [  HBT_freehand_formals [ not_empty; dout; dequeue; din; enqueue ] ]
    let meld_ = nbm.fu_end ww

    // Local registers/nets.
    let occ = hx_register_with_reset ww nbm ("0") "l_occ" 2
    let pl0 = hx_register ww nbm "l_PLACE0" word_width
    let pl1 = hx_register ww nbm "l_PLACE1" word_width
    let overrun = hx_register ww nbm "overrun" 1

    // Four commands and two error flags.  Use simplnet for combinational nets, but remember to save them as freehand_locals
    let (aL0, aL1, aXX, aXL, aEoverrun, aEunderrun) = (simplenet "aL0", simplenet "aL1", simplenet "aXX", simplenet "axL", simplenet "e_over_run", simplenet "e_under_run")
    nbm.add_freehand_locals ww [ aL0; aL1; aXX; aXL; aEoverrun; aEunderrun ]


    let _:hexp_t = aL0
    // Behaviour
//
//   occ  enq deq   action       occ'       Four actions:
//    0    0   1     E:underrun  (0)
//    0    1   0     LO           1         L0 - load 0
//    0    1   1     E:underrun  (0)
//    1    0   1     null         0
//    1    1   0     L0           2         L1 - load 0
//    1    1   1     XL           1
//    2    0   1     XX           1         XX - transfer 1 to 0
//    2    1   0     E:overrun   (2)        
//    2    1   1     XL           2         XL - transfer 1 to 0 and load 1
    let occ_xitions = 
     [ //occ; enq; deq
       ([ 0;    0;   1;],  [   aEunderrun ],     (0) )//
       ([ 0;    1;   0;],  [   aL0         ],     1  )//        L0 - load 0
       ([ 0;    1;   1;],  [   aEunderrun  ],    (0) )//
       ([ 1;    0;   1;],  [   (*null*) ],        0  )//
       ([ 1;    1;   0;],  [   aL1      ],        2  )//        L1 - load 1
       ([ 1;    1;   1;],  [   aL0      ],        1  )//        L0 - load 0 (also being unloaded on old content).
       ([ 2;    0;   1;],  [   aXX      ],        1  )//        XX - transfer 1 to 0
       ([ 2;    1;   0;],  [   aEoverrun    ],   (2) )//        
       ([ 2;    1;   1;],  [   aXL      ],        2  )//        XL - transfer 1 to 0 and load 1
     ]

    hx_compile_sm0 ww nbm [occ; enqueue; dequeue] occ_xitions

    nbm.add_seq_g aL0 (pl0, din)
    nbm.add_seq_g aXX (pl0, pl1)
    nbm.add_seq_g aXL (pl0, pl1)
    nbm.add_seq_g aXL (pl1, din)
    nbm.add_seq_g aL1 (pl1, din)
    
    nbm.add_comb (not_empty, hx_dned xi_zero occ)
    nbm.add_comb (dout, pl0)

    let vmd = nbm.finish_definition ww []
    let ww = WF 2 "mk_two_place_fifo" ww "Finish"
    vmd


// eof
