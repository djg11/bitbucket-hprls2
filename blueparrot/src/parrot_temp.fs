//
// CBG BlueParrot - an HLS Compiler
// (C) 2023 University of Cambridge, Computer Laboratory.
//
module parrot_temp


open System.Numerics
open System.Collections.Generic


open opath_hdr
open hprls_hdr
open abstract_hdr
open dotreport
open abstracte
open moscow
open meox
open yout
open hprxml
open hprtl
open protocols


open bp_gram
open bp_parser
open bp_canned_basic_fus

// Verbosity control:
let g_xvd = ref 4 // nominally 3 - higher for more output. eg it is on 4 at the moment.

let (|Let|) value input = (value, input)

type modtemp_t =
    | IR_nbm of string * nbm_t

type blueparrot_settings_t =
    {
        stagename:                          string
        prenormal_dot_report_enabled:       bool
        normalised_dot_report_enabled:      bool
        final_dot_report_enabled:           bool        
        m_report_file_contents:             string list ref
        m_local_module_defs:                (string * modtemp_t) list ref  // List of modules defined in input src files.
        use_diadic_only:         bool // For replicators, rendezvous and call site managers
        canned_fu_settings:      canned_fu_settings_t
    }


let g_blueparrot_banner = "CBG Blueparrot HLS Compiler: Version alpha 0 (July 2023)"

let g_parrot_temp_iplib = { vendor= "HPRLS"; library= "BP-EXPERIMENTS"; kind=["-vanilla-"]; version= "1.0" }


let blueparrot_banner() =
    g_blueparrot_banner

let g_self_instance_name = "$MYSELF"
let g_bus_zygote = "$BUS-ZYGOTE"

type bp_currency_t =
    | C_method_elaborated of string * arcid_t * ((ipblk_protocol_t * netport_pattern_schema_t list) * string * d22_t) option * ((ipblk_protocol_t * netport_pattern_schema_t list) * string * d22_t) * bp_arc_t list // The schema is only lightly djusted by busrez - port name is added
    // C_rule ... proactive behaviour
    | C_ipinst of string * (vm2_iinfo_t * hpr_machine_t option)
    | C_ghost of string * string // iname and kind
    | C_filler of string    


let blue_concise_report vd =
    let rl g1 = 
       //vprintln vd (sprintf "Report concise:  %s denotes %s" (xb2str g1) (xbToStr(deblit g1)))    
       ()
    //for g1 in g_blue_concise_set do rl g1 done
    ()



let drop_hs msg = function
    | _ :: _ :: tt -> tt
    | other -> sf(sprintf "drop_hs %s: Cannot drop missing SSI handshake other=%A" msg other)

let local_logtidy ww settings ytag =
    blue_concise_report 1
    aux_report_log 1 ytag (rev !settings.m_report_file_contents)
    //aux_report_log 1 "States Statistics." (rev !settings.m_states_report_lines)    


let local_cleanexit ww settings msg =
    let msg = "Exit message=" + msg
    mutadd settings.m_report_file_contents "Errored or early exit from compilation."
    mutadd settings.m_report_file_contents msg
    local_logtidy ww settings "Exit report on error."
    cleanexit msg


// Source file name and line number tracking, mostly for error reporting.
let m_linepoint = ref ("", "")
let lp_msg msg = sprintf ": %s, %s, %s" (fst !m_linepoint) (snd !m_linepoint) msg
let lp_lineno() = (snd !m_linepoint)
let note_lp (file, line) =
    let clean (ss:string) = if ss.Length > 0 then ss.Replace("\"","") else "-missing-" // Remove the quotes
    m_linepoint := (clean file, clean line)

let lookup_defn ww settings instance_name kind =
    
    let local_defn = op_assoc kind !settings.m_local_module_defs
    match local_defn with
        | Some ans -> ans
        | None ->  local_cleanexit ww settings (lp_msg instance_name + sprintf ": module %s definition cannot be found." kind)





let bp_bit_width settings msg = function
    | BP_integer -> (true, 32) // For no good reason
    | BP_void -> (false, 0)
    | BP_sized_int_type(orig_, signedf, width) ->
        if width<0 then (false, 0)// for an alternate void representation?
        else (signedf, width)
    
    | other -> sf(lp_msg msg + sprintf ": bp_bit_width other %A" other)


type linear_use_t = (string * string * bp_env_vale_t)        

type thread_properties_t =
    {
        tid:           string
        eis:           bool
        m_arcs:        bp_arc_t list ref
        m_new_fus:     (string * (bp_currency_t)) list ref
        m_arg_uses:    linear_use_t list ref
    }


let get_schemaN0((schema_name, pi_name, sex, schema), structured_ports) = (schema_name, schema)


let bp_get_d22_bus msg = function
    | BPA_alpha_bus _ -> None // This is a place holder for a physical bus, so d22 cannot be returned here.
    | BPA_fu_terminal(binding_note_, formal_terminal, _, formal_schema00) -> None 
    | BPA_fu_contact(binding_note_, terminal, d22)  -> Some d22
    | other -> sf(sprintf "key=%s bp_get_d22_bus other form %A" msg other)

let bp_get_binding_note msg = function
    | BPA_fu_terminal(binding_note, formal_terminal, _, formal_schema00) -> binding_note
    | BPA_fu_contact(binding_note, terminal, d22)  -> binding_note
    | BPA_alpha_bus(binding_note, terminal, sch00)  -> binding_note      
    | other -> sf(sprintf "key=%s bp_get_binding_note other form %A" msg other)

    
let fclose_arc ww msg key src_alpha dest_alpha =
    //vprintln 3 (sprintf "close_arc %s %s" msg key)
    if src_alpha = BPA_dead then hpr_yikes (sprintf "%s: %s: src_alpha is dead. Cannot discard" msg key)
    let na = BPARC_src_dest(key, src_alpha, dest_alpha)
    na

let close_arc ww tprops msg key src_alpha dest_alpha =
    //vprintln 3 (sprintf "close_arc %s %s" msg key)
    if src_alpha = BPA_dead then
        vprintln 3 (sprintf "%s: %s: src_alpha is dead. Discarding it." msg key)
    else
        let na = BPARC_src_dest(key, src_alpha, dest_alpha)
        mutadd tprops.m_arcs na


let iname_hint = function
    | BN_note(key, msg) -> key
    | other -> sf(sprintf "iname_hint other form %A" other)


//
// Invoke this method under the given state_alpha if there is no alpha in the args.
// Args is at most length one since rendezvous will be instantiated by parent if needed.
let close_call ww tprops msg (nbm:nbm_t) callsite_id alpha00 fu_inst method_name actual_argo =
    let _:bp_env_vale_t option = actual_argo                    //ALWAYS A SINGLETON OR UNIT ARG SINCE RENDEZVOUS IS EXTERNAL
    let (structured_ports, instance_name) = 
        match fu_inst with
            | BE_component(instance_name, atts, the_component, structured_ports) -> (structured_ports, instance_name) // These structured ports are not local nets, they are the formals for the component.
            | other -> sf(sprintf "Need to find method '%s' schemas from other form of component %A" method_name other)    


    let call_ =
       let side = "put"
       let (put_point, f_scname)  =
           match op_assoc (method_name, side) structured_ports with
               | None ->
                   vprintln 0 (sprintf "Available ports/methods are " + sfold (fun ((name, side), _) -> name + "/" + side) structured_ports)
                   cleanexit(sprintf "Need method '%s/%s' calling schema from instance '%s'" method_name side instance_name)
               | Some d22 ->
                   let scname = get_schema_name method_name d22
                   // vprintln 3 (sprintf "Apply: formal arg schema name: %A" scname)
                   // dev_println(sprintf "Apply: formal arg drink schema d22 %A" d22)                              
                   let terminal = (instance_name, method_name, side)

                   //(BPA_fu_contact(binding_note, terminal, d22), scname)  // deprecate? lookup again later...
                   // Perhaps prefer this, but it's basically the same plus lanes and direction
                   let binding_note = BN_note("callsite", callsite_id)
                   (BPA_fu_terminal(binding_note, terminal, -1, Some(get_schemaN0 d22)), scname) // (note, direction, terminal_id, lane,  full_schema) 

       let actual_activation =
           //dev_println(sprintf "Apply arg drink args %A" actual_args)
           match actual_argo with
               | None -> alpha00 // For unit arg calls - is this route NASTY?  TODO
               | Some(BE_result(Some alpha)) -> alpha
               | other -> sf(sprintf "Cannot find activation in argument: '%s' schemas from %A" method_name other)    
           // Where args is not null, there will be an alpha in the args as well as the passed-in alpha.  These should be the same but give preference to the ones in the args if different?
           
       close_arc ww tprops method_name ("close_apply_call:name="+method_name) actual_activation put_point // close_arc instance bind: actuals then formals
               
    let alpha_return_bus =
        let side = "aout"
        let (return_schema, alpha_return_contact)  =
            match op_assoc (method_name, side) structured_ports with
                | None ->
                    vprintln 0 (sprintf "Available ports/methods are " + sfold (fun ((name, side), _) -> name + "/" + side) structured_ports)
                    cleanexit(sprintf "Need method '%s' '%s' return schema from %A" method_name side instance_name)
                | Some ((ipb_protocol, ss_, bool_, schema), nets) ->
                    let d22 = ((ipb_protocol, ss_, bool_, schema), nets)
                    let return_schema = (ipb_protocol, schema)
                    let binding_note = BN_note("returnsite", callsite_id)
                    let alpha_return_contact = BPA_fu_contact(binding_note, (instance_name, method_name, side), d22) 
                    (return_schema, alpha_return_contact)

        let later_buzres_flag11 = false // for this one false I think is best, but no longer make the d22 lowered form.
        if later_buzres_flag11 then
            alpha_return_contact
        else
            let sex = None // Create local bus for the return token (and result subfields when non-void).
            let id = funiqu_chars 15 (instance_name + ":" + method_name)
            let token = (sprintf "%s_result_bus" id)
            //let return_bus = nbm.busrez2 ww [] sex return_schema token
            let arcid = "arc:" + id // DONT really need concepts of ARCID and TOKEN ?
            let alpha_return_bus = BPA_alpha_bus(BN_note(arcid, token), (token, g_bus_zygote, "token"), return_schema)
            close_arc ww tprops method_name "close_apply_return"  alpha_return_contact alpha_return_bus  // close_arc instance bind: src then dest here (not actuals then formal contact any longer)

            alpha_return_bus // If this is the ulimate return value from an external method, this bus will be continuously assigned to the port formals (no tail-call special verbosity reduction).
            // bind this return_bus to the instance and also add a replicator ...

    alpha_return_bus

let bp_get_d22 ww msg0 msg1 = function
    //| BPA_always_ready_expression(ivale, d22)
    | BPA_fu_contact(binding_note, _, d22) ->  (binding_note, d22)

    | other -> sf(sprintf "%s %s: get_d22 other form alpha %A" msg0 msg1 other)


    
type EMODE_t = 
    | EMODE_active
    | EMODE_active_noauto  // To supress nested ...
    | EMODE_unactive       // Simple expressions, not embedded in an alpha. Alpha will be None
    | EMODE_lmode          // LHS (of assign) mode 


//
// Reveng schema underlying data types
//

let bp_d22_mine_prec_old vbf msg = function // It's probably in the SSI_n protocol_name actually!
     | ((IPB_custom protocol_name_, _, _, _), [ valid_; rdy_ ]) -> // Unit form ---
        if vbf then vprintln 3 (sprintf "Unit/event arg  %s" msg)
        [] // 
     | ((IPB_custom protocol_name_, _, _, _), valid_ :: rdy_ :: subfields) -> //
          // TODO perhaps check name_ accords to BP convention
        let mine1 (name, dinbus_hexp) =
            let prec = mine_prec g_bounda dinbus_hexp // longwinded! Please tidy
            //if vbf then vprintln 3 (sprintf "Blah arg in rendezvous %s" site_name)
            (prec.signed=Signed, valOf_or prec.widtho 32)
        map mine1 subfields

     | other -> sf(lp_msg msg + sprintf ": bp_d22_mine_prec_old other form %A" other)    

let bp_schema00_mine_prec msg subfield_no = function // It's probably in the SSI_n protocol_name actually!
    | (IPB_custom protocol_name_, schema00) when length schema00 >= 2 ->
        let procline line = (line.signed=Signed, atoi32 line.width)
        if subfield_no >= 0 then
            let line = select_nth (subfield_no+2) schema00  // 2 nets in SSI handshake to skip over
            [procline line]
        else map procline (tl(tl schema00)) // aka cddr

    | other -> sf(lp_msg msg + sprintf ": bp_schema00_mine_prec other form %A" other)    


//
// Reveng activation underlying data types
//
let rendezvous_marshal ww site_name several = 
    let vbf = false
    let args_prec =

        let selact4 = function
            | BPA_fu_terminal(BN_bound_formal(arcid, idx, ss, (ty, signedf, bit_width)), _, _, _) -> [(signedf, bit_width)]

            // We currently only expect this form when the schema00 does not need to be refined (ie subfield_no=-1), so the question of whether it is pre-refined is moot, athough it is actually _not_ pre-refined by convention. update note elsewhere
            | BPA_fu_terminal(BN_note _, _, subfield_no, Some schemaN0) when subfield_no<0 ->
                let answers = bp_schema00_mine_prec site_name -1 schemaN0 // So wont need lane_no then.? subfield project
                answers

            | BPA_fu_contact(binding_note, terminal, d22) ->
                let answers = bp_d22_mine_prec_old vbf site_name d22
                answers

            | BPA_combinate(op, note, brothers, Some schN0) ->
                let answers = bp_schema00_mine_prec site_name -1 schN0 // So wont need lane_no then? 
                answers
            | BPA_alpha_bus(note, _, schN0) -> // If already rezzed, d22 will have been refined for the formal subfield - old approach, now deprecated. Still getting the odd fu_contact in the line above though!
            let answers = bp_schema00_mine_prec site_name -1 schN0 // So wont need lane_no then??
            answers

                                //  else [answers.[lane_select]]
            | BPA_always_ready_expression(BE_biginteger bn) -> [(true, bound_log2 bn)] // TODO the always_ready_expression could be a vector (list) of constant expressions in the future.
            | other -> sf(sprintf"selact4: %s L331 mine other %A" site_name other)
        map selact4 several
    vprintln 3 (sprintf "Rendezvous %s needs input precisions %A" site_name args_prec)

    let rzstruct =
        let add_rzmap_token subfield_types = ("rzmap-token-temp", subfield_types)
        map add_rzmap_token args_prec // for now
    rzstruct

    
let bp_eval ww settings msg nbm (tprops:thread_properties_t) (alpha000:bp_activation_t option) (env:bp_env_t) (arg:bp_exp_t) =
    let vbf = true

    let rec ev1 emode  (tprops:thread_properties_t) evalpha arg000 =
        match arg000 with
        | BPE_op(lp_, BP_assign, [BPE_op(lpo, BP_subscript, [lhs; write_idx]); rhs]) -> // Pattern match: this is shorthand for calling the 'write' method on a RAM
            let msg = "bp-eval-subscripted-assign"
            let (evald_lhs, tprops_) = ev1 EMODE_lmode tprops evalpha lhs
            match evald_lhs with
                | BE_component(instance_name, atts, the_component0, structured_ports) ->
                    vprintln 3 (sprintf "Convert %s to RAM write method call" msg)
                    // TODO note in tprops has become side-effecting?  Write method is EIS.
                    let write_cmd = // A full constant generator here is too much.  Optimise already
                       BPE_num 1I // Command 1 for write.
                    let replacement_expression = BPE_apply(BPE_op(lpo, EO_dotaccess, [BPE_var instance_name; BPE_var "cmd"]), [write_cmd; write_idx; rhs])
                    ev1 emode tprops evalpha replacement_expression
                | other ->
                    cleanexit(lp_msg msg + sprintf ": unrecognised LHS for %s %A" msg other) 
                    sf "Developer mode stop"

        | BPE_op(lpo, BP_assign, [lhs; rhs]) -> // This is shorthand for calling the 'write' method on a lhs register.
            let msg = "bp-eval-assign"
            //let (evald_, tprops, alpha) = ev1 emode tprops alpha rhs
            let (evald_lhs, tprops_) = ev1 EMODE_lmode tprops evalpha lhs
            match evald_lhs with
                | BE_component(instance_name, atts, the_component0, structured_ports) ->
                    vprintln 3 (sprintf "Convert %s to write method call" msg)
                    // TODO note in tprops has become side-effecting?  Write method is EIS.
                    let replacement_expression = BPE_apply(BPE_op(lpo, EO_dotaccess, [BPE_var instance_name; BPE_var "write"]), [rhs])
                    ev1 emode tprops evalpha replacement_expression
                | other ->
                    cleanexit(lp_msg msg + sprintf ": unrecognised LHS for %s %A" msg other) 
                    sf "Developer mode stop"
                    
        | BPE_op(lpo, BP_subscript, [lhs; read_idx]) -> // Pattern match: this is shorthand for calling the 'read' method on a RAM or something else in that typeclass.  We'll want to support bit-field extract and other RTL-like aspects in future though.
            let msg = "bp-eval-subscripted-rmode"
            let (evald_lhs, tprops_) = ev1 EMODE_lmode tprops evalpha lhs
            match evald_lhs with
                | BE_component(instance_name, atts, the_component0, structured_ports) -> // TODO please check autoapply attribute in atts
                    vprintln 3 (sprintf "Convert %s to read method call" msg)
                    let cmd_read = BPE_num 0I // Command 0 for read
                    let filler = BPE_num 0I // Constant 0 passed in to wdata port during a read operation.
                    let replacement_expression = BPE_apply(BPE_op(lpo, EO_dotaccess, [BPE_var instance_name; BPE_var "cmd"]), [cmd_read; read_idx; filler])

                    ev1 emode tprops evalpha replacement_expression 
                | other ->
                    let replacement_expression = BPE_op(lpo, EO_bit_subscription, [lhs; read_idx])
                    ev1 emode tprops evalpha replacement_expression 

                    cleanexit(lp_msg msg + sprintf ": unrecognised subscripted expression: %s %A" msg other) 
                    sf "Developer mode stop"

            
        | BPE_op(_, (EO_plus as op), [lhs; rhs])         
        | BPE_op(_, (EO_minus as op), [lhs; rhs])
        | BPE_op(_, (EO_dgtd as op), [lhs; rhs])
        | BPE_op(_, (EO_dltd as op), [lhs; rhs])
        | BPE_op(_, (EO_dged as op), [lhs; rhs])
        | BPE_op(_, (EO_dled as op), [lhs; rhs]) ->                           
            let msg = "bp-eval-arith-operator"

            let rec desugar (op, lhs, rhs, invf) =
                match op with
                    | EO_dltd -> (op, lhs, rhs, invf) // Convert all four comparisonips to less than (dltd)
                    | EO_dged -> (EO_dltd, lhs, rhs, not invf)
                    | EO_dgtd -> (EO_dltd, rhs, lhs, invf)
                    | EO_dled -> desugar (EO_dged, rhs, lhs, invf)

                    | EO_deqd -> (EO_deqd, lhs, rhs, false)
                    | EO_dned -> (EO_deqd, lhs, rhs, true)                    

                    | EO_plus -> (EO_plus, lhs, rhs, invf)
                    | EO_minus -> (EO_plus, lhs, rhs, not invf) // Minus is slightly oddly denoted as op + inverted. 
                    

            let (op, lhs, rhs, invf) = desugar (op, lhs, rhs, false)
            let (compound_arg:bp_env_vale_t, tprops) = parallel_ev1 emode msg tprops evalpha [lhs; rhs]

            // Here we willy-nilly allocate a fresh FU (adder etc.), but in future, there will be a mapping phase based on load balancing and priority.
            let instance_name = funiqu_chars 15 ("bp_basic_arith") // put line number in?
            let callsite_id = instance_name // This callsite_id is not needed until we start binding multiple AST arithmetic operations to one physcal FU.
            let msg = "for instance " + instance_name
            let ww = WF 3 "BP_eval" ww (lp_msg msg)
            let args_spec =
                let bit_width = 32 // temp for now - take larger of the two args please.
                let signedf = true // temp for now
                [ (signedf, bit_width); (signedf, bit_width) ]
            let result_spec =
                let  bit_width = 32 // temp for now
                let signedf = true // temp for now
                (signedf, bit_width)

            
            let (kind_name, vmd, structured_ports, overrides_) = bp_create_kind_canned_basic_arith_alu ww settings.canned_fu_settings msg (op, invf, args_spec, result_spec)
            let iinfo =
                    { g_null_vm2_iinfo with
                        definitionf=          false // This is an instance, so false here.
                        iname=                instance_name
                        generated_by=         g_blueparrot_banner
                        kind_vlnv=            (hpr_minfo vmd).name
                        externally_provided=  false // TBD - likey yes?
                      }

            let the_component0 = (iinfo, Some vmd)

                
            let the_component1 = C_ipinst(instance_name, (iinfo, Some vmd))  
            let atts = { g_default_bp_component_atts with femeral=true } 
            mutadd tprops.m_new_fus (instance_name, the_component1)
            let env_pseudo = BE_component(instance_name, atts, the_component0, structured_ports)
            let fu_inst = env_pseudo // This does not need to be in env, but close_call is expecting such, so refactor?
            match vmd with
                | HPR_VM2 _ -> ()
                | other -> sf(lp_msg msg + sprintf ": L220 form %A" other)    


                // A method with no arguments could be called eagerly, with disregard for state edges or following a left-to-right pattern or schedulled etc..  We use state_alpha for now.
                // A method with only constant args ditto. Their return value must rendezvous with the e_as_c control-flow alpha...?
                
                // close call will rez a result bus?
            let operation = "operate" // Standard method name. TODO explain when used please?
            let arith_result_alpha = close_call ww tprops "msg" nbm callsite_id (valOf_or_fail "need an activation for apply" evalpha) fu_inst operation (Some compound_arg)
            let alpha = if nonep alpha000 then muddy "L416" else Some(arith_result_alpha) 

            (BE_result alpha, tprops)


        | BPE_var id -> 
            match op_assoc id env with
                | None ->
                    vprintln 0 (sprintf "Identifiers in scope are " + sfold (fun (a,b) -> a) env)
                    cleanexit(lp_msg msg + sprintf ": bp_eval: unbound variable '%s'" id)
                
                        
                | Some vale ->
                    //if vbf then vprintln 3 (sprintf "ev1 identifier '%s' vale from env is %A" id vale)
                    match vale with
                        | BE_component(instance_name, atts, _, _) when emode=EMODE_active && not_nonep atts.autoapply_methodname ->
                            let method_name = BPE_var(valOf atts.autoapply_methodname)
                            let auto_args = []
                            let replacement_expression = BPE_apply(BPE_op(None, EO_dotaccess, [arg000; method_name]), auto_args)
                            if vbf then vprintln 3 (sprintf "'%s' want to autoapply %A" id replacement_expression)
                            ev1 EMODE_active_noauto tprops evalpha replacement_expression
                        | other ->
                            // We ultimately count the number of uses of a bound variable within a strictness site
                            let is_static = 
                                match vale with 
                                    | BE_component _ -> true // Static elaboration value does not need to be noted.
                                    | _ -> false

                            let site_name = funiqu_chars 15 "VAR_" + id
                            if not is_static then
                                let note_arg_use id vale = mutadd tprops.m_arg_uses ("identifier-use-site", id, vale)
                                note_arg_use id vale
                            // Consider: when exported method loops forever, its aout port is unreachable and never valid.

//Consider local registers.
// Consider: an array of instantiated, separately-compiled, identical FUs with common control flow should share a common controller, whether tokenised, sequenced or hybrid.
                            let vale = // State edge rendezvous may be required here? Consider: or, should it go in to the stateful FU apply rendezvous?
                                if is_static then vale
                                elif nonep evalpha then vale // evalpha will typically be a unit activation representing a state edge. When would it not be?
                                else
                                    let dede = function // Abstract me as eval_tidy
                                        |BE_bound_activation(history_, alpha) -> alpha
                                        |BE_result(Some alpha) -> alpha                                    
                                        | other -> sf (sprintf "L456 dede eval_tidy other %A" other)
                                    // idiomatic code follows: abstract please
                                    let brothers = [ valOf evalpha; dede vale ]
                                    let rzstruct = rendezvous_marshal ww site_name brothers
                                    let arg_precs = map snd rzstruct
                                    let result_schemaN0 = bp_gec_rendezvous_return_schema ww msg arg_precs

                                    let a0 = BPA_combinate(BPC_rendezvous rzstruct, BN_note(site_name, "bv-rendezvous-L453"), brothers, Some result_schemaN0)
                                    BE_result(Some a0)
                            (vale, tprops) // Bound variable use is noted here so that replication can be added above if this zone is non-strict.

        | BPE_apply(BPE_op(lp_, EO_dotaccess, [fu_inst; BPE_var method_name]), args) ->
            let (fu_inst, tprops) = ev1 EMODE_unactive tprops None fu_inst  // Unactivated eval for fu handle.
            let callsite_id = funiqu_chars 15 (lp_lineno() + "callsite") // Remove quotes we hope
            let msg = sprintf "method '%s' callsite %s" method_name callsite_id
            let ww = WF 3 "BP_eval" ww (lp_msg msg)

            // If the args are all constant and there is no state edge ordering, the input activation, evalpha, can be ignored.
            let (compound_arg:bp_env_vale_t, tprops) = parallel_ev1 emode msg tprops evalpha args // Call by value, all args evaluated in parallel.
            // Callsite manager addded during normalisation ensures each method ultimately is called from point.
            let return_alpha = close_call ww tprops "msg" nbm callsite_id (valOf_or_fail "need an activation for apply" evalpha) fu_inst method_name (Some compound_arg)  // get_side    
            let alpha = Some(return_alpha)
            // TODO tprops needs eis to be set if called method is eis.
            (BE_result alpha, tprops)

        | BPE_num ivale -> // Numeric constants 
            let vale = BE_biginteger ivale
            if emode=EMODE_unactive then
                (vale, tprops)

            elif false then // Create activated constant instead of hopelessly-verbose constant generators
                (BE_result(Some(BPA_always_ready_expression vale)), tprops)
            //match alpha with
            //| other -> sf (sprintf "Cannot activate with constant other form alpha %A" other)
            else
                vprintln 3 (sprintf "Create constant generating activation of %A" ivale)
                let (kind_name, vmd, structured_ports, overrides_) = bp_create_kind_canned_integer_generator ww settings msg ivale
                let instance_name = funique ("constantgen")
                let iinfo =
                    { g_null_vm2_iinfo with
                        definitionf=          false // This is an instance, so false here.
                        iname=                instance_name
                        generated_by=         g_blueparrot_banner
                        kind_vlnv=            (hpr_minfo vmd).name
                        externally_provided=  false // TBD - likey yes?
                     }

                let the_component0 = (iinfo, Some vmd)
                let port_labels = [] // for now    
                let the_component1 = C_ipinst(instance_name, (iinfo, Some vmd))
                let atts = { g_default_bp_component_atts with femeral=true } 
                mutadd tprops.m_new_fus (instance_name, the_component1)

                // We'll try to manage without binding put ports on our constant generators from day zero, although its against our nacent religion.

                let (method_name, side) = ("operate", "aout")
                let (return_schema, alpha_return_point) =
                    match op_assoc (method_name, side) structured_ports with
                        | None ->
                            vprintln 0 (sprintf "Available ports/methods are " + sfold (fun ((name, side), _) -> name + "/" + side) structured_ports)
                            sf(sprintf "Need '%s/%s' return contact from constant generator '%s'" method_name side instance_name)
                        | Some ((ipb_protocol, ss_, bool_, schema), nets) ->
                            let d22 = ((ipb_protocol, ss_, bool_, schema), nets)
                            let return_schema = (ipb_protocol, schema)
                            let terminal = (instance_name, method_name, side)
                            let alpha_return_point =
                                let binding_note = BN_note(funique "edgekey", "constant-ival-L421")
                                BPA_fu_terminal(binding_note, terminal, -1, Some(get_schemaN0 d22)) // (note, direction, terminal_id, lane,  full_schema)
                            //BPA_fu_contact(binding_note, terminal, d22) 
                            (return_schema, alpha_return_point)
                (BE_result(Some alpha_return_point), tprops)

                //let sex = None // local
                //let id = instance_name
                //let return_bus = nbm.busrez2 ww [] sex return_schema (sprintf "%s_result_bus" id)  
                //let constant_generator_alpha = BPA_alpha_bus(BN_note("arc:" + id, id), return_bus)
                //close_arc ww tprops method_name "close_constantgen_return" constant_generator_alpha alpha_return_point
                //(BE_result(Some constant_generator_alpha), tprops)

        | other -> sf(lp_msg msg + sprintf ": bp_eval other form %A" other)    


    and parallel_ev1 emode msg tprops evalpha = function
        | [] -> (BE_result evalpha, tprops) 
        | [onlyone] ->
            let (vale, tprops) = ev1 emode tprops evalpha onlyone
            //dev_println (sprintf "ev1 arg=%A\nresult=%A" onlyone vale)
            (vale, tprops)


        | several ->
            let vbf = true
            let site_name = funique "pareval"
            let several = map (ev1 emode tprops evalpha) several //TODO: Parallel eval needs to do a phi on the property changes in the parallel strands! Don't use map then - fold the props and replicate the activation event.

            let several =
                let eval_tidy = function
                    | (BE_bound_activation(_, alpha), _) -> alpha
                    | (BE_result(Some alpha), tprops_should_have_been_chained__) -> alpha
                    | other -> sf (sprintf "eval tidy L485 other form %A" other)
                map eval_tidy several

            // Create rendezvous of several parallel activations.  If unactivated, this is all unnecessary -- please add another route above, so "alphas will be used" always here.
            // idiomatic code follows: abstract please
            let rzstruct = rendezvous_marshal ww site_name several
            let arg_precs = map snd rzstruct
            let result_schemaN0 = bp_gec_rendezvous_return_schema ww msg arg_precs
            let a0 = BPA_combinate(BPC_rendezvous rzstruct, BN_note(site_name, "unused-L480"), several, Some result_schemaN0)
            (BE_result(Some a0), tprops)

    let emode = (if nonep alpha000 then EMODE_unactive else EMODE_active)
    let (v, tprops) = ev1 emode tprops alpha000 arg
    (v, tprops) // env always unchanged?  for now anyway.

// Unactivated eval
let bp_eval_unactivated ww settings msg nbm env exp =
    let tprops = { tid= "aux"; eis= false; m_arcs= ref []; m_new_fus= ref[]; m_arg_uses=ref []  }
    let alpha = None
    let (evald, tprops_) = bp_eval ww settings msg nbm tprops alpha env exp
    // check arcs is still empty please!
    evald

// Constant variant too please ...



type bp_code_label_t =
    {
        lab_name:      string
        incoming:      bp_activation_t list ref
    }

let new_lab thread_name key = // a code 'label'
    let lab_name = funiqu_chars 20 ("LL_" + thread_name + "/" + key)
    { lab_name=lab_name; incoming = ref [] }

//
//       
//
let bp_compile_cmds ww settings thread_name nbm tprops alpha return_lab env cmd =


    let unit_schema = bp_gec_standard_synchronous_netport_schema "l" [] // Unit schema
        
    let rec compile_cmd1 (tprops:thread_properties_t, labs, alpha, env) cmd =
        //dev_println (sprintf "Next command is %A" cmd)
        match cmd with
        | BP_skip -> (tprops, alpha, env) // A NOP (no-operation)

        // Consider: please define the semantics of break, continue and return in a parallel block.
        | BP_block lst -> compile_serial_cmd_list (tprops, labs, alpha, env) lst

        | BP_e_as_c(lp, exp) -> // Consider: Should we rendezvous the state or end of the subcommand with the state alpha ?  Interesting!
            note_lp lp                            
            let msg = lp_msg "BP e_as_c"
            let ww = WF 3 "BP compile" ww msg
            let do_easc msg exp = // TODO Consider: we could give it a headstart and rendezvous on the result value. 
                let (eval_result, tprops) = bp_eval ww settings msg nbm tprops alpha env exp
                match eval_result with
                    | BE_result(Some alpha) -> (tprops, Some alpha)
                    | other -> sf (lp_msg (sprintf "Other from e_as_c result %A" other))

            let (tprops, alpha) = do_easc "BP expression as command" exp
            (tprops, alpha, env)                            


        | BP_continue(lp)
        | BP_break(lp) ->
            note_lp lp                                        
            let breaks = (match cmd with |BP_break _ -> "break" | _ -> "continue")
            let msg = lp_msg (sprintf "BP %s command" breaks)
            let ww = WF 3 "BP compile" ww msg
            let (continuation_lab, break_lab, return_lab_) = labs
            let lab = if breaks = "break" then break_lab else continuation_lab
            if nonep lab then cleanexit (lp_msg (sprintf ": %s statement encountered outside of any loop body" breaks))
            mutadd (valOf lab).incoming (valOf_or_fail "L656" alpha)
            (tprops, None, env) // These are control-flow stops with no outgoing alpha.
            
        | BP_return(lp, exp) ->
            note_lp lp                                        
            let msg = lp_msg "BP return command"
            let ww = WF 3 "BP compile" ww msg
            let (continuation_lab_, break_lab_, return_lab) = labs
            if nonep return_lab then cleanexit (lp_msg ": return statement found outside of a returning context")

            //dev_println(sprintf "Alpha at start of BP_return (thread/method %s) is %A" msg (alphaToStr (valOf_or_fail "L588" alpha)))
            let (result, tprops) = bp_eval ww settings msg nbm tprops alpha env exp
            match result with
                | BE_result(Some(BPA_alpha_bus(note, _, _) as alpha)) ->
                    mutadd (valOf return_lab).incoming alpha
                | other ->
                    // Consider: when does an always ready resource want to ignore its input (put) activation?
                    // TODO Consider: a manifest constant will ignore its input activation.  That's ok really.  Is a register-read likewise free to ignores its input activation. I think so? We don't want a debugging print of a constant to 'print all the time' though.
                    sf(lp_msg msg + sprintf ": expected a bound activation or similar. other=%A" other)

            (tprops, Some BPA_dead, env)

#if OLD
//This is the temp code not using the return lab.
            match result with
                | BE_result(Some(BPA_alpha_bus(note, _, _) as alpha)) ->
                    dev_println(sprintf "Alpha being returned by BP_return (%s) is from alpha_bus %A" msg (alphaToStr alpha))
                    (tprops, Some alpha, env) // TODO tidy - these two forms are equivalent always

                | BE_result(Some(BPA_fu_contact _ as alpha)) ->
                    dev_println(sprintf "Alpha being returned by BP_return (%s) is from fu_contact %A" msg (alphaToStr alpha))
                    (tprops, Some alpha, env)

                    (tprops, alpha, env)                            
#endif
                    
        | BP_for(lp, icmd, gexp, cmd, body) -> // Recode as a while for now, although we recognised that will destroy useful strucutre useful for loop optmisations ...
            note_lp lp
            let expansion = BP_block[icmd; BP_while(lp, gexp, BP_block [ body; icmd ])]
            compile_cmd1 (tprops, labs, alpha, env) expansion
        
        | BP_while(lp, gexp, body) ->
            note_lp lp
            let (continuation_lab__, break_lab__, return_lab) = labs
            let (continuation_lab, break_lab) = (new_lab thread_name "continue", new_lab thread_name "break")
            let loop_id = continuation_lab.lab_name
            let msg = lp_msg "BP compile 'while' command " + loop_id 
            let ww = WF 3 msg ww "start"

            // The body is non-strict, so the head arc will convey the free variables, as per 'if' and loop-carried updates too!?
            let tig schema = 
                let head_arc = BPA_alpha_bus(BN_note(loop_id + "HA", msg), (loop_id + "_head_arc", g_bus_zygote, "token"), schema)
                let body_entry_arc = BPA_alpha_bus(BN_note(loop_id + "BEA", msg), (loop_id + "_body_entry", g_bus_zygote, "token"), schema)
                (head_arc, body_entry_arc)

            let (head_arc, body_entry_arc) = tig unit_schema // Use a temporary schema (or None) until these are known.

            let m_arg_uses = ref []
            let tprops101 = { tprops with m_arg_uses= m_arg_uses }
            let (guardian, tprops101) = bp_eval ww settings thread_name nbm tprops101 (Some head_arc) env gexp

            // TODO - check if guardian is manifestly constant and then simplify accordingly.
            let labs = (Some continuation_lab, Some break_lab, return_lab)
            let (tprops101, end_of_body_alpha, env_) = compile_cmd1 (tprops101, labs, Some body_entry_arc, env) body

            // Compilation of guard and body are now complete. Wire them up, redefining the continue schema for free variable access etc..
            // TODO chaining of tprops - used functionally ... hmm easier to make mutable, but for now need to fold effects back to the returned tprops - copy 1

            // Rendezvous the bound activations feeding into the non-strict body. Consider: we need to rendezvous for each point of entry to the region?
            let tidy_uses =
                let linear_uses_tidy = function
                    | (_, _, BE_bound_activation(_, alpha)) -> alpha
                    | (_, _, BE_result(Some a)) -> a
                    | other -> sf "uses tidy L722"
                map linear_uses_tidy !m_arg_uses
            let rzstruct = rendezvous_marshal ww loop_id tidy_uses

            // TODO wire-in this rendezvous. It will have an augmented schema owing to the split predicate, so just put None now for convenience and let the back end work it out. This means rzstruct is wrong. TODO.
            //let a0 = BPA_combinate(BPC_rendezvous rzstruct, BN_note(loop_id + "_RVZ", "unused-L658"), tidy_uses, None)
            let arg_precs = map snd rzstruct
            let schemaN0 = bp_gec_rendezvous_return_schema ww msg arg_precs

            // Did we note the rendezvous'd values to the parent arg_uses have we? Yes, done on env lookup.

            let (head_arc, body_entry_arc) = tig schemaN0 // Regnerate the arcs with correct schema now known.

            // Arcs connected to the head are: the incoming alpha, the end_of_body_alpha and any continue arcs
            let _ =
                let end_of_body = if nonep end_of_body_alpha then [] else [valOf end_of_body_alpha]
                let flows = (valOf_or_fail "L736-alpha" alpha)::(!continuation_lab.incoming @ end_of_body)
                match length flows with
                    | 0 -> () // BPA_dead - unreachable loop.
                    | 1 ->
                        close_arc ww tprops "LoopHeadJoin" "head-join" (hd flows) head_arc // eg. an infinite loop may have no backedge.
                    | n ->
                        let head_join = BPA_combinate(BPC_joinphi, BN_note(loop_id + "_HJ", "unused-L703"), flows, Some unit_schema)
                        close_arc ww tprops "LoopHeadJoin" "loop-head-join" head_join head_arc
            
            // The splitter routes the guardian to either the body_entry_arc or to the predicate_fail_arc
            let predicate_fail_arc = BPA_alpha_bus(BN_note(loop_id + "PFA", msg), (loop_id + "_predicate_fail", g_bus_zygote, "token"), schemaN0)
            let guardian_alpha = // TODO: Combine with best bits of 'if' code please.
                match guardian with      // The guardian could be an infinite loop itself.
                    | BE_result(alpha_o)                             -> alpha_o
                    | BE_bound_activation(strictness_history, alpha) -> Some alpha // Strictness history is not needed... alpha has been modified to refer to the output of the strictness splitter.
                    | other -> sf(lp_msg msg + sprintf ": L752 other form guardian (eg infinite loop?) %A" other)
            // Do not invoke close_arc until temporary schema is replaced with full schema.

            if not_nonep guardian_alpha then // The guardian could be an infinite loop itself.
                let split = BPA_combinate(BPC_split, BN_note(loop_id + "SPLIT", "unused-L709"), [predicate_fail_arc; body_entry_arc], Some unit_schema)
                close_arc ww tprops "LoopHeadJoin" "loop-head-join" (valOf guardian_alpha) split // src then dest order here.

            // The outgoing activation is the predicate_fail_arc and any break arcs.
            let outgoing_arc =
                if nullp !break_lab.incoming then predicate_fail_arc
                else BPA_combinate(BPC_joinphi, BN_note(loop_id + "_OGA", "unused-L706"), predicate_fail_arc::!break_lab.incoming, Some unit_schema)
            (tprops, Some outgoing_arc, env) // TODO: would prefer to return None activate instead of Some BPA_dead or worse.

        | BP_if(lp, gexp, true_hand, false_hand_option) ->
            note_lp lp
            let cmd_id = funiqu_chars 15 (sprintf "BP-IF%s" thread_name)
            let msg = lp_msg "BP compile 'if' command " + cmd_id
            let ww = WF 3 msg ww "start"
            let (guardian, tprops) = bp_eval ww settings msg nbm tprops alpha env gexp


            let strict_id = cmd_id // Nested strictness context tag.
            let gec_child_alphas schemaN0 =
                let tig key = BPA_alpha_bus(BN_note(cmd_id, msg), (cmd_id+"_"+key, g_bus_zygote, "token"), schemaN0)
                (tig "true-start", tig "false-start")

            
            let (alpha_true_start, alpha_false_start) =
                let temp_schema = bp_gec_standard_synchronous_netport_schema "l" [] // Unit schema
                gec_child_alphas temp_schema  // This schema will be replaced once we know the environment needs of the hands below.
            

            // Here we can check whether the guardian is manifestly constant and only compile one side of the IF if so. TODO

            // perhaps it is better to precompile to get VSFG?
            // To work out the arguments that need passing in, we need to evaluate the two hands first, before rezzing an appropriate splitter.
            // The arg does not need to go through the splitter really, but the two hands need padding out to use the union of args in each precisely the same number of times, such as once.

            let guardian_alpha =
                match guardian with      // The guardian could be an infinite loop itself.
                    | BE_result(Some alpha) -> alpha
                    | BE_bound_activation(strictness_history, alpha) -> alpha                     // Strictness history is not needed... alpha has been modified to refer to the output of the strictness splitter.
                    | other -> sf(lp_msg msg + sprintf ": other form guardian (eg infinite loop?) %A" other)
            // Do not invoke close_arc until temporary schema is replaced with full schema.

            let splitter_instance_name = cmd_id + "_SPLIT"            
            let (arg_uses_tt, arg_uses_ff) = (ref [], ref [])

            // Naming schemes:     splitter and joiner have identical naming schemes:  inputs are operate/put and outputs are operate/aout where operate has a sufix _n if there is more than one of them (output of splitter and input of joiner).  Both splitters and joiners can route value through, although the schemas will generally match.  Moreover, the splitter will consume its first input subfield and so the split activations have one fewer subfields.


            // TODO: evaluate to-be-split options in the current env - causing use notes for our parent and bind 

            
            let tag_env splitter_port_no env =
                let tag_terminal ot = // It's more of a rewrite
                    let (instance_name, portname, side) = ot
                    let nt = (splitter_instance_name, sprintf "operate_%i" splitter_port_no, "aout")
                    dev_println (sprintf "Splitter terminal rename %s -> %s" (bp_terminalToStr ot) (bp_terminalToStr nt))
                    nt
                let tag_alpha = function
                    | BPA_fu_terminal(binding_note, terminal, lanes_no, schemaN0) -> BPA_fu_terminal(binding_note, tag_terminal terminal, lanes_no, schemaN0)
                    | other -> sf(sprintf "tag_alpha other form %A" other)
                let tag_env = function // Cannot only do the ones used by the inner context since we don't know that yet! Hence we cannot allocate the subfield numbers, so the new subfield numbers need a further level of indirection...
                    //| (bv_name, BE_result)
                    | (id, (BE_component _ as vale)) -> (id, vale)
                    | (id, BE_bound_activation(binding_history, vale)) -> (id, BE_bound_activation(strict_id::binding_history, tag_alpha vale))
                    | other -> sf(sprintf "tag_env other form %A" other)
                map tag_env env
            let (tprops, alpha_true_end, env__) =
                let tprops101 = { tprops with m_arg_uses=arg_uses_tt }
                compile_cmd1 (tprops101, labs, Some alpha_true_start, tag_env 1 env) true_hand
            // Chain EIS taint and other threadprops through both hands
            let (tprops, alpha_false_end, env__) =
                match false_hand_option with
                    | Some false_hand ->
                        let tprops101 = { tprops with m_arg_uses=arg_uses_ff }
                        compile_cmd1 (tprops101, labs, Some alpha_false_start, tag_env 0 env) false_hand            
                    | None -> (tprops, Some alpha_false_start, env)

                    // TODO chaining of tprops - used functionally ... hmm easier to make mutable, but for now need to fold effects back to the returned tprops - copy 2

            let union_uses = lst_union !arg_uses_tt !arg_uses_ff
            dev_println (sprintf "Hands of the if %s need %i vs %i bound variables. Total %i." cmd_id (length !arg_uses_tt) (length !arg_uses_ff) (length union_uses))

            // TODO check expression type is suitable for use as a boolean.
            let union_uses = ("", "", BE_result(Some guardian_alpha)) :: union_uses // First arg is dispatcher to splitter.

            // Rendezvous the bound activations feeding into the non-strict region.
            let tidy_uses =
                let linear_uses_tidy = function
                    | (_, _, BE_bound_activation(_, alpha)) -> alpha
                    | (_, _, BE_result(Some a)) -> a
                    | other -> sf "uses tidy L671"
                map linear_uses_tidy union_uses
            let rzstruct = rendezvous_marshal ww cmd_id tidy_uses


            let a0 = BPA_combinate(BPC_rendezvous rzstruct, BN_note(cmd_id + "_RVZ", "unused-L658"), tidy_uses, None)

            // Create union schema: fragile: we need union over bound variables which can be achieved with union over terminals but a simple union is
            // being used now (including over BE_result baggage which is possibly going to fail unification in the future?)

            // The rendezvous will have numbered these fields, not us here.  The same numbers will apply for the splitter input and output.
            //let schemaN0 = // Field_no numbers effectively change over this unification and a mapping is likely needed.
            let arg_precs = map snd rzstruct
            let schemaN0 = bp_gec_rendezvous_return_schema ww msg arg_precs

            let alpha_joined =
                match (alpha_true_end, alpha_false_end) with
                    | (Some alpha_true_end, Some alpha_false_end) ->
                        vprintln 3 (lp_msg msg + ": Two output phi activations")
                        Some(BPA_combinate(BPC_joinphi, BN_note(cmd_id + "_PHI", msg), [ alpha_true_end; alpha_false_end ], Some schemaN0))
                    | (Some a0, None) 
                    | (None, Some a0) ->
                        vprintln 3 (lp_msg msg + ": one output phi activation")
                        Some a0
                    | _ ->
                        vprintln 3 (lp_msg msg + ": no output phi activations")
                        None


            let (alpha_true_start, alpha_false_start) = gec_child_alphas schemaN0
            let alpha_point = BPA_combinate(BPC_split, BN_note(splitter_instance_name, msg), [alpha_false_start; alpha_true_start], Some schemaN0) // Defer creating the splitter
            close_arc ww tprops msg ("close_splitter_guardian_input") a0 alpha_point // Finally commit with revised schema.
            //dev_println(sprintf "Alpha at end of BP-IF  (thread/method %s) is %A" msg alpha_joined)
            let ww = WF 3 msg ww "end"
            (tprops, alpha_joined, env)

        | BP_if(lp, gexp, true_hand, false_hand_option) when false -> // unit-if non-tertiary. old code. delete me.
            note_lp lp
            let msg = lp_msg "BP if command"
            let ww = WF 3 "BP compile" ww msg
            let (guardian, tprops) = bp_eval ww settings msg nbm tprops alpha env gexp

            // OLD CODE: Here we can check whether the guardian is manifestly constant and only compile one side of the IF if so. TODO

            let iname_root = funique ("CFG" + lp_lineno())
            let instance_name_split =  iname_root + "_oldsplit"
            let instance_name_join  =  iname_root + "_oldphi"


            let (kind_name_s, vmd_s, structured_ports_s, overrides_) = bp_create_kind_canned_split_phi ww settings.canned_fu_settings msg "UNIT_SPLIT" []
            let (kind_names_, vmd_j, structured_ports_j, overrides_) = bp_create_kind_canned_split_phi ww settings.canned_fu_settings msg "UNIT_PHI" []
            let iinfo_s =
                    { g_null_vm2_iinfo with
                        definitionf=          false // This is an instance, so false here.
                        iname=                instance_name_split
                        generated_by=         g_blueparrot_banner
                        kind_vlnv=            (hpr_minfo vmd_s).name
                        externally_provided=  false // TBD - likey canned really.
                      }

            let iinfo_j =
                    { g_null_vm2_iinfo with
                        definitionf=          false // This is an instance, so false here.
                        iname=                instance_name_join; 
                        generated_by=         g_blueparrot_banner
                        kind_vlnv=            (hpr_minfo vmd_j).name
                        externally_provided=  false // TBD - likey canned really.
                      }


            let port_labels = [] // for now    

            let atts = { g_default_bp_component_atts with femeral=true } 
            let the_component0_s = (iinfo_s, Some vmd_s)
            let the_component1_s = C_ipinst(instance_name_split, (iinfo_s, Some vmd_s))    
            let the_component0_j = (iinfo_j, Some vmd_j)
            let the_component1_j = C_ipinst(instance_name_join, (iinfo_j, Some vmd_j))
            mutadd tprops.m_new_fus (instance_name_join, the_component1_j)



            let sex = None // Join: create four unit alphas to join the components
            // Bind guard expression egress alpha (aka guardian) to forker (aka splitter).
            let _ =
                let (port_name, side) = ("operate", "put")
                let (schema, alpha_point) =
                    match op_assoc (port_name, side) structured_ports_s with
                        | None ->
                            vprintln 0 (sprintf "Available ports/methods are " + sfold (fun ((name, side), _) -> name + "/" + side) structured_ports_s)
                            cleanexit(sprintf "If/then needs method '%s' '%s' call schema from %A" port_name side instance_name_split)
                        | Some ((ipb_protocol, ss_, bool_, schema), nets) ->
                            let d22 = ((ipb_protocol, ss_, bool_, schema), nets)
                            let schema = (ipb_protocol, schema)
                            let binding_note = BN_note(funique "edgekey", "*notinuse*648")
                            let terminal = (instance_name_split, port_name, side)
                            //let alpha_fork_point = BPA_fu_contact(binding_note, terminal, d22)
                            let alpha_fork_point = BPA_fu_terminal(binding_note, terminal, -1, Some(get_schemaN0 d22))
                            (schema, alpha_fork_point)
                let guardian_alpha =
                    match guardian with
                        | BE_result(Some alpha) -> alpha
                        | other -> sf(lp_msg msg + sprintf ": other form guardian %A" other)
                close_arc ww tprops port_name (lp_msg "close_ifthen_splitter_guardian_input") guardian_alpha alpha_point
                ()

            // Create forked hands and bind to splitter outputs.
            let (alpha_true_start, alpha_false_start) = 
                let rezandbind hand_idx =
                    let (port_name, side) = (sprintf "operate_%i" hand_idx, "aout")
                    let (schema, alpha_point) =
                        match op_assoc (port_name, side) structured_ports_s with
                            | None ->
                                vprintln 0 (sprintf "Available ports/methods are " + sfold (fun ((name, side), _) -> name + "/" + side) structured_ports_s)
                                cleanexit(sprintf "If/then needs contact '%s/%s' from %A" port_name side instance_name_split)
                            | Some ((ipb_protocol, ss_, bool_, schema), nets) ->
                                let d22 = ((ipb_protocol, ss_, bool_, schema), nets)
                                let schema = (ipb_protocol, schema)
                                let terminal = (instance_name_split, port_name, side)
                                let binding_note = BN_note(funique "edgekey", "*notinuse*673")
                                //let alpha_forked_point = BPA_fu_contact(binding_note, terminal, d22)
                                let alpha_forked_point = BPA_fu_terminal(binding_note, terminal, -1, Some(get_schemaN0 d22))
                                (schema, alpha_forked_point)

                    alpha_point
                (rezandbind 0, rezandbind 1)

            let (arg_uses_tt, arg_uses_ff) = (ref [], ref [])
            let (tprops, (alpha_true_end:bp_activation_t option), env__) =
                let tprops101 = { tprops with m_arg_uses=arg_uses_tt }
                compile_cmd1 (tprops101, labs, Some alpha_true_start, env) true_hand

            let (tprops, alpha_false_end, env__) =
                match false_hand_option with
                    | Some false_hand ->
                        let tprops101 = { tprops with m_arg_uses=arg_uses_ff }
                        compile_cmd1 (tprops101, labs, Some alpha_false_start, env) false_hand            
                    | None -> (tprops, Some alpha_false_start, env)

                    //let operation = "operate" // Standard method name
            dev_println (sprintf "If hands need %i vs %i bound variables" (length !arg_uses_tt) (length !arg_uses_ff))


            // Bind the resulting activations from the two hands/branches to the phi/join inputs.
            let _ =
                let bind_join_input (input_idx, arg_activation) =
                        let arg_activation = valOf_or_fail msg arg_activation
                        //if vbf then vprintln 3 (sprintf "Binding 'if' hand %s input %i" instance_name input_idx)
                        //let (note, alpha_d22) = bp_get_d22 ww msg "phi/join bind input" arg_activation
                        let formal_contact =
                            let (port, side) = (sprintf "operate_%i" input_idx, "put")
                            match op_assoc (port, side) structured_ports_j with
                                | None ->
                                    vprintln 0 (sprintf "Available ports/methods for join input are " + sfold (fun ((name, side), _) -> name + "/" + side) structured_ports_j)
                                    hpr_yikes(sprintf "Cannot find '%s/%s' on %s" port side instance_name_join)
                                    BPA_dead // for now
                                | Some ((ipb_protocol, ss_, bool_, schema), nets) ->
                                    let d22 = ((ipb_protocol, ss_, bool_, schema), nets)
                                    let binding_note = BN_note(funique "edgekey", "*notinuse*699")
                                    let terminal = (instance_name_join, port, side)
                                    BPA_fu_terminal(binding_note, terminal, -1, Some(get_schemaN0 d22)) // (note, direction, terminal_id, lane,  full_schema)
                                    //BPA_fu_contact(binding_note, terminal, d22) 
                        close_arc ww tprops instance_name_join "close_ifthen_join_arg" arg_activation formal_contact // close_arc instance bind: src (activation from basic block output) then dest (input to phi).
                        ()

                bind_join_input (0, alpha_true_end)
                bind_join_input (1, alpha_false_end)                       

            // Bind a return bus from the phi/join
            let (port_name, side) = ("operate", "aout")
            let terminal = (instance_name_join, port_name, side)
            let (return_schema, alpha_return_contact) =
                match op_assoc (port_name, side) structured_ports_j with
                    | None ->
                        vprintln 0 (sprintf "Available ports/methods are " + sfold (fun ((name, side), _) -> name + "/" + side) structured_ports_j)
                        cleanexit(sprintf "If/then needs contact '%s/%s' return schema from %A" port_name side instance_name_join)
                    | Some ((ipb_protocol, ss_, bool_, schema), nets) ->
                        let d22 = ((ipb_protocol, ss_, bool_, schema), nets)
                        let return_schema = (ipb_protocol, schema)
                        let terminal = (instance_name_join, port_name, side)
                        let alpha_return_contact =
                            //BPA_fu_contact(binding_note, terminal, d22)
                            let binding_note = BN_note(funique "edgekey", "*notinuse*699")
                            BPA_fu_terminal(binding_note, terminal, -1, Some(get_schemaN0 d22)) // (note, direction, terminal_id, lane,  full_schema)
                        (return_schema, alpha_return_contact)

            (tprops, Some alpha_return_contact, env)

        | other -> sf(lp_msg thread_name + sprintf ": other form bev command %A" other)



        // NB: parallel construct is easy to add...
        
    and compile_serial_cmd_list (tprops, labs, alpha, env) = function
        | [] -> (tprops, alpha, env)
        | h::tt ->
            match h with
                | BP_return _ -> // Early version of this code!
                    if not_nullp tt then cleanexit(lp_msg thread_name + ": return statement must be at end of the last block in this early version of BP compiler")
                | _ -> ()
            let (tprops, alpha, env) = compile_cmd1 (tprops, labs, alpha, env) h
            //dev_println(sprintf "Alpha in intermediate command sequence thread/method %s with %i to go is %A" thread_name (length tt) alpha)
            compile_serial_cmd_list (tprops, labs, alpha, env) tt

    let (continuation_lab, break_lab, return_lab) = (None, None, return_lab)
    let labs = (continuation_lab, break_lab, return_lab)

    let (tprops, alpha, env) = compile_cmd1 (tprops, labs, alpha, env) cmd
    //dev_println(sprintf "Alpha at end of command sequence thread/method %s is %A" thread_name alpha)
    (tprops, alpha, env)


let note2archid msg = function
    | BN_note(arcid, _) -> arcid
    | BN_bound_formal(arcid, idx, ss, (ty, signedf, width)) -> arcid
    //| other -> sf(lp_msg msg + sprintf ": note2arcid other form %A" other)


//
// Write out in GraphViz dot format for visualisation.
//       
let make_dot_report ww settings mod_name msg instances methods edge_centric_bindings =
    let ww = WF 2 "BP" ww (sprintf "Writing a dot cfg report.  keyname=%s  %s" mod_name msg)

    let rec port_fold = function
        | []     -> "NIL"
        | [(port_name, port_label)] -> sprintf "<%s> %s" port_name port_label
        | (port_name, port_label)::tt -> sprintf "<%s> %s |" port_name port_label  + port_fold tt


    // For port use you need to add this line or at least make sure the shape is 'record'
    let port_use_directive = DLITERAL("node [shape=record, height=.1];\n")
    let plip_child_fu cc = function
        | (_, C_ipinst(iname, (iinfo, Some vmd))) ->
            let port_labels =
                let rez_label cc = function
                    | DB_group(dbmeta, _) ->
                        //dev_println(sprintf "DBmeta is %A" dbmeta)
                        if dbmeta.kind  = g_hprtl_director_formals then cc //Do not display director connections (clk, reset, etc.).
                        elif dbmeta.pi_name = "" then cc
                        else (dbmeta.pi_name, dbmeta.pi_name)::cc
                    | arg -> sf (sprintf "rez_label show other form %A" arg)
                List.fold (rez_label) [] (hpr_decls vmd)
            // let gubbins = "cmd=" + newop.cmd + "\n" + sfoldcr_lite (fun x->fully_sanitise (xToStr x)) newop.args
            let label = sprintf "<iname> %s|%s" iname (port_fold port_labels)
            let ats = [ ("shape", "record"); ("label", label); ("color", "blue") ]
            DNODE_DEF(iname, ats)::cc
        | _ -> cc

    let nodes = List.fold plip_child_fu [] instances

    let plip_exported_method cc = function // Change this for port sharing, where there's more than one method on some ports.
        | (_, C_method_elaborated(method_name, arcid, _, _, _)) ->


            let label = sprintf "<> %s | <put> put  | <aout> aout" method_name
            let ats = [ ("shape", "record"); ("label", label); ("color", "orange") ]
            DNODE_DEF(method_name, ats)::cc
        | _ -> cc

    let nodes = List.fold plip_exported_method nodes methods

    let arcs =
        let plip_interconnect cc = function  // render arc in dot plot.
            | (stereo_arc_id, ends) ->
                let arc_id:string  = fst stereo_arc_id + snd stereo_arc_id
                let qq (arcid, (instance_name, method_name, side)) =
                    if instance_name = g_self_instance_name then // ie "$MYSELF"
                        DNODE(method_name, side) //
                    else DNODE(instance_name, method_name + "_" + side) // port ..
                if length ends < 2 then cc
                elif length ends <> 2 then
                    let endToStr = function
                        | (arcid_, (a1, a2, a3)) -> sprintf "End %s/%s/%s" a1 a2 a3
                        //| other -> sf(sprintf "endToStr other %A" other)
                    vprintln 0 (sprintf "Ends are " + sfold endToStr ends)
                    hpr_yikes(sprintf "Dot output: %s %i vertices at the ends of arc %s" msg (length ends) arc_id)


                    let manyends cc v2 =
                        let ats = [  ("label", arc_id); ("color", "yellow") ]
                        DARC(qq (hd ends), qq v2, ats)::cc
                    List.fold manyends cc (tl ends)
                else
                    let (v1, v2) = (hd ends, cadr ends)
                    let ats = [  ("label", arc_id); ("color", "brown") ]
                    DARC(qq v1, qq v2, ats)::cc
            | other ->
                dev_println(sprintf "dot report plip_interconnect other %A" other)
                cc
        List.fold plip_interconnect [] edge_centric_bindings

    let keyname = filename_sanitize ['_'; '.'] mod_name
    let title = keyname
    let lst = port_use_directive :: nodes @ arcs
    let filename = keyname + "_" + msg + ".dot"
    let fd = yout_open_log filename // This will be in the relevant subfolder of obj
    yout fd (report_banner_toStr "// ")
    let title = sprintf "CBG BP %s %s" mod_name msg
    dotout_plus fd (Some title) (DOT_DIGRAPH("profiler_cf_" + keyname, lst))
    yout_close fd


let alpha_get_schemaN0_o msg = function
    | BPA_alpha_bus(BN_note(key1, key2), terminal_, schemaN0)                  -> Some schemaN0
    | BPA_fu_contact(BN_note(key1, key2), terminal, formal_d22)                -> Some(get_schemaN0 formal_d22)
    | BPA_fu_terminal(_, terminal, lanes_, schemaN0_o)                         -> schemaN0_o
    | other -> sf(sprintf "%s alpha_get_schemaN0 other form %A" msg other)

let alpha_get_schemaN0 msg arg =
   let ans = alpha_get_schemaN0_o msg arg
   valOf_or_fail msg ans


let alpha_get_terminal msg = function
    | BPA_alpha_bus(BN_note(key1, key2), terminal, _) -> terminal 
    | BPA_fu_contact(BN_note(key1, key2), terminal, formal_d22_) -> terminal
    | BPA_fu_terminal(BN_note(key1, key2), terminal, lanes_, schema00_) -> terminal // schema00 refined for lanes already?  TODO define the convention.
    | BPA_fu_terminal(BN_bound_formal(arcid, idx, ss, ty), terminal, _, _) -> terminal
    | other -> sf(sprintf "%s alpha_get_terminal other form %A" msg other)


let get_src_dest_terminal msg destf = function
    | BPARC_src_dest(key, src_alpha, dest_alpha) ->
        let src_term = alpha_get_terminal  msg src_alpha
        let dest_term = alpha_get_terminal  msg dest_alpha
        //let src_schema00 = alpha_get_schemaN0 msg src_alpha
        //let src_arcid = alpha_edge_key msg src_alpha
        //let dest_arcid = alpha_edge_key msg dest_alpha
        if destf then dest_term else src_term
    | other -> sf(sprintf "%s get_src_dest_terminal other form %A" msg other)




// For plotting only
let gec_all_arcs_with_terminals_and_edge_keys ww mod_name msg all_arcs =
    let ww = WF 2 "BP compile" ww (sprintf "tidy for dot plotting %s %s %i" mod_name msg (length all_arcs))
    let alpha_edge_key mod_name = function
        | BPA_alpha_bus(BN_note(key1, key2),  _, sch00) -> (key1, key2)
        | BPA_fu_contact(BN_note(key1, key2), terminal_, formal_d22_) -> (key1, key2)
        | BPA_fu_terminal(BN_note(key1, key2), terminal_, lanes_, schema00_) -> (key1, key2)
        | BPA_fu_terminal(BN_bound_formal(arcid, idx, ss, ty), terminal_, _, _) -> ("BF", arcid)                
        | other -> sf(sprintf "%s alpha_edge_key other form %A" mod_name other)

    let m_ghosts = ref [] // Create lightweight/temporary ghost nodes from IR tree for visualisaion.
    let rec arc_tidy101 cc arc = 
        let freshen = function // arcids are needed for collating perhaps. Does no harm.
           | BN_note(key1, key2) -> BN_note(key1, funique key2)
           | _ -> sf "L1071 freshen"
        //dev_println (sprintf "arc_tidy101: Arc is %A" arc)
        match arc with
            | BPARC_src_dest(key, BPA_combinate(op, note, srcs, _), dest_alpha)::tt ->
                let iname = funique "ghost"  + key
                let nw1 = BPARC_src_dest("ghost", BPA_fu_terminal(freshen note, (iname, "aout", "aout"), -1, None), dest_alpha)
                let kind = combinatorToStr op
                mutadd m_ghosts (iname, C_ghost(iname, kind))
                let new_work =
                    let ri src = BPARC_src_dest("ghostlink", src, BPA_fu_terminal(freshen note, (iname, "out_nn", "aout"), -1, None))
                    map ri srcs
                arc_tidy101 (cc) (nw1 :: new_work @ tt)


            | BPARC_src_dest(key, src_alpha, BPA_combinate(op, note, dests, _))::tt ->
                muddy "bar"

            | BPARC_src_dest(key, BPA_dead, dest_alpha)::tt -> 
                vprintln 3 (sprintf "arc_tidy101: dead arc should never have been stored. key=%s" key)
                arc_tidy101 cc tt

            | BPARC_src_dest(key, _, BPA_dead)::tt -> 
                sf (sprintf "arc_tidy101: dead arc dest seems unreasonable. key=%s" key) 
                arc_tidy101 cc tt

            | BPARC_src_dest(key, src_alpha, dest_alpha)::tt ->
                let src_term   = alpha_get_terminal  mod_name src_alpha
                let dest_term  = alpha_get_terminal  mod_name dest_alpha
                let src_arcid  = alpha_edge_key mod_name src_alpha
                let dest_arcid = alpha_edge_key mod_name dest_alpha // Use origin/src binding key as primary key for the edge and hence dest_arcid is likely never needed    
                let schstr = match alpha_get_schemaN0_o "prenorm-arc-msg" dest_alpha with
                                    | None -> ""
                                    | Some (_, schema00) -> schema00ToStr true schema00
                vprintln 3 (sprintf "arc %s -> %s      [%s]" (alphaToStr src_alpha) (alphaToStr dest_alpha)  schstr)
                let na = ((key, src_term, src_arcid), (key, dest_term, dest_arcid), arc)
                arc_tidy101 (na::cc) tt
            | other::tt -> sf(sprintf "%s arc_tidy101 other form %A" mod_name other)
            | [] -> cc

    let plotting_arcs = arc_tidy101 [] all_arcs
    let ghost_instances = !m_ghosts
    let ww = WF 2 "BP compile" ww (sprintf "tidy for dot plotting %s %s. Needed  %i ghost instances." mod_name msg (length ghost_instances))
    (plotting_arcs, ghost_instances)




//
//
//
let add_callsite_managers ww settings m_tail_fus mod_name nbm all_arcs =

    let ww = WF 2 "BP compile" ww (sprintf "determining callsite managers needed for module %s" mod_name)

    let collated_on_dest_terminal = // Collate on dest terminal identifies bus fights likely arising from a lack of callsite managers.
        generic_collate (get_src_dest_terminal mod_name true) all_arcs

    let dest_measured = map (fun (x, lst) -> (x, length lst, lst)) collated_on_dest_terminal
    reportx 3 "muliply-driven nodes" (fun (x, n_drivers, lst) -> sprintf "%s %A has %i sources" (if n_drivers>1 then "*" else " ") x n_drivers) dest_measured

    // Collate on src terminal: identifies where either a replicator or return-side of a callsite manager is needed.
    let collated_on_src_terminal__ = 
        let cost = generic_collate (get_src_dest_terminal mod_name false) all_arcs

        let cost_pr (term, lst) =
            let n_srcs = length lst
            sprintf "replicator check: %s %A sources %i dests" (if n_srcs>1 then "*" else " ") term n_srcs
        reportx 3 "first pass multiply-sourcing nodes (replicator and returnsite candidates)" cost_pr cost
        cost

    let returnsite_directory =
        let rsd_entry cc arc =
            match arc with
                | BPARC_src_dest(key, src_alpha, dest_alpha) ->
                    match src_alpha with 
                    | BPA_fu_contact(BN_note("returnsite", callsite_id), terminal, d22_) -> (callsite_id, (false, arc))::cc
                    | BPA_fu_terminal(BN_note("returnsite", callsite_id), terminal, lanes, shemea) -> (callsite_id, (false, arc))::cc

                    | BPA_fu_terminal _
                    | BPA_fu_contact _
                    | BPA_fu_terminal _
                    | BPA_alpha_bus _ -> cc
                    | other ->
                        sf (sprintf "rsd_entry other form. key=%s\nsrc=%A\ndest=%A" key src_alpha dest_alpha)
                | other ->
                    sf (sprintf "get_callsite: other arc form. %A" arc)
        let rsd = List.fold rsd_entry [] all_arcs
        vprintln 3 (sprintf "Found %i return site directory entries." (length rsd))
        rsd

    let find_corresponding_return_arc callsite_id =
        match op_assoc callsite_id returnsite_directory with // There is exactly one return site for a given call.
            | Some (flag_, arc) -> arc
            | None -> sf (sprintf "Cannot find return site for %s" callsite_id)
                          
            
    let adjustments =
        let adjust_multiply_driven cc (dest_terminal, n_drivers, lst) =
            if n_drivers >= 2 then
                //dev_println(sprintf "Need to adjust (n_drivers=%i) %A" n_drivers lst) // We expect each driver to relate to a different callsite
                let get_callsite cc arc =
                    match arc with
                    | BPARC_src_dest(key, src_alpha, dest_alpha) ->
                        match dest_alpha with 
                        | BPA_fu_contact(BN_note("callsite", callsite_id), _, _) 
                        | BPA_fu_terminal(BN_note("callsite", callsite_id), _, _, _) ->
                            let return_arc = find_corresponding_return_arc callsite_id
                            (callsite_id, arc, return_arc)::cc


                        | BPA_fu_contact _
                        | BPA_alpha_bus _ -> cc

                        | other ->
                            sf (sprintf "get_callsite: other callsite form. key=%s\nsrc=%A\ndest=%A" key src_alpha dest_alpha)
                            cc
                    | other ->
                        sf (sprintf "get_callsite: other arc %A" other)
                        cc
                (dest_terminal, List.fold get_callsite [] lst)::cc
            else cc

        List.fold adjust_multiply_driven [] dest_measured

    //let collated_adjustments = generic_collate fst adjustments // Collate by callsite identifier.
    let callsite_adjustments = adjustments // for now - some will be replicators
    vprintln 2 (sprintf "add_callsite_managers: %i ops found across %i sites requiring management." (length adjustments) (length callsite_adjustments))

    let rez_callsite_manager (dest_terminal, members) = // This can cope with more than 2 ports, but perhaps make only 2 ports at a time to start with.
        let instance_name = funique "csm"
        let arity = length members
        let msg = sprintf "BP rez_callsite_manager %s for %s arity=%i" instance_name (bp_terminalToStr dest_terminal) arity
        let ww = WF 3 msg ww "start"

        // Get terminal names for the shared FU method.  Somewhat longwinded, but perhaps more flexible going forward?
        let (callee_put_dx, callee_aout_dx) = // Each dx is a  '(string * string * string) * (ipblk_protocol_t * netport_pattern_schema_t list)'
            let uf_terminal returnf (callsite_id_, call_arc, return_arc) =
                let arc = if returnf then return_arc else call_arc
                match arc with
                    | BPARC_src_dest(key, src_alpha, dest_alpha) ->
                        let terminal = alpha_get_terminal msg (if not returnf then dest_alpha else src_alpha)
                        let schemaN0 = alpha_get_schemaN0 msg (if not returnf then dest_alpha else src_alpha)
                        let note = BN_note("callsite-manager", if returnf then "return-port" else "call-port")
                        //let dir = (if returnf then Aout else Ain) // Never used so far anyway.
                        let alpha = BPA_fu_terminal(note, terminal, -1, Some schemaN0)
                        (alpha, schemaN0)
                    | site -> sf(sprintf "uf_terminal returnf=%A other %A" returnf site)

            // Should also check one spec?

            let check_one_value terminals = // As a sanity check there should be two or more copies of the same terminal. Lanes may differ?
                //dev_println (sprintf "%s: check_one_value terminal arg %A" instance_name terminals)
                if length terminals<>arity || arity<2 then sf(sprintf "%s: arity mismatch encountered on %A" instance_name terminals)
                let example = hd terminals
                let check term = (if term <> example then sf (sprintf "%s: Terminal disagreement %A" instance_name terminals))
                app check (tl terminals)
                example
                
            (check_one_value (map (uf_terminal false) members), check_one_value (map (uf_terminal true) members))
//+++ devx: csm10: check_one_value terminal arg [("savoir", "cmd", "aout"); ("savoir", "cmd", "aout")]
//+++ devx: csm10: check_one_value terminal arg [("savoir", "cmd", "put"); ("savoir", "cmd", "put")]

        let (put_spec, aout_spec) =
            let topair line = (line.signed=Signed, atoi32 line.width)
            (map topair (drop_hs msg (snd(snd callee_put_dx))), map topair (drop_hs msg (snd (snd callee_aout_dx)))) 
            
        dev_println (sprintf "csm %s  put_spec  %A"   instance_name   put_spec)
        dev_println (sprintf "csm %s  aout_spec %A"   instance_name   aout_spec)

        let (kind_name, vmd, structured_ports, overrides_) = bp_create_kind_canned_callsite_manager ww settings.canned_fu_settings msg put_spec aout_spec arity


        let (manager_to_fu_port, f_scname_)  =
            let (method_name, side) = ("to_fu", "aout") // ("from_fu", "ain")
            match op_assoc (method_name, side) structured_ports with
                | None ->
                    vprintln 0 (sprintf "Available ports/methods are " + sfold (fun ((name, side), _) -> name + "/" + side) structured_ports)
                    cleanexit(sprintf "Need method '%s/%s' calling schema from instance '%s'" method_name side instance_name)
                | Some d22 ->
                    let scname = get_schema_name method_name d22
                    let terminal = (instance_name, method_name, side)
                   //(BPA_fu_contact(binding_note, terminal, d22), scname)  // deprecate? lookup again later...
                    let binding_note = BN_note("callsite-manager", "to_the_fu")
                    (BPA_fu_terminal(binding_note, terminal, -1, Some(get_schemaN0 d22)), scname) // (note, terminal_id, lane, full_schema)

        let (manager_from_fu_port, f_scname_)  =
            let (method_name, side) = ("from_fu", "ain")
            match op_assoc (method_name, side) structured_ports with
                | None ->
                    vprintln 0 (sprintf "Available ports/methods are " + sfold (fun ((name, side), _) -> name + "/" + side) structured_ports)
                    cleanexit(sprintf "Need method '%s/%s' calling schema from instance '%s'" method_name side instance_name)
                | Some d22 ->
                    let scname = get_schema_name method_name d22
                    let terminal = (instance_name, method_name, side)
                   //(BPA_fu_contact(binding_note, terminal, d22), scname)  // deprecate? lookup again later...
                    let binding_note = BN_note("callsite-manager", "from_the_fu")
                    (BPA_fu_terminal(binding_note, terminal, -1, Some(get_schemaN0 d22)), scname)


        let manager_put_port idx =
            let terminal = (instance_name, sprintf "site_%i" idx, "put")
            let binding_note = BN_note("callsite-manager", "site-put")
            BPA_fu_terminal(binding_note, terminal, -1, Some(snd callee_put_dx)) 

        let manager_aout_port idx =
            let binding_note = BN_note("callsite-manager", "site-aout")
            let terminal = (instance_name, sprintf "site_%i" idx, "aout")
            BPA_fu_terminal(binding_note, terminal, -1, Some(snd callee_aout_dx)) 


        let src_dest_of destf arc =
            match arc with
                | BPARC_src_dest(key, src_alpha, dest_alpha) ->
                    if destf then dest_alpha else src_alpha
                | other -> sf (sprintf "src_dest_of other form %A" other)

        let (old_arcs, new_arcs) =
            let render_edit_instruction (old_arcs, new_arcs) ((callsite_id, call_arc, return_arc), idx) =

                let new_call =
                    let key = funique "managed"
                    BPARC_src_dest(key, src_dest_of false call_arc, manager_put_port idx) 

                let new_return =
                    let key = funique "managed"
                    BPARC_src_dest(key, manager_aout_port idx, src_dest_of true return_arc) 

                (call_arc::return_arc::old_arcs, new_call::new_return::new_arcs)
            let key = "new-fu-link"
            let new_fu_call = BPARC_src_dest(key, manager_to_fu_port, fst callee_put_dx)
            let new_fu_return = BPARC_src_dest(key, fst callee_aout_dx, manager_from_fu_port)
            List.fold render_edit_instruction ([], [new_fu_call; new_fu_return]) (zipWithIndex members)




        let iinfo_s =
                    { g_null_vm2_iinfo with
                        definitionf=          false // This is an instance, so false here.
                        iname=                instance_name
                        generated_by=         g_blueparrot_banner
                        kind_vlnv=            (hpr_minfo vmd).name
                        externally_provided=  false // TBD - likey canned really.
                      }
        let iinfo = { iinfo_s with iname=instance_name; kind_vlnv=(hpr_minfo vmd).name }

        let port_labels = [] // for now    
        let the_component0 = (iinfo, Some vmd)
        let the_component1 = C_ipinst(instance_name, (iinfo, Some vmd))    
        let atts = { g_default_bp_component_atts with femeral=true } 
        mutadd m_tail_fus (instance_name, the_component1)
        (old_arcs, new_arcs) 



    let edits = map rez_callsite_manager callsite_adjustments

    let (old_arcs, new_arcs) = List.unzip edits
    
    (lst_subtract all_arcs (list_flatten old_arcs)) @ (list_flatten new_arcs) // end of add calsite managers

                
//
//       
// 
let instantiate_split_join_rvz ww settings mod_name m_new_fus all_arcs =
    let ww = WF 2 "BP instantiate_split_join_rvz_units" ww "start"
    let rez_split_serf arc =
        match arc with // unlike rendezvous/phi, we work on the whole arc here. Similar for replicators no doubt.
            | BPARC_src_dest(key, guardian_alpha, BPA_combinate(BPC_split, bn, [w0; w1], schemaN0_o)) -> // Hardcode for arity=2 for now
                let instance_name_split = iname_hint bn // Use this name directly since terminal references are pre-named that way.  funiqu_chars 15 (iname_hint bn + "_split")
                let msg = "rez splitter " + instance_name_split
                let atts = { g_default_bp_component_atts with femeral=true } 
                let schemaN0 = valOf_or_fail msg schemaN0_o
                let data_fields = bp_schema00_mine_prec msg -1 schemaN0
                let (kind_name_s, vmd_s, structured_ports_s, overrides_) = bp_create_kind_canned_split_phi ww settings.canned_fu_settings msg "SPLIT" data_fields
                vprintln 3 (sprintf "Instantiate splitter iname=%s    kind=%s" instance_name_split kind_name_s)
                // Bind guard expression egress alpha (aka guardian) to forker (aka splitter).
                let na0 =
                    let (port_name, side) = ("operate", "put")
                    let (schema, alpha_point) =
                        match op_assoc (port_name, side) structured_ports_s with
                            | None ->
                                vprintln 0 (sprintf "Available ports/methods are " + sfold (fun ((name, side), _) -> name + "/" + side) structured_ports_s)
                                cleanexit(sprintf "If/then needs method '%s' '%s' call schema from %A" port_name side instance_name_split)
                            | Some ((ipb_protocol, ss_, bool_, schema), nets) ->                 // was very longwinded - we do not need to get find the concrete d22 and then abstract it again. This at least serves as some sort of sanity check.
                                let binding_note = BN_note(instance_name_split, "*notinuse*1252")
                                let terminal = (instance_name_split, port_name, side)
                                let alpha_fork_point = BPA_fu_terminal(binding_note, terminal, -1, Some schemaN0)
                                (schema, alpha_fork_point)
                    fclose_arc ww port_name (lp_msg "close_ifthen_splitter_guardian_input") guardian_alpha alpha_point


                let rezandbind cc (w_whereto, hand_idx) =
                    let (port_name, side) = (sprintf "operate_%i" hand_idx, "aout")
                    let (schema_, a0) =
                        match op_assoc (port_name, side) structured_ports_s with
                            | None ->
                                vprintln 0 (sprintf "Available ports/methods are " + sfold (fun ((name, side), _) -> name + "/" + side) structured_ports_s)
                                cleanexit(sprintf "If/then needs contact '%s/%s' from %A" port_name side instance_name_split)
                            | Some ((ipb_protocol, ss_, bool_, schema), nets) ->
                                let d22 = ((ipb_protocol, ss_, bool_, schema), nets)
                                let schema = (ipb_protocol, schema)
                                let terminal = (instance_name_split, port_name, side)
                                let binding_note = BN_note(funique "edgekey", "*notinuse*673")
                                //let alpha_forked_point = BPA_fu_contact(binding_note, terminal, d22) // could do this equally since we have the bus now ...
                                let alpha_forked_point = BPA_fu_terminal(binding_note, terminal, -1, Some(get_schemaN0 d22))
                                (schema, alpha_forked_point)
                    let na = fclose_arc ww port_name (lp_msg "close_ifthen_splitter_output") a0 w_whereto
                    na::cc
                let new_arcs = List.fold rezandbind [na0] [ (w0, 0); (w1, 1) ]


                let iinfo_s =
                    { g_null_vm2_iinfo with
                        definitionf=          false // This is an instance, so false here.
                        iname=                instance_name_split
                        generated_by=         g_blueparrot_banner
                        kind_vlnv=            (hpr_minfo vmd_s).name
                        externally_provided=  false // TBD - likey canned really.
                      }
                let the_component0_s = (iinfo_s, Some vmd_s)
                let the_component1_s = C_ipinst(instance_name_split, (iinfo_s, Some vmd_s))    
                mutadd m_new_fus (instance_name_split, the_component1_s)
                new_arcs 
            | other -> sf(sprintf "specialist split rez other form %A" other)

    let rez_joinphi_serf = function // Make new arcs to wire up its inputs and return its output terminal. Rendezvous is similar in that sense.
        | BPA_combinate(BPC_joinphi, bn, [aw0; aw1], schemaN0_) -> // Hardcode for arity=2 for now
            let instance_name_join = iname_hint bn //  funiqu_chars 15 (iname_hint bn + "_join")
            let msg = "rez joiner " + instance_name_join
            let (kind_names_, vmd_j, structured_ports_j, overrides_) = bp_create_kind_canned_split_phi ww settings.canned_fu_settings msg "UNIT_PHI" [] // Joins _will_ be unit until we get ternary operator in use.
            vprintln 3 (sprintf "Instantiate joinphi %s" instance_name_join)
                // Bind the resulting activations from the two hands/branches to the phi/join inputs.
            let bind_join_input cc (arg_activation, input_idx) =
                //if vbf then vprintln 3 (sprintf "Binding 'if' hand %s input %i" instance_name input_idx)
                //let (note, alpha_d22) = bp_get_d22 ww msg "phi/join bind input" arg_activation
                let formal_contact =
                    let (port, side) = (sprintf "operate_%i" input_idx, "put")
                    match op_assoc (port, side) structured_ports_j with
                        | None ->
                            vprintln 0 (sprintf "Available ports/methods for join input are " + sfold (fun ((name, side), _) -> name + "/" + side) structured_ports_j)
                            hpr_yikes(sprintf "Cannot find '%s/%s' on %s" port side instance_name_join)
                            BPA_dead // for now
                        | Some ((ipb_protocol, ss_, bool_, schema), nets) ->
                            let d22 = ((ipb_protocol, ss_, bool_, schema), nets)
                            let binding_note = BN_note(instance_name_join, "*notinuse*1295")
                            let terminal = (instance_name_join, port, side)
                            BPA_fu_terminal(binding_note, terminal, -1, Some(get_schemaN0 d22)) // (note, direction, terminal_id, lane,  full_schema)
                let na = fclose_arc ww instance_name_join "close_ifthen_join_arg" arg_activation formal_contact // close_arc instance bind: src (activation from basic block output) then dest (input to phi).
                na :: cc

            let new_arcs = List.fold bind_join_input [] [ (aw0, 0); (aw1, 1) ]

            // Bind a return bus from the phi/join
            let (port_name, side) = ("operate", "aout")
            let terminal = (instance_name_join, port_name, side)
            let (return_schema, alpha_return_contact) =
                match op_assoc (port_name, side) structured_ports_j with
                    | None ->
                        vprintln 0 (sprintf "Available ports/methods are " + sfold (fun ((name, side), _) -> name + "/" + side) structured_ports_j)
                        cleanexit(sprintf "If/then needs contact '%s/%s' return schema from %A" port_name side instance_name_join)
                    | Some ((ipb_protocol, ss_, bool_, schema), nets) ->
                        let d22 = ((ipb_protocol, ss_, bool_, schema), nets)
                        let return_schema = (ipb_protocol, schema)
                        let terminal = (instance_name_join, port_name, side)
                        let alpha_return_contact =
                            let binding_note = BN_note(funique "edgekey", "*notinuse*699")
                            BPA_fu_terminal(binding_note, terminal, -1, Some(get_schemaN0 d22)) // (note, terminal_id, lane, schema)
                        (return_schema, alpha_return_contact)

            let iinfo_j =
                    { g_null_vm2_iinfo with
                        definitionf=          false // This is an instance, so false here.
                        iname=                instance_name_join; 
                        generated_by=         g_blueparrot_banner
                        kind_vlnv=            (hpr_minfo vmd_j).name
                        externally_provided=  false // TBD - likey canned really.
                      }

            let the_component0_j = (iinfo_j, Some vmd_j)
            let the_component1_j = C_ipinst(instance_name_join, (iinfo_j, Some vmd_j))
            mutadd m_new_fus (instance_name_join, the_component1_j)
            (alpha_return_contact, new_arcs)

        | other -> sf(sprintf "specialist joinphi rez other form %A" other)


    let rez_rvx = function // Make new arcs to wire up its inputs and return its output terminal. Rendezvous is similar in that sense.
        | BPA_combinate(BPC_rendezvous rzstruct, BN_note(site_name, unused_), several, result_schemaN0_) -> // Final result schema only, not binarised intermediatates.
            let m_rvz_new_arcs = ref []//Fold these probably. Be functional!
            let args_prec = map snd rzstruct
            let pairs = List.zip several args_prec
            let rez_rvz_serf msg1 arg_pairs =
                let (args, args_prec) = List.unzip arg_pairs
                let instance_name = funiqu_chars 15 site_name
                vprintln 3 (sprintf "Instantiate rendezvous %s" instance_name)
                let msg = "instantiate rendezvous site " + instance_name
                let (kind_name, vmd, structured_ports, overrides_) = bp_create_kind_canned_rendezvous ww settings.canned_fu_settings msg args_prec

                let iinfo =
                        { g_null_vm2_iinfo with
                            definitionf=          false // This is an instance, so false here.
                            iname=                instance_name
                            generated_by=         g_blueparrot_banner
                            kind_vlnv=            (hpr_minfo vmd).name
                            externally_provided=  false // TBD - likey yes?
                          }
                let port_labels = [] // for now                            
                let the_component1 = C_ipinst(instance_name, (iinfo, Some vmd))  
                let atts = { g_default_bp_component_atts with femeral=true } 
                mutadd m_new_fus (instance_name, the_component1)

                let _ =  // Bind input arguments to new rendezvous FU
                    let bind_input = function
                        | (arg_activation, input_idx) ->
                            let vbf = false
                            if vbf then vprintln 4 (sprintf "Binding rendezvous %s input %i" instance_name input_idx)
                            let formal_contact =
                                let (port, side) = (sprintf "arg_%i" input_idx, "put")
                                match op_assoc (port, side) structured_ports with
                                    | None ->
                                        vprintln 0 (sprintf "Available ports/methods are " + sfold (fun ((name, side), _) -> name + "/" + side) structured_ports)
                                        hpr_yikes(sprintf "Cannot find '%s/%s' on rendezvous %s" port side instance_name)
                                        BPA_dead // for now
                                    | Some ((ipb_protocol, ss_, bool_, schema), nets) ->
                                        let d22 = ((ipb_protocol, ss_, bool_, schema), nets)
                                        let binding_note = BN_note(instance_name, "*notinuse*509")
                                        let terminal = (instance_name, port, side)
                                        BPA_fu_terminal(binding_note, terminal, -1, Some(get_schemaN0 d22)) // (note, terminal_id, lane,  full_schema)
                                        //BPA_fu_contact(binding_note, terminal, d22) 
                            let na = fclose_arc ww instance_name "close_rendezvous_arg" arg_activation formal_contact // close_arc instance bind: src then dest
                            mutadd m_rvz_new_arcs na
                        | _ -> sf "L376 unactivated?"
                    app bind_input (zipWithIndex args)

                // Return the aout terminal from the rendezvous
                let (method_name, side) = ("alltogether", "aout")
                let (alpha_return_contact, schemaN0_) =
                    match op_assoc (method_name, side) structured_ports with
                        | None -> cleanexit(sprintf "Rendezvous needs method '%s' '%s' return schema from %A" method_name side instance_name)
                        | Some ((ipb_protocol, ss_, bool_, schema), nets) ->
                            let d22 = ((ipb_protocol, ss_, bool_, schema), nets)
                            let return_schema = (ipb_protocol, schema)
                            let alpha_return_contact =
                                let binding_note = BN_note(instance_name, "*notinuse*162")
                                let terminal = (instance_name, method_name, side)
                                // BPA_fu_terminal(binding_note, terminal, -1, get_schemaN0 d22)
                                BPA_fu_contact(binding_note, terminal, d22) // Can use this form now rez is deferred.
                            (alpha_return_contact, get_schemaN0 d22)
                (alpha_return_contact, list_flatten args_prec) 

            let rec gen_rvz_tree = function
                | [singleton] -> singleton
                | a0::a1::tt when settings.use_diadic_only ->  // Create tree of binary rendezvous with arbitrary association (no need to balance it for now)
                   let binarypaired = rez_rvz_serf "pair tree" [a0; a1]
                   gen_rvz_tree (binarypaired::tt)
                | several -> rez_rvz_serf "unrestricted several" several
            let (alpha, _) = gen_rvz_tree pairs
            (alpha, !m_rvz_new_arcs)
            
        | other ->
            sf(sprintf "rez_rvx form L1275 %A" other)        

    
    let rec rez_sjr_maid cc = function // Build any arcs containing algebraic sub-trees into an equivalent set of flattened arcs and FUs.
        | [] -> cc
        | hd_arc::tt ->
            match hd_arc with
            | BPARC_src_dest(key, guardian_alpha, BPA_combinate(BPC_split, bn, [w0; w1], schemaN0_o)) -> // Hardcode for arity=2 for now
               let new_arcs = rez_split_serf hd_arc 
               rez_sjr_maid cc (new_arcs @ tt) //

            | BPARC_src_dest(key, (BPA_combinate(BPC_joinphi, bn, lst_, schemaN0_) as src_alpha), dest_alpha) -> 
                let (output_terminal, new_arcs) = rez_joinphi_serf src_alpha
                let na = BPARC_src_dest(key, output_terminal, dest_alpha)
                rez_sjr_maid cc (na::new_arcs @ tt) //


            | BPARC_src_dest(key, (BPA_combinate(BPC_rendezvous rzstruct, _, _, _) as src_alpha), dest_alpha) ->  
                let (output_terminal, new_arcs) = rez_rvx src_alpha
                let na = BPARC_src_dest(key, output_terminal, dest_alpha)
                rez_sjr_maid cc (na :: new_arcs @ tt) //

            | other ->
                //dev_println(sprintf "%s rez_split_join other form %A" mod_name other) // Pass on since nothing detected that needs attention.
                rez_sjr_maid (other::cc) tt //


    let all_arcs = rez_sjr_maid [] all_arcs

                

    let ww = WF 2 "BP instantiate_split_join_rvz_units" ww "finished"
    all_arcs




let bp_compile ww settings = function
    | BP_module(mod_name, decls) ->
        let ww = WF 2 "BP compile" ww (sprintf "start compile module %s" mod_name)


        let check_already_defined instance_name cc = 
           match op_assoc instance_name cc with
               | Some existing_ -> local_cleanexit ww settings (lp_msg mod_name + sprintf ": method or instance name %s redeclared" instance_name)
               | None -> ()

        let genv = [] // generic parameter environment - null for now
        let nbm = new nbm_t(ww, mod_name, false, genv)


        let rec eval_actuals instance_name env = function
            | ("Bracketed", index_size) ->
                bp_eval_unactivated ww settings instance_name nbm env index_size
            | other -> sf(lp_msg instance_name + sprintf ": other form instance actual %A" other)


        let compile_instance ww (cc, env) = function
            | BP_memory_instance_sram(lp, dwidth, instance_name, actuals) ->
                note_lp lp
                check_already_defined instance_name cc
                let msg = lp_msg (sprintf "instantiate SSRAM iname=%s" instance_name)
                let actuals = map (eval_actuals instance_name env) actuals
                let locations =
                    match actuals with // bp_eval_const TODO
                        | [BE_biginteger locations] -> locations
                        | _ -> sf (lp_msg instance_name + ": Too many/few constructor arguments provided")
                let addr_width = bound_log2 locations
                let (signedf, word_width) = bp_bit_width settings instance_name dwidth
                let latency = 1  // TODO: Get from high-level src "ssram" or "ssram2" etc
                let no_ports = 1 // Hardwired in create_kind for now... TODO also get from high-level src statement/pragma 
                //let props = []
                //let netst = if signedf then Signed else Unsigned
                
                let (kind_name, vmd, structured_ports, overrides_) = bp_create_kind_cvip_SSRAM ww settings.canned_fu_settings msg (latency, int64 locations, (signedf, word_width))

                // This is the untokenised core ...
                //let (kind_vlnv, portmeta_h, actuals_rides, the_component, net_schema_formal, block_meld) = cvipgen.gec_cv_romram_ip_block ww settings.stagename kkey descr no_ports portmeta_s netst (*constval*)[] (*rom_id_o*)None latency props // This will be fully-pipelined with no handshakes.

                let iinfo =
                        { g_null_vm2_iinfo with
                            definitionf=          false // This is an instance, so false here.
                            iname=                instance_name
                            generated_by=         g_blueparrot_banner
                            kind_vlnv=            (hpr_minfo vmd).name
                            externally_provided=  false // TBD - likey yes?
                          }


                let the_component = (iinfo, Some vmd) 
                let instance = C_ipinst(instance_name, the_component)
                let atts = { g_default_bp_component_atts with autoapply_methodname=Some "read" }
                let nb = (instance_name, BE_component(instance_name, atts, the_component, structured_ports))
                ((instance_name, instance)::cc, nb::env)

            | BP_register_instance(lp, dwidth, instance_name, actuals) ->
                note_lp lp
                check_already_defined instance_name cc
                //let defn = lookup_defn ww settings instance_name "register instance"
                let (signedf, bit_width) = bp_bit_width settings instance_name dwidth
                let msg = "for instance " + instance_name
                let (kind_name, vmd, structured_ports, overrides_) = bp_create_kind_canned_register ww settings.canned_fu_settings msg (signedf, bit_width)
                let iinfo =
                    { g_null_vm2_iinfo with
                        definitionf=          false // This is an instance, so false here.
                        iname=                instance_name
                        generated_by=         g_blueparrot_banner
                        kind_vlnv=            (hpr_minfo vmd).name
                        externally_provided=  false // TBD - likey yes?
                      }

                let the_component = (iinfo, Some vmd)
                let port_labels = [] // for now
                let instance = C_ipinst(instance_name, the_component)
                let atts = { g_default_bp_component_atts with autoapply_methodname=Some "read" }
                let nb = (instance_name, BE_component(instance_name, atts, the_component, structured_ports))
                ((instance_name, instance)::cc, nb::env)

            | BP_instance(lp, kind, instance_name, actuals) -> // generic IP block/FU instance.
                note_lp lp
                check_already_defined instance_name cc
                let defn = lookup_defn ww settings instance_name kind
                // ...
                let atts = g_default_bp_component_atts
                //let nbe = (instance_name, BE_component(instance_name, atts, the_component, structured_ports))
                let instance = C_filler instance_name // for now...
                ((instance_name, instance)::cc, (* nbe:: *)env)

            | other -> (cc, env)

        let compile_methods ww (cc, cd, env) = function
            | BP_memory_instance_sram _
            | BP_register_instance _
            | BP_instance _ -> (cc, cd, env)
            | BP_method_export(lp, rwidth, method_name, formals, body) ->
                note_lp lp
                let suppress_void_result_busses = false  // It is never (?) good to set this since we still need to convey the activation. 
                let initial_eis               = false // Parse this from a method pragma please.
                check_already_defined method_name cc
                // We shall hard-code put/get here, with no port sharing, but will support other flavours in the future using pragams etc.. TODO.


                let put_arcid = funique "exportedformal" // Delete me and use terminal ids in future

                let (alpha0, put_arg, env) = // This is where the argument comes in to an exported method.
                    //let total_arg_bit_width = List.fold (fun c (ty, formal_name, spare_)-> c+(snd (bp_bit_width settings method_name ty))) 0 formals
                    let additional_portmeta = []
                    let side = "put"
                    let arg_port_name = method_name + "_put"
                    let put_arg_schema:bp_schemaN0_t =
                        let add_sub_bus(ty, formal_name, spare_) = (formal_name, snd (bp_bit_width settings method_name ty))
                        bp_gec_standard_synchronous_netport_schema "i" (map add_sub_bus formals)

                    let d22:d22_t = nbm.busrez2 ww additional_portmeta (Some false) put_arg_schema arg_port_name // can rez formals now, but all others deferred rez ...

                    let terminal_id = (g_self_instance_name, method_name, side) // This is before port sharing.
                    let note = BN_note(put_arcid, method_name)
                    let alpha0 = BPA_fu_terminal(note, terminal_id, -1, Some put_arg_schema) // This pretends we have not rezzed it. FIX


                    let env =
                        let count = length formals
                        let formals = map (fun ((ty, formal_name, spare_), idx) -> (idx, ty, formal_name, bp_bit_width settings method_name ty)) (zipWithIndex formals)
                        // When there is one subfield to an exported method put (one formal), the external activation can be put in the env monolithically. Denoted as idx=-1.
                        // Adding to the env is essentially a let-bind and the number of lookups is later counted so that replicators can be instantiated.
                        // When there are several subfields, these are also essentially replications, although often they may be used in groups or monolithically, but we'll leave that to the optimiser.
                            
                        let add_formal_to_env_old env (idx, ty, formal_name, (signedf, width)) =
                            let subfield_no = if count = 1 then -1 else idx
                            let note = BN_bound_formal(put_arcid, idx, formal_name, (ty, signedf, width))
                            //dev_println (sprintf "Entry of method %s: use subfield_no %i for %s" method_name subfield_no formal_name)

                            let refined_d22 =
                                if count=1 then d22
                                else
                                    let ((protocol_name, pi_name, sex, schema), busnets) = d22 // Temporary munge code before we have proper replicator. rdy will be assigned more than once YUK!
                                    let rec munge pos = function // old munge site
                                        | [] -> [] // Premature?
                                        | h::tt when pos < 2 -> h::(munge (pos+1) tt)
                                        | h::tt ->if pos-2 = idx then [h] else munge (pos+1) tt
                                    let schema1 = munge 0 schema
                                    let busnets1 = munge 0 busnets
                                    let protocol_name1 =
                                        match protocol_name with
                                            | IPB_custom ss -> IPB_custom(ss + sprintf "subfield_%i" idx) // Yuck for now
                                            | other -> sf(sprintf "munge protocol name other %A" other)
                                    ((protocol_name1, pi_name, sex, schema1), busnets1)
                            let nb = muddy "BE_result(Some(BPA_alpha_bus(note, refined_d22))) Not being used?"
                            (formal_name, nb)::env // old code

                        let add_formal_to_env env (idx, ty, formal_name, (signedf, width)) =
                            let subfield_no = if count = 1 then -1 else idx
                            let note = BN_bound_formal(put_arcid, idx, formal_name, (ty, signedf, width))
                            dev_println (sprintf "Entry of method %s: use subfield_no %i for %s" method_name subfield_no formal_name)
                            let nb = BE_bound_activation([], BPA_fu_terminal(note, terminal_id, subfield_no, Some put_arg_schema)) // Whole schema is here, regardless of inevitable lanes. Yes, projection is always on consumer (dest) end.
                            (formal_name, nb)::env

                        List.fold add_formal_to_env env formals

                    
                    (alpha0, (put_arg_schema, arg_port_name, d22), env) // The put arg d22 is our actuals.

                let tprops = { tid=method_name; eis=initial_eis; m_arcs= ref []; m_new_fus= ref []; m_arg_uses=ref [] }
                let alpha_entry = Some(alpha0)
                let return_lab = new_lab method_name "return"
                let (tprops, alpha_o, env_) = bp_compile_cmds ww settings method_name nbm tprops alpha_entry (Some return_lab) env body  // We can do the initial compile of a method without seeing the other methods yet.
                let alpha_return =
                    match alpha_o with
                        | Some alpha ->
                            //dev_println(sprintf "Alpha at end of thread/method %s is %A" method_name alpha)
                            [alpha]
                        | None ->
                            vprintln 3 (sprintf "%s: No activation at end of method and %i body returns." method_name (length !return_lab.incoming))
                            []
                let all_returns = alpha_return @ !return_lab.incoming
                //let exit_points = length all_returns
                vprintln 1 (sprintf "%s: %i activations return from the method." method_name (length all_returns))
                let alpha_return =
                    match all_returns with
                        | [] -> BPA_dead
                        | [item] -> item
                        | several -> BPA_combinate(BPC_joinphi, BN_note(method_name + "_ReturnJoin", "unused-L1873"), several, None)
                let result_side = "aout"
                let (aout_result, env) =
                    let (_, result_bit_width) = bp_bit_width settings method_name rwidth
                    if result_bit_width=0 && suppress_void_result_busses then (None, env)
                    else
                        let result_schema00 =
                            let fields = if result_bit_width = 0 then [] else [("RDATA", result_bit_width)]
                            bp_gec_standard_synchronous_netport_schema "o" fields
                        let additional_portmeta = []
                        let result_port_name = method_name + "_aout"  // We probably don't want this to be rezzed here ...
                        let busrez = nbm.busrez2 ww additional_portmeta (Some false) result_schema00 result_port_name 
                        (Some(result_schema00, result_port_name, busrez), env)

                            
                match aout_result with
                    | None ->
                        vprintln 2 (sprintf "No aout/return result for method %s" method_name)
                       //close_arc ww tprops "join_external_method_result_dead" BPA_dead alpha_return  
                    | Some (result_schema00, result_port_name, busrez_) ->
                        let get_terminal_id = (g_self_instance_name, method_name, "aout")
                        let b2 = BPA_fu_terminal(BN_note(put_arcid, method_name + "_external_method_result"), get_terminal_id, -1, Some result_schema00)
                        close_arc ww tprops method_name ("join_external_method_result:name="+method_name) alpha_return b2 // src then dest arg order. // Make a joint
                        //let b2 = BPA_alpha_bus(BN_note(put_arcid, method_name + "_external_method_result"), f3o3(aout_schema))

                let meth = C_method_elaborated(method_name, put_arcid, aout_result, put_arg, !tprops.m_arcs)

                let exported_method_pseudo_instance = (method_name, meth) // We justify methods, instances and rules all being the same type by the need for these pseudo instances arising in the wiring network.
                let cd = (exported_method_pseudo_instance :: !tprops.m_new_fus) :: cd
                ((method_name, meth)::cc, cd, env)
            | other -> sf(sprintf "%s compile methods other form %A" mod_name other)

        let env000 = []
        let (instances, env) = List.fold (compile_instance ww) ([], env000) decls // explict (non-femeral ones)

        
        let (methods_and_threads, instances2, env) = List.fold (compile_methods ww) (instances, [], env) decls
        
        let meld_ = nbm.fu_end ww // Call this after last formal added.
        //nbm.add_child ww element None element_iname portmeta (element_bindings1 @ element_bindings2)
        
        let ans = IR_nbm(mod_name, nbm) // This is the naive/unoptimised version.  It needs likely to have a temporary name if the optimised one is later to be created as the proper nbm.  We could emit both pre and post versions if you like!

        let ww = WF 2 "BP compile" ww (sprintf "finished compile %s" mod_name)

        let all_arcs = // Collect up all of the unoptimised IR code from the elaborated callable methods and local threads.
            let all_arcs_getter cc (id, content) =
                match content with 
                | C_ipinst _ -> cc // These should probably be on a different list with different type. They are on the thread (femeral ones) so collect non femeral ones that way too?
                | C_method_elaborated(parental_method_name, put_arcid_, get_result_, put_arg_, arcs) -> arcs @ cc
                | other -> sf(sprintf "%s all_arcs_getter other form %A" mod_name other)
            List.fold all_arcs_getter [] methods_and_threads

        let m_tail1_fus = ref []

        let all_arcs = instantiate_split_join_rvz ww settings mod_name m_tail1_fus all_arcs
        //let all_arcs = instantiate_rendezvous_units ww settings m_tail1_fus all_arcs


        //let all_arcs_with_terminals_and_edge_keys101 = gec_all_arcs_with_terminals_and_edge_keys mod_name msg all_arcs

        vprintln 2 (sprintf "%i raw, pre-normalised arcs for %s" (length all_arcs) mod_name)

        let instances = !m_tail1_fus @ instances @ list_flatten instances2 // Combine these preliminary for dot render .. Inventory will subsequently vary.

        // Dotplot is fairly neutral and wants a list of (arcid, terminal) pairs which it can sort out itself.
        // We optionally write out the pre-normalised IR code in dot form.
        if settings.prenormal_dot_report_enabled then
            let (all_arcs_with_terminals_and_edge_keys101, ghost_instances) = gec_all_arcs_with_terminals_and_edge_keys ww mod_name "prenormal" all_arcs
            let dot_arcs = // Split arcs into a list of endpoints (Some might have 3 ends etc for now)
                let shone cc ((key, src_term, src_arcid), (dest_key, dest_term, dest_arcid_), arc) =

                    if src_term = dest_term then sf(sprintf "Encountered reflexive arc with key %s  %A" key (src_term, src_arcid))
                    (src_arcid, src_term) :: (src_arcid, dest_term) :: cc
                List.fold shone [] all_arcs_with_terminals_and_edge_keys101
            let project_arc_id = fst
            let d101 = generic_collate project_arc_id dot_arcs // Collate as needed for dot
            make_dot_report ww settings mod_name "prenormal" (ghost_instances @ instances) methods_and_threads d101

        // Now normalise the code, adding call site managers and replicators, etc..

        let all_arcs =
            let dead_arc_predicate = function
                | BPARC_src_dest(_, BPA_dead, _) -> false
                | BPARC_src_dest(_, _, BPA_dead) -> false                
                | _ -> true
            List.filter dead_arc_predicate all_arcs

        let m_tail2_fus = ref [] //     (string * (bp_currency_t)) list ref

        let all_arcs = add_callsite_managers ww settings m_tail2_fus mod_name nbm all_arcs



        //let all_arcs = add_replicators ww settings m_tail2_fus mod_name nbm all_arcs


            
        // Now optimise our intermediate code that represents the exported method bodies and any local behaviour.




        // Now move on to rendering phase
        let ww = WF 2 "BP compile" ww (sprintf "finshed optimise %s. Start rendering as HPRL/S VM." mod_name)

        let blaze_joint vbf (cbinds, cjoins) key src_binding_note src_d22 dest_binding_note dest_d22 = // weld a joint
            let ((src_protocol, src_bus_name, _, schema_), src_nets) = src_d22 // Schema will indicate which direction of assignment needed. 
            let ((dest_protocol, dest_bus_name, _, schema), dest_nets) = dest_d22
            let src_nets = 
                if length src_nets <> length dest_nets || length schema <> length dest_nets then
                    vprintln 0 (sprintf "Haxard %s combjoin: protocols %A and %A" key src_protocol dest_protocol)
                    vprintln 0 (sprintf "Haxard %s combjoin: dest_binding_note=%s" key (binding_note_to_str dest_binding_note))
                    vprintln 0 (sprintf "Haxard %s combjoin: total busses in port  %i  vs %i  (|schema|=%i"  key (length src_nets) (length dest_nets) (length schema))
                    if length src_nets = 4 && length dest_nets = 3 then
                        let [ s0; s1; _; s3 ] = src_nets
                        dev_println ("As a hack for now, when we join the result of a RAM read with its result bus, we want to discard the penultimate value which is the command out.     ")                           
                        [s0; s1; s3]
                    elif length src_nets = 4 && length dest_nets = 2 then
                        let [ s0; s1; _; _] = src_nets
                        dev_println ("As a hack for now, when we join the result of a RAM write with its expected void result, we discared the unused cmd/aout and RDATA sub-busses.")                           
                        [s0; s1 ]
                    else sf(sprintf "%s: Combinational join of differently-widthed busses fails" key)
                else src_nets
            if vbf then vprintln 3 (sprintf "%s  : join protocols %A and %A" key src_protocol dest_protocol)
            if src_protocol <> dest_protocol then hpr_yikes(sprintf "%s: attempt to simply join different protocols %A and %A" key src_protocol dest_protocol)
            //dev_println (sprintf "%s: q33 wire elaborated food src %A"  key src_alpha)
            //dev_println (sprintf "q33 wire elaborated  dest %s dest %A"  parental_method_name dest_d22)

            let combjoin_net (schema_line, (src_name, src_net), (dest_name, dest_net)) =
                dev_println (sprintf "Combjoin %s %s under %A" src_name dest_name schema_line)
                if schema_line.sdir = "o" then nbm.add_comb(src_net, dest_net)   // Reverse flow (eg READY signals)
                elif schema_line.sdir = "i" then nbm.add_comb(dest_net, src_net) // Normal flow
                else
                    hpr_yikes(sprintf "Cannot combjoin %s %s under %A" src_name dest_name schema_line)
            app combjoin_net (List.zip3 schema src_nets dest_nets)
            let na2 =
                let src_arcid = note2archid key src_binding_note
                let dest_arcid = note2archid key dest_binding_note
                let joint_name = funiqu_chars 15 ("joint:" + dest_arcid)
                let side = "JOINT"
                [ (dest_arcid, (joint_name, src_bus_name, side)); (dest_arcid, (joint_name, dest_bus_name, side)) ] // Could mark the sides as J-IN and J-OUT

            // add to arcs for plotting purposes
            (cbinds, na2@cjoins) // No bind from a welded joint: nbm.add_comb has been invoked just above instead.


        let wire_arc vbf (cbinds, cjoins) = function
            | BPARC_src_dest(key, _, BPA_dead)
            | BPARC_src_dest(key, BPA_dead, _) ->
                vprintln 3 (sprintf "Drop dead binding for method %s  key=%s." "parental_method_name" key)
                (cbinds, cjoins) 

            | BPARC_src_dest(key, alpha0, alpha1) -> // Ambidexterous (mostly, except for bus-to-bus joints)
                let blaze_bind (cbinds, cjoins) the_d22 alpha_new = // new will be a set of formal contacts, d22 is a bus correspondng to alpha_existing to be connected.  Don't really need alpha_existing to be passed in here
                    match alpha_new with
                        | (Let -1 (subfield_no, BPA_alpha_bus(binding_note_, terminal, schemaN0)))
                        | BPA_fu_terminal(binding_note_, terminal, subfield_no, Some schemaN0) ->
                            if subfield_no >= 0 then hpr_yikes(sprintf "blaze_bind %s: was not expecting subfield_no %i" key subfield_no) // Should go away after normalisation.
                            let (instance_name, method_name, side) = terminal

                            let l_actuals = length (snd the_d22)
                            let l_formals = length (snd schemaN0)

                            let rec merge_rename = function
                                | (((old_lname, actual_exp) as actual)::acts, sline::slines) ->
                                    if length acts = length slines then  (sline.lname, actual_exp)::(merge_rename (acts, slines)) // zip from now on
                                    elif old_lname = sline.lname then (sline.lname, actual_exp)::(merge_rename (acts, slines))
                                    elif (encoding_width actual_exp = atoi32 sline.width) then merge_rename (acts, slines) // heuristic match!
                                    elif not_nullp acts && old_lname = "CMD" then merge_rename (acts, sline::slines) // heuristic discard CMD terminal (RAM output)
                                    elif not_nullp acts && old_lname.Contains "DATA" then merge_rename (acts, sline::slines) // heuristic discard DATA terminal (RAM output)

                                    //elif old_lname.Contains "DATA" && sline.lname.Contains "DATA" then  (sline.lname, actual_exp)::(merge_rename (acts, slines))
                                    else
                                        dev_println (sprintf "merge_rename: cannot do actual=%A formal-schema-line=%A" actual sline)
                                        []
                                | _ -> []

                                 
                            let rename (schema, structured_nets) = // Adjust binding to use the formal names actually present (positional order is consistent we here assume). Clearer if this is inlined please.
                                 if l_actuals <> l_formals then
                                     hpr_yikes(sprintf "key=%s Attempt to rename %i actuals for  %i formals on %s port %s/%s" key l_actuals l_formals instance_name method_name side)
                                     dev_println(sprintf "renamex: %s: want to rename actual 1/2 in %s" key (d22_to_str the_d22))
                                     dev_println(sprintf "renamex: %s: want to rename formal 2/2 in %s" key (schema00ToStr true (f4o4 schema)))
                                     dev_println(sprintf "renamex: %s: want to rename formal 3/2 in %s" key (schema00ToStr true (snd schemaN0))) 
                                     let structured_nets =
                                         let (schema, structured_nets) = the_d22
                                         merge_rename (structured_nets, snd schemaN0)
                                     (schema, structured_nets)
                                 else
                                     let renamer ((old_lname, actual_exp), line) = (line.lname, actual_exp)
                                     (schema, map renamer (List.zip structured_nets (snd schemaN0)))
                            let renamed_bus = rename the_d22

                            
                             //dev_println(sprintf "renamex: Did rename actual 3/2 in %s" (d22_to_str renamed_bus))
                             //dev_println(sprintf "renamex Binding now become %A" renamed_bus)
                            vprintln 3 (sprintf "Blaze_bind primary bind %s:  %s/%s/%s (%i) to %s (%i)" key instance_name method_name side l_formals (d22_to_str the_d22) l_actuals)
                            let binding = HBT_structured_bind([method_name + "_" + side], renamed_bus)
                            let nb = (instance_name, binding)
                             //let na = (arcid, (instance_name, method_name, side)) // Dont want arc ids beyond here now ...
                            (nb::cbinds, (*na::*)cjoins)
                        | other -> sf(sprintf "%s: key: blaze_bind other form alpha_new %A" key alpha_new)

                let get_schema00 = function
                    | BPA_alpha_bus(binding_note_, formal_terminal, formal_schemaN0) -> formal_schemaN0
                    | BPA_fu_terminal(binding_note_, formal_terminal, subfield_no, Some formal_schema00) ->
                         if subfield_no < 0 then formal_schema00
                         else // This wont happen if properly normalised
                             //dev_println (sprintf "New and still temporary munge site %i " subfield_no)
                             let (protocol_name, schema) = formal_schema00 // Temporary munge code before we have proper replicator. rdy will be assigned more than once YUK!
                             let rec munge pos = function // New munge site
                                 | [] -> [] // Premature?
                                 | h::tt when pos < 2 -> h::(munge (pos+1) tt)
                                 | h::tt -> if pos-2 = subfield_no then [h] else munge (pos+1) tt
                             let schema = munge 0 schema
                                  // let busnets1 = munge 0 busnets
                             let protocol_name =
                                 match protocol_name with
                                         | IPB_custom ss -> IPB_custom(ss + sprintf "subfield_%i" subfield_no) // Yuck for now
                                         | other -> sf(sprintf "Late munge protocol name other %A" other)
                             (protocol_name, schema)
                    | other -> sf(sprintf "key=%s get_schema00 other form %A" key other)


                let bp_force_d22_bus msg alfa = // convert any zygotes into real busses
                    match bp_get_d22_bus msg alfa with
                        | Some d22 -> Some d22
                        | None ->
                            match alfa with
                                | BPA_alpha_bus(BN_note _, (iname, bus_name, side_), schemaN0) when bus_name = g_bus_zygote -> // busname is in the iname field for historic reasons and sex could be encoded in the 'side' field. perhaps tidy.
                                    let sex = None // For late rez of input or  output terminal bus, sex is needed here...
                                    let bus_name = iname
                                    dev_println (sprintf "bp_force_d22: Creating bus %s sex=%A" bus_name sex)
                                    let schema = get_schema00 alfa
                                    let additional_portmeta = []
                                    let d22:d22_t = nbm.busrez2 ww additional_portmeta sex schema bus_name
                                    Some d22
                                | _ -> None
                match (bp_force_d22_bus "L1729-alpha0" alpha0, bp_force_d22_bus "L1729-alpha1" alpha1) with
                     | (None, None) -> 
                         let busname_root =
                             match alpha0 with
                                 //| BPA_alpha_bus(BN_note _, (iname, bus_name, side), schemaN0) when bus_name = g_bus_zygote -> iname // This is the name of the bus, actually.
                                 | BPA_fu_terminal(BN_note(_, _), (iname, method_name, side), _, _) -> iname+method_name+side
                                 | BPA_fu_terminal(BN_bound_formal(_, _, formalname, _), _, _, _) -> formalname
                                 | _ ->
                                     dev_println (sprintf "Want bus hint name from %A" alpha0)
                                     "gluebus"
                         let bus_name = funique busname_root
                         let sex = None // For late rez of input or  output terminal bus, sex is needed, but those cases are handled in the force code just above for now.
                         dev_println (sprintf "blaze_bind: creating bus %s sex=%A" bus_name sex)
                         let schema = get_schema00 alpha0
                         let additional_portmeta = []
                         let d22:d22_t = nbm.busrez2 ww additional_portmeta sex schema bus_name

                         // Make the connection of the new bus to alpha1
                         let (cbinds, cjoins) = blaze_bind (cbinds, cjoins) d22 alpha1 
                         // then proceed with connecting alpha0.
                         blaze_bind (cbinds, cjoins) d22 alpha0

                     | (None, Some d22) -> blaze_bind (cbinds, cjoins) d22 alpha0 // Second arg is already a bus, so bind it to first one ...
                     | (Some d22, None) -> blaze_bind (cbinds, cjoins) d22 alpha1 // Vice versa

                     | (Some src_d22, Some dest_d22) -> // Two busses, weld them together.
                         let src_binding_note = bp_get_binding_note "msg" alpha0
                         let dest_binding_note = bp_get_binding_note "msg" alpha1
                         //let ((src_protocol, _, _, schema_), src_nets) = src_d22 // Schema will indicate which direction of assignment needed. 
                         blaze_joint vbf (cbinds, cjoins) key src_binding_note src_d22 dest_binding_note dest_d22
                         


                 //dev_println (sprintf "%s: q25 wire food elaborated 2/2 internal to %s %s %s formal %A" key instance_name method_name side formal_schema00)

            | other -> sf(lp_msg (sprintf "(in method %s): wire arc other form %A" mod_name other))


        let (all_bindings, cjoins) = List.fold (wire_arc false) ([], []) all_arcs

        let _ =
            let report_line = sprintf "BP arc stats: ^all_arc=%i ^all_bindings=%i  ^cjoins=%i" (length all_arcs) (length all_bindings) (length cjoins)
            mutadd settings.m_report_file_contents report_line
            vprintln 2 report_line
            
        let silly_counter = // Ignore this in the output
           let counter_value = hx_logic_s ww nbm "ignore_this_counter_please" (false, 2) 
           nbm.add_seq(counter_value, ix_plus (xi_one) counter_value)


        // Now wire up its components. We transpose from method-centric to component-centric, collating on nets to a given instance
        let instance_centric_bindings = generic_collate fst all_bindings

        let instances = !m_tail2_fus @ instances

        let lookup_component instance_name =
            match op_assoc instance_name instances with
                | Some v -> v
                | None ->
                    vprintln 0 (sprintf "Components available are " + sfold (fst) instances)
                    sf (sprintf "2066 missing component " + instance_name)

        // Add wired-up component instances as children of the main result VM
        let _ =
            let add_and_bind_component (instance_name, bindings) =
                if (instance_name = g_bus_zygote && false) then
                    dev_println(sprintf "Still need to rez bus and bind for (^=%i) %A" (length bindings) bindings) // TODO
                    ()
                elif instance_name = g_self_instance_name then
                    hpr_yikes ("ignore bind to self for now")
                    () // FOR now ?
                else
                match lookup_component instance_name with 
                | C_ipinst(instance_name, (iinfo, Some vmd)) ->
                    vprintln 3 (sprintf "Start bind child instance %s with %i bindings " instance_name (length bindings))
                    let domain = None // Can be used for wire length estimation in the future.
                    let portmeta = []
                    let bindings = map snd bindings
                    nbm.add_child ww vmd domain instance_name portmeta bindings // Add this child only when its final bindings are known.
                | other -> sf(sprintf "%s add_and_bind_component other form %A" instance_name other)
            app add_and_bind_component instance_centric_bindings
                
        let ww = WF 2 "BP compile" ww (sprintf "finshed compile %s" mod_name)
        mutadd settings.m_local_module_defs (mod_name, ans)



        if settings.final_dot_report_enabled then // Too much repeated code
            let (all_arcs_with_terminals_and_edge_keys202, ghost_instances) = gec_all_arcs_with_terminals_and_edge_keys ww mod_name "final" all_arcs
            let dot_arcs = // Split arcs into a list of endpoints (Some might have 3 ends etc for now)
                let shone cc ((key, src_term, src_arcid), (dest_key, dest_term, dest_arcid_), arc) =
                    if src_term = dest_term then sf(sprintf "postnormal: Encountered reflexive arc with key %s  %A" key (src_term, src_arcid))
                    (src_arcid, src_term) :: (src_arcid, dest_term) :: cc
                List.fold shone [] all_arcs_with_terminals_and_edge_keys202
            let project_arc_id = fst
            let d102 = generic_collate project_arc_id dot_arcs // Collate as needed for dot
            make_dot_report ww settings mod_name "final" (ghost_instances @ instances) methods_and_threads d102


        ans

    | other -> sf(sprintf "bp_compile other %A" other) // end of bp_compile



let opath_blueparrot_vm ww (op_args:op_invoker_t) vm0_ =
    let orangename = op_args.stagename
    let ban = orangename + ": " + g_blueparrot_banner + " " + get_os()
    vprintln 1 (ban + "\n" + timestamp(true) + "\n")
    g_version_string := ban
    g_m_xact_op_vendor :=  g_parrot_temp_iplib.vendor // These three are yuck. Please refactor.
    g_m_xact_op_library := g_parrot_temp_iplib.library
    g_m_xact_op_version :=  g_parrot_temp_iplib.version

    let ww = WF 2 orangename ww "Start"
    let xmlsrc = control_get_s orangename op_args.c3 "bpxmlsrc" None


    g_xvd := control_get_i op_args.c3 ("blueparrot-loglevel") 20 

    let the_canned_fu_settings =
        {
            m_aux_fu_defs=            ref []            
        }

    let settings =
        {
            stagename =                           op_args.stagename
            prenormal_dot_report_enabled=         true
            normalised_dot_report_enabled=        true
            final_dot_report_enabled=             true
            m_report_file_contents=   ref []
            m_local_module_defs=      ref []
            canned_fu_settings=       the_canned_fu_settings
            use_diadic_only=          true
            //prod_vd=                5
        }

    mutadd settings.m_report_file_contents (blueparrot_banner())
    mutadd settings.m_report_file_contents (timestamp true)


    let inputs = import_src_file_in_xml_form ww settings xmlsrc

    let selected root_node = None
    let (mod_count, other_count) =
        let count_inputs  (mod_count, other_count) = function
            | BP_module(_, _) -> (mod_count+1, other_count)

            | other -> sf(sprintf "count_inputs other form %A" other)

        List.fold count_inputs (0, 0) inputs

    vprintln 1 (sprintf "BlueParrot: read in %i module definitions. %i other things." mod_count  other_count)

    let compiled = map (bp_compile ww settings) inputs // Each module compiled separately at the moment.  Will want to flatten in future versions ...



    let instantiating_vms = // Main compiler output(s) - one per module compiled.  Modules do not share resource instances (only kinds).
        let vmpolish = function
            | IR_nbm(mod_name, nbm) ->
                let attributes = []
                let vmd = nbm.finish_definition ww attributes  // TODO do not call this here... do earlier.
                let vlnv_name = { g_parrot_temp_iplib with kind=[mod_name] }
                let iinfo =
                    { g_null_vm2_iinfo with
                        definitionf=          true
                        generated_by=         g_blueparrot_banner
                        kind_vlnv=            vlnv_name
                        externally_provided=  false // false, since these should not be separate modules
                      }

                (iinfo, Some vmd)

            | other -> sf (sprintf "vmpolish other form %A" other)

        map vmpolish compiled


    let rez_instantiated_fu_vms() = // An aux output of other signatures or implementations.
        let items = !the_canned_fu_settings.m_aux_fu_defs
        let ww = WF 2 "Rez_instantiated_fu_vms" ww (sprintf "%i definitions to tack on to primary output." (length items))

        let gen_bespoke_fu_skeleton (mod_name, vmd) =
            let vlnv_name = { g_parrot_temp_iplib with kind=[mod_name] }
            let iinfo =
                    { g_null_vm2_iinfo with
                        definitionf=          true
                        generated_by=         g_blueparrot_banner
                        kind_vlnv=            vlnv_name
                        externally_provided=  false // True in general in the future? That's not worked out yet...
                      }
            (iinfo, Some vmd)
        let enabled = true // get from recipe please
        if enabled then map gen_bespoke_fu_skeleton items
        else []
    // ...



#if SPARE
    // Form Output VM2
    let (locals, formals, execs) = ([], [], []) // For now
    let kind = "settings.op_kindname-fornow" 
    let atts = []
    // library="BlueparrotAutoRender" 
            
    let local_decls =
        let wrap nn = DB_leaf(None, Some nn)
        DB_group({ g_null_db_metainfo with kind="internal_nets" }, map wrap locals)

    let iinfo =
        { g_null_vm2_iinfo with
            definitionf=          true
            generated_by=         orangename
            kind_vlnv=            vlnv_name
            externally_provided=  false // false, since these should not be separate modules
        }

    let vm2 = HPR_VM2({ g_null_minfo with name=vlnv_name; atts=atts }, formals @[local_decls], [], execs, [])
#endif

    instantiating_vms @ rez_instantiated_fu_vms() // @ [(iinfo, Some vm2)]    


let blueparrot_used () =
    let stagename = "blueparrot_compile"
    let argpattern =
        [
            Arg_required("bpxmlsrc", -1, "Name of input source file (as generated by the BTBP front end.", "");

//          Arg_required("named-conserve-predicate", 1, "Pre-parser conservation predicate", "");
//            Arg_required("o", 1, "Name of SystemC or Verilog output file", "");

            Arg_int_defaulting("blueparrot-loglevel", 3, "Verbosity level (higher leads to larger report files)");
        ]

    install_operator (stagename,  "BlueParrot HLS Compiler", opath_blueparrot_vm, [], [], argpattern)


// eof
