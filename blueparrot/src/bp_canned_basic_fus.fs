//
// CBG BlueParrot - an HLS Compiler
// (C) 2023 University of Cambridge, Computer Laboratory.
//

module bp_canned_basic_fus



open System.Numerics
open System.Collections.Generic


open opath_hdr
open hprls_hdr
open abstract_hdr
open dotreport
open abstracte
open moscow
open meox
open yout
open hprxml
open hprtl
open protocols

open bp_gram


// Elaboration types (move to own file?)

type canned_fu_settings_t =
    {
        m_aux_fu_defs:           (string * hpr_machine_t) list ref  // Those hprtl leaf cells created (for protypes/reference/etc) // NB also held in a dictionary here, but this is the list for rendering as RTL aux outputs perhaps ...
    }

type bp_component_atts_t =
    {
        autoapply_methodname: string option // Used to automatically apply a function (eg .read() on registers) where an FU is used in an implicit method call situation.
        femeral:              bool          // Holds if instantiated by the compiler and not the user.
    }

let g_default_bp_component_atts =
    {
        femeral=                     false
        autoapply_methodname=        None
    }

type rzmapping_t = (string * (bool * int) list) list // This is essentially a precision list and needs to be extended for BP16 and so on.
    
type bp_combinator_t =
    | BPC_split
    | BPC_joinphi
    | BPC_rendezvous of rzmapping_t

type bp_arc_t =
    BPARC_src_dest of string * bp_activation_t * bp_activation_t

and bp_schema22_t = (ipblk_protocol_t * string * bool option * netport_pattern_schema_t list) // (protocol_name, pi_name, sex, schema)

and bp_schemaN0_t = protocols.ipblk_protocol_t * netport_pattern_schema_t list

and d22_t =  bp_schema22_t * (string * hexp_t) list     // busrez22 result aka temp_structured_netgroup_t TODO tidy names.  ((protocol_name, pi_name, sex, schema), busnets) 

and bp_sex_t = Ain | Aout

and terminal_id_t = string * string * string // (instance_name, method_name, side)
and bp_activation_t =
    | BPA_dead
    | BPA_combinate of bp_combinator_t * binding_note_t * bp_activation_t list * bp_schemaN0_t option
    | BPA_alpha_bus of binding_note_t * terminal_id_t * bp_schemaN0_t
    | BPA_always_ready_expression of bp_env_vale_t // 
    | BPA_fu_terminal of binding_note_t * terminal_id_t * int * bp_schemaN0_t option // (note, terminal_id, lane,  small_schema)  // Direction is unused and its too easy to get wrong, so delete me please.

    // deprecated - ok? - the structured nets are the formals.
    | BPA_fu_contact of binding_note_t * terminal_id_t * d22_t // (instance_name, method_name, side, d22)

    //| BPA_ext_method_alpha of bp_env_vale_t * string * d22_t // First two fields are annotations to be pulled into the binding_note?

// and d1_t = (string * string) * (ipblk_protocol_t * netport_pattern_schema_t list)   // rename me

and structured_ports_t = ((string * string) * d22_t) list

and bp_env_vale_t =
    | BE_biginteger of BigInteger
    | BE_component of string * bp_component_atts_t * (vm2_iinfo_t * hpr_machine_t option) * structured_ports_t
    | BE_bound_activation of string list *  bp_activation_t
    | BE_result of bp_activation_t option
    | BE_unit
    
and binding_note_t = // We most likely won't need this except for error reporting.  No, it can essentially contain the contact id which is most important.
    | BN_bound_formal of arcid_t * int * string * (bp_type_t * bool * int)
    | BN_note of arcid_t * string

and arcid_t = string // a unique identifier starting with "arc" by convention.

type bp_env_t = (string * bp_env_vale_t) list



let binding_note_to_str = function

    //| BN_bound_formal of int * string * (bp_type_t * bool * int)
    //| BN_note(arcid, msg) -> 
    | BN_note(oner, twoer) -> oner + "\\" + twoer
    | other -> sprintf "binding_note_to_str other %A" other


let bp_terminalToStr (instance_name, method_or_bus_name, side) =  sprintf "%s/%s/%s" instance_name method_or_bus_name side

let combinatorToStr = function
    | BPC_rendezvous _ -> "RVZ"
    | BPC_split -> "SPLIT"
    | BPC_joinphi -> "PHI"

let rec alphaToStr arg =
    match arg with 
       | BPA_dead -> "DEAD"
       | BPA_alpha_bus(binding_note, terminal, schema0N_) -> binding_note_to_str binding_note + bp_terminalToStr terminal
       | BPA_always_ready_expression(env_vale) -> sprintf "envToStr? %A env_vale" env_vale
       | BPA_combinate(op, binding_note, acts, _) ->
           sprintf "%s[%s]" (combinatorToStr op) (sfold alphaToStr acts)
       | BPA_fu_terminal(binding_note, terminal, lanes, schemaN0_o) -> sprintf "TERMINAL(%s) lanes=%i" (bp_terminalToStr terminal) lanes
       | BPA_fu_contact(binding_note, terminal, d22) -> sprintf "CONTACT(%s,%s,%s)" (f1o3 terminal) (f2o3 terminal) (f3o3 terminal) 



// end of elaboration types
// ==================================



let g_m_bp_defined_primitive_kinds = new OptionStore<string, hpr_machine_t * structured_ports_t>("bp_defined_primitve_kinds")//Dont need to save nbm!


// Standard synchronous protocol simplex bus schema generator.
// VALID has the same direction as the data.  They are both inputs, denoted with "i" for a put port and outputs for an aout port.
// RDY has the opposite direction always.
let bp_gec_standard_synchronous_netport_schema datadir fields =

    let revdir = // Note, VALID always has the same direction (sex) as the data fields (sub-busses).
        match datadir with
            | "i" -> "o"
            | "o" -> "i"
            | other -> other // should be 'x' nominally.
            
    //let total_bit_width = List.fold 
    let bobs = 
        let valid =
            { g_null_netport_pattern_schema with lname="VALID"; width="1"; sdir=datadir;    idleval="0"; ats=[(g_control_net_marked, "true")] }
        let rdy =
            { g_null_netport_pattern_schema with lname="RDY"; width="1"; sdir=revdir;    idleval="0"; ats=[(g_control_net_marked, "true")] }
        let dgen (name, width) cc  =
            if width=0 then cc // Supress zero-width sub-busses (eg void return)
            else { g_null_netport_pattern_schema with lname=name; width=i2s width; sdir=datadir }::cc
        // The order of listed the nets here may be relied on below. Sdir is the direction on the slave/target/completer.
        valid :: rdy :: List.foldBack dgen fields []

    // TODO ideally reflect signed information in the result bus.
    let protocol_name =
        if nullp fields then "SSI_unit" else
        "SSI" + List.foldBack (fun (subfield_name_, width) cc -> "_" + i2s width + cc) fields ""
    //dev_println(sprintf "rez SSI %A fields=%A" protocol_name  fields)
    let schema00_pair = (IPB_custom protocol_name, bobs) // protocols.ipblk_protocol_t * netport_pattern_schema_t list
    schema00_pair    // end of gec_SSI


//
// ---------------------------------------------------------------
//

let g_regs_width_override = false // Enable this in future when it works. Then allow it to be set from the recipe please.
    
// BlueParrot canned register. TODO add a reset value attribute.  TODO sort out overrides.
let bp_create_kind_canned_register_really ww fusettings msg kind_name (signedf, (bit_width_o:int option)) = // If bit_width is None then make a generic with parameter override for width.
    let msg = (sprintf "rezzing %s %A %A (%s)" kind_name signedf bit_width_o msg)
    let ww = WF 3 "BP canned register" ww msg

    //let genv = [ ("DATA_WIDTH", xi_num bit_width); ("SIGNED", xi_num(if signedf then 1 else 0))] // generic parameter environment - hardly needed?
    let genv = []
    

    let nbm = new nbm_t(ww, kind_name, false, genv) // caller ensure not defined before

    // We'll have all four put/get ports in this most verbose form, although the write_get port will normally be supressed by pragma, since it serves no use at all.
    let spare_prefix = ""
    let write_put =
        let schema_pair = bp_gec_standard_synchronous_netport_schema "i" [("WDATA", (valOf_or bit_width_o 32))]
        let additional_portmeta = []
        let port_name = spare_prefix + "write_put"
        let d22 = nbm.busrez2 ww additional_portmeta (Some false) schema_pair port_name 
        (("write", "put"), d22) 
    let write_aout =
        let schema_pair = bp_gec_standard_synchronous_netport_schema "o" []
        let additional_portmeta = []
        let port_name = spare_prefix + "write_aout"
        let d22 = nbm.busrez2 ww additional_portmeta (Some false) schema_pair port_name 
        (("write", "aout"), d22) 
    let read_put =
        let schema_pair = bp_gec_standard_synchronous_netport_schema "i" []
        let additional_portmeta = []
        let port_name = spare_prefix + "read_put"
        let d22 = nbm.busrez2 ww additional_portmeta (Some false) schema_pair port_name 
        (("read", "put"), d22)

    let wideness = valOf_or bit_width_o 32            
    let rdata_bus_name = sprintf "YDATA%i" wideness
    let read_aout =
        let schema_pair = bp_gec_standard_synchronous_netport_schema "o" [(rdata_bus_name, wideness)]
        let additional_portmeta = []
        let port_name = spare_prefix + "read_aout"
        let d22 = nbm.busrez2 ww additional_portmeta (Some false) schema_pair port_name 
        (("read", "aout"), d22)

    let structured_ports = [ write_put; write_aout; read_put; read_aout ]
    let _ = nbm.fu_end ww  // Call this after last formal added.

    let hx_sform0 (_, d22) lname = hx_sformal msg d22 lname
        
    let the_underlying_register = hx_logic_s ww nbm kind_name (signedf, valOf_or bit_width_o 32) 
    nbm.add_seq_g (hx_and (hx_sform0 write_put "VALID") (hx_sform0 write_put "RDY"))  (the_underlying_register, hx_sform0 write_put "WDATA")


    nbm.add_comb (hx_sform0 read_aout rdata_bus_name, the_underlying_register)

    nbm.add_comb (hx_sform0 read_aout "RDY", hx_sform0 read_put "RDY") // Forward always-ready put/aout pair to each other.  Idiomatic.
    nbm.add_comb (hx_sform0 read_put "VALID", hx_sform0 read_aout "VALID") 

    nbm.add_comb (hx_sform0 write_aout "RDY", hx_sform0 write_put "RDY") // Forward always-ready put/aout pair to each other. 
    nbm.add_comb (hx_sform0 write_put "VALID", hx_sform0 write_aout "VALID") 
    
    let attributes = []; // att_att_t list

    let vmd = nbm.finish_definition ww attributes 
    mutadd fusettings.m_aux_fu_defs (kind_name, vmd)
    (vmd, structured_ports) // End of built-in register kind.





let bp_create_kind_canned_register ww fusettings msg (signedf, bit_width) =
    let (kind_name, overrides) =
        if g_regs_width_override then (sprintf "BPREG_%i_%s" bit_width (if signedf then "S" else "US"), [("DATA_WIDTH", i2s bit_width)])
        else (sprintf "BPREG_P_%s" (if signedf then "S" else "US"), [])

    match g_m_bp_defined_primitive_kinds.lookup kind_name with
        | Some (existing_vmd, structured_ports) ->
            (kind_name, existing_vmd, structured_ports, overrides)
        | None ->
            let bit_width_o = (if g_regs_width_override then None else Some bit_width)
            // We can pass bit_width in here or leave it as a parameter to be overridden on instantiation.
            let (ans, structured_ports) = bp_create_kind_canned_register_really ww fusettings msg kind_name (signedf, bit_width_o)
            g_m_bp_defined_primitive_kinds.add kind_name (ans, structured_ports)
            (kind_name, ans, structured_ports, overrides) // end of canned_register


//
// ---------------------------------------------------------------
//

//
//
//
let bp_create_kind_canned_basic_arith_alu_really ww msg kind_name (op, invf, args_spec, result_spec) overrides =
        // If bit_width is None then make a generic with parameter overrides in the future...
    let msg = sprintf "creating BP canned kind %s (%s)" kind_name msg
    let ww = WF 3 "BP canned_basic_arith_alu_really" ww msg

    //let genv = [ ("DATA_WIDTH", xi_num bit_width); ("SIGNED", xi_num(if signedf then 1 else 0))] // generic parameter environment - hardly needed?
    let genv = []

    let nbm = new nbm_t(ww, kind_name, false, genv)

    // Arith has one method with two args and needs an external rendezvous.
    // Internal instead would clearly help certain optimisations, but that's the easy way to a low minimum result.
    let spare_prefix = ""
    let arg_put =
        let args1 = map (fun ((signedf, bit_width), index) -> (sprintf "SUB%i" index, bit_width)) (zipWithIndex args_spec)
        let schema_pair = bp_gec_standard_synchronous_netport_schema "i" args1
        let additional_portmeta = []
        let port_name = spare_prefix + "operate_put"
        let d22 = nbm.busrez2 ww additional_portmeta (Some false) schema_pair port_name 
        (("operate", "put"), d22) 
    let rdata_bus_name = sprintf "YDATA%i" (snd result_spec)
    let result_aout =
        let schema_pair = bp_gec_standard_synchronous_netport_schema "o" [(rdata_bus_name, snd result_spec)]
        let additional_portmeta = []
        let port_name = spare_prefix + "operate_aout"
        let d22 = nbm.busrez2 ww additional_portmeta (Some false) schema_pair port_name 
        (("operate", "aout"), d22)

    let structured_ports = [ arg_put; result_aout ]
    let _ = nbm.fu_end ww  // Call this after last formal added.

    let silly_counter = // Locals
       let counter_value = hx_logic_s ww nbm "ignore_this_counter_please" (false, 2) 
       nbm.add_seq(counter_value, ix_plus (xi_one) counter_value)

    // Implementation entirely missing ...



    let hx_sform0 (_, d22) lname = hx_sformal msg d22 lname
        
    let attributes = []; // att_att_t list

    let vmd = nbm.finish_definition ww attributes 
    (vmd, structured_ports) // end of ALU_really

 // Generate a textual name for a set of input sub-channels.
let squirrel_args_spec ww msg args_spec =
    let arg_squirrel (signedf, bit_width) = (i2s bit_width)  + (if signedf then "S" else "U")
    sfolds (map arg_squirrel args_spec)
                

// The arguments have already been rendezvoused into a single SSI port with (normally two) sub-busses.
let bp_create_kind_canned_basic_arith_alu ww fusettings msg (op, invf, args_spec, result_spec) =
    let args_sq = squirrel_args_spec ww msg args_spec
    let opcode = 
        match op with
            | EO_plus   -> "1"
            | EO_dltd   -> "2"
            | EO_deqd   -> "3"
            | EO_binor  -> "4"
            | EO_bitand -> "5"
            | EO_xor    -> "6"
            | EO_times  -> "8"                
            | EO_divide -> "9"  
            | other -> sf (msg + sprintf ": unsupported basic arith operator %A" other)

    let overrides =
        [ ("OPCODE", opcode); ("INVF", if invf then "1" else "0") ]

    let signedf = true // FOR NOW --- TODO
    let result_spec = (signedf, 32) // A simple default for now! TODO
    let result_sq =  (i2s (snd result_spec))  + (if fst result_spec then "S" else "US")

    let (kind_name, overrides) = // Squirrel arg specs into a string
        let opcode_s = sprintf "_OP%s" opcode
        let invf_s = (if invf then "_INVF" else "")
        if g_regs_width_override then
            let overrides = muddy "L302 (\"DATA_WIDTH\", i2s bit_width)::overrides"
            (sprintf "BP_ADDER_ETC_P_%s" (if signedf then "S" else "US"), overrides) // TODO  - this is not right yet

        else (sprintf "BP_ADDER_ETC_%s%s%s_%s" opcode_s invf_s result_sq  args_sq, [])


    match g_m_bp_defined_primitive_kinds.lookup kind_name with
        | Some (existing_vmd, structured_ports) ->
            (kind_name, existing_vmd, structured_ports, overrides)
        | None ->
            // We can pass bit widths in here or leave it as a parameter to be overridden on instantiation.
            let (vmd, structured_ports) = bp_create_kind_canned_basic_arith_alu_really ww msg kind_name (op, invf, args_spec, result_spec) overrides
            mutadd fusettings.m_aux_fu_defs (kind_name, vmd)
            g_m_bp_defined_primitive_kinds.add kind_name (vmd, structured_ports)
            (kind_name, vmd, structured_ports, overrides) // end of arith kind create

//
// ---------------------------------------------------------------
//


// Catenating rendezvous: result port has flattened sub-busses for each input arg subfield.
let bp_gec_rendezvous_return_schema ww msg args = 
    let ss signedf = if signedf then "S" else "U"    
    let output_subs = 
        let gen_sub_field (subfields, arg_index) cc =
            let gen_sub_sub ((signedf, bit_width), sub_index) cc =
                (sprintf "RSUB%i_%i_%s%i" arg_index sub_index (ss signedf) bit_width, bit_width)::cc
            List.foldBack gen_sub_sub (zipWithIndex subfields) cc
        List.foldBack gen_sub_field (zipWithIndex args) []
    let result_schema0N = bp_gec_standard_synchronous_netport_schema "o" output_subs
    result_schema0N
        
//
//       
let bp_create_kind_canned_rendezvous_really ww msg kind_name arity args = 
    let msg = (sprintf "creating BP canned kind %s (%s)" kind_name  msg)
    let ww = WF 3 "BP canned_rendezvous" ww msg
    let genv = []
    let nbm = new nbm_t(ww, kind_name, false, genv)
    let spare_prefix = ""
    let ss signedf = if signedf then "S" else "U"    

    let gen_input_port (subfields, input_index) =

        let subfield_pairs = map (fun ((signedf, bit_width), sub_index) -> (sprintf "DIN_%i_%i_%s%i" input_index sub_index (ss signedf) bit_width, bit_width)) (zipWithIndex subfields)
        let schema_pair = bp_gec_standard_synchronous_netport_schema "i" subfield_pairs
        let additional_portmeta = []
        let port_name = spare_prefix + sprintf "arg_%i_put" input_index
        let d22 = nbm.busrez2 ww additional_portmeta (Some false) schema_pair port_name 
        ((sprintf "arg_%i" input_index, "put"), d22) 

    let input_ports = map gen_input_port (zipWithIndex args)
    let result_aout =
        let result_schema_pair =  bp_gec_rendezvous_return_schema ww msg args
        let additional_portmeta = []
        let port_name = spare_prefix + "alltogether_aout"
        let d22 = nbm.busrez2 ww additional_portmeta (Some false) result_schema_pair port_name 
        (("alltogether", "aout"), d22)

    let structured_ports = input_ports @ [ result_aout ]
    let _ = nbm.fu_end ww  // Call this after last formal added.

    // Implementation entirely missing ...
    let hx_sform0 (_, d22) lname = hx_sformal msg d22 lname

    // An example
    let ss_set = simplenet "setter"  // Two simple scalars (one-bit wide nets) a wire. These must later appear on the lhs of nbm.add_comb (l,r)
    let ss_clear = simplenet "clearer"
    nbm.add_freehand_locals ww [ ss_set; ss_clear ] // 

    let synch_set_reset_example1 = hx_register ww nbm "SSR1" 1(*word_width*) // A one-bit register
    nbm.add_seq (synch_set_reset_example1, hx_or ss_set (hx_and (hx_inv ss_clear) synch_set_reset_example1))
    // End of example

    let silly_counter = // Locals
       let counter_value = hx_logic_s ww nbm "ignore_this_counter_please" (false, 2) 
       nbm.add_seq(counter_value, ix_plus (xi_one) counter_value)
        
    let attributes = []; // att_att_t list

    let vmd = nbm.finish_definition ww attributes 
    (vmd, structured_ports) // end of rendezvous_really


let bp_create_kind_canned_rendezvous ww fusettings msg args =
    let arity = length (args:(bool * int) list list) // Args is a list of lists of subfields.

//    let total_input_width_ =
//        let summer cc (signed, bit_width) = cc+bit_width
//        List.fold summer 0 args


    let (kind_name, overrides) =
        let gin (subfields, arg_index) =
            let ss signedf = if signedf then "S" else "U"            
            let letter = (char) ((int 'A') + arg_index)
            "R_" + underscore_fwd(map (fun (s,w)->sprintf "%c%s%i" letter (ss s) w) subfields)

        (sprintf "RENDEZVOUS101_N%i_%s" arity (underscore_fwd (map gin (zipWithIndex args))), []) 

    match g_m_bp_defined_primitive_kinds.lookup kind_name with
        | Some (existing_vmd, structured_ports) ->
            (kind_name, existing_vmd, structured_ports, overrides)
        | None ->
            // We can pass bit_width in here or leave it as a parameter to be overridden on instantiation.
            let (vmd, structured_ports) = bp_create_kind_canned_rendezvous_really ww msg kind_name arity args
            g_m_bp_defined_primitive_kinds.add kind_name (vmd, structured_ports)
            mutadd fusettings.m_aux_fu_defs (kind_name, vmd)
            (kind_name, vmd, structured_ports, overrides)
    
//
//
// ---------------------------------------------------------------
//
//
//
let bp_create_kind_canned_integer_generator ww fusettings msg (ivale:BigInteger) =

    let bits = bound_log2 ivale
    let bits = if bits=0 then 1 else bits //  We'll have one bit in a zero.
    let (kind_name, overrides) = // Squirrel arg specs into a string
        (sprintf "BP_CONST_%A" ivale, []) // use an override in future but hopefully optimise completely away normally anyway.
    let ww = WF 3 "BP canned_integer_generator" ww msg
    let genv = []
    let nbm = new nbm_t(ww, kind_name, false, genv)
    let spare_prefix = ""
    

    // Strictly this should have a put port to simplify it, but can we not ignore that for now?
    let operate_put =
        let result_schema_pair = bp_gec_standard_synchronous_netport_schema "i" []
        let additional_portmeta = []
        let port_name = spare_prefix + "operate_put"
        let d22 = nbm.busrez2 ww additional_portmeta (Some false) result_schema_pair port_name 
        (("operate", "put"), d22)

    let result_bus_name = sprintf "YDATA%i" bits

    let operate_aout =
        let result_schema_pair = bp_gec_standard_synchronous_netport_schema "o" [(result_bus_name, bits)]
        let additional_portmeta = []
        let port_name = spare_prefix + "operate_aout"
        let d22 = nbm.busrez2 ww additional_portmeta (Some false) result_schema_pair port_name 
        (("operate", "aout"), d22)

    let structured_ports = [ operate_put; operate_aout ]
    let _ = nbm.fu_end ww  // Call this after last formal added.

    let hx_sform0 (_, d22) lname = hx_sformal msg d22 lname
    
    nbm.add_comb (hx_sform0 operate_aout "RDY", hx_sform0 operate_put "RDY") // Forward always-ready put/aout pair to each other.  Idiomatic.
    nbm.add_comb (hx_sform0 operate_put "VALID", hx_sform0 operate_aout "VALID") 
    nbm.add_comb (hx_sform0 operate_aout result_bus_name, xi_bnum ivale) // This is the actually functionality!
    
    let attributes = []; // att_att_t list

    let vmd = nbm.finish_definition ww attributes
    //mutadd fusettings.m_aux_fu_defs (kind_name, vmd) // TODO more than once...?
    
    (kind_name, vmd, structured_ports, overrides)

//
// ---------------------------------------------------------------
//

// Tokenised synchronous static RAM (SSRAM) - single-ported for now.
    
let bp_create_kind_cvip_SSRAM_really ww fusettings msg (latency, locations, (signedf, word_width)) kind_name overrides =
    let msg1 = "bp_create_kind_cvip_SSRAM"
    let ww = WF 3 msg1 ww msg
    let addr_width = bound_log2 (BigInteger (locations:int64))
    let genv = []
    let nbm = new nbm_t(ww, kind_name, false, genv)
    let spare_prefix = ""


    // This is the untokenised core ...
    let core_iname = "untokenised_core"
    let (kind_vlnv, portmeta_h, actuals_rides, the_component, net_schema_formal, block_meld) =
        let portmeta_s = overrides
        let kkey = core_iname
        let descr = msg1
        let props = []
        let netst = if signedf then Signed else Unsigned
        let no_ports = 1

        cvipgen.gec_cv_romram_ip_block ww msg1 kkey descr no_ports portmeta_s netst (*constval*)[] (*rom_id_o*)None latency props // This will be fully-pipelined with no handshakes.  Simple nets: addr wdata rdata wen ren


    let portmeta = overrides

    let the_component = valOf_or_fail msg1 (snd the_component) // This deletes the iinfo, which is recreated in both ways (def and instance) later on... not ideal?
    mutadd fusettings.m_aux_fu_defs (kind_name, the_component) // Save core definition for optional render later on


    // Sub-bus names (sb_names).
    let cmd_cmd_sb_name = "CMD"
    let cmd_addr_sb_name = sprintf "ADDR%i" addr_width
    let cmd_wdata_sb_name = sprintf "WDATA%i" word_width
    let aout_cmd_sb_name = "CMD"
    let aout_rdata_sb_name = sprintf "RDATA%i" word_width
    
    let cmd_put =
        let result_schema_pair = bp_gec_standard_synchronous_netport_schema "i"  [(cmd_cmd_sb_name, 1); (cmd_addr_sb_name, addr_width); (cmd_wdata_sb_name, word_width)]
        let additional_portmeta = []
        let port_name = spare_prefix + "cmd_put"
        let d22 = nbm.busrez2 ww additional_portmeta (Some false) result_schema_pair port_name 
        (("cmd", "put"), d22)

    let cmd_aout =
        let result_schema_pair = bp_gec_standard_synchronous_netport_schema "o" [(aout_cmd_sb_name, 1); (aout_rdata_sb_name, word_width)]
        let additional_portmeta = []
        let port_name = spare_prefix + "cmd_aout"
        let d22 = nbm.busrez2 ww additional_portmeta (Some false) result_schema_pair port_name 
        (("cmd", "aout"), d22)

    let structured_ports = [ cmd_put; cmd_aout ]
    let _ = nbm.fu_end ww  // Call this after last formal added.

    let hx_sform0 (_, d22) lname = hx_sformal msg d22 lname
    
    let attributes = []; // att_att_t list

    let silly_counter = // Locals
       let counter_value = hx_logic_s ww nbm "ignore_this_counter_please" (false, 2) 
       nbm.add_seq(counter_value, ix_plus (xi_one) counter_value)

    let hx_sform0 (_, d22) lname = hx_sformal msg d22 lname
       
    let actual_bindings =
        let binds =
            [ ("wdata", hx_sform0 cmd_put cmd_wdata_sb_name)
              ("addr", hx_sform0 cmd_put cmd_addr_sb_name);
              ]
        [ HBT_freehand_bindings binds ]// of (string * hexp_t) list
    let portmeta = map (fun (key, vale)->(key, xi_num64(atoi64 vale))) overrides
    nbm.add_child ww the_component None core_iname portmeta actual_bindings
    
    let vmd = nbm.finish_definition ww attributes
    (vmd, structured_ports) // end of RAM_really
    


let bp_create_kind_cvip_SSRAM ww fusettings msg (latency, locations, (signedf, word_width)) =
    let addr_width = bound_log2 (BigInteger (locations:int64))
    let (kind_name, overrides) = // Squirrel arg specs into a string
        let portmeta_s = [ ("DATA_WIDTH", i2s word_width); ("LANE_WIDTH", i2s word_width); ("ADDR_WIDTH", i2s addr_width); ("WORDS", i2s64 locations) ]
        (sprintf "BP_SSRAM_SP_FL%i_%ix%i_%i" latency locations word_width word_width, portmeta_s) // use an override of the generic in future, as per the core IP block!


    match g_m_bp_defined_primitive_kinds.lookup kind_name with
        | Some (existing_vmd, structured_ports) ->
            (kind_name, existing_vmd, structured_ports, overrides)
        | None ->
            // We can pass bit widths in here or leave it as a parameter to be overridden on instantiation.
            let (vmd, structured_ports) = bp_create_kind_cvip_SSRAM_really ww fusettings msg (latency, locations, (signedf, word_width)) kind_name overrides
            mutadd fusettings.m_aux_fu_defs (kind_name, vmd)
            g_m_bp_defined_primitive_kinds.add kind_name (vmd, structured_ports)
            (kind_name, vmd, structured_ports, overrides) // end of RAM kind create


//
// ---------------------------------------------------------------
//

// Used with 'if' statements and later on to be used with '? :' ternary constructs and non-strict logand and logor sugar expansion to ternary operator.
            
let bp_create_kind_canned_split_phi_really ww fusettings msg mode kind_name data_fields =
    let splitf = 
        match mode with
            | "SPLIT"
            | "UNIT_SPLIT" -> true
            | "PHI"
            | "UNIT_PHI"   -> false
            | _ -> sf "L551"

    let msg = (sprintf "creating BP canned kind %s (%s)" kind_name  msg)
    let ww = WF 3 "BP_create_kind" ww msg
    let genv = []
    let nbm = new nbm_t(ww, kind_name, false, genv)
    let spare_prefix = ""

    // let data_fields = [] // This was is the unit code, so no fields, but even an IF needs free variable (let-bounds and formals) being directed in.

// Naming schemes:     splitter and joiner have identical naming schemes:  inputs are operate/put and outputs are operate/aout where operate has a sufix _n if there is more than one of them (output of splitter and input of joiner).  Also, a splitter accepts a boolean value on its DIN subfield and a joiner will accept type-specific subfields on the two values it is joining, which should be the same type as each other.
    let ss signedf = if signedf then "S" else "U"
    let input_ports =
        let gen_input_port (subfields, index) =
            let port_suffix = if index < 0 then "" else sprintf "_%i" index
            let sub_suffix sub_index = if sub_index < 0 then "" else sprintf "_%i" sub_index   
            let subfield_pairs = map (fun ((signedf, bit_width), sub_index) -> (sprintf "DIN%s%s_%s%i" port_suffix (sub_suffix sub_index) (ss signedf) bit_width, bit_width)) (zipWithIndex subfields)
            let schema_pair = bp_gec_standard_synchronous_netport_schema "i" subfield_pairs
            let additional_portmeta = []
            let port_name = spare_prefix + sprintf "operate%s_put" port_suffix
            let d22 = nbm.busrez2 ww additional_portmeta (Some false) schema_pair port_name 
            ((sprintf "operate%s" port_suffix, "put"), d22) 

        let split_predicate = (false, 1) // A boolean branch condition. For multiway splits in the future it might be other than 0=false or non_zero=true
        let specs = if splitf then [ ([split_predicate], -1) ] else [ (data_fields, 0); (data_fields, 1) ]  // 
        map gen_input_port specs

    let output_ports =
        let gen_input_port (subfields, index) =
            let port_suffix = if index < 0 then "" else sprintf "_%i" index
            let sub_suffix sub_index = if sub_index < 0 then "" else sprintf "_%i" sub_index   
            let subfield_pairs = map (fun ((signedf, bit_width), sub_index) -> (sprintf "DOUT%s%s" port_suffix (sub_suffix sub_index), bit_width)) (zipWithIndex subfields)
            let schema_pair = bp_gec_standard_synchronous_netport_schema "o" subfield_pairs
            let additional_portmeta = []
            let port_name = spare_prefix + sprintf "operate%s_aout" port_suffix
            let d22 = nbm.busrez2 ww additional_portmeta (Some false) schema_pair port_name 
            ((sprintf "operate%s" port_suffix, "aout"), d22) 

        let specs = if splitf then [ ([], 0); ([], 1) ]  else [ (data_fields, -1) ]
        map gen_input_port specs


    let structured_ports = input_ports @ output_ports
    let _ = nbm.fu_end ww  // Call this after last formal added.

    // Implementation entirely missing ...
    let hx_sform0 (_, d22) lname = hx_sformal msg d22 lname

    let silly_counter = // Locals
       let counter_value = hx_logic_s ww nbm "ignore_this_counter_please" (false, 2) 
       nbm.add_seq(counter_value, ix_plus (xi_one) counter_value)
        
    let attributes = []; // att_att_t list

    let vmd = nbm.finish_definition ww attributes 
    (vmd, structured_ports) // end of split join really



let bp_create_kind_canned_split_phi ww fusettings msg mode data_fields =
    let (kind_name, overrides) = // Squirrel arg specs into a string

        let ss signedf = if signedf then "S" else "U"
        let gin subfields = underscore_fwd(map (fun (signedf, w)->sprintf "%s%i" (ss signedf) w) subfields)
        let arity = 2 // No multi-way jump tables for now.
        let kind_name = sprintf "BP_%s_N%i_%s" mode arity (gin data_fields)
        let portmeta_s = []
        (kind_name, [])

    match g_m_bp_defined_primitive_kinds.lookup kind_name with
        | Some (existing_vmd, structured_ports) ->
            (kind_name, existing_vmd, structured_ports, overrides)
        | None ->
            let (vmd, structured_ports) = bp_create_kind_canned_split_phi_really ww fusettings msg mode kind_name data_fields
            mutadd fusettings.m_aux_fu_defs (kind_name, vmd)
            g_m_bp_defined_primitive_kinds.add kind_name (vmd, structured_ports)
            (kind_name, vmd, structured_ports, overrides) // end of split/phi kind create



//
// ---------------------------------------------------------------
//
//
//
let bp_create_kind_canned_replicator_really ww msg kind_name arg_spec arity = 
    let msg = (sprintf "creating BP canned kind %s (%s) arity=%i" kind_name  msg arity)
    let ww = WF 3 "BP canned_replicator" ww msg
    let genv = []
    let nbm = new nbm_t(ww, kind_name, false, genv)
    let spare_prefix = ""
    let ss signedf = if signedf then "S" else "U"
    let subfield_pairs = zipWithIndex arg_spec

    let input_port =
        let input_subs = 
            let gen_sub_sub ((signedf, bit_width), sub_index) = (sprintf "ISUB_%i_%s%i" sub_index (ss signedf) bit_width, bit_width)
            map gen_sub_sub subfield_pairs

        let result_schema_pair = bp_gec_standard_synchronous_netport_schema "i" input_subs 
        let additional_portmeta = []
        let port_name = spare_prefix + "replicate_put"
        let d22 = nbm.busrez2 ww additional_portmeta (Some false) result_schema_pair port_name 
        (("replicate", "put"), d22)

    let gen_output_port index =
        let output_subs = map (fun ((signedf, bit_width), sub_index) -> (sprintf "OSUB_%s_%i_%s%i" (roman_encode index) sub_index  (ss signedf) bit_width, bit_width)) subfield_pairs
        let schema_pair = bp_gec_standard_synchronous_netport_schema "o" output_subs
        let additional_portmeta = []
        let port_name = spare_prefix + sprintf "copy_%i_aout" index
        let d22 = nbm.busrez2 ww additional_portmeta (Some false) schema_pair port_name 
        ((sprintf "copy_%i" index, "aout"), d22) 

    let output_ports = map gen_output_port [0..arity-1]
    let structured_ports = input_port :: output_ports
    let _ = nbm.fu_end ww  // Call this after last formal added.

    // Implementation entirely missing ...
    let hx_sform0 (_, d22) lname = hx_sformal msg d22 lname

    let silly_counter = // Locals
       let counter_value = hx_logic_s ww nbm "ignore_this_counter_please" (false, 2) 
       nbm.add_seq(counter_value, ix_plus (xi_one) counter_value)
        
    let attributes = []; // att_att_t list

    let vmd = nbm.finish_definition ww attributes 
    (vmd, structured_ports) // end of replicate_really


// Replicate the provided arg n=arity times.
let bp_create_kind_canned_replicator ww fusettings msg arg arity =

    let (kind_name, overrides) =
        let ss signedf = if signedf then "S" else "U"
        let gin subfields = underscore_fwd(map (fun (signedf, w)->sprintf "%s%i" (ss signedf) w) subfields)
        (sprintf "REPLICATOR101_N%i_%s" arity (gin arg), []) 

    match g_m_bp_defined_primitive_kinds.lookup kind_name with
        | Some (existing_vmd, structured_ports) ->
            (kind_name, existing_vmd, structured_ports, overrides)

        | None ->
            // We can pass bit_width in here or leave it as a parameter to be overridden on instantiation.
            let (vmd, structured_ports) = bp_create_kind_canned_replicator_really ww msg kind_name arg arity
            g_m_bp_defined_primitive_kinds.add kind_name (vmd, structured_ports)
            mutadd fusettings.m_aux_fu_defs (kind_name, vmd)
            (kind_name, vmd, structured_ports, overrides)
    
//
//
// ---------------------------------------------------------------
//
//
//
let bp_create_kind_canned_callsite_manager_really ww msg kind_name put_spec aout_spec arity = 
    let msg = (sprintf "creating BP canned kind %s (%s) arity=%i" kind_name  msg arity)
    let ww = WF 3 "BP canned_callsite_manager" ww msg
    let genv = []
    let nbm = new nbm_t(ww, kind_name, false, genv)
    let spare_prefix = ""
    let ss signedf = if signedf then "S" else "U"
    // A callsite manager has a put and aout pair of ports to connect to the FU and then it has arity further pairs of these, one pair per supported call site.

    let putter_subfield_pairs = zipWithIndex put_spec
    let aouter_subfield_pairs = zipWithIndex aout_spec

    let fu_put_port =
        let output_subs = 
            let gen_sub_sub ((signedf, bit_width), sub_index) = (sprintf "OSUB_%i_%s%i" sub_index (ss signedf) bit_width, bit_width)
            map gen_sub_sub putter_subfield_pairs

        let result_schema_pair = bp_gec_standard_synchronous_netport_schema "o" output_subs 
        let additional_portmeta = []
        let port_name = spare_prefix + "to_fu_aout"
        let d22 = nbm.busrez2 ww additional_portmeta (Some false) result_schema_pair port_name 
        (("to_fu", "aout"), d22)

    let fu_aout_port =
        let input_subs = 
            let gen_sub_sub ((signedf, bit_width), sub_index) = (sprintf "ISUB_%i_%s%i" sub_index (ss signedf) bit_width, bit_width)
            map gen_sub_sub aouter_subfield_pairs

        let result_schema_pair = bp_gec_standard_synchronous_netport_schema "i" input_subs 
        let additional_portmeta = []
        let port_name = spare_prefix + "from_fu_ain"
        let d22 = nbm.busrez2 ww additional_portmeta (Some false) result_schema_pair port_name 
        (("from_fu", "ain"), d22)


    
    let gen_site_port index =
        let putter_subs = map (fun ((signedf, bit_width), sub_index) -> (sprintf "ISUB_%s_%i_%s%i" (roman_encode index) sub_index  (ss signedf) bit_width, bit_width)) putter_subfield_pairs
        let schema_pair = bp_gec_standard_synchronous_netport_schema "i" putter_subs
        let additional_portmeta = []
        let port_name = spare_prefix + sprintf "site_%i_put" index
        let d22 = nbm.busrez2 ww additional_portmeta (Some false) schema_pair port_name 
        let putter = ((sprintf "site_%i" index, "put"), d22) 

        let getter_subs = map (fun ((signedf, bit_width), sub_index) -> (sprintf "OSUB_%s_%i_%s%i" (roman_encode index) sub_index  (ss signedf) bit_width, bit_width)) aouter_subfield_pairs

        let schema_pair = bp_gec_standard_synchronous_netport_schema "o" getter_subs
        let additional_portmeta = []
        let port_name = spare_prefix + sprintf "site_%i_aout" index
        let d22 = nbm.busrez2 ww additional_portmeta (Some false) schema_pair port_name 
        let getter = ((sprintf "site_%i" index, "aout"), d22) 
        [ putter; getter]

    let site_ports = map gen_site_port [0..arity-1]
    let structured_ports = fu_put_port :: fu_aout_port :: list_flatten site_ports
    
    let _ = nbm.fu_end ww  // Call this after last formal added.

    // Implementation entirely missing ...
    let hx_sform0 (_, d22) lname = hx_sformal msg d22 lname

    let silly_counter = // Locals
       let counter_value = hx_logic_s ww nbm "ignore_this_counter_please" (false, 2) 
       nbm.add_seq(counter_value, ix_plus (xi_one) counter_value)
        
    let attributes = []; // att_att_t list

    let vmd = nbm.finish_definition ww attributes 
    (vmd, structured_ports) // end of canned site_manager really


// Replicate the provided arg n=arity times.
let bp_create_kind_canned_callsite_manager ww fusettings msg put_spec aout_spec arity =

    let (kind_name, overrides) =
        let ss signedf = if signedf then "S" else "U"
        let gin subfields = underscore_fwd(map (fun (signedf, w)->sprintf "%s%i" (ss signedf) w) subfields)
        (sprintf "CALLSITE_MANAGER101_%s_N%iN_%s" (gin put_spec) arity (gin aout_spec), []) 

    match g_m_bp_defined_primitive_kinds.lookup kind_name with
        | Some (existing_vmd, structured_ports) ->
            (kind_name, existing_vmd, structured_ports, overrides)

        | None ->
            // We can pass bit_width in here or leave it as a parameter to be overridden on instantiation.
            let (vmd, structured_ports) = bp_create_kind_canned_callsite_manager_really ww msg kind_name put_spec aout_spec arity
            g_m_bp_defined_primitive_kinds.add kind_name (vmd, structured_ports)
            mutadd fusettings.m_aux_fu_defs (kind_name, vmd)
            (kind_name, vmd, structured_ports, overrides)
    
//
//
// ---------------------------------------------------------------
//
// Ryan temp
let bp_create_kind_canned_two_place_buffer_really ww fusettings msg kind_name (signedf, (bit_width_o:int option)) = // If bit_width is None then make a generic with parameter override for width.
    let msg = (sprintf "rezzing %s %A %A (%s)" kind_name signedf bit_width_o msg)
    let ww = WF 3 "BP canned two_place_buffer" ww msg

    let wideness = valOf_or bit_width_o 32
    //let genv = [ ("DATA_WIDTH", xi_num wideness); ("SIGNED", xi_num(if signedf then 1 else 0))] // generic parameter environment - hardly needed?
    let genv = []
    

    let nbm = new nbm_t(ww, kind_name, false, genv) // caller ensure not defined before

    // We'll have all four put/get ports in this most verbose form, although the write_get port will normally be supressed by pragma, since it serves no use at all.
    let spare_prefix = ""
    let put =
        let schema_pair = bp_gec_standard_synchronous_netport_schema "i" [("DATA", wideness)]
        let additional_portmeta = []
        let port_name = spare_prefix + "put"
        let d22 = nbm.busrez2 ww additional_portmeta (Some false) schema_pair port_name 
        (("put"), d22) 
    let rdata_bus_name = "RDATA"
    let aout =
        let schema_pair = bp_gec_standard_synchronous_netport_schema "o" [(rdata_bus_name, wideness)]
        let additional_portmeta = []
        let port_name = spare_prefix + "aout"
        let d22 = nbm.busrez2 ww additional_portmeta (Some false) schema_pair port_name 
        (("aout"), d22) 
    
    let wideness = valOf_or bit_width_o 32            
    let structured_ports = [ put; aout; ]
    
    let _ = nbm.fu_end ww  // Call this after last formal added.

    let hx_sform0 (_, d22) lname = hx_sformal msg d22 lname
        
    let the_underlying_register0 = hx_logic_s ww nbm kind_name (signedf, valOf_or bit_width_o 32) //what is this underlying register hmmm?
    let the_underlying_register1 = hx_logic_s ww nbm kind_name (signedf, valOf_or bit_width_o 32)

    let SR0 = hx_logic_s ww nbm "SR0" (false, 1) 
    let SR1 = hx_logic_s ww nbm "SR1" (false, 1)

    let spare_ = hx_and (hx_and (hx_sform0 aout "VALID") (hx_sform0 aout "RDY")) (hx_inv SR1)
    
    //clk_enable_set0 = hx_and (hx_sform0 put "VALID") (hx_sform0 put "RDY")
    //clk_enable_set1 = hx_and(SR0)(hx_and (hx_sform0 put "VALID") (hx_sform0 put "RDY"))
    //clear0 = hx_and(hx_inv(SR1))(hx_and (hx_sform0 aout "VALID")(hx_sform0 aout "RDY"))
    //clear1 = hx_and(SR1)(hx_and (hx_sform0 aout "VALID")(hx_sform0 aout "RDY"))

    // i think the hx_sform is just for error catching and formal verification to make sure all the nets referred to actually exist.
    nbm.add_seq_g (hx_and (hx_sform0 put "VALID") (hx_sform0 put "RDY"))  (the_underlying_register0, hx_sform0 put "DATA")  //sequential assignment of l=r with guard expression. i.e. if (g) l <= r ;
    nbm.add_seq_g (hx_and (SR0) (hx_and (hx_sform0 put "VALID") (hx_sform0 put "RDY")))  (the_underlying_register1, the_underlying_register0)

    // SR0 <= clk_enable_set0 | (!clear0 && SR0);
    // SR1 <= clk_enable_set1 | (!clear1 && SR1);
    nbm.add_seq_g (hx_or(hx_and (hx_sform0 put "VALID") (hx_sform0 put "RDY"))(hx_and(hx_inv(hx_and(hx_inv(SR1))(hx_and (hx_sform0 aout "VALID")(hx_sform0 aout "RDY"))))(SR0))) (SR0, xi_one)
    nbm.add_seq_g (hx_or(hx_and(SR0)(hx_and (hx_sform0 put "VALID") (hx_sform0 put "RDY")))(hx_and(hx_inv(hx_and(SR1)(hx_and (hx_sform0 aout "VALID")(hx_sform0 aout "RDY"))))(SR1))) (SR1, xi_one)


    //MUX for aout_data
    nbm.add_comb_g (SR1) (hx_sform0 aout rdata_bus_name, the_underlying_register1)  //combinational assignment l = r;
    nbm.add_comb_g (hx_inv(SR1)) (hx_sform0 aout rdata_bus_name, the_underlying_register0)

    nbm.add_comb (hx_sform0 put "RDY", hx_or(hx_sform0 aout "RDY")(hx_inv(hx_and(SR0)(SR1))))
    nbm.add_comb (hx_sform0 aout "VALID", hx_or(SR1)(SR0)) 

    
    let attributes = []; // att_att_t list

    let vmd = nbm.finish_definition ww attributes 
    mutadd fusettings.m_aux_fu_defs (kind_name, vmd)
    (vmd, structured_ports) // End of built-in register kind.
// eof
