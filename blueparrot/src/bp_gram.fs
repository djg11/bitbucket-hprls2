//
// CBG BlueParrot - an HLS Compiler
// (C) 2023 University of Cambridge, Computer Laboratory.
//

// This file contains the abstract syntax tree definition for the input src language.

module bp_gram

open System.Numerics





type bp_diadic_op_t =
    |  BP_query
    |  BP_assign    
    |  BP_subscript
    |  EO_implies
    |  EO_logor
    |  EO_logand
    |  EO_xor
    |  EO_deqd
    |  EO_dned
    |  EO_dltd
    |  EO_dled
    |  EO_dgtd
    |  EO_dged
    |  EO_bitor
    |  EO_bitand
    |  EO_plus
    |  EO_minus
    |  EO_times
    |  EO_divide
    |  EO_dotaccess
    |  EO_lshift
    |  EO_rshift
    |  EO_bit_subscription

type bp_module_t =
    | BP_module of string * bp_decl_t list

and bp_lp_t = string * string

and bp_decl_t =
    | BP_method_export of bp_lp_t * bp_type_t * string * bp_formal_t list * bp_cmd_t
    | BP_instance of bp_lp_t * string(*kind*) * string(*instance_name*) * (*actuals*)(string * bp_exp_t) list
    | BP_memory_instance_sram of bp_lp_t * bp_type_t(*dwidth*) * string(*instance_name*) * (*actuals*)(string * bp_exp_t) list
    | BP_register_instance of bp_lp_t * (*dwidth*)bp_type_t * string(*instance_name*) * (*actuals*)(string * bp_exp_t) list

and bp_formal_t = bp_type_t * string * string  //second string is spare!

and bp_cmd_t =
    | BP_block of bp_cmd_t list
    | BP_lvardecl of bp_lp_t * bp_type_t * bp_exp_t * bp_exp_t option
    | BP_continue of bp_lp_t
    | BP_break of bp_lp_t     
    | BP_fence of  bp_lp_t * bp_exp_t
    | BP_skip
    | BP_return of bp_lp_t * bp_exp_t
    | BP_while of bp_lp_t * bp_exp_t * bp_cmd_t
    | BP_if of bp_lp_t * bp_exp_t * bp_cmd_t * bp_cmd_t option    
    | BP_for of bp_lp_t * bp_cmd_t * bp_exp_t * bp_cmd_t * bp_cmd_t
    | BP_e_as_c of bp_lp_t * bp_exp_t // Expression as statement (especially assignments).
    
and bp_type_t =
    | BP_void
    | BP_integer
    | BP_sized_int_type of string(*textual name*) * bool (*signed*) * int

and bp_exp_t =
    | BPE_num of BigInteger                 // Numeric constants
    | BPE_apply of bp_exp_t * bp_exp_t list // Function application
    | BPE_var of string                     // Variables
    | BPE_string of string                  // String literals
    | BPE_op of bp_lp_t option * bp_diadic_op_t * bp_exp_t list
    | BPE_builtin_fn of string              // Built-in function    



// End of grammar.


// eof
