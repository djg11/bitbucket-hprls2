/*
 *  $ID: $
 *
 * Bigtop.  
 * (C) 2003 DJ Greaves. All rights reserved.
 *
 */



/* cbg solos cbglish cbgeval.c */

/* $Id: cbgeval.c  Master 24 June 94 - Oct 30 96 */

#include <stdio.h>
#include <stdlib.h>
#include "cbglish.h"
#include <string.h>

#define TRC(X)
/*
 * define EVALTRACE
 */
#define SKIP(X)
#define STACKMAGICNO (12345678)


extern builder *fassoc(builder *x, builder *y);
builder *cbgeval();
builder *currentgoal;
builder *fapply();
builder *libraryfind(builder *x);
builder *argsubst();
static int evals;
builder *slocal, *sdo, *sdefine, *sxplus;




void backtrace(int a)
{
#if 0
  int *sp = &a + 2;
  printf ("Back trace...\n");
  while (*sp != (int) sp)
  {
    if (sp[-5] == STACKMAGICNO) 
    {
      builder **frame = (builder **) sp[-3];
      printf("EVAL: "); fdisplay((builder *) sp[-2]);
      if (sp[-4]) printf("   1: "); fdisplay(frame[0]);
      if (sp[-4] > 1) printf("   2: "); fdisplay(frame[-1]);
      if (sp[-4] > 2) printf("   3: "); fdisplay(frame[-2]);
    }
    sp = (int *) (*sp);
  }
#endif
}

builder *mfsetq(builder *r, builder **frame, int nargs)
{
  struct atom *a = fcar_atom(r);
  check_an_atom((builder *)a, "setq");
  a->carval = cbgeval(fcadr(r), frame, nargs);
  return (builder *) a;
}

builder *fset(builder *a, builder *v)
{
  check_an_atom(a, "set");
  m_carval(a) = v;
  return (builder *) a;
}


builder *fmap(builder *f, builder *x)
{
  if (x == nil) return nil;
  return fcons(cbgeval(fcar(x), nil, 0), fmap(f, fcdr(x)));
}


builder *cmapcar(builder *(*f)(), builder *x, builder *aux)
{
  builder *r;
  if (x == nil) return nil;
  r = (builder *) f(fcar(x), aux);
  return fcons(r, cmapcar(f, fcdr(x), aux));
}


builder *cdualmapcar(builder *(*f)(), builder *xlist, builder *ylist)
{
  builder *r;
  if (xlist == nil) return nil;
  if (ylist == nil) return cmapcar(f, xlist, nil);
  r = (builder *) f(fcar(xlist), fcar(ylist));
  return fcons(r, cdualmapcar(f, fcdr(xlist), fcdr(ylist)));
}


builder *cmapcar_tails(builder *(*f)(), builder *x, builder *aux, builder *tails)
{
  builder *r;
  if (x == nil) return tails;
  r = (builder *) f(fcar(x), aux);
  return fcons(r, cmapcar_tails(f, fcdr(x), aux, tails));
}


builder *fatomp(builder *x)
{
  if (x == nil || m_key(x) != ss_pair) return shasht;
  return nil;
}

builder *fnot(builder *x)
{
  if (x == nil) return shasht;
  return nil;
}

builder *mfand(builder *r, builder **frame, int nargs)
{
  while (r)
  {
    if (cbgeval(fcar(r), frame, nargs) == nil) return nil;
    r = fcdr(r);
  }
  return shasht;
}

builder *mfor(builder *r, builder **frame, int nargs)
{
  while (r)
  {
    if (cbgeval(fcar(r), frame, nargs)) return shasht;
    r = fcdr(r);
  }
  return nil;
}

builder *feq(builder *x, builder *y)
{
  if (x == y) return shasht;
  return nil;
}

builder *fequal(builder *x, builder *y)
{
  if (fatomp(x))
  {
    if (x == y) return shasht;
    return nil;
  }
  if (fpairp(y) == nil) return nil;
  if (fequal(fcar(x), fcar(y)) && fequal(fcdr(x), fcdr(y))) return shasht;
  return nil;
}

builder *fpairp(builder *x)
{
  // Working on the basis that all heap pointers are -ve ...
  if (x != nil && x->key == ss_pair) return shasht;
  return nil;
}

builder *fnullp(builder *x)
{
  if (x == nil) return shasht;
  return nil;
}

builder *fstringp(builder *x)
{
  if (x && x->key == ss_string) return shasht;
  return nil;
}

builder *fdstringp(builder *x)
{
  if (x && x->key == ss_dstring) return shasht;
  return nil;
}


builder *fnumberp(builder *x)
{
  if (x && x->key == ss_number) return shasht;
  return nil;
}

builder *ffsymbolp(builder *x)
{
  if (x && x->key == ss_symbol) return shasht;
  return nil;
}

builder *fevenp(builder *x)
{
  if (x == nil || x->key != ss_number) 
    { 
      cbgerror(-1, "even? not on a number", x);
    }
  else if ((m_ncdr(x) & 1) == 0) return shasht;
  return nil;
}

builder *foddp(builder *x)
{
  if (x == nil || x->key != ss_number) 
    { 
      cbgerror(-1, "odd? not on a number", x);
    }
  else if ((m_ncdr(x) & 1) == 1) return shasht;
  return nil;
}

builder *fpositivep(builder *x)
{
  if (x == nil || x->key != ss_number) 
    { 
      cbgerror(-1, "positive? not on a number", x);
    }
  else if (m_ncdr(x) >= 0) return shasht;
  return nil;
}

builder *fnegativep(builder *x)
{
  if (x == nil || x->key != ss_number) 
    { 
      cbgerror(-1, "negative? not on a number", x);
    }
  else if (m_ncdr(x) < 0) return shasht;
  return nil;
}

builder *fdivide(builder *x, builder *y)
{
  if (x == nil || x->key != ss_number ||
      y == nil || y->key != ss_number)
    { 
      cbgerror(-2, "divide not on a number", x, y);
    }
  if ( m_ncdr(y) == 0) cbgerror(0, "divide by zero", y);
  return mknumber(m_ncdr(x) / m_ncdr(y));
}


builder *fabso(builder *x)
{
  if (x == nil || x->key != ss_number)
    { 
      cbgerror(-1, "abs not on a number", x);
    }
  if (m_ncdr(x) >= 0) return x;
  return mknumber(0 - m_ncdr(x));
}

builder *fplus(builder *x, builder *y)
{
  if (x == nil || x->key != ss_number ||
      y == nil || y->key != ss_number)
    { 
      cbgerror(-2, "plus not on a number", x, y);
      return NULL;
    }
  else return mknumber(m_ncdr(x) + m_ncdr(y));
}

builder *ffmax(builder *x, builder *y)
{
  if (x == nil || x->key != ss_number ||
      y == nil || y->key != ss_number)
    { 
      cbgerror(-2, "max not on a number", x, y);
          return NULL;
    }
  else return (m_ncdr(x) > m_ncdr(y)) ? x: y;
}

builder *ffmin(builder *x, builder *y)
{
  if (x == nil || x->key != ss_number ||
      y == nil || y->key != ss_number)
    { 
      cbgerror(-2, "min not on a number", x, y);
      return NULL;
    }
  else return (m_ncdr(x) < m_ncdr(y)) ? x: y;
}

builder *fsign(builder *x)
{
  int i, o;
  if (x == nil || x->key != ss_number)
  { 
    cbgerror(-1, "sign not on a number", x);
    return NULL;
  }
  i = m_ncdr(x);
  o = 0;
  if (i>0) o = 1;
  else if (i<0) o = -1;
  return mknumber(o);
}


builder *ftimes(builder *x, builder *y)
{
  if (x == nil || x->key != ss_number ||
      y == nil || y->key != ss_number)
    { 
      cbgerror(-2, "times not on a number", x, y);
      return NULL;
    }
  else return mknumber(m_ncdr(x) * m_ncdr(y));
}


builder *fminus(builder *x, builder *y)
{
  if (x == nil || x->key != ss_number ||
      y == nil || y->key != ss_number)
    { 
      cbgerror(-2, "minus not on a number", x, y);
      return NULL;
    }
  return mknumber(m_ncdr(x) - m_ncdr(y));
}

builder *fmodulo(builder *x, builder *y)
{
  if (x == nil || x->key != ss_number ||
      y == nil || y->key != ss_number)
    { 
      cbgerror(-2, "modulo not on a number", x, y);
      return NULL;
    }
  if ( m_ncdr(y) == 0) cbgerror(-1, "modulo by zero", y);
  return mknumber(m_ncdr(x) % m_ncdr(y));
}

builder *divide(builder *x, builder *y)
{
  if (x == nil || x->key != ss_number ||
      y == nil || y->key != ss_number)
    { 
      cbgerror(-2, "divide not on a number", x, y);
      return NULL;
    }
  else return mknumber(m_ncdr(x) / m_ncdr(y));
}

builder *fltp(builder *x, builder *y)
{
  if (x == nil || x->key != ss_number ||
      y == nil || y->key != ss_number)
    { 
      cbgerror(-2, "< not on a number", x, y);
      return NULL;
    }
  if (m_ncdr(x) < m_ncdr(y)) return shasht;
  return nil;
}

builder *flep(builder *x, builder *y)
{
  if (x == nil || x->key != ss_number ||
      y == nil || y->key != ss_number)
    { 
      cbgerror(-2, "<= not on a number", x, y);
      return NULL;
    }
  if (m_ncdr(x) <= m_ncdr(y)) return shasht;
  return nil;
}

builder *fgtp(builder *x, builder *y)
{
  if (x == nil || x->key != ss_number ||
      y == nil || y->key != ss_number)
    { 
      cbgerror(-2,"> not on a number", x, y);
      return NULL;
    }
  if (m_ncdr(x) > m_ncdr(y)) return shasht;
  return nil;
}

builder *fgep(builder *x, builder *y)
{
  if (x == nil || x->key != ss_number ||
      y == nil || y->key != ss_number)
    { 
      cbgerror(-2, ">= not on a number", x, y);
      return NULL;
    }
  if (m_ncdr(x) >= m_ncdr(y)) return shasht;
  return nil;
}

int lisp_num_lim(builder *x, char *caser, int limit)
{
  int r;
  if (x == nil || x->key != ss_number)
  { 
    errorprintf("arg for %s ", caser);
    cbgerror(-1, "not a number", x);
  }
  r = m_ncdr(x);
  if (r > limit)
  {
    cbgerror(0, "%s out of range %i/%i", caser, r, limit);
  }
  return r;
}

builder *fappend(builder *x, builder *y)
{
  if (y == nil) return x;
  if (x == nil) return y;
  return fcons(fcar(x), fappend(fcdr(x), y));
}

builder *freverse1(builder * r, builder *c)
{
  if (fatomp(r)) return c;
  return freverse1(fcdr(r), fcons(fcar(r), c));
}

builder *freverse(builder * r)
{
  return freverse1(r, nil);
}


builder *mftimes(builder *x, builder **frame, int nargs)
{
  int r = 1;
  while (x)
  {
    builder *y = cbgeval(fcar(x), frame, nargs);
    if (y == nil || y->key != ss_number)
    { 
      cbgerror(-1, "times not on a number", y);
    }
  x = fcdr(x);
  r = r * m_ncdr(y);
  }
  return mknumber(r);
}

builder *mfplus(builder *x, builder **frame, int nargs)
{
  int r = 0;
  while (x)
  {
    builder *y = cbgeval(fcar(x), frame, nargs);
    if (y == nil || y->key != ss_number)
    { 
      cbgerror(-1, "plus not on a number", y);
    }
  x = fcdr(x);
  r = r + m_ncdr(y);
  }
  return mknumber(r);
}


/* mfmdefine:
   The built-in macro for defining user defined macros
*/
builder *mfmdefine(builder *rest)
{
  builder *proto = ffirst(rest);
  builder *bodies = fcdr(rest);
  builder *name = ffirst(proto);
  builder *args = fcdr(proto);
  if (length(args) != 1) cbgerror(0, "one arg only desired for a macro define");
  bodies = argsubst(bodies, args);
  fputprop(name, smacro, bodies);  
  return name;
}

builder *setdefinelv(builder *a, builder *val, builder **frame, int nargs, char *s)
{
  check_an_atom(a, s);
  val = cbgeval(val, frame, nargs);
  if (m_ncar(a) != ss_argument) cbgerror(-1, "define not applied to an arg or local", a);
  frame[-m_ncdr(a)] = val;
  return val;
}
 
/* mfdefine:
   The built-in macro for defining user defined functions and
   setting the values of local variables.
*/ 

builder *mfdefine(builder *rest, builder **frame, int nargs)
{
  builder *proto = ffirst(rest);  /* Here we assign a value to a local var */
  if (fatomp(proto))
  {
    builder *a = (builder *) proto;
    if (m_ncar(a) != ss_argument) cbgerror(-1, "define not applied to an arg or local", a);
    frame[-m_ncdr(a)] = cbgeval(fsecond(rest), frame, nargs);
    return a;
  }
  else
  {
    builder *name = ffirst(proto);
    builder *bodies = fcdr(rest);
    builder *args = fcdr(proto);
    builder *newbodies = argsubst(bodies, args);
    fputprop(name, flength(args), fcons(slambda, fcons(args, newbodies)));
    return name;
  }
}

/* Routine to textually replace the dummy args of a routine and
   also the local variables (introduced by do, define etc)
   with arg pointer values.
*/
static builder *apinch;
builder *argsubst1();
builder *argsubst(builder *bod, builder *args)
{ 
  apinch = args;
  return argsubst1(bod);
}

builder *argsubst1(builder *bod)
{
  if (bod == nil || apinch == nil) return bod;
  if (fatomp(bod))
  {
    int i = 0;
    builder *aa = apinch;
    while(aa)
    {
      if (fcar(aa) == bod)
      {
        return fcons(ss_argument, i);
      }
      i ++;
      aa = fcdr(aa);
    }
    return bod;
  }
  else
  {
    /*    if (fcar(bod) == squote) return bod;
     */
    if (fcar(bod) == sdefine)  /* Add local vars on to arg list */
    {
      apinch = fappend(apinch, list1(fcadr(bod)));
    }   
    if (fcar(bod) == sdo)  /* Also the variables introduced by do */
    {
      builder *p = fcadr(bod); /* Get definition list from a do */
      while(p)
      {
        apinch = fappend(apinch, list1(fcaar(p)));
        p = fcdr(p);
      }
    }
    return fcons(argsubst1(fcar(bod)), argsubst1(fcdr(bod)));
  }
}

builder *fcar(builder *x)
{
  check_not_an_atom(x, "car");
  return x->car;
}

builder *fcdr(builder *x)
{
  check_not_an_atom(x, "cdr");
  return x->cdr;
}

builder *mfquote(builder *rest)
{
  return fcar(rest);
}

builder *mfprog(builder * r, builder **frame, int nargs)
{
  builder *rc = nil;
  while (r)
  {
    rc = cbgeval(fcar(r ), frame, nargs);
    r = fcdr(r);
  }
  return rc;
}

builder *mfcond(builder * r, builder **frame, int nargs)
{
  while (r)
  {
    if (cbgeval(fcaar(r), frame, nargs))
        return mfprog(fcdar(r), frame, nargs);
    r = fcdr(r);
  }
  return nil;
}

/* return nth item of a list - if present */
builder *foptidx(builder * x, int n)
{
  while (x)
  {
    check_not_an_atom(x, "optidx");
    if (--n == 0) return fcar(x);
    x = fcdr(x);
  }
  return nil;  /* List too short */
} 

builder *mfif(builder * r, builder **frame, int nargs)
{
  builder *cond = cbgeval(ffirst(r), frame, nargs);
  builder *bod;
  if (cond) bod = fsecond(r); else bod = foptidx(r, 3);
  if (bod) return cbgeval(bod, frame, nargs);
  return nil;
}

/* The looping constructor 'do':
    do ((variable init step) ...) (test expression) command ... 
*/
builder *mfdo(builder * r, builder **frame, int nargs)
{
  builder *defs = foptidx(r, 1);
  builder *exit = foptidx(r, 2);
  builder *commands = fcddr(r);
  builder *p;
  /* First initialise the variables */
  p = defs;
  while(p)
  {
    builder *var = fcaar(p);
    builder *init = foptidx(fcar(p), 2);
    setdefinelv(var, init, frame, nargs, "do initialisation");
    p = fcdr(p);
  }
  while(cbgeval(exit, frame, nargs))
  {
    mfprog(commands, frame, nargs);
    p = defs;
    while(p)
    {
      builder *var = fcaar(p);
      builder *step = foptidx(fcar(p), 3);
      setdefinelv(var, list3(sxplus, var, step), frame, nargs, "do inc");
      p = fcdr(p);
    }
  }
  return nil;
}

/* prim:
   Routine to set the properties of a built-in lisp/scheme primitive function or macro.
   A symbol can be one or the other, but not both, and multiple
   definitions of a function, with disjoint arities, are possible.
*/

void prim(key_t code, char *name, builder *(*f)())
{
  builder *p = (builder *) str_to_atom(name, 0);
  m_key(p) = code;
  m_fb_carval(p) = (builder *) f;
}




builder *feval(builder *expr)
{
  builder *a1;
  evals = 0;
  return cbgeval(expr, &a1, 0);
}


builder *cbgeval(builder *x, builder **frame, int nargs)
{
  builder *usermacro;
  int preps = 20;
  builder *arity, *f;
  if (x == 0) return nil;

  if (evals++ > 100000) cbgerror(-1, "too much recursion", x);


#ifdef EVALTRACE
  printf("Eval of %x %x :", x, x->key);
  fprintl(x);
  printf("\n");
#endif

  /* Code to evaluate an atom */
  if (m_key(x) < 0)  /* An atom has its key field -ve */
  { 
    if (x->key == ss_argument)
    {
      int a = m_ncdr(x);
      TRC(printf("Supplying arg/lv %i ", a); fdisplay(frame[-a]));
      return frame[-a];
    }
    if (x == shasht) return x;
    if (x->key == ss_number) return x;
    if (x->key == ss_string) return x;
    
    if (x->key == ss_symbol)
    { 
      /* Check for dummy parameter which needs be substituted */

      return m_carval(x);  /* Otherwise return its value */
    }
    return sbuiltin;
  }

  /* Code for evaluating a function application (ie a list where the
     first is the function.
   */

  f = fcar(x);  /* Get the function */
   
  if (f == nil) 
  {
    cbgerror(0, "using nil as a function");
    return nil;
  }

  /* Check all of the possible built-in special cases in the following
     switch statement
  */
  if (m_key(f) < 0) switch(m_key(f))
  {
    case ss_nmacro:
    {
      builder *(*g)();
      TRC(printf("Native macro %s\n", f->cdr.textual));
      g = m_fb_carval(f);
      return g(fcdr(x), frame, nargs);
    }

    case ss_argument:
      f = cbgeval(f, frame, nargs);
      break;

    case s_native0:
    {
      builder *(*g)() = m_fb_carval(f);
      return g();
    }

    case s_native1:
    {
      builder *(*g)() = m_fb_carval(f);
      return g(cbgeval(fcadr(x), frame, nargs));
    }


    case s_native2:
    {
      builder *(*g)() = m_fb_carval(f);
      return g(cbgeval(fsecond(x), frame, nargs),
               cbgeval(fthird(x), frame, nargs)
              );
    }

    case s_native3:
    {
      builder *(*g)() = m_fb_carval(f);
      return g(cbgeval(fcadr(x), frame, nargs),
               cbgeval(fthird(x), frame, nargs),
               cbgeval(ffourth(x), frame, nargs)
              );
    }

    case s_native6:
    {
      builder *(*g)() = m_fb_carval(f);
      return g(cbgeval(foptidx(x, 2), frame, nargs),
               cbgeval(foptidx(x, 3), frame, nargs),
               cbgeval(foptidx(x, 4), frame, nargs),
               cbgeval(foptidx(x, 5), frame, nargs),
               cbgeval(foptidx(x, 6), frame, nargs),
               cbgeval(foptidx(x, 7), frame, nargs)
              );
    }

    case ss_symbol:
      break;

    default:
     fprintl(f);
     cbgerror(cbge_fatal, "unevaluatable special atom (%x)\n", m_ncar(f));
     return nil;
  }
  else /* first item is a pair */
  {
    cbgerror(-1, "list used as function", f);
    return nil;
  }

  if (fatomp(f)) usermacro = fgetprop(f, smacro);  /* Is it a user defined macro ? */

  if (usermacro)
  {
    builder *arg = fcdr(x);
    /*    builder *a, *b, *c, *d, *e, *f, *g, *h, *i, *j; / * Space for stack frame */
    TRC(printf("User defined macro called\n"));
    return mfprog(usermacro, &arg, 1);
  }

  arity = flength(fcdr(x));
  /* Prepare function for evaluation */
  while (1)
  {
    if (preps -- < 0) cbgerror(0, "Bad function ", f);
    SKIP(printf("Preparing "); print(f); printf("\n"));

    if (fcar(f) == slambda) return fapply(f, fcdr(x), frame, nargs);
    if (m_ncar(f) == ss_symbol)
    { 
      builder *r = fgetprop(f, arity);
      if (r == nil)
      {
        cbgerror(0, "Function %s not defined for arity %i\n", atom_to_str(f),
                     atom_to_int(arity));
        return nil;
      }
      f = fcdr(r);
      continue;
    }

    if (m_ncar(f) == ss_argument) 
    {
      f = cbgeval(f, frame, nargs);
      continue;
    }    
    break;
  }
  fprintl(f); cbgerror(0, "undefined function", f);
  return nil;
}

char fname[100];

builder *libraryfind(builder *x)
{
  FILE *fd;
  char * name =  m_ccdr(x);
  strcpy(fname, name);
  strcat(fname, ".l");
  fd = fopen(fname, "r");
  if (fd == NULL)
  {
    printf("Cant open %s\n", fname);
    return NULL;
  }
/*
 *  while(feof(fd)==0) rep(fd, stdout);
 */
  fclose(fd);
  return x;
}

builder *fapply(builder *fn, builder *rargs, builder **frame, int nargs)
//      builder *fn;
//      builder *rargs;
//      builder **frame;
{
  builder *a1;
  builder **argp = &(a1);
  builder *bodies = fcdr(fcdr(fn));
  builder *rc = nil;
  int newargs = 0;


#ifdef EVALTRACE
  printf("Apply "); fprintl(fn);
  printf(" to "); fprintl(rargs); 
  printf("\n");
#endif
  while(rargs)  /* Evaluate each of the args in this loop */
  {
    *argp-- = cbgeval(fcar(rargs), frame, nargs);
    newargs ++;
    rargs = fcdr(rargs);
  }
  while(bodies)  /* Execute each of the bodies in this loop */
  {
    TRC(printf("First body "); fdisplay(fcar(bodies)));
    rc = cbgeval(fcar(bodies), &(a1), newargs);
    bodies = fcdr(bodies);
  }
  return rc;
}


builder *fcadar(builder *x) { return fcar(fcdr(fcar(x))); }

builder *fcaar(builder *x) { return fcar(fcar(x)); }
builder *fcadr(builder *x) { return fcar(fcdr(x)); }
builder *fcaddr(builder *x) { return fcar(fcdr(fcdr(x))); }
builder *fcadddr(builder *x) { return fcar(fcdr(fcdr(fcdr(x)))); }
builder *fcadddddr(builder *x) { return fcar(fcdr(fcdr(fcdr(fcdr(fcdr(x)))))); }
builder *fcaddddr(builder *x) { return fcar(fcdr(fcdr(fcdr(fcdr(x))))); }
builder *fcdar(builder *x) { return fcdr(fcar(x)); }
builder *fcddr(builder *x) { return fcdr(fcdr(x)); }
builder *fcdddr(builder *x) { return fcdr(fcdr(fcdr((x)))); }

builder * cbgeval_list(builder *k, builder **frame, int nargs)
{
  if (k == nil) return k;
  return fcons(cbgeval(fcar(k), frame, nargs),
               cbgeval_list(fcdr(k), frame, nargs));
}

builder *mfexit(builder *k, builder **frame, int nargs)
{
  k = cbgeval_list(k, frame, nargs);
  cbgerror(-1, "exit", k);
  exit(1);
}

/* A nop function used also for (local x y ..) at run time */
builder *fnop()
{
  return nil;
}

 

builder *flist(builder *r, builder **frame, int nargs)
{
  if (r == nil) return nil;
  return fcons(cbgeval(fcar(r), frame, nargs), flist(fcdr(r), frame, nargs));
}

  

/* initialisation code for this file 
*/
void define_lisp_prims()
{

  smacro = str_to_atom("$macro");
  slocal = str_to_atom("local");
  sdo = str_to_atom("do");
  sxplus = str_to_atom("+");
  sdefine = str_to_atom("define");
  prim(ss_nmacro, "local", fnop);
  prim(s_native2, "set", fset);
  prim(ss_nmacro, "setq", mfsetq);
  prim(ss_nmacro, "or", mfor);
  prim(ss_nmacro, "and", mfand);
  prim(ss_nmacro, "exit", mfexit);
  prim(s_native2, "append", fappend);
  prim(s_native2, ">", fgtp);
  prim(s_native2, ">=", fgep);
  prim(s_native2, "<", fltp);
  prim(s_native2, "<=", flep);
  prim(s_native2, "map", fmap);
  prim(s_native1, "atomp", fatomp);
  prim(s_native2, "eq", feq);
  prim(s_native1, "eval", feval);
  prim(s_native2, "equal", fequal);
  prim(s_native1, "pairp", fpairp);
  prim(s_native2, "plus", fplus);
  prim(s_native1, "numberp", fnumberp);
  prim(s_native1, "symbolp", ffsymbolp);
  prim(s_native1, "evenp", fevenp);
  prim(s_native1, "oddp", foddp);
  prim(s_native1, "sign", fsign);
  prim(s_native1, "reverse", freverse);
  prim(s_native1, "positivep", fpositivep);
  prim(s_native1, "negativep", fnegativep);
  prim(s_native2, "divide", fdivide);
  prim(s_native1, "display", fdisplay);
  prim(s_native1, "abs", fabso);
  prim(s_native2, "-", fminus);
  prim(s_native2, "minus", fminus);
  prim(s_native2, "plus", fplus);
  prim(s_native2, "modulo", fmodulo);
  prim(ss_nmacro, "mdefine", mfmdefine);
  prim(ss_nmacro, "cond", mfcond);
  prim(ss_nmacro, "setq", mfsetq);
  prim(ss_nmacro, "define", mfdefine);
  prim(ss_nmacro, "do", mfdo);
  prim(ss_nmacro, "if", mfif);
  prim(ss_nmacro, "begin", mfprog);

  prim(ss_nmacro, "quote", mfquote);
  prim(s_native3, "putprop", fputprop);
  prim(s_native2, "getprop", fgetprop);
  prim(s_native2, "/", fdivide);
  prim(ss_nmacro, "*", mftimes);
  prim(ss_nmacro, "star", mftimes);
  prim(ss_nmacro, "+", mfplus);
  prim(s_native1, "null", fnullp);
  prim(s_native1, "car", fcar);
  prim(s_native1, "cdr", fcdr);
  prim(s_native1, "cadr", fcadr);
  prim(s_native1, "cddr", fcddr);
  prim(s_native1, "not", fnot);
  prim(s_native1, "cdar", fcdar);
  prim(s_native1, "third", fcaddr);
  prim(s_native1, "second", fcadr);
  prim(s_native1, "first", fcar);
  prim(s_native1, "caar", fcaar);
  prim(s_native2, "cons", fcons);
  prim(ss_nmacro, "list", flist);
  prim(s_native0, "read", firead);
  prim(s_native1, "cadar", fcadar);
}


/* end of cbg eval.c */
