/*
// 
// HPR L/S Orangepath Parser for BlueParrot V0 experimental language
// 
// (C) 2023 DJ Greaves, University of Cambridge Computer Laboratory. All rights reserved.
// No avoidance of liability for infringements, loss or failures is warranted in this code.
// Not for commercial use without license.
//
 */



%{
#include "cbglish.h"

#define BP_LINEPOINT(X) add_linepoint("BP_linepoint", X)
#define ZNOTE  builder *zz = add_linepoint0();
#define BP_LINEPOINTZ(_) zz


int yyerror (const char *s);
extern builder *g_ast_parsed;

#define CANNED_TYPE(X)  ( yyvsp[0].auxval )

%}


%union {
	builder *auxval;
       }

%type <auxval> bp_module bp_module_list bp_decl_list prog bp_statement_list 
%type <auxval> exp exp1 exp2 exp3 exp4 exp5 exp6 exp7 exp8 exp9 exp10 exp11 exp12 exp13 exp13a bp_statement exp_comma_list sd_id_comma_list bp_instance_actuals bp_builtin_function 

%type <auxval> bp_decl bp_type bp_enumeration_type  bp_formals bp_formal_item_list 





%%


prog:
  bp_module_list
 	{
	builder *r = $1;
	g_ast_parsed = LISTCONS(r, g_ast_parsed);
	//printf("BT: Add top level item\n");
	//g_ast_parsed = LISTCONS(TREE0("sentinel"), g_ast_parsed);	
	g_ast_parsed = freverse(g_ast_parsed);
        $$ = (void *) 0;
	}
;

bp_module_list	:	/* nothing */
	{ $$ =	LISTEND(0);
	}
| bp_module bp_module_list 
	{
	//printf("BT: Add module to module_list. Was %i length.\n", length($2));
	$$ = LISTCONS($1, $2);
	}
;

bp_module: s_module sd_id ss_lpar ss_rpar ss_lsect bp_decl_list ss_rsect
 	{ ZNOTE;
	$$ = TREE4("BPmodule", BP_LINEPOINTZ(0), TREE1("module_name", $2), TREE1("module_args", 0), TREE1("module_body", $6));
	}
;

bp_decl_list	:	/* nothing */
	{ $$ =	LISTEND(0);
	}
| bp_decl bp_decl_list 
	{
	   $$ = LISTCONS($1, $2);
	}
;

bp_formal_item_list:
	/* nothing */	{ $$ =
	LISTEND(0);
	}
| bp_type sd_id ss_comma bp_formal_item_list
	{ $$ =
	LISTCONS(TREE2("BP_fp", $1, $2), $4);
	}

| bp_type sd_id
	{ $$ =
	LISTCONS(TREE2("BP_fp", $1, $2), LISTEND(0));
	}
;



bp_formals:
	ss_lpar bp_formal_item_list ss_rpar { $$ = 
	TREE1("BP_formal_lst", $2);
	}

|	ss_lpar bp_formal_item_list ss_rpar ss_colon bp_type { $$ =
	TREE2("BP_function", $2, $5);
	}
;


bp_instance_actuals:
	/* nothing */
	{
	$$ = TREE0("None"); 
	}

| ss_lbra exp_comma_list ss_rbra { $$ = TREE1("Bracketed", $2); }
| ss_lpar exp_comma_list ss_rpar { $$ = TREE1("Rounded", $2); }
;






bp_decl:
   s_instance sd_id ss_colon sd_id bp_instance_actuals ss_semicolon { ZNOTE;
	$$ = TREE4("BP_instance", BP_LINEPOINTZ(0), TREE1("instance_kind", $2), TREE1("instance_name", $4), TREE1("instance_actuals", $5)); 
	}

// Only sram supported for now
|  s_memory s_sram bp_type ss_colon sd_id bp_instance_actuals ss_semicolon { ZNOTE;
	$$ = TREE4("BP_memory_instance_sram", BP_LINEPOINTZ(0), TREE1("dwidth", $3), TREE1("instance_name", $5), TREE1("instance_actuals", $6)); 
	}

|  s_register bp_type ss_colon sd_id bp_instance_actuals ss_semicolon { ZNOTE;
	$$ = TREE4("BP_register_instance", BP_LINEPOINTZ(0), TREE1("dwidth", $2), TREE1("instance_name", $4), TREE1("instance_actuals", $5)); 
	}

// Externally callable method
| s_export bp_type sd_id bp_formals ss_lsect bp_statement_list ss_rsect ss_semicolon { ZNOTE;
	$$ = TREE5("BP_method_export", BP_LINEPOINTZ(0), TREE1("rwidth", $2), TREE1("name", $3), TREE1("formals", $4), TREE1("body", $6)); 
	}


// Local rule/behaviour ... missing
;


bp_builtin_function:
	s_MIN { $$ = TREE1("BP_builtin_fn", YYLEAF("MIN")); }
|	s_MAX { $$ = TREE1("BP_builtin_fn", YYLEAF("MAX")); }	
;

bp_statement_list:
	/* nothing */
	{ $$ =
	LISTEND(0);
	}
| bp_statement bp_statement_list
	{ $$ =
	LISTCONS($1, $2);
	}
;
  

//bp_using_list:
//	/* nothing */
//	{ $$ = LISTEND(0);
//	}
//| s_using bp_statement ss_semicolon bp_using_list /* Not an action really */
//	{ $$ =
//	LISTCONS($2, $4);
//	}
//;
  




bp_statement:
// Variable declartion, no assignment.
 bp_type sd_id_comma_list ss_semicolon  { ZNOTE; $$ = 
 	TREE4("BP_lvardecl", BP_LINEPOINTZ(0), $1, $2, TREE0("None"));
	}

// Declaration with assignment. Cannot list assignments here! TODO.
| bp_type sd_id_comma_list ss_equals exp ss_semicolon  { ZNOTE; $$ = 
  	TREE4("BP_lvardecl", BP_LINEPOINTZ(0), $1, $2, TREE1("Some", $4));
	}

|  exp { ZNOTE; $$ = 
	TREE2("BP_e_as_c", BP_LINEPOINTZ(0), $1);
	}


//#| s_for s_each s_in exp bp_statement ss_semicolon { $$ = 
//#	BP_LINEPOINT(TREE2("BP_foreach", $4, $5));
//#	}

| s_while exp bp_statement { ZNOTE; $$ = 
	TREE3("BP_while", BP_LINEPOINTZ(0), $2, $3);
	}

| s_for ss_lpar bp_statement ss_semicolon exp ss_semicolon bp_statement ss_rpar bp_statement{ ZNOTE; $$ = 
	TREE5("BP_for", BP_LINEPOINTZ(0), $3, $5, $7, $9);
	}

| s_fence exp { ZNOTE; $$ = 
	TREE2("BP_fence", BP_LINEPOINTZ(0), $2);
	}

| s_continue exp bp_statement { ZNOTE; $$ = 
	TREE1("BP_continue", BP_LINEPOINTZ(0));
	}

| s_break { ZNOTE; $$ = 
	TREE1("BP_break", BP_LINEPOINTZ(0));
	}

| s_pragma exp { ZNOTE; $$ = 
	TREE2("BP_pragma", BP_LINEPOINTZ(0), $2);
	}


| s_if ss_lpar exp ss_rpar bp_statement { ZNOTE; $$ = 
	TREE4("BP_if", BP_LINEPOINTZ(0), $3, $5, TREE0("None"));
	}


| s_if ss_lpar exp ss_rpar bp_statement s_else bp_statement  { ZNOTE; $$ = 
	TREE4("BP_if", BP_LINEPOINTZ(0), $3, $5, TREE1("Some", $7));
	}


| ss_semicolon { ZNOTE; $$ = 
  	TREE1("BP_skip", BP_LINEPOINTZ(0));
	}

| s_return exp ss_semicolon { ZNOTE; $$ = 
	TREE2("BP_return", BP_LINEPOINTZ(0), $2);
	}

| ss_lsect bp_statement_list ss_rsect { $$ = 
	TREE1("BP_block", $2);
	}


| exp ss_equals exp ss_semicolon  { ZNOTE; $$ = 
	TREE4("BP_assign", YYLEAF("MM_assign"), BP_LINEPOINTZ(0), $1, $3);
	}

| exp ss_lshift_assign exp ss_semicolon  { ZNOTE; $$ = 
	TREE4("BP_assign", YYLEAF("MM_lshift_assign"), BP_LINEPOINTZ(0), $1, $3);
	}

| exp ss_rshift_assign exp ss_semicolon  { ZNOTE; $$ = 
	TREE4("BP_assign", YYLEAF("MM_rshift_assign"), BP_LINEPOINTZ(0), $1, $3);
	}

| exp ss_plusequals exp ss_semicolon  { ZNOTE; $$ = 
	TREE4("BP_assign", YYLEAF("MM_plus_assign"), BP_LINEPOINTZ(0), $1, $3);
	}

| exp ss_minusequals exp ss_semicolon  { ZNOTE; $$ = 
	TREE4("BP_assign", YYLEAF("MM_minus_assign"), BP_LINEPOINTZ(0), $1, $3);
	}

| exp ss_andequals exp ss_semicolon  { ZNOTE; $$ = 
	TREE4("BP_assign", YYLEAF("MM_bitand_assign"), BP_LINEPOINTZ(0), $1, $3);
	}

| exp ss_orequals exp ss_semicolon  { ZNOTE; $$ = 
	TREE4("BP_assign", YYLEAF("MM_bitor_assign"), BP_LINEPOINTZ(0), $1, $3);
	}
| exp ss_xorequals exp ss_semicolon  { ZNOTE; $$ = 
	TREE4("BP_assign", YYLEAF("MM_xor_assign"), BP_LINEPOINTZ(0), $1, $3);
	}

;


// 	(yyval.auxval) = LISTCONS((yyvsp[-1].auxval), (yyvsp[0].auxval));
 
bp_type:
  bp_enumeration_type { $$ = $1; }

| s_void    { $$ = CANNED_TYPE(0); }
| s_double  { $$ = TREE1("BP_real", "double"); }
| s_float   { $$ = TREE1("BP_real", "float"); }
| s_uint1_t { $$ = CANNED_TYPE(0); }
| s_uint2_t { $$ = CANNED_TYPE(0); }
| s_uint3_t { $$ = CANNED_TYPE(0); }
| s_uint4_t { $$ = CANNED_TYPE(0); }
| s_uint5_t { $$ = CANNED_TYPE(0); }
| s_uint6_t { $$ = CANNED_TYPE(0); }
| s_uint7_t { $$ = CANNED_TYPE(0); }
| s_uint8_t { $$ = CANNED_TYPE(0); }
| s_uint9_t { $$ = CANNED_TYPE(0); }
| s_uint10_t { $$ = CANNED_TYPE(0); }
| s_uint11_t { $$ = CANNED_TYPE(0); }
| s_uint12_t { $$ = CANNED_TYPE(0); }
| s_uint13_t { $$ = CANNED_TYPE(0); }
| s_uint14_t { $$ = CANNED_TYPE(0); }
| s_uint15_t { $$ = CANNED_TYPE(0); }
| s_uint16_t { $$ = CANNED_TYPE(0); }
| s_uint17_t { $$ = CANNED_TYPE(0); }
| s_uint18_t { $$ = CANNED_TYPE(0); }
| s_uint19_t { $$ = CANNED_TYPE(0); }
| s_uint20_t { $$ = CANNED_TYPE(0); }
| s_uint21_t { $$ = CANNED_TYPE(0); }
| s_uint22_t { $$ = CANNED_TYPE(0); }
| s_uint23_t { $$ = CANNED_TYPE(0); }
| s_uint24_t { $$ = CANNED_TYPE(0); }
| s_uint25_t { $$ = CANNED_TYPE(0); }
| s_uint26_t { $$ = CANNED_TYPE(0); }
| s_uint27_t { $$ = CANNED_TYPE(0); }
| s_uint28_t { $$ = CANNED_TYPE(0); }
| s_uint29_t { $$ = CANNED_TYPE(0); }
| s_uint30_t { $$ = CANNED_TYPE(0); }
| s_uint31_t { $$ = CANNED_TYPE(0); }
| s_uint32_t { $$ = CANNED_TYPE(0); }
| s_uint33_t { $$ = CANNED_TYPE(0); }
| s_uint34_t { $$ = CANNED_TYPE(0); }
| s_uint35_t { $$ = CANNED_TYPE(0); }
| s_uint36_t { $$ = CANNED_TYPE(0); }
| s_uint37_t { $$ = CANNED_TYPE(0); }
| s_uint38_t { $$ = CANNED_TYPE(0); }
| s_uint39_t { $$ = CANNED_TYPE(0); }

| s_int1_t { $$ = CANNED_TYPE(0); }
| s_int2_t { $$ = CANNED_TYPE(0); }
| s_int3_t { $$ = CANNED_TYPE(0); }
| s_int4_t { $$ = CANNED_TYPE(0); }
| s_int5_t { $$ = CANNED_TYPE(0); }
| s_int6_t { $$ = CANNED_TYPE(0); }
| s_int7_t { $$ = CANNED_TYPE(0); }
| s_int8_t { $$ = CANNED_TYPE(0); }
| s_int9_t { $$ = CANNED_TYPE(0); }
| s_int10_t { $$ = CANNED_TYPE(0); }
| s_int11_t { $$ = CANNED_TYPE(0); }
| s_int12_t { $$ = CANNED_TYPE(0); }
| s_int13_t { $$ = CANNED_TYPE(0); }
| s_int14_t { $$ = CANNED_TYPE(0); }
| s_int15_t { $$ = CANNED_TYPE(0); }
| s_int16_t { $$ = CANNED_TYPE(0); }
| s_int17_t { $$ = CANNED_TYPE(0); }
| s_int18_t { $$ = CANNED_TYPE(0); }
| s_int19_t { $$ = CANNED_TYPE(0); }
| s_int20_t { $$ = CANNED_TYPE(0); }
| s_int21_t { $$ = CANNED_TYPE(0); }
| s_int22_t { $$ = CANNED_TYPE(0); }
| s_int23_t { $$ = CANNED_TYPE(0); }
| s_int24_t { $$ = CANNED_TYPE(0); }
| s_int25_t { $$ = CANNED_TYPE(0); }
| s_int26_t { $$ = CANNED_TYPE(0); }
| s_int27_t { $$ = CANNED_TYPE(0); }
| s_int28_t { $$ = CANNED_TYPE(0); }
| s_int29_t { $$ = CANNED_TYPE(0); }
| s_int30_t { $$ = CANNED_TYPE(0); }
| s_int31_t { $$ = CANNED_TYPE(0); }
| s_int32_t { $$ = CANNED_TYPE(0); }
| s_int33_t { $$ = CANNED_TYPE(0); }
| s_int34_t { $$ = CANNED_TYPE(0); }
| s_int35_t { $$ = CANNED_TYPE(0); }
| s_int36_t { $$ = CANNED_TYPE(0); }
| s_int37_t { $$ = CANNED_TYPE(0); }
| s_int38_t { $$ = CANNED_TYPE(0); }
| s_int39_t { $$ = CANNED_TYPE(0); }


| s_bool {
 	$$ = TREE0("BP_bool");
	}

| s_string {
 	$$ = TREE0("BP_string");
	}

| s_integer {
 	$$ = TREE0("BP_integer");
	}

| s_time {
 	$$ = TREE0("BP_time");
	}


| s_int {
 	$$ = TREE0("BP_integer");
	}

//| sd_id {
// 	$$ = TREE1("BP_usertype", $1);
//	}
;


bp_enumeration_type:
  ss_lbra  exp_comma_list ss_rbra 
    { $$ =
	TREE1("BP_enum", $2);
    }
;
 


/*
onq_square_range:
	ss_lbra  exp ss_colon exp ss_rbra 
	{ $$ = 
	TREE2("Onq_square_range", $2, $4);
	}
;
*/



sd_id_comma_list:
	/* nothing */
	{ $$ =
	LISTEND(0);
	}
| sd_id { $$ = LISTCONS($1, LISTEND(0)); }

| sd_id ss_comma sd_id_comma_list { $$ = LISTCONS($1, $3); }
;



exp_comma_list:
	/* nothing */
	{ $$ =
	LISTEND(0);
	}
| exp
	{ $$ =
	LISTCONS($1, LISTEND(0));
	}
| exp ss_comma exp_comma_list
	{ $$ =
	LISTCONS($1, $3);
	}
;



exp:
  exp1 { $$ = $1; }
| exp1 ss_equals exp1 
	{ $$ =
	TREE2("Assign2", $1, $3);
	}

//% Hmm we have two sites where we parse these assignments. TODO! The other site expands them in the fsharp code.
//| exp1 ss_plusequals exp1 
//	{ $$ =
//	TREE2("Assign2", $1, 	TREE3("MM_diadic", YYLEAF("MM_plusequals"), $1, $3)); 
//	}


//| exp1 ss_minusequals exp1 
//	{ $$ =
//	TREE2("Aassign2", $1, 	TREE3("MM_diadic", YYLEAF("MM_minusequals"), $1, $3)); 
//	}

;


exp1:
  exp2	{ $$ = $1; }
| exp2 ss_query exp2 ss_colon  exp1 { $$ = TREE3("BP_query", $1, $3, $5); }
;

exp2:
  exp3	{ $$ = $1; }

| exp3 ss_rarrow2 exp3	{ $$ = 
	TREE3("BP_diadic", YYLEAF("EO_implies"), $1, $3); 
	}
;


exp3:
  exp4	{ $$ = $1; }

;


exp4:
  exp5	{ $$ = $1; }

| exp4 ss_logor exp5	{ $$ = 
	TREE3("BP_diadic", YYLEAF("EO_logor"), $1, $3); 
	}
;


exp5:
  exp6	{ $$ = $1; }


| exp5 ss_logand exp6	{ $$ = 
	TREE3("BP_diadic", YYLEAF("EO_logand"), $1, $3); 
	}



| exp5 ss_caret exp6	{ $$ = 
	TREE3("BP_diadic", YYLEAF("EO_xor"), $1, $3); 
	}
;

exp6:
  exp7	{ $$ = $1; }

| exp7 ss_deqd exp7	{ $$ = 
	TREE3("BP_diadic", YYLEAF("EO_deqd"), $1, $3); 
	}
| exp7 ss_dned exp7	{ $$ = 
	TREE3("BP_diadic", YYLEAF("EO_dned"), $1, $3); 
	}
| exp7 ss_dltd exp7	{ $$ = 
	TREE3("BP_diadic", YYLEAF("EO_dltd"), $1, $3); 
	}
| exp7 ss_dled exp7	{ $$ = 
	TREE3("BP_diadic", YYLEAF("EO_dled"), $1, $3); 
	}
| exp7 ss_dgtd exp7	{ $$ = 
	TREE3("BP_diadic", YYLEAF("EO_dgtd"), $1, $3); 
	}
| exp7 ss_dged exp7	{ $$ = 
	TREE3("BP_diadic", YYLEAF("EO_dged"), $1, $3); 
	}
;



exp7:
  exp8	{ $$ = $1; }
| exp8 ss_stile exp7	{ $$ = 
	TREE3("BP_diadic", YYLEAF("EO_bitor"), $1, $3); 
	}


;

exp8:
  exp9	{ $$ = $1; }
| exp8 ss_ampersand exp9	{ $$ = 
	TREE3("BP_diadic", YYLEAF("EO_bitand"), $1, $3); 
	}

;

exp9:
  exp10	{ $$ = $1; }
| exp9 ss_plus exp10	{ $$ = 
	TREE3("BP_diadic", YYLEAF("EO_plus"), $1, $3); 
	}


| exp9 ss_minus exp10	{ $$ = 
	TREE3("BP_diadic", YYLEAF("EO_minus"), $1, $3); 
	}

;


exp10:
  exp11	{ $$ = $1; }

| exp10 ss_star  exp11	{ $$ = 
	TREE3("BP_diadic", YYLEAF("EO_times"), $1, $3); 
	}

| exp10 ss_slash  exp11	{ $$ = 
	TREE3("BP_diadic", YYLEAF("EO_divide"), $1, $3); 
	}

;

exp11:
  exp12	{ $$ = $1; }
;

exp12:
  exp13	{ $$ = $1; }

| exp12	ss_lbra  exp ss_colon exp ss_rbra { $$ = 
	TREE3("BP_slice", $1, $3, $5);
	}

| exp12	ss_lbra  exp ss_rbra { $$ = 
	TREE2("BP_subscript", $1, $3);
	}

| ss_dot exp13a { $$ = 
	TREE1("BP_medot", $2);
	}
|  ss_percent ss_lsect exp_comma_list ss_rsect { $$ =
	TREE1("BP_bitcat", $3);
	}

| exp12 ss_dot exp13a	{ $$ = 
	TREE3("BP_diadic", YYLEAF("EO_dotaccess"), $1, $3); 
	}

| exp12 ss_lpar exp_comma_list ss_rpar    { $$ =
	TREE2("BP_apply", $1, $3);
    }
| bp_builtin_function ss_lpar exp_comma_list ss_rpar    { $$ =
	TREE2("BP_apply", $1, $3);
    }

;


exp13a:
  sd_id { $$ =
	TREE1("BP_id", $1);
	}

| s_time { $$ =
	TREE0("BP_timeval");
	}

;

exp13:
 ss_lbra  exp ss_dotdot exp ss_rbra 
    { $$ =
	TREE2("BP_range", $2, $4);
    }


| exp13a { $$ = $1; }

| ss_lpar exp ss_rpar { $$ = $2; }


| sd_number
    { $$ =
	TREE1("BP_num", $1);
	}

| sd_dstring    { $$ =
	TREE1("BP_dstring", $1);
	}


| sd_string
    { $$ =
	TREE1("BP_stringval", $1);
	}

| ss_tilda exp13
	{ $$ =
	TREE1("BP_binnot", $2);
	}

| ss_pling exp13
	{ $$ =
	TREE1("BP_lognot", $2);
	}
;




%%

#include <stdio.h>
#include <ctype.h>
#include "lineno.h"

extern FILE *stderr;
int yyerror (const char *s) 
{
	extern void exit(int);	
	extern builder *lishlex();
	int line_number = filestack[filesp].line_number;
	const char *fn = filestack[filesp].fn;
	printf("+++ Syntax error: %s: %s:; %i\n", s, fn, line_number);
	printf("Next symbol: %s\n", atom_to_str(lishlex()));
	fflush(stdout);
	fflush(stderr);
	exit(1);
	return 1;

}

void yydebug_off()
{
#ifdef YYDEBUG
//	yydebug = 0;
#endif
}

char *smllib = "", *langsuffix = ".bp"; // BlueParrot src files end with .bp

int g_need_bb_starter = 0; // Turn this off for now - the strange php-style bracketing.


/*
 * Two characters which define the escape sequence for comment to end of line.
 */
char commentchar1 = '/';
char commentchar2 = '/';


int yylex()
{
  int vd = g_lextracef;
  char s;  int v = 0;
  extern builder *lishlex();
  extern int lextracef;
  builder *keywordf, *a = lishlex();

  if (!a)
  {
    if (vd) printf("cbg: yylex EOF\n");
    return 0;	
  }	  
  yylval.auxval = a;

//  printf("yylex called\n");

  if (fnumberp(a)) return sd_number;
  if (fstringp(a)) return sd_string;
  if (fdstringp(a)) return sd_dstring;
  keywordf = fgetprop(a, smacro);
  if (vd)  printf("  BTBP Lex %i macrof=%p '%s'\n", v, keywordf, atom_to_str(a)); fflush(stdout);
  if (keywordf) 
  {
     int symno = atom_to_int(fcdr(keywordf)); 
     if (vd)printf("  keyword symno=%i\n", symno);
     return symno;
  }
  s = atom_to_str(a)[0];
  if (isalpha(s) || s=='_') return sd_id;
  cbgerror(cbge_fatal, "Illegal input token %c", s);
  return 0;
}


/* eof */

