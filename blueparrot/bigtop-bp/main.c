/*
 *
 * Bigtop.  
 * (C) 2003-16 DJ Greaves. All rights reserved.
 *
 */


#include <stdio.h>
#include "cbglish.h"
#include <string.h>

void reenter()
{
  extern void exit(int);
  exit(0);
}

static int tokens = 0;
int g_lextracef=0;
int g_vbf = 0; // Verbose mode
const int g_fsharpmode = 0;

const char *incdirpath = NULL;

void xml_print_top(const char*src_fn, FILE *f, builder *v); // prototype

// Render as ML/FSharp source code
void mlprint1(FILE *f, builder *v, int depth)
{
  int i;
  if (tokens++>12)
    {
      tokens = 0;
      fprintf(f, "\n");
      for (i=0;i<depth;i++) fprintf(f, "  ");
    }
  if (!v) fprintf(f, "[]");
  else if ((long int)v < (long int)4096)
    {
      fprintf(f, "<duff%p>\n", v);
      //breakpoint();
    }
  else if (fpairp(v) && fcar(v) == smacro) 
    {
      int firstf = 1;
      v = fcdr(v);
      fprintf(f, "%s", atom_to_str(fcar(v)));
      v = fcadr(v);
      if (v)
	{
	  fprintf(f, "(");
	  while(v)
	    {
	      if (firstf) firstf = 0; else fprintf(f, ", ");
	      mlprint1(f, fcar(v), depth+1);
	      v = fcdr(v);
	    }
	  fprintf(f, ")");
	}
    }
  else if (fpairp(v))
    {
      int firstf = 1;
      fprintf(f, "[");
      while(v)
	{
	  if (firstf) firstf = 0; else fprintf(f, "; "); // For FSharp, the list delimiter is a semicolon.
	  mlprint1(f, fcar(v), depth+1);
	  v = fcdr(v);
	}
      fprintf(f, "]");
    }
  else if (fnumberp(v)) 
    {
      fprintf(f, "%i", atom_to_int(v));
    }

  else if (fatomp(v)) 
    {
      builder *p = fgetprop(smacro, v);
      if (!p) fprintf(f, "\"%s\"", atom_to_str(v));
      else fprintf(f, "%s", atom_to_str(v));
    }
  else 
    {
      fprintf(f, "-?bad-");
      printf("Bad ml item\n"); fdisplay(v);
    }
}



void mlprint(const char*src_fn, FILE *f, builder *v)
{
  fprintf(f, "let %s_ast =(\"%s\", \n", smllib, src_fn);
  mlprint1(f, v, 0);
  fprintf(f, ");\n\n\n");

}

void mlprintlist(const char*src_fn, FILE *f, builder *v)
{
  while (v)
    {
      mlprint(src_fn, f, fcar(v));
      v = fcdr(v);
    }
}


static FILE *cos = NULL;

const char *g_opfn = (g_fsharpmode) ? "spool.fs": "spool.xml";

extern const char *g_progname;

int main(int argc, const char *argv[])
{
  g_progname = argv[0];
  extern void define_keywords();

  init_cbglish();
  cos = stdout;
  define_keywords();

#if YYDEBUG
  extern int yydebug;
  yydebug = 0;
#endif


  while (argc >= 2 && ((argv[1][0]=='-') || (argv[1][0]=='+')))
    {
      if (!strcmp(argv[1], "-lextrace"))
      {
	g_lextracef = 1;
	argv +=1; argc-=1;
	continue;
      }

      if (!strcmp(argv[1], "-verbose"))
      {
	g_vbf = 1;
	argv +=1; argc-=1;
	continue;
      }

      if (!strcmp(argv[1], "-yydebug"))
      {
#if YYDEBUG
	extern int yydebug;
	yydebug = 1;
#endif
	argv +=1; argc-=1;
	continue;
      }

      if (!strcmp(argv[1], "-o"))
      {
	g_opfn = argv[2];
	argv +=2; argc-=2;
	continue;
      }
      
      if (!strncmp(argv[1], "+incdir+", 8))
	{
	  incdirpath = argv[1]+8;
	  argv +=1; argc-=1;
	  continue;
      }
      break;
    }

  if (argc < 2) 
    {
      printf("Useage:  %s [ -lextrace -yydebug ] [ +incdir+PATH ] srcfiles ...", g_progname);
      return -1;
    }

  cos = fopen(g_opfn, "w");

  if (g_vbf) printf("Opened %s for spool file\n", g_opfn);
  if (g_fsharpmode)
    {
      fprintf(cos, "module spool\n");
      fprintf(cos, "#lite \"off\"\n");
      fprintf(cos, "open %s;\n\n", smllib); 
    }
  else
    {
      fprintf(cos, "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
    }
  
  while (argc >= 2) 
    {
      const char *src_fn = argv[1];
      builder * r =  cbglish(c_filename, 0, src_fn, NULL);
      if (g_vbf)  fdisplay(r);

      if (g_fsharpmode)
	{
	  fprintf(cos, "(* Parse tree from %s *)\n\n", src_fn); 
	  mlprintlist(src_fn, cos, r);
	  fprintf(cos, "\n\n\n");
	}
      else
	{
	  fprintf(cos, "<!-- Parse tree from %s -->\n\n", src_fn); 
	  xml_print_top(src_fn, cos, r);
	  fprintf(cos, "\n\n\n");
	}

      argc --; argv ++;
    }

  if (g_fsharpmode)
    {
      fprintf(cos, "\n\n(* eof *)\n");
    }
  else
    {
      fprintf(cos, "<!-- eof -->\n");
    }
  if (cos != stdout) fclose(cos);
  if (g_vbf) printf("Normal exit\n");
  return 0;
}


builder *genconst(int A, char *N, builder *D)
{
  builder *r = FLIST3(smacro, str_to_atom(N), D);
  if (0)
    {
      mlprint1(stdout, r, 0);
      printf("\n");
      fflush(stdout);
    }
  return r;
}


extern void cleanexit(int code)
{
  extern void exit(int);
  if (cos && cos != stdout) fclose(cos);
  if (g_opfn && code)
    {
      printf("Deleting %s output file owing to errors\n", g_opfn);
      remove(g_opfn);
    }
  if (g_vbf) printf("%s: Exit code rc=%i\n", g_progname, code);  
  exit(code);
}	


// Render as ML/FSharp source code
void xml_print1(FILE *f, builder *v, int depth)
{
  int i;
  if (tokens++>12)
    {
      tokens = 0;
      fprintf(f, "\n");
      for (i=0;i<depth;i++) fprintf(f, "  ");
    }
  if (!v) fprintf(f, "<list/>");
  else if ((long int)v < (long int)4096)
    {
      fprintf(f, "xmlp-%p", v);
      printf("Yikes, wierd xmlp-%p\n", v);
      //breakpoint();
    }
  else if (fpairp(v) && fcar(v) == smacro)  //These are elements
    {
      const char *ename = atom_to_str(fcadr(v));
      int firstf = 1;
      v = fcdr(v);
      v = fcadr(v);
      if (v)
	{
	  if (depth)
	    {
	      fprintf(f, "\n");
	      for (int p = 0; p<depth; p++) fprintf(f, " "); // Indent
	      tokens = 0;
	    }
	  fprintf(f, "<%s>", ename);	  
	  if (v)
	    {
	      while(v)
		{
		  if (firstf) firstf = 0; else fprintf(f, " ");
		  xml_print1(f, fcar(v), depth+1);
		  v = fcdr(v);
		}
	    }
	  fprintf(f, "</%s>", ename);      
	}
      else fprintf(f, "<%s />", ename);      
    }
  else if (fpairp(v))
    {
      if (!v)
	{
	  fprintf(f, "<list/>");
	}
      else
	{
	  int firstf = 1;
      	  fprintf(f, "<list>");
	  while(v)
	    {
	      if (firstf) firstf = 0; else fprintf(f, " ");
	      xml_print1(f, fcar(v), depth+1);
	      v = fcdr(v);
	    }
	  fprintf(f, "</list>");
	}
    }
  else if (fnumberp(v)) 
    {
      fprintf(f, "%i", atom_to_int(v));
    }

  else if (fatomp(v)) 
    {
      builder *p = fgetprop(smacro, v); // Print macros without quotes, real strings and user id's with.
      if (!p) fprintf(f, "\"%s\"", atom_to_str(v));
      else fprintf(f, "%s", atom_to_str(v));
    }
  else 
    {
      fprintf(f, "-?bad-");
      printf("Bad ml item\n"); fdisplay(v);
    }
}




void xml_print(const char*src_fn, FILE *f, builder *v)
{
  xml_print1(f, v, 0);
  fprintf(f, "\n\n\n");
}

void xml_print_top(const char*src_fn, FILE *f, builder *v)
{
  while (v)
    {
      xml_print(src_fn, f, fcar(v));
      v = fcdr(v);
    }
}


/* eof */
