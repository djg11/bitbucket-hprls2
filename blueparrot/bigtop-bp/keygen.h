/*
 *  $ID: $
 *
 * Bigtop.  
 * (C) 2003 DJ Greaves. All rights reserved.
 *
 */
#ifndef KEYGEN_H
#define KEYGEN_H

#ifdef GENL

#define  BIGTOP_KEYWORD(NAME, DOMAIN, X, Y)     %token s_ ## NAME
#define  BIGTOP_PUNCTUATION(SYMBOL, NAME, DOMAIN, X, Y)     %token  NAME

#else

#define  BIGTOP_KEYWORD(NAME, DOMAIN, X, Y)     cbglish_keyword(DOMAIN, #NAME,  s_ ## NAME, X, Y);
#define  BIGTOP_PUNCTUATION(SYMBOL, NAME, DOMAIN, X, Y)     cbglish_symbol(DOMAIN, SYMBOL,  NAME, X, Y);
#endif


/* eof */
#endif
