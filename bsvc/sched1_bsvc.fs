// CBG SQUANDERER : Rudimentary/Toy Bluespec Verilog Compler: Coded in F#.
// (C) 2012 DJ Greaves, University of Cambridge. 

(* All rights reserved, except as licensed by LGPL below.
 * DJ Greaves, University of Cambridge, Computer Laboratory.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *)



// $Id: sched1_bsvc.fs,v 1.1 2013-08-28 11:39:34 djg11 Exp $

module sched1_bsvc

open System.Collections.Generic;
open System.Numerics

open moscow
open meox
open yout
open linepoint_hdr

(*
open dotreport
open tableprinter
*)

open tydefs_bsvc
open hprls_hdr

type rules_indexed2_t = Map<string list, rc_t * hbexp_t * hbexp_t>

let fpathToStr(idl, n) = htosp idl + "/n=" + i2s n

let g_nameof_solos_class = "$solos_class$"




// Zadeh fuzzy logic equations 

let fuzzyprob ww fv msg bexp =

    let zadeh_not x = 1.0-x
    let rec zadeh_or = function
        | [] ->
            hpr_yikes(sprintf "zadeh_or applied to empty list")
            0.0
        | [a] -> a
        | a::b::tt -> max (a) (zadeh_or (b::tt))
    and zadeh_and = function
        | [] ->
            hpr_yikes(sprintf "zadeh_and applied to empty list")
            1.0
        | [a] -> a
        | a::b::tt -> min (a) (zadeh_or (b::tt))
        
    and fprop arg =
        match arg with
        | X_true ->  1.0
        | X_false ->  1.0        
        | W_cover(cubes, _) ->
            let fuzzy_deblit item =
                if item < 0 then zadeh_not(fprop(deblit (0-item))) else fprop (deblit item)
            let cubit_and lst =
                zadeh_and(map fuzzy_deblit lst)
            zadeh_or (map cubit_and cubes)
        | W_bdiop _ ->
            let nn = xb2nn arg
            let vale = 0.5
            //dev_println (sprintf "fprop alloc %A for nn=%i" vale nn)
            vale

        | other ->
            dev_println (sprintf "fprop other fuzzy shine %A" other)
            0.5

    fprop bexp



// Obey the descending_urgency manual priority specification.  
// This needs to be a conservative sort.
let sort_rules_according_to_users_urgency ww urgency_edges rules_in_class =
    let msg = "descending_urgency topological sort"
    let rules_indexed = new OptionStore<string list, string list * xidx_t list * xidx_t list list * int * int>("class_rules_indexed")
    let add_to_idx cc arg =
        match arg with
        | (idl, coms, rems, natural_order, textual_order) ->
            if not_nonep(rules_indexed.lookup idl) then sf(msg + sprintf ": two rules with the same name '%s'" (hptos idl))
            rules_indexed.add idl arg
            idl::cc

    let nodes = List.fold add_to_idx [] rules_in_class
    vprintln 2 (msg + sprintf ": %i rules to sort/schedule" (length nodes))
    let ans = topological_sort ww msg hptos nodes urgency_edges

    if false then
        vprintln 3 (msg + sprintf ": rule sort output %A" (map hptos ans))
        //vprintln 3 (sprintf "rule names %A" (sfold (fst>>hptos) named_rules))
    let ans =
        let rehyd rname = (valOf_or_fail "rehyd-L106" (rules_indexed.lookup rname))
        map rehyd ans
    (ans)


// A first gash implementation of automatic run-time arbiters.
// TODO change to a proper round-robin one and also support starvation avoiding dynamic priority.    
let simple_arbiter_gen ww bobj clkinfo sh_emit cls arity = // One share per participant.
    let bits = bound_log2(BigInteger(arity-1))
    let keystr = funique ("ARX_" + cls)
    let arb_emits = gec_blank_emission ("simple_arbiter_" + keystr)
    let bsv_rr_arb0 = // Generate state net for a round-robin arbiter.
        let ats = [ Nap(g_resetval, "0") ]
        ionet_w(keystr + "_arbiter_state_reg", bits, LOCAL, Unsigned, ats)

    let ww = WF 2 "simple_arbiter_gen" ww (sprintf " arbiter arity=%i state " arity + netToStr bsv_rr_arb0)
    let arb_advance_net = simplenet (keystr + "_advance")
    mutadd arb_emits.nets arb_advance_net
    mutadd arb_emits.nets bsv_rr_arb0
    mutadd arb_emits.synch_logic  (clkinfo, [gen_dflop(xi_orred arb_advance_net, bsv_rr_arb0, ix_mod (ix_plus (xi_num 1) bsv_rr_arb0) (xi_num arity))])
    let m_idx = ref 0
    let arbiter_gen rulename = // We return a function that a customer can call and another one that advances the state.
        let ans = ix_deqd (xi_num !m_idx) bsv_rr_arb0
        mutinc m_idx 1
        (None, ans)
    mutadd sh_emit.children arb_emits
    (arbiter_gen, Some arb_advance_net)


//
// Generate a round-robin arbiter with combinational req to ack paths. Not recommended for large (8 or more arity) owing to combinational logic complexity.
//
// Preferably see also mk_arbiter_rrobin coded in hprtl style.
let round_robin_arbiter_gen ww bobj clkinfo sh_emit cls arity = // One share per participant.
    let bits = bound_log2(BigInteger(arity-1))
    if arity < 2 then sf "no-bits-rr-arbiter"
    else
        //let arity = arity + 4 // for now
        let keystr = funique ("ARRX_" + cls)
        let arb_emits = gec_blank_emission ("round_robin_arbiter_" + keystr)
        let assign lhs rhs = mutadd arb_emits.comb_logic (gen_buf(lhs, rhs))
        let statereg = // Generate state net for a round-robin arbiter.
            let ats = [ Nap(g_resetval, "0") ]
            ionet_w(keystr + "_arbiter_state_reg", bits, LOCAL, Unsigned, ats)
        let ww = WF 2 "round_robin_arbiter_gen" ww (sprintf " arbiter arity=%i state " arity + netToStr statereg)    
        let moda x = if x < 0 then x + arity elif x >= arity then x - arity else x
        let reqs_and_acks =
            let gencon i = (i, simplenet (sprintf "%s_req_%i" keystr i),  simplenet (sprintf "%s_ack_%i" keystr i)) 
            map gencon [0..arity-1]
        let b_reqs_and_acks = map (fun (i, r, a) -> (i, xi_orred r, xi_orred a)) reqs_and_acks
        let (_, reqs, acks) = List.unzip3 b_reqs_and_acks
        let gen_ack_logic (i, req, ack) =
            let rec razzer j =
                let site = ix_deqd (statereg) (xi_num j)
                let rec reqa k =
                    if k = i then X_true else 
                        //dev_println(sprintf "reqa   i=%i  j=%i  k=%i" i j k)
                        ix_and (xi_not reqs.[k]) (reqa (moda(k+1)))
                let here = ix_and site (reqa (moda(j+1)))
                if j=i then here else ix_or here (razzer (moda(j-1)))
            let no_higher = razzer (moda(i-1))
            //dev_println (sprintf " round robin no-higher predicate for i=%i is %s" i (xbToStr no_higher))
            assign ack (xi_blift (ix_and (xi_orred req) no_higher))
        app gen_ack_logic reqs_and_acks

        let nsf_logic = // A simple unary to binary encoder is all that is needed.  We generate a priority encoder which is a little bit of overkill.
            let rec nsf_enc i =
                if i = 0 then xi_num 0
                else ix_query acks.[i] (xi_num i) (nsf_enc (i-1))
            nsf_enc (arity-1) // The state register contains the ordinal of the last-serviced client.
        let arb_advance_net = simplenet (keystr + "_advance")
        mutadd arb_emits.synch_logic  (clkinfo, [ gen_dflop(xi_orred arb_advance_net, statereg, nsf_logic) ])

        mutadd arb_emits.nets arb_advance_net
        mutapp arb_emits.nets (map f2o3 reqs_and_acks @ map f3o3 reqs_and_acks)
        mutadd arb_emits.nets statereg
        mutadd sh_emit.children arb_emits
        let advance_condition = ix_orl reqs // An n-input OR gate.
        mutadd arb_emits.comb_logic (gen_buf(arb_advance_net, xi_blift advance_condition))
        let m_idx = ref 0

        let rr_arbiter_gen rulename = 
            let n = !m_idx
            let ans = (Some reqs.[n], acks.[n])
            mutinc m_idx 1
            ans
        (rr_arbiter_gen, None)  // We return a function that a customer can call.  We locally determine the advance net expression from dijunction of requests.


// Assign un-even shares 
let weighted_arbiter_gen ww bobj clkinfo sh_emit cls share_holdings = 
    let arity =
        let summer cc (id_, shares) = shares + cc
        List.fold summer 0 share_holdings
    let (arbiter_gen, arb_advance_net) = simple_arbiter_gen ww bobj clkinfo sh_emit cls arity      // Underlier makes one share per participant.

    let fanned_out_share_holdings =

        let randomise _ = 0
        22
    // We want to permute the share holdings to spread them out - easiest way is coprime modulus. 

    (arbiter_gen, arb_advance_net) 





let shedule_class_on_splitters ww vd (rules_in_class, cls_no) =
    let cls = sprintf "CC%i" cls_no
    //vprintln 2 (sprintf "%i resources in shed class %s being " (length rules_in_class) cls + sfold (fun x->x) resources)
    
//    let rules_in_class =
//        List.filter (fun (ri, items) -> list_intersection_pred(items, mems)) rules // it would be better to use the inverted index map instead of this quadratic intersect.

    vprintln 2 (sprintf "%i rules in shed class %s being " (length rules_in_class) cls + sfold (fun (idl, ks_, guard) -> htosp idl) rules_in_class)
    // When a class is partitioned, the resources use within a partition may no longer conflict?
    let commonise_and_report (idl, ks, ig) =
            let tointform = function
                | W_cover(prods, _) -> prods
                | other when xi_isfalse other -> []
                | other when xi_istrue other  -> [[]]
                | other -> [[xb2nn other]]
            let ints = tointform ig   // Should perhaps first convert to CNF form using xi_clauses ?
            // Need to find common factors since can combine without blocking on them.
            let common_factors =  // We find common factors since can partition on differing polarity of any common factor.
                let rec commons = function
                    | [] -> []
                    | [item] -> item
                    | h::t -> list_intersection(h, commons t)
                commons ints    
            let rems = map (fun term -> list_subtract(term, common_factors)) ints
            let s = (xbToStr) ig + "  " + sfold (fun x -> sfold i2s x + "|") ints
            let s = s + "  common_factors=" + sfold i2s common_factors
            if vd>=4 then vprintln 4 (sprintf "sched1: Rule firing=%s" (htosp idl) + " " + s)
            (idl, common_factors, rems)
    let rls1 = map commonise_and_report rules_in_class
    // Now we need to partition the rules according to disjoint conditions.
    // If there is more than one rule in a partition then that partition later needs a schedulling decision.
    let support = List.sortWith (fun a b -> abs(a)-abs(b)) (List.fold (fun cc (ri, commons, rems)->lst_union commons cc) [] rls1)
    let oldlength1 = length rules_in_class
    let splitters = // Find predicates appearing in both polarities.
            let rec fs = function
                | a::b::tt when a = -b -> (abs a) :: fs tt
                | _::tt -> fs tt
                | [] -> []
            fs support
    if vd>=4 then vprintln 4 (sprintf "Rule spitting predicate (clauses with both polarities) heap indexs are " + sfold i2s splitters)
    let partitions = // Only one partition in a class can possibly fire but the partition may have several conflicting rules that could potentially be fired at once.
            //After partitioning, the members of a partition may no longer conflict.
            let rec dopart rls = function // split a list into two lists of the original type.
                | h::tt ->
                    //dev_println (sprintf "Partition lengths in are: " + sfold (length>>i2s) rls)
                    let ap pol (idl, commons, rems) = memberp (if pol then -h else h) commons 
                    let subpart rset cc =
                        let neg = List.filter (ap true) rset // could use groom and also double check no repetition here.
                        let pos = List.filter (ap false) rset
                        let (lp, ln, ltot) = (length pos, length neg, length rset)
                        let cleanf = lp + ln = ltot
                        vprintln 3 (sprintf "dopart: clean split=%A: %i + %i <> %i for pred %i %s." cleanf lp ln ltot h  (xbToStr(deblit h)))
                        if cleanf then pos ::neg :: cc else rset::cc
                    let rls = List.foldBack subpart rls []
                    //dev_println (sprintf "Partition lengths out are: " + sfold (length>>i2s) rls)
                    //let rxToStr (idl, commons, rems) = hptos idl + "  commons=" + sfold i2s commons 
                    //reportx 0 "Rules-in-play" rxToStr (hd rls)
                    dopart rls tt

                | [] -> rls
            let partitions = dopart [rls1] splitters
            let newlength = List.fold (fun cc a -> cc + length a) 0 partitions
            let lmsg = (sprintf "%s: New rule total count %i - (should be same as old %i) " cls newlength oldlength1 +  sfold (fun x->i2s(length x)) partitions)
            if nullp partitions then vprintln 2 (sprintf "No splitters to partition on.")
            else
                vprintln 2 (sprintf "Class split into %i partitions." (length partitions))
                vprintln 2 lmsg
            if newlength <> oldlength1 then sf lmsg
            partitions
    (cls, partitions)


let schedule_class_hi ww vd (rules_of_the_class, cls_no) =
    let nr = length rules_of_the_class
    vprintln 2 "\n------------------+"
    if nr < 2 then
        vprintln 3 (sprintf "Schedulling class CC%i of %i members: too few rules for a conflict to be possible." cls_no nr)
        // Pass through here for type compatibility but not needed.
        shedule_class_on_splitters ww vd (rules_of_the_class, cls_no)
    else
        vprintln 3 (sprintf "Schedulling class CC%i of %i members." cls_no nr)
        let intersect cc (a_name, a_ks_, a_grd) (b_name, b_ks_, b_grd) =
            let simult = ix_and a_grd b_grd
            if myfalse simult then
                vprintln 2 (sprintf "No conflict arises (disjoint guards) between resources %s and %s" (hptos a_name) (hptos b_name))
                cc
            else
                vprintln 2 (sprintf "Conflict exists between resources used by %s and %s" (hptos a_name) (hptos b_name))
                ((a_name, a_ks_, a_grd), (b_name, b_ks_, b_grd))::cc


    // For example, these two rules race, but they both write the same value to iax so it is not a conflict under idempotent rules.         
    //            flict SimpleProcessorTb.mkTb.go           	SimpleProcessorTb.mkTb.iax._write  idem=true  actuals=500203
    //            flict SimpleProcessorTb.mkTb.loadInstrs       	SimpleProcessorTb.mkTb.iax._write  idem=true  actuals=500203

        let rec quadratic_intersect cc = function
            | []
            | [_] -> cc
            | a::b::tt -> quadratic_intersect (intersect cc a b) (b::tt)
        let conflicts = quadratic_intersect [] rules_of_the_class
        if nullp conflicts then
                vprintln 3 (sprintf "No conflicts for class CC%i of %i members" cls_no (length rules_of_the_class))

            // TODO - use the above info .... <----------------
        shedule_class_on_splitters ww vd (rules_of_the_class, cls_no)


//
// Rules that invoke common resources potentially conflict.
//    There is no conflict if their activation expressions have empty intersection.
//    There is no conflict on idempotent actions with identical arg expressions.
//
let conflict_log_collate ww bobj vd conflict_free (rule_map:rules_indexed2_t) conflict_log = 
        let ww = WF 2 "conflict log collate" ww (sprintf "Collate and index %i conflict_log entries." (length conflict_log))
        let collated_conflict_log = new ListStore<string, (string list * hbexp_t)>("collated_conflict_log")  // Resource to rule mapping
        let collated_use_log = new ListStore<string list, (string * string * hbexp_t)>("collated_use_log")   // Rule to resource mapping
        let flict_prep (alpha, (ext_methodf_, monica), resname, fname, actuals, resats) =
            if memberp monica conflict_free then
                vprintln 3 (sprintf "Ignoring conflicts on rule %s since asserted to be conflict_free" (hptos monica)) // TODO print this after checking whether it really was.
                ()
            else
                let lactuals = map (fun (ty, vale) -> bsvc.bsv_lower ww bobj vale) actuals
                let siga = map x2nn lactuals
            // A rule can conflict with itself if alpha expressions are not disjoint.
                let res_string = hptos resname + "_" + fname 
                let idempotentf = resats.idempotentf
                let ignorablef = resats.can_be_ignored_for_schedulling
                let res_use_string = res_string + "_" + (sfold i2s siga) + (if idempotentf then "" else funique "impotent")                
                if not ignorablef then
                    collated_conflict_log.add res_string (monica, alpha)
                    collated_use_log.add monica (res_string, res_use_string, alpha)
                // TODO process idempotent flag.
                // Multiple written scalar registers can be ignored for conflict processing.
                vprintln 3 (sprintf "flict:  %s       \t%s  idem=%A ignorable=%A actuals=%s" (hptos monica) (hptos (resname)) idempotentf ignorablef (sfold i2s siga))
        app flict_prep conflict_log

        // Rather than a full quadratic pairwise compare, we partition into classes of possible interference.
        let ruling_classes =
            let rc = new EquivClass<string>("ruling_classes")  // Equivalence classes of resources used by rules.
            let arc rulename resources =
                let items = map (fun (res_name, opaque_use_name_, guard) -> res_name) resources
                if not_nullp items then
                    let cls_ = rc.AddFreshClass (fun _ _ _ -> ()) items     // AddFreshClass causes automatic conglomoration resource classes shared by more than one rule
                    ()
            for kv in collated_use_log do arc kv.Key kv.Value done

            let m_clis = ref []
            let rules_using cc resource_name =
                    match collated_conflict_log.lookup resource_name with
                        | items ->
                            //dev_println (sprintf "rule names using %s are %A" (resource_name) (map fst items))
                            lst_union (list_once(map fst items)) cc
            for kv in rc do mutadd m_clis (List.fold rules_using [] (snd kv.Value)) done
            let m_solo_rules = ref []
            let check_solo rulename =
                if nullp(collated_use_log.lookup rulename) then mutadd m_solo_rules rulename
            for kv in rule_map do check_solo kv.Key done
            vprintln 2 (sprintf "Rules have been placed in %i disjoint classes for schedulling with a further %i as solo rules." (length !m_clis) (length !m_solo_rules))
            !m_clis @ (map (fun x-> [x]) !m_solo_rules)  // This is a list of lists of rule names. Each rule name is a string list.

        let ruling_classes = zipWithIndex ruling_classes
        reportx 3 ("classes of potentially conflicting rules")  (fun (lst, cls_no) -> sprintf "CC%i : %s" cls_no (sfold hptos lst)) ruling_classes


        let ruling_classes =
            let rehydrate rule_name =
                match rule_map.TryFind rule_name with
                    | None -> sf (sprintf "cannot rehydrate rule %s since not indexed" (hptos rule_name))
                    | Some (ri, g1, g2) -> (rule_name, ri, ix_and g1 g2) 
            map (fun (lst, cls) -> (map rehydrate lst, cls)) ruling_classes

        // Two rules in a class only conflict if their composite guards intersect and their conflicting resource use activation expressions, under that composite guard, also intersect.  The first check is redundant but is an optimisation.
        
        let schedulled = map (schedule_class_hi ww vd) ruling_classes
        vprintln 2 "\n------------------*"            
        let ww = WF 2 "conflict log collate" ww "finished"
        schedulled













//
// TODO: ideally we need to schedule within each clock domain...  especially if we start introducing clocked logic during schedulling, such as stateful arbiters, but for now we just look at the reset net.
//
let default_schedule ww bobj iname sh_emit clkinfo shed3 conflict_log rulectl00 =
    let msg = "scheduler=default_schedule"
    let vd = bobj.sched_loglevel
    let ww = WF 1 "Using default scheduler " ww ("Start for " + iname)    
    let m_fire_rates_report = ref []
    let get_reset_bar ww bobj msg clkinfo =
        match clkinfo.dreset with
            | "" | "default_reset" -> xi_orred(reset_net())
            | reset_name ->
                let ans = xi_orred(g_input (Some sh_emit) ([reset_name], 1, []))
                let ans = if clkinfo.rstpos then xi_not ans else ans
                ans
    let resetbar = get_reset_bar ww bobj msg clkinfo 

    let (descending_urgency, conflict_free, mutually_exclusive_) = shed3
    let conflict_free = List.fold (fun cc (k, vale) -> vale :: cc) [] conflict_free
    //if not_nullp conflict_free_ then hpr_yikes(msg + " conflict_free annotation(s) currently ignored - just punt the rule to the solo class?")

    if not_nullp mutually_exclusive_ then hpr_yikes(msg + " mutually_exclusive annotation(s) currently ignored")    

    let igconj_top key cc arg = 
        let rec igconjl cc = function // Non-strict intrinsic guard here .... TODO preserve strict mode too.
            | IG_resource(idl, ids, x, resats) -> ix_and cc x
            | IG_alpha explicit_guard ->
                //dev_println (sprintf "IG_alpha for %s is %s" (hptos key) (xbToStr explicit_guard))
                ix_and cc explicit_guard
            | IG_mux(g, t, f) -> ix_and cc (xi_bquery(g, Map.fold igconjm X_true t, Map.fold igconjm X_true f))
        and igconjm cc (external_methodf, key_) item =
            //dev_println (sprintf "sched1: igconjl key=%s da=%s" (hptos key_) (igItemToStr item))
            igconjl cc item
        igconjm cc arg

    let _ =
        let rule_counter (rule_count, ext_method_count) = function
            | RC_rule ri as arg -> (rule_count + 1, ext_method_count)
            | RC_methbuf(idl, ids, en, l, resource_records) as arg -> (rule_count, ext_method_count  + 1)
        let (rule_count, ext_method_count) = List.fold rule_counter (0, 0) rulectl00
        vprintln 1 (sprintf "Scheduler start for %i rules and %i methods." rule_count ext_method_count)
        ()

    // Compute the composite guard for each rule and put them in a map.        
    let rule_map =
        let rule_index_and_composite (mm:rules_indexed2_t) = function
            | RC_rule ri as arg ->
                match mm.TryFind ri.rulename with
                    | Some ov -> cleanexit(msg + sprintf ": two rules with the same name '%s'" (hptos ri.rulename))
                    | None ->
                        vprintln 3 (sprintf "Indexing rule " + hptos ri.rulename)
                        let total_guard = Map.fold (igconj_top ri.rulename) X_true ri.resource_records.igs
                        //The explicit guard is already included for now as an IG_alpha entry.
                        //dev_println (sprintf "Rule %s: total_guard=%s" (hptos ri.rulename) (xbToStr total_guard))
                        mm.Add(ri.rulename, (arg, total_guard, total_guard))
            | RC_methbuf(idl, ids, en, l, resource_records) as arg ->
                match mm.TryFind idl with
                    | Some ov -> cleanexit(msg + sprintf ": method name clashes with rule or method with the same name '%s'" (hptos idl))
                    | None ->
                        vprintln 3 (sprintf "index external method " + hptos idl)
                        //Map.fold (fun (mm:Map<string, iguard_t>) k v -> mm.Add(htosp k, v)) mm rr.igs
                        let intrinsic_guard = Map.fold (igconj_top idl) X_true resource_records.igs
                        let explicit_guard = xi_orred en

                        mm.Add(idl, (arg, intrinsic_guard, explicit_guard))
        List.fold rule_index_and_composite Map.empty rulectl00


    let partitions = conflict_log_collate ww bobj vd conflict_free rule_map conflict_log 

    // There may be an urgency edge between rules in different schedulling classes: such edges may simply be ignored.
    let urgency_edges = new ListStore<string list, string list>("edges")
    let verify_exists arg = if nonep(rule_map.TryFind arg) then hpr_yikes(sprintf "Rule %s named for %s was not found" (hptos arg) msg)
    let urgency_edge_list =
        let scannate cc = function
            | (Pragma_shed_control("descending_urgency" as keyk, sublst), codepoint) ->
                let rec to_xition cc = function
                    | []  -> cc
                    | [b] ->
                        verify_exists b
                        cc
                    | a::b::tt ->
                        verify_exists a
                        //if vd>=4 then vprintln 4 (sprintf "%s edge %s -> %s" keyk (hptos a) (hptos b))
                        vprintln 3 (sprintf "Rule ordering: %s ahead of %s" (hptos a) (hptos b))
                        urgency_edges.add a b
                        to_xition ((a,b)::cc) (b::tt)
                to_xition cc sublst
            | other -> sf (sprintf "Bad or unexpected rule sorting directive. other=%A" other)
        List.fold scannate []  descending_urgency 
  
    let urgency_nodes_d =
        let adder cc (a, b) =
            verify_exists b
            singly_add b cc
        List.fold adder [] urgency_edge_list


    let (named_rules, method_ctl) =
        let pair_with_name arg (cc:Map<string list, rc_t>, cd) =
            match arg with
            | RC_rule ri ->
                vprintln 3 (sprintf "index rule again " + hptos ri.rulename)
                //if not_nonep(rule_map.TryFind ri.rulename) then sf(msg + sprintf ": two rules with the same name '%s'" (hptos ri.rulename))
                //rules_indexed.add ri.rulename arg
                (cc.Add(ri.rulename, arg), cd)

            | RC_methbuf(idl, ids, en, l, rr) -> (cc.Add(idl, arg), arg :: cd)
            //other -> (cc, other::cd)
        List.foldBack pair_with_name rulectl00 (Map.empty, []) 

//OLD
    let collate_meth_bufs (mm:Map<string, iguard_t>) = function
        | RC_methbuf(idl, ids, en, l, rr) ->                Map.fold (fun (mm:Map<string, iguard_t>) (ext_methodf_, k) v -> mm.Add(htosp k, v)) mm rr.igs
        | other -> mm
    let methbufs = List.fold collate_meth_bufs Map.empty rulectl00


// OLD

    // Externally called methods can lead to combinational loops between schedulers.
    // PLEASE EXPLAIN - TODO
    // Under old non-ephemeral local methods, we need transitive closure of resource use.
    let meth_resources = //Make a dictionary where the methbuf intermediates are subliminal.
        let dd = new Dictionary<string, string list>()
        let rec transclose k =
            let (found, ov) = dd.TryGetValue k
            if found then ov
            else
                let support = methbufs.TryFind k  // hope for no loops in methbufs!
                let nv =
                    match support with
                        | None -> [k]
                        | Some item ->
                            let details cc = function
                                | IG_resource(idl, ids, x, resats) ->
                                    if resats.can_be_ignored_for_schedulling then cc else ids :: cc // need ids since EquivClass is fixed with 'a=string
                                | _ -> cc
                            details [] item
                if vd>=4 then vprintln 4 (k + " resource transclose needs " + sfold (fun x->x) nv)
                dd.Add(k, nv)
                nv
        for z in methbufs do ignore(transclose z.Key) done           
        dd
    vprintln 1 ("meth_resources made")


    let oldlength = named_rules.Count
    vprintln 1 (sprintf "Sched1: Groom %i items into %i rules and %i others (methods). Log length %i flicts." (length rulectl00)  oldlength (length method_ctl) (length conflict_log))
    

        
    let def_scheduler_capoff_external_methods = function
        | RC_methbuf(idl, ids, en, rdynet, resource_records) ->
            let g1 = Map.fold (igconj_top idl) X_true resource_records.igs
            if myfalse g1 then hpr_warn(msg + sprintf ": Method '%s' can never fire (intrinsic guard invalid) and is being discarded." (htosp idl))
            vprintln 3 (sprintf "RC_methbuf: %s := %s;" (xToStr rdynet) (xbToStr g1))
            //mutadd sh_emit.comb_logic (gen_buf(l, en))
            mutadd sh_emit.comb_logic (gen_buf(rdynet, xi_blift g1))
            ()
        | other -> sf (msg + sprintf ": other form in def_scheduler_m %A" other)


    let def_scheduler_rules (cls, partition) =
        let xprint1 msg (px) =
            let xprint (idl, coms, rems, natural_pri) =
                let pos ints = sfold (fun x -> sfold i2s x + "|") ints
                if vd>=4 then vprintln 4 (sprintf "xprinty: %s for sorting %s:  coms=%s  rems=%s  natural_pri=%i" cls (hptos idl) (sfold i2s coms) (pos rems)  natural_pri)
            xprint px

        let _:(string list * xidx_t list * xidx_t list list) list list = partition
        let partition = // Find fair order within a partition, which is where, for a pair of conflicting rules where the guard of the first implies the guard of the second, but not the other way around, the second is given priority so that it gets fairer service.
            // Cannot use xi_implies in general, since this needs a SAT solver etc, but the espresso variant is a better guide than just taking length...
            let xprint1 msg_ (px, exno) =
                let xprint (idl, coms, rems) =
                    let pos ints = sfold (fun x -> sfold i2s x + "|") ints
                    if vd>=4 then vprintln 4 (sprintf "def_scheduler: %s %i for sorting %s:  coms=%s  rems=%s" cls exno (hptos idl) (sfold i2s coms) (pos rems))
                app xprint px
            if vd>=4 then app (xprint1 msg) (zipWithIndex partition) // Number the guards with exno
#if OLD_EDIT
            let implies_approx_pred a b =
                let bl = length (f3o3 b) // rems field
                let al = length (f3o3 a) // rems field
                al-bl
            List.sortWith implies_approx_pred partition
            //partition

        let partition =  // Note: natural and fair are synonyms.
            let add_fair_priority (lst, natural_pri) = 
                dev_println (sprintf "PIG add%i" natural_pri)
                let gen (rc, coms, rems) = (rc, coms, rems, natural_pri)
                map gen lst 
            list_flatten (map add_fair_priority (zipWithIndex partition))  // Sorted into fair order, so zipWithIndex makes this explicit.
#endif
            let add_fair_priority pp0 =           // Note: natural and fair are synonyms .            // TODO? sort by fair order
                let gen (rc, coms, rems) =
                    let natural_pri = length rems + length coms
                    //let al = length (f3o3 a) // rems field
                    let it = (rc, coms, rems, natural_pri) // Simplistic higher fair priority if have more conditions.
                    //let it = (rc, coms, rems)
                    //if vd>=4 then xprint1 "msg" (it) // Number the guards with exno                    
                    it
                let pp0 = map gen pp0 

                // Now sort by natural priority:  highest priority first.
                let implies_approx_pred a b =
                    let bl = (f4o4 b) // natural_pri field
                    let al = (f4o4 a) // 
                    bl-al
                List.sortWith implies_approx_pred pp0
            let partiton = map add_fair_priority partition
            list_flatten (partiton)  // Sorted into fair order, so zipWithIndex makes this explicit.
            //let _:(rc_t * xidx_t list * xidx_t list list * int) list = partition
            
        let partition = // We prefer to restore static ordering from source file line number and so on before overriding with fair order or users urgency. This gives better consistency.
            let linepoint_ordering_pred (a, _, _, _) (b, _, _, _) =
                let get_srcfileref = function
                    | RC_rule rc -> rc.srcfileref
                    | _ -> sf "L310 other"
                let (LP(af, an), LP(bf, bn)) = (get_srcfileref a, get_srcfileref b)
                let ans =
                    if af = bf then an - bn
                    elif af < bf then -1 else 1
                if bobj.textual_reverse then -ans else ans
                    
            let sorted = partition //List.sortWith linepoint_ordering_pred partition  <---- TODO re-instate
            let add_textual_ordering ((ri, coms, rems, natural), textual) = (ri, coms, rems, natural, textual)
            map add_textual_ordering (zipWithIndex sorted)

        // Within groups that have the same fair ordering we use textual ordering (for compilation consistency if nothing else.)
        // Users descending_urgency has ultimate control.
        let partition =
            if nullp descending_urgency then
                vprintln 2 (msg + sprintf ": no descending_urgency markup to obey.") // could print this message on a per-partition basis.
                partition
            else
                sort_rules_according_to_users_urgency ww urgency_edges partition
            
        let _ = 
            let banner = sprintf "Rule Ordering for Rule Group '%s'" cls
            let rowgen = function
                | ((idl, coms_, rems_, fair_order, textual_order), final) ->
                    let urgs = not_nonep(op_assoc idl urgency_edge_list)
                    let urgd = memberp idl urgency_nodes_d
                    let urgf = (if urgs then "S" else "") + (if urgd then "D" else "")
                    [ hptos idl; sprintf "%i %i" (length coms_) (length rems_); i2s fair_order; i2s textual_order; urgf; i2s final ]
                //| _ -> ["???"]
            let rows = map rowgen (zipWithIndex partition)

            let table = tableprinter.create_table(banner, [ "Rule Name"; "Counts";  "Fair Order"; "Textual Ordering"; "Urgency"; "Final Order" ], rows)
//           let table = tableprinter.create_table(banner, [ "Rule Name"; "Counts";  "Fair Priority"; "Textual Ordering"; "Urgency"; "Final Order" ], rows)
            aux_report_log 2 bobj.stagename table


        let partition_with_guards =
            let pargrd arg =
                match arg with
                | (idl, coms_, rems_, fair_priority_, textural_priority_) -> 
                    let g1 =
                        match rule_map.TryFind idl with
                            | Some(idl, ks_, grd) -> grd
                            | None -> sf "L548"
                    (arg, g1)

                //| other -> sf (msg + sprintf ": other form in def_scheduler_r %A" other)

            map pargrd partition

        let (analysis, arbiter_o) =
            let rec simple_starve_check sofars guard = function // We shall offer a grander one in the paper. This implementation is just an initial placeholder.
                | [] -> []
                | ((idl, coms_, rems_, fair_priority_, textural_priority_), g1)::tt ->
                    let pfire = ix_and g1 guard

                    let pstarved = myfalse pfire
                    let rate_target =
                        match named_rules.TryFind idl with
                            | Some(RC_rule ri) -> ri.rulerec.rate_target
                            | _                -> None
                    let smsg = if bobj.add_simple_arbiters then "potential " else ""
                    if bobj.report_rule_fire_rates then
                        let shine = fuzzyprob ww () "pfire" pfire
                        mutadd m_fire_rates_report (idl, rate_target, shine)
                    if pstarved then
                        hpr_warn (msg + sprintf ": %s: starvation detected for rule " smsg + hptos idl)
                        hpr_warn (msg + sprintf ": earlier rules being greedy are " + sfold hptos sofars)
                    let guard = ix_and (xi_not g1) guard
                    (idl, pstarved) :: simple_starve_check (idl :: sofars) guard tt
                    
            let analysis = simple_starve_check [] X_true partition_with_guards
            let starve_count = length (List.filter snd analysis)
            let (smsg, arbiter_arity) =
                if starve_count = 0 then ("", 0)
                elif bobj.add_simple_arbiters then ("An arbiter will be generated owing to recipe setting -bsv-auto-arbiters=blah", starve_count+1)
                else ("No arbiter will be generated owing to recipe setting -bsv-auto-arbiters=disable", 0)
            vprintln (if starve_count>0 then 0 else 3) (sprintf "In partition %s, %i of the %i rules are potentially starving. %s" cls (starve_count) (length analysis) smsg)
            //if not_nullp pstarving then
            //    vprintln (sprintf "The following rules are potentially starving:")
            let arbiter_o =
                if arbiter_arity < 2 then None // TODO control locally
                elif bobj.round_robin_mode then Some(round_robin_arbiter_gen ww bobj clkinfo sh_emit cls arbiter_arity)
                else Some(simple_arbiter_gen ww bobj clkinfo sh_emit cls arbiter_arity)
            (analysis, arbiter_o)

        let partition_with_guards_and_credit = // faminef is a bool but will be a real for real-time scheduller!
            let rec balance faminef = function
                | [] -> []
                | [((r1, g1), _)] -> [(r1, g1, faminef)]
                | ((r0, g0), (_, _))::((r1, g1), (x1, f1))::tt  ->
                   let faminef = f1 || faminef
                   (r0, g0, faminef) :: balance faminef (((r1, g1), (x1, f1))::tt)
            balance false (List.zip partition_with_guards analysis)

        
        let rec gateclass summaries advancers sofars guard = function
            | ((idl, coms_, rems_, fair_priority_, textural_priority_), g1, faminef)::tt -> 

                if myfalse guard then
                    hpr_warn (msg + ": second starvation detected for rule " + hptos idl)
                    hpr_warn (msg + ": earlier rules being greedy are " + sfold hptos sofars)
                let go = ix_and resetbar g1
                let (arb, advancers) =
                    if not faminef then ([], advancers)
                    else match arbiter_o with
                            | None -> ([], advancers)
                            | Some (genf, advance_net) ->
                                let (req_o, ack) = genf()
                                if not_nonep req_o then mutadd sh_emit.comb_logic (gen_buf(xi_blift(valOf req_o), xi_blift(ix_and go guard)))
                                ([ ack ], go::advancers)

                let (fire_when_enabled, fireguard_o) =
                    match named_rules.TryFind idl with
                        | Some(RC_rule ri) -> (memberp SF_fire_when_enabled ri.shedats, Some ri.fireguard)
                        | _                -> (false, None)
                if fire_when_enabled then // TODO fold arbiter in and this warning will be flagged more often...
                    if not (mytrue guard) then
                        hpr_yikes (sprintf "+++ pragma fire_when_enabled seemingly failed by scheduler for rule %s. The following expression needs to always hold: guard=%s" (hptos idl) (xbToStr guard))
                let capoff = ix_andl (go::arb) // For round-robin, this includes go again, but that does not matter.                                            
                let fire = ix_and capoff guard // For round-robin, this includes guard again, but that does not matter.
                if myfalse fire then
                    //hpr_warn(msg + sprintf ": Rule CC '%s' iguard=%A" (htosp idl) ri.resource_records.igs)
                    hpr_warn(msg + sprintf ": Rule BB '%s' g1=%s" (htosp idl) (xbToStr g1))
                    hpr_warn(msg + sprintf ": Rule AA '%s' can never fire (intrinsic guard invalid) and is being discarded." (htosp idl))
                //dev_println(sprintf "Capoff rule %s FIRE=%s" (hptos idl) (xbToStr fire))

                if not_nonep fireguard_o then
                    mutadd sh_emit.comb_logic (gen_buf(valOf fireguard_o, xi_blift fire)) // local capoff if rule and not exported method.
                else
                    //dev_println (sprintf "sort out capoff of external method please " + hptos idl)
                    ()

                let alwaysf = mytrue guard
                let arbiterf = faminef && not_nonep arbiter_o
                let guard =
                    if arbiterf then guard // If being starved, leave the arbitration to the arbiters where present, otherwise degrade as usual.
                    else ix_and (xi_not capoff) guard
                let summary = (idl, (cls, alwaysf, arbiterf, faminef, fire_when_enabled))
                gateclass (summary::summaries) advancers (idl :: sofars) guard tt

            //other::tt ->  hpr_yikes (msg + sprintf ": other form in def_scheduler_r %A" other); gateclass advancers sofars guard tt
            | [] -> (summaries, advancers)

        let (summaries, advancers) = gateclass [] [] [] X_true partition_with_guards_and_credit
        let _ = 
            match arbiter_o with
                | Some (genf, Some advance_net) ->  // Drive simple arbiter advance net if any rules guarded by that arbiter are firing.
                    mutadd sh_emit.comb_logic (gen_buf(advance_net, xi_blift(ix_orl advancers)))
                | _ -> ()                

        summaries
                
#if OLD
    let def_solo_rule (cls_, (ri, _)) = // Capoff for rule not affected by schedulling.
        vprintln 3 (sprintf "def_solo_rule %s" (hptos ri.rulename))
        let fire = Map.fold igconjm X_true ri.resource_records.igs
        if myfalse fire then
            hpr_warn (msg + ": rule cannot fire (intrinsic guard never holds) rulename=" + hptos ri.rulename)
        mutadd sh_emit.comb_logic (gen_buf(ri.fireguard, xi_blift fire)) // local capoff        
        (ri.rulename, ("solo"))
#endif
    app def_scheduler_capoff_external_methods method_ctl  // External methods only when re-entrant internals.  

    let summaries = list_flatten(map def_scheduler_rules partitions)


    if true then
        let rule_report_line (idl, (cls, alwaysf, arbiterf, faminef, fire_when_enabled)) =
            let c0 = if faminef then "F" else "-"
            let c1 = if arbiterf then "-arbiter-" else "-"
            let c2 = if alwaysf  then "A" else "-"
            let c3 = if fire_when_enabled then "f" else "-"            
            [ cls; hptos idl; c0 + c1 + c2 + c3]
        let table = tableprinter.create_table("Rule Partitions And Conflicts.", ["Rule"; "Partition"; "Status Flags"], map rule_report_line summaries)
        aux_report_log 2 bobj.stagename table
        app (vprintln 1) table

    if bobj.report_rule_fire_rates then
        let shineline (idl, rate_target, shine) = [hptos idl; valOf_or_blank rate_target; sprintf "%1.2f" shine]
        let table = tableprinter.create_table("Rule Nominal Firing Rates.", ["Rule"; "Target"; "Actual"], map shineline !m_fire_rates_report)
        aux_report_log 2 bobj.stagename table
        app (vprintln 1) table
    ()

//
// eof  
