// CBG SQUANDERER : Rudimentary/Toy Bluespec Verilog Compiler: Coded in F#.
// (C) 2012 DJ Greaves, University of Cambridge.

(* All rights reserved, except as licensed by LGPL below.
 * DJ Greaves, University of Cambridge, Computer Laboratory.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *)


// $Id: cambridgetech.fs,v 1.1 2013-08-28 11:39:34 djg11 Exp $

module cambridgetech

open System.Collections.Generic;
open System.Numerics

open moscow
open meox
open yout
open opath_hdr
open hprls_hdr
open linepoint_hdr
open abstract_hdr
open abstracte


open lex_bsvc
open tydefs_bsvc
open bsvc

let xforml = function
    | Tid(nf, id) -> Bsv_ty_id(nf, Some P_defn1, (id, (* fresh_bindindex*) "xforml"))  // A defintion of a maker.

//
type rpb =
    {
      asynch:     bool
      dreg:       bool
      initval:    bool option
      config:     bool
      shedstring: string
      iotype:     varmode_t
      signed:     netst_t   // The currency generator needs also to know about signed/unsigned.
    }


let g_reg_shed_annotation_string = "_read _write : CF SB SA SBR" // * A write is sequenced after (SA) a read. * A read is sequenced before (SB) a write.

let g_configReg_shed_annotation_string = "_read _write : CF CF CF SBR" // Read and write order not strictly observed for a config register.


// Should the Wire also be built in like the Reg is ?
let mk_kernel_Reg ww name package dummy_ = 
    let vd = 3
    let qm = "cambridgetech implement builtin " + name
    let rpb =
        {
                        asynch=   false
                        initval=  None
                        dreg=     false
                        iotype=   LOCAL
                        config=   false // not needed now we have shedstring
                        shedstring=g_reg_shed_annotation_string // Read these from markup in the prelude or can it here?
                        signed=   Signed
        }
    let props =
        match name with
            | "mkReg"    -> { rpb with initval=Some true }
            | "mkRegU"   -> { rpb with signed=Unsigned }
            | "mkRegA"   -> { rpb with initval=Some true; asynch=true }

            | "mkDReg"     -> { rpb with initval=Some true; dreg=true }
            | "mkDRegU"    -> { rpb with dreg=true; signed=Unsigned }
            | "mkDRegA"    -> { rpb with initval=Some true; asynch=true; dreg=true }

            | "mkConfigReg"     -> { rpb with initval=Some true; config=true; shedstring=g_configReg_shed_annotation_string }
            | "mkConfigRegU"    -> { rpb with config=true; shedstring=g_configReg_shed_annotation_string; signed=Unsigned }
            | "mkConfigRegA"    -> { rpb with initval=Some true; asynch=true; config=true; shedstring=g_configReg_shed_annotation_string }

            | "mkNonBlockingInputReg" -> { rpb with asynch=true; config=true; shedstring=g_configReg_shed_annotation_string; iotype=INPUT }
            | "mkNonBlockingOutputReg" -> { rpb with initval=Some true; asynch=true; config=true; shedstring=g_configReg_shed_annotation_string; iotype=OUTPUT }

            | other -> sf (qm + " variation known as " + other + " not implemented")

    let arity =
        match props.initval with
            | None   -> -1 // Minus 1 denotes a constant monad that does not need to be applied. Like mkRegU. TODO xcheck this
            | Some _ -> 1

    let maker_type =
        let alpha_v = Tid(VP_value(false,  ["prct-alpha"]), "prct-alpha")
        let alpha_t = Tid(VP_type, "prct-alpha")        
        let reg_ifc = Bsv_ty_nom({g_blank_nom_typ_ats with nomid=["Reg"; "Prelude"]; cls=Prov_ifc }, F_ifc_temp, [alpha_t])
        let idl = ["do"; "name"; "cambridge"]
        let do_ty = Bsv_ty_nom({g_blank_nom_typ_ats with nomid=idl; cls=Prov_method }, F_fun(g_ty_sum_monad,  {g_blank_fid with fids=["foo"];  methodname=htosp idl }, [reg_ifc]), freshlister ww g_ty_sum_monad [reg_ifc])
        if true || arity > 0
           then
               let arg_tys = map xforml [alpha_v]
               Bsv_ty_nom({g_blank_nom_typ_ats with nomid=[name; "cambridge"]; cls=Prov_method }, F_fun(do_ty, { g_blank_fid with methodname=htosp[name; "cambridge"]; fids=["prct-alpha"] }, arg_tys), freshlister ww do_ty arg_tys)
           else do_ty 

    //  vprintln 0 ("Summary type for " + name + " is " + bsv_sumToStr maker_type)

    let mk ww bobj emits_o (ee:ee_t) (dirinfo, prefix, pargs, iargs, siggy) =
#if SPARE
        let maker_type_ =
            let ded = ["Reg" ]
            match ee.genenv.TryFind ded with
                | None ->
                    let r = ref []
                    for z in ee.genenv do mutadd r z.Key done 
                    vprintln 0 (htosp ded + " was not found in the env. All=" + sfold htosp !r)
                    cleanexit "Prelude.Reg interface must be declared in prelude before registers can be rezzed"

                | Some(GE_binding(_, _, EE_typeAbs(ifc_ty, sum_ty))) -> sum_ty
                | Some other -> cleanexit(sprintf "Reg interface defined wrongly in prelude: %A"  other)
#endif

        let prefixs = htosp prefix
        match siggy with
            | [(ifc_name, ifc_currency)] ->
                let mm = "mkCambridgeReg: create reg " + prefixs
                let mf() = mm
                let ww = WF 3 "mkCambridgeReg" ww mm
                let igenerics =
                    match ifc_currency with
                        | Tfa(fa, _)  -> fa
                if vd>=6 then vprintln 6 (mm + sprintf ": igenerics  %A" igenerics)
                if length igenerics <> 1 then sf (qm + ": Expected exactly one arg")
                let splot = snd(hd igenerics) //op_assoc "prct" igenerics // Name 'prct' must match name in Prelude
                //vprintln 0 (qm + sprintf ": ifc_currency is %A" ifc_currency)
                let d_width = enc_width_e ww mf splot //(valOf_or_failf (fun () -> mm +  sprintf ": width not determined for register '%s'" prefixs) splot)

                let initval =
                    match props.initval  with
                        | None -> None
                        | Some _ ->
                            let x = snd (valOf_or_fail (mm + ": " + name + ": Reg initial value formal not called 'initial_value'") (op_assoc "initial_value" pargs))
                            match x with
                                | EE_typef(_, Bsv_ty_dontcare _) -> None
                                | x ->
                                    let iv = bsv_lower ww bobj (bsv_valueof_lifted_core mf x)
                                    // asynch wants constant - synch reset is happy to be variable?
                                    //if constantp iv then () else cleanexit(mm + ": " + name + ": Constant value needed for register reset value in " + htosp ifc_name)
                                    vprintln 3 (mm + ": Initial value is " + xToStr iv) // + sprintf ": pargs=%A" pargs)
                                    Some iv 

                if vd>=5 then vprintln 5 (sprintf "init complete %A" (initval))

                let r_if = find_action_or_method "_read" ifc_currency
                let rreg = find_rv qm r_if
#if OLD_EXPLICIT_RESETS
                // The initval needs to be part of the net's rez and reset action is implemented by gbuild and other code renderers, not in here.
                let gflop = if nonep initval then gen_dflop(xi_orred en, rreg, din)
                        else gen_dflop(ix_or (xi_orred en) reset_expr, rreg, ix_query reset_expr (valOf initval) din)
#else
                let _ = 
                    if props.iotype = INPUT then
                        let termname = prefixs // + "_in"
                        let the_input = ionet_w(termname, d_width, props.iotype, props.signed, [])
                        match emits_o with
                            | None -> ()
                            | Some emits ->
                                mutadd emits.nets the_input
                                mutadd emits.comb_logic (gen_bufif(rreg, the_input, X_true))

                    else
                        let w_if = find_action_or_method "_write" ifc_currency
                        let din = f2o3(select_nth 0 (find_args qm w_if))
                        let en = find_en w_if
                        if not_nonep initval then set_net_resetval "mkReg" (valOf initval) rreg
                        let gflop =  [gen_dflop(xi_orred en, rreg, din) ]
                        match emits_o with
                            | None -> ()
                            | Some emits ->
                                mutadd emits.synch_logic (dirinfo, gflop)

#endif
    // The initval for the currency output net was set earlier.
    // When the initval is now found to be non zero, we could generate a fresh local register and make a buffer assign to the currency net, so that the currency net's initval is never used going forward, but for now we note the
    // initval is in the f2 mutable component and mutate the f2 for the currency net.

                if props.iotype = OUTPUT then
                    let termname = prefixs //  + "_out"
                    let the_output = ionet_w(termname, d_width, props.iotype, props.signed, [])
                    match emits_o with
                        | None -> ()
                        | Some emits ->
                            mutadd emits.nets the_output
                            mutadd emits.comb_logic (gen_bufif(the_output, rreg, X_true))
                    ()
                vprintln 3 (sprintf "Created register %s %A with width %i bits."  prefixs props.iotype d_width)
                []
            | other -> sf (qm + sprintf ": L185 other %A" other)
    (register_hardstate [name; package ], mk, (maker_type, 0)) // end of mk_kernel_Reg

type wire_attributes_t =
    | R_ar // read always ready
    | W_ar // write always ready
    //| R_maybe // read port is maybe option
    | R_send_read
    | R_setget
    | R_nodata

//
// The Bluespec wires allow us to pass information between methods and rules combinationally, but they forget/reset their value at the end of a 'cycle'.
//   
// They are implemented fully within the main body of the compiler and its currency but we must have the correct execution order.
//
// The associated logic generated here in cambridgetech is not needed. It is just for visualisation under a net-level debugger.
//
// TODO CAMRET augment white paper: Wires that are driven more than once per clock cycle must indeed be implemented in the currency, but perhaps that is not allowed in standard Bluespec. We can easily support it.
//
let mk_kernel_Wire ww name package dummy_ = 
    let qm = "mk_kernel_Wire"
    let (ctor_arity, pulse_flag, safe_flag, rflag, root, xts, ifc_name) =
        match name with

            | "mkWire"         -> (0, false, true, false,  "wire", [], "Wire")  // Wire interface has methods _read and _write. The Wire is the same as RWire but valid bit is the intrinsic guard.
            | "mkUnsafeWire"   -> (0, false, false, false, "wire", [], "Wire")

            | "mkPulseWire"    -> (0, false, true, true, "pulsewire", [R_nodata; R_send_read], "PulseWire")  // PulseWire is an RWire without data. Methods are send() and _read().
//          | "mkPulseWireOr"  -> (true, . ., "wireOr", [R_nodata; R_send_read])         

            | "mkRWire"        -> (0, false, true, true,  "rwire", [ R_setget], "RWire")   // RWire wset and wget.  This is the 'baseline' wire type and others are specialisms of it?
            | "mkUnsafeRWire"  -> (0, false, false, true, "rwire", [ R_setget], "RWire")   // The read result is one bit wider than written owing to maybe wrapper.

            | "mkBypassWire"   -> (0, false, true, false, "bypasswire", [], "Wire")  // Bypass wire uses Wire/Reg interface but the write method is always enabled.


(*            DWire is an implementation of the Wire interface where the _read
method is an always_ready method and thus has no implicit
conditions. Unlike the BypassWire however, the _write method need not
be always enabled. On cycles when a DWire is written to, the _read
method returns that value. On cycles when no value is written, the
_read method instead returns a default value that isspecified as an
argument during instantiation.
*)
            | "mkDWire"        -> (1, false, true, false,  "dwire", [], "Wire")  // DWire uses Wire/Reg interface. Has a default value returned when not ready.
            | "mkUnsafeDWire"  -> (1, false, false, false, "dwire", [R_ar], "Wire")

            | other -> sf ("Failed cambridgetech wire frame: cannot make a " + other)

    let maker_type =
        let alpha_t = Tid(VP_type, "prct-alpha")
        let alpha_v = Tid(VP_value(false,  ["prct-alpha"]), "prct-alpha")
        let reg_ifc = Bsv_ty_nom({g_blank_nom_typ_ats with nomid=[ifc_name; "Prelude"]; cls=Prov_ifc }, F_ifc_temp, [alpha_t])
        let do_ty = 
            let idl = ["do"; "name"; "cambridge"]
            Bsv_ty_nom({g_blank_nom_typ_ats with nomid=idl; cls=Prov_method }, F_fun(g_ty_sum_monad, {g_blank_fid with methodname=htosp idl; fids=["foo"]}, [reg_ifc]), freshlister ww g_ty_sum_monad [reg_ifc])
        if ctor_arity=1 then
            let idl = [name; "cambridge"]
            let arg_tys = map xforml [alpha_v]
            Bsv_ty_nom({g_blank_nom_typ_ats with nomid=idl; cls=Prov_method }, F_fun(do_ty, {g_blank_fid with methodname=htosp idl; fids=["prct-alpha"]; }, arg_tys), freshlister ww do_ty arg_tys)
        else
            let idl = [name; "cambridge"]
            Bsv_ty_nom({g_blank_nom_typ_ats with nomid=idl; cls=Prov_method }, F_fun(do_ty, {g_blank_fid with methodname=htosp idl; fids=["prct-alpha"]; }, []), freshlister ww do_ty []) // TODO: Not correct for regular BSV b?

    let mk ww bobj emits_o env (dirinfo, prefix, pargs, iargs, siggy) =
        let prefixs = htosp prefix
        match siggy with
            | [(ifc_name, ifc_currency)] ->
                let mm = "mkCambridgeWire: create wires of various sorts.  Wire type is " + name + "  prefix=" +  prefixs
                let ww = WF 3 ("mkCambridge") ww mm
                let igenerics =
                    match ifc_currency with
                        | Tfa(fa, _)  -> fa

                let nodata = memberp R_nodata xts
                let (wm_name, rm_name) =
                    if memberp R_setget xts then ("wset", "wget")
                    elif memberp R_send_read xts then ("send", "_read")                    
                    else ("_write", "_read")

                let (r_if, w_if) = (find_action_or_method rm_name ifc_currency, find_action_or_method wm_name ifc_currency)
                let wire_rv = find_rv mm r_if

                let (grd_net, data_width, data_net_o_) =
                    if nullp igenerics then
                        if not nodata then sf (mm + " no generics yet data bus needed")
                        (wire_rv, 0, None)
                    else
                        let typeo = snd(hd igenerics) //op_assoc "prct" igenerics // Name prct must match name in Prelude
                        let dwidth = enc_width_e ww (fun ()->mm) typeo // (valOf_or_failf (fun () -> m +  sprintf ": width not determined for '%s'" prefixs) splot)
                        (wire_rv, dwidth, None)

                //vprintln 0 (mm + sprintf "ifc_currency is %A" ifc_currency)
                //vprintln 0 (mm + sprintf ": igenerics is %A" igenerics)    

                let din =
                    if nodata then None
                    else Some(f2o3(select_nth 0 (find_args qm w_if)))

                let en = find_en w_if

                if not_memberp R_ar xts then
                    let r_rdy = find_rdy r_if
                    match emits_o with
                        | None -> ()
                        | Some emits ->
                            mutadd emits.synch_logic (dirinfo, [gen_buf(r_rdy, xi_blift X_true)])

                if not_memberp W_ar xts then
                    let w_rdy = find_rdy w_if
                    match emits_o with
                        | None -> ()
                        | Some emits ->
                            mutadd emits.synch_logic (dirinfo, [gen_buf(w_rdy, xi_blift X_true)])

                let thewire =
                    if nonep din then
                        gen_buf(wire_rv, en) // Not correct.
                    else
                        gen_buf(wire_rv, ix_bitor (valOf din) (ix_lshift en (xi_num data_width)))
                    
                //vprintln 3 ("Created wire " + prefixs + sprintf " geom %i  " width)
                match emits_o with
                    | None -> ()
                    | Some emits ->
                        mutadd emits.synch_logic (dirinfo, [thewire])
                []
    (register_hardstate [name; package ], mk, (maker_type, 0)) // end of mk_kernel_Wire


//
// Some synchronous SRAMs.  Rendered here as either BRAM or simple RTL arrays, but note that Orangepath has a 'restructure' module that will remove the RTL array and instead instantiate block RAMs and also retime the design as needed for longer latency components.
//
// With latency 0 on FPGA we make distributed (aka LUT) RAM.
// With latency 1 on FPGA we get BRAM.

// The normal Bluespec approach to a BRAM uses put/get with a read-FIFO on the output which is put there by BRAM.bsv.
//
// The DJG extension for register forwarded BRAMs avoids the FIFO (see white paper).    


// The prototype file in camlib/BRAMs.v specifies these as always ready and that read is always enabled, so no handshaking wires are found here.   
//
let mk_kernel_Ram ww name package dummy_ = 
    let ifc_name = "SSRAM_Cambridge_SP1D"  // Single-ported.
    let maker_type =
        let aty_v = Tid(VP_value(false,  ["ram-aty"]), "ram-aty")
        let dty_v = Tid(VP_value(false,  ["ram-dty"]), "ram-dty")       
        let aty_t = Tid(VP_type,  "ram-aty")
        let dty_t = Tid(VP_type,  "ram-dty")       
        let ram_ifc = Bsv_ty_nom({g_blank_nom_typ_ats with nomid=[ifc_name; "Prelude"]; cls=Prov_ifc }, F_ifc_temp, [aty_t; dty_t])
        let do_ty =
            let idl = ["do"; "name"; "cambridge"]
            Bsv_ty_nom({g_blank_nom_typ_ats with nomid=idl; cls=Prov_method }, F_fun(g_ty_sum_monad, { g_blank_fid with methodname=htosp idl; fids=["foo"]; }, [ram_ifc]), freshlister ww g_ty_sum_monad [ram_ifc])
        let ans =
            let idl = [name; "cambridge"]
            let arg_tys = map xforml [aty_v; dty_v]

            Bsv_ty_nom({g_blank_nom_typ_ats with nomid=idl; cls=Prov_method }, F_fun(do_ty, { g_blank_fid with methodname=htosp idl; fids=["ram-aty"; "ram-dty"]; }, arg_tys), freshlister ww do_ty arg_tys)
        ans
        
    let mk ww bobj emits_o env (dirinfo, prefix, pargs, iargs, siggy) =
        let prefixs = htosp prefix
        match siggy with
            | [(ifc_name, ifc_currency)] ->
                let qm = "mkCambridgeRam: create RAM " + prefixs
                let ww = WF 3 "mkCambridgeRam" ww qm
                let mf() = qm
                let igenerics =
                    match ifc_currency with
                        | Tfa(fa, _) -> fa
                //vprintln 0 (qm + sprintf ": igenerics is %A" igenerics)
                // Positional binding: Address comes first, data comes second.
                
                //let a_splot = (op_assoc "address_tci" igenerics)
                //let d_splot = (op_assoc "content_tci" igenerics)                
                let (a_splot, d_splot) =
                    match igenerics with
                        | [(_, a); (_, b)] -> (a,b)
                        | other -> cleanexit(qm + " expected exactly two parameters to Cambridge RAM maker (a_width and d_width)")
                //let d_splot = (op_assoc "content_tci" igenerics)                

                //vprintln 0 (qm + sprintf ": ifc_currency is %A" ifc_currency)
                let a_width = enc_width_e ww mf a_splot//(valOf_or_failf (fun () -> m + sprintf ": address width not determined for builtin RAM '%s'" prefixs) a_splot)
                let d_width = enc_width_e ww mf d_splot//(valOf_or_failf (fun () -> m + sprintf ": data width not determined for builtin RAM '%s'" prefixs) d_splot)
                let length = int32(two_to_the a_width)
                let (latency, ports) =
                    match name with
                        | "mkSSRAM_Cambridge_SP0D" -> (0, 1)
                        | "mkSSRAM_Cambridge_DP0D" -> (0, 2)
                        | "mkSSRAM_Cambridge_SP1D" -> (1, 1)
                        | "mkSSRAM_Cambridge_DP1D" -> (1, 2)
                        | other -> sf (qm + " variation known as " + other + " not implemented")
                vprintln 3 (qm + sprintf ": prams collated: aw=%i dw=%i length=%i latency=%i ports=%i" a_width d_width length latency ports)  

                let gec_port port_no =
                    let prefixs = if port_no < 0 then prefixs else prefixs + "_" + i2s port_no
                    let address_bus = vectornet_ws(prefixs + "_addr", a_width, Unsigned)
                    let theram = arraynet_w(prefixs, [int64 length], d_width)
                    match emits_o with
                        | None -> ()
                        | Some emits ->
                            mutadd emits.nets theram
                            mutadd emits.nets address_bus
                    let (r_if, w_if) = (find_action_or_method "native_read" ifc_currency, find_action_or_method "native_write" ifc_currency)
                    let r_ain = f2o3(select_nth 0 (find_args qm r_if))
                    let w_ain = f2o3(select_nth 0 (find_args qm w_if))
                    let w_din = f2o3(select_nth 1 (find_args qm w_if))
                    let wen = find_en w_if
                    let rd_rv = find_rv qm r_if
                    // These next two lines make the RAM itself in RAM inference form (not the cvipgen CV_SP_SSRAM_FL1 etc structural forms).
                    // For latency=1 BRAM RAM inference to work we need one address bus per port and the read operation to be registered.
                    let wenb = xi_orred wen
                    let address_mux = gen_bufif(address_bus, ix_query wenb w_ain r_ain, X_true) 
                    let write = gen_dflop(wenb, ix_asubsc theram address_bus, w_din)
                    // For synchronous read we need one output buffer.  If latency > 1 then need further pipeline stages... TODO.

                    let read =
                        match latency with
                            | 0 -> gen_bufif(rd_rv, ix_asubsc theram address_bus, X_true)
                            | 1 -> gen_dflop(xi_not(xi_orred wen), rd_rv, ix_asubsc theram address_bus) // Can Bluespec make use of the read old and write new primitive? 

                            | _ -> sf(sprintf "BRAM latency latency>1 missing but easily added. latenct=%i" latency)
                    [write; read; address_mux]

                let bobs = if ports < 2 then gec_port -1 else list_flatten (map gec_port [0 .. ports-1])
                vprintln 3 (sprintf "Created RAM %s ports=%i " prefixs ports + sprintf " geom %i x %i " d_width length)
                match emits_o with
                    | None -> ()
                    | Some emits ->
                        mutadd emits.synch_logic (dirinfo, bobs)  // Both ports in common clock domain for now.  Should generalise.  Perhaps use cvipgen instances ?
                []
    (register_hardstate [name; package ], mk, (maker_type, 0)) // end of mk_kernel_Ram


// eof
        
