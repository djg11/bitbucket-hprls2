// CBG SQUANDERER : Rudimentary/Toy Bluespec Verilog Compiler: Coded in F#.
// (C) 2012 DJ Greaves, University of Cambridge. All rights reserved.

(* All rights reserved, except as licensed by LGPL below.
 * DJ Greaves, University of Cambridge, Computer Laboratory.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *)

// $Id: fsm_bsvc.fs,v 1.1 2013-08-28 11:39:34 djg11 Exp $

module fsm_bsvc

//
// The 'plugin' code in this file converts the FSM sublanguage to regular BSV.
// The concrete FSM syntax is a hardcoded language extension but this compiler could possibly be implemented in BSV?
//

open System.Collections.Generic;
open System.Numerics
open parser_types;

open moscow
open meox
open yout
open hprls_hdr
open linepoint_hdr
open tydefs_bsvc
open bsvc

let g_mkReg ww (bobj:bsvc_free_vars_t) ee lp (evc, idl, width, initval) =  // a local copy of the mkReg primitive 
    let vd = 3
    let mm = ("g_mkReg@" + htosp idl)
    let ww = WF 3 mm ww "start"
    //let rty = Bsv_ty_uint{signed=Some Signed; width=width; oldname=Bsv_ty_dontcare; }
    let fresh_textid m = funique "Z" + "_" + m
    let uid = fresh_textid mm
    let lhs_ncp = extend_callpath bobj "ECP28" uid ee.callpath
    let ifc_ty =
        match !g_reg_backdoor with 
            | Some ((Bsv_ty_nom(_, _, [tyarg])) as sty) ->
                match tyarg with
                    | Tid(_, fid) ->
                        let kickoff = Some [ (fid, Fresher_ty (mk_bitty width)) ]
                        afreshen_sty ww bobj mm kickoff sty
                    | Tty _ -> sf "unexpected L42"
                    
            | None -> sf "reg backdoor not defined yet"
    let gf = //B_modgen(["mkReg"], None)
        let qidl = ["mkReg"]
        match ee.genenv.TryFind qidl with 
            | None -> cleanexit(sprintf " %s not bound" (htosp idl))
            | Some(GE_binding(_, _, EE_e(sty_, modgen))) ->  // Perhaps dont need backdoor if we can get the type from this lookup:
                //if vd>=4 then vprintln 4 (sprintf "Need to cf %A with %A" x !g_reg_backdoor)
                modgen //B_modgen(idl, Some modgen)
            | moda -> sf ("g_mkReg" + sprintf ": %s bound to the following as a module mk: %A" (htosp idl) moda)

    [
        Bsv_eascActionStmt(B_moduleInst(idl, (evc, idl, lhs_ncp), idl, B_applyf(PI_bno(gf, None), Bsv_ty_dontcare "L106FSM", [(Bsv_ty_intconst initval, B_hexp(Bsv_ty_intconst initval, xi_num initval))], tag_this_lp lp), (lp, uid)), Some lp); // The decl and assign get reversed again when used.
        Bsv_varDecl(false, ifc_ty, idl, None, (lp, uid));
    ]



// mutliply out the parallel operator as follows
//
//  Obvious rules
//     R1:   a;b || c;d;e                      -->  a||c; b||d  ; e
//
//     R2:   (if a then b else c) || d         --> if a then b||d else c||d
//
//   Example (if a then (b;p) else c) || d;e || f  --> if (a then (b||d;e);p else (c||d;e) || f
//                                                 --> if (a then (b||d;e);p else (c||d;e) || f  

type toy_t =
  | T_leaf
  | T_if of int * toy_t * toy_t
  | T_seq of toy_t * toy_t 
  | T_par of toy_t * toy_t
  | T_while of int * toy_t   

// The following code illustrates the distributive laws to force parallel constructs down to the bottom
let rec gen_T_par arg =
    match arg with 

    | (x, T_if(g,a,b)) -> T_if(g, gen_T_par(x, a), gen_T_par(x, b))
    | (T_if(g,a,b), y) -> T_if(g, gen_T_par(a, y), gen_T_par(b, y))

    | (T_seq(a, b), y) -> T_seq(gen_T_par(a, y), gen_T_par(b, y))
    | (x, T_seq(a, b)) -> T_seq(gen_T_par(x, a), gen_T_par(x, b))

//  | (T_while _, T_While(a, b)) -> sf "Two whiles placed in parallel not supported."
//  | (T_while(g, a), y) ->

    | (a, b) -> T_par(a, b)


let g_stmt_vty = Bsv_ty_id(VP_value(false,  ["Stmt"]),  None, ("Stmt",  (*fresh_bindindex*) "stmt_vty")) // This is another built-in type infact but we leave it as an unresolved variable for now... better to define a concrete type for it?

// Guard is prev_step
let mk_kernel_FSM ww name package dummy_ =
    let vd = 3
    let msg = "mk_Kernel_FSM"
    let ww = WN msg ww
    let provisos = []
    let lp = g_builtin_lp

    let ifc_name = "FSM";
    let maker_type =
        let fsm_ifc = Bsv_ty_nom({g_blank_nom_typ_ats with nomid=[ifc_name; "Prelude"]; cls=Prov_ifc }, F_ifc_temp, [])
        let do_ty =
            let idl = ["do"; "name"; "cambridge"]
            Bsv_ty_nom({g_blank_nom_typ_ats with nomid=idl; cls=Prov_method }, F_fun(g_ty_sum_monad,  { g_blank_fid with fids=["fsm"]; methodname=htosp idl; }, [fsm_ifc]), freshlister ww g_ty_sum_monad [fsm_ifc])
        let idl = [name; "cambridge"]
        Bsv_ty_nom({g_blank_nom_typ_ats with nomid=idl; cls=Prov_method }, F_fun(do_ty, { g_blank_fid with fids=["Stmt"]; methodname=htosp idl; }, [g_stmt_vty]), freshlister ww do_ty [g_stmt_vty])

    let fsm_mk ww0 bobj emits_o env (dirinfo, prefix, pargs, iargs, [(ifc_iname, actions)]) =

        //let evc = refine_dir_info ww vd msg clkinfo
        //
        let evc = dirinfo  // rename please evc

        let prefixs = htosp prefix
        //let long_iname = prefix
        let msg = ("mkFSM " + prefixs)
        let ww = WF 3 msg ww0 "start"
        //vprintln 0 ("All= " + sfold (fun (a,(ty, b))->a + "->" + bsv_tnToStr b) pargs)
        let seq_phrase = snd (valOf_or_fail "mkFSM need seq_stmt argument" (op_assoc "seq_stmt" pargs))
        let (m_decls, m_rules, m_state_nets) = (ref [], ref [], ref [])
        let perhaps_eval x = x // TODO is an eval needed here?
        let id = prefixs + "_fsm_"

        let [ start_if; waitTillDone_if; done_if ] = map (fun x-> find_action_or_method x actions) ["start"; "waitTillDone"; "done"]
        let start_en = find_en start_if // This provides the leading one-hot one.
        let rdy = find_rdy start_if
        let done_rv = find_rv msg done_if // TODO complete more of this wiring to the interface...
        let run_en = B_bexp X_true // This is a 'clock enable' for the FSM - strangely not exported through the FSM interface

        let bg_actionVal interfac action rt args = B_applyf(PI_bno(B_dyn_b(action, interfac), None), rt, args, tag_this_lp g_builtin_lp)
        let bg_read rt interfac = bg_actionVal interfac "_read" rt []
                //vprintln 0 ("pcvals = "  + sfold (fun x->x) pcvals)

        let rec foldins aa =
            match aa with
            | Bsv_repeatFsmStmt(s, lp) -> foldins (Bsv_whileFsmStmt(B_bexp X_true, s, lp))
            | Bsv_whileFsmStmt(g0, s, lp) -> Bsv_whileFsmStmt(g0, foldins s, lp)
            
            | Bsv_ifFsmStmt(g, true_clause, None, lp)    -> Bsv_ifFsmStmt(g, foldins true_clause, None, lp)
            | Bsv_ifFsmStmt(g, true_clause, Some ff, lp) -> Bsv_ifFsmStmt(g, foldins true_clause, Some(foldins ff), lp)  

            | Bsv_seqFsmStmt(a, b, lp) -> Bsv_seqFsmStmt(foldins a, foldins b, lp)

            | Bsv_parFsmStmt(x, y, lp) -> gen_par lp x y

            | Bsv_eascFsmStmt(h, stepname_, lp) -> aa
            | other -> muddy ("foldin: other form in sub-language: " + other.ToString())

        and gen_par lp a0 b0 =
            match (foldins a0, foldins b0) with
            | (x, Bsv_ifFsmStmt(g, a, Some b, lpy)) -> Bsv_ifFsmStmt(g, gen_par lp x a, Some(gen_par lp x b), lpy)
            | (Bsv_ifFsmStmt(g, a, Some b, lpx), y) -> Bsv_ifFsmStmt(g, gen_par lp a y, Some(gen_par lp b y), lpx)

            | (x, Bsv_ifFsmStmt(g, a, None, lpy)) -> Bsv_ifFsmStmt(g, gen_par lp x a, Some x, lpy)
            | (Bsv_ifFsmStmt(g, a, None, lpx), y) -> Bsv_ifFsmStmt(g, gen_par lp a y, Some y, lpx)

            | (x, Bsv_seqFsmStmt(a, b, lpy)) -> Bsv_seqFsmStmt(gen_par lp x a, gen_par lp x b, lpy)
            | (Bsv_seqFsmStmt(a, b, lpx), y) -> Bsv_seqFsmStmt(gen_par lp a y, gen_par lp b y, lpx)            

            | (Bsv_whileFsmStmt(g, y, lpy), x)
            | (x, Bsv_whileFsmStmt(g, y, lpy)) -> muddy (lpToStr0 lp + " par construct with 'while' inside is not supported")

            | (a, b) -> Bsv_parFsmStmt(a, b, lp)

        let rec fsm_seq_step prev = function
            | [] -> prev
            | Bsv_ifFsmStmt(g, true_clause, false_optional_clause, lp)::t ->
                let g = (perhaps_eval g)
                let lpt = gen_B_and[g; prev]
                let lpf = gen_B_and[gen_B_not g; prev] 
                let x1 = fsm_seq_step lpt [true_clause]
                let x2 = if nonep false_optional_clause then lpf else fsm_seq_step lpf [valOf false_optional_clause]  
                let xy = gen_B_or[x1;x2]
                fsm_seq_step xy t                        

            | Bsv_whileFsmStmt(g0, s, lp)::t ->
                let xid = funique id
                let g = perhaps_eval g0
                let lp0 = g_net emits_o ([xid], 1, []) // activation net
                let lp1 = gen_B_and[g; prev; run_en]
                let n = fsm_seq_step lp1 [s]
                mutadd m_rules (Bsv_primBuffer(B_hexp(g_bool_ty, lp0), B_blift n, lp))
                let xy = gen_B_and[B_orred(B_hexp(g_bool_ty, lp0)); gen_B_not g] // exit on active and guard not holding.
                fsm_seq_step xy t

            | Bsv_seqFsmStmt(a, b, lp)::t -> fsm_seq_step prev (a :: b :: t)

            | Bsv_parFsmStmt(a, b, lp)::t -> // should now be at bot
                let r = fsm_par_step prev a b
                fsm_seq_step r t

            | Bsv_eascFsmStmt(h, stepname_, lp)::t ->
                let rulename = funique id // This is better than the flat rulenames.
                let rulerec = { rate_target=None; name=rulename; copycount=1 }
                let suf (rulerec:bsv_rulerec_t) ss = { rulerec with name= rulerec.name + ss }
                let oh_state_bit = [  "_OH"; rulename ]
                app (mutadd m_decls) (g_mkReg ww bobj env lp (evc, oh_state_bit, 1, 0))
                let i = B_ifc(PI_ifc_flat oh_state_bit, g_bool_ty)
                mutadd m_state_nets (bg_read g_bool_ty i)
                let state_bit_rd = B_orred(B_applyf(PI_bno(B_dyn_b("_read", i), None), g_bool_ty, [], tag_this_lp lp))
                let set = B_applyf(PI_bno(B_dyn_b("_write", i), None), g_ty_action, [(g_bool_ty, B_hexp(g_bool_ty, xi_num 1))], tag_this_lp lp)
                let clr = B_applyf(PI_bno(B_dyn_b("_write", i), None), g_ty_action, [(g_bool_ty, B_hexp(g_bool_ty, xi_num 0))], tag_this_lp lp)
                let got = gen_B_and[prev; run_en; gen_B_not state_bit_rd]
                // wrap this as a B_action ? TODO use B_action to combine these...
                mutadd m_rules (Bsv_rule(suf rulerec "_s", got, [ Bsv_eascActionStmt(h, Some lp); Bsv_eascActionStmt(set, Some lp)], [], lp))
                let c = fsm_seq_step (state_bit_rd) t
                mutadd m_rules (Bsv_rule(suf rulerec "_c", gen_B_and[run_en; c], [ Bsv_eascActionStmt(clr, Some lp)], [], lp))
                B_firing (rulename + "_s")
            | other::t -> muddy ("seq: other form in sub-language: " + other.ToString())
        and fsm_par_step prev a b =
            let rec parallelise aa cc =
                match aa with
                    | Bsv_parFsmStmt(a, b, lp) -> parallelise a (parallelise b cc)
                            
                    | other -> (fsm_seq_step prev [other])::cc // The collated parallel items are all now equal in duration can now be placed in an action block
                    //| other -> muddy ("par: other form in sub-language: " + other.ToString())
            gen_B_and(parallelise a (parallelise b [])) 

        let build_seq = function
            | EE_e(_, B_fsmStmt(seq_phrase0, pcvals)) ->
                let seq_phrase1 = foldins seq_phrase0
                fsm_seq_step (B_orred(B_hexp(g_bool_ty, start_en))) [seq_phrase1]
            | other -> sf ("mkFSM : expected seq sub-language phrase, not " + eeToStr other)


        let exit_ = build_seq seq_phrase
        let rdy1 = gen_B_not(gen_B_or (map (fun x-> B_orred x) !m_state_nets))
        //vprintln 0 ("rdy1= " + bsv_bexpToStr rdy1)
        mutadd m_rules (Bsv_primBuffer(B_hexp(g_bool_ty, rdy),     B_blift rdy1, lp))
        mutadd m_rules (Bsv_primBuffer(B_hexp(g_bool_ty, done_rv), B_blift rdy1, lp))        
        
        if true then
                reportx 3 "FSM builder result: " bsv_stmtToStr (!m_decls @ !m_rules)
        else
            vprintln 3 ("------------------------------------------------start")
            vprintln 3 ("FSM builder result: " + sfoldcr bsv_stmtToStr (!m_decls @ !m_rules))
            let ww = WF 3 ("Make fSM " + prefixs) ww0 "finish"
            vprintln 3 ("------------------------------------------------end")
            ()
        !m_decls @ !m_rules
    ([name; package ], fsm_mk, (maker_type, 0))

// eof
