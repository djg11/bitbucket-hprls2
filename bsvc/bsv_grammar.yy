// This file may contain copyright or proprietary information. 
// It was based on the following publicly online documents indexed by Google:
// http://www.bluespec.com/pdfs_and_docs/BluespecReferenceGuide04052.pdf
// http://csg.csail.mit.edu/6.S078/6_S078_2012_www/resources/reference-guide.pdf
// However, BSV programs found on the net had some further constructs that I have added my own clauses for.
//
// Bluespec BSV parser implemented in F# for fsyacc.
// David Greaves, University of Cambridge, UK.
//


// See also    https://github.com/cambridgehackers/bsvtokami/blob/master/src/main/antlr/bsvtokami/BSV.g4
// See also    https://github.com/rsnikhil/goParseBSV/blob/master/grammar.txt
//  http://www.expert-fsharp.com/Topics/FsYacc

%{

open yout
open moscow
open hprls_hdr
open linepoint_hdr
open parser_types



// To get a parser FSM trajectory trace change the next line to do something?
let debug (x:string) = ()//vprintln 0 ("GRM:" + x)

let ldebug f = () // debug(f())

let atoi32 (x:string) = System.Convert.ToInt32(x)

let gm_ty_stack = ref []     // Type variables in scope

let gm_constag_in_scope_list = ref []   // Constructor tags in scope - now not used.

let mktype (ty:string) = 
    //vprintln 0 (sprintf "mktype %s" ty)
    if ty.[0] >= 'a' && ty.[0] <= 'z' then mutaddonce gm_ty_stack ty
    ty

let mk_cons_tag lst = 
    let adder (name, ast_o_) =
        //vprintln 0 (sprintf "mk_cons_tag %s" ty)
        mutaddonce gm_constag_in_scope_list name
        ()
    app adder lst
    lst


// We use bison for debug output.
// Bison does not like at sign in what it thinks is 'C' code.
let make_bison_happy_append(l, r) = l @ r

let glp() = get_linepoint()

// Note line point
let nlp() =
    let xx = glp() 
    xx



// Restore type variables in scope to an earlier checkpoint.
let unwind (_, x) rr =
    gm_ty_stack := x.ty_stack_point
    rr

let parse_error (err_msg:string) =
    vprintln 0 ("parse error: " + err_msg)
    ()


// undefine this to use the silent default (ie add an underscore on its end):
let parse_error_rich = 
    let per (a:FSharp.Text.Parsing.ParseErrorContext<_>) =    
    //  per (a:Microsoft.FSharp.Text.Parsing.ParseErrorContext<_>) =
        if f_verbose() > 1 || true then  
            System.Console.Write("djg parse error rich: " + a.Message + " yacc states=")
            app (fun x -> (printf " %i" x)) a.StateStack
            System.Console.WriteLine()
            ()
    Some per


let fresh_textid(s) = (funique "$Z") + "_" + s

let prep_identifier = function
    | ss          -> KB_id(ss, (glp(), fresh_textid ss))


let mine = function
    | KB_id(id, _) -> id
    | x            -> "$FUZZ"

let op_textid arg =
    let s = 
        match arg with
        | KB_ab  x -> sprintf "%A" x 
        | KB_bop x -> sprintf "%A" x 
        | KB_op  x -> sprintf "%A" x 
        | KB_ast x -> mine x
        | KB_unop s -> s
    let ans = fresh_textid s
    //if ans = "Z248_V_minus" then sf "hit it Z248"
    ans

let module_easc(rhs, lp) =
    let ty = KB_id("Empty", (lp, g_null_uid))
    let lhs = KB_id (funique "_anon_", (lp, fresh_textid "module_easc"))
    KB_varDeclDo(Some ty, lhs, rhs, lp)


let gen_diadic(oo, ll, rr, id) = KB_functionCall(oo, [ll; rr], id) // Strict - call by value diadic operators only - not logand/logor normally - they should be converted to query expressions.
%}


%type <bsv_ast_t>        typeAssertion
%type <bsv_ast_t>       actionBlock
%type <bsv_ast_t>       actionValueBlock 
%type <bsv_ast_t list>  attributeInstance_list
%type <string>          attrName
%type <bsv_ast_t>       attrSpec
%type <bsv_ast_t list>  attrSpec_comma_list 
%type <string list>     backdoors
%type <bsv_ast_t>       beginEndExpr
%type <bsv_ast_t>       beginEndExprStmt 
%type <bsv_ast_t list>  beginEndExprStmt_list
%type <x_lifted_op_t>  binop5 binop6 binop7 binop8 binop9 binop10 binop11 binop12_nonstrict binop13_nonstrict binop14
%type <bsv_ast_t>        bitConcat
%type <string option>    colon_id_option
%type <string option>    colon_Id_option
%type <bsv_ast_t>        noncondExpr caseExpr condPredicate exprOrCondPattern
%type <bsv_ast_t>        const_pattern

%type <bsv_ast_t>                    moduleImpBeginEndStmt
%type <bsv_ast_t>                    moduleImpCase
%type <bsv_ast_t list * bsv_ast_t>        moduleImpCaseArm
%type <(bsv_ast_t list* bsv_ast_t) list>  moduleImpCaseArm_list
%type <bsv_ast_t>                    moduleImpIf
%type <bsv_ast_t>                    moduleImpStmt 
%type <bsv_ast_t list>               moduleImpStmt_list

%type <bsv_ast_t>                    actionBeginEndStmt
%type <bsv_ast_t>                    actionCase
%type <bsv_ast_t list * bsv_ast_t>        actionCaseArm
%type <(bsv_ast_t list * bsv_ast_t) list> actionCaseArm_list
%type <bsv_ast_t>                    actionIf
%type <bsv_ast_t>                    actionStmt 
%type <bsv_ast_t list>               actionStmt_list

%type <bsv_ast_t>                    actionValueBeginEndStmt
%type <bsv_ast_t>                    actionValueCase
%type <bsv_ast_t list * bsv_ast_t>        actionValueCaseArm
%type <(bsv_ast_t list * bsv_ast_t) list> actionValueCaseArm_list
%type <bsv_ast_t>                    actionValueIf
%type <bsv_ast_t>                    actionValueStmt 
%type <bsv_ast_t list>               actionValueStmt_list

%type <bsv_ast_t>                    fbodBeginEndStmt
%type <bsv_ast_t>                    fbodCase
%type <bsv_ast_t list * bsv_ast_t>        fbodCaseArm
%type <(bsv_ast_t list * bsv_ast_t) list> fbodCaseArm_list
%type <bsv_ast_t>                    fbodIf
%type <bsv_ast_t>                    fbodStmt 
%type <bsv_ast_t list>               fbodStmt_list

%type <string>                       type_function_name
%type <string list>      derives_list
// %type <x_lifted_op_t>        displayTaskName
%type <bsv_ast_t list>   exportDecl
%type <bsv_ast_t>        exportItem
%type <bsv_ast_t list>   exportItem_comma_list
%type <bsv_ast_t>        expression
%type <bsv_ast_t list>   expression_comma_list expression_comma_or_bras_list
%type <bsv_ast_t>        exprPrimary
%type <bsv_ast_t list>                             functionBody

%type <bsv_ast_t>                                  functionDef
%type <bsv_ast_typeformal_t>                       functionFormal
%type <bsv_ast_typeformal_t list>                  functionFormals_list 
%type <tk_t * bsv_ast_t>                           functionProto
%type <string>                                     ident enum_identifier
%type <(string * bsv_ast_t option) list>      enum_comma_list
%type <string list>      Identifier_comma_list 
%type <bsv_ast_t option> implicitCond
%type <bsv_ast_t>        importItem
%type <bsv_ast_t list>   importItem_comma_list
%type <bsv_ast_t list>   interfaceDeclMember_list
%type <bsv_ast_t>        interfaceDecl
%type <bsv_ast_t>        subinterfaceDecl
%type <bsv_ast_t>        interfaceDef00
%type <bsv_ast_t list>   interfaceDef_list
%type <bsv_ast_t>        interfaceExpr
%type <bsv_ast_t>        interfaceExpItem
%type <bsv_ast_t list>   interfaceExpItem_list

%type <bsv_ast_t>        letBinding
%type <bsv_ast_t>        lValue_ass 
%type <bsv_ast_t>        lValue_decl
%type <string * bsv_ast_t>          memberBind
%type <(string * bsv_ast_t) list>   memberBind_comma_list
%type <bsv_ast_t>                         methodDef
%type <bsv_ast_t option * string>         methodFormal
%type <(bsv_ast_t option * string) list>  methodFormals
%type <bsv_ast_t>                         methodProto
%type <bsv_ast_typeformal_t>          methodProtoFormal
%type <(bsv_ast_typeformal_t) list>    methodProtoFormals_list 
%type <bsv_ast_t>                         moduleActualArg
%type <bsv_ast_t list>                    moduleActualArgs 
%type <bsv_ast_typeformal_t list>         moduleArgs
%type <bsv_ast_t>                         moduleDef
%type <tk_t * bsv_ast_t>                  moduleSig
%type <bsv_ast_typeformal_t>              moduleParam 
%type <bsv_ast_typeformal_t list option>  moduleParams
%type <bsv_ast_typeformal_t list>         moduleParamsList_oom
%type <bsv_ast_t>                         moduleStmt
%type <bsv_ast_t list>                    moduleStmt_list

%type <bsv_ast_t>        operatorExpr5 operatorExpr6 operatorExpr7 operatorExpr8 operatorExpr9 operatorExpr10 operatorExpr11 operatorExpr12 operatorExpr13 operatorExpr14 operatorExpr15
%type <bsv_ast_t list>   overloadedDef
%type <bsv_ast_t>        packageDecl
%type <bsv_ast_t>        packageStmt packageStmt_ntr
%type <bsv_ast_t list>   packageStmt_list
%type <bsv_ast_t>        pattern
%type <bsv_ast_t>        gpattern
%type <bsv_ast_t list>   pattern_comma_list
%type <bsv_ast_t list>   gpattern_comma_list
%type <bsv_ast_t>        proviso
%type <bsv_ast_t list>   proviso_comma_list
%type <bsv_ast_t list>   provisos_list
%type <bsv_ast_t>        regWrite 
%type <bsv_ast_t>        returnStmt
%type <bsv_ast_t>        ruleDefn 
%type <bsv_ast_t list>   ruleAssertion_list
%type <bsv_ast_t list>   ruleBody 
%type <bsv_ast_t option> ruleCond_option 
%type <bsv_ast_t>        rulesExpr
%type <bsv_ast_t>        rulesStmt
%type <bsv_ast_t>        structExpr
%type <bsv_ast_t>        structMember 
%type <bsv_ast_t list>   structMember_list         
%type <bsv_ast_t>        structPattern
%type <string * bsv_ast_t>      structPattern_item
%type <(string * bsv_ast_t) list> structPattern_item_comma_list
%type <bsv_ast_t>        subStruct 
%type <bsv_ast_t>        subTagged 
%type <bsv_ast_t>        sublangExpr
%type <bsv_ast_t>        sublangExp2
%type <bsv_ast_t>        scheduleDef
%type <string list>      scheduleItemList
%type <bsv_ast_t list>   sublangExpr_list
%type <bsv_ast_t>        systemFunctionCall
%type <bsv_ast_t>        systemTaskCall
%type <bsv_ast_t>        taggedUnionExpr
%type <bsv_ast_t>        tuplePattern
%type <bsv_ast_t>        type       etype
%type <bsv_ast_t>        type_def   etype_def typeName
%type <bsv_ast_t option> type_def_option
%type <bsv_ast_typeformal_t list>   typeFormals
%type <bsv_ast_t>        typeclassDef 
%type <string>           typeclassIde
%type <bsv_ast_t>        typeclassInstanceDef
%type <bsv_ast_t list>   type_comma_list
%type <bsv_ast_t>        e2_type
%type <bsv_ast_t list>   e2_type_comma_list 
%type <bsv_ast_t list>   type_comma_list_def
%type <bsv_ast_t>        typeDef
%type <bsv_ast_typeformal_t list>   typeFormal_list
%type <bsv_ast_typeformal_t list>   typeFormal_list1
%type <string>           typeIde
%type <string>           typeIde_def
%type <string>           typeIdeUC
%type <string>           typeNat
%type <bsv_ast_t option> type_option
%type <bsv_ast_t>        unionMember
%type <bsv_ast_t list>   unionMember_list
%type <string>           unop
%type <bsv_ast_t>        varAssign
%type <bsv_ast_t>        varDeclAssign
%type <bsv_ast_t>        varODeclAssign
%type <bsv_ast_t>        varDeclDo
%type <bsv_ast_t>        varDecl
%type <bsv_ast_t>        varDo 
%type <bsv_ast_t list>   vflist 

%token S_TYPEASSERT_WARNING
%token <string>   S_SYSID S_IDC S_IDL  S_TY SP_stringLiteral
%token <string>   S_NAT
%token <bool * int  * string>   SX_INTLITERAL
%type <bsv_ast_t> SP_intLiteral
%type <tk_t> TK

// Preprocessor tokens: these are not encountered by yacc but are returned by lex in the current implementation.
%token P_ifdef P_endif P_else P_define P_include
%token <string> P_macro

%token DS_display
%token DS_displayb
%token DS_displayh
%token DS_displayo
%token DS_dumpoff
%token DS_dumpon
%token DS_dumpvars
%token DS_finish
%token DS_printtimescale
%token DS_stime
%token DS_format
%token DS_stop
%token DS_time
%token DS_write
%token DS_writeb
%token DS_writeh
%token DS_writeo



%token SSS_EOF
%token SS_LPAR
%token SS_LPARSTAR
%token SS_SLASHEQUALS2
%token SS_RPAR
%token SS_PLING
%token SS_FWDQUOTE
%token SS_COLON
%token SS_COLONCOLON
%token SS_COMMA
%token SS_DOT1
%token SS_DOTDOT
%token SS_DOTSTAR
%token SS_EQUALS
%token SS_HASH
%token SS_LBRA
%token SS_LBRACE
%token SS_LEFTARROW
%token SS_QUERY
// This token seems to be ignored by fsharp's yacc.
%token SS_RBRA
%token SS_RBRACE
%token SS_SEMI

%token SS_AMPAMPAMP

// nonassoc
%token SS_LSHIFT
%token SS_RSHIFT


%token SS_DLTD
%token SS_DGTD
%token SS_DLED
%token SS_DGED
%token SS_DEQD
%token SS_DNED



// Note the diadic operators ought to be declared in order of increasing precedence but fsyacc ignores this?
// Left precedence:
%token SS_STILE2
%token SS_AMPAMP
%token SS_STILE
%token SS_AMP
%token SS_CARET
%token SS_MINUS SS_PLUS
%token SS_PERCENT SS_SLASH SS_STAR
%token  S_TAdd  S_TSub  S_TMul  S_TDiv  S_TLog  S_TExp  S_TMax  S_TMin  S_TAbs S_SizeOf
%token SS_STARRPAR
%token SS_STARSTAR
%token S_action
%token S_actionvalue
%token S_begin
%token S_bit
%token S_case
%token S_clocked_by
%token S_default
%token S_dependencies
%token S_deriving
%token S_else
%token S_end
%token S_endaction
%token S_endactionvalue
%token S_endcase
%token S_endfunction
%token S_endinstance
%token S_endinterface
%token S_endmethod
%token S_endmodule
%token S_endpackage
%token S_endrule
%token S_endrules
%token S_endtypeclass
%token S_enum
%token S_export
%token S_function
%token S_for
%token S_if
%token S_import
%token S_instance
%token S_interface
%token S_int
%token S_let
%token S_matches
%token S_method
%token S_module
%token S_numeric
%token S_package
%token S_parameter
%token S_provisos
%token S_reset_by
%token S_return
%token S_rule
%token S_rules
%token S_schedule
%token S_struct
%token S_tagged
%token S_typeclass
%token S_type
%token S_typedef
%token S_union
%token S_void
%token S_valueof
%token S_valueOf
%token S_while

%token DS_BUILTIN
%token S_noAction

// Sequential sub-language:
%token S_await
%token S_seq
%token S_par
%token S_endseq
%token S_endpar

// Decending precedence.
%left SS_STARSTAR SS_SLASH SS_STAR SS_PERCENT SS_PLUS SS_MINUS  
%nonassoc SS_DLTD SS_DGTD SS_DLED SS_DGED SS_DEQD SS_DNED
%left SS_AMP SS_CARET SS_STILE SS_AMPAMP SS_STILE2 
%right SS_QUERY
%nonassoc DUMMY_LOWER_THAN_QUERY


%start packageStmt_list

%%



packageDecl: S_package S_IDC SS_SEMI exportDecl packageStmt_list
             S_endpackage colon_Id_option SSS_EOF { KB_package($2, $4, $5, $7) }
;

TK:  { let x = nlp() in (x, { lp= x; ty_stack_point= !gm_ty_stack }) }

// Any capitalisation of identifier:
ident:	S_IDL   { $1}
     |	S_IDC   { $1}
;


colon_id_option: /* Empty */         { None }  
               | SS_COLON S_IDL      { Some $2 }
;

colon_Id_option: /* Empty */        { None }  
               | SS_COLON S_TY      { debug("redux colon_Id_option " + $2); Some $2 }
               | SS_COLON S_IDC     { debug("redux colon_Id_option " + $2); Some $2 }
;

exportDecl: /* Empty */                               { [] }
          | S_export exportItem_comma_list SS_SEMI   { $2 }
;

exportItem_comma_list:  exportItem                                { [ $1 ] }
                     |  exportItem SS_COMMA exportItem_comma_list {  $1 :: $3 }
;


exportItem: ident  SS_LPAR SS_DOTDOT SS_RPAR       { KB_export($1, true, glp()) }  // Package names should be always S_IDC
          | ident                                  { KB_export($1, false, glp()) }
;

importItem_comma_list:  importItem                                { [ $1 ] }
                     |  importItem SS_COMMA importItem_comma_list {  $1 :: $3 }
;


importItem:  ident SS_COLONCOLON SS_STAR     { KB_import($1, None, glp()) }           // Package names should be always S_IDC
          |  ident SS_COLONCOLON ident       { KB_import($1, Some $3, glp()) }
;

packageStmt_list: /* Empty */                     { [] }
          | packageStmt packageStmt_list          { $1:: $2 }
;


packageStmt:  packageStmt_ntr  { ldebug (fun () -> sprintf "redux %A" $1); $1 }
;

packageStmt_ntr:
             attributeInstance_list TK packageStmt_ntr { KB_attribute($1, $3, fst $2) }
           | moduleDef           { $1 }
           | interfaceDecl       { $1 }
           | functionDef         { $1 }
           | S_import importItem_comma_list SS_SEMI     { KB_nestlist $2 }
           | varDecl                                    { $1 }
           | varAssign SS_SEMI                          { $1 }
           | varDeclAssign SS_SEMI                      { $1 }
           | typeDef                                    { $1 }
           | typeclassDef                               { $1 }
           | typeclassInstanceDef                       { $1 }
	   | packageDecl                                { $1 }
//         | externModuleImport                         { $1 }
;


attributeInstance_list: /* Empty */                                                        { [] } 
                      | SS_LPARSTAR attrSpec_comma_list SS_STARRPAR attributeInstance_list { make_bison_happy_append($2, $4) }
;


attrSpec_comma_list: /* Empty */  { [] }
                   | attrSpec     { [ $1 ] }
                   | attrSpec SS_COMMA attrSpec_comma_list   { $1::$3 }

;

attrSpec: attrName SS_EQUALS expression  { KB_attr_spec($1, Some $3) }
        | attrName                       { KB_attr_spec($1, None) }
;

attrName: S_IDC     { $1 }
        | S_IDL     { $1 }
;


methodProto: attributeInstance_list S_method type S_IDL SS_LPAR methodProtoFormals_list SS_RPAR provisos_list SS_SEMI { KB_methodProto($1, $3, $4, $6, $8, glp()) }
;

methodProtoFormals_list: /* Empty */                                         { [] } 
                       |  methodProtoFormal                                  { [$1] }
                       |  methodProtoFormal SS_COMMA methodProtoFormals_list { $1 :: $3 }
;


methodProtoFormal: etype S_IDL                                               { (SO_formal, Some $1, $2) } // TODO want _def here?
//               | S_IDL identifier                                          { (SO_none  "methodProtoFormal", Some "$uq_idl", KB_id($1, (glp(), fresh_textid $1)), $2) }
;

proviso_comma_list:                                         { [ ]     }
                  | SS_COMMA  proviso proviso_comma_list    {  $2::$3 }
        

provisos_list: /* empty */                                             { [] } 
             |  S_provisos SS_LPAR proviso proviso_comma_list SS_RPAR  { $3::$4 }
;

proviso: S_IDC SS_HASH SS_LPAR type_comma_list_def SS_RPAR                 { KB_proviso($1, $4) }
;

// We dont currently put S_IDL in general types without it being flagged as a type since this leads to other parser errors.  
e2_type:  // As found in RegFile where a lower case formal is an arg to a type function.
      S_IDL       { KB_type_dub(false, $1, [], glp()) }
    | type        { $1 }
;

e2_type_comma_list: 
       e2_type                                 { [ $1 ]    }
    |  e2_type  SS_COMMA e2_type_comma_list    {  $1 :: $3 }
;


type_comma_list:  type                           { [ $1 ]    }
               |  type  SS_COMMA type_comma_list {  $1 :: $3 }
;

//  Defining context.
type_comma_list_def:  type_def                               { [ $1 ]    }
                   |  type_def  SS_COMMA type_comma_list_def {  $1 :: $3 }
;



expression_comma_or_bras_list:  expression                                               { [ $1 ]    }
                             |  expression SS_COMMA expression_comma_or_bras_list        {  $1 :: $3 }
// reduce problem kills lots |  expression SS_RBRA SS_LBRA expression_comma_or_bras_list {  $1 :: $4 }
;

expression_comma_list:  expression                                { [ $1 ] }
                     |  expression SS_COMMA expression_comma_list {  $1 :: $3 }
;

letBinding: S_let TK S_IDL SS_EQUALS expression        { debug("redux: letBinding1 " + $3); KB_letBinding($3, $5, "=", (fst $2, fresh_textid $3)) }
          | S_let TK S_IDL SS_LEFTARROW expression     { debug("redux: letBinding2 " + $3); KB_letBinding($3, $5, "<-", (fst $2, fresh_textid $3)) }
;


varAssign: lValue_ass TK SS_EQUALS expression                            { KB_varDeclAssign(None, $1, Some $4, fst $2) }
//       | lValue_ass TK SS_LEFTARROW expression                         { KB_varDeclAssign(None, $1, Some $4, fst $2) }
;

// We want to use a type_def here in varDecl because of things like 
//     let entries = hi_index-lo_index+1;
//     Vector#(entries, Reg#(data_t)) rf_vector;
// where entries is a bound numeric variable.

//  but this seems to much up Test1b
//       Reg#(Bit#(15)) doner [2] ;

varDecl: type lValue_decl TK SS_SEMI                                 { unwind $3 (KB_varDeclAssign(Some $1, $2, None, fst $3)) }
;

varDeclAssign: type lValue_decl TK SS_EQUALS expression     { debug("redux: varDeclAssign"); KB_varDeclAssign(Some $1, $2, Some $5, fst $3) }
;

varODeclAssign: varDeclAssign   { $1 }
              | varAssign       { $1 }
;


typeDef: S_typedef TK S_enum SS_LBRACE enum_comma_list SS_RBRACE S_IDC derives_list SS_SEMI  { unwind $2 (KB_typedefEnum(mk_cons_tag $5, mktype $7, $8, fst $2)) }

       | S_typedef TK S_struct SS_LBRACE structMember_list SS_RBRACE type_def derives_list SS_SEMI  { unwind $2 (KB_typedefSTUnion("struct", $5, $7, $8, fst $2)) }

       | S_typedef TK S_union S_tagged SS_LBRACE unionMember_list  SS_RBRACE type_def derives_list SS_SEMI { unwind $2 (KB_typedefSTUnion("tunion", $6, $8, $9, fst $2)) }

       | S_typedef SP_intLiteral S_IDC SS_SEMI    { KB_typedefConst($3, $2, glp()) }
       | S_typedef SP_stringLiteral S_IDC SS_SEMI { KB_typedefConst($3, KB_stringLiteral $2, glp()) }
//typedefSynonym ::= S_typedef type typeDefType ;
//typeDefType ::= typeIde [ typeFormals ]
//typeFormals ::= # ( typeFormal { , typeFormal })
//typeFormal ::= [ S_numeric ] S_type typeIde

// You had once mixed up the type referencing and defining concepts but now it is clear.
    | S_typedef TK type type_def SS_SEMI                                                       { unwind $2 (KB_typedefSynonym($4, $3, fst $2)) } // No problem unwinding here since the defined type should be a captial S_IDC anyway.
;


Identifier_comma_list:  S_IDC                                { [ $1 ] }
                     |  S_IDC SS_COMMA Identifier_comma_list { $1 :: $3 }
;

enum_identifier:
      S_IDC       { $1 }
//  | S_CONSTAG   { $1 }   // If already in use in another enum. Also for robustness against any silly re-parsing/re-declaration.
;

enum_comma_list:
      enum_identifier                                               { [ ($1, None) ]      }
    | enum_identifier  SS_EQUALS expression                         { [ ($1, Some $3) ]   }
    | enum_identifier SS_COMMA enum_comma_list                      { ($1, None) :: $3    }
    | enum_identifier SS_EQUALS expression SS_COMMA enum_comma_list { ($1, Some $3) :: $5 }
;


derives_list: /* Empty */                                      { [] }
            | S_deriving SS_LPAR Identifier_comma_list SS_RPAR { debug("redux: derives_list reduced"); $3 }
;



//Defining
structMember_list: /* Empty */                      { [] }
                 | DS_BUILTIN                       { [ KB_builtin("struct") ] }
                 | structMember structMember_list   { $1 :: $2 }
;

//Defining
unionMember_list: /* Empty */                      { [] }
                 | unionMember unionMember_list    { $1 :: $2 }
;

//Defining : TODO subtagged etc
structMember: type_def S_IDL SS_SEMI     { KB_structMember($1, $2) }
            | subTagged                  { KB_structMemberSubTagged($1) }
;



unionMember:  type_def typeIde SS_SEMI   { KB_unionMember(Some $1, $2) }
           |  S_void typeIde SS_SEMI     { KB_unionMember(None,    $2) }
           |  subStruct                  { $1 }
           |  subTagged                  { $1 }
;

//Defining
subStruct: S_struct         SS_LBRACE structMember_list SS_RBRACE typeIde SS_SEMI  { KB_unionSubStruct($3, $5) }
;

//Defining
subTagged: S_union S_tagged SS_LBRACE unionMember_list SS_RBRACE typeIde SS_SEMI   { KB_structSubTagged($4, $6) }
;

typeclassDef: S_typeclass typeclassIde typeFormals provisos_list SS_SEMI
                      overloadedDef S_endtypeclass colon_Id_option { KB_typeclassDef($2, $3, $4, $6, $8, glp()) }
;

typeclassIde:  S_IDC { $1 }
;

overloadedDef: /* Empty */     { [] }
             | functionProto  SS_SEMI overloadedDef { (snd $1)::$3 }
             | moduleSig              overloadedDef { (snd $1)::$2 }
             | varDecl overloadedDef                { $1::$2 }
;

// Defining
functionFormal: etype_def S_IDL        { (SO_formal,                  Some $1, $2) }
              | S_type S_numeric S_IDL { (SO_numeric,                    None, $3) }
              | S_numeric S_type S_IDL { (SO_numeric,                    None, $3) }
              | S_type S_IDL           { (SO_type,                       None, $2) }
              | functionProto          { (SO_none "functionFormalProto", Some(snd $1), "$HOF") }
;


functionFormals_list: /* Empty */                                    { [] } 
                    | functionFormal  SS_COMMA functionFormals_list { $1 :: $3 }
                    | functionFormal                                { [$1] }
;


functionProto: S_function TK type_def S_IDL SS_LPAR functionFormals_list SS_RPAR provisos_list { ($2, KB_functionProto($3, $4, $6, $8, fst $2)) }
             | S_function TK type_def S_IDL provisos_list                                      { ($2, KB_functionProto($3, $4, [], $5, fst $2)) }
;


functionBody: actionValueBlock       { [KB_easc($1, glp())] }// Not correct?
            | actionBlock            { [KB_easc($1, glp())] }// Not correct?
            | fbodStmt_list          { $1 } 
;

functionDef: functionProto SS_SEMI TK functionBody S_endfunction colon_id_option { unwind (fst $1) (KB_functionDef3(snd $1, false, $4, $6, fst $3)) }
           | functionProto SS_EQUALS expression SS_SEMI                          { unwind (fst $1) (KB_functionDef3(snd $1, true,  [KB_returnStmt($3, (glp(), fresh_textid "RETURN"))], None, glp())) }
;


fbodStmt:         fbodIf                      { $1 }
                | fbodBeginEndStmt            { $1 }
                | fbodCase                    { $1 }
                | varDecl                     { $1 }
                | varAssign SS_SEMI           { $1 }
                | varDeclAssign SS_SEMI       { $1 }
                | varDo                       { $1 }
                | varDeclDo                   { $1 }
                | functionDef                 { $1 }
                | exprPrimary SS_SEMI         { KB_easc($1, glp())      } // For function call.
                | systemTaskCall              { KB_easc($1, glp()) }
//                | expression SS_SEMI          { KB_easc($1, glp()) }
                | returnStmt                  { $1 }
                | regWrite                    { $1 }
                | S_for TK SS_LPAR varODeclAssign SS_SEMI expression SS_SEMI varAssign SS_RPAR fbodStmt { KB_for($4, $6, $8, $10, fst $2) }
                | letBinding SS_SEMI         { $1 }
;



moduleActualArgs: moduleActualArg                           { [ $1 ]   }
                | moduleActualArg SS_COMMA moduleActualArgs { $1 :: $3 }
;

moduleActualArg: expression                           { $1 }
  	       | S_clocked_by expression              { KB_eventControl("clocked_by", $2, glp()); }
	       | S_reset_by expression                { KB_eventControl("reset_by", $2, glp());   }
;

returnStmt: S_return TK expression SS_SEMI      { KB_returnStmt($3, (fst $2, fresh_textid "RETURNSTMT")) }
;
 
systemFunctionCall: DS_time  { KB_systemFunction ("$time", [], (glp(), fresh_textid "$time")) }
                  | DS_stime { KB_systemFunction ("$stime", [], (glp(), fresh_textid "$stime")) }
                  | DS_format { KB_systemFunction ("$format", [], (glp(), fresh_textid "$stime")) }
                  | DS_format SS_LPAR expression_comma_list SS_RPAR{ KB_functionCall(*system*)(KB_dk "$format",   $3, (glp(), fresh_textid "$"))}
;

systemTaskCall:
// displayTaskName SS_LPAR expression_comma_list SS_RPAR SS_SEMI { KB_functionCall(*system*)($1, $3, (glp(), fresh_textid "$")) }

 	        S_SYSID SS_LPAR expression_comma_list SS_RPAR SS_SEMI { KB_functionCall(*system*)(KB_dk $1, $3, (glp(), fresh_textid $1)) }
 	      | S_SYSID SS_SEMI                                       { KB_functionCall(*system*)(KB_dk $1, [], (glp(), fresh_textid $1)) }
              | DS_printtimescale                     SS_SEMI { KB_functionCall(*system*)(KB_dk "$printtimescale", [], (glp(), fresh_textid "$")) }
              | DS_finish SS_LPAR expression SS_RPAR  SS_SEMI { KB_functionCall(*system*)(KB_dk "$finish",   [$3], (glp(), fresh_textid "$"))}
              | DS_finish                             SS_SEMI { KB_functionCall(*system*)(KB_dk "$finish",   [], (glp(), fresh_textid "$"))  }
              | DS_stop   SS_LPAR expression SS_RPAR  SS_SEMI { KB_functionCall(*system*)(KB_dk "$stop",     [$3], (glp(), fresh_textid "$"))}
              | DS_stop                               SS_SEMI { KB_functionCall(*system*)(KB_dk "$stop",     [], (glp(), fresh_textid "$"))  }
              | DS_dumpvars                           SS_SEMI { KB_functionCall(*system*)(KB_dk "$dumpvars", [], (glp(), fresh_textid "$")) }
              | DS_dumpon                             SS_SEMI { KB_functionCall(*system*)(KB_dk "$dumpon",   [], (glp(), fresh_textid "$")) }
              | DS_dumpoff                            SS_SEMI { KB_functionCall(*system*)(KB_dk "$dumpoff",  [], (glp(), fresh_textid "$")) }
;

/*
// not used
displayTaskName: DS_display      { KB_dk "$display" }
               | DS_displayb     { KB_dk "$displayb" }
               | DS_displayo     { KB_dk "$displayo" }
               | DS_displayh     { KB_dk "$displayh" }
               | DS_write        { KB_dk "$write" }
               | DS_writeb       { KB_dk "$writeb" }
               | DS_writeo       { KB_dk "$writeo" }
               | DS_writeh       { KB_dk "$writeh" }
;
*/

// <- Structural/generative assignment.
//    Also found in fbody?
varDeclDo: type lValue_decl SS_LEFTARROW TK expression SS_SEMI { KB_varDeclDo(Some $1, $2, $5, fst $4) }
;

// Action statement form.
varDo:    lValue_ass TK SS_LEFTARROW expression SS_SEMI      { KB_varDeclDo(None, $1, $4, fst $2) }
;


//Enter sublanguage on par or seq keyword.
sublangExpr: S_seq sublangExp2 sublangExpr_list S_endseq                { KB_sublang("seq", $2::$3, glp()) }
           | S_par sublangExp2 sublangExpr_list S_endpar                { KB_sublang("par", $2::$3, glp()) }
;


// This may not be correct or complete:
sublangExp2: S_seq sublangExp2 sublangExpr_list S_endseq                { KB_sublang("seq", $2::$3, glp()) }
           | S_par sublangExp2 sublangExpr_list S_endpar                { KB_sublang("par", $2::$3, glp()) }

           | S_while SS_LPAR expression SS_RPAR sublangExp2 { KB_while($3, $5, glp())      }
           | S_for TK SS_LPAR varODeclAssign SS_SEMI expression SS_SEMI sublangExp2 SS_RPAR sublangExp2 { KB_for($4, $6, $8, $10, fst $2) }
           | actionBlock                                    { $1                   }
           | regWrite                                       { $1                   }
           | S_if TK SS_LPAR expression SS_RPAR sublangExp2 S_else sublangExp2  { KB_if($4, $6, Some $8, fst $2) }
           | S_if TK SS_LPAR expression SS_RPAR sublangExp2                     { KB_if($4, $6, None,    fst $2) }
           | TK exprPrimary SS_SEMI                           { KB_easc($2, fst $1)   } // function call
           | S_await TK SS_LPAR expression SS_RPAR SS_SEMI     { KB_await($4, fst $2)  }
;

sublangExpr_list: /* Empty */                   { [ ]      }
                | sublangExp2 sublangExpr_list  { $1 :: $2 }
;

        

bitConcat: SS_LBRACE expression_comma_list SS_RBRACE  { KB_bitConcat($2, glp()) }
;




beginEndExpr: S_begin colon_id_option beginEndExprStmt_list expression S_end colon_id_option { KB_beginEndExpr($2, $3, $4, $6) }
;

beginEndExprStmt_list: /* Empty */                              { [] }
                     | beginEndExprStmt beginEndExprStmt_list  { $1 :: $2 }
;


beginEndExprStmt: varDecl                       { $1 }
                | varAssign SS_SEMI             { $1 }
                | varDeclAssign SS_SEMI         { $1 }
                | functionDef                   { $1 }
// next 3 KB_easc?
                | systemTaskCall                { KB_easc($1, glp()) }
                | expression SS_SEMI            { KB_easc($1, glp()) } // Hmmm want easc on these ?
;

// todo forces at least one
taggedUnionExpr: S_tagged S_IDC SS_LBRACE memberBind memberBind_comma_list SS_RBRACE   { KB_taggedUnionExpr($2, $4::$5, None, (glp(), fresh_textid $2)) }
               | S_tagged S_IDC expression                                             { KB_taggedUnionExpr($2, [], Some $3, (glp(), fresh_textid $2)) }
               | S_tagged S_IDC                                                        { KB_taggedUnionExpr($2, [], None, (glp(), fresh_textid $2)) } 
;


memberBind_comma_list: /* Empty */                               { []       }
                     | SS_COMMA memberBind memberBind_comma_list { $2 :: $3 }


memberBind: S_IDL SS_COLON expression { debug ("redux: memberBind+" + $1); ($1, $3) }
;

actionBlock: S_action colon_id_option actionStmt_list S_endaction colon_id_option    { debug "redux actionBlock"; KB_actionValueBlock("action", $2, $3, $5, (glp(), fresh_textid "AVALB")) }
// Having noAction hardwired precludes actions as first class values. Action noAction = (action endaction);
           | S_noAction  SS_SEMI { debug "redux noAction actionBlock"; KB_actionValueBlock("action", Some "noAction", [], None, (glp(), fresh_textid "AVALB")) }
;

actionStmt: 
            expression SS_SEMI       { KB_easc($1, glp()) } 
	  | attributeInstance_list TK actionStmt { KB_attribute($1, $3, fst $2) }
          | actionIf                 { $1 }
          | actionCase               { $1 }
          | actionBeginEndStmt       { $1 }
          | S_for TK SS_LPAR varODeclAssign  SS_SEMI expression SS_SEMI varAssign SS_RPAR actionStmt   { KB_for($4, $6, $8, $10, fst $2) }
          | S_while TK SS_LPAR expression SS_RPAR actionStmt { KB_while($4, $6, fst $2)      }
          | regWrite                 { $1 }
          | varDecl                  { $1 }
          | varAssign SS_SEMI        { $1 }
          | varDeclAssign SS_SEMI    { $1 }
          | varDo                    { $1 }
          | varDeclDo                { $1 }
          | letBinding SS_SEMI       { $1 } // A letBinding can be used wherever a variable decl and assignment can be used
          | systemTaskCall           { KB_easc($1, glp()) }
          | actionBlock              { KB_easc($1, glp()) } 
; 

// ASSIGNOP is <= dled
regWrite: lValue_ass TK SS_DLED expression SS_SEMI { KB_regWrite_raw($1, $4, (fst $2, fresh_textid "<=")) }
;

//begin generic contexts. contexts are: functionBody action moduleImp actionValue action

moduleImpIf: S_if TK SS_LPAR expression SS_RPAR moduleImpStmt S_else moduleImpStmt  { KB_if($4, $6, Some $8, fst $2) }
           | S_if TK SS_LPAR expression SS_RPAR moduleImpStmt                       { KB_if($4, $6, None, fst $2) }
;

moduleImpBeginEndStmt: S_begin colon_id_option moduleImpStmt_list S_end colon_id_option { KB_block($2, $3, $5, glp()) }
;

moduleImpStmt_list: /* Empty */                             { [] }
                  | moduleImpStmt moduleImpStmt_list        { $1:: $2 } 
;

moduleImpCase: S_case SS_LPAR expression SS_RPAR matches_optional  moduleImpCaseArm_list S_endcase { KB_case($3, $6, glp()) }
;


moduleImpCaseArm_list: /* Empty */  { [] }
                 |  moduleImpCaseArm moduleImpCaseArm_list  { $1 :: $2 }
;


moduleImpCaseArm: gpattern gpattern_comma_list SS_COLON moduleImpStmt   { ($1::$2, $4) }
                | S_default SS_COLON moduleImpStmt                      { ([], $3) }
;

// end moduleImp

fbodIf: S_if TK SS_LPAR expression SS_RPAR fbodStmt S_else fbodStmt  { KB_if($4, $6, Some $8, fst $2) }
      | S_if TK SS_LPAR expression SS_RPAR fbodStmt                  { KB_if($4, $6, None, fst $2)    }
;

fbodBeginEndStmt: S_begin colon_id_option fbodStmt_list S_end colon_id_option { KB_block($2, $3, $5, glp()) }
;

fbodStmt_list: /* Empty */                           { [] }
             | fbodStmt fbodStmt_list                { $1:: $2 }
;

fbodCase: S_case SS_LPAR expression SS_RPAR matches_optional  fbodCaseArm_list S_endcase { KB_case($3, $6, glp()) }
;


fbodCaseArm_list: /* Empty */  { [] }
                |  fbodCaseArm fbodCaseArm_list  { $1 :: $2 }
;


fbodCaseArm: gpattern gpattern_comma_list  SS_COLON fbodStmt   { ($1::$2, $4) }
            | S_default SS_COLON fbodStmt   { ([], $3) }
;

// end fbod

actionValueIf: S_if SS_LPAR expression SS_RPAR actionValueStmt S_else actionValueStmt  { KB_if($3, $5, Some $7, glp()) }
             | S_if SS_LPAR expression SS_RPAR actionValueStmt                   { KB_if($3, $5, None, glp()) }
;

actionValueBeginEndStmt: S_begin colon_id_option actionValueStmt_list S_end colon_id_option { KB_block($2, $3, $5, glp()) } 
;

actionValueStmt_list: /* Empty */                             { [] }
                    | actionValueStmt actionValueStmt_list    { $1:: $2 }
;

actionValueCase: S_case SS_LPAR expression SS_RPAR matches_optional  actionValueCaseArm_list S_endcase { KB_case($3, $6, glp()) }
;


actionValueCaseArm_list: /* Empty */  { [] }
                       |  actionValueCaseArm actionValueCaseArm_list  { $1 :: $2 }
;


actionValueCaseArm: gpattern gpattern_comma_list SS_COLON actionValueStmt   { ($1::$2, $4) }
                  | S_default SS_COLON actionValueStmt   { ([], $3) }
;

// end actionValue

actionIf: S_if TK SS_LPAR expression SS_RPAR actionStmt S_else actionStmt  { KB_if($4, $6, Some $8, fst $2) }
        | S_if TK SS_LPAR expression SS_RPAR actionStmt                    { KB_if($4, $6, None,    fst $2) }
;

actionBeginEndStmt: S_begin colon_id_option actionStmt_list S_end colon_id_option { KB_block($2, $3, $5, glp()) } 
;

actionStmt_list: /* Empty */                             { [] }
               | actionStmt actionStmt_list              { $1:: $2 }
;

actionCase: S_case SS_LPAR expression SS_RPAR matches_optional  actionCaseArm_list S_endcase { KB_case($3, $6, glp()) }
;

matches_optional: /*empty */ { }
                | S_matches  { }
;

actionCaseArm_list: /* Empty */  { [] }
                  |  actionCaseArm actionCaseArm_list  { $1 :: $2 }
;

actionCaseArm: gpattern gpattern_comma_list  SS_COLON actionStmt   { ($1::$2, $4) }
             | S_default SS_COLON actionStmt             { ([], $3) }
;
// end action



// Oct 2013: easc action statements being reduced as lvalue_assigns and hence failing.
//    we'll get rid of lvalues, both decl and assign, now and see what the interface expression problem is...

// Having a different form for lValues in assignments tends to require quite a bit of SLR lookahead when
// easc is a command as well (we won't know until we get to the assignment operator) so use expression here too.  NO NO - Make expression use lvalue!

//lValue_ass:  expression { ldebug (fun () -> sprintf "redux lValue_ass %A" $1); $1}

// interfaceExpr can get parsed where it is meant to be an interface decl e.g. at top level where an expression is being parsed as the lhs of an elaborated assignment.
// or  as lValue_ass as part of a varAss at the package level.

// 2-d arrays are supported here, and multi-dim with commas, but not bit inserts at the moment.
lValue_ass: S_IDL                      { prep_identifier $1 }
          | lValue_ass SS_DOT1 S_IDL   { KB_tag($1, $3, (glp(), fresh_textid ("." + $3))) } // why both IDL and IDC?
          | lValue_ass SS_DOT1 S_IDC   { KB_tag($1, $3, (glp(), fresh_textid ("." + $3))) }

          | lValue_ass         SS_LBRA expression_comma_or_bras_list SS_RBRA  { KB_subscripted(false, $1, $3, ref false, (glp(), fresh_textid "[.]")) }
	  | lValue_ass SS_DOT1 SS_LBRA expression_comma_or_bras_list SS_RBRA  { KB_subscripted(true, $1, $4, ref false, (glp(), fresh_textid ".[.]")) }
          | lValue_ass SS_LBRA expression SS_COLON expression SS_RBRA  { KB_bitRange2($1, $3, $5, (glp(),  fresh_textid "[:]")) }
//Was functionCall: but we can assign to a function call with a postfix tag or BRA etc.
          | lValue_ass SS_LPAR moduleActualArgs SS_RPAR        { debug "redux fcall n "; KB_functionCall(KB_ast $1, $3, (glp(), "a" + fresh_textid(mine $1))) }
          | lValue_ass SS_LPAR SS_RPAR                         { debug "redux fcal 0 "; KB_functionCall(KB_ast $1, [], (glp(), "ua" + fresh_textid(mine $1))) }

;


lValue_decl:  S_IDL                                                 { prep_identifier $1 }
           |  S_IDL SS_LBRA expression SS_COLON expression SS_RBRA  { KB_bitRange2(KB_id($1, (glp(), fresh_textid $1)), $3, $5, (glp(), fresh_textid $1)) }
           |  S_IDL SS_LBRA         expression_comma_or_bras_list SS_RBRA   { KB_subscripted(false, KB_id($1, (glp(), fresh_textid $1)), $3, ref false, (glp(), fresh_textid $1)) }
           |  S_IDL SS_DOT1 SS_LBRA expression_comma_or_bras_list SS_RBRA   { KB_subscripted(true, KB_id($1, (glp(), fresh_textid $1)), $4, ref false, (glp(), fresh_textid $1)) }	   
           |  S_IDL SS_LBRA expression_comma_or_bras_list SS_RBRA SS_LBRA expression_comma_or_bras_list SS_RBRA   { KB_subscripted(false, KB_id($1, (glp(), fresh_textid $1)), make_bison_happy_append($3, $6), ref false, (glp(), fresh_textid $1)) }
;

backdoors: /* empty */                  { []     }
         | DS_BUILTIN SP_stringLiteral  { [ $2 ] }


moduleSig: S_module TK backdoors S_IDL moduleParams  SS_LPAR type_def_option SS_RPAR provisos_list SS_SEMI { ($2, KB_moduleSig($3, $4, $5, $7, [], $9, fst $2)) }   // Single interface module syntax (old) 

         | S_module TK backdoors S_IDL moduleParams moduleArgs provisos_list SS_SEMI { ($2, KB_moduleSig($3, $4, $5, None, $6, $7, fst $2)) } // Multi-interface module syntax
;

moduleDef: moduleSig moduleStmt_list S_endmodule colon_id_option { unwind (fst $1) (KB_moduleDef(snd $1, $2, $4)) }
;

//Defining context
moduleParams: /*Empty */                                                { None }
            | SS_HASH SS_LPAR moduleParam moduleParamsList_oom SS_RPAR  { Some ($3::$4) }
;

//Defining context
moduleParamsList_oom: /* Empty */                               { [] }
                    | SS_COMMA moduleParam moduleParamsList_oom { $2 :: $3 }
;


//Defining context
moduleParam: etype_def S_IDL                  { (SO_formal,                        Some $1, $2)      }
           | S_parameter etype_def S_IDL      { (SO_parameter,                     Some $2, $3)      }
           | functionProto                     { (SO_none "$HOF",                   Some(snd $1), "$HOF")  }
;

moduleArgs: SS_LPAR moduleParam moduleParamsList_oom SS_RPAR    { $2 :: $3 }
          | SS_LPAR SS_RPAR                                     { [] }


type_option: /* Empty */ { None } 
           | type { Some $1 }
;

moduleStmt_list: /* Empty */                          { [] }
               | moduleStmt moduleStmt_list          { $1 :: $2 }
;

moduleStmt: methodDef                 { $1 }
          | scheduleDef               { $1 } // A djg extension.
          | functionDef               { $1 }
          | moduleImpStmt             { $1 }
          | varDecl                   { $1 }
          | letBinding SS_SEMI        { $1 } 
          | interfaceDef00            { $1 }
          | typeDef                   { $1 }
;

scheduleDef:
  S_schedule TK scheduleItemList SS_SEMI { KB_scheduleOrder($3, $2)    }
| S_schedule TK SP_stringLiteral SS_SEMI { KB_scheduleMatrix($3, $2)   }
;

scheduleItemList:  /* empty */                  { [] }
                | S_IDL scheduleItemList        { $1::$2 }
                | S_IDC scheduleItemList        { $1::$2 }
                | SS_LPAR scheduleItemList      { "(" :: $2 }
                | SS_RPAR scheduleItemList      { ")" :: $2 }
;



// 
interfaceDecl: S_interface TK type_def SS_SEMI interfaceDeclMember_list S_endinterface colon_Id_option  { unwind $2 (KB_interfaceDecl($3, $5, $7, fst $2)) }
;

interfaceDeclMember_list: /* empty */                         { [] } // hmmm recurses nastily and hence subifc has own production?
                |  subinterfaceDecl interfaceDeclMember_list  { $1 :: $2 }
                |  methodProto interfaceDeclMember_list       { $1 :: $2 }
		|  scheduleDef interfaceDeclMember_list       { $1 :: $2 }
;


subinterfaceDecl: attributeInstance_list S_interface type_def S_IDL SS_SEMI { KB_subinterfaceInst($1, $3, $4, glp()) }


interfaceDef_list: /* empty */                       { [] }
                |  interfaceDef00 interfaceDef_list  { $1 :: $2 }

;

// Now using subinterfaceDef00 for all defs
// this is a IDC at $3 not a bound type in Test6:27:   interface Left gauche;
// .... TODO this could be hier path, and also we may want to keep track of types declared earlier ... this could have some typformals quoted here ... why not.

interfaceDef00:
   S_interface TK S_IDC SS_COLONCOLON S_IDC S_IDL SS_SEMI interfaceDef_list S_endinterface colon_Id_option        { unwind $2 (KB_subinterfaceDef00(Some $3, $5, $6, $8, $10, fst $2)) }   

//This old form does not have the colon-colon in the concrete syntax? Was that one wrong or is now deprecated?
|   S_interface TK S_IDC S_IDL SS_SEMI interfaceDef_list S_endinterface colon_Id_option                            { unwind $2 (KB_subinterfaceDef00(None, $3, $4, $6, $8, fst $2)) }

// An interface member can also be defined using the following syntax. 
// subinterfaceDef: interface [ type ] identifier = expression ;
// The identifier is just the subinterface member name.
               | S_interface TK      S_IDL SS_EQUALS expression SS_SEMI   { unwind $2 (KB_subinterfaceDef1(None,    $3, $5, fst $2)) }
               | S_interface TK type S_IDL SS_EQUALS expression SS_SEMI   { unwind $2 (KB_subinterfaceDef1(Some $3, $4, $6, fst $2)) }
               | methodDef                 { $1 }
;
                    

moduleImpStmt:
            moduleImpIf                      { $1 }
          | moduleImpCase                    { $1 }
          | moduleImpBeginEndStmt            { $1 }
          | S_for TK SS_LPAR varODeclAssign SS_SEMI expression SS_SEMI varAssign SS_RPAR moduleImpStmt { KB_for($4, $6, $8, $10, fst $2) }
	  | varDeclAssign SS_SEMI        { $1 }
          | varDo                        { $1 }
          | varDeclDo                    { $1 }
          | varAssign SS_SEMI            { $1 }
          | systemTaskCall               { module_easc($1, glp()) }
          | expression SS_SEMI           { module_easc($1, glp()) }
          | returnStmt                   { $1 }
          | ruleDefn                     { $1 };


methodDef: S_method TK type_option S_IDL SS_LPAR methodFormals SS_RPAR implicitCond SS_SEMI
                       functionBody S_endmethod colon_id_option { unwind $2 (KB_methodDef1($3, $4, $6, $8, $10, $12, fst $2)) }
         | S_method TK type_option S_IDL implicitCond SS_SEMI
                       functionBody S_endmethod colon_id_option { unwind $2 (KB_methodDef1($3, $4, [], $5, $7, $9, fst $2)) }
// A method can also be defined using the following syntax using an equals sign.
// methodDef:  method [ type ] identifier ( methodFormals ) [ implicitCond ] = expression ;
        | S_method TK type_option S_IDL SS_LPAR methodFormals SS_RPAR implicitCond SS_EQUALS expression SS_SEMI { KB_methodDef2($3, $4, $6, $8, $10, fst $2) }
        | S_method TK S_IDL SS_EQUALS expression SS_SEMI { unwind $2 (KB_methodDef2(None, $3, [], None, $5, fst $2)) }
;




methodFormals: /* Empty */                            { [] }
             |  methodFormal                          { [$1] }
             |  methodFormal SS_COMMA methodFormals   { $1 :: $3 }
;

methodFormal: type_option S_IDL                  { ($1, $2) }
;

implicitCond: /* Empty */                                { None }
            | S_if SS_LPAR condPredicate SS_RPAR         { Some $3 }

;

typeNat: S_NAT { $1 }
;

//
// Using form: a capital identifier or a bound lower identifier.
typeIde:  S_IDC   { debug ("redux typeIde " + $1); $1 } 
       |  S_TY    { debug ("redux typeIde TY " + $1); $1 } // a lower-case identifier that is bound in the lexer as a type.
//       |  S_module { "module" } // this keyword is also a type for higher-order programs - but having it here causes reduce/reduce errors
//       | S_IDL { $1 }
// | S_IDL { mktype $1 }
;

typeIde_def: S_IDC   { debug ("redux typeIde_def " + $1); $1 } 
           | S_IDL   { debug ("redux typeIde_def lc " + $1); $1 }  // mktype called by parent
           | S_TY    { debug ("redux typeIde_def TY " + $1); $1 }  // already in scope - perhaps not freed or else masking outer binding
           | S_bit   { "bit" }                                 
           | S_int   { "int" }                                 
//         | S_bit SS_LBRA typeNat SS_COLON typeNat SS_RBRA    { KB_typeBit_lc(Some(atoi32 $3, atoi32 $5)) }
// Note S_module is also a type I think. Like bit and int it starts with a lower-case letter.
;

// Are these tags? Well used in structure expressions like the Tuple2 in: Tuple2 { tpl_1: arg1, tpl_2: arg2 };
typeIdeUC: S_IDC { $1 }
         | S_TY { $1 }
;

// Extended type (in formals for instance, but not in top-level variable declarations outside a module!
etype_def: type_def                                       { $1 }
         | S_module SS_HASH SS_LPAR typeFormal_list SS_RPAR  { KB_type_dub(true, "module", $4, glp()) }
;

// Extended type (in formals for instance, but not in top-level variable declarations outside a module!
etype: type                                              { $1 }
     | S_module SS_HASH SS_LPAR type_comma_list SS_RPAR  { KB_type_dub(false, "module", map todub $4, glp()) }
;

type:   typeIde                                           { KB_type_dub(false, $1, [], glp()) }
    |   typeIde SS_HASH SS_LPAR type_comma_list SS_RPAR   { KB_type_dub(false, $1, map todub $4, glp())  }
    |   type_function_name SS_HASH SS_LPAR e2_type_comma_list SS_RPAR   { KB_type_fun($1, map todub $4, glp())  }
    |   typeNat                                           { KB_typeNat $1      }
    |   SS_QUERY                                          { KB_dontcare (glp()) }
    |   S_bit                                             { KB_typeBit_lc(None) }
    |   SS_LPAR type SS_RPAR                              { $2 }
// The type int is just a synonym for Int#(32). Unusually, it has a lower case name.
// This is another exception that needs special parsing since, like bit, it is in lowercase.
//typedef Int#(32) int;
    |   S_int                                             { g_int_ast }
    |   S_bit SS_LBRA typeNat SS_COLON typeNat SS_RBRA    { KB_typeBit_lc(Some(atoi32 $3, atoi32 $5)) }
;

type_function_name:
	  S_TAdd { "TAdd" }
	| S_TSub { "TSub" }
	| S_TMul { "TMul" }
	| S_TDiv { "TDiv" }
	| S_TLog { "TLog" }
	| S_TExp { "TExp" }
	| S_TMax { "TMax" }
	| S_TMin { "TMin" }
	| S_TAbs { "TAbs" }
	| S_SizeOf { "SizeOf" }
;

typeclassInstanceDef: S_instance TK S_IDC SS_HASH SS_LPAR type_comma_list_def SS_RPAR SS_SEMI
                           vflist S_endinstance colon_Id_option { unwind $2 (KB_typeclassInstanceDef($3, $6, $6, $9, $11, fst $2)) }
;

vflist: /* Empty */                   { [] }
      | varAssign SS_SEMI vflist      { $1 :: $3 }
      | varDeclAssign SS_SEMI vflist  { $1 :: $3 }
      | functionDef vflist            { $1 :: $2 }
      | moduleDef vflist              { $1 :: $2 }
;



interfaceExpItem:
	       attributeInstance_list TK interfaceExpItem { KB_attribute($1, $3, fst $2) }
             | varDecl                   { $1 }
             | varDeclAssign SS_SEMI     { $1 }
             | varAssign SS_SEMI         { $1 }
             | interfaceDef00            { $1 } // sub
;

interfaceExpItem_list: /* Empty */                          { []       }
                  | interfaceExpItem interfaceExpItem_list  { $1 :: $2 }
;

// SmallExamples ex_03_h has a missing semicolon so I could have made it optional here in the grammar.
// Accept a type_def here rather than an ide so as to avoid premature commit between interfaceExpr and interfaceDecl.

// OLD:
//semi_optional:  /* Empty */ { } |  SS_SEMI  { }
//;
//interfaceExpr: S_interface TK type_def SS_SEMI /*semi_optional*/ interfaceExpItem_list
//               S_endinterface colon_Id_option { unwind $2 (KB_interfaceExpr($3, $5, $7, fst $2)) }
//;

typeName:
      type_def                { $1 }
    | typeName SS_COLONCOLON type_def       { if true then $3 else  KB_structured_id($1, $3) } // Ignored for now. TODO use KB_packageIdentifier
;

interfaceExpr: S_interface TK typeName SS_SEMI interfaceExpItem_list
                  S_endinterface colon_Id_option { unwind $2 (KB_interfaceExpr($3, $5, $7, fst $2)) }


exprPrimary:  
              S_IDC                         { KB_constantAlias($1, (glp(), fresh_textid $1)) }
//          | S_IDC SS_COLONCOLON ident     { KB_packageIdentifier ... }
            | SP_intLiteral                 { $1 }
            | SP_stringLiteral              { KB_stringLiteral $1 }
            | lValue_ass                    { $1 }
            | SS_LPAR expression SS_RPAR    { $2 }
            | SS_LPAR interfaceExpr SS_RPAR { $2 }
            | SS_LPAR beginEndExpr SS_RPAR  { $2 }
//          | SS_LPAR caseExpr SS_RPAR      { $1 }
            | bitConcat                     { $1 }
            | exprPrimary         SS_LBRA expression_comma_list SS_RBRA     { KB_subscripted(false, $1, $3, ref false, (glp(), fresh_textid "[.]")) }
            | exprPrimary SS_DOT1 SS_LBRA expression_comma_list SS_RBRA     { KB_subscripted(true, $1, $4, ref false, (glp(), fresh_textid ".[.]")) }
            | exprPrimary SS_LBRA expression SS_COLON expression SS_RBRA    { KB_bitRange2($1, $3, $5, (glp(), fresh_textid "[:]")) }


// SizeOf, which converts a type to a (numeric) type, should not be confused with the pseudo-function valueof, described in Section 4.2.1, which converts a numeric type to a numeric value.
	    | S_valueOf SS_LPAR type SS_RPAR { KB_valueOf($3, (glp(), fresh_textid "valueOf")); }
	    | S_valueof SS_LPAR type SS_RPAR { KB_valueOf($3, (glp(), fresh_textid "valueOf")) ; }
            | actionBlock                { $1 }
            | actionValueBlock           { $1 }
            | systemFunctionCall         { $1 } 
            | typeAssertion              { $1 } // restored. Some are handled in lex for sized numeric literals.
            | taggedUnionExpr            { $1 }
            | structExpr                 { $1 }
            | rulesExpr                  { $1 }
            | sublangExpr                { $1 } // djg added

//          | S_SizeOf SS_HASH SS_LPAR expression SS_RPAR       { debug "redux fcal S_SizeOf"; KB_functionCall(KB_ast(KB_id("SizeOf#", (glp(), fresh_textid "SizeOf"))), [$4], (glp(), ...) }
;


unop:   SS_PLUS      { "+" }
|       SS_MINUS     { "-" }
|       SS_PLING     { "!" }

;

// FSYacc does not support operator precedence? So we have to have all these detailed rules!
// Should ** be on a higher binding power?
binop5: SS_STAR      { KB_op V_times  }
|       SS_STARSTAR  { KB_op V_exp    }
|       SS_SLASH     { KB_op V_divide }
|       SS_PERCENT   { KB_op V_mod    }
;

binop6: SS_PLUS      { KB_op V_plus  }
|       SS_MINUS     { KB_op V_minus }
;

binop7: SS_LSHIFT    { KB_op V_lshift }
|       SS_RSHIFT    { KB_op (V_rshift Signed)}
;

binop8: SS_DLTD      { KB_bop (V_dltd Signed) } // These are set as signed in the parser but will typically need to be changed to unsigned
|       SS_DGTD      { KB_bop (V_dgtd Signed) }
|       SS_DLED      { KB_bop (V_dled Signed) }
|       SS_DGED      { KB_bop (V_dged Signed) }
;

binop9: SS_DEQD      { KB_bop V_deqd }
|       SS_DNED      { KB_bop V_dned }

binop10:        SS_AMP       { KB_op V_bitand }
;

binop11:        SS_CARET     { KB_op V_xor }
;
binop12_nonstrict:        SS_STILE     { KB_op V_bitor }
;

binop13_nonstrict:        SS_AMPAMP    { KB_ab V_band }
;

binop14:        SS_STILE2    { KB_ab V_bor }
;



operatorExpr5: unop exprPrimary                  { KB_functionCall(KB_unop $1, [$2], (glp(), fresh_textid $1))    }
             | exprPrimary                       { $1 }

operatorExpr6: operatorExpr5                            { $1                   }
             | operatorExpr6 binop5 operatorExpr5       { gen_diadic($2, $1, $3, (glp(), op_textid($2))) }
;
operatorExpr7: operatorExpr6                            { $1                   }
             | operatorExpr7 binop6 operatorExpr6       { gen_diadic($2, $1, $3, (glp(), op_textid($2))) }
;
operatorExpr8: operatorExpr7                            { $1                   }
             | operatorExpr7 binop7 operatorExpr7       { gen_diadic($2, $1, $3, (glp(), op_textid($2))) } // << >> nonassoc.
;
operatorExpr9: operatorExpr8                            { $1                   }
             | operatorExpr9 binop8 operatorExpr8       { gen_diadic($2, $1, $3, (glp(), op_textid($2))) }
;
operatorExpr10: operatorExpr9                           { $1                   }
              | operatorExpr10 binop9 operatorExpr9      { gen_diadic($2, $1, $3, (glp(), op_textid($2))) }
;  
operatorExpr11: operatorExpr10                          { $1                   }
              | operatorExpr11 binop10 operatorExpr10   { gen_diadic($2, $1, $3, (glp(), op_textid($2))) }
;
operatorExpr12: operatorExpr11                          { $1                   }
              | operatorExpr12 binop11 operatorExpr11   { gen_diadic($2, $1, $3, (glp(), op_textid($2))) }
;
operatorExpr13: operatorExpr12                          { $1                   }
              | operatorExpr13 binop12_nonstrict operatorExpr12   { gen_diadic($2, $1, $3, (glp(), op_textid($2))) }
;
operatorExpr14: operatorExpr13                          { $1                   }
              | operatorExpr14 binop13_nonstrict operatorExpr13   { gen_diadic($2, $1, $3, (glp(), op_textid($2))) }
;
operatorExpr15: operatorExpr14                          { $1                   }
              | operatorExpr15 binop14 operatorExpr14   { gen_diadic($2, $1, $3, (glp(), op_textid($2))) }
;


// It is critical to force right associativity such that a?b:c?d:e does not parse as (a?b:c)?d:e.


// condExpr: condPredicate SS_QUERY expression SS_COLON expression { KB_query($1, $3, $5, (glp(), fresh_textid "COND")) } 

expression: 
            noncondExpr        { $1 }
	  | noncondExpr SS_QUERY expression SS_COLON expression { KB_query($1, $3, $5, (glp(), fresh_textid "COND")) } 
          | SS_QUERY        { KB_dontcare (glp()) }
;

noncondExpr:
            condPredicate   { $1 }
          | operatorExpr15  { $1 }
;


condPredicate: exprOrCondPattern                                  { $1 }
	     | exprOrCondPattern SS_AMPAMPAMP exprOrCondPattern   {  KB_functionCall(KB_unop "MATCHES", [$1; $3], (glp(), fresh_textid "MATCHES")) }

;

exprOrCondPattern: noncondExpr { $1 }
	         | caseExpr    { $1 }
;

caseExpr: noncondExpr S_matches pattern       { KB_caseExpr($1, $3, glp()) }
;

                
// Parser says the empty clause will never be used, so comment out.
type_def_option:  // /* Empty */    { None }
                   type_def     { Some $1 }
;


// Defining context: lower cass identifiers become bound as types
type_def: typeIde_def                                { KB_type_dub(true, mktype $1, [], glp())           }
        | typeIde_def typeFormals                    { KB_type_dub(true, mktype $1, $2, glp())           }
;

// Defining context
typeFormal_list: S_numeric S_type typeIde_def typeFormal_list1      { (SO_numeric, None, mktype $3) :: $4  }
               | S_type S_numeric typeIde_def typeFormal_list1      { (SO_numeric, None, mktype $3) :: $4  }
               |           S_type typeIde_def typeFormal_list1      { (SO_type,    None, mktype $2) :: $3  }
               |                  typeIde_def typeFormal_list1      { (SO_none "$unq_lc", None, mktype $1) :: $2  }


// This third case appears in   "interface Get#(req_type) request;" 
//               |                  typeIde_def typeFormal_list1      { KB_typeFormal("$bound",   $1) :: $2  }
// but is ambiguous w.r.t the fourth more general case where a lower-case IDL can now be a type.

// This fourth case appears in   interface Put#(WordT) in;
               |                  S_IDC typeFormals typeFormal_list1  { (SO_none "$tf2", Some(KB_type_dub(true, $1, $2, glp())), "$tyvar2") :: $3  }
               |                  S_IDC typeFormal_list1              { (SO_none "$tf1", Some(KB_id($1, (glp(), fresh_textid $1))), "$tyvar1") :: $2  }
               |                  SP_intLiteral typeFormal_list1              { (SO_none "$tf", Some($1), "$tyvar") :: $2  }
;

//Defining context
typeFormal_list1: /* empty */               { [] }
                | SS_COMMA typeFormal_list  { $2 }
;
//Defining context
typeFormals: /* empty */                             { [] }
           | SS_HASH SS_LPAR typeFormal_list SS_RPAR { $3 }
;


actionValueBlock: S_actionvalue colon_id_option actionValueStmt_list S_endactionvalue colon_id_option { KB_actionValueBlock("actionvalue", $2, $3, $5, (glp(), fresh_textid "AVALB")) }
;


actionValueStmt: actionValueIf                   { $1 }
               | actionValueCase                 { $1 }
               | actionValueBeginEndStmt         { $1 }
               | regWrite                  { $1 }
               | varDecl                   { $1 }
               | varAssign SS_SEMI         { $1 }
               | varDeclAssign SS_SEMI     { $1 }
               | varDo                     { $1 }
               | varDeclDo                 { $1 }
               | systemTaskCall            { KB_easc($1, glp()) }
               | expression SS_SEMI        { KB_easc($1, glp()) }
               | returnStmt                { $1 }
;




typeAssertion: S_TYPEASSERT_WARNING type SS_FWDQUOTE bitConcat                  { KB_typeAssertion($2, $4, (glp(), fresh_textid "TA1")) }
             | S_TYPEASSERT_WARNING type SS_FWDQUOTE SS_LPAR expression SS_RPAR { KB_typeAssertion($2, $5, (glp(), fresh_textid "TA2")) }
;

// This forces at least one member. Is this always correct?  See also KB_constantAlias which is essentially the enum form (hence without members).
structExpr: typeIdeUC SS_LBRACE memberBind memberBind_comma_list SS_RBRACE { KB_structExprAssociative($1, $3::$4, (glp(), fresh_textid $1)) }
          | typeIdeUC SS_LPAR expression_comma_list SS_RPAR                { KB_structExprPositional($1, $3, (glp(), fresh_textid $1))  }
;


const_pattern: S_IDC                      { KB_constantAlias($1, (glp(), fresh_textid $1)) } // Enum label, such as True or False.
             | SP_intLiteral              { $1 }
;

pattern: 
//identifier                          { KB_patternVar $1 }             //  Pattern variable (reduce/reduce error)
         const_pattern                       { $1 }                           //  Constant
       | SS_DOT1 S_IDL                       { KB_dot $2 }                    //  Pattern var
       | SS_DOTSTAR                          { KB_wildcard }                  //  Wildcard
       | S_tagged S_IDC SS_DOT1 S_IDL        { KB_taggedUnion($2, [], Some(KB_patternVar $4),  (glp(), fresh_textid $2)) }  // Tagged union, no braces.
       | S_tagged S_IDC structPattern        { KB_taggedUnion($2, [], Some $3,  (glp(), fresh_textid $2)) }  // Tagged union
       | S_tagged S_IDC                      { KB_taggedUnion($2, [], None,  (glp(), fresh_textid $2)) }  // Tagged union
       | tuplePattern                        { $1 }                           // Structure
;

tuplePattern: SS_LBRACE pattern pattern_comma_list SS_RBRACE { KB_tuplePattern($2 :: $3, (glp(), fresh_textid "PAT")) }
;

pattern_comma_list: /* Empty */                         { [] }
                  | SS_COMMA pattern pattern_comma_list { $2 :: $3 }
;

// Guarded patterns should have the SS_AMPAMPAMP &&& filters implemented.
gpattern: pattern { $1 } // For now
;

gpattern_comma_list: /* Empty */                         { [] }
                   | SS_COMMA gpattern gpattern_comma_list { $2 :: $3 }
;

// Reference manual says:
// Note that the first and second grammar productions overlap, since an expression can be an identifier.
// The first production has priority, i.e., if we encounter a naked identifier, it is interpreted as a pattern variable. 

structPattern: tuplePattern                                           { $1 }
             |  SS_LBRACE structPattern_item structPattern_item_comma_list SS_RBRACE     { KB_structPattern($2::$3, (glp(), fresh_textid "SPAT")) }
;


structPattern_item_comma_list: /* Empty */                                               {  [] }
                             | SS_COMMA structPattern_item structPattern_item_comma_list { $2 :: $3 }
;
           
structPattern_item: S_IDL SS_COLON pattern { ($1, $3) }
;


           
ruleDefn:  ruleAssertion_list S_rule TK S_IDL ruleCond_option SS_SEMI
                    ruleBody S_endrule colon_id_option  { unwind $3 (KB_rule($1, $4, $5, $7, $9, fst $3)) }
;

// Keyword 'if' is optional here now.
ruleCond_option: /* Empty */                       { None } 
               |      SS_LPAR expression SS_RPAR   { Some $2 }
               | S_if SS_LPAR expression SS_RPAR   { Some $3 }
;

ruleBody: actionStmt_list { $1 }
;

rulesExpr: S_rules colon_id_option rulesStmt S_endrules colon_id_option  { KB_rulesExpr($2, $3, $5) }
;

rulesStmt: varDecl                  { $1 }
         | varAssign SS_SEMI        { $1 }
         | varDeclAssign SS_SEMI    { $1 }
         | ruleDefn                 { $1 }
;

                  
ruleAssertion_list: attributeInstance_list { $1 }
;


SP_intLiteral: SX_INTLITERAL             { KB_intLiteral(Some "", $1, glp()) }
             | S_NAT SX_INTLITERAL       { KB_intLiteral(Some $1, $2, glp()) }
             | S_NAT                     { KB_intLiteral(None, (false, 10, $1), glp()) }

%%
// eof


