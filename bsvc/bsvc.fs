// CBG SQUANDERER : Toy Bluespec Verilog Compiler: Coded in F#.
// (C) 2012-15 DJ Greaves, University of Cambridge.

(* All rights reserved, except as licensed by LGPL below.
 * DJ Greaves, University of Cambridge, Computer Laboratory.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *)

module bsvc

open System.Collections.Generic
open System.Numerics

open moscow
open meox
open yout
open opath_hdr
open hprls_hdr
open linepoint_hdr
open abstract_hdr
open abstracte
open parser_types
open tydefs_bsvc
open dotreport
open tableprinter

open tydefs_bsvc
open lex_bsvc

let g_swrite = "_write"
let g_sread = "_read"

let g_enable_drop_debug = false

let g_smell = false // TODO rename and explain or else delete me.

let g_selectname = "select" // check a plain select does not commonly exist of this name: should be in a typeclass such as List.select

let s_native_write = "native_write"

// A SPOG is a note of a function that is passed to another. We record extra annotations for such items, in case these later might be needed.
// A mutable set or map might be more efficient, but there will not be a lot of these generally used.
let g_spogs = ref [] // TODO rename. And not global, but we can assume a flat namespace or globally unique dropping names for now.


let hof_deco_close uid_us fid idl ee_2 aty =
    let cp = CPS(PS_suffix, [(uid_us, 0, None)])
    let cp' =
        match cp with // longwinded way to make a hof-pair  formal and uid.
            | CPS(PS_suffix, lstl) ->
                let ans = CPS(PS_suffix, lstl @ [ (fid, 0, Some ee_2) ])
                vprintln 3 (sprintf "hof decorated to get %s and %s as " fid uid_us + cpToStr ans)
                ans
    let token = funique "GammaHOF"
    (token, cp', Tannot [(fid, aty)]) // Tannot is never matched so just a place holder ...


let log_conflict_record emits_o mf record =
    match emits_o with
        | None -> ()
        | Some emits ->
            //dev_println (mf() + sprintf ": Conflict log entry %A" record)
            mutadd emits.conflict_log record

// Return whether an auto_apply to a register-like interface is acceptable given the applied provisos:
// Current technique: acceptable unless the asReg modifier is present.
// The 'asReg' and 'asIfc' pseuod functions/modifiers apply to any interface that has a unit-arg method (called _read?), not just to the Prelude.Reg interface.
let auto_apply_allowedp vd arg prov =
    let m2 = "asReg"
    let rec mine = function
        | Prov_asReg_asIfc _ -> false
        | Prov_s lst -> conjunctionate mine lst
        | other ->
            let allowed = true
            if vd>=5 then vprintln 5 (sprintf "%s GroomProvisos %s for ty %s? allowed=%A" m2 (provToStr other) (bsv_tnToStr arg) allowed)
            allowed
    let allowed = mine prov
    if vd>=5 then vprintln 5 (sprintf "%s GroomProvisos %s top for %s allowed=%A" m2 (provToStr prov) (bsv_tnToStr arg) allowed)
    allowed


// OR-in a new disjunct for hard currency invocation.
let alpha_fire monica new_alpha = function
    | BsvValue ro -> (BsvValue ro)
    | BsvAction(rdy, enol, na) ->
        //vprintln 0 (mm + ": New disjunct for Action method enable: " + xbToStr new_alpha)
        BsvAction(rdy, (monica, new_alpha)::enol, na)

    
// Say whether an interface is acceptable given the applied provisos:
// current Kludge - if Ord or Arith is requested cannot have an interface
let ifc_enabled arg prov =
    let m2 = "asIfc"
    let rec mine = function
        | Prov_si "Ord"
        | Prov_si "Arith" -> false
        | Prov_s lst -> conjunctionate mine lst
        | other ->
            let ans = true
            vprintln 3 (sprintf "%s GroomProvisos %s for ty %s? ans=%A" m2 (provToStr other) (bsv_tnToStr arg) ans)
            ans
    let ans = mine prov
    vprintln 3 (sprintf "%s GroomProvisos %s top for %s ans=%A" m2 (provToStr prov) (bsv_tnToStr arg) ans)
    ans


// An empty type checking environment.
let g_null_tb = { rv=None; tve=[]; recursion_stack=[]; tb_static_path=[] }

let g_maker_sty_ty = // The type of an unapplied maker
    let fid = "needs-freshen-ifc-module-arg"
    Bsv_ty_nom({g_blank_nom_typ_ats with nomid=["$abstractmodule"] }, F_fun(g_ty_sum_monad, { g_blank_fid with fids=[fid]; methodname="$abstractmodule"}, [Bsv_ty_id(VP_value(false, [fid]), Some P_decl0, (fid, (*fresh_bindindex*) "-1"))]), [])

let g_empty_resource_record =
    { igs=         Map.empty
      fpaths=      Map.empty
    }


// Active pattern that can be used to assign values to symbols in a pattern
let (|Let|) value input = (value, input)


let g_ty_monad = g_unit_ty_raw
let g_reg_backdoor = ref None


type predef_map_t = NoEqMap<string list, bsv_type_t>             
//let g_predef_types = ref (new predef_map_t(Map.empty))

let g_kernels_builtin = new OptionStore<string list, kernel_mk_t * (bsv_type_t * int)>("kernels_builtin")


// Global database of tpe annotations.    
let g_type_annotates = Dictionary<string, (string * dropping_t)>()



let local_xi_bnum arg =
    //dev_println(sprintf "local_xi_bnum %A" arg)
    xi_bnum arg

// Hide the prefix part of a hierarchic name.
// Owing to hptos working on reversed lists, the prefix is at the end of the list.
let tacitate prefix_to_hide full_path =
    let rec scap = function
        | ([], zl) -> rev zl
        | (ax::axs, bx::bxs) when ax=bx -> scap (axs, bxs)
        | _ -> full_path
    if g_add_kind_to_formals then full_path else scap (rev prefix_to_hide, rev full_path)


//
// Find hidden free variables in type expressions.
//
let rec mine_latent_generics ww (mf:unit->string) ast_ty cc =
    match ast_ty with
        | KB_type_dub(_, tname, generics, lp) ->
            List.foldBack (mine_latent_generics2 ww mf) generics cc

        | KB_intLiteral(fwo, (dontcares, b, s), lp) ->
            cc

        | KB_id(id, (lp, uid)) -> 
            vprintln 3 (mf() + sprintf ": +++: mine_latent_generics: id form ignored %s" id)
            cc

        | other ->
            vprintln 3 (mf() + sprintf ": +++: mine_latent_generics: Other form ignored %A" other)
            cc

and mine_latent_generics2 ww mf arg cc =            
    let tyvar_keep so id = if strlen id >= 1 && System.Char.IsLower id.[0] then [(so, id)] else [] // Retain only type variables (lower case first letter).  Parser dummy formals, such as $tf2, all start with a dollars sign.
    match arg with
        | (so, None, id)-> tyvar_keep so id
        | (so, Some ast, id)->
             tyvar_keep so id @ mine_latent_generics ww mf ast cc


// Higher-order function predicate.
let get_hof site fid ft = // TODO there is a better copy within deepbind - use it.
    if fid = "$HOF" then sf (sprintf "$HOF should already have been recoded (L6304a) from %A" ft)
    match ft with
        | ft when leaf_grounded_type_pred ft -> None
        | Bsv_ty_nom(nst, F_fun _, _) -> Some("true")  // Really this returned value is just a bool now. Perhaps re-code.
        | Bsv_ty_nom _                -> None
        | other ->
            vprintln 3 (sprintf "(%s) Ignored get_hof for %s  %s" site fid (bsv_tnToStr other))
            None

let name_alias_check mf dkey = function
    | (None, None)       -> ()
    | (Some e1, Some e2) ->
        if x2nn e1 = x2nn e2 // use memoising heap equivalence for name alias determination
        then ()
        else // Need a linepoint ideally in all cleanexits.
            cleanexit(mf() + sprintf ": name alias conflict on subscript expressions to %s  s1=%s  s2=%s" (hptos dkey) (xToStr e1) (xToStr e2))
    | _  -> sf (mf() + sprintf ": this item is used both with and without subscript %s" (hptos dkey))


//
// grave_deepbind_arg_ty_list: create an env where formals are bound to actuals.  Note that the formals and actuals are not just scalars, they also include structures of various forms, such as templated types. Hence 'deep' denotes recursively binding over such structures.
// Both values and types need binding. 
// deepbind accepts three forms: (fid/ty) pairs (as used for args), singles (as used for return types), and 'trips' of form (fid, (ty, vale)). 
//
//   
// One theory is we never need to deepbind for typevars because all free typevars are listed in the top-level tyvar and they
// have they have been suitably alpha-renamed structurally below to share common names for identical parameters.  But functions are not like that at the moment always?
//
//            
let grave_deepbind_arg_ty_list ww vd mf uido aliases (em:genenv_map_t) pairs trips = 
    let ww = WF 3 "grave_deepbind_arg_ty_list" ww "start"
    let mm = ""
    let singles = [] // could be used more cleanly for rt but that is just using fid="" for now.

    let rec deepbind_arg_ty_core hof stars fid (em:genenv_map_t) sty = // All deepbind items finally come to this core function for the deeper parts.
        if vd>=4 then vprintln 4 (mf() + sprintf ": deepbind_arg_ty_core: binding fid '%s' stars=%i  with " fid stars + bsv_tnToStr sty)
        let em = if fid="" then em else (em.Add([fid], GE_binding([fid], {g_blank_bindinfo with whom="deepbind_grave"; hof_route=hof; }, EE_typef(G_none, sty))))
        let em =
            // Formal names that are mapped to a username via the alias list also have this username bound against the actual.
            match op_assoc fid aliases with
                | None -> em
                | Some(Bsv_ty_id(_, Some P_free, (username, _))) ->
                    if vd>=4 then vprintln 4 (sprintf ": deepbind: binding fid '%s' stars=%i as alias also of %A sty=%s" fid stars username (bsv_tnToStr sty))
                    em.Add([username], GE_binding([username], {g_blank_bindinfo with whom="genty1u"; hof_route=hof; }, EE_typef(G_none, sty)))
                | Some x ->
                    if vd>=4 then vprintln 4 (sprintf ": deepbind: ignoring aliases for fid='%s' now %A" fid x)
                    em
        match sty with
            | Bsv_ty_stars(n_stars, ct) -> deepbind_arg_ty_core hof (stars+n_stars) fid em ct // stars is ignored at the moment but need to check polarity of this op if used in future TODO
            | Bsv_ty_knot(idl, k2) when not(nonep !k2) -> deepbind_arg_ty_core hof stars fid em (valOf !k2)

            | Bsv_ty_id (f, prc, (fid1, _)) -> // May have been bound in immediate parent if came via pairs list.
                if vd>=4 then vprintln 4 (mm + sprintf "deepbind: ++++ For temp: binding missed out for now: ignoring LINK of fid '%s' to %s" fid fid1)
                em

            // Functions require deep binding because we have not defined a standard way of dentoing their free typevars in ... ... TODO explain
                
            | Bsv_ty_nom(_, F_fun(rt, matts, args), _) ->
                if fid = "" then vprintln 3 (sprintf "Ignore deepbind fun of fid='%s' (owing t null formal name) with %A" fid (bsv_tnToStr sty))
                else vprintln 0 (sprintf "+++ TODO wrongly ignore deepbind fun of fid='%s' with %A" fid (bsv_tnToStr sty))                
                em // for now - TODO please justify - why not same as vector etc ..

            | Bsv_ty_nom(_, _, binds) -> // What about inner frees on the poly funcs etc?  TODO see note... which note?
                List.fold deepbind_arg_typram em binds 

            | Bsv_ty_int_vp _ when fid = ""   -> em
            | Bsv_ty_int_vp _ (* otherwise *) ->
                vprintln 0 (sprintf "+++ wrongly (?) ignore deepbind int vp %A with %A" fid sty) 
                em            

            | other when leaf_grounded_type_pred other -> em

            | other ->  sf (sprintf "deepbind_arg_type fid='%s' other ty/vale=%A" fid other)

    and deepbind_arg_typram (em:genenv_map_t) = function
        | Tty(sty, fid) ->
            if vd>=4 then vprintln 4 (mm + sprintf ": deepbind: binding fid '%s' arg_typram ty/vale=%A" fid (bsv_tnToStr sty))
            let hof = get_hof "deepbind_arg_typram" fid sty // not needed?
            deepbind_arg_ty_core hof 0 fid em sty

        | Tid _ -> em


    let rec deepbind_arg_for_trip (em:genenv_map_t) ((_, ft, fid), (sty, vale)) = // These 'trips' have a vale and a sty, whereas pairs have only a sty.
        let hof = get_hof "deepbind_arg_for_trip" fid ft // TODO this hof get does not create a SPOG - please do so as per the for_pair branch.
        if vd>=4 then vprintln 4 (mm + sprintf ": deepbind: binding arg_for_trip: '%s' hof=%A sty=%s" fid hof (bsv_tnToStr sty))
        let em = em.Add([fid], GE_binding([fid], {g_blank_bindinfo with whom="deepbind_arg_trip"; hof_route=hof }, EE_e(sty, vale))) // Bind a value
        deepbind_arg_ty_core hof 0 "" em sty

    let deepbind_arg_for_pair (em:genenv_map_t) = function
        | ((_, ft, fid), actual_sty) ->
            if vd>=4 then vprintln vd (mm + ": deepbind ft bind " + bsv_tnToStr ft + " against " + bsv_tnToStr actual_sty)
            let em =
                match ft with 
                | Bsv_ty_id(_, _, (tyfid, _)) -> // If the formal is a type id then bind it to the actual.
                    // This is NOT DEEP - NYD TODO: we want deep behaviour for pairs as well as trips?
                    // TODO: if tyfid is already bound, we should check/resolve the old and new bindings and delete the associated value if they disagree, just keeping the resolved type.
                    let em = em.Add([tyfid], GE_binding([fid], {g_blank_bindinfo with whom="NYD formal pair";  }, EE_typef(G_none, actual_sty))) 
                    if vd>=4 then vprintln 4 (mm + sprintf "deepbind: binding parent=%s tyfid=%s actual_sty=%s" fid tyfid (bsv_tnToStr actual_sty))
                    em
                    
                | _ -> em
            let hof = // This is the supposedly better variant of get_hof - but its not better - its just the same and trivially simple - we all need to guard the SPOG mutadd so leave alone.
                match ft with
                    | Bsv_ty_nom(nst, F_fun(_, mats, _), _) ->
                        if vd>=4 then vprintln 4 (sprintf "check hof decorate uid=%A of %s %A" uido fid (bsv_tnToStr actual_sty))
#if OLD
                        //let x3 = hof_deco_close "uid_us-TODO" fid nst.id evty_m sty
                        //let cc = g_null_tb // This will be a closed item for mutable anotation.
                        // Old PS_closed approach ...
                        // This seems strange - TODO why done here (if not needed dont need mut rec with store_type_annotation (done)).  These are the ones containing just a ^^^ with no uid in their path.
                        //let tbcc_ = List.fold (fun cc (id, cp, drop) -> store_type_annotation ww em "L6397" "" (mm + " grave body drop") tbcc cp drop) tbcc [x3]
#endif
                        // TODO: unify the two copies of this spog storing code.
                        // A SPOG is a note of a function being passed to another.
                        // A further, similar mechanisms would be needed if returning of functions were supported...
                        // The spog collation is unrelated to the hof_route tagging which denotes whether this current function is a hof apply.
                        let _ =
                            if not_nonep uido then
                                let spog = SPOG(("-10", [mf()]), nst.nomid, (em, g_null_tb, []), valOf uido, fid)
                                dev_println ("Made spog (L320) " + spogToStr spog)
                                if vd>=4 then vprintln 4 ("deepbind: Made spog (L320) " + spogToStr spog)
                                mutadd g_spogs spog // addonce might be nicer?
                        Some fid
                    | _ -> None
            deepbind_arg_ty_core hof 0 fid em actual_sty
        

    let em = List.fold deepbind_arg_for_pair em pairs
    let em = List.fold deepbind_arg_for_trip em trips    
    List.fold (deepbind_arg_ty_core None 0 "") em singles


// This is placeholder code for a scheme where a currency entry has a primary inode that is accessed by various interface-returning expressions.
let primary_iss sigma hintname = 
    let ans = IIS (hintname + "_iis")
//    match sigma.ifcmap.TryFind ans with
//        | None ->
    ans

let make_dropping_key = function
    | CPS(gf, path) -> htosp( map f1o3 path)



let retrieve_type_annotation bobj mf site arg =
    match arg with
    | CPS(gf, path) ->
        if gf=PS_suffix then sf (mf() + ": grounded type annotation wanted ")
        else
        let kpath = make_dropping_key arg
        let (found, ov) = g_type_annotates.TryGetValue kpath
        if not found then
            let r = ref [] 
            let pis k = mutadd r k
            for k in g_type_annotates do pis k.Key done
            sf(mf() + sprintf ": site=%s +++ No type annotation found for " site + cpToStr arg)
            //sf(msg + ": +++ No type annotation found for " + htosp path + "\nConcourse=" + sfold cpToStr !r)            
        else
            if bobj.misc_vd >= 4 then vprintln 4 (mf() + sprintf ":retrieved type annotation %s for " (fst ov) + cpToStr arg)
            snd ov
    
    | CP_none x -> sf ("retrieve without callstring path: " + mf())


let simple_retrieve ww bobj mf site ee uid =
    let struct_ncp = extend_callpath bobj site uid ee.callpath
    let sty = 
        match retrieve_type_annotation bobj mf site struct_ncp with
            | Tanno_function(sty, _, _, _) -> sty
            | other -> sf (sprintf "%s: %s:  wrong form dropping retrieved for %s  %A" (mf()) site (cpToStr struct_ncp) other)
    if bobj.normalise_loglevel>=4 then vprintln 4 (sprintf "Retrieved site=%s on ncp %s type %s" site (cpToStr struct_ncp) (bsv_tnToStr sty))
    sty

let kludge_formal_name_to_type_o m = function
    | (so, Some t, _) -> Some t
    | (so, None, fid) when fid <> "" ->
        //vprintln 0 (mm + sprintf ": kludge name to type_o so=%s  for '%s'" (soToStr true so) fid)
        Some(KB_type_dub(false, fid, [], g_builtin_lp))

let kludge_formal_name_to_type2 m = function
    | (so, Some t, a) -> (so, Some t, a)
    | (so, None, fid) when fid <> "" ->
        //vprintln 0 (mm + sprintf ": kludge name to type2 so=%s  for '%s'" (soToStr true so) fid)
        (so, Some(KB_type_dub(false, fid, [], g_builtin_lp)), "")

//
// Do a deep dive in a type to find its free tyvars.
// We need to define the convention as to when this is done and noted.  TODO. have a wrapper form to denote an abstract type - or better a  grounded one?  Bsv_ty_list is the only one we really need to handle?
//
// In a perfect implementation this is the only place where a deep mine is needed - all other bindings can then be done from the top report returned by this function.
//
let get_freshlist ww sty cc =
    let mm = "get_freshlist"
    let ww = WN mm ww
    let rec sum_ty_support_xo stack arg cc =
        match arg with
            | (_, None) -> cc
            | (_, Some sty) -> sum_ty_support stack sty cc
    and sum_ty_support_x stack arg cc =
        match arg with
            | (_, sty) -> sum_ty_support stack sty cc
    and sum_ty_support stack arg cc =
        match arg with

        | Bsv_ty_sum_knott(idl, x) when memberp idl stack -> cc
// TODO this case missing?
//        | Bsv_ty_sum_knott(idl, x) when not(nonep !x) -> sum_ty_support (idl :: stack) (valOf !x) cc
        | Bsv_ty_sum_knott(idl, x) (*otherwise*) ->
            vprintln 0 (mm + ": untied (d)" + htosp idl)
            cc
            
        | Bsv_ty_knot(idl, x) when memberp idl stack -> cc
        | Bsv_ty_knot(idl, x) when not(nonep !x) -> sum_ty_support (idl :: stack) (valOf !x) cc
        | Bsv_ty_knot(idl, x) (*otherwise*) ->
            vprintln 0 (mm + ": untied " + htosp idl)
            cc

        | Bsv_ty_list(_, lst) -> List.foldBack (sum_ty_support stack) lst cc

        | Bsv_ty_int_vp _ -> cc 

        | ft when leaf_grounded_type_pred ft -> cc

        | Bsv_ty_stars(m, a)  -> sum_ty_support stack a cc
        | Bsv_ty_id(nf, _, (s, _)) ->
            let rec lsingly_add sofar = function
                | [] -> (nf, s):: cc (* TODO: need to rev sofar here if really want to maintain order! *)
                | (nf', s') :: tt when s = s' -> (rev sofar) @ (vp_resolve(nf, nf'), s) ::  tt (* Maintain order, adding on end *)
                | x :: tt -> lsingly_add (x::sofar) tt
            lsingly_add [] cc


        | Bsv_ty_nom(idl, cat, args) ->  // This is the one item of code that does need to dive in if we are at the top level, but that's supposed to be the difference between freshlister and get_freshlist. The args_ are ignored and  does not need to dive in here ever (and also for typep) ... xref note ??
            let cc = List.foldBack (sum_ty_support stack) (map rose args) cc

            // We definitely cannot ignore the pramargs because, e.g. for Vector they are the only manifestation of the content.
            // Also for functions with or without higher-order args we may currently need to do it.
//Mine site
            let cc2 = 
              match cat with
                | F_ifc_temp // ifc_temp is a placeholder untill a partially built-in interface is fully defined by prelude.
                | F_action
                | F_enum _
                | F_monad
                | F_tyfun _
                | F_module -> cc 

                | F_vector -> cc
                | F_ntype -> cc

                
                | F_tunion(br, sto_list, _) -> List.foldBack (sum_ty_support_xo stack) sto_list cc // TODO it is still not clear whether this deep mining should ever find something fresh not mentioned in the nom list.
                | F_struct(st_list, _) -> List.foldBack (sum_ty_support_x stack) st_list cc
                | F_actionValue sty
                | F_subif(_, sty) -> sum_ty_support stack sty cc
                | F_ifc_1 ii -> List.foldBack (sum_ty_support stack) ii.iparts cc
                | F_fun(rt, mats, args) ->
                    //Here we mine the body_gamma too even. TODO explain why this might be needed. We are here not careful with ordering since a positional binding is not relevant.
                    let mans = ref []
                    let fl_bg(id, cp, drop) =
                        let mans_add ty = (mutadd mans ty; ty)
                        //let _ = lprintln g_avd (fun()-> " nf_sum  F_fun bg  " + id + " " + cpToStr cp + " giving " + bsv_tnToStr drop)
                        let _ = dropapp mans_add drop
                        ()
                    let cc = sum_ty_support stack rt cc // Return type first.
                    let cc = List.foldBack (sum_ty_support stack) args cc // Then args in order.
                    app fl_bg mats.body_gamma 
                    let cc = List.foldBack (sum_ty_support stack) !mans cc
                    cc
            let l1 = length cc
            let l2 = length cc2 
            let _ =
                if false then
                    if l2 > l1 then vprintln 0 ("GREATER INS")
                    if l2 < l1 then vprintln 0 ("LESSER INS")
                    if l1 <> l2 then vprintln 0 (sprintf "instrumented difference %i %i on nom %s" l1 l2 (bsv_tnToStr (Bsv_ty_nom(idl, cat, args))))
                    ()
            cc2
                
        | Bsv_ty_methodProto(methodname, _,  mth_provisos, proto, _) -> List.foldBack (sum_ty_support stack) (un_protoprotocol [] proto) cc

        //| ty    -> cc
        | other -> sf(sprintf "sum_ty_support other: %A" other)

    sum_ty_support [] sty cc


let freshlister ww rt args =
    let cc = get_freshlist ww rt []
    let cc = List.foldBack (fun ty cc -> get_freshlist ww ty cc) args cc
    map (fun (vp, fid) -> Tid(vp, fid)) cc



// Return true if ungrounded ... wierd polarity - TODO invert
let rec maid_plaster ww = function // We have dual purpose pramargs and here we split them on the proper axis
    | Tty(Bsv_ty_id(nf, Some P_free, fid), _) -> true
    | Tty(ty, fid) when is_grounded ww ty ->
        vprintln 3 (sprintf "Disposed of arg %s ty=%s" fid (bsv_tnToStr ty))
        false
    | Tty(ty, fid) ->
        vprintln 3 (sprintf "+++ Ambiguious plaster dispose (plaster means dont need it since grounded) of arg %s %s" fid (bsv_tnToStr ty))
        true
    | Tid _ -> true


and is_grounded ww sty =
    let rec isg = function
        | ty when leaf_grounded_type_pred ty -> true
        
        | Bsv_ty_list(_, lst) -> conjunctionate (is_grounded ww) lst



        | Bsv_ty_knot(idl, kr) when not(nonep !kr) -> isg (valOf !kr)
        | Bsv_ty_stars(m, a) ->  isg a
        | Bsv_ty_id(nf, _, s) -> false

        | Bsv_ty_nom(_ , F_fun(rt, _, fargs), _)  ->  is_grounded ww rt && conjunctionate isg fargs // TODO can use args

        | Bsv_ty_nom(idl, _, args)     -> nullp(List.filter (maid_plaster ww) args)

        //| Bsv_ty_methodProto(methodname, _,  mth_provisos, proto, _) -> List.fold sum_ty_support cc (un_protoprotocol [] proto)
        | other -> sf(sprintf "is_grounded other: %A" other)
    let ans = isg sty
    //vprintln 0 (sprintf "is_grounded returns %A for %s" ans (bsv_tnToStr sty))
    ans



let temp_revert = function
    | (_, Some x, _)  -> x
    | (_, None, "")   -> sf ("temp_revert fail anon")
    | (so, None, fid) ->
        let mm = "temp_revert"
        //vprintln 0 (mm + sprintf ": temp_revert kludge name to type so=%s  for '%s'" (soToStr true so) fid)
        KB_type_dub(false, fid, [], g_builtin_lp)



let temp_revert_ol = function    
    | None ->   None
    | Some x -> Some (map f2o3 x)
    
let rec hyper_op_assoc v lst =
    match lst with
        | [] ->  None
        | ((x,y)::tt) -> if memberp v x then Some(y) else hyper_op_assoc v tt

        
let hyper_assoc_update f tag alist =
    let rec scan = function
        | [] -> [([tag], f None)]
        | (tags, v)::tt when memberp tag tags -> (tags, f (Some v)) :: tt
        | (t, v)::tt -> (t, v) :: scan tt
    scan alist

#if SPARE
//
// Not used...
//       
let ig_res_add__ lst item =
    match item with
        | IG_resource(resname, sff, ig, resats) ->
            let rec add = function
                | [] ->
                    let i = ig
                    //vprintln 0 (sff + "ig_res_add 0 " + xbToStr i)                    
                    [ IG_resource(resname, sff, i, resats) ]
                | IG_resource(_, _, ig', resats)::tt ->
                    let i = ix_and ig ig' //will almost always be ANDing with self
                    //vprintln 0 (sff + sprintf " ig_res_add 1 %s: %s /\ %s -> %s " (xbToStr gater) (xbToStr ig) (xbToStr ig') (xbToStr i))
                    IG_resource(resname, sff, i, resats)::tt
                | x::tt -> x :: add tt
            add lst
        | other -> [other]
        //| other -> sf (sprintf "other ig_res_add %A" other)
#endif

let ig_combine v r =
    match v with
        | _ when nonep r -> v
        
        | IG_alpha q ->
            match r with
                | Some(IG_alpha r) -> IG_alpha(ix_and q r) // Generally q==r
                | other ->sf (sprintf "other0 other=%A r=%A" other r)

        | IG_resource(k, sff, v1, resats) ->            
            match r with
                | None -> v
                | Some(IG_resource(resname, sff, i, ra)) -> IG_resource(resname, sff, ix_and i v1, resat_resolve resats ra) // Intrinsic guard conjunction.
                | other -> sf (sprintf "other1 %A" other)
        | other -> sf (sprintf "other2 other=%A r=%A" other r)                

// Fancy guard mux: for strict we represent the conjunction as a Map of all the resources
// When non strict, the resources are in a mux tree and we have conditional presence we have to gate in the guard and then we want a dammn'd disjunction!        
let ig_mux_dist split (ig1_baseline:resource_records_t) g tt ff =
    let igs = 
        if split then // non-strict if split.
            ig1_baseline.igs.Add((false, [funique "igux"]), IG_mux(g, tt.igs, ff.igs))
        else
            let folded_strict (m:iguards_t) k v = m.Add(k, ig_combine v (m.TryFind k))
            let a1 = Map.fold folded_strict ig1_baseline.igs tt.igs
            Map.fold folded_strict a1 ff.igs
    let xbine = Map.fold (fun (m:Map<key1_t, fpath_t list>) k v -> m.Add(k, lst_union (valOf_or_nil(m.TryFind k)) v))
    in
      {
        igs=         igs    
        fpaths=      xbine (xbine ig1_baseline.fpaths tt.fpaths) ff.fpaths
      }
      
let dig p q = ((p * 2) &&& 0xFFFFFFF) ^^^ q ^^^ (p >>> 16)



let choose_proto ww m arg =
    match arg with
    | Bsv_ty_nom(_, F_action, _)         -> (BsvProtoAction, g_action_ty)
    | Bsv_ty_nom(_, F_actionValue ty, _) -> (BsvProtoActionValue ty, ty)
    | Bsv_ty_id(_, _, _) -> (BsvProtoValue arg, arg)
    | _ -> (BsvProtoValue arg, arg)    

//    | other -> sf (mm + sprintf ": choose_proto other form %A" (bsv_tnToStr other))    

let rec myhash = function 
    | B_abs_lambda _ -> 1
    | B_aggr(ty, TU_tunion(vale, tag, no, totalwidth, tag_width)) -> dig 12 (myhash vale)
    | B_aggr(ty, TU_struct((_, (ty_, w, pos, vale))::_)) -> dig 13 (myhash vale)
    | B_aggr _ -> 2
    | B_field_select(_, a, b, c) -> dig (myhash a) (dig b c)
    | B_blift e    -> 3
    | B_dyn_b(_, e) -> dig 4 (myhash e)

    | B_shift(e, m)
    | B_mask(m, e) -> dig m (myhash e)
    | B_subs(df, l, r, _) -> dig (myhash l) (myhash r)
    | B_hexp(_, v) -> v.GetHashCode()


    | B_reveng_h _ -> 4
    | B_reveng_l _ -> 5

    | B_fsmStmt _ -> 6
    | B_ifc _ -> 7
    | B_ifc_immed _ -> 8

    | B_valof _ -> 9

    | B_diadic(_, t, f, _)
    | B_query(_, _, t, f, _, _) -> dig (myhash t) (myhash f)

    | B_pliStmt(s, _, _)
    | B_maker_abs(s::_, _, _, _, _,_,_,_)
    | B_subif(s, _, _, _)
    | B_vector(s, _)
    | B_knot(s::_, _)
    | B_string s
    | B_maker_l(s::_, _, _, _, _, _, _)
    | B_action((s, _, _), _, _, _)
    | B_lambda(Some s, _, _, _, _, _)
    | B_var(_, s::_, _) -> s.GetHashCode()

    | B_applyf(_, _, args, _)
    | B_applylam(_, _, args, _, _, _) -> List.fold (fun c (_, e) -> dig c (myhash e)) 10 args 
    | B_format _ -> 12
    
    | _ -> 11


let recast2 = function
    | (so, Some ty, name) -> ty
    | (so, None, name)    -> KB_type_dub(false, name, [], g_builtin_lp)

let recast2a = function
    | (so, Some ast_ty, name) -> (so, name, ast_ty)
    | (so, None, name)    -> (so, name, KB_type_dub(false, name, [], g_builtin_lp))

let recast1 = function
    | (SO_numeric, _, fid) -> (true, fid)
    | (SO_none _, _, fid)
    | (SO_type, _, fid) -> (false, fid)
    | other -> sf (sprintf "recast1 other %A" other)


let give_an_elaboration_stack_trace vd dynamic_chain = 
    vprintln vd (sprintf "   elab-stack-frames (%i): " (length dynamic_chain) + sfoldcr (fun (x, cp)->htosp x) dynamic_chain)


    
let exec_reporter_predicate = function
    | "warning"
    | "message"
    | "error" -> true
    | _ -> false
//
// :: error, warning and message handler.
//
// The warning and message handlers return their second arg.
//    
let exec_reporters ww mf m mc dynamic_chain args =
    let msg = m + ": " + mc + "T: " + sfold (fun (t,v)->bsv_expToStr v) args
    //let msg = m + ": " + mc + sprintf "%A" args
    vprintln 3 ("exec_reporters: Prepare an error/message/warning reporting msg: " + msg)
    let op_vd = 0 // Use zero, the most visible output.
    give_an_elaboration_stack_trace op_vd dynamic_chain
    
    let _ =
        if mc = "message" || mc = "messageM" then vprintln op_vd msg
        elif mc = "warning" || mc = "warningM" then hpr_warn msg
        elif mc = "error" || mc = "errorM" then cleanexit(msg)
        else
            vprintln op_vd (sprintf "Unsupported exec reporter " + mc)
            hpr_warn msg
    if length args >= 2 then hd(tl args) else (Bsv_ty_dontcare mc, B_hexp(Bsv_ty_dontcare mc, X_undef))
        
let mi_hexp_check = function
    | (Bsv_ty_intconst _) -> true
    | (Bsv_ty_intexpi(B_hexp(_, v))) -> true
    | other -> false
        
let mi_dehexp = function // TODO rationalise with mi_valueof etc..
    | (Bsv_ty_intconst n) -> xi_num n 
    | (Bsv_ty_intexpi(B_hexp(_, v))) -> v
    | other -> sf("other mi_dehexp " + bsv_tnToStr other) 

//==========================================================================
//==========================================================================
//==========================================================================


let gen_Bsv_ty_id(nf, prec, (fid, bi)) =
    match fid with
        | "int"        -> g_canned_int_ty // Special lower-case forms
        | "bit"        -> g_canned_bit_ty // The type bit[m:0] is synonymous with Bit#(m + 1) ... TODO but this "bit" takes no args?
        | "module"     -> g_maker_sty_ty
        | "$unit_type" -> g_unit_ty 
        | fid -> Bsv_ty_id(nf, prec, (fid, bi))  

let undefp = function
    | B_hexp(_, X_undef) -> true
    | _ -> false

let dontcarep = function
    | Bsv_ty_dontcare _ -> true
    | _ -> false

let resolve_intLiteral ww mf = function
    | KB_intLiteral(fwo, (dontcares, b, s), lp) ->
        let mf() = mf() + lpToStr0 lp + sprintf ": intLiteral base=%i s=" b + s
        let def_fw = 32
        let is_xzq c = (c = '?'|| c = 'x' || c = 'X' || c = 'z' || c = 'Z')
        match fwo with
            | None    ->
                let _ = cassert(b=10, "base is ten")
                (Bsv_ty_integer, xi_num (System.Convert.ToInt32 s), None) // hmmm canot cope with wide ints without field sel ... TODO trap overflow
            | Some fw when dontcares ->
                let rec splitup_q pos = function // Partition at the query symbols.
                    | [] -> []                        
                    | a :: tt when is_xzq a ->  splitup_q (pos+1) tt
                    | a::tt -> splitup_notq pos [] (a::tt) 
                and splitup_notq pos sofar = function
                    | [] when nullp sofar -> []
                    | [] -> [(pos - length sofar, length sofar, sofar)]
                    | a :: tt when is_xzq a && nullp sofar->  splitup_q (pos+1) tt
                    | a :: tt when is_xzq a -> (pos - length sofar, length sofar, sofar) :: splitup_q (pos+1) tt
                    | a::tt ->  splitup_notq (pos+1) (a::sofar) tt 

                let splots = splitup_q 0 (rev(explode s))
                let ifw = if fw="" then def_fw else System.Convert.ToInt32 fw
                let splots' = map (fun (offset, width, sex) -> (offset, width, snd(verilog_literal_parse mf "" 32 (b, sex)))) splots
// composite '?110?1011?0101' dont care, sublists=[(0, ['0'; '1'; '0'; '1']); (5, ['1'; '0'; '1'; '1']); (10, ['1'; '1'; '0'])]
// sublists=[(0, 5I); (5, 11I); (10, 6I)] ans=6501I
                let degenf c (offset:int, width, vale) = c + powerXI (BigInteger b) (BigInteger offset) * vale 
                let degenerate_value = List.fold degenf 0I splots' // Degenerate, treating dont-cares as zeros
                // (mm + sprintf "  with dont cares, sublists=%A ans=%A" splots' degen)
                (mk_int_ty ifw, local_xi_bnum degenerate_value, Some(b, splots'))
            | Some fw when not dontcares ->
                let (i_fieldwidth, vale) = verilog_literal_parse mf fw def_fw (b, explode s)
                (mk_int_ty i_fieldwidth, local_xi_bnum vale, None)


//
//
//
let prep_module_app ww (mf:unit->string) arg = 
    let provisos' = [] // Really - ?
    let mc generator = match generator with
                        | ("warning" as mc)  | ("error" as mc) | ("message" as mc)
                        | ("warningM" as mc)  | ("errorM" as mc) | ("messageM" as mc)  -> //perhaps only the M form should be here.
                            let _ = exec_reporters //ww m (lpToStr0 lp) mc genargs'' -- dont do it now!
                            Some generator
                        | _ -> None

    let rec preper grds = function
        | KB_id(generator, (lp, uid)) -> // e.g. mkRegU takes no args. We steal the rhs identifier's uid for the function app uid since it is not otherwise used. The lhs identifier will be used for the do's uid.

            let mf() = mf() + ":" + lpToStr0 lp + sprintf " prep_module_app (decldo) (no apply - like mkRegU) of '%s'" generator
            [(grds, (mf, generator, mc generator, provisos', None, uid))]

        | KB_query(grd, tt, ff, uid) -> (preper ((true, grd)::grds) tt) @ (preper ((false, grd)::grds) ff)

        | KB_functionCall(KB_ast(KB_id(generator, (lp, _))), args, (_, uid)) ->
            let mf() = mf() + ":" + lpToStr0 lp + " module app (decldo) (functionCall syntax) + " + generator
            //let ww = WN mm ww
            let filter_off_eventcontrol (n, actual) (cc, ec) =
                    match actual with  
                    | KB_eventControl(s, eci, lp) ->
                        let mm = lpToStr0 lp + ":KB_event_control + " + s
                        //dev_println (sprintf "evc: deferred pre %A" eci) // +++ devx: Deferred KB_id ("testClock",(LP ("smalltests/Test1b.bsv",11), "Z18_testClock"))
                        //let eci' = (norm_bsv_exp_rmode ww bobj (uNn 1 utag00 n) ee) eci
                        //dev_println (sprintf "evc: deferred %A" eci') //
                        (cc, (s, eci, lp)::ec)
                    | oarg -> (oarg:: cc, ec)

            let (args_sans_evc, evc) = List.foldBack filter_off_eventcontrol (List.zip [1..length args] args) ([], [])
            [(grds, (mf, generator, mc generator, provisos', Some(args, evc, args_sans_evc), uid))] // TODO dont need all these bits

        | other -> muddy (mf() + sprintf ": prep_module_app other %A " other)        
    preper [] arg
    
let prep_for ww qm = function
    | KB_for(s1, g, s2, body, lp) ->
        let mm = lpToStr0 lp + " for loop : expanded to a while"
        let ww = WF 3 qm ww mm
        let w = KB_while(g, KB_block(Some "fromfor-body", [ body; s2 ], None, lp), lp)
        //dev_println (sprintf "prep_for: g=%A" g)
        let expanded = KB_block(Some "bev-fromfor", [ s1; w], None, lp)
        (mm, ww, expanded)


let prep_typeIntLiteral ww aa = 
    let (b, fwo, mf, s) =
        match aa with
            | KB_typeNat s                              -> (10, None, (fun()->s), s) 
            | KB_intLiteral(fwo, (dontcares, b, s), lp) -> (b, fwo, (fun()->lpToStr0 lp), s)
        //vprintln 0 (mf() + sprintf ": Temp ignore fw %A and use %i ?" fw dw)
    let dw = 32
    let ans =
        match fwo with
        | None ->
            try Bsv_ty_intconst(System.Convert.ToInt32 s) // Type/width has been discarded.
            with _ -> cleanexit(mf() + sprintf ": integer '%s' outside supported 32 bit range" s)

        | Some fw ->
            let (tt, vale) = verilog_literal_parse mf fw dw (b, explode s)
            // TODO check value does not overflow provided field width.
            in try Bsv_ty_intconst(int vale) // Type/width has been discarded.
               with _ -> cleanexit(mf() + " integer outside supported 32-bit range " + s)
    ans

    
let rec gpred__ ww ee mm = function//This will return false if there is an unbound type var or on the nasty deabs. The type separation into ty and absty embodies this and so we dont need this code...?
    | Bsv_ty_intconst _ -> true
    | other -> sf (mm + sprintf ": other form in gpred %A" other)


// type_is_def could potentially hold in two ways: either for a grounded type or for a defined abstract type.
// When gg holds we check for a grounded type.
// Type identifiers beginning with a capital letter (except for int, module and bit exceptions) ...
    
let rec type_is_def_s (ee:ee_t) gg = function
    | "$unit_type" -> Some g_unit_ty 
    | "int"        -> Some g_canned_int_ty  // Special lower-case forms
    | "bit"        -> Some g_canned_bit_ty
    | "module"     -> Some g_maker_sty_ty
    | typen ->
        // use System.Char.IsUpper(typen.[0]) ?
        match ee.genenv.TryFind [typen] with
            | None -> None
            | Some d ->
                match d with
                    | GE_binding(bound_name, whom, EE_typef(_, sty)) -> Some sty

                    | d_ ->
                        vprintln 0 (sprintf "is_def finds %s %A" typen d_)
                        muddy "Some d_ // May not be a type!"
    
and type_is_def (ee:ee_t) gg = function
    //| KB_typeFormal(SO_numeric, i) -> true // A numeric type cannot be undefined as a type.
    | KB_id(typen, _) -> not (nonep(type_is_def_s ee gg typen))
    //| KB_typeFormal(SO_formal, i)
    | KB_type_dub(_, typen, args, lp) when (not gg) -> not(nonep(type_is_def_s ee gg typen))
    | KB_type_dub(_, typen, args, lp) when gg -> not(nonep(type_is_def_s ee gg typen)) && conjunctionate (type_is_def ee gg) (map recast2 args)
        
    | other -> sf (sprintf "type_is_def other %A" other)


let rec isrec_stunion_type ww bobj ee stack ((key, items, idl) as arg) =
    let mm = key + " isrec? " + htosp idl
    let ww = WF 3 "isrec_stunion_type" ww mm
    match arg with
    | ("struct", items, nomid) ->
        let qor_struct = function
            //| KB_id(typen, _) 
            | KB_structMember(KB_id(typen, _), _)               -> memberp [typen] stack // this ignores prams being different?
            | KB_structMember(KB_type_dub(_, typen, (*prams*)_, lp), _) -> memberp [typen] stack // this ignores prams being different?
            

            //| KB_structMember(ty, id) when false -> false
            | KB_builtin _ -> false
            | other -> sf (sprintf " other form in qor_struct %A" other)
        disjunctionate qor_struct items

    | ("tunion", variants, nomid) ->
        if memberp nomid stack then true
        else
            // Lasso stem may occur owing to a flat name ... but ultimately it is finite or will loop!
            // Really we have two stacks as one here.
        let stack' = nomid::stack
        let qor_tagged_union = function 


// This is a simple recursion checker... TODO check for mutual recursions needed ...
            | KB_unionMember(Some(KB_id(typen, (lp, _))),      tag) 
            | KB_unionMember(Some(KB_type_dub(_, typen, _, lp)), tag) -> memberp [typen] stack // this ignores prams being different?

            | KB_unionMember(None, id) -> false
            | KB_unionSubStruct(items, tagid) ->
                let qk x = isrec_stunion_type ww bobj ee stack' ("struct", items, tagid::idl)
                conjunctionate qk items

            | other -> sf ("qor_union other " + sprintf "%A" other)

        disjunctionate qor_tagged_union variants


// This is a little naive for paths such as a.[b].c.[d] since the stars are all added up!  TODO.
let add_stars_decl stars ct =
    if false then add_stars stars ct
    else
        let vec_wrap ct =
            let bb = funique "vec3470vec"
            Bsv_ty_nom({g_blank_nom_typ_ats with nomid=["Vector"] }, F_vector, [Tid(VP_value(true, [bb + "len"]), bb + "len"); Tty(ct, bb + "ct")])
            
        let rec add_vector stars ct =
            if stars < 0 then sf "add negative stars unexpected"
            elif stars = 0 then ct
            else add_vector (stars-1) (vec_wrap ct)
        add_vector stars ct


// Dereference n times.
let rec take_stars mf n ct =
    if n = 0 then ct
    elif n < 0 then add_stars (-n) ct
    else
        match tyderef_o mf ct with
            | None -> cleanexit(mf() + sprintf ": dereference failed on  [L879] type=%s" (bsv_tnToStr ct))
            | Some r -> take_stars mf (n-1) r

        
// Augment_env_entry: updates ee.
// (Note it is not this code, but it is ass_imp that makes the imperative assignments as, for example, a FOR loop is unwound during elaboration.)
let rec augment_env_entry ww bobj mf site (ee:ee_t) arg =
    let vd = bobj.normalise_loglevel
    match arg with
    | Aug_mut(bound_name, idl, (aty, fer, lp)) ->
        //let _ = ee.muts.add fer (aty, nvector ee {iname=idl; ct=ct; len=subs; vt=aty; data=NoEqMap.empty; })
        if vd>=4 then vprintln 4 (sprintf "Augment environment mutable.  bound_name=%s idl=%s. mutable_reference_key=%s"  (hptos bound_name) (hptos idl) (fer))
        let w = GE_mut(idl, aty, site, fer)    
        { ee with genenv=ee.genenv.Add(bound_name, w) }
    | Aug_tycl(tyclass_id, idl, bodies, defs, lp) ->
        if vd>=4 then vprintln 4 (sprintf "Augment environment entry tycl. bound_name=%s  idl=%s" tyclass_id (hptos idl))
        match ee.typeclasses.TryFind tyclass_id with
            | None ->
                vprintln 3 (lpToStr0 lp + sprintf ": defined typeclass %s with %i items, idl=%s" tyclass_id (length defs)  (hptos idl))
                let add_overloaded ee (id, ty) =
                    let kv = ([id], GE_overloadable(ty, [])) 
                    if vd>=4 then vprintln 4 (sprintf "Add to typeclass %s id=%s" tyclass_id id)
                    // TODO see if defined already as not part of a typeclass.
                    { ee with genenv=ee.genenv.Add kv }
                let ee = List.fold add_overloaded ee defs // add each item
                { ee with typeclasses=ee.typeclasses.Add(tyclass_id, (idl, bodies)); }
            | Some _ -> cleanexit(lpToStr0 lp + sprintf ": typeclass '%s' defined more than once (cannot currently be extended with further members)." tyclass_id)
                
    | Aug_env(tyclass, bound_name, (dmtof, who, EE_typef(gf, ty))) ->  // A type var or type only augment.
         let w = GE_binding(bound_name, who, EE_typef(gf, ty))
         //if htosp idl = "RegFile.RegFileSize" then dev_println (sprintf "swold: augmentation is %A" w)
         if vd>=4 then vprintln 4 (sprintf "Augment environment entry. type only. bound_name=%s" (hptos bound_name))
         { ee with genenv=ee.genenv.Add(bound_name, w); } 

    | Aug_env(tyclass, bound_name, (dmtof, who, EE_e(ty, nv))) -> // value augment - used where possible.
      if vd>=4 then vprintln 4 (sprintf "Augment environment EE_e. bound_name=%s" (hptos bound_name))
      let nv =
        match tyclass with
            | None ->
                let _ =
                    match ee.genenv.TryFind bound_name with
                    | Some _ when (not dmtof) ->
                        vprintln 3 (mf() + sprintf ": defined '%s' more than once. [L6181] - spurious error?" (hptos bound_name))
                        ()
                    | _ -> ()
                let nv = GE_binding(bound_name, who, EE_e(ty, nv))
                (bound_name, nv)
            | Some (tyclass_ids) ->
                match ee.genenv.TryFind bound_name with
                    | None ->
                        let w = GE_binding(bound_name, who, EE_e(ty, nv))
                        (bound_name, w) 
                    | Some(GE_overloadable(prototy, lst)) ->
                        let nv = GE_overloadable(prototy, (tyclass_ids, ((ty, []), nv))::lst)
                        (bound_name, nv)
                           // TODO check there is not existing overload for this signature?
                           // TODO we should check that this new overload is conformant with prototy
                    | Some other -> cleanexit(mf() + sprintf "attempt to overload '%s' which is something already defined outside of a typeclass '%s': oldform=%A" (hptos bound_name) (htos1 "/" (map tyclassToStr tyclass_ids)) other)
      { ee with genenv=ee.genenv.Add nv }

    | Aug_env(tyclass, bound_name, (dmtof, who, lee)) when false -> //a complete replicant clause ?
      let nv =
        if vd>=4 then vprintln 4 (sprintf "Augment environment Aug_env: bound_name=%s" (hptos bound_name))
        match tyclass with
            | None ->
                match ee.genenv.TryFind bound_name with
                    | Some xx when (not dmtof) ->
                        vprintln 3 (mf() + sprintf ": defined '%s' more than once. [L6204]: olddef=" (hptos bound_name)  + "probably spurious error")
                        (bound_name, GE_binding(bound_name, who, lee))  
                    | _ ->
                        let w = GE_binding(bound_name, who, lee)
                        (bound_name, w)  
            | Some (tyclass_ids) ->
                match ee.genenv.TryFind bound_name with
                    | None -> (bound_name, GE_binding(bound_name, who, lee)) 
                    | Some(GE_overloadable(prototy, lst)) ->
                        match lee with
                            | EE_typef(gf, sty) when false ->
                                let nv = GE_overloadable(prototy, (tyclass_ids, ((sty, muddy "nv cannot.."), muddy "nv cannot be provided for a type"))::lst)
                                (bound_name, nv)
                           // TODO check there is not existing overload for this signature?
                           // TODO we should check that this new overload is conformant with prototy
                            | other -> sf (sprintf "other overloader %A" other)
                    | Some other -> cleanexit(mf() + sprintf "attempt to overload '%s' which is something already defined outside of a typeclass '%s': oldform=%A" (hptos bound_name) (htos1 "/" (map tyclassToStr tyclass_ids)) other)
      //if vd>=5 then vprintln 5 ( sprintf "Aug_env_entry '%s'" (hptos bound_name))
      { ee with genenv=ee.genenv.Add nv }

    | Aug_tags newkeys ->
        let keys_augment (c:ctor_tags_t) (s, (st, n)) =
            if vd>=5 then vprintln 5 ( "   add tag " + s)
            match c.TryFind s with
                | None -> c.Add(s, [(st, n)])
                | Some lst -> c.Add(s, (st, n)::lst)
        { ee with ctor_tags=List.fold keys_augment ee.ctor_tags newkeys }


    //| other -> sf (sprintf "Augment env other form %A" other)



//
//  Unify two types.
//
let rec sum_type_meet ww (bobj:bsvc_free_vars_t) (mf:unit->string) h0 (ll_top, rr_top) =

    //vprintln 0 (sprintf "sum_type_meet p1 %s: LT=%s RT=%s" m (bsv_tnToStr ll_top) (bsv_tnToStr rr_top))
    // Silly: all the work is done twice, once in choose and once in meet.
    let (_, ll_top) = sty_choose ww bobj mf (ll_top, rr_top)

    //vprintln 0 (sprintf "sum_type_meet p2 %s: LT=%s RT=%s" m (bsv_tnToStr ll_top) (bsv_tnToStr rr_top))
    match sum_type_meet_serf ww (bobj:bsvc_free_vars_t) mf h0 (ll_top, rr_top) with
        // TODO may want to recollate the answer when not called from newsum yup... as in argfun's call of ty_eval ?
        
        | [(h0, ans)] ->
            //vprintln 0 (sprintf "   Pairserf done ans=%s" (bsv_tnToStr ans))
            //vprintln 0 (mf() + sprintf ": sum_type_meet:\n   L=%s\n   R=%s\n  Ans=%s\n   h0=%s" (bsv_tnToStr ll_top) (bsv_tnToStr rr_top) (bsv_tnToStr ans) (sfold hindToStr h0))
            //vprintln 0 (mf() + sprintf ": sum_type_meet:\n   L=%A\n   R=%A " (ll_top) (rr_top) )
            (h0, ans)
        | [] ->
            vprintln 0 (mf() + sprintf ": cannot resolve types L and R:\n   L=%s\n   R=%s" (bsv_tnToStr ll_top) (bsv_tnToStr rr_top))
            cleanexit(mf() + " incompatible types (L1204)")
        | lst  ->
            vprintln 0 (mf() + ": several answers " + sfold bsv_tnToStr (map snd lst))
            // currently this cannot represent we a unification where certain equivalences are conditional on others...
            muddy "several"
        

// Cartesian type reduce.
// Find any pair(s) from the cartesian product of two lists of hindly/types that resolve.
and sty_product_choose ww bobj mf symmetricf sags_l sags_r =
    let ww = WN "sty_product_choose" ww
    dev_println ("not all operators have symmetric types")
    let rec hresolve l_h r_h =
        if nullp l_h then r_h
        elif nullp r_h then l_h
        else
            let (l_h, (l_ty, l_noms, l_vp)) = (tl l_h, hd l_h)
            let rec resolve2 donef = function
                | [] -> (donef, [])
                | (r_ty, r_noms, r_vp)::tt -> // TODO: can a resolution trigger a cascade?
                    let mergef = list_intersection_pred (map fst l_noms, map fst r_noms)
                    let (donef, nv) =
                        if mergef then
                            let ty =
                                match (l_ty, r_ty) with
                                    | (None, None) -> None
                                    | (Some _, None) -> l_ty
                                    | (None, Some _) -> r_ty
                                    | (Some lty, Some rty) ->
                                        let sags = sum_type_meet_serf ww bobj mf [] (lty, rty)
                                        if length sags = 1 then Some(snd(hd sags))
                                        else
                                            hpr_yikes(sprintf "Cartesian type binding clash on %A and %A" l_noms r_noms)
                                            Some lty // Hope for best
                            let noms = lst_union l_noms r_noms
                            let vp = vp_resolve(l_vp, r_vp)
                            (true, (ty, noms, vp))
                        else
                            (donef, (r_ty, r_noms, r_vp)) 
                    let (donef, tt) = resolve2 donef tt
                    (donef, nv::tt)

            let (donef, r_h) = resolve2 false r_h
            let r_h = if donef then r_h else (l_ty, l_noms, l_vp) :: r_h
            hresolve l_h r_h

    let sty_product1 h0 (l_h, l_item) =
        let sty_product2 h0 (r_h, r_item) =
            let resolved_h0 = hresolve l_h r_h
            let nvl = sum_type_meet_serf ww bobj mf resolved_h0 (l_item, r_item)
            if nullp nvl then h0
            else
                let nvl = map (fun (h0, sty) -> (l_item, r_item, h0, sty)) nvl
                nvl @ h0

        List.fold sty_product2 h0 sags_r
    let ans = List.fold sty_product1 [] sags_l
    ans

    
//
// Where a number of types are possible we choose one/those that match.  Removes any ty_list construct from the lhs operand.
//
// We could just use type_meet directly ...? then bind the h entries ... ? this is perhaps quicker..
// 
and sty_choose ww bobj mf (opts, targ) = // opts will represent more than one type using Bsv_ty_list construct ...
    let choose_check (tl, tr) =
        let h0 = []  // g_null_tb
        match sum_type_meet_serf ww bobj mf h0 (tl, tr) with
            | [] -> false
            | (h_, ans)::_ -> 
                //vprintln 3 (sprintf "choose_check : type compatible check: found ok %s u %s -u-> %s" (bsv_tnToStr tl) (bsv_tnToStr tr) (bsv_tnToStr ans))
                true

    let m_decisions = ref []

    let rec decision_walker arg = 
        let walk ty =
            let ans = chooser true (ty, ty)
            match ans with // A silly way to make a deep walker!
                | Some x -> x
                | None   -> sf "Chooser walk step returned none"
        let recode = function
            | Tty(ty, tyid) -> Tty(walk ty, tyid)
            | x -> x
        recode arg
        
    and chooser justwalk (arg, targ) =
        //The old expression crashes at compile time on fsharpc 4.1?
        //let qf = (if justwalk then decision_walker else (fun x -> x))
        let qf arg = if justwalk then decision_walker arg else arg
        let aok arg = conjunctionate not_nonep arg
        let aok1 lst = // Need two copies of this for the fsharp implementation to type check!
            conjunctionate not_nonep lst
        let tc1 (l, r) =
            match chooser justwalk (l, r) with
                | Some item -> Some item
                | None ->
                    //vprintln 3 (mf() + sprintf ": chooser none from %s cf %s" (bsv_tnToStr arg) (bsv_tnToStr targ))
                    None
                
        //dev_println (mf() + sprintf ": chooser zip:\n       L=%s\n  zip: R=%s" (bsv_tnToStr arg) (bsv_tnToStr targ))
        match (arg, targ) with // This must walk all abstract forms.

                | (Bsv_ty_nom(s_l, F_struct(items, details_), fv), Bsv_ty_nom(s_r, F_struct(items_r, detail_r_), fv_r)) when length items = length items_r ->
                    if not (nonep details_) then vprintln 3 (mf() + " chooser: details need recompute for " + htosp s_l.nomid)
                    let qf0_struct((tag_id, t), (tag_id_r, t_r)) =
                        if tag_id <> tag_id_r then sf "struct tags mismatch"
                        else tc1(t, t_r)

                    let bb = map qf0_struct (List.zip items items_r)
                    if aok bb then Some(Bsv_ty_nom(s_l, F_struct(List.zip (map fst items) (map valOf bb), None), map qf fv))
                    else None
                    
                | (Bsv_ty_nom(s_l, F_tunion(lr, items, details_), fv), Bsv_ty_nom(s_r, F_tunion(_, items_r, detail_r_), fv_r))  when length items = length items_r ->
                    if not_nonep details_ then vprintln 3 (mf() + " chooser: details need recompute for " + htosp s_l.nomid)
                    let qf0_tunion = function
                        | ((tag_id, None), _) -> Some None
                        | ((tag_id, Some t), (tag_id_r, Some t_r)) ->                            
                            if tag_id <> tag_id_r then sf "tuniton tags mismatch"
                            else (Some(tc1(t, t_r)))
                        | _ -> sf (mf() +  " wierd tunion choice")

                    let bb = map qf0_tunion (List.zip items items_r)
                    if aok1 bb then Some(Bsv_ty_nom(s_l, F_tunion(lr, List.zip (map fst items) (map valOf bb), None), map qf fv))
                    else None
                    

                | (Bsv_ty_nom(s_l, F_actionValue  si_l, fv), Bsv_ty_nom(s_r, F_actionValue si_r, fv_r)) ->
                    match tc1 (si_l, si_r) with
                        | Some si_l' -> Some(Bsv_ty_nom(s_l, F_actionValue(si_l'), map qf fv))
                        | None -> None

                | (Bsv_ty_nom(s_l, F_fun(rt_l, mats_l, args_l), fv), Bsv_ty_nom(s_r, F_fun(rt_r, mats_r, args_r), fv_r)) ->
                    let rt' = tc1 (rt_l, rt_r)
                    if nonep rt' then None
                    else
                    let (ll, lr) = (length args_l, length args_r)
                    if ll<>lr then
                            vprintln 0 (sprintf "chooser cannot zip fun %s^%i %s^%i (who=%s,%s)" mats_l.methodname ll mats_r.methodname lr mats_l.who mats_r.who)
                            None
                    else
                        // potentially want to mux the body_gamma
                        let args' = map (chooser justwalk) (List.zip args_l args_r)
                        let nst = if length s_l.nomid >= length s_r.nomid then s_l else s_r
                        if aok args' then Some(Bsv_ty_nom(nst, F_fun(valOf rt', mats_l, map valOf args'), map qf fv))
                        else None
                        
                | (Bsv_ty_nom(s_l, F_subif(a_l, si_l), fv), Bsv_ty_nom(s_r, F_subif(a_r, si_r), fv_r)) ->
                    match tc1 (si_l, si_r) with
                        | None -> None
                        | Some si_l' -> Some(Bsv_ty_nom(s_l, F_subif(a_l, si_l'), map qf fv))


                | (Bsv_ty_nom(s_l, F_ifc_1 ii_l, fv), Bsv_ty_nom(s_r, F_ifc_1 ii_r, fv_r)) -> 
                    if s_l.nomid <> s_r.nomid then
                        vprintln 3 (mf() + sprintf " (chooser) cannot use interface %s when %s expected" (htosp s_l.nomid) (htosp s_r.nomid))
                        None
                    else
                        let (ll, lr) = (length ii_l.iparts, length ii_r.iparts)
                        //vprintln 0 (sprintf "chooser zip ifc %i %i" ll lr)
                        if ll <> lr then sf ("strange different lengths despite... overloading of interfaces?")
                        let iparts' = map (chooser justwalk) (List.zip ii_l.iparts ii_r.iparts)
                        if aok iparts' then Some(Bsv_ty_nom(s_l, F_ifc_1 { ii_l with iparts=map valOf iparts' } , map qf fv))
                        else None
                    
                | (Bsv_ty_list(id, lst), targ) ->
                    if justwalk then
                        match op_assoc id !m_decisions with
                            | None ->
                                vprintln 3 ("chooser: No decision found for type selector " + id)
                                Some(Bsv_ty_list(id, lst))
                            | Some[ n ]->
                                vprintln 3 (sprintf "selected %i which is %s from %A" n (bsv_tnToStr(lst.[n])) lst)
                                Some(lst.[n])
                            | _ -> muddy "more than one - do a new list"
                    else
                    let zop (n, ll) cd = 
                        match chooser justwalk (ll, targ) with
                            | None -> cd
                            | Some a ->
                                // keep options open with list here ... TODO review.
                                vprintln 3 (sprintf "chooser: zop '%s' kept item %i %s chosen for %s" id n (sfold bsv_tnToStr [a]) (bsv_tnToStr targ))
                                (n, [a]) :: cd

                    match List.foldBack zop (List.zip [0..length lst-1] lst) [] with
                        | [] -> cleanexit (mf() + ": chooser selection was none - no suitable overload")
                        | [(n, a)] ->
                            vprintln 3 (mf() + sprintf ": chooser decision: selected %s one item: no=%i" id n)
                            mutadd m_decisions (id, [n])
                            //Some (hd a)  // old
                            Some (lst.[n]) // new - the meet is casual over what it returns - return unadulterated.                          
                        | items ->
                            vprintln 3 (mf() + sprintf ": chooser decision: selected %s several, %i, items" id (length items) + sfold (fun (n,a) -> i2s n) items)
                            match op_assoc id !m_decisions with
                                | None ->
                                    let nos = map fst items
                                    mutadd m_decisions (id, nos)
                                    //let ans = map hd (map snd items) // old
                                    let ans = map (fun n->lst.[n]) nos // new - the meet is casual over what it returns - return unadulterated.                          
                                    Some(gen_Bsv_ty_list ans)
                                | Some _ -> sf " // TODO want intersection of multiple mutadds"

                | (other, targ) when choose_check (other, targ) -> Some other 

                | _ -> None // Discard other forms from selection.

    match chooser false (opts, targ)  with
        | Some l ->
            //vprintln 0 (mf() + sprintf ": chooser 1st pass returns L=" + bsv_tnToStr l)
            // run chooser a second time to recode the decisions - just use it as a walker
            match chooser true (l, targ) with
                | Some item -> (!m_decisions, item)
                | _ -> sf "Choice chos impossible 1 - a singleton list should be always returned in walk stage" 
        | other -> cleanexit(mf() + sprintf ": sty_choose: no possibilities when unifying types:\n   L=%s\n   R=%s" (bsv_tnToStr opts) (bsv_tnToStr targ))


//
// Unify a pair of types if possible.  Returns a list of possible answers where an answer is a pair of (unifications, specfic_type).
// This leaf routine must not abend if no match possible; it just returns null.
and sum_type_meet_serf ww (bobj:bsvc_free_vars_t) mf cc (ll_top, rr_top) =  // TODO this would better just return unifications and not returning a specific answer - the answer being rezz'd when needed.
    let avd = 3
    let rec pairserfpram stack cc pairs =
        let rec step cc = function
            | [] ->  [cc]
            | (lt, rt)::tt ->
                let (lt, rt) = (rose lt, rose rt)
                //vprintln 0 ("  Pairserfing arg lt=" + bsv_tnToStr lt)
                //vprintln 0 ("  ....serfing arg rt=" + bsv_tnToStr rt)        
                let (lst:(hindley_t * bsv_type_t) list) = pairserf stack false cc (lt, rt)
                list_flatten (map (fun (h, ty_) -> step h tt) lst)
        let ans = step cc pairs
        ans

    and pairserf stack flip cc ((lt:bsv_type_t), (rt:bsv_type_t)) = //sum_type_meet
        //vprintln 0 (" Pairserfing lt=" + bsv_tnToStr lt)
        //vprintln 0 (" ....serfing rt=" + bsv_tnToStr rt)        

        let trybind id lrt cc (no, l) sofar =
            vprintln 3 (sprintf "start trybind option no %i" no)
            let lst = pairserf stack flip cc (l, lrt)
            vprintln 3 (sprintf "pairserf left/right list '%s' item %i %s lstlen=%i" id no (bsv_tnToStr l) (length lst))
            lst @ sofar

        match (lt, rt) with
        | (Bsv_ty_list(id, lst), rt) ->
            let rlst = List.foldBack (trybind id rt cc) (List.zip [0..length lst-1] lst) []
            match rlst with
                | lst -> lst // map (fun (cc, no, ty) -> (cc, ty)) lst
        | (lt, Bsv_ty_list(id, lst)) ->
            let rlst = List.foldBack (trybind id lt cc) (List.zip [0..length lst-1] lst) []
            match rlst with
                | lst -> lst //map (fun (cc, no, ty) -> (cc, ty)) lst

            // TODO use kren squirrel here.
        | (Bsv_ty_knot(idl, kl), _) when not(nonep !kl || memberp idl stack) -> pairserf (idl::stack) false cc (valOf !kl, rt)
        | (_, Bsv_ty_knot(idl, kr)) when not(nonep !kr || memberp idl stack) -> pairserf (idl::stack) false cc (lt, valOf !kr)

        | (Bsv_ty_knot(idl, k), _) 
        | (_, Bsv_ty_knot(idl, k)) -> [(cc, lt)]

        | (tt, Bsv_ty_dontcare _)
        | (Bsv_ty_dontcare _, tt) ->  [(cc, tt)]


        | (Bsv_ty_nom(_, F_vector, [lenny; ct]), Bsv_ty_stars(n, sct)) 
        | (Bsv_ty_stars(n, sct), Bsv_ty_nom(_, F_vector, [lenny; ct])) when n > 0 ->        
            let lst  = pairserf stack false cc (sct, rose ct)
            map (fun (cc, _) -> (cc, lt)) lst            
             

        | (Bsv_ty_nom(lid, F_actionValue l, _),  Bsv_ty_nom(rid, F_actionValue r, _)) when lid.nomid = rid.nomid ->
            let lst  = pairserf stack false cc (l, r)
            map (fun (cc, _) -> (cc, lt)) lst
            
        | (Bsv_ty_nom(xidl, F_actionValue l, _), r)
        | (l, Bsv_ty_nom(xidl, F_actionValue r, _)) ->
            let lst = pairserf stack false cc (l, r)
            let ans q = Bsv_ty_nom(xidl, F_actionValue q, [Tty(q, "$actionvalue-fid")]) // NOT CORRECT NOT A Tty always?
            map (fun (cc, q) -> (cc, ans q)) lst            
            //muddy(sprintf "2pairserfpram cc (l, r) // Discard 'Action' wrappers lt=%A rt=%A" (bsv_tnToStr lt) (bsv_tnToStr rt))

        | (Bsv_ty_nom(idl_l, xl, args_l), Bsv_ty_nom(idl_r, xr, args_r)) ->
            let temp_pred = function
                | F_ifc_temp -> true
                | _ -> false
            let function_pred = function
                | F_fun _ -> true
                | F_tyfun _ -> true                
                | _ -> false

                
            if temp_pred xl   then [(cc, rt)]
            elif temp_pred xr then [(cc, lt)]
            else
            let isfunc = function_pred xl
            let _ = 
                if idl_l.nomid <> idl_r.nomid then
                    vprintln 3 (sprintf " ID-L=%s\n ID-R=%s" (bsv_tnToStr lt) (bsv_tnToStr rt))
                    let svd = if isfunc then 4 else 0
                    vprintln svd (mf() + sprintf ": nominal mismatch: (will attempt structural type match) using type %s where expecting %s  (%i cf %i)" (htosp idl_l.nomid) (htosp idl_r.nomid) (length args_l) (length args_r)) // Not necessarily an error - structural matching may work
            let cc_lst =
                match (xl, xr) with // pair-serfing on the args is always sufficient: don't need to be deep here. Droppings will be rewritten later.

                    | (F_struct(ls, _), F_struct(rs, _))          -> [cc]
                    | (F_tunion(_, ls, _), F_tunion(_, rs, _))    -> [cc]                    
                    | (F_actionValue ret_l, F_actionValue ret_r)  -> [cc]
                    | (F_tyfun x, F_tyfun y) when x=y             -> [cc]
                    | (F_ntype, F_ntype)     -> [cc]
                    | (F_vector, F_vector)   -> [cc]
                    | (F_action, F_action)   -> [cc]
                    | (F_ifc_1 _, F_ifc_1 _) -> [cc]
                    | (F_subif _, F_subif _) -> [cc] // We might want to support structural matching of subinterfaces instead of nominal!
                    | (F_enum _, F_enum _)   -> [cc] // We will get an above msg if not the same enum or subifc etc.
                    | (F_fun(fret_l, x_l, fargs_l), F_fun(fret_r, x_r, fargs_r)) when length fargs_l = length fargs_r ->
                        let fgrounded = nullp args_l && nullp args_r
                        // TODO skip if fgrounded  in FIFO = vprintln 0 (sprintf "fargs of a function are not good enough - here we deep bind: grounded=%A" fgrounded)
                        let rec fun_pairserf_list h0_lst = function
                            | [] -> h0_lst
                            | (pair, no)::pairs ->
                                let refine h0 cc = 
                                    match pairserf stack false h0 pair with 
                                        | [] ->
                                            vprintln 3 (sprintf "F_fun argpair_serf end case on %i" no)
                                            cc
                                        | [(h0, _)] -> h0 :: cc
                                        | others ->
                                            vprintln 3 (sprintf " +++ F_fun argpair_serf bifurcation on function args/result")
                                            map fst others @ cc
                                let ans = List.foldBack refine h0_lst []
                                if nullp ans then vprintln 3 (sprintf "F_fun no unifications possible")
                                fun_pairserf_list ans pairs
                        let h0_lst = fun_pairserf_list [cc] (zipWithIndex(List.zip (fret_l::fargs_l)  (fret_r::fargs_r)))
                        h0_lst // Do we have a 'list_once' operator that could be applied here?  It might be nice.
                        
// Functions have no specific ordering for their typevars so the presence of vars just indicates a generic abstract function that must be bound arg-by-arg and on return type.  If all bindings become grounded the typevars could be deleted from the resultant to avoid further re-evaluation, but this step can be done in type_eval.

                    // | (x, x') when (not flip) -> this needs throwing to outer level : TODO
                        
                    | (x, x') ->
                        vprintln 0 (sprintf "cannot typecheck:\n   X-L=%s\n   X-R=%s" (bsv_tnToStr lt) (bsv_tnToStr rt)) // TODO do not always print this since we are used for overload disambiguation now ...
                        //
                        [] // sprintf ": impossible type mismatch on form xl=%A xr=%A" xl xr)) //(bsv_tnToStr xl) (bsv_tnToStr xr)))

            if nullp cc_lst then [] // rough test since existing entries present? TODO delete me
            else
            let m1 = mf() + " type args to " + htosp idl_l.nomid
            if isfunc (*&& length args_l <> length args_r *)then // varadic problem?
                // This work is already done for functions ... owing to the tyargs to the nominal wrapper not being present when defined concretely.
                (if length cc_lst = 1 then [(hd cc_lst, lt)] else muddy "TODO isfunc ccl_lst")
                elif length args_l <> length args_r then []
                else // all other forms do it this way.
                    arity_check_hard "non-func" true mf idl_l.nomid args_l args_r
                    let ccl = pairserfpram stack cc (List.zip args_l args_r)
                    map (fun cc-> (cc, lt)) ccl

        | (Bsv_ty_id(nf_l, prec_l, (id_l, bi_l)), Bsv_ty_id(nf_r, prec_r, (id_r, bi_r))) -> // Two type id's unified. Use passed in bindindex? We fail here. TODO.
            let nf = vp_resolve(nf_l, nf_r)
            let cc =
                let keep_distinct = false
                if id_l = id_r && not keep_distinct then
                    let (it, remnants) = tcc_dig cc [id_l]
                    match it with
                        | None ->
                            if avd >=4 then vprintln 4 (sprintf "hindley: bind to None %s" (id_l))
                            (None, [([id_l], fresh_bindindex "LFB1396")], nf)::cc 
                        | Some(ty, names, nf_) -> cc // Nothing new to add. Could check nf consistency though.
                else
                    //if vd>=4 then vprintln 4 (sprintf "hindley: need resolve of ids  %s/%A cf %s/%A" id_l prec_l id_r prec_r)
                    //let (idd_l, idd_r) = ((([id_l], bi_l), nf_l), (([id_r], bi_r), nf_r))
                    let (idd_l, idd_r) = (([id_l], fresh_bindindex "bi_l"), ([id_r], fresh_bindindex "bi_r"))
                    
                    if keep_distinct then
                        sf "Not used"
#if SPARE                        
                        let (it_l, remnants) = tcc_dig_nf_basis cc [id_l] // idd_l 
                        let (it_r, remnants) = tcc_dig_nf_basis remnants [id_r] // idd_r  

                        let ll = match it_l with
                                    | None           -> (None, [idd_l])
                                    | Some(ax, idsx) -> (ax, idd_l::idsx) 
                        let rr = match it_r with
                                    | None           -> (None, [idd_r])
                                    | Some(ax, idsx) -> (ax, idd_r::idsx) 
                        ll :: rr :: remnants
#endif
                    else
                    let (it_l, remnants) = tcc_dig cc [id_l] 
                    let (it_r, remnants) = tcc_dig remnants [id_r]                
                    match (it_l, it_r) with
                        | (None, None) ->
                            let d = leftprec "L1462" (prec_l, prec_r)
                            vprintln 3 (sprintf "Giving precchox L1462 lprec=%A for %s/%A cf %s/%A" d id_l prec_l id_r prec_r)
                            let aa = if (d) then singly_add idd_l [idd_r] else singly_add idd_r [idd_l] // Both are kept but maintain defs first in list. Why?
                            let vp = if (d) then nf_l else nf_r // Or else use a simple disjunction of numeric property (with warn if not both present?)
                            (None, aa, vp)::cc 
                        | (Some(ax, idsx, vp), None) ->
                            //let _:gammate_t = (ax, idd_r::idsx, vp)
                            (ax, idd_r::idsx, vp)::remnants
                        | (None, Some(ax, idsx, vp)) -> (ax, idd_l::idsx, vp)::remnants

                        | (Some(ax, ids1, vp1), Some(None, ids2, vp2))
                        | (Some(None, ids1, vp1), Some(ax, ids2, vp2))      ->
                            if avd >=4 then vprintln 4 (sprintf "hindley: Combine 1/2 ids %s and %s" (sfold (fst>>hptos) ids1) (sfold (fst>>hptos) ids2))
                            
                            let d = leftprec "L1476" (prec_l, prec_r)
                            vprintln 3 (sprintf "Giving precchox L1476 lprec=%A for %s/%A cf %s/%A" d id_l prec_l id_r prec_r)
                            let vp = if (d) then nf_l else nf_r // Or else use a simple disjunction with warn
                            (ax, lst_union ids1 ids2, vp)::remnants

                        | (Some(Some ax1, ids1, vp1), Some(Some ax2, ids2, vp2)) ->
                            //dev_println (sprintf "hindley: Recursive call to meet the bindings of id pair %s/%A cf %s/%A" id_l prec_l id_r prec_r)
                            let d = leftprec "L1482" (prec_l, prec_r)
                            vprintln 3 (sprintf "Giving precchox L1483 lprec=%A for %s/%A cf %s/%A" d id_l prec_l id_r prec_r)
                            let vp = if (d) then vp1 else vp2 // Or else use a simple disjunction with warn

                            match  pairserf stack false remnants (ax1, ax2) with // Recursive call to meet the bindings 
                                | [(ccf, ax)] ->
                                    if avd >=4 then vprintln 4 (sprintf "hindley: Combine 2/2 ids %s and %s" (sfold (fst>>hptos) ids1) (sfold (fst>>hptos) ids2))
                                    (Some ax, lst_union ids1 ids2, vp)::ccf
                                | [] -> sf "hindley: nowhere"
                                | _  -> sf "hindley: multiplies"
            //vprintln 0 (sprintf "Resolved id pair  %s/%A cf %s/%A" id_l prec_l id_r prec_r)  
            [(cc, Bsv_ty_id(nf, prec_l, (id_l, bi_l)))]  // Owing to overloading or other non-determinism, we can return a list of possible answers. Each answer is a pair of the unified hindley and the type resolved.  
            
        | (rx, Bsv_ty_id(nf, prec, (id, bid_)))
        | (Bsv_ty_id(nf, prec, (id, bid_)), rx) ->
            let idd = ([id], fresh_bindindex "-L1458")
            let (it, remnants) = tcc_dig cc [id]
            match it with
                | None ->
                    if avd>=4 then vprintln 4 (sprintf "hindley: Freshly enter " + id)
                    //if id = "prct" then dev_println (sprintf "STOSH hit it prct  bin=>%s<" (bidToStr (snd idd)))
                    [((Some rx, [idd], nf)::cc, rx)]  // Note: rx will never be a sum_id, ie never be a type name. Any names are in the second field.

                | Some(Some ax, idsx, nfx) -> // Has a type already - need further resolve.
                    if avd>=4 then vprintln 4 (sprintf "hindley: Augment hindley entry 1/2 " + id)
                    let lst = pairserf stack false remnants (ax, rx) // So there may be a problem here with recursive types since idd is now out of scope!
                    let vp_final = vp_resolve (nfx, nf)
                    let dij (cc2, rx_final) = ((Some rx_final, singly_add idd idsx, vp_final)::cc2, rx_final)
                    map dij lst

                | Some(None, idsx, nfx) ->
                    if avd>=4 then vprintln 4 (sprintf "hindley: Entries exist for this id but no type yet. id=" + id)
                    let vp_final = vp_resolve (nfx, nf)
                    [((Some rx, singly_add idd idsx, nf)::remnants, rx)]

                    

        | (Bsv_ty_int_vp _, _)
        | (_, Bsv_ty_int_vp _)

        | (Bsv_ty_intexpi _, _)
        | (_, Bsv_ty_intexpi _)
        
        | (Bsv_ty_intconst _, _)
        | (_, Bsv_ty_intconst _)        

        | (Bsv_ty_integer _, _)
        | (Bsv_ty_primbits _, _)

        | (_, Bsv_ty_integer _)
        | (_, Bsv_ty_primbits _)         -> numeric_serf cc (lt, rt)

        | (_, Bsv_ty_format) -> [(cc, rt)]
        | (Bsv_ty_format, _) -> [(cc, lt)]        

        | (Bsv_ty_fsm_stmt _, Bsv_ty_fsm_stmt _) -> [(cc, lt)]
        
        //The flag flip notes the swapped over args for a commutative 2nd attempt
        | (lt, rt) when not flip -> pairserf stack true cc (rt, lt)
        
        | (lt, rt) ->
            // The parent should instead abend after it has tried all options.
            //cleanexit(mf() + sprintf ": Type error: cannot use these two types together: \n   lt=%s\n   rt=%s\nL=%A\nR=%A" (bsv_tnToStr lt) (bsv_tnToStr rt) lt rt)
            []

    //and newserf cc stack (ll, rr) = fst(pairserf stack cc (ll, rr))

    and numeric_serf cc (lt, rt) = 
        //vprintln 0 "NUMERIC SERF START"
        let rec ntrank = function
            | Bsv_ty_nom(_, F_enum _, _) -> 60
            | Bsv_ty_primbits _          -> 50
            | Bsv_ty_nom(_, F_ntype, _)  -> 45
            | Bsv_ty_intexpi _           -> 40
            | Bsv_ty_intconst _          -> 40            
            | Bsv_ty_integer             -> 20
            | Bsv_ty_int_vp(VP_none _)   -> 20
            | Bsv_ty_int_vp(VP_value(b, sl)) when nullp sl -> 20 + (if b then 2 else 0)
            | Bsv_ty_int_vp(VP_value(b, sl)) (* otherwise *) ->
                //vprintln 0 ("otherwisers VP_value sl="  + sfold (fun x->x) sl + sprintf " lt=%s cf rt=%s" (bsv_tnToStr lt)  (bsv_tnToStr rt))
                22 + (if b then 1 else 0)
            | Bsv_ty_int_vp(VP_type) -> 20 

            | Bsv_ty_stars(_, ct) -> ntrank ct - 1
            | Bsv_ty_dontcare _    -> 1
            | dregs ->
                vprintln 0 (sprintf " treating as numeric dregs %s" (bsv_tnToStr dregs))
                -1

        match (lt, rt) with
        | (Bsv_ty_intconst _,  Bsv_ty_intconst _)
        | (Bsv_ty_intconst _,  Bsv_ty_intexpi _)
        | (Bsv_ty_intexpi _, Bsv_ty_intconst _)
        | (Bsv_ty_intexpi _, Bsv_ty_intexpi _) ->
            let vl = mi_dehexp lt
            let vr = mi_dehexp rt
            let q = xi_deqd(vl, vr)
            if bconstantp q then
                match xbmonkey q with
                    | Some true -> ()
                    | _ ->
                        // We need to mark this as an integer value not an integer type... Just returning Integer here will give alternate results depening on an odd or even number of unifications...
                        vprintln 3 (mf() + sprintf ":numeric constant types differ in value (%s cf %s)" (xToStr vl) (xToStr vr))
                [(cc, rt)]
            else
                vprintln 0 (mf() + sprintf ": Type error: cannot use these two numeric types together: \n   lt=%s\n   rt=%s" (bsv_tnToStr lt) (bsv_tnToStr rt))
                muddy (mf() + sprintf ": numeric constant types were not constant!    %s cf %s" (xToStr vl) (xToStr vr))

        | (Bsv_ty_primbits s_l, Bsv_ty_primbits s_r) ->
            let res = function // a signed resolution function
                | (Some a, Some b) when a=b -> Some a
                | _ -> None
            [(cc, Bsv_ty_primbits { branding=s_l.branding; signed=res(s_l.signed, s_r.signed); width = max s_l.width s_r.width; })]


        | (lt, rt) ->
             let (lr, rr) = (ntrank lt, ntrank rt)
             //vprintln 0 (sprintf "NUMERIC SERF %i %i" lr rr)
             if lr < 0 || rr < 0 then
                 vprintln 0 (mf() + sprintf ": Type error: cannot use these two numeric types together: \n   lt=%s\n   rt=%s\nL=%A\nR=%A" (bsv_tnToStr lt) (bsv_tnToStr rt) lr rr)
                 []
             elif lr>rr then [(cc, lt)] else [(cc, rt)]                                                 

    let x0 = pairserf [] false cc (ll_top, rr_top)
    x0 // End of sum_type_meet_serf


let dolog log m ans = mutadd log (m, ans)
    
let newsum_arg_base ww (bobj:bsvc_free_vars_t) (mf:unit->string) ee m_arglog h0 (ll, rr) =
    let (h, ans) = sum_type_meet ww bobj mf h0 (ll, rr) 
    let vd = bobj.afreshen_loglevel
    if vd>=4 then
        vprintln 4 (mf() + ": newsum_arg Logging newsum type resolution ll=" + bsv_tnToStr ll)
        vprintln 4 (mf() + ": newsum_arg Logging newsum type resolution rr=" + bsv_tnToStr rr)
        vprintln 4 (mf() + ": newsum_arg Logging newsum type resolution " + bsv_tnToStr ans)
        vprintln 4 (mf() + sprintf ": h now is %A " (sfold hindToStr h))
    let mm = mf()
    match ans with
        | Bsv_ty_nom(ats, cat, binds) ->
            //vprintln 0 (mf()  + "  " + bsv_tnToStr ans)
            dolog m_arglog mm ans
            (h, ans)

        | Bsv_ty_id(nf, prec, id) -> // TODO explain this please
            dolog m_arglog mm ans
            //vprintln 0 (sprintf "whack out '%s' for %A" id (sfold hindToStr tb.h))
            (h, ans)
        | ans ->
            dolog m_arglog mm ans
            (h, ans)




let make_fidder idl = function
    | Bsv_ty_id(_, _, (fid, _)) -> fid    
    | other ->
        vprintln 0 (sprintf " ++++ make_fid for %s : other form %A " (htosp idl) (bsv_tnToStr other))
        "$PFID" // TODO please explain this!

let get_fid2 (n, a) =
    match a with
    | Bsv_ty_id(_, prec, (fid, _)) -> fid
    | other -> "$formal_" + i2s n


// Select a suitable type for an overloaded tag name
let tag_type_checker ww bobj lp mm vtag ty possible_types =
    let possible_types = map fst possible_types
    //vprintln 3 (sprintf "Check type for tag %s" vtag)
    let ans = 
        let nominate cc ty =
            match ty_untie ty with
                | Bsv_ty_nom(ia, _, _) -> ia.nomid :: cc
                | _ -> cc

        let simple_l = nominate [] ty
        let simple_r = List.fold nominate [] possible_types
        if not_nullp simple_l && memberp (hd simple_l) simple_r then ty
        else
            let ww = WN "tag_type_checker" ww
            let ty_filter_pred arg = 
                //dev_println (sprintf "For tag %s need to type meet %A with %A" vtag arg ty)
                let ans = sum_type_meet_serf ww (bobj:bsvc_free_vars_t) mm [] (arg, ty)
                let ok = (length ans = 1)
                vprintln 3 (sprintf "Tag type check: compare %s with %s: ok=%A ans=%A" (bsv_short_tnToStr arg) (bsv_short_tnToStr ty) ok ans)
                ok // We here assume ans is the same as ty but an alternative is that meet has refined the type.
            let ans = List.filter ty_filter_pred possible_types
            if length ans = 1 then hd ans
            else
                cleanexit (lpToStr0 lp + sprintf ": Cannot resolve type of ctor tag %A out of %s. ty=%s" vtag (sfold bsv_short_tnToStr possible_types) (bsv_short_tnToStr ty))
    vprintln 3 (sprintf "Check type for tag %s, found as %s." vtag (bsv_short_tnToStr ans))
    ans



let get_fidder = function
    | (_, ast_ty, "$HOF") ->
        match ast_ty with
            | Some(KB_functionProto(return_type, name, formals, provisos, lp)) -> name
            | other -> sf(sprintf "fid hof other form %A" ast_ty)
    | (_, _, fid) -> fid    


//
//   
let proviso_parse ee c = function
    // TODO KB_proviso("Bits",[KB_typeIde ("element_type1",[]); KB_typeIde ("width_any",[])]) ignored
    | KB_proviso(id, args) ->
        // These are not currently checked so we do not even parse them here.
        //let _ =  vprintln 0 (sprintf "proviso_parse id=%s found %A" id args)
        c // for now
    | other -> sf (sprintf "another form of proviso found %A" other)



let unify_local ww bobj mm ee (cc:hindley_t) pairs =
    let rec qq cc sofar = function
        | [] -> (cc, sofar)
        //| ((, fid), tr)::tt    -> qq cc ((Some tr, fid)::sofar) tt
        | ((tl, fid), tr)::tt -> 
            let (cc, ans) = sum_type_meet ww bobj mm cc (tl, tr)
            //vprintln 0 (sprintf "            doung que an ans=%A" ans)        
            qq  cc ((ans, fid)::sofar) tt
    //vprintln 0 (sprintf " now decently doing que pairs=%A" pairs)        
    let (cc, ans) = qq cc [] pairs
    (cc, rev ans)




type freshener_t =
   |  Fresher_id of valueprop_t * string * dbi_t
   |  Fresher_ty of bsv_type_t

type freshmap_t = Map<string, freshener_t>



let bsv_fToStr = function
    | Fresher_ty ty -> bsv_tnToStr ty
    | Fresher_id (a, fid, dbi) -> fid





// Predicate on whether we have a fresh parameter or a binding ... need to look for prec code P_free TODO
        

//
// afreshen_sty: Copy out a summary type giving a fresh identifier for each type var.
//
//       
// The kickoff denotes that some of the formals in the signature have concrete types or specific names being passed in (eg from constant types or tyvars bound at an outer level) and can give prefered formal names.

// The freshlist should be all the free typevars from any depth. These are those that want a rename mapping. When do these occur please - e.g. in polymorphic functions whose types do not appear in the top signature ? Concrete example needed.
       
let afreshen_sty ww bobj site kickoff_o arg  =
    let vd = bobj.afreshen_loglevel
    let freshtok = funique "FTOK"
    //if freshtok = "FTOK150" then sf (site + " hit it")
    let kickoff = valOf_or_nil kickoff_o
    let m_kickoff_msg = ref []
    let make_initial_freshmap (freshmap:freshmap_t) (fid, v) =
        if vd>=4 then mutadd m_kickoff_msg (sprintf "   afreshen kickoff entry: %s as %s" fid (bsv_fToStr v))
        freshmap.Add(fid, v)
    let freshmap0 = List.fold make_initial_freshmap Map.empty kickoff

    let startmsg =
        if vd>=4 then
            sprintf "token=FT-%s site=%s arg=%s kickoff(^%i)=%s" freshtok site (bsv_tnToStr arg) (length kickoff) (sfoldcr_lite id !m_kickoff_msg) 
        else "start FT-" + freshtok
    let ww = WF 3 "afreshen_sty" ww startmsg

    let freshlist = get_freshlist ww arg []
    if vd>=5 then vprintln 5 ("wh2: " + whereAmI_concise ww)

    //if freshtok = "FTOK122" then             g_hl_call_trace := true // swold
    let ww = WF 3 "afreshen_sty" ww (sprintf "token=FT-%s arg=%s frees=%s" freshtok (bsv_tnToStr arg) (sfold snd freshlist))
    let m_no = ref 1
    let tag() =
        mutinc m_no 1
        match kickoff_o with
            | None    -> "$f" + i2s !m_no + "_"
            | Some ko -> "$t" + i2s !m_no + "_"
     
    let ren idl = idl // We do not rename the types - it would make nominal matching fail - it is however criticial that we alpha convert (freshen) all the free tyvars.
    let knotren s = freshtok :: "knotren" :: s // squirrel?
    
    let rec assoc (freshmap:freshmap_t) arg =
        match arg with
            | Tty(Bsv_ty_id(nf_, (*prec*)_, (id, _)), _) // Interchangable denotations of unbound type identifier!
            | Tid(nf_, id) ->
                match freshmap.TryFind id with
                    | Some(Fresher_id(nf, x, dbi))  -> Tid(nf, x)  // 
                    | Some(Fresher_ty ty)      -> Tty(ty, id)
                    | None -> sf("No binding for afreshen of type var (1) " + id)
            | Tty(ty, s) -> Tty(afresh freshmap [] ty, s) // Should be able to use ty_eval for this - please do ... TODO
    
    and slfun_no numberf freshmap sl = // Add further unbound items to the map with new aliases - return the new list.
        let biz id = funique(id + "_$BIZ") // Or could use a Bruijn dbi ...
        let map_with_newname (freshmap:freshmap_t, dbi) = function
            | Tty(Bsv_ty_id(nf, (*prec*)_, (fid, _)), _)             // Interchangable denotations of unbound type identifier!
            | Tid(nf, fid) ->
                //if numberf then vprintln 0 (sprintf "slfun %s perhaps whack2 %A" fid nf)
                match freshmap.TryFind fid with
                    | None ->
                        let id1 = fid + tag()
                        let nv = Tid(nf, id1) 
                        (freshmap.Add(fid, Fresher_id(nf, biz fid, dbi)), dbi+1) // A new fresh identifier.
                    | Some x ->
                        //vprintln 0 (sprintf "freshmap '%s' already bound (by kickoff perhaps) as %s " fid (bsv_fToStr x))
                        (freshmap, dbi)
            | Tty(ty, _) -> (freshmap, dbi)  // what if free ty vars in here - well they should be listed at parent level but we still need to recurse in here in assoc
            
        let (freshmap, _) = List.fold map_with_newname (freshmap, 200) sl
        (map (assoc freshmap) sl, freshmap) 

    and slfun = slfun_no false 


    and afresh (freshmap:freshmap_t) stack arg = // Just a variant of ty_eval please.
        if vd>=8 then vprintln 8 (sprintf "afresh start of %s" (bsv_tnToStr arg))
        match arg with
        | arg when leaf_grounded_type_pred arg -> arg
        | Bsv_ty_stars(ss, ct) -> Bsv_ty_stars(ss, afresh freshmap stack ct)
        | Bsv_ty_list(id, items) ->
            let items' = map (afresh freshmap stack) items
            gen_Bsv_ty_list items

        // A knot needs replacing with a fresh one whether tied or untied. 
        | Bsv_ty_knot(idl, x) -> 
            let key = knotren idl
            match op_assoc key (stack) with
                | Some (newknot) -> newknot
                | None when nonep !x -> // Copying out an untied structure with knots.
                    if vd>=4 then vprintln 4 ("afresh: untied: adding a knot on knots stack for " + htosp idl) // FAULT STOP
                    let ka = ref None
                    let newknot = Bsv_ty_knot(idl, ka)
                    newknot
                | None -> // Copying out a tied structure with knots.
                    if vd>=5 then vprintln 5 ("afresh: tied: addding a knot on knots stack for " + htosp idl)
                    let ka = ref None
                    let newknot = Bsv_ty_knot(idl, ka)
                    let ans = afresh freshmap ((key, newknot)::stack) (valOf !x) // NEEDS NEW TIE
                    ka := Some ans
                    newknot
            
        | Bsv_ty_sum_knott(idl, x) ->
            let rec noggle = function
                | GE_normthunk(instf, idl, ans, reaped, _, ff) when not(nonep(op_assoc (ren idl) (stack))) -> muddy "wide open - please remove this code and use the other knots or replicate that functionality here"
                | GE_normthunk(instf, idl, ans, reaped, _, ff) when nonep(!ans) ->
                    sf ("stale knot ??" + htosp idl)
                    Bsv_ty_sum_knott(idl, x) // does this point to stale loop?  
                | GE_normthunk(instf, idl, ans, reaped, _, ff) -> noggle (valOf !ans)
                    //afresh freshmap (valOf !ans)
                | GE_binding(bound_name, _, EE_typef(_, sty)) -> afresh freshmap stack sty
                | other -> sf(sprintf "noggle wiondod %A" other)
            noggle !x

        | Bsv_ty_id(_, prec, (id, bid)) ->
            match freshmap.TryFind id with
                | Some(Fresher_id(nf, id, dbi_))   -> Bsv_ty_id(nf, Some P_decl0, (id, bid))
                | Some(Fresher_ty sty)       -> sty
                | None -> sf("No binding for afreshen of type var (2) " + id)

        | Bsv_ty_nom(ats, guts, flst) ->
            let key = kren flst ats.nomid // We squirrel in case of mutually recursion with same nominal type using different parameters.
            match op_assoc key stack with
                | Some loop ->
                    if vd>=4 then vprintln 4 ("afreshen: serving recursive type from freshen stack " + htosp ats.nomid)
                    loop
                | None ->
                    let ka = ref None
                    let stack = (key, Bsv_ty_knot(ats.nomid, ka))::stack // in case needed
                    let ans =
                        match Bsv_ty_nom(ats, guts, flst) with
                            | Bsv_ty_nom(idl, F_action, []) -> Bsv_ty_nom(ren idl, F_action, [])        

                            | Bsv_ty_nom(idl, F_actionValue ty, sl) ->
                               let (sl', freshmap) = slfun freshmap sl
                               Bsv_ty_nom(ren idl, F_actionValue(afresh freshmap stack ty), sl')

                            | Bsv_ty_nom(idl, F_fun(rt, mats, args), sl) ->
                                if vd>=8 then vprintln 8 (sprintf "Function %s start hydrated/freshened:\n  IN=%A\n  " mats.methodname (bsv_tnToStr arg))
                                if vd>=8 then vprintln 8 (sprintf "Function %s start hydrated/freshened:\n  IN_BG=%A\n  " mats.methodname (sfold (fun (a,b,c)->droppingToStr c) mats.body_gamma))
                                let (sl', freshmap) = slfun freshmap sl
                                let soxdeb arg =
                                    if vd>=4 then vprintln 4 (mats.methodname + sprintf ": afresh drop of %s" (bsv_tnToStr arg))
                                    afresh freshmap stack arg
                                let afresh_bg(id, cp, drop) =
                                    if vd>=4 then vprintln 4 ("  nnn  " + id + " " + cpToStr cp)
                                    (id, cp, dropapp (soxdeb) drop)

                                let mats = { mats with protocol= protoapp (afresh freshmap stack) mats.protocol; body_gamma=map afresh_bg mats.body_gamma }
                                let rt' = afresh freshmap stack rt
                                if vd>=4 then vprintln 4 (sprintf " afreshen rt -> rt'   :  %s ---> %s " (bsv_tnToStr rt) (bsv_tnToStr rt'))
                                let ans = Bsv_ty_nom(idl, F_fun(rt', mats, map (afresh freshmap stack) args), sl')
                                //if vd>=5 then vprintln 5 (sprintf "Function %s was hydrated/freshened:\n  IN=%A\n  OUT=%A" mats.methodname arg ans)
                                ans


                            | Bsv_ty_nom(idl, F_ifc_1 ii, sl) ->            
                                let (sl', freshmap) = slfun freshmap sl
                                let xi f = afresh freshmap stack f
                                //if vd>=5 then vprintln 5 (sprintf "afreshen: token=FT-%s F_ifc_1 sl'=%s" freshtok (sfold tidToStr sl'))
                                let ans = Bsv_ty_nom(ren idl, F_ifc_1 { ii with iparts= map xi ii.iparts}, sl')
                                //if vd>=5 then vprintln 5 (sprintf "afreshen: token=FT-%s F_ifc_1  ans=%s" freshtok (bsv_tnToStr ans))
                                ans

                            | Bsv_ty_nom(idl, F_subif(iname, f), sl) ->            
                                let (sl', freshmap) = slfun freshmap sl
                                let xi f = afresh freshmap stack f
                                let ans = Bsv_ty_nom(ren idl, F_subif(iname, xi f), sl')
                                //if vd>=5 then vprintln 5 (sprintf "afreshen: token=FT-%s F_subif  ans=%s" freshtok (bsv_tnToStr ans))
                                ans

                            | Bsv_ty_nom(idl, F_tunion(mrecflag, variants, details_), sl) ->
                                let (sl', freshmap) = slfun freshmap sl
                                let xxo = function
                                   | (id, None) -> (id, None)
                                   | (id, Some ty) -> (id, Some(afresh freshmap stack ty))

                                let ans = Bsv_ty_nom(ren idl, F_tunion(mrecflag, map xxo variants, None), sl')
                                //if vd>=5 then vprintln 5 (sprintf "afreshen: token=FT-%s F_tunion  ans=%s" freshtok (bsv_tnToStr ans))
                                ans
                                
                            | Bsv_ty_nom(idl, F_enum(miu, members), sl) -> Bsv_ty_nom(idl, F_enum(miu, members), sl)

                            | Bsv_ty_nom(idl, F_struct(fields, details_), sl) ->            
                                let (sl', freshmap) = slfun freshmap sl
                                let xx (id, ty) = (id, afresh freshmap stack ty)
                                let ans = Bsv_ty_nom(ren idl, F_struct(map xx fields, None), sl')
                                //if vd>=5 then vprintln 5 (sprintf "afreshen: token=FT-%s F_struct  ans=%s" freshtok (bsv_tnToStr ans))
                                ans

                            | Bsv_ty_nom(ats, F_monad, sl) ->            
                               let (sl', freshmap_) = slfun freshmap sl
                               Bsv_ty_nom(ren ats, F_monad, sl')

                            | Bsv_ty_nom(ats, F_ntype, sl) ->            
                               let (sl', freshmap_) = slfun_no true freshmap sl
                               let silly_mogrif = function
                                   | Tty(ty, fid) ->  ty
                                   | Tid(nf, fid) -> Bsv_ty_id(nf, None, (fid, (*fresh_bindindex*) "-2"))
                                   //sf(fid + ": afreshen silly_mogrif idl=" + htosp ats.nomid)
                               let sl2 = map silly_mogrif sl'
                               hydrate_type_builtins ww (fun()->"afreshen") (hd ats.nomid) sl2 (Bsv_ty_nom(ren ats, F_ntype, sl'))

                            | Bsv_ty_nom(idl, F_tyfun x, sl) ->            
                               let (sl', freshmap_) = slfun freshmap sl
                               Bsv_ty_nom(ren idl, F_tyfun x, sl')

                            | Bsv_ty_nom(idl, F_vector, sl) ->            
                               let (sl', freshmap_) = slfun freshmap sl
                               Bsv_ty_nom(ren idl, F_vector, sl')

                            | sty -> sf ("afreshen_sty: other nominal form " + bsv_tnToStr sty + "\n" + sprintf "%A" sty)
                    ka := Some ans
                    if vd>=5 then vprintln 5 (sprintf "afreshen: token=FT-%s reiterate ans=%s" freshtok (bsv_tnToStr ans))
                    ans
                    
        | sty -> sf ("afreshen_sty: other type form " + bsv_tnToStr sty + "\n" + sprintf "%A" sty)


    let buz id = id + freshtok
    let map_with_newname (freshmap:freshmap_t, dbi) (nf, id) =
        match freshmap.TryFind id with
            | None ->
                let id1 = id + tag()
                let nv = Tid(nf, id1)
                let bb = buz id
                if vd>=4 then vprintln 4 (sprintf "Freshen: token=%s: new mapping: %s -> %s" freshtok id bb)
                (freshmap.Add(id, Fresher_id(nf, bb, dbi)), dbi+1)
            | Some x -> // What about nest masking?  Don't want to overwrite those in the kickoff but do perhaps want inner masking...
                if vd>=7 then vprintln 7 ( sprintf "Freshen: token=%s: Var '%s' already bound (by kickoff perhaps) as %s" freshtok id (bsv_fToStr x))
                (freshmap, dbi)
    let (freshmap1, _) = List.fold map_with_newname (freshmap0, 300) freshlist
    let ans = afresh freshmap1 [] arg
    if vd>=4 then vprintln 4 (sprintf "afreshen_sty token=FT-%s. end. Final ans=%s" freshtok (bsv_tnToStr ans))
    ans // end of afreshen_sty 



let gen_VP_value = function
    | ""  -> VP_value(false, [])
    | fid -> VP_value(false, [fid])

let toVP so fid =
    match so with
    | SO_numeric -> VP_value(true, (if fid = "" then [] else [fid]))
    | SO_type    -> VP_type
    | _          -> gen_VP_value fid

let toSTP = function
    | (SO_numeric, _ , fid)    -> Tid(VP_value(true, [fid]),  fid)
    | (SO_type, _, fid)        -> Tid(VP_type, fid)
    | (SO_none _,       _, fid)-> Tid(gen_VP_value fid, fid)                
    | other -> muddy (sprintf "other need STP %A" other)                


  
//
// collect_sum1: convert an ast type expression to a summary type (our main internal type representation).
// Where the type contains instances of other types that are abstract, these are freshened and hydrated in the current context.
//
let collect_sum1 ww bobj (ee:ee_t) mf pty_o vp_top ast_ty =
    let m0 = "collect_sum1 (typecheck):"
    let vd = bobj.afreshen_loglevel
    let ww = WF 3 m0 ww (if vd>=4 then mf() else "start")
    //vprintln 0 (sprintf "collect_sum1 vp_top=%A  ast_ty=%A" vp_top ast_ty)
    let revertmakesum1 arg cc =
        match arg with 
        | Fresher_id(nf, fid, dbi) -> Tid(nf, fid)::cc
        | Fresher_ty (Bsv_ty_id (nf, Some P_free, (fid, _))) -> Tid(nf, fid) :: cc//TODO need nicer routine ... DO ONLY IF P_free please 
        | Fresher_ty (Bsv_ty_id (nf, other, (fid, _))) ->
            //vprintln 0 (mf() + sprintf ": +++ YUCK: reverted non free var to Tid %s" fid)
            Tid(nf, fid) :: cc//TODO need nicer routine ... DO ONLY IF P_free please 
        | Fresher_ty other ->
            //vprintln 0 (sprintf "+++ revertsum1 discard %A" other)
            cc //Tty(other, fid)
        //| other -> sf(sprintf "revertsum1 other %A" other)


    let rec hydrate_named_type ww tb h0 typen use_prams = function
        | GE_normthunk(instf, idl, ans, reaped, _, ff) when not(nonep !ans) -> hydrate_named_type ww tb h0 typen use_prams (valOf !ans)

        | GE_normthunk(instf, idl, ans, reaped, being_done, ff) when not(nonep !being_done) ->
            match valOf !being_done with
                | Bsv_ty_knot(idl, ty_knt) -> Bsv_ty_knot(idl, ty_knt)
                | other -> other // may want to keep the knot here to spot recursive types more easily?
                //sf (sprintf "other L1706 %A" other)
        | (GE_normthunk(instf, idl, ans, reaped, being_done, ff)) as self ->
            let mf() = "resolve forward reference (normthunk): " + htosp idl
            let ww = WN (mf()) ww
            let (ge_knt, ty_knt) = (ref self, ref None)
            if not (nonep !being_done) then sf (mf() + " recursion being_done L1343")           

            being_done := Some(Bsv_ty_knot(idl, ty_knt)) // Does not matter if being done recursively with different bindings later on because the abstract type is freshened on each use site, including the knot.
            //vprintln 0 (mf() + " starting call")
            let a1 = ff ww ee // { ee with genenv=ee.genenv.Remover [ hd idl]; } // It is in there as a shortname - copy this elsewhere?
            being_done := None            
            //vprintln 0 (mf() + " call done and reaped")                    
            reaped := a1
            let kater hh = function
                | Aug_env(tyclass, bound_name, (dmtof, who, vale))->
                    ans := Some(GE_binding(bound_name, who, vale)) // TODO check k=hd idl! and check only one
                    ge_knt := valOf !ans
                    let _ =
                        match vale with
                            | EE_typef(_, sty) -> ty_knt := Some sty
                            | other -> sf(sprintf " knot tie result other form %A" other) 
                    hh
                | Aug_tags newkeys ->                     
                    vprintln 0 ("+++ temporarily adding tags " + sfold (fst) newkeys)
                    let ty_keys_augment (c:hindley_t) (s, (st, na)) = tcc_add_tag c (VP_none "L1341") (s, st) // NOT CALLED?
                    hh

                | tt ->
                    sf (sprintf "kater other '%s' %A" (htosp idl) tt)
                    hh
            let tb = List.fold kater (*augment_env_entry ww m*) tb a1 // put in tb rather than ee
            hydrate_named_type ww tb h0 typen use_prams (valOf !ans)

        | GE_binding(bound_name, binfo, EE_typef(gf, sty)) ->
            let m0 = "hydrate_named_type typeAbs  " + htosp bound_name
            let ww = WN m0 ww

            if vd>=4 then vprintln 4 (sprintf "collect_sum1: the item retrieved under the name %s for freshening was %s" (htosp bound_name) (bsv_tnToStr sty))
            if binfo.synonymf then  // division of work between caller and callee is not made clear. who is to call afresh_wrapper?
                if vd>=5 then vprintln 5 ("wh1: " + whereAmI_concise ww)
                if vd>=4 then vprintln 4 (sprintf "Invoke synonym freshen bypass on %s" (hptos bound_name))
                sty
            else
                afresh_wrapper ww tb h0 use_prams (gf, sty)

        | other -> sf (sprintf "hydrate_named_type '%s' up tt other form  %A" typen other)
// Is this comment out of place:
// Rather complex inside a synonym:  typedef Reg#(element_t) Wire#(type element_t); 
//                               or  typedef Foo#(e_t, Bit#(4), e_t) Bar#(type e_t);                   
// Like C/C++ typedefs the entity being defined is the last listed.
//
// The second call to afreshen_sty in a typedef (ie the defining/formal one) can either be declaring a new free typevar or substituting a concrete type (or outer-bound tyvar) and this will effect whether the new tyvar will appear in the return prams. That's why we have two forms Fresher_ty and and Fresher_id. ... but really we need post-wrap it with and not collate via cc ... seems far more logical  ... so 
    and gen_afresher_kickoff (hh:hindley_t, sofar) = function
        | (Tty(Bsv_ty_id(numericf, prec, (f, bid)), fid_), Fresher_id(nf, id, dbi_)) -> // binding to self somehow ... example please - same name in ifc def as decl.
            let nv = Bsv_ty_id(nf, prec, (id, bid)) // TODO disjunction of nf/numericf?
            if vd>=4 then vprintln 4 (sprintf "gen_afresher_kickoff: 1/2 bind to self for '%s' please explain why this is right using a test case where the same formal name is used for the same thing at two levels in the design hireachy" f)
            let np = (f, nv)  // Do need to keep same number of pairs
            (hh, np::sofar) // Why does this happen and does it need to be fresh? TODO

        | (Tty(ty, fid_), Fresher_id(nf, id, dbi_)) ->
            if vd>=4 then vprintln 4 (sprintf "gen_afresher_kickoff: qbind %s fid=%s to '%s' " (bsv_tnToStr ty) fid_ id)
            let nv = Bsv_ty_id(nf, None, (id, (*fresh_bindindex*) "-3")) // more of an anti-P_free since it is bound ... perhaps want explicit precedence here.
            let np = (fid_, nv)
            (hh, np::sofar)
        
        | (Tid(numericf, f), Fresher_ty((Bsv_ty_nom _) as nv)) -> // TODO do we need to recurse on freshen here if this nominal has its own free vars ...
            // An example is: typedef Foo3#(Baz1#(f_t), Bit#(4), f_t) Bar#(type f_t);
            if vd>=4 then vprintln 4 (sprintf "gen_afresher_kickoff: bind '%s' to nominal %s" f (bsv_tnToStr nv))
            let np = (f, nv)
            (tcc_add_binding hh numericf  np, np::sofar)

        | (Tid(numericf, f), Fresher_id(nf, id, dbi_)) when f=id -> // binding to self somehow ... example please
            let nv = Bsv_ty_id(nf, Some P_free, (id, (*fresh_bindindex*) "-4")) ///<<============ TPDP explain
            if vd>=4 then vprintln 4 (sprintf "gen_afresher_kickoff: 2/2 bind to self for '%s' please explain why this is right!" f)
            let np = (f, nv)  // Do need to keep same number of pairs
            (hh, np::sofar)   // Why does this happen and does it need to be fresh? TODO

        | (Tid(numericf, f), Fresher_id(nf, id, dbi_)) -> // Binding to an unbound (abstract) type variable.
            match type_is_def_s (ee:ee_t) false id with
                | None ->
                    // Create fresh alias: should be in answer.
                    // Example is: typedef Reg#(foo) NewReg#(foo).
                    let nf' = vp_resolve(nf, numericf)
                    let nv = Bsv_ty_id(nf', Some P_free, (id, (*fresh_bindindex*) "-5"))
                    if vd>=4 then
                        let msg = (sprintf  "gen_afresher_kickoff: left bind '%s' to another (undefined) id '%s' " f id)
                        vprintln 4  msg
                    let np = (f, nv)
                    (tcc_add_binding hh nf' np, np::sofar)
                | Some nv ->
                    // Create fresh alias: should be in answer.
                    // Example is: typedef Reg#(foo) NewReg#(foo).
                    if vd>=4 then vprintln 4 (sprintf  "gen_afresher_kickoff: left bind '%s' to another (defined) id '%s' %s" f id (bsv_tnToStr nv))
                    let np = (f, nv)
                    (tcc_add_binding hh nf np, np::sofar)

                    // This is the fresher_id clause that may newly work which is not using the Fresher_id flag at all!
        | (Tid(numericf, f), Fresher_ty((Bsv_ty_id(nf, Some P_free, fid)) as nv))->  // Reverse bind
            // An example is: typedef RedBit#(16) M16;
            if vd>=4 then vprintln 4 (sprintf "gen_afresher_kickoff: reverse bind fid '%s' with type %s" f (bsv_tnToStr nv))
            let np = (f, nv)
            let nf' = vp_resolve(nf, numericf)
            (tcc_add_binding hh nf' np, np::sofar)

        | (Tid(numericf, f), Fresher_ty((Bsv_ty_id(nf, _, fid)) as nv))->  // pig in middle bind
            // An example is: typedef RedBit#(16) M16;
            vprintln 0 (sprintf  "gen_afresher_kickoff: pig in middle bind fid '%s' with type %s" f (bsv_tnToStr nv))
            let np = (f, nv)
            let nf' = vp_resolve(nf, numericf)
            (tcc_add_binding hh nf' np, np::sofar)

        | (Tid(numericf, f), Fresher_ty nv) ->  // Straightforward bind to concete type: M16 has no prams but there is one bind concrete bind for RedBit's prams.
            // An example is: typedef RedBit#(16) M16;
            if vd>=4 then vprintln 4 (sprintf  "gen_afresher_kickoff: straightforward bind fid '%s' with type %s" f (bsv_tnToStr nv))
            let np = (f, nv)
            (tcc_add_binding hh numericf np, np::sofar)
                                        
        | (lother, rother) -> sf (sprintf "gen_afresher_kickoff: other\n  lother=%A\n  rother=%A" lother rother)
                                    
    and afresh_wrapper ww (tb:tb_t) (h0:hindley_t) use_prams (gf, arg) =
        match arg with
            | arg when leaf_grounded_type_pred arg -> arg
            | Bsv_ty_nom(ats, _, args) ->
                let mf() = " checking type " + htosp ats.nomid
                //vprintln 3 ("TODO: bsvc.fs  +++ (swold) The use_prams norm here needs to take into account so stored for the retrieved type.")

                let use_args =
                    if nullp use_prams then []
                    else
                        arity_check_hard "afresh" false mf ats.nomid args use_prams
                        let resolve_so (a, b) =
                            match (a, b) with
                            | (Tty(ty, id), (SO_none _, ast_tyo, fid)) ->
                                //dev_println  (sprintf "afresh_wrapper pram resolve_so Tty(%s, %s) with fid b is just   b=%s" (bsv_tnToStr ty) id fid)
                                b 

                            | (Tid(VP_value(true, _), id), (SO_none _, ast_tyo, fid)) ->
                                //dev_println  (sprintf "afresh_wrapper pram resolve_so Tid(VP_value(true, _,) %s) with fid b is SO_numeric  b=%s" id fid)
                                (SO_numeric, ast_tyo, fid)
                            | (Tid(nf, id), (SO_none _, ast_tyo, fid)) ->
                                //dev_println  (sprintf "afresh_wrapper pram resolve_so Tid(_ %s) with fid b is non-numeric   b=%s" id fid)
                                b 
                            | (Tty(ty, id), b)     -> muddy (sprintf "afresh_wrapper yy id=%A b=%A" id b)
                            | (Tid(nf, id), (b))   -> muddy (sprintf "afresh_wrapper: x id=%A b=%A" id b)
                        let use_prams' = map resolve_so (List.zip args use_prams)
                        map (makesum2_use_useargs tb h0) use_prams'

               //Kickoff computation: It needs to recurse - otherwise it will never find addr_t55 for instance - but it needs to structurally bind not simply zip and the directory will only have one data_t55. TODO ratify again.

                //dev_println (sprintf "swold: afresh_wrapper use_args are %A" use_args)
                let kickoff =  // Where (more) concrete args are provided for type formals, we put these in the kickoff so they are used instead of fresh args.
                    match gf with
                        | G_some(ex_tyargs, latent_tyargs) ->
                            arity_check_hard "kickoff" false mf ats.nomid ex_tyargs use_args
                            //dev_println (sprintf "swold: dx-L1966 some synargs lengths explicit^=%i and latent^=%i" (length ex_tyargs) (length latent_tyargs))
                            //dev_println (sprintf "swold: dx-L1966 some synargs lengths explicit=%A and latent=%A" (ex_tyargs) (latent_tyargs))
                            let latent_usargs =
                                let bind_latent_tyarg = function
                                    | Tty(ty, fid)   ->  sf ("bind_latent_tyarg: Formal not type expected for binding for latent fid " + fid)
                                    | Tid(vpso, fid) ->
                                        match ee.genenv.TryFind [fid] with
                                            | Some aug ->
                                                muddy (sprintf "swold:  latent bind %s aug=%A" fid aug)
                                            | None ->
                                                let dbi = muddy ("Bind as self for latent fid " + fid)
                                                Fresher_id(vpso, fid, dbi)
                                map bind_latent_tyarg latent_tyargs
                            //let (tb_, kickoff) = List.fold gen_afresher_kickoff (tb, []) (List.zip (ex_tyargs) (use_args))  // When do args disappear? Bsv_ty_sum_nominal's args list are part of its name and what needs freshening (using ren) if not bound in kickoff
                            let (hh_, kickoff) = List.fold gen_afresher_kickoff (h0, []) (List.zip (ex_tyargs @ latent_tyargs) (use_args @ latent_usargs))  // When do args disappear? Bsv_ty_sum_nominal's args list are part of its name and what needs freshening (using ren) if not bound in kickoff
                            kickoff

                        | G_none ->
                            //dev_println "swold: dx-L1970"
                            let (tb_, kickoff) = List.fold gen_afresher_kickoff (h0, []) (List.zip args use_args)  // When do args disappear? Bsv_ty_sum_nominal's args list are part of its name and what needs freshening (using ren) if not bound in kickoff
                            kickoff
                            
                let kickoff = map (fun (x,y) -> (x, Fresher_ty y)) kickoff//recapsulate again - TODO messy 0 want some to be reverse_ids
                // Where the same abstract type occurs more than once with the same args, we do not really need to separately freshen each occurrence, but we do.
                let ans = afreshen_sty ww bobj "recapsulate" (Some kickoff) arg // Not the only callsite - others dont have useargs or kickoff? One has a kickoff from a useargs! The ifc one...
                ans

            | ty -> 
               vprintln 0 (sprintf "+++ afresh_wrapper other form ignored %A" ty)
               ty

    and topper_type_numeric ww tb h0 ast_ty = // dangerous since does not return tb' - better to not use in future
        let sty = makesum1 ww tb h0 (VP_value(true, [])) (temp_revert ast_ty)
        sty

    and makesum1 ww tb h0 sum1_maincontext aa =
        let vd = -1
        match aa with
        | KB_intLiteral _
        | KB_typeNat _    -> prep_typeIntLiteral ww aa


        | KB_type_dub (_, "Empty", [], _)  -> g_ty_empty
        | KB_type_dub (_, "Empty", _, lp)  -> cleanexit(lpToStr0 lp + ": Type Empty should not be parameterised.")        
        | KB_type_dub (_, "Action", [], _) -> g_ty_action
        | KB_type_dub (_, "Action", _, lp) -> cleanexit(lpToStr0 lp + ": Type ActionValue should be used when a value is returned instead of Action.")

        | KB_type_dub (_, "ActionValue", use_prams, lp) -> 
            if length use_prams <> 1 then cleanexit(mf() + ": Exactly one type should be specified as argument to an ActionValue")
            let (sl:freshener_t list) = map (makesum2_use_useargs tb h0) use_prams
            let wib = function
                | Fresher_ty sty      -> Tty(sty, "action_fid")
                | Fresher_id(nf, fid, dbi) -> Tid(nf, fid)
            let wob = function
                | Fresher_ty sty -> sty
                | Fresher_id(nf, fid, dbi) -> Bsv_ty_id(nf, Some P_free, (fid, (*fresh_bindindex*) "-6"))
            let skl = map wib sl // dont like wib and skl - perhaps delete somehow.
            Bsv_ty_nom({g_blank_nom_typ_ats with nomid=["ActionValue"] }, F_actionValue (wob(hd sl)), skl)


        | KB_typeBit_lc(None) -> g_canned_bit_ty
            
        | KB_typeBit_lc(Some(a,b)) ->
            vprintln 3 (sprintf "bit range type used range %i .. %i" a b)
            mk_bitty (abs(a-b)+1)

        | (Let [] (use_prams, KB_id(typen, (lp, _))))
        | (Let [] (use_prams, KB_constantAlias(typen, (lp, _))))
        | KB_type_fun (typen, use_prams, lp)
        | KB_type_dub ((*usecontext_flag_*)_, typen, use_prams, lp) ->
            //vprintln 0 (sprintf "swold: The raw AST use_prams to %s are %A" typen use_prams)
            if memberp typen tb.recursion_stack then
                sf (mf() + " recursive call")
            else
            let tb = { tb with recursion_stack= typen :: tb.recursion_stack }

            let ans =
                match typen with
                    | "int"    -> g_canned_int_ty  // Special lower-case forms
                    | "bit"    -> g_canned_bit_ty
                    | "module" -> g_maker_sty_ty
                    | "$unit_type" -> g_unit_ty 

                    | "SizeOf"  ->
                        let mm = typen
                        if length use_prams <> 1 then cleanexit(mf() + sprintf " type function '%s' requires exactly one argument" typen)
                        // do not want topper_type_numeric here - want SO_none instead.
                        let args' = map (makesum1 ww tb h0 (VP_none "sizeof arg")) (map temp_revert use_prams)
                        run_type_fun ww ee mf typen args'

                    | "TLog" | "TExp" | "TAbs" ->
                        let mm = typen
                        if length use_prams <> 1 then cleanexit(mf() + sprintf " type function '%s' requires exactly one argument" typen)
                        let args' = map (topper_type_numeric ww tb h0) use_prams
                        run_type_fun ww ee mf typen args'

                    | "TAdd" | "TSub" | "TMul" | "TDiv" | "TMax" | "TMin" ->  
                        if length use_prams <> 2 then cleanexit(typen + sprintf ": type function '%s' requires two arguments" typen)
                        //vprintln 0 (sprintf "2Type function '%s'  %A" typen use_prams)
                        let args' = map (topper_type_numeric ww tb h0) use_prams
                        run_type_fun ww ee mf typen args'


                    | typen when System.Char.IsUpper(typen.[0]) ->
                        let mf() = lpToStr0 lp + ": typecheck uppercase Type identifier " + typen
                        let ww = WF 3 m0 ww (mf())
                        
                        match typen with // Dont use this backdoor: (!g_predef_types).TryFind [typen] with
                            | "Clock" -> g_canned_Clock_type
                            | "Reset" -> g_canned_Reset_type

                            | "String" ->
                                // String should be a typedef but parsing '?' as a type for unspecfied length fails.
                                let useargs = map (makesum2_use_useargs tb h0) use_prams
                                let revert = function
                                    | Fresher_id(vp, fid, dbi) -> Bsv_ty_dontcare ("ntype-revert_" + fid)//stupid!
                                    | Fresher_ty(ty) -> ty
                                let ans = hydrate_type_builtins ww mf typen (map revert useargs) (g_string_ty)
                                //vprintln 0 (sprintf "Hydrate String gave " + bsv_tnToStr ans)
                                ans

                            | ss when ss = g_canned_cbusitem_type_name -> g_canned_cbusitem_type
                            | ss when ss = g_canned_CRAddr_type_name -> g_canned_CRAddr_type
                            
                            | _ ->
                                match ee.genenv.TryFind [typen] with
                                    | None ->
                                        match ee.ctor_tags.TryFind typen with
                                            | None ->
                                                let vd = 0
                                                let r = ref []
                                                vprint vd "In-scope tags are: "
                                                for z in ee.ctor_tags do ignore(vprint vd (z.Key + " ")) done 
                                                vprintln vd ""
                                                for z in ee.genenv do mutadd r z.Key done 
                                                vprintln vd ("Identifier was not found in the env. All=" + sfold_unlimited htosp !r)
                                                cleanexit(mf() + sprintf ": type or constructor '%s' was not defined in scope" typen)
                                            | Some [(enum_type, n)] ->
                                                //dev_println (sprintf "identified tag %s as member of enum %s" typen (bsv_tnToStr enum_type))
                                                enum_type // Exactly one

                                            | Some possible_types  ->
                                                let vtag = typen
                                                //dev_println (sprintf "For tag %s we start with %i possible types." vtag (length possible_types))
                                                let possible_types =
                                                    match pty_o with
                                                        | Some ty ->
                                                            let ty = tag_type_checker ww bobj lp mf vtag ty possible_types
                                                            //dev_println (sprintf "We selected %s" (bsv_short_tnToStr ty))
                                                            [ty]
                                                        | None -> map fst possible_types
                                                //dev_println (sprintf "For tag %s we end up with %i possible types." vtag (length possible_types))
                                                if length possible_types > 1 then
                                                    cleanexit (lpToStr0 lp + sprintf ": Cannot resolve type of ctor tag %A out of %s. pty_o=%s" typen (sfold bsv_short_tnToStr possible_types) (if nonep pty_o then "None" else bsv_short_tnToStr(valOf pty_o)))
                                                else hd possible_types

                                    | Some tdef ->
                                        let ans = hydrate_named_type ww tb h0 typen use_prams tdef
                                        //dev_println (sprintf "holy swold-E hydrated answer is %A" ans)
                                        ans

                    | typen -> // lower case indentifier - a type variable.
                        let fvd = false // extra printing for debugging
                        match ee.genenv.TryFind [typen] with
                            | None ->
                                match tcc_tagfind h0 [typen] with // tags?  lower case? TODO
                                    | None ->
                                        // so here want the id flagged as P_free or a different id form ... ? interesting to see if there are any non-free ones finally encountered in afresher ... a flagged id -> to become a fresher_id not a fresher_ty
                                        match sum1_maincontext with
                                            | VP_value(true, fidl) when not(nullp fidl) && fidl <> [""] -> // crude! 
                                               vprintln 0 (" Yes, we want an annotated integer here " + htosp fidl)
                                               Bsv_ty_int_vp(sum1_maincontext)
                                            | _ ->
                                                vprintln 3 (mf() + sprintf ": +++ %i used type var '%s' without any binding. We will need to know its new name.. prec=%A so cts=%A" -1 typen pty_o sum1_maincontext)
                                                Bsv_ty_id(sum1_maincontext, Some P_free, (typen, (*fresh_bindindex*) "-7"))
                                    | Some item ->
                                        match item with
                                            | Some(Bsv_ty_id(f, prec, typen)) -> Bsv_ty_id(f, prec, typen) // does nothing more than next clause.
                                            | Some ty -> ty
                                            | None -> cleanexit(mf() + sprintf " type name '%s' not bound" typen)
                            | Some(GE_binding(_, _, EE_typef(_, sty))) ->
                                if fvd then dev_println (mf() + sprintf ": fapped %s to item %A" typen sty)
                                sty
                            | Some(GE_binding(_, _, EE_e(sty, vale))) ->
                                if fvd then dev_println (mf() + sprintf ": fapped (wos numeric=%A have a vale %s)  %s of type %A" sum1_maincontext (bsv_expToStr vale) typen (bsv_tnToStr sty))
                                match sum1_maincontext with
                                    | VP_value(true, fidl) ->
                                        match vale with
                                            | B_hexp _ ->
                                                let ans = Bsv_ty_intexpi vale
                                                if fvd then vprintln 0 (mf() + " numeric fap " + bsv_tnToStr ans)
                                                ans
                                            | other -> muddy (mf() + sprintf "need upconvert for numeric vale %A" other)
                                    | other ->
                                        if fvd then dev_println "fapped to sty"
                                        sty
                            | Some other ->
                                sf (mf() + sprintf ": fapped %s to other form  %A" typen other)
            ans 

        | KB_functionCall(oo, args, (lp, uid)) -> muddy "other makesum1 KB_functionCall"

        | KB_functionProto(return_type, name, formals, provisos, lp) ->
            let provisos' = List.fold (proviso_parse ee) [] provisos
            let formals' = map (makesum2_def tb h0) formals
            let rt = makesum1 ww tb h0 (VP_value(false, [])) return_type
            let idl = if ee.hof_float then [name] else name :: ee.static_path
            let sty = Bsv_ty_nom({g_blank_nom_typ_ats with nomid=idl; cls=Prov_func }, F_fun(rt, {g_blank_fid with fids=map get_fidder formals; methodname=name; fprovisos=provisos'}, formals'), freshlister ww rt formals')
            sty

        | KB_typedefSTUnion(key, members_or_variants, KB_type_dub(true, id, use_prams, lp), derives, lp_) ->
            let (sl:freshener_t list) = map (makesum2_use_useargs tb h0) use_prams
            let idl = id :: tb.tb_static_path
            let recf = false // top hint only.
            let sl' = List.foldBack revertmakesum1 sl []
            //let sl' = List.foldBack (collect_sum2_tid ww bobj ee mm) use_prams []
            let sty = typrep_stunion_type ww tb h0 sl' recf (key, members_or_variants, idl)
            sty

        | KB_methodProto(pragmas, return_ty, methodname, formals, provisos, lp) ->
            let idl = if ee.hof_float then [methodname] else methodname :: tb.tb_static_path             // methods are never HOF in bluespec really.
            let mm = lpToStr0 lp + ": typecheck method prototype + " + htosp idl
            let ww = WF 3 m0 ww mm
            let _ = ins_ats bobj lp "method" bobj.prags idl pragmas // retrieve on definition path. TODO need global not local name
            //vprintln 0 (mf() + sprintf " save methodProto att %A" pragmas)
            let rt' = makesum1 ww tb h0 (VP_value(false, [])) return_ty
            let arg_tys = map (fun (so, fid, ty) -> makesum1 ww tb h0 (toVP so fid) ty) (map recast2a formals) // this is a sum2 call infact ? TODO
            let provisos' = List.fold (proviso_parse ee) [] provisos
            let (proto, rt_) = choose_proto ww mm rt'
            let mty = Bsv_ty_nom({g_blank_nom_typ_ats with nomid=idl; cls=Prov_method}, F_fun(rt', {g_blank_fid with fids=map get_fidder formals; protocol=proto; methodname=methodname; fprovisos=provisos'}, arg_tys), freshlister ww rt' arg_tys)
            mty

        // Some of these items, like an interface definition, for the sake of typechecking are just synonym definitions, so we could tidy up by projecting them into that form and using the synonym typechecker code instead of having special code each time. 
        | KB_interfaceDecl(KB_type_dub(_, name, use_prams, lp), items, nameo, lp_) ->  // TODO we dont have 'use' prams in a declaration?  ---Q Why would we freshen it at declaration time ?  A This can be an instance of this interface - the parsing token should be different to help us disambiguate.
            let iidl = name :: ee.prefix 
            let arity = length use_prams
            let mm = sprintf " typecheck interfaceDecl %s (actually a use site of a pre-declared interface) kind=%s" (lpToStr0 (lp)) (htosp iidl) + (if arity=0 then "" else "<" + sfold asty3ToStr use_prams + "...>")
            let ww = WF 3 m0 ww mm
            let (useargs:freshener_t list) = map (makesum2_use_useargs tb h0) use_prams
            let (h0, kickoff) = List.fold gen_afresher_kickoff (h0, []) (List.zip (map toSTP use_prams) useargs)  // When do args disappear? Bsv_ty_sum_nominal's args list are part of its name and are what needs freshening (using ren) if not bound in kickoff
            let kickoff = map (fun (x,y) -> (x, Fresher_ty y)) kickoff//recapsulate again - TODO messy --- and call the afresh_wrapper instead of using local code? This is a sort of hybryd use/decl site where new formal names are given.
            let usemelots arg = afreshen_sty ww bobj "interfaceDecl" (Some kickoff) arg 
            let tb_inner = { tb with tb_static_path=iidl }
            //dev_println (sprintf "holy ast to send to makesum1 %A" items)
            let items' = map (makesum1 ww tb_inner h0 (VP_type)) items
            //dev_println (sprintf "unholy answer from makesum1 %A" items')
            let sl' = List.foldBack revertmakesum1 useargs []
            let meta =
                let getsched cc = function
                    | KB_scheduleOrder(lst, (lp, _)) ->
                        let mm = lpToStr0 lp
                        parse_schedule mm lst :: cc
                    | _ -> cc
                List.fold getsched [] items
            Bsv_ty_nom({g_blank_nom_typ_ats with nomid=iidl; cls=Prov_ifc}, F_ifc_1 { g_blank_ii with iparts=map usemelots items';  imeta=meta;  }, sl')

            
        //This recurses by reference not by nesting in the declaration.  This is an instance of a subinterface in the definition of the outer.
        | KB_subinterfaceInst(attributes, KB_type_dub(f, kind, use_prams, lp), iname, lp_) -> // although grammatically a KB_typedef these are actual args to feed in.
            let arity = length use_prams
            let mm = " subinterfaceInst kind=" + kind + " iname=" + iname + (if arity=0 then "" else "<" + sfold asty3ToStr use_prams + "...>")
            let ww = WF 3 m0 ww mm
            let (sl:freshener_t list) = map (makesum2_use_useargs tb h0) use_prams            
            //dev_println (sprintf "holy sin sl=%A" sl) // THIS sl is not used for anything - that's wrong  .  use params are already 'evald' but need to bind the formals 
            let sif = makesum1 ww tb h0 (VP_value(false, [])) (KB_type_dub(f, kind, use_prams, lp))  // We should be looking it up and freshening it - makesum1 does do that.
            //dev_println (sprintf "holy sin %A" sif)
            let sl' = List.foldBack revertmakesum1 sl []
            Bsv_ty_nom({g_blank_nom_typ_ats with nomid=[kind];cls=Prov_ifc}, F_subif(iname, sif), sl')  // this just renames the formals but leaves the old formals unbound and ignored, like t55 - well makesum1 gets the old and new and should handle

        | KB_scheduleOrder(lst, lp) ->
            Bsv_ty_dontcare "scheduleOrder"
            
        | other -> sf (sprintf "other form found in makesum1 %A" other)

    and makesum2_def tb h0 = function//Process an AST type formal in a declaration context: masking any outer typevar binding of the same name.
        | (so, Some tas, fid) ->
            match makesum1 ww tb h0 (toVP so fid) tas with
                | other -> other
#if SPARE_CODE                
                | Bsv_ty_id(nf, prec, id) -> Bsv_ty_id(nf, prec, id)
                | Bsv_ty_nom(idl, c, d)   -> Bsv_ty_nom(idl, c, d)
                | other -> sf (mf() + sprintf ": makesum2_def  other [1] %A" other)
#endif                
        | (so, None, fid)    ->
            let nf = toVP so fid
            muddy "Bsv_ty_id(nf, muddy \"hereplease\", fid)  "


    and makesum2_use_useargs tb h0 arg =
        match arg with // Process an AST typeformal in a use context (type vars are looked up to find type values rather than being declared).
        | (so, Some tas, fid) ->
            let _ =
                match fid with
                    | "" 
                    | "$tyvar" 
                    | "$tyvar1"
                    | "$tyvar2" -> ()
                    | other -> muddy (sprintf "non-null id '%s'" fid)
            match makesum1 ww tb h0 (toVP so fid) tas with // TODO surely a VP_type default if non-numeric ?
                | Bsv_ty_id(nf, Some P_free, (id, _)) -> Fresher_id(nf, id, -1)
                | other ->
                    //vprintln 3 (mf() + sprintf ": makesum2_use_useargs backstop on [1] %A" other)
                    (Fresher_ty other)
        | (so, None, fid)    ->
            //vprintln 0 (sprintf "makesum2_use_useargs convert %A fid=%s" so fid)
            makesum2_use_useargs tb h0 (so, Some(KB_type_dub (false, fid, [], g_builtin_lp)), "")// Resolve parser ambig. 'so' does not meaningfully exist in a use context

    and typrep_stunion_type ww tb h0 sl recf ((key, items, idl) as arg) =
        let mm = key + " " + htosp idl // + sprintf " '%s' <%s>" (htosp idl) (sfold (gaToStr) genbinds_) // only for debug
        let ww = WF 3 "typrep_stunion_type " ww mm
        let simple_recf = isrec_stunion_type ww bobj ee [] arg
        vprintln 3 (sprintf "%s: simple_recf %A" mm simple_recf)
        let recf = simple_recf || recf
        match arg with
        | ("struct", items, nomid) ->
            let sum_struct = function
                | KB_structMember(ty, id) ->
                    let ty = makesum1 ww tb h0 (VP_value(false, [])) ty // does this freshen!!!!!!!
                    in (id, ty)
                | KB_builtin _ -> sf (htosp idl + " was not properly built in to the compiler")
                | other -> sf (sprintf " other form in sum_struct %A" other)
            let fields = map sum_struct items
            Bsv_ty_nom({g_blank_nom_typ_ats with nomid=idl; cls=Prov_None "struct"; }, F_struct(fields, None), sl)

        | ("tunion", variants, nomid) ->
            //let inners = ref []
            let mrecflag = ref MR_unset
            let dmto id = cleanexit("Tag " + id + " added to tagged union more than once in " + htosp idl)        
            let sum_tagged_union cc tu =
                    let tags = map fst cc
                    match tu with
                    | KB_unionMember(Some tya, id) ->
                        let p = makesum1 ww tb h0 (VP_value(false, [])) tya
                        if memberp id tags then dmto id
                        (id, Some p)::cc
                    | KB_unionMember(None, id) ->
                        let p = None // Encountered with zero arity constructor tags (like Nil and Invalid).
                        //vprintln 0 ("+++ want to make a note of when this form is encounted " + id)
                        if memberp id tags then dmto id
                        (id, p)::cc // THE LACK OF A SOME HERE FORCES WHOLE DATA STRUCTURE TO BE AN OPTION - CAN THIS BE IMPROVED? TODO. - when is this form encountered? For zero arity enum forms such as None itself!
                    | KB_unionSubStruct(items, id) ->
                        // mrecflag may get set later! ?  Or is it mixed up with recf here?
                        let p = typrep_stunion_type ww tb h0 sl recf ("struct", items, id::idl)
                        //let _ = inners := newkeys @ !inners
                        if memberp id tags then dmto id
                        (id, Some p)::cc

                    | other -> sf ("sum_union other " + sprintf "%A" other)

            let variants' = rev(List.fold sum_tagged_union [] variants)

            Bsv_ty_nom({g_blank_nom_typ_ats with nomid=idl; cls=Prov_None "tunion"; }, F_tunion(mrecflag, variants', None), sl)

    makesum1 ww { g_null_tb with tb_static_path=ee.prefix } [] vp_top ast_ty



let collect_sum2x ww bobj ee msg dprec = function
    | (so, Some ast_ty, fid)  ->
        if fid = "$HOF" then
            let pty_o = None // (Some(P_tlab "2f_hof"))
            let sty = collect_sum1 ww bobj ee msg pty_o (toVP so "$HOF") ast_ty // have a SO_form for hof special case instead of a reserved string $HOF ?
            match sty with
                | Bsv_ty_nom(_, F_fun(rty, mats, arg_tys), ty_args) ->
                    (so, set_hof_flag_ sty, mats.methodname)
                | other -> muddy (sprintf "HOF (A) other form - must have a name - %A " other)
        else (so, collect_sum1 ww bobj ee msg None (toVP so fid) ast_ty, fid)
    | (so, None, fid)      ->
        if fid = "$HOF" then sf "illegal hof form"
        else
            //let dprec = muddy (sprintf "threrplease %A %A" so fid)
            (so, gen_Bsv_ty_id(toVP so fid, dprec, (fid, (*fresh_bindindex*) "-8")), fid)

let collect_sum2_tid ww bobj ee msg dprec_ arg cc =  // Not used now.
    match collect_sum2x ww bobj ee msg None arg with // TODO fold down cc!
        | (_, Bsv_ty_id(nf, prec, (id, _)), _) -> Tid(nf, id)::cc

let collect_sum2f ww bobj ee mf = function
    | (so, Some(ty:bsv_ast_t), "$HOF") ->
        let pty_o = None // (Some(P_tlab "2f_hof"))
        let sty = collect_sum1 ww bobj { ee with hof_float=true } mf pty_o (toVP so "$HOF") ty // have a SO_form for hof special case instead of a reserved string $HOF ?
        //vprintln 3 ("HOF function formal has type " + bsv_tnToStr ty')
        match sty with
            | Bsv_ty_nom(_, F_fun(rty, mats, arg_tys), ty_args) ->
                (set_hof_flag_ sty, mats.methodname)
            | other -> muddy (sprintf "HOF (B) other form - must have a name - %A " other)
    
    | (so, Some tas, fid) ->
        let pty_o = None // Some P_defn1   // TODO correct? - or delete throughout since not used.
        if fid = "$HOF" then sf "illegal hof form"
        else (collect_sum1 ww bobj ee mf pty_o (toVP so fid) tas, fid)
//    | (so, None, fid)     -> gen_Bsv_ty_id((so=SO_numeric), fid)
    | other -> sf (mf() + sprintf ": other form collect_sum2f %A" other)



let norm_find_method mf name = function
    | Bsv_ty_nom(iidl, F_ifc_1 ii, _) ->
        let m_pred = function
            | Bsv_ty_nom(_, F_fun(_, fats, _), _) -> fats.methodname = name
            | _ -> false
        let candidates = List.filter m_pred ii.iparts
        if length candidates<>1 then cleanexit(mf() + sprintf ": method called '%s' was missing on interface (or duplicated!)" name)
        if nullp candidates then cleanexit (mf() + sprintf "Empty interface has no method called '%s'" name)
        else hd candidates

    | other -> sf(mf() + sprintf ": other form norm_find_method '%s' other=%A" name other) 




type monica_pair_t = bool * monica_t // Ext method flag and rule/method name.

type smrec_t =
    | MS_bluevale of hbexp_t * bsv_exp_t option
    | MS_denot of monica_pair_t * string * hbexp_t * (bsv_type_t * bsv_exp_t) * hexp_t option * string

type statemap_t = Map<string list, smrec_t>

type fwdpath_closure_t = 
    {
        rhs:          bsv_exp_t       // The expression to be stored in the forwarded-over register
        fwdtrigger:   hbexp_t         // The expression that holds when the forwarding path is needed next clock cycle.
        dirinfo:      director_info_t
    }

// The pair formed from a register name and assign site are guaranteed to be unique owing to every expansion of a module definition has a unique iname.
type fwdpath_assign_t =  // When a vector is assigned from a forwarding path there is one instance of this per vector element. Please explain how indexed. TODO indexed not yet supported.
    {
        rhs_tag:      tagged_linepoint_t// The tag of the read operation on the pipelined operator whse result needs forwarding.
        name:         string             // Unique identifer for this path (from source to register 
        scorebd:      hexp_t             // A one-hot net that holds when forwarding path is active
        closed:       fwdpath_closure_t option
        fwd_func:     (hexp_t -> hexp_t) // Lightweight combinational logic replicated on forwarding path (eg field select or invert).
        rdbus:        hexp_t             // The output read bus for the pipelined operator.
    }

type fwdpath_rec_t =  // When a vector is assigned from a forwarding path there is one instance of this per vector element. Please explain how indexed. TODO indexed not yet supported.
    {
        //tag:          tagged_linepoint_t// The tag number for the register write site that needs shoothru via the forwarding logic.
        ty:           bsv_type_t      // The type of the data flowing down the forwarding path
        register:     hexp_t          // The forwarded over register output bus.
        //yval:         bsv_exp_t       // The composite read expression at the output of all forwarding multiplexors. Not needed anymore.
        assigns:      fwdpath_assign_t list
        cmd_name:     string
    }
    
type fwdpaths_map_t = Map<string list, fwdpath_rec_t> // Indexed by tag number from tagged linepoint.

type qmaps_t = // The type of sigma, the currency.
    {
        ifcmap:       ifcmap_t     // Interfaces directory - maps interface instance names and aliases to interface inodes (many-to-one potentially).
        methmap:      methmap_t    // Interfaces directory - the currency        
        pendingmap:   statemap_t   // Pending updates created by current rule, not yet committed to statemap. Aka \sigma_p.
        statemap:     statemap_t   // Shotgun denotational mapping of hardstate for superscalar modes. Aka \sigma_c.
        bluewires:    statemap_t   // This holds the current state of each 'wire' such as PulseWire RWire and so on.
        fwdpaths:     fwdpaths_map_t  // Need for post render of scoreboard logic
        exordering:   execution_order_constraint_t  list              // Execution ordering constraints
    }


// This is a write to the compile-time env, post elab.
let carenv_write bobj pass site sigma lhs rhs = 
    let vd = bobj.build_loglevel
    // Do not save carenv data on the second pass since stmts are compiled outside of their defining inode context and carenv updates can be wrong. The correct entries should be present from the first pass anyway.
    if pass.pass_no >= 2 then sigma
    else
        let ifcmap = (sigma:qmaps_t).ifcmap.Add(lhs, rhs)
        if vd>=4 then
            let ls = (hptos lhs)
            let rs =  sprintf "%A" (rhs)
            vprintln 4 (sprintf "carenv_write: pass=%i: %s: lhs=%s rhs=%s" pass.pass_no site ls rs)
        let sigma = { sigma with ifcmap=ifcmap }
        sigma
        
// Find a method in the currency.
// Return either the concrete or ephemeral version of a method or else an error message.
let find_meth mf sigma ifc_name method_name =
    let vf = false
    match (sigma.ifcmap:ifcmap_t).TryFind ifc_name with
        | None ->
            let em = sprintf "find_meth: method_name=%s no inode for ifc %s" method_name (hptos ifc_name)
            if vf then dev_println "find_meth: No inode found"
            ((IIS(""), "-rein-"), None, None, Some em)
        | Some inode ->
            let rec gobble inode = 
                let key1 = (inode, method_name)
                let r2 = (sigma.methmap:methmap_t).TryFind key1 
                match r2 with
                    | Some(Sigma _) ->
                        if vf then dev_println(sprintf "Found concrete currency for method " + method_name)
                        let concrete_currency = sigma
                        (key1, r2, None, None) // Concrete found

                    | Some(Sigma_e _) ->
                        if vf then dev_println(sprintf "Found ephemeral currency for method " + method_name)
                        let concrete_currency = sigma
                        (key1, None, r2, None) // Ephemeral found

                    | None ->
                        let ms1 = sprintf "find_meth: method_name=%s not present in sigma under inode=%A  for interface %s" method_name inode (hptos ifc_name)
                        vprintln 3 ms1
                        //dev_println (sprintf "not-yet-bound: ifc_name=%s maps to %A and %s" (hptos ifc_name) inode ms1)
                        (key1, None, None, Some ms1)
            gobble inode



let getnum32 ww namehint tag = function
    | None -> sf ("Needed parameter=`" + tag + "' in " + namehint)
    | Some (ty, const_exp) -> xi_manifest "getnum32" const_exp
//    | Some (ty, other) -> sf ("Needed numeric parameter=`" + tag + "' in " + namehint + ", not " + xToStr other)




let bsv_valueof_core mf = function
    | Bsv_ty_intconst n              -> B_hexp(Bsv_ty_integer, xi_num n) 
    | Bsv_ty_intexpi(B_hexp(t, x))   -> B_hexp(t, x)
    //| Bsv_ty_primbits s when false ->  let a = B_hexp(Bsv_ty_integer, xi_num s.width) in (rty, None, a)
    //| Bsv_ty_primbits(None, Some(_, n)) -> B_hexp (xi_num n)  // was taking width
    | other -> cleanexit(mf() + ": Unsuitable type used as numeric type: " + bsv_tnToStr other)


let bsv_valueof_lifted_core mf = function
    | EE_typef(_, x) -> bsv_valueof_core mf x
    | EE_e(_, B_hexp(ty, v)) -> B_hexp(ty, v)
    | other -> sf (mf() + sprintf ": Unsuitable lifted type used as numeric value: " + eeToStr other)

                        
let gen_fireguard emits_o longname =
    let rr = vectornet_w(longname + "_FIRE", 1)
    if not_nonep emits_o then mutadd (valOf emits_o).nets rr
    rr


    
let find_ifc mf what (qq:qmaps_t) =
    let ns1 = (fun a -> a) // + sprintf "<%A>" a)        
    match qq.ifcmap.TryFind(what) with
        | None ->
            let m_r = ref []
            for k in qq.ifcmap do mutadd m_r (htosp k.Key) done
            cleanexit(mf() + ": cannot find interface called '" + htosp what + "'\n all=" + sfold_unlimited ns1 !m_r)
                        //"\n env= " + sfold (fun (a,b)-> htosp a + "->" + bsv_expToStr b) env +

        | Some h -> h // Not a loose match
 



let rec vmemberp vtag = function
    | [] -> false
    | KB_unionSubStruct(_, t)::tt
    | KB_unionMember(_, t)::tt -> t=vtag || vmemberp vtag tt
    | snother::_ -> sf (sprintf "vmemberp snother %A" snother)



let bi_dopack ww x = x


let rec is_signed = function
    | Bsv_ty_nom(_, F_fun(t, _, _), _)
    | Bsv_ty_nom(_, F_actionValue t, _)
    //Bsv_ty_action(RV_netlevel(t, _))
            -> is_signed t    

    | Bsv_ty_primbits l  -> l.signed=None || l.signed = Some Signed
    | Bsv_ty_integer -> true
    | other ->

        dev_println (sprintf "is_signed: default unsigned for %A" other)
        false


let rec gen_B_shift amount = function (* positive offset is left shift *)
    | B_shift(arg, a1) -> gen_B_shift (amount+a1) arg // NB: This does not clip lost bits
    | arg ->B_shift(arg, amount)

let rec gen_B_mask wid = function
    | B_mask(w1, arg) -> gen_B_mask (if w1<wid then w1 else wid) arg
    | arg ->B_mask(wid, arg)


let g_report ig = " g=" + sfold gaToStr ig

// The <= operator is sugar for an application of the _write method.
let desugar_varDeclAssign ww msg arg =
    let change_write_form = function // The raw form is changed thus to serve as a done marker. I am not sure why it does not change to _write() form. It used to? Is it so we can trap superscalar ops?
        | (lhs, r, (lp0, uid)) -> KB_regWrite(lhs, r, (lp0, uid))
        //KB_varDeclAssign_raw(tyo, lhs, rhso, lp) -> KB_varDeclAssign(tyo, lhs, rhso, lp)    

    let rec desug = function
        // a[b] <= r is expanded as a[b] <= r (a nop)
        | KB_regWrite_raw(KB_subscripted(false, lhs, p, q, (lp1, uid)), rhs, lp0) -> change_write_form (KB_subscripted(false, lhs, p, q, (lp1, uid)), rhs, lp0)

        // a.[b] <= r is expanded as a._native_write(b, r)
        | KB_regWrite_raw(KB_subscripted(true, lhs, [subs], _, (lp1, uid)), rhs, lp0) ->
            //sprintf "hit desugar_varDeclAssign %s: %A" msg arg)
            let expanded = KB_functionCall(KB_ast(KB_tag(lhs, s_native_write, lp0)), [subs; rhs], lp0) // method call on this instance.
            expanded

        // a.b <= r is expanded as a.b._write(r)
        | KB_regWrite_raw(KB_tag(item, htag, (lp, uid)), rhs, lp0) ->
            // OLD a.b <= r is expanded as a.b <= r (a nop)
            // OLD change_write_form (KB_tag(item, htag, (lp, uid)), rhso, lp0)
            let expanded = KB_functionCall(KB_ast(KB_tag(KB_tag(item, htag, (lp, uid)), g_swrite, lp0)), [rhs], lp0) 
            expanded

        // a <= r is expanded as a <= r (a nop).  Was previously expanded as a._write(b) ?
        | KB_regWrite_raw(KB_id(ss, (lp, uid)), rhso, lp0) -> change_write_form (KB_id(ss, (lp, uid)), rhso, lp0)


        | other ->
            sf(sprintf "misapplied desugar_varDeclAssign %s: %A" msg other)

    desug arg
        
let fun_arity_check mf fname gf sl =
    if nullp gf then () // varargs case
    elif length sl <> length gf then
        vprintln 0 (sprintf "raw forms are formals expected=%A, actuals provided=%A" gf sl)
        let sx = mf() + sprintf ": arity check failed, wrong number of arguments for function/method application %s: expected=%i provided=%i" fname (length gf) (length sl)
        cleanexit sx


// Check whether the return type with an auto_apply is viable.
let auto_apply_compat ww bobj mf cc tyo asReg rt =
    let tyo =
        match (tyo, asReg) with
           | (None, Prov_sty sty) -> Some sty
           | (Some sty, Prov_sty sty') -> sf "Two tyo's provided!"
           | (tyo, _) -> tyo
    if nonep tyo then
        vprintln 3 (mf() + ": auto_apply_compat - no tyo so holds true")
        true
     else
         let mm = "auto_apply: compat"
         let h0 = []
         match sum_type_meet_serf ww bobj mf h0 (valOf tyo, rt) with
             | [] -> false
             | (_, ans)::_ ->
                 vprintln 3 ("auto_apply : type compatible check: found ok resolved type=" + bsv_tnToStr ans)
                 true


let m_next_lp_tag = ref 400 // Use a distinctive starting number to assist debug,
// A tagged linepoint has a unique natural number paired alongside a source file reference.
// Each apply site is given a textual tag (used as forwarding paths signature key).
let tag_this_lp lp =
    let no = !m_next_lp_tag
    mutinc m_next_lp_tag 1
    (lp, no)


//
// auto_apply: Invoke the _read() method if present and proviso asReg=None.
//
let prep_auto_apply ww bobj mf (prov: bsv_proviso_t) tyo lp arg =
    let vd = bobj.normalise_loglevel
    let rd_pred1 = function
        | Bsv_ty_nom(_, F_fun(_, mats, args), _) ->
            let ans = mats.methodname = "_read" && nullp args 
            if vd>=5 then vprintln 3 (sprintf "prep_auto_appyl: rd_pred1 %s %A" mats.methodname ans)
            ans
            // TODO why does this one differ from the next and not include protos?
        | other ->
            if vd>=5 then vprintln 4 (mf() + sprintf ": auto_apply rd_pred1 other %A" other)
            false // also have subinterfaces here. sf 
        
    let quik_ri lp methodname path rt = B_applyf(PI_bno(B_dyn_b(methodname, path), None), rt, [], tag_this_lp lp)

    let quik_rx lp methodname path rt =
        if vd>=5 then vprintln 5 (sprintf "auto_apply (function) function=%s\npath=%A" methodname (bsv_expToStr path))
        B_applyf(PI_bno(path, None), rt, [], tag_this_lp lp)

            
    match arg with
    | ((Bsv_ty_nom(_, F_fun(rt, mats, formals), _)) as ty, lmode, path) when auto_apply_allowedp vd rt prov ->
        let rf = rd_pred1 ty 
        let uf = nullp formals 
        if   rf then (rt, lmode, quik_rx lp mats.methodname path rt)
        elif uf then (rt, lmode, quik_rx lp mats.methodname path rt) // auto apply unit method - TODO delete this.
        else  (ty, lmode, path)

    | ((Bsv_ty_nom(iidl, F_ifc_1 ii, _)) as t, lmode, path) ->
        let old_rd_pred = function
            | Bsv_ty_methodProto(methodname, _,  mth_provisos, proto, Bsv_oty_fun(sty_, _, rt, args, provisos, _)) ->  methodname = "_read" &&  auto_apply_allowedp vd rt prov
            | Bsv_ty_nom(idl, F_fun(rt, matts, args), _) when matts.methodname = "_read" && nullp args ->
                let ans = auto_apply_allowedp vd rt prov
                if vd>=5 then vprintln 5 (sprintf "auto_apply: old_rd_pred: Conider _read() method auto_apply_allowedp=%A prov=%A" ans prov)
                ans
            | other ->
                if vd>=5 then vprintln 5 (mf() + sprintf ": auto_apply: old_rd_pred: other %A" (bsv_tnToStr other))
                false // also have subinterfaces here but we would not auto_apply in a subinterace. 

        let rd_candidates = List.filter old_rd_pred ii.iparts
        let n_rds = length rd_candidates
        if vd>=5 then
            vprintln 5 (sprintf "%s Auto_apply metrics n_rds=%i enabled=%A" (bsv_expToStr path) n_rds prov)
            vprintln 5 (sprintf "auto_apply rds==%A " rd_candidates)   
        let null_cc = []
        match (rd_candidates) with
            | [] -> (t, lmode, path)
            | ([Bsv_ty_methodProto(methodname, prats, mth_provisos, proto, Bsv_oty_fun(sty_, _, rt, args, provisos, _))]) when auto_apply_compat ww bobj mf null_cc tyo prov rt -> // NB: the underscore for the unit candidates give auto-precedence to doing a read on a both-cases tie, such as in a Reg or DWire, but we could instead predicate this on a r-mode preference passed in as a 3-way tyo from apply8.
                let path' = quik_ri lp methodname path rt
                vprintln 3 (sprintf "Did a %s auto_apply, (methodproto) returning %s " methodname (bsv_tnToStr rt))
                (rt, lmode, path')// Here rt and the lmode type will be different.

            | [Bsv_ty_nom(_, F_fun(rt, matts, args), _)] when auto_apply_compat ww bobj mf null_cc tyo prov rt ->
                let path' = quik_ri lp matts.methodname path rt
                vprintln 3 (sprintf "Did an auto_apply (interface method) %s, returning %s " matts.methodname (bsv_tnToStr rt))
                (rt, lmode, path')
                
            | (_) ->
                let mname = function
                    | Bsv_ty_methodProto(methodname, prats, mth_provisos, proto, _) -> methodname
                    | Bsv_ty_nom(_, F_fun(rt, matts, args), _)                      -> matts.methodname
                vprintln 3 (mf() + sprintf ": prep_auto_apply: +++ Interface %s has  %i read methods: need to disambiguate implied operation based on type requested tyo=%s" (htosp iidl.nomid) n_rds (if nonep tyo then "None" else bsv_tnToStr(valOf tyo)))
                (t, lmode, path)

            //| other -> muddy (sprintf "other prep_auto_apply_read other=%A" other)

    | (t, lmode, path) ->
        if vd>=5 then vprintln 5 (sprintf "Auto_apply (asReg=%A) did not auto_apply to type %s" prov (bsv_tnToStr t))
        (t, lmode, path)



// Get a brief, possibly meaningless, summary of a lhs for error logging and so on.
let lhs_path_shortname ww mf lhs = 
    // This should support all prep_path forms ... can use prep_path when null tyo
    let rec diz_lhs_path prefix = function
        | KB_id(ss, (lp, uid))                                          -> (ss::prefix, 0, uid, lp)
        | KB_bitRange2(lhs, range_hi, range_lo, lp) -> diz_lhs_path prefix lhs
        | KB_varDeclAssign(_, lhs, _, _)            -> diz_lhs_path prefix lhs
        | KB_subscripted(dotflag, abase, subs_lst, _, _) ->
            let (shortname, stars, uid, lp) = diz_lhs_path prefix abase
            (shortname, stars + length subs_lst, uid, lp) // The index is a vector dimension - so perhaps not an expression that needs typechecking? Should be done here on typehchecking pass.
        | KB_tag(item, htag, (lp, uid))                                 -> diz_lhs_path (htag::prefix) item
        | _ ->
            vprintln 0 (sprintf "unacceptable lhs=%A" lhs)
            cleanexit (mf() + ": lhs_path_shortname: unacceptable lhs (L2860)")

    let (idl, stars, lhs_uid, lp) = diz_lhs_path [] lhs
    let idl = rev idl
    //println (sprintf "lhs_path full ast = %A" lhs)
    (hptos idl, idl, stars, lhs_uid, lp)



let rec prep_bitflag ww bobj mf lp (asReg:bsv_proviso_t) bt = // bitflag update ? Might be moved to prep?
    match bt with // TODO this predicate is elsewhere call is_intlike_pred ... please have one copy and use it.
        | Bsv_ty_nom(nst, _, _) when nst.nomid = ["List" ; "List"] -> false
                    
        | Bsv_ty_nom(_, F_ifc_1 _, _) ->                        
            let filler = B_hexp(Bsv_ty_integer, xi_num 0) // A nonse - we only care about type here.
            let (bt', _, _) = prep_auto_apply ww bobj mf asReg None lp (bt, 0, filler)
            prep_bitflag ww bobj mf lp (Prov_asReg_asIfc "1bitflag") bt'
        | Bsv_ty_nom(ats, F_ntype, _) when ats.nomid=["Bit"] -> true
        | Bsv_ty_integer 
        | Bsv_ty_primbits _ -> true

        | Bsv_ty_nom(_, F_vector, _) -> false // TODO include stars. discuss auto_apply read on vector?
                        
        | Bsv_ty_stars(nn, _) when nn <> 0 -> false

        | other ->
            hpr_warn (mf() + sprintf ": +++ check for bit extract/insert: other base type ignored %A" (bsv_tnToStr other))
            false


            
// Typecheck: Handle l-values and hence the majority of primary expression forms.
// We return the uid of the lhs of any subscript operation.
let rec typrep_path ww bobj typecheck_exps mf ee tbcc h0 (asReg:bsv_proviso_t) path = 
    let ww = WF 3 "typrep_path" ww (mf() + " start")
    let q0 = "typrep_path"
    let rec prep_ap stars tbcc h0 asReg arg = // TODO we might not need any other auto_apply call site now? 
        let (ty, uid, tbcc, h0) = prep_dig stars tbcc h0 asReg arg
        let tyo = None
        let lp = g_builtin_lp
        let mm = "prep_ap"
        let filler = B_hexp(Bsv_ty_integer, xi_num 0) // nonse - only doing type here
        let pre = bsv_tnToStr ty
        //vprintln 0 (sprintf "  asReg=%A pre auto_apply %A" asReg pre)
        let (ty', lmode, path) = prep_auto_apply ww bobj mf asReg tyo lp (ty, 0, filler)
        let post = bsv_tnToStr ty'
        //vprintln 0 (sprintf "  asReg=%A post auto_apply %A" asReg (if pre=post then "SAME" else post))
        //if post = "fun:first{<NONE>->struct:{tpl_1, tpl_2}-named-as'Tuple2'#(^ubit#16/tax_t, ^ubit#16/tbx_t)}-named-as'first'#(struct:{tpl_1, tpl_2}-named-as'Tuple2'#(^ubit#16/tax_t, ^ubit#16/tbx_t)/contenttypeZUB10)"                 then muddy "temp2 stop"
        (ty', uid, tbcc, h0)

    and prep_dig stars tbcc h0 (asReg:bsv_proviso_t) arg = // asReg as passed in only applies to ... TODO say and do
        match arg with
        | KB_bitRange2(item, range_hi, range_lo, lp) -> // Tested in ex_08_d.
            let p0 = prep_dig stars tbcc h0 (Prov_None "bitRange2") item
            //muddy (sprintf " %A // Treat as an id but path may be longer... expect at outermost only" arg)
            p0 // We assume, for now, that the type is unaffected by a range select, but we could return a bits value or indeed a bits field with precise width?

        | KB_subscripted(dotflag, item, subs_lst, m_bitflag, (lp, subsc_uid_)) when !m_bitflag ->
            let (rr, uid, tbcc, h0) = prep_dig stars tbcc h0 (Prov_None "subscripted-1") item // Do not delete a star for a bit extract/insert.
            let  sags = typecheck_exps ww (ee, tbcc, h0) subs_lst
            let (tbcc, h0, tys_) = if length sags = 1 then hd sags else muddy "HBOMB-29321"
            (rr, uid, tbcc, h0)

        | KB_subscripted(dotflag, item, subs_lst, m_bitflag, (lp, subsc_uid_)) when (not !m_bitflag) ->
            let (p0, uid, tbcc, h0) = prep_dig stars tbcc h0 (Prov_None "subscripted-2") item
            let bitflag2 = prep_bitflag ww bobj mf lp (asReg) p0 // Further check of bitflag
            if bitflag2 then
                vprintln 3 (sprintf "Setting bitflag on subscript operator instance at " + lpToStr lp)
                m_bitflag := true
                prep_dig stars tbcc h0 asReg arg // Go round again
            else
                let sags = typecheck_exps ww (ee, tbcc, h0) subs_lst
                let (tbcc, h0, tys_) = if length sags = 1 then hd sags else muddy "HBOMB-29323"
                let rr =
                    if dotflag then // The dotted extension does not deref the BRAM. Not sure why.  It would be more sensible to.
                        //vprintln 3 (sprintf "Typecheck dotted/BRAM subscript")
                        p0
                    else take_stars mf (length subs_lst) p0             // We remove one star in the subscripting operation since it is a dereference.
                (rr, uid, tbcc, h0)
        
        | KB_tag(item, htag, (lp, uid_)) ->
            let rec tycheck_apply_tag stars = function
                | Bsv_ty_stars(mm, ty) -> tycheck_apply_tag (mm+stars) ty

                | Bsv_ty_nom(idl, F_vector, [subs_type_; Tty(ty, _)]) -> tycheck_apply_tag (stars+1) ty
                   
                | Bsv_ty_nom(idl, F_subif(iname, sty), bb) when false -> // vestigial form? - now handled inside F_ifc_1 clause
                    vprintln 0 (sprintf "Strange discard step of F_subif %s on tag " (bsv_tnToStr(Bsv_ty_nom(idl, F_subif(iname, sty), bb))) + htag)
                    tycheck_apply_tag stars sty // why? dont we need to check idl=htag?

                | Bsv_ty_nom(ats, F_tunion(mrecflag, variants, details_), _) -> 
                    match List.filter (fun (a,b)->a=htag) variants with
                        | [] ->
                            vprintln 0 ("Available fields in tagged union : " + sfold fst variants)
                            cleanexit(mf() + sprintf ": tag '%s' applied to %s that does not have a member of this name" htag (htosp ats.nomid))
                        | [(id, Some ty)] ->
                            vprintln 4 (mf() + sprintf "// record dropping ty %s against uid %A" (bsv_tnToStr ty) uid_)
                            ty
                        | [(id, None)] -> cleanexit (mf() + sprintf ": constant tag '%s'" htag) // muddy

                        | other ->
                            cleanexit(lpToStr1 lp + sprintf ": tag '%s' applied to tagged union %s that seems to have more than one member of this name" htag (htosp ats.nomid))

                        
                | Bsv_ty_nom(ats, F_struct(fields, details_), _) ->
                    match List.filter (fun (a,b)->a=htag) fields with
                        | [] ->
                            vprintln 0 ("Available fields in struct : " + sfold fst fields)
                            cleanexit(mf() + sprintf ": tag '%s' applied to %s that does not have a member of this name" htag (htosp ats.nomid))
                        | [item] ->
                            let ty = snd item
                            //vprintln 4 (mf() + sprintf "// record dropping ty %s against uid %A" (bsv_tnToStr ty) uid)
                            ty
                        | other ->
                            cleanexit(lpToStr1 lp + sprintf ": tag '%s' applied to struct %s that seems to have more than one member of this name" htag (htosp ats.nomid))

                | Bsv_ty_nom(ats, F_ifc_1 ii, _) ->
                    match List.filter (cander_ifc htag) ii.iparts with
                        | [] ->
                            vprintln 0 ("L1676: Available components and methods in target interface are: " + sfold bsv_tnToStr  ii.iparts)
                            cleanexit(mf() + sprintf ": tag '%s' applied to interface %s that does not have a member of this name." htag (htosp ats.nomid))
                        | [Bsv_ty_nom(ats, F_subif(iname, sty), ignored_)] ->
                            //vprintln  0 (sprintf ("Generated vestig form and chose wrong component ? tag=%s item=%s") htag (bsv_tnToStr item))
                            sty

                        | [Bsv_ty_nom(ats, F_fun(rt, fats, fargs), sl)] -> Bsv_ty_nom(ats, F_fun(rt, fats, fargs), sl)
                        
                        | [other] ->
                            vprintln 0 (mf() + sprintf " another tag form dripping thru %A" other)
                            other
                        | other ->
                            cleanexit(mf() + sprintf ": tag '%s' applied to %s that seems to have more than one member of this name" htag (htosp ats.nomid))

                | other -> cleanexit(lpToStr1 lp + sprintf " tag '%s' stars=%i applied to untagged form %s details=%A" htag stars (bsv_tnToStr other) other)
            let (s, uid, tbcc, h0) =  prep_dig 0 tbcc h0 (Prov_None "L2305") item
            (tycheck_apply_tag 0 s, uid, tbcc, h0) // Set both stars back to zero for now. What's correct?

        | KB_id(s, (lp, uid)) ->
            //vprintln 0  (mf() + sprintf ": path prep  '%s' %A" s (ee.genenv.TryFind[s]))
            let sty = 
                match ee.genenv.TryFind[s] with
                | None ->
                    //let mm = lpToStr0 lp // handed-in m seems null
                    let r = ref []
                    for z in ee.genenv do mutadd r z.Key done 
                    vprintln 0 ("Identifier was not found in the env. All=" + sfold_unlimited htosp !r)
                    cleanexit(mf() + ":" + lpToStr0 lp + sprintf ": prep undefined id '%s'" s)
                | Some(GE_binding(_, _, EE_typef(_, sum_ty))) -> sum_ty
                | Some(GE_binding(_, _, EE_e(sty, _))) -> sty
                | Some(GE_overloadable(prototy, oloads)) ->  prototy
#if SPARE
                    match oloads with // if only one def then use it! refudge. TODO - replicated
                        | [(forwot, ((sty,fl_), item))] -> sty
                        | [] -> sf (mf() + sprintf " no overloads defined (L2459) for '%s'  prototy=%A" s prototy)
                        | items -> sf (mf() + sprintf " need disambig L2459 %s" (sfold bsv_tnToStr items)) // TODO call generic code
#endif
                
                | Some x -> sf (mf() + sprintf ": path prep other form '%s' %A" s x) 
            //vprintln 0 (sprintf "Path root id (pre strange discard step) gave %s on basis of id=%s" (bsv_tnToStr sty) s)
            (sty, uid, tbcc, h0)

        | KB_functionCall(wot, args, (lp, uid)) ->      // This will be used in cases such foo().bar
            let (asregf, display_tasko, forms, m_, scalar_function_name, lp, uid_, tbcc, h0) = typrep_functionCall ww bobj typecheck_exps q0 ee tbcc h0 (KB_functionCall(wot, args, (lp, uid)))
            let mm = (mf() + sprintf ": %A apply go asregf=%A" scalar_function_name asregf)
            if asregf then prep_ap stars tbcc h0 (Prov_asReg_asIfc mm) (hd args)
            else // see if has a _read method
                //let (ee, cc, sty) = typecheck_exp_as ww (ee, cc) asReg (KB_functionCall(wot, args, (lp, uid)))
                // when more than one form we need to typemeet their types ...
                let (asregf, _, forms, mf, _, lp, uid_us, tbcc, h0) = typrep_functionCall ww bobj typecheck_exps (mm:string) ee tbcc h0 (KB_functionCall(wot, args, (lp, uid)))
                if length forms <> 1 then cleanexit(lpToStr0 lp + ": overloaded function not supported here. " (*+ scalar_function_name*))
                let ol = forms
                match ol with
                    | [(Bsv_ty_nom(_, F_fun(rt, fats, fargs), sl), _, _)] ->
                       (rt, uid, tbcc, h0)
                    | _ -> sf (mm + sprintf ": Prep path: other function call form: ^%i forms : %A" (length ol) ol)
        | other -> sf(sprintf " typrep_path stars=%i other form %A" stars other)
    prep_ap 0 tbcc h0 asReg path



and lookup_function_type ww bobj ee mf fns = // one call site.
   let qm = "lookup_function_type"
   let rec typeprep_func8 = function
            | GE_overloadable(prototy, instances) -> 
                //vprintln 0 (sprintf " instances=%A scalar=%A" instances fns)
                if nullp instances then
                    [(prototy, None, None)]
                else
                    let hof_route = None // We do not currently support higher-order typeclasses but they could be added here...
                    let dig (forwot, ((sty, fl_), item)) = (sty, Some item, hof_route)
                    map dig instances

            | GE_binding(bound_name, w, EE_e(sum_ty, body))   ->
                //vprintln 3 (sprintf "Fetch (1) function binding %s w=%A" (htosp bound_name) w)
                [(sum_ty, Some body, w.hof_route)]

            | GE_binding(bound_name, w, EE_typef(_, sum_ty))  ->
                //vprintln 3 (sprintf "Fetch (2) function binding %s w=%A" (htosp bound_name) w)
                [(sum_ty, None, w.hof_route)] 

            | GE_normthunk(instf, idl, ans, reaped, _, ff) when not(nonep !ans) -> typeprep_func8(valOf !ans)
            | GE_normthunk(instf, idl, ans, reaped, being_done, ff) when not(nonep !being_done) ->
                //vprintln 3 ("want to make a recursion marker for " + htosp idl)
                [(Bsv_ty_dontcare("recursive-function:" + htosp idl), None, None)]
            | GE_normthunk(instf, idl, ans, reaped, being_done, ff) -> //
                let _ = if not (nonep !being_done) then sf (mf() + " recursion being_done L2052")           
                let knot2 = ref None
                being_done := Some(Bsv_ty_knot(idl, knot2))
                let env_updates = ff ww ee //  Do not remove since cannot support mutual recursion ! { ee with genenv=ee.genenv.Remover [hd idl]; } // 
                being_done := None
                reaped := env_updates
                let kate_f8 = function
                    | Aug_env(tyclass, bound_name, (dmtof, who, vale)) ->
                        knot2 := Some(ee_sty mf vale)
                        ans := Some(GE_binding(bound_name, who, vale))
                        ()
                    | other -> sf(sprintf "kate_f8 other '%s' %A" (htosp idl) other)
                let _ = app kate_f8 env_updates
                typeprep_func8(valOf_or_fail "answer should be present now" !ans) // Go round again.

            | other -> cleanexit(mf() + sprintf ": unexpected function form when typechecking function application (func8) %A" other)

   match ee.genenv.TryFind[fns] with
       | Some func -> 
           //vprintln 0 (sprintf "Typechecking method/functionCall '%s' - found a thunk or similar for its definition in env %A" fns func)
           typeprep_func8 func
        | None ->
            vprintln 0 (mf() + sprintf ": temp stop undefined function '%s'" fns)
            let varargs = [] // Denote varargs with the empty list for now
            let t = gec_func_sty ww fns varargs (Bsv_ty_dontcare "result-from-unknown-function")
            t


and gec_func_sty ww name args rt =
    let ty = Bsv_ty_nom({g_blank_nom_typ_ats with nomid=[name]; cls=Prov_func; }, F_fun(rt, {g_blank_fid with who="gec_func_sty"; fids=map get_fid2 (List.zip [1..length args] args); methodname=name; }, args), freshlister ww rt args) 
    [(ty, None, None)]



    
// NB: The type must be freshend by the caller.
// A prep function does some preprocessing on the AST used both for typecheck and norm phases.
and typrep_functionCall ww bobj typecheck_exps (qm:string) ee tbcc h0 arg =
    let vd = bobj.afreshen_loglevel
    //dev_println (sprintf "typrep_functionCall %A" arg)
    match arg with
    | KB_functionCall(KB_ast(KB_id("asReg", _)), args, (lp, uid)) ->
        let mf() = lpToStr0 lp + " KB_asReg call"
        let ww = if vd >=4 then WF 4 qm ww (mf()) else ww
        if (length args) <> 1 then sf "Precisely one argument needed for asReg"
        // Could conceivably implement this as a Reg wrapper around an abstract type arg!
        // TODO should check/bind arg agains Reg interface
        let ty = Bsv_ty_dontcare "prep_fcaller_asReg" // Actually an identity function of reg ifc to reg ifc
        let fats = {g_blank_fid with methodname= "asReg"; fids=["asregarg"] } 
        let sty = Bsv_ty_nom({g_blank_nom_typ_ats with nomid=["asReg"]; cls=Prov_func; }, F_fun(ty, fats, [ty]), [])
        (true, None, [(sty, None, None)], mf, Some "asReg", lp, uid, tbcc, h0)

    | KB_functionCall(KB_dk xs, args, (lp, uid)) ->
        let mf() = lpToStr0 lp + sprintf " prep system function call '%s'" xs
        let ww = if vd >=4 then WF 4 qm ww (mf()) else ww
        // Could conceivably implement this as a Reg wrapper around an abstract type arg!
        match op_assoc (xs) g_pliTasks with // Weak: this stops pli being higher order!
            | None -> cleanexit(mf() + " unsupported")
            | Some s ->
                let sty = Bsv_ty_dontcare "sysfcaller" // weak - we might well have some constraints in the pli function table.
                (false, Some s, [(sty, None, None)], mf,  Some xs, lp, uid, tbcc, h0)

    | KB_functionCall(KB_ast path, aargs, (lp0, uid)) -> // builtin function call or (newly) module app (do), but not an interface method call.
        let rec summary sl = function
            | KB_subscripted(dotflag, item, subs_lst, m_bitflag, lp) -> summary ("[..]"::sl) item
            | KB_tag(b, id, _) -> summary (id::sl) b
            | KB_id(s, (lp, _)) when sl=[] -> (lpToStr0 lp + " " + htosp ee.static_path + " " + qm + " prep functionCall + " + s, Some s, lp)
            | KB_id(s, (lp, _)) when sl<>[] -> (lpToStr0 lp + " " + htosp ee.static_path + " " +  qm + " prep functionCall + " + htosp(s::sl), None, lp)
            | other -> (lpToStr0 lp0 + " " + htosp ee.static_path + " functionCall name non-immediate ", None, lp0)
        let (mm, scalar_function_name, lp) = summary [] path
        let ww = WF 3 qm ww mm
        let mf() = mm // Shame.
        // PLI/display tasks should all be parsed in the KB_dk form? Check here too.
        let display_tasko = if nonep scalar_function_name then None else op_assoc (valOf scalar_function_name) g_pliTasks // Weak: this stops pli being higher order!
        if not_nonep display_tasko then sf ("hit old style PLI call " + mm)
        let (forms, tbcc, h0) =
            match scalar_function_name with
                | None ->
                    let (ty, _, tbcc, h0) = typrep_path ww bobj typecheck_exps (fun()->qm) ee tbcc h0 (Prov_asReg_asIfc "asReg-fcall-path2") path
                    match ty with
                        | _ -> ([ty, None, None], tbcc, h0)
#if FALSE_CLAUSE1
#if FALSE_CLAUSE                            
                        | Bsv_ty_methodProto(methodname, xr,  mth_provisos, proto, ifc_sty) when false -> // TODO make universally clear about this being the ifc_sty
                            let _ = "dontcheck arity"
                            let tag_id = methodname
                            let ty =
                                match ifc_sty with
                                | Bsv_ty_nom(idl, F_ifc_1 meths, _) ->
                                    match List.filter (cander_ifc tag_id) meths with
                                        | [] ->
                                            vprintln 0 ("L1759: Available components and methods in target interface are: " + sfold bsv_tnToStr meths)
                                            cleanexit(mf() + sprintf ": tag '%s' applied to interface %s that does not have a member of this name." tag_id (htosp idl))
                                        | [item] -> item
                                        | other ->
                                            cleanexit(mf() + sprintf ": tag '%s' applied to %s that seems to have more than one member of this name. " tag_id (htosp idl))
                                    
                                | other ->sf(mf() + sprintf "FAK1 other %A" other)
                            ([(ty, None, hof_route)], tbcc)
#endif
                        | Bsv_ty_nom(idl, F_fun(rt, fats, fargs), sl)  ->
                            fun_arity_check mf fats.methodname fargs aargs//dont need to check here as well
                            ([(Bsv_ty_nom(idl, F_fun(rt, fats, fargs), sl), None)], tbcc) // What exercises this clause please?
#if DISPOSEING_TY_FUN
//  This is being retyped with a brand
                        | Bsv_ty_methodProto(methodname, xr,  mth_provisos, proto, Bsv_ty_nom(idl, F_fun(rt, fats, fargs), sl)) -> // no: it is the method now - clause below not used.
                            [(Bsv_ty_nom(idl, F_fun(rt, fats, fargs), sl), None)]
#endif
                        | other -> sf(sprintf "%s: %s: other form encounted, cannot be applied: " qm mf() + bsv_tnToStr other)
#endif
                | Some name ->
                    let varargs = [] // Denote varargs with the empty list for now
                    // Autogenerate a function signature for the primitive functions.
                    if not_nonep display_tasko then (gec_func_sty ww name varargs g_ty_sum_void, tbcc, h0)
                    elif nonep scalar_function_name then sf(mf() + " temp stop on null fname")
                    else
                        match name with
(* 
  these builtins now have their signatures in the prelude if not their bodies...
                    let integer_ = Bsv_ty_integer
                    let alpha_ = Bsv_ty_id(VP_value(false, ["alphate"]), None, "alphate")
                    let numeric_ = Bsv_ty_id(VP_value(true, []), None, "alpha")                    
x                            | "pack"                          -> gec_func_sty [alpha] integer // Or better to return unspecified length bit vector.
                            | "unpack"                        -> gec_func_sty [integer] alpha
                            | "fromInteger"                   -> gec_func_sty [integer] alpha
                            | "message" | "error" | "warn"    -> gec_func_sty varargs unit
                            | "$format"                       -> gec_func_sty varargs g_string_ty
                            | "div" | "quot" | "mod" | "rem"
                            | "max" | "min" | "exp"           -> gec_func_sty [integer; integer] integer
                            | "zeroExtend" | "signExtend" | "extend" | "truncate"  -> gec_func_sty [integer] integer
                            | "abs"                           -> gec_func_sty [integer] integer
                            | "sizeof"
                            | "SizeOf" -> muddy "T SizeOf"  *)
                            | fns ->
                                let t = lookup_function_type ww bobj ee mf fns
                                (t, tbcc, h0)


        //vprintln 0 (mf() + sprintf ": prep: Returning maker_type as %A" maker_type)
        (false, display_tasko, forms, mf, scalar_function_name, lp, uid, tbcc, h0)

    | other ->
        sf (sprintf "typecheck KB_functioncall: other form %A" other)

//
// Perform preparatory transformations on a module signature, converting all AST type expressions to summary types.
//        
let prep_module_sig ww bobj idl mf m0 ee prec = function
    | KB_moduleSig(backdoors, mkname, params_o, iftype, args, provisos, lp) ->
        let ww = WF 3 m0 ww mkname
        let (args, iftype) = // Last one is exported interface when a list is given.
            match (args, iftype) with // combine the two possible syntaxes of moduleSig and insert 'Empty' if nothing at all is given.
                | ([], None)    ->    ([], KB_type_dub(false, "Empty", [], g_builtin_lp))
                | ([], Some i)  ->    ([], i)
                | (lst, None)   ->    (rev(tl(rev lst)), valOf(f2o3(hd(rev lst)))) // Complicated but does not ultimately do anything special? Discards formal name of exported i/f. But sometimes these are just prams!
                | _ -> sf "parser returned both styles of moduleFormalArgs"

                // There's a potential difference between a module with signature () and with no signature.
                // The () form in a module declaration is explicitly converted to (Empty) if using nominal types, or can be just the empty list of methods if using structural types.
                // Whereas a constant app, like mkPulseWire or mkRegU that takes no arguments can be instantiated either with or without parenthesis
                // owing to syntactic jiggery where the last argument on the rhs is stripped off and used as the lhs of the do. The further jiggery is
                // implemented in this compiler as inserting the unit application and for mkRegU and so on to have type "unit -> (ifc -> $M)".
                //
                //   1. Reg#(Bit#(12)) ureg_sans_paren <- mkRegU;
                //   2. Reg#(Bit#(14)) ureg_with_paren <- mkRegU();

        let konstantf = false // nonep params_o && nullp args
        let parameters = valOf_or_nil params_o
        let provisos' = List.fold (proviso_parse ee) [] provisos

        let ifc_sty = collect_sum1 ww bobj ee mf prec (VP_value(false, [])) iftype //TODO parameters ignored when making signature here ? or atleast revbinds in them

        let aliases = // We need the new names for formals that the user gave names to.
            let get_revbind cc = function
                | Tty((Bsv_ty_id(_, Some P_free, _)) as nv, fid) -> (fid, nv) :: cc
                | _ -> cc
            match ifc_sty with
                | Bsv_ty_id(_, prec, (fid, _)) ->
                    [(fid, ifc_sty)]
                    //muddy (sprintf "fid ifc_eval %s -> %A" fid (ee.genenv.TryFind[fid]))
                    
                | Bsv_ty_nom(_, _, ids) -> List.fold get_revbind [] ids
                | other -> sf (mf() + sprintf ": formal_pram_names other %A"  other)
        //vprintln 0 (mf() + sprintf " formal_pram aliases are %A" aliases)
        let (maker_type, pram_tys, arg_tys) =
            let mname = htosp("do"::idl)
            // Please explain why it is the do that has the body gamma as this next comment says ... well we process both ways on the rx site.
            let do_ty = Bsv_ty_nom({g_blank_nom_typ_ats with nomid=[mname]; cls=Prov_func}, F_fun(g_ty_sum_monad,  {g_blank_fid with fids=["foo"]; body_gamma=[](*inserted shortly in nf*); who="do_ty"; methodname=mname; fprovisos=provisos'}, [ifc_sty]), freshlister ww g_ty_sum_monad [ifc_sty])
            if konstantf then (do_ty, [], [(SO_none "module-konstantf", ifc_sty, "foo")])
            else
                let dprec = Some P_decl0
                let pram_tys = map (collect_sum2x ww bobj ee mf dprec) parameters
                let arg_tys = map (collect_sum2x ww bobj ee mf dprec) args                
                //vprintln 0 (mf() + ": module parameter types are " + sfold bsv_tnToStr pram_tys)
                //vprintln 0 (mf() + ": module arg types are " + sfold bsv_tnToStr arg_tys)                
                //let (pram_fids, arg_fids) = (map (make_fidder idl) prams_tys, map (make_fidder idl) args_tys)
                let tyls = map f2o3 (pram_tys @ arg_tys)
                let ty = Bsv_ty_nom({g_blank_nom_typ_ats with nomid=idl; cls=Prov_func}, F_fun(do_ty, {g_blank_fid with who="pram_fid"; fids=map f3o3 (pram_tys @ arg_tys); methodname=htosp(idl); fprovisos=provisos'}, tyls), freshlister ww do_ty tyls)
                (ty, pram_tys, arg_tys)
        //vprintln 0 (sprintf "%s: moduleSig konstant=%A\nparams_o=%A\nargs=%A\niftype=%A\nmaker_type=%s" (lpToStr0 lp + " " + mkname) konstantf params_o args  iftype (bsv_tnToStr maker_type))

        (mkname, backdoors, provisos, (pram_tys, arg_tys, iftype), (ifc_sty, maker_type, aliases), lp)



let prep_tagged_expr ww bobj ee = function
    | KB_taggedUnion(vtag, amembers, soloo, (lp, uid))
    | KB_taggedUnionExpr(vtag, amembers, soloo, (lp, uid)) ->
        let vd = bobj.afreshen_loglevel
        let mf() = lpToStr0 lp + ": taggedUnionExpr (tagged vtag=" + vtag + ") + uid=" + uid
        let ww = WF 3 "prep_tagged_expr" ww (if vd>=4 then mf() else "start")
        if not(nullp amembers) && not(nonep soloo) then sf (mf() + ": Both solo and assoc present 2/2")
        let rec has_such_tag c aa =
            match aa with
            | (Bsv_ty_nom _, _)   -> aa::c
            | (Bsv_ty_knot(idl, k2), _) when nonep !k2 -> aa::c // Knot not yet tied - presume it will get tied correctly shortly.
            | (Bsv_ty_knot(idl, k2), _) -> has_such_tag c (valOf !k2, -1)
            | (other, _) ->
                vprintln 0 ("+++ has_such_tag: ignored other form: " + sprintf "%A" (bsv_tnToStr other))
                c
        let possible_types =
            match ee.ctor_tags.TryFind vtag with
                | None -> []
                | Some x ->
                    //vprintln 0 (sprintf "tags related : Found %A" x) //(sfold bsv_tnToStr x))
                    List.fold has_such_tag [] x
        (mf, vtag, possible_types, uid)

let prep_spa mx aa =
    let (uid, ttags, lp) =
        match aa with
        | KB_structPattern(strlst, (lp, uid)) -> (uid, map fst strlst, lp)
        | KB_structExprPositional(ttag1, _, (lp, uid))        
        | KB_structExprAssociative(ttag1, _, (lp, uid)) -> (uid, [ttag1], lp)

    let mf() = lpToStr0 lp + ": " + mx + " structExprAssociative/Positional tname=" + sfold (fun x->x) ttags
    (uid, ttags, mf)

let rec tagarity mf = function
    | Bsv_ty_knot(idl, k) when not(nonep !k) -> tagarity mf (valOf !k)
    | Bsv_ty_nom(idl, F_tunion(mrecflag, variants, details_), args) -> Some 1 // structs have more than one to chose between
//                match op_assoc ttag variants with
//                    | Some(tyo, no) ->
//                    | None -> (sprintf "tag '%s' missing on second lookup" ttag)
    | x -> muddy (mf() + ": tag arity other: " + bsv_tnToStr x)


let provisos_needed = function
    | KB_op  V_times | KB_op V_divide | KB_op V_exp -> [Prov_si "Arith"]

    | KB_op V_mod | KB_op  V_plus   | KB_op V_minus -> [Prov_si "Arith"]

    | KB_op (V_rshift signed_) ->  [Prov_si "Bitwise"]
    | KB_op V_lshift  | KB_op V_neg     | KB_op V_onesc ->  [Prov_si "Bitwise"]     


    | KB_op  V_bitand | KB_op V_bitor  | KB_op V_xor ->   [Prov_si "Bitwise"] // really?
   
    | KB_op  other ->  sf(sprintf "other x_diop %A" other)


    | KB_bop V_orred
    
    | KB_bop V_deqd   | KB_bop V_dned ->  [Prov_si "Eq"]

    | KB_bop (V_dged signed_)  | KB_bop (V_dltd signed_)
    | KB_bop (V_dled signed_)  | KB_bop (V_dgtd signed_) ->  [Prov_si "Ord"]


    | KB_ab V_band //  bool*bool->bool
    | KB_ab V_bor
    | KB_ab V_bxor -> [Prov_si "Logical??"]


// The type of a function application becomes the return type under auto_apply to unit (as for register read method etc).
let unitapp mf = function
    | Bsv_ty_nom(_, F_fun(rt, _, []), _) -> rt
    | Bsv_ty_nom(_, F_actionValue rt, _) -> rt
    | Bsv_ty_nom(ats, _, []) when ats.nomid=["ActionValue"] -> muddy "action rt  with arg!  "    

    | other -> cleanexit(mf() + "unitfun or action value expected as rhs of do <- not : " + bsv_tnToStr other)
    
let apparg mf = function
    | Bsv_ty_nom(_, F_fun(monad_void_type__, _, [arg]), _) -> arg // TODO check rt is a monad infact.
    | other -> cleanexit(mf() + "a function that takes a single interface argument expected on rhs of do <- not : " + bsv_tnToStr other)


//
let tynorm_schedule ww bobj tycheckf site arg = 

    match arg with
        | KB_scheduleOrder(lst, tk) ->

            muddy "sdd - scheduleOrder - can be used to partially populate matrix fully rezzed in the next clause"

        | KB_scheduleMatrix(matrix_string, tk) -> // DJG extended Bluespec syntax.
            let error tk msg =
                cleanexit(lpToStr0 (fst tk) + ":" + site + ": " + msg + sprintf " in '%s'"  matrix_string)
        
            let validate_schedule_matrix_entry arg =
                if not (memberp arg g_allowable_schedulling_tokens) then error tk (sprintf "Matrix entry '%s' unrecognised" arg)
            let items = split_string_at [ ';' ; ':' ] matrix_string
            if length items <> 2 then error tk ("two regions separated by a colon expected")

            let methods = split_string_at_whitespace_and_trim (hd items)
            let matrix =  split_string_at_whitespace_and_trim (cadr items)
            let n_methods = length methods 
            if length matrix <> n_methods * n_methods then error tk (sprintf "Expected %i items in matrix for %i methods not %i" (n_methods * n_methods) n_methods (length matrix))
            app validate_schedule_matrix_entry matrix
            (tk, methods, matrix)


        | _ -> sf "L3066 other"

// Add an interface and all its  methods (currency thereof) to the currency map under a fresh inode.    
let sigma_add_methods_core site hint_idl inode sigma contents =
    let vd = 3
    let hintname = hptos hint_idl

    if vd>=4 then vprintln 4 (sprintf "sigma_add_methods_core: site=%s Saving %i methods under inode %A as for hintname=%s" site (length contents) inode hintname)
    let addsig2 (mm:methmap_t) arg =
        match arg with
            | Sigma((kind, _, fname) as id3, resats, plop, pip) ->
                if vd>4 then vprintln 4 (sprintf "sigma_add_method: concrete fname=%s  inode=%A" fname inode)
                mm.Add((inode, fname), arg)
            | Sigma_e(fname, rt, proto, formals, callee_explicit_condition, actions, lp) ->  // Ephemeral methods do not have concrete currency, but they do have a sigma entry.
                if vd>=4 then vprintln 4 (sprintf "sigma_add_method: ephemeral fname=%s  inode=%A" fname inode)
                mm.Add((inode, fname), arg)
            
    let methmap = List.fold addsig2 sigma.methmap contents
    { sigma with methmap=methmap }

let sigma_add_methods site hint_idl inode sigma currency =
    let unwrap = function
        | (_, Tfa(gb_, sigma_items)) -> sigma_items // This Tfa form is pointless. Delete it.
    let contents = list_flatten(map unwrap currency)
    sigma_add_methods_core site hint_idl inode sigma contents
    
// This is the  B_field_select as an hexp_t operator.
let lower_field_select baser width arg = ix_mask Unsigned width (ix_rshift Unsigned arg (xi_num baser))
   
// =====================================================================================
// >>This is the start of the main set of mutually recursive functions that implement the compiler.
//


// Bluewire operations on RWire and friends.
// They will be encountered in an un-sorted order on the zeroth/first pass for ordering constraint noting
// and it is not until the second pass that they should be in the correct order for effects to be useful.
let rec bluewire_operation ww bobj pass mf ee sigma10 g11 (kind, iname, fname) ty actuals firex =
    let ww = WF 3 "bluewire_operation" ww (sprintf "Start %s" fname)
    //if length iname00 < 2 || fname <> hd iname00 then sf (sprintf "bluewire_operation: malformeid iname00=%s for fname=%s" (hptos iname00) fname)
    //let iname = tl iname00    // Delete fname from instance name to get iname of the wire.
    //let vprintln 3 (sprintf "bluewire_operation:  iname00=%A  fname=%s" iname00  fname)
    let resname = (IIS (hptos iname), fname)
    let (writef, arg_o) =
        match fname with
            | "send"   ->  (true, None)
            | "wset"  
            | "_write" ->
                if length actuals <> 1 then sf(mf() + sprintf ": bluewire: expected one argument to method name %s applied to %s." fname (hptos iname))
                (true, Some(hd actuals))

            | "wget" 
            | "_read" -> (false, None)

            | fname -> sf(mf() + sprintf ": bluewire: bad method name %s applied to %s of kind %s" fname (hptos iname) (hptos kind))

    let (priorf, ov_grd, ov_vale) = 
        match sigma10.bluewires.TryFind iname with
            | Some(MS_bluevale(grd, vale)) ->
                //dev_println(sprintf "Previous bluewire entry found for %s: grd=%A vale=%A" (hptos iname)  grd vale)
                (true, grd, vale)
                //if not writef && myfalse grd then hpr_yikes(sprintf "Likely error in bluewire execution order: write of valid=false found when %s attempted for %s" fname (hptos iname))
            | _ ->
                if not writef && pass.pass_no=2 then hpr_yikes(sprintf "Likely error in bluewire execution order: no writes found when %s attempted for %s" fname (hptos iname))
                (false, X_false, None)

    let unsafef =
        match hd kind with
            | "UnsafeWire"
            | "UnsafeRWire"
            | "UnsafePulseWire" -> true
            | _ -> false

    let (blockingf, rv) =
        if writef then
            // Can bluewire writes block?
            (false, B_hexp(ty, X_undef))
        else
        match hd kind with     // We implement different semantics according to kind.
            | "UnsafeRWire"   // The RWire returns the maybe type
            | "RWire" ->
                let b_valid = B_hexp(g_bool_ty, xi_blift ov_grd)
                let datawidth = enc_width_no ww ee (fun()->"bluewire width") ty
                let top = (Bsv_ty_integer, B_shift(b_valid, datawidth)) // Construct a Maybe expression.
                let bot = (ty, valOf_or ov_vale (B_hexp(ty, X_undef)))
                let v0 = gen_B_diadic bobj ww mf (V_bitor) ty (top, bot)
                (false, v0)

            | "PulseWire" -> // Return a Bool which is the guard and is always ready.
                (false, B_hexp(g_bool_ty, xi_blift ov_grd))

            | "DWire"  // DWire default value is a 'numeric' type parameter inside ty.
            | "PulseWireOr"
            | "UnsafePulseWire"
            | "UnsafePulseWireOr" ->
                muddy (sprintf "bluewire_operation: need bluewire type %s" (hptos kind))

            | "UnsafeWire"
            | "Wire" ->
                // The Wire type returns the value or else blocks the intrinsic guard.
                let rv = (ty, valOf_or ov_vale (B_hexp(ty, X_undef)))
                (true, snd rv)

            | other -> sf (sprintf "bluewire operation: not (yet) supported kind %s" other)

    vprintln 3 (sprintf "bluewire_operation kind=%s  iname=%s fname=%s writef=%A blocking=%A priorf=%A" (hptos kind) (hptos iname) fname writef blocking priorf)
            
    if not writef && blockingf && myfalse ov_grd then hpr_yikes(sprintf "Likely error in bluewire execution order: manifestly false valid status will cause surrounding rule never to fired. Method %s attempted for %s.  TODO print rule name and elaborator callstack. " fname (hptos iname))

    // When blockingf we conjunct negation ov_grd into the intrinsic guard.  Flag warning/error if manifestly false on second pass.
    let g11 =
        if blockingf then
            let token = (false, [funique  "bluewire-block-token"])
            let resats = 
              {
                  actionf=                         false
                  can_be_ignored_for_schedulling=  false
                  idempotentf=                     false
                  specialmodel=                    SM_bluewire
              }        
            let grd = IG_resource(resname, htosp iname, ov_grd, resats)
            if pass.pass_no=2 && myfalse ov_grd then hpr_yikes(sprintf "bluewire operation %s %s is never ready when read and blocks the rule/thread." (hptos iname) (fname))
            { g11 with igs=g11.igs.Add(token, grd) } // If the read is blocking we augment the intrinsic arg in g11
        else g11
    
    
    let eoc =
      {
        kind=      kind
        ekey=      iname
        fname=     fname
        unsafef=   unsafef
        orderer=   RO_write_before_read
        writef=    writef
      }

    let sigma10 = { sigma10 with exordering= eoc::sigma10.exordering }


    // TODO check pulseWire OR semantics - do we block on write if already set and not an OR-ing operation?
    let sigma10 =
        if writef then

            let nv_content = if nonep arg_o then None else Some(snd(valOf arg_o)) // We save just the value of the type/value pair.

            let nv =
                if not priorf then MS_bluevale(firex, nv_content)
                else
                    let nvx =
                        if nonep arg_o then None
                        else
                            let ncp = extend_callpath bobj "L3677a" "bluewire-mux" ee.callpath
                            Some(gen_B_query ww bobj (false, B_bexp(firex), valOf nv_content, valOf_or_fail "L3678" ov_vale, ty, (g_builtin_lp, ncp)))
                    MS_bluevale(ix_or ov_grd firex, nvx)
            //dev_println(sprintf "Bluewire write %s: guard=%s with %A" (hptos iname)  (xbToStr firex) nv)
            { sigma10 with bluewires=sigma10.bluewires.Add(iname, nv) }
        else sigma10
    let denotgrd = X_true 
    (sigma10, g11, Some(denotgrd, rv)) // end of bluewire_operation



// For a type meet between a formal and an actual and then eval.  Could do it the other way around?  TODO show equivalent.
and newsum_arg ww (bobj:bsvc_free_vars_t) (mf:unit->string) ee log (tbcc, h0) (ll, rr) =
    let vd = bobj.dropping_loglevel
    let ww = (WN "newsum_arg" ww)
    let (h0, ans) = newsum_arg_base ww bobj mf ee log (h0) (ll, rr)
    let ans' = ty_eval_generic ww bobj ee mf (Some h0) ans
    if vd>=4 then vprintln 4 (sprintf "newsum_arg: base convert %s ---> TO ---> %s" (bsv_tnToStr ans) (bsv_tnToStr ans'))
    (tbcc, h0, ans')

// _temp variant discards answer but accumulates bindings in h0
and newsum_arg_temp ww (bobj:bsvc_free_vars_t) mm ee log (tb, h0) (ll, rr) =
    let (tb, h0, _) = newsum_arg ww (bobj:bsvc_free_vars_t) mm ee log (tb, h0) (ll, rr)
    (tb, h0)
    
and simple_store_type_annotation (ee, bobj) ww genenv site mm tbcc h0 ncp styx =
    store_type_annotation (ee, bobj) ww genenv site "" mm tbcc h0 ncp (Tanno_function(styx, [], [], []))   

//
// store_type_annotation: this updates the global g_type_annotates database of type annotations.
//
//       


and store_type_annotation (ee0, bobj0) ww (em:genenv_map_t) site id (mf:unit->string) tbcc h0 ncp arglog = 
    let ww = WN "store_type_annotation" ww
    let vd = bobj0.dropping_loglevel
    let rec dedropper_for_hof_aka_spogger ww depth gf tbcc h0 path dropping =
        let ww = WN "store_type_annotation_spogger" ww
        let m_possible_envs = ref [ (em, tbcc, h0) ] // Start off with surrounding env.
        let rec sc sofar_ = function
            | (id, recf, None)::tt -> sc ((id, recf, None)::sofar_) tt
            | [] ->
                vprintln 0 "+++ Marked yet end reached" // We should not be called if no marker
                (false, tbcc, h0)
            | [item] -> sf "solo hoftagged path element!"
            | (id1, _, Some(hoftokens_, evty_i))::(id2, _, _)::ttail ->
                if vd>=4 then vprintln 4 (mf()  +  sprintf ":%s: want to despog id1=%s id2=%s" site id1 id2)
                //vprintln 3 (sprintf "spogs on list are %A" (map spogToStr !g_spogs))
                let mapped_fid1 =  // We map the tagged formal name to the user's formal name.
                    match dropping with
                        | Tanno_hof(rt_, _, hofpaths, args_) ->
                            match List.filter (fun (a,b)->a=id1) hofpaths with
                              | [] -> sf (sprintf "depth=%i, a mapping is missing (L191) for %s\nMappings=%A\nTokens_=%A" depth id1 hofpaths hoftokens_)  
                              | _::_::_ -> muddy "multispog"
                              | [(_, id1a)] ->
                                  dev_println (sprintf "sc:   mapped %s to %s" id1 id1a)
                                  if vd>=4 then vprintln 4 (sprintf "sc:   mapped %s to %s" id1 id1a)
                                  id1a
                        | _ -> // This means there is no intra-body hof passing - the function provided came globally or was an anon lambda if BSV had such a form in its src code.
                            vprintln 3 (sprintf "no mappings present - %s unmapped owing to" id1 + droppingToStr dropping)
                            id1

                let pogscan1 cc = function // We find the set of all functions potentially passed via this formal.
                    | SPOG(_, called_idl, mmp, pathtag, formal_name_i1) when pathtag=id2 && formal_name_i1=mapped_fid1 ->
                        dev_println (sprintf "possible_envs: pogscan1: Add possible env 1/2 for formal_name_i1=%s pathtag=%s called_idl=%s" formal_name_i1 pathtag (hptos called_idl))
                        mutadd m_possible_envs mmp
                        (id1, called_idl, mmp) :: cc // prefer singly_add but mm is non-equality type
                    | SPOG(_, called_idl, mmp, pathtag, formal_name_i1) ->
                        dev_println(sprintf "possible_envs: pogscan1: Did not add: pathtags %s <> %s or formal names %s <>%s" pathtag id2 (formal_name_i1) (mapped_fid1))
                        cc
                    | _ -> cc
                let potential_pogs = List.fold pogscan1 [] !g_spogs
                dev_println(sprintf "%i pogs are relevant out of %i" (length potential_pogs) (length !g_spogs))
                let _ =
                    if nullp potential_pogs
                    then vprintln 0 (mf() + sprintf " +++ Seemingly there are no HOF functions passed through this formal site. mapped_fid1=%s id2=%s" mapped_fid1 id2)
                    else vprintln 3 (mf() + ": despog these functions: " + sfold (fun (i1, idl, mm) -> i1 + ":" + htosp idl) potential_pogs)
                //vprintln 0 (sprintf "despog further: kings rawmaterial=%s"  (droppingToStr dropping))

                let base_genenv = evty_lookup evty_i
                let mm1 = mf()//base_genenv
                match dropping with
                    | Tanno_function(rt, _, _, args)
                    | Tanno_hof(rt, _, _, args) ->                               
                    //| Tannot([fidid, Bsv_ty_nom(_, F_fun(rt, mats, args), nargs)]) ->
                        let atypes = rt::(map snd args)
                        let rec recursive_get_hoffed evdepth cd (i1, idl, (mm3, cc3, h3)) =
                            if length idl = 1 then
                                    let pogscan2 cqc = function // We need to recurse to track inter-hof function calls.
                                        | SPOG(_, called_idl2, mm4p, pathtag, fid) when fid=hd idl ->  // This is not a loose match since we know idl has length unity.
                                            dev_println (sprintf "possible_envs: pogscan2: Add possible env 2/2 for %s  (already %i)" (hptos idl) (length !m_possible_envs))
                                            mutadd m_possible_envs mm4p
                                            (i1, called_idl2, mm4p) :: cqc // prefer singly_add but mm is non-equality type
                                        | SPOG(_, called_idl2, mm4p, pathtag, fid) ->
                                            dev_println (sprintf "pogscan2: missed match on fid=%s pathtag=%s called_idl2=%s when checking fid = hd %s" (fid) pathtag (hptos called_idl2) (hptos idl))
                                            cqc
                                        | _ ->
                                            cqc
                                    let idl2s = List.fold pogscan2 [] !g_spogs
                                    dev_println (sprintf "pogscan2 evdepth=%i done: %i further spogs" evdepth (length idl2s))
                                    if vd>=3 then vprintln 3 (mf() + sprintf ": for %s further despog these functions: " (htosp idl) + sfold (fun (i1, idl, mm) -> i1 + ":" + htosp idl) idl2s)
                                    List.fold (recursive_get_hoffed (evdepth+1)) cd idl2s
                            else
                                dev_println (sprintf "Call to %s has no hof args (despite call itself being hof)." (hptos idl))
                                (evdepth, i1, idl, (mm3, cc3, h3)) :: cd
                            
                        let deep_spogs = List.fold (recursive_get_hoffed 0) [] potential_pogs 
                        dev_println (sprintf "number of deep_spogs is %i  " (length deep_spogs) + (sfold (f3o4>>hptos) deep_spogs))
                        let retrieve cc (evdepth, i1, idl, (mm5, cc5, h5)) = 
                            let bv = 
                                match (mm5:genenv_map_t).TryFind idl with
                                    | None ->
                                            if vd>=9 then vprintln 9 ("worry not :  Function (used as higher-order arg) was not found using full name: " + htosp idl)
                                            match mm5.TryFind [hd idl] with
                                                | None -> cleanexit (mf() + ": Function (used as higher-order arg) was not found: " + htosp idl)
                                                | Some x -> x
                                    | Some x -> x
                            match bv with
                                | GE_binding(_, _, EE_typef(_, Bsv_ty_nom(nst, F_fun(rt, mats, args), nargs)))
                                | GE_binding(_, _, EE_e(Bsv_ty_nom(nst, F_fun(rt, mats, args), nargs), _)) ->                                    
                                    if nst.nomid <> idl then sf ("Found wrong function (another of the same name) " + htosp idl + " cf " + htosp nst.nomid)
                                    //vprintln 0 (sprintf "despog to hyd and drop are %A" mats.body_gamma)
                                    (evdepth, i1, idl, Bsv_ty_nom(nst, F_fun(rt, mats, args), nargs))::cc
                                | other ->
                                    vprintln 0 (sprintf "deepspog other form %A" other)
                                    sf ("deepspog other for " + htosp idl)

                        let for_dropping = List.fold retrieve [] deep_spogs
                        let make_hof_function_droppings (tbcc, h0) (evdepth, i1, idl, Bsv_ty_nom(nst, F_fun(rt, mats, args), nargs)) =
                            let ww = WF 3 "despog" ww ("make hof droppings for function " + htosp idl)

                            let evty_l1 ty =
                                let ln = length !m_possible_envs 
                                let do_toy ((mm_t:genenv_map_t, cc_t_, hh), nn) = 
                                       let ee_t = { ee0 with genenv=mm_t } 
                                       let ansf = ty_eval_generic (WN "despog-evty-l1" ww) bobj0 ee_t (fun ()->"despog-evty-l1") (Some hh)
                                       let ans = ansf ty
                                       let a0 = bsv_tnToStr ty
                                       let a1 = bsv_tnToStr ans
                                       if a0=a1 || leaf_grounded_type_pred ty then () else vprintln 3 (sprintf "%i/%i xx1 despog evty ok ? runran %s -> %s" nn ln a0 (if a0=a1 then "SAME" else "DIFFERENT:" + a1))
                                       ans
                                let answers = map do_toy (List.zip !m_possible_envs [0..(ln-1)])
                                match answers with
                                    | [] -> sf "no evty_l1 answers"
                                    | _ ->
                                        if ln <> 1 then hpr_yikes(sprintf "evty_l1: more than one possible_env: %i" ln)
                                        hd (rev answers) // This is seemingly an arbitrary selection of one possible answer. TODO explain.  We take the last one added. Not the initial default.

                            let atypes = map evty_l1 atypes

                            let pairs =
                                let fillers = map (fun x -> "$filler") atypes
                                List.zip (List.zip3 fillers (rt::args) ("$RT"::mats.fids)) atypes

                            //dev_println  (sprintf "Want to make new cc assocs now based on ILL %s" (sfold (fun ((_, lt, _), rt) -> bsv_tnToStr lt + " for " + bsv_tnToStr rt) pairs))
                            let (tbcc, h0) =
                                let make_a_hof_unification (tbcc, h0) ((_, lt, _), rt) =
                                    //vprintln 0 (sprintf " make_a_hof_unification: new cc assocs from HOF " + bsv_tnToStr lt + " for " + bsv_tnToStr rt)
                                    let (h0, a0_) = sum_type_meet ww bobj0 mf h0 (lt, rt)
                                    (tbcc, h0)
                                List.fold make_a_hof_unification (tbcc, h0) pairs
                            let evty_f = // Form a custom type evaluator function to be applied to each dropping.
                                let trips = []
                                let aliases = []
                                let mm = grave_deepbind_arg_ty_list ww vd mf None aliases (em:genenv_map_t) pairs trips // TODO? start with Empty

                                //mutadd m_possible_envs mm
                                //TODO need to freshen etc... ?
                                // TODO this binding also makes further cc unifications
                                
                                let evty_ya ww ty =
                                    let ln = length !m_possible_envs   
                                    let do_toy ((mm_t____:genenv_map_t, cc_t, h_t), nn) = 
                                       let ee_t = { ee0 with genenv=mm } 
                                       let ansf = ty_eval_generic (WN "despog-evty-l2" ww) bobj0 ee_t (fun ()->"despog-evty-l2") (Some h_t)
                                       let ans = ansf ty
#if SPARE
                                       let _ =
                                           let a0 = bsv_tnToStr ty
                                           let a1 = bsv_tnToStr ans
                                           if a0=a1 || leaf_grounded_type_pred ty then () else dev_println (sprintf "%i/%i xx2 despog evty ok ? runran %s -> %s" nn ln a0 (if a0=a1 then "SAME" else "DIFFERENT:" + a1))
#endif
                                       ans
                                    let answers = map do_toy (List.zip !m_possible_envs [0..(ln-1)])
                                    hd answers // TODO this is an arbitrary selection - need to just run this once, correctly.
                                evty_ya

                            let newprefix = (i1, 0, None)::(id2, 0, None) :: ttail
                            //vprintln 0 ("despoging under newprefix " + htosp (map f1o3 newprefix))
                            let new_call_prefix = CPS(gf, newprefix) // new call prefix
                            let spog_pushdown_gamma (id, bg_cp, dp) = // hof function call/apply use of pushdown.
                                let dp' = dropapp (evty_f ww) dp                            
                                if vd>5 then
                                    vprintln 5 (sprintf "hof_pushdown : id=%s extend newtop=%A with item cp=%A" id (cpToStr new_call_prefix) (cpToStr bg_cp))
                                let cp' = join_cp bobj0 "new_hof_pushdown" new_call_prefix bg_cp // Join together this call context with the annotation's existing context.
                                (id, cp', dp')

                            let spogged = map spog_pushdown_gamma mats.body_gamma // The annotations of the invoked function are regenerated for the current call context.

                            let (tbcc, h0) =
                                let ww = WF 3 "despog" ww ("store hof " + htosp idl)
                                let recursive_annotate (tbcc, h0) (id, cp, dp) =
                                    let newie = store_annotation ww bobj0 em (depth+1) (site + sprintf "_recurse%i" depth) id mf tbcc h0 cp dp
                                    newie
                                List.fold recursive_annotate (tbcc, h0) spogged 
                            (tbcc, h0)
                        let (tbcc, h0) = List.fold make_hof_function_droppings (tbcc, h0) for_dropping
                        (false, tbcc, h0) // spog_instead -  we need false here otherwise mandrake has nothing to pull.

                    | other ->
                        (false, tbcc, h0)

        sc [] path

    and store_annotation ww bobj (em:genenv_map_t) depth site id mm tbcc h0 ncp dropping =
        let ww = WN "store_type_annotation" ww
        match ncp with
            | CP_none s -> muddy ("storx with no callpath " + s)

            | CPS(gf, path) ->
                let cps = if id="" then funique "GAMG" else id
                let marked = gf <> PS_suffix && disjunctionate (fun (id, recf, hofo) -> not(nonep hofo)) path
                let kpath = make_dropping_key ncp
                let marker = if marked then " despogging\n >>>>>>>>>>>>>>>>>>>>>>" else ""
                if bobj.dropping_loglevel >= 4 then vprintln 4 ( mf() + sprintf ":%s: depth=%i: Storing type annotation %s %s: %s: %s" site depth marker cps kpath (droppingToStr dropping))
                let (tbcc, h0) =
                    if gf = PS_suffix then (tbcc, h0)
                    else
                        let (spog_instead, tbcc, h0) =
                            if marked then dedropper_for_hof_aka_spogger ww depth gf tbcc h0 path dropping//to save time only call if marker present TODO
                            else (false, tbcc, h0)

                        if spog_instead then (tbcc, h0)
                        else 
                            let (found, ov) = g_type_annotates.TryGetValue kpath
                            if found then
                                vprintln 3 (sprintf "site=%s: Type anno strangely dropped already for %s depth=%i\n    OV=%s\n    NV=%s" site (kpath) depth (droppingToStr (snd ov)) (droppingToStr dropping))
                                (tbcc, h0)
                            else
                                if not spog_instead then g_type_annotates.Add(kpath, (cps, dropping))
                                else vprintln 0 ("Did not drop since spogged " + (cpToStr ncp) + "  " + (droppingToStr dropping))
                                (tbcc, h0)
                ({ tbcc with tve= (cps, ncp, dropping)::tbcc.tve }, h0) // TODO not to return augmented if spog_instead happened - holdon SPOG has serious cc bindings
    let ans = store_annotation ww bobj0 (em:genenv_map_t) 0 site id mf tbcc h0 ncp arglog
    ans
    
// rehydrate ...
// Tension to freshen with kickoff or rehydrate or freshen without kickoff and then make deepbindings.
and ty_eval ww bobj ee mm aa = // This function should not be called from anywhere except pushdown when a concrete call path is present  ... please delete old sites - No now we use it on abstract callpaths during type checking too, where the binding is in hindley.
    ty_eval_generic ww bobj ee mm None aa

and ty_eval_generic ww bobj ee (mf:unit->string) hoption aa = // This function can be called with a hindley or an ee - is dual use

    //vprintln 0 (mf() + ": Start ty_eval_generic " + bsv_tnToStr aa)

    let named_items = ref [] // TODO what if used more than onece -- TODO need squirrel

    let rec evaler_xo ee stack = function
        | (x, None)     -> (x, None)
        | (x, Some sty) -> (x, Some(evaler ee stack  (SO_none "tys471") sty))
    and evaler_x ee stack = function
        | (x, sty) -> (x, evaler ee stack  (SO_none "tys9823") sty)

    and tidf_bind_so ee stack so arg =
        let pnumberf = (so = SO_numeric)
        match arg with
        | Tid (vp, typen) ->
            //if pnumberf then vprintln 0 (sprintf "slfun %s perhaps whack222 %A" typen vp)
            let nf = vp // was VP_none "tidf-bind-default" // TODO check me - get from vp please ...
            //let ty_eval_s__ ww ee mf so nf typen = lookup_type ww bobj ee mf so ("ty_eval_s", typen, nf, None, g_builtin_lp)
            // NOTE - THIS VARIANT DID NOT IMPERIALISE BEFORE. STOPPED WITH false ARG
            let def = Bsv_ty_id(vp, None, (typen,  (*fresh_bindindex*) "-9"))
            let ans = ty_eval_s ww "tidf_bind" typen ee mf so def false nf typen
            //vprintln 0  (sprintf "tid eval (knicks) '%s' -> %A" typen (bsv_tnToStr ans))
            if typen = "a35ty10" then
                vprintln 0 "+++ Kludge special test a35ty10" // TODO delete this!
                Tty(def, typen)
            else Tty(ans, typen)

        | Tty(ty, fid) ->
            //if pnumberf then vprintln 0 (sprintf "slfun %s perhaps whack2444 %A" fid so) 
            let ty' = evaler ee stack so ty 
            Tty(ty', fid)

    and ty_eval_imperialise aa =
        match aa with
        | Bsv_ty_int_vp vp ->
            match vp with
                | VP_value(_, []) -> aa // Synonymous with Bsv_ty_integer ... perhaps refactor so that there is only one rep : TODO.
                | VP_value(b_, fidl) ->
                    let bail m1 = // Imperial bail error site
                        //vprintln 0 (sprintf "bail on env ty_eval %s.  env=" (bsv_tnToStr aa))
                        //for z in ee.genenv do ignore(vprint(0, sprintf " key=%s " (htosp z.Key)))  done 
                        //vprintln 0 ""
                        //vprintln 0 (m1 + sprintf "+++ty_eval imperial want '%s' from  " (sfold (fun x->x) fidl)  + (bsv_tnToStr aa))
                        //muddy ("missing bail " + m1)
                        aa // for now

                    let imperialise cc fid =
                            match ee.genenv.TryFind [fid] with
                                | None -> cc
                                | Some(GE_binding(_, _, EE_typef(_, ty))) ->
                                    match getival (fun()->"imperial: " + fid) [ty] with
                                        | None   ->
                                            vprintln 3 (sprintf "imperial: ival not present yet fid=%s ty=%s" fid (bsv_tnToStr ty))
                                            (fid, aa)::cc
                                        | Some n -> (fid, mk_intansi (xi_num n)) :: cc

                                | Some(GE_binding(_, _, EE_e(_, vale))) -> // As found mkRegFile
                                    vprintln 0 (sprintf " vale == %A" vale)
                                    match vale with
                                         | B_hexp(_, X_undef) ->
                                             vprintln 0 ("imperial undef constant being provided (1) for fid=" + fid)
                                             (fid, aa) ::cc
                                         | vale ->
                                             let v = bsv_manifest ww bobj (fun()->"imperial constant being provided (1) for fid=" + fid) vale
                                             (fid, mk_intansi (local_xi_bnum v)) :: cc

                                | Some x -> muddy (sprintf "imperial %s -> %A" fid x)
                    match List.fold imperialise [] fidl with
                            | [item] -> snd item
                            | [] -> bail "no items"
                            | items ->
                                sf (mf() + sprintf " +++ imperial AnnoInt multiple items  %s" (sfold (fun (x, y)->x + " :::" + bsv_tnToStr y) items))
                                aa
        | other  -> aa


    and ty_eval_s ww site debug_arg ee (mf:unit->string) so def impf nf typen =
        let tailer () =
            match ee.genenv.TryFind [typen] with
                    | Some x_ -> // Strangely  x is ignored and we look up again.  TODO explain...
                        //dev_println (sprintf "swold: strange ignore x of %A when lookup %s again" x_ typen)
                        let ans = lookup_type ww bobj ee mf so ("ty_eval_s(2b)", typen, nf, None, g_builtin_lp(*yuck*))
                        ans


                    | None -> 
                        match nf with
                            | VP_value(true, fidsl) when not(nullp fidsl) && fidsl <> [""] -> // somewhat crude again
                                let y = (Bsv_ty_int_vp nf)  
                                if impf && nonep hoption then ty_eval_imperialise y else y  //<------------------ too many call sites locally TODO.
                            | other ->
                                let debug_utv = false
                                let _ =
                                    if debug_utv then
                                        let r = ref []
                                        let vd = 0
                                        vprint vd "In-scope tags are: "
                                        for z in ee.ctor_tags do ignore(vprint vd (z.Key + " ")) done 
                                        vprintln vd ""
                                        for z in ee.genenv do mutadd r z.Key done 
                                        vprintln vd (mf() + sprintf ": %s arg=%s L6817: Type identifier '%s' was not found in the env. All=" site debug_arg typen + sfold_unlimited htosp !r)
                                        // Should also render tbcc/h0 here for completeness.
                                        ()
                                    else vprintln 3 (mf() + sprintf ": %s arg=%s used free type variable '%s' (L6817 nf=%A)" site debug_arg typen nf)
                                def

        let ans = // First lookup is in tbcc/h. Then tailer looks in ee.
            match hoption with
                | None -> tailer ()

                | Some (h) ->
                    let rec lookie = function
                        | [] ->
                            //vprintln 3 ("Not found in hindley option " + typen)
                            tailer()
                        | (tyo, xl, _)::tt when memberp [typen] (map fst xl) -> // TODO use more efficient lookup
                             match tyo with
                                 | None ->
                                     //vprintln 3 ("Found " + typen + " in hindley but no type in the group")
                                     tailer()
                                 | Some ty ->
                                     //vprintln 3 ("Found " + typen + " in hindley with a type in the group " + bsv_tnToStr ty)
                                     ty
                        | _::tt -> lookie tt
                    lookie h  // Look up the type name in the hindley env if we are typechecking.
                   
// Avoid double imper call please. TODO.
        (if impf && nonep hoption then ty_eval_imperialise ans else ans)  //<------------------ too many call sites locally TODO.
        // Only imperialise if found in ee env - or if hindley is null meaning this is not a typechecking call.

    and evaler ee stack so aa =
        let copied_knots = ref []

        //vprintln 0 (sprintf "  evaler %s" (bsv_tnToStr aa))
        match aa with
            | Bsv_ty_id(nf, prec, (fid, _)) ->                
                let real_ans = ty_eval_s ww "evaler" fid ee mf so aa true nf fid // please use a local variant here? Found it so we lookit up again
                real_ans

            | Bsv_ty_knot(idl, k2) when not(nonep !k2) -> // Untied knot should never occur at this point.
                match op_assoc idl stack with
                    | Some p -> p
                    | None -> 
                        match op_assoc idl !named_items with
                            | None ->
                                let knot3 = ref None
                                let ans0 = Bsv_ty_knot(idl, knot3) // Make a fresh knot.
                                mutadd named_items (idl, ans0)
                                vprintln 3 ("added to copied knots: " + htos idl)
                                let ans = evaler ee ((idl, ans0)::stack) so (valOf !k2)
                                knot3 := Some ans
                                ans
                            | Some vale -> vale

                    
            | Bsv_ty_nom(ats, F_ntype, binds) ->
                let binds' = map (tidf_bind_so ee stack (SO_numeric)) binds // numeric for these? well vector is wierd but otherwise yes TODO
                let revert = function
                    | Tty(ty, fid) -> ty
                hydrate_type_builtins ww mf (hd ats.nomid) (map revert binds') aa

            | Bsv_ty_nom(ats, F_tyfun fname, binds) ->
                //vprintln 3 ("Start eval tyfun binds for " + fname)
                let binds' = map (tidf_bind_so ee stack SO_numeric) binds // FORCE NUMERIC HERE
                //vprintln 3 ("Finish eval tyfun binds for " + fname)
                let revert = function
                    | Tty(ty, fid) -> ty
                run_type_fun ww ee mf fname (map revert binds')


            | Bsv_ty_nom(ats, cat, binds) ->
                let binds' = map (tidf_bind_so ee stack (SO_none "tys44")) binds
                let shonf (em:genenv_map_t) = function
                    | Tty(ty, id) -> em.Add([id], GE_binding([id], {g_blank_bindinfo with whom="shonf rehydrate"}, EE_typef(G_none, ty)))
                let eex = { ee with genenv=  List.fold shonf ee.genenv binds' }
                let ff = evaler eex stack (SO_none "tys55") 
                let cat' =
                    match cat with
                    | F_tyfun _ -> sf "tyfun should be matched above for eval"
                    | F_ntype   -> sf "ntype should be matched above for propagates"

                    | F_ifc_temp // placeholder till fully defined by prelude
                    | F_action
                    | F_enum _
                    | F_monad
                    | F_vector
                    | F_module -> cat


                    | F_tunion(br, sto_list, details) ->
                        let shig = function
                            | None -> None // This null form is all that is expected in ty_eval
                            | Some(Bsv_tunion(variant_details, (width, tag_width, arity, recflag), mapping)) ->
                                Some(Bsv_tunion(map (muddy "ff it might change") variant_details, (width, tag_width, arity, recflag), mapping))

                        F_tunion(br, map (evaler_xo eex stack) sto_list, shig details)
                    | F_struct(st_list, details)  ->
                        let shog = function
                            | None -> None // This null form is all that is expected in ty_eval
                            | Some(Bsv_struct((width, arity, recf), field_details)) ->
                                Some(Bsv_struct((width, arity, recf), muddy "shog global stats might change"))
                        F_struct(map (evaler_x eex stack) st_list, shog details)
                    | F_actionValue sty -> F_actionValue (ff sty)
                    | F_subif(idl, sty) -> F_subif(idl, ff sty)
                    | F_ifc_1 ii ->
                        //let yes = htosp idl = "Prelude.Reg"
                        //if yes then vprintln 0 ("AUCANADA1: Doing ty_eval of F_ifc_1" + htosp ats.nomid + sprintf " binds pre=%A\npost=%A" (sfold tidToStr binds) (sfold tidToStr binds') )
                        F_ifc_1 { ii with iparts= map (ff) ii.iparts }

                    | F_fun(rt, mats, args) -> 
                        let evaler_bg(id, cp, drop) =
                            vprintln 0 (sprintf "  evaler function %s body_gamma=%s" (mats.methodname) (droppingToStr drop))
                            (id, cp, dropapp (ff) drop)
                        let mats = { mats with protocol= protoapp ff mats.protocol; body_gamma=map evaler_bg mats.body_gamma }
                        F_fun(ff rt, mats, map (ff) args)

                let ans = Bsv_ty_nom(ats, cat', binds') 
                mutadd named_items (ats.nomid, ans)
                ans
                
            | Bsv_ty_stars(mm, ty) -> add_stars mm (evaler ee stack  (SO_none "tys9191") ty)

            | Bsv_ty_int_vp vp -> ty_eval_imperialise aa

            | aa when leaf_grounded_type_pred aa -> aa // manifestly grounded forms

            | other ->
                //vprint(0, "env ty_eval other is  env=")
                //for z in ee.genenv do ignore(vprint(0, sprintf " key=%s vale=%A\n" z.Key z.Value)) done 
                //vprintln 0 ""
                vprintln 0 ("+++ty_eval other " + bsv_tnToStr other)
                other



    let ptk = (SO_none "tys000")
    match aa with
        | Bsv_ty_nom(nst, _, _) ->
            let topknot = ref None
            let hitem = (nst.nomid, Bsv_ty_knot(nst.nomid, topknot)) // TODO squirrel actuals
            mutadd named_items hitem
            let ans = evaler ee [hitem] ptk aa
            topknot := Some ans
            ans
        | aa -> evaler ee []  (SO_none "tys000") aa


//
// typecheck_topper performs type checking: it is applied to a list of module statements called the 'body'.
// The free type vars and parameters should be bound in the input ee env.
// It records type annotations (also known as making droppings) about its findings.
//           
// This should perhaps work in two passes, making the hindley resolutions on the first pass and making firmer droppings on the second pass?
//        
and typecheck_topper ww bobj ee (mf:unit->string) (ifc_sty:bsv_type_t option) body  = 
    let vd = bobj.dropping_loglevel
    let m0 = "typecheck_topper: "
    let ww = WF 3 m0 ww ("start " + (if vd>=4 then mf() else ""))
    let write_pred = function // TODO also native_write for BRAMs
        | Bsv_ty_nom(_, F_fun(_, fats, _), _) when fats.methodname = g_swrite-> true
        | _ -> false

        // With an interface we auto-apply the _read() unit method when present, unless disabled with asReg.
        // With a method we auto-apply it to unit if it takes unit args. This also applies to functions since we do not distinguish but they rarely have no arguments.

    // tyo is now in asReg potentially
    // Why do we have unitapp ass well as this code?
    let auto_apply_ty mf (asReg:bsv_proviso_t) tyo ty0 = // Three call sites: tag,id,subscript. Disable for reads with asReg flag holding.
        let aavd = 3
        let ww = WN "auto_apply_ty" ww

        let ok_rt rt =
            let cc = []  // g_null_tb
            auto_apply_compat ww bobj mf cc tyo asReg rt

        let read_pred2 = function
            | Bsv_ty_nom(_, F_fun(rt, fats, _), _) when fats.methodname = "_read" && auto_apply_allowedp vd rt asReg && ok_rt rt -> true
            | _ -> false


        match ty0 with
        | Bsv_ty_nom(ats, F_ifc_temp, prams) -> muddy (mf() + " s1 ifc " + htosp ats.nomid)
#if UNUSED
        | Bsv_ty_nom(ats, F_ifc_temp, prams) -> // OLD - expect fresh
            vprintln 0 "deprecated"
            let maker_type =
                match ee.genenv.TryFind ats.nomid with
                | None -> cleanexit (sprintf "%s interface must be declared in prelude before can be rezzed" (htosp ats.nomid))

                | Some(GE_binding(_, _, EE_typef(_, sum_ty))) ->
                     sf (sprintf " Got reg maker %A" sum_ty)
                | Some other -> cleanexit(sprintf "%s: interface defined wrongly prelude: %A"  (htosp ats.nomid) other)
            muddy "here L2210 ifc_temp"
#endif

        | Bsv_ty_nom(_, F_fun(rt, fats, []), _) -> // see unitap
            if aavd>=4 then vprintln 4 (sprintf "auto_apply_ty of unit arg applied to method '%s' giving %s " fats.methodname (bsv_tnToStr rt))
            rt

        | Bsv_ty_nom(iidl, F_ifc_1 ii, _) ->
            let rd_pred = function
                | Bsv_ty_methodProto(methodname, _,  mth_provisos_, proto, Bsv_oty_fun(sty_, _, rt, args, provisos_, _)) -> methodname = "_read" && auto_apply_allowedp vd rt asReg
                | Bsv_ty_nom(_, F_fun(rt, matts, args), _) -> matts.methodname = "_read" && nullp args && auto_apply_allowedp vd rt asReg
                | other ->
                    if aavd>=4 then vprintln 4 (mf() + sprintf ": auto_apply_ty rd_pred other %A" other)
                    false // also have subinterfaces here. sf (sprintf "rd_pred other %A" other)

            let rd_candidates = List.filter rd_pred ii.iparts
            if aavd>=4 then
                vprintln 4 (mf() + sprintf ": %s Auto_apply_ty metrics rds=%i enabled=%A" (bsv_tnToStr ty0) (length rd_candidates) (asReg))
                vprintln 4 (sprintf "AA=%A " rd_candidates)

            let sol_select cc = function
                | Bsv_ty_nom(_, F_fun(rt, matts, args), _) ->
                    if ok_rt rt then
                        if aavd>=4 then vprintln 4 (sprintf "Did an auto_apply_ty (414) %s, returning %s " matts.methodname (bsv_tnToStr rt))
                        rt::cc
                    else cc
                | Bsv_ty_methodProto(methodname, prats, mth_provisos, proto, Bsv_oty_fun(sty_, _, rt, args, provisos, _)) -> // NB: the underscore for the unit candidates give auto-precedence to doing a read on a both-cases tie, such as in a Reg or DWire, but we could instead predicate this on a r-mode perference passed in as a 3-way tyo from apply8.
                    //let path' = B_applyf({name=PI_bno(B_dyn_b(methodname, path)); }, [], lp)
                    //let path' = quik_ri lp methodname path rt
                    if ok_rt rt then
                        vprintln 3 (sprintf "Did a %s (415) auto_apply" methodname)
                        rt::cc
                    else cc
                | other -> cleanexit(mf() + ": more than one _read method available")

            let ifc_item = if ifc_enabled ty0 asReg then [ty0] else []
            let cands = List.fold sol_select [] rd_candidates
            //vprintln 0 (sprintf "PROVISOSOSOS and LIST GEN %i %i" (length ifc_item) (length cands))
            match (ifc_item, cands) with
                | ([], []) -> sf (mf()  + "no possibility")
                | ([], cands) -> gen_Bsv_ty_list (cands) // only one in general
                | ([ifc], []) -> ifc
                | (ifc_item, cands) -> // Give static priority to the auto_apply option
                    if aavd>=4 then vprintln 4 (mf() + ": NOT KEEPING BOTH AUTO APS OPEN")
                    gen_Bsv_ty_list (cands)
                  

        | other ->
            if aavd>=6 then vprintln 6 (mf() + sprintf ": Not trying to auto_apply () or _read on %s. asReg=%A A=%A" (bsv_tnToStr other) asReg other)
            other

    let rec typecheck_diadic_bool (ee, (tbcc:tb_t), h0) mf ncp (ooo, lhs, rhs, lp) =
        let provisos = provisos_needed(ooo)
        let (l_sags) = ptypecheck_exp_p ww (ee, tbcc, h0) provisos lhs
        if nullp l_sags then sf (mf() + ": no base type")
        let rside (tbcc, h0, bt) = // Possibly this is better than cartesian resolve?
            let (r_sags) = ptypecheck_exp_p ww (ee, tbcc, h0) provisos rhs
            map (fun (tbcc, h0, tys) -> (tbcc, h0, bt, tys)) r_sags
        let prods = list_flatten (map rside l_sags)
        let (tbcc, h0, lt, rt) = if length prods = 1 then hd prods else sf (mf() + sprintf ": HBOMB-tidyadic %i" (length prods))
//          -and store_type_annotation (ee0, bobj0) ww (em:genenv_map_t) site id (mm:string) cc ncp arglog = 
//          +and store_type_annotation (ee0, bobj0) ww (em:genenv_map_t) site id (mf:unit->string) cc h0 ncp arglog = 
        let (tbcc, h0_) = store_type_annotation (ee, bobj) ww ee.genenv "L2959" "" mf tbcc h0 ncp (Tanno_function(g_bool_ty, [], [], [("LB",lt); ("RB",rt)]))
        //dev_println "// TODO be consistent about returning h0"
        (tbcc) 

    and typecheck_diadic ww (ee, (tbcc:tb_t), h0) (mf:unit->string) ncp (ooo, lhs, rhs, lp) = // Special code for diadic operator overload is not needed! Please delete and use function overload generic code.
        let ww = WN "typecheck_diadic" ww
        let provisos = provisos_needed(ooo)

        let l_sags = ptypecheck_exp_p ww (ee, tbcc, h0) provisos lhs
        let rsider cc (tbcc, h0, lty) = // Possibly this is better than cartesian resolve?  We want the type meet aspect though?
            let r_sags = ptypecheck_exp_p ww (ee, tbcc, h0) provisos rhs
            let meeter cc (tbcc, h0, rty) =
                let nvl = sum_type_meet_serf ww bobj mf h0 (lty, rty)
                let nvl = map (fun (h0, sty) -> (lty, rty, tbcc, h0, sty)) nvl
                nvl @ cc
            List.fold meeter cc r_sags
        let prods = List.fold rsider [] l_sags

        //let prods = muddy "sty_product_choose ww bobj mf true sags_l sags_r"
        let isFormat = function
            | Bsv_ty_format -> true
            | _ ->false

        let rec findfirst = function
            | [] ->
                cleanexit(lpToStr0 lp + sprintf ": no suitable overload out of %i overloads for diadic operator %A" (length prods) ooo)
                
            | (lty, rty, tbcc, h0, sty)::tt ->
                //let (h, a0) = sum_type_meet ww bobj mf h0 (lt, rt)
                let str_cat = is_stringty lty && is_stringty rty && ooo = KB_op V_plus
                let ans =
                    match ooo with
                        | KB_bop _ -> g_bool_ty
                        | KB_op V_plus when is_stringty lty && is_stringty rty -> g_string_ty
                        | KB_op _  
                        | KB_ab _  -> if (isFormat lty || isFormat rty) then Bsv_ty_format else Bsv_ty_integer // Diadic + is overloaded as string cat: need to do a proper typeclass lookup really...
                        | other -> sf (sprintf "other form typecheck diadic %A" other)
                //dev_println(sprintf "diadic typecheck str_cat=%A op=%A  lty=%s rty=%s ans=%s" str_cat (ooo) (bsv_tnToStr lty) (bsv_tnToStr rty) (bsv_tnToStr ans))
                (lty, rty, tbcc, h0, ans)

        let (lt, rt, tbcc, h0, ans) = findfirst prods
        //dev_println (qm + whereAmI_concise ww)
        let (tbcc, h0) = store_type_annotation (ee, bobj) ww ee.genenv "L2978-diadic" "" mf tbcc h0 ncp (Tanno_function(ans, [], [], [("LD",lt); ("RD",rt)]))

        ([tbcc, h0, ans])
        
    and typecheck_bexp ww (ee, (tbcc:tb_t), h0) arg =
        let bool_ty_check ww mf h0 sty =
            let (h0, _) = sum_type_meet ww bobj mf h0 (g_bool_ty, sty)
            h0
            
        match arg with // TODO just use default clause for all... but check rt is bool

        | KB_functionCall(KB_bop oo, [l; r], (lp, uid)) -> //TODO would match any other diadic function - dont use this use typecheck diadic always please TODO.
            let mf() = lpToStr0 lp + sprintf ": typecheck diadic boolean operator %A" oo 
            let ww = WF 3 m0 ww (mf())
            let ncp_ = extend_callpath bobj "L2973" uid ee.callpath
            //vprintln 0 (mf() + "+++ need to log fcall diadic bool type resolution " + cpToStr ncp)
            let tbcc = typecheck_diadic_bool (ee, tbcc, h0) mf ncp_ (KB_bop oo, l, r, lp)
            (tbcc, h0)

        | KB_functionCall(KB_unop "!", [arg], (lp, uid)) ->
            let mf() = lpToStr0 lp + ": monadic!"
            let ww' = WN (mf()) ww
            //let ncp = extend_callpath bobj  "L2982" uid ee.callpath
            //vprintln 0 (mf() + "+++ need to log fcall type unary ! resolution " + cpToStr ncp)
            let provisos = [Prov_si "Arith"] // TODO check
            let _ = ptypecheck_exp_p ww (ee, tbcc, h0) (provisos) arg
            let (ee_, tbcc, sty) = muddy "HBOMB-10"
            let h0 = bool_ty_check ww mf h0 sty
            (tbcc, h0)

        | other ->
            let (sags) = ptypecheck_exp ww (ee, tbcc, h0) other
            let (tbcc, h0, sty) = if length sags=1 then hd sags else muddy (sprintf "HBOMB-bool %i" (length sags))
            let h0 = bool_ty_check ww (mf) h0 (auto_apply_ty (mf) (Prov_None "L2759") (Some g_bool_ty) sty)
            //sf (mf() + sprintf ": typecheck other bexp: " + astToStr other) 
            (tbcc, h0)

    and ptypecheck_exps ww (ee, tbcc, h0) expressions =
        let rec tce tbcc h0 = function
            | [] -> [(tbcc, h0, [])]
            | h::tt ->
                let (sags) = ptypecheck_exp ww (ee, tbcc, h0) h
                let _:(tb_t * hindley_t * bsv_type_t) list = sags
                let tc1 (tbcc, h0, ty) = 
                    let ans = tce tbcc h0 tt // Form multi-d cartesian product of possibilities from each position.
                    let insp (tbcc, h0, tyl) = (tbcc, h0, ty::tyl)
                    map insp ans
                list_flatten(map tc1 sags)
        let ans = tce tbcc h0 expressions
        let _:(tb_t * hindley_t * bsv_type_t list) list = ans
        (ans)


    and ptypecheck_exp ww (ee, tbcc, h0) arg = ptypecheck_exp_as ww (ee, tbcc, h0) (Prov_None "defaulting") arg

    // With provisos
    and ptypecheck_exp_p ww (ee, tbcc, h0) provisos arg = ptypecheck_exp_as ww (ee, tbcc, h0) (Prov_s provisos) arg    

    and ptypecheck_exp_as ww (ee, tbcc, h0) asReg arg =
        let _:hindley_t = h0
        let rtyo = None // not being used
        //vprintln 0 (sprintf "--Commence typecheck exp %A" arg)
        match arg with
        | KB_dontcare lp -> ([tbcc, h0, Bsv_ty_dontcare(lpToStr0 lp)])
            
        | KB_taggedUnion(vtag, amembers, soloo, (lp, _))
        | KB_taggedUnionExpr(vtag, amembers, soloo, (lp, _)) ->
            let (mf, vtag, possible_types, uid) = prep_tagged_expr ww bobj ee arg
            let possible_types =
                match asReg with
                    | Prov_sty ty when length possible_types > 1 -> // Where a type context is provided, it should be easy to disambiguate tags shared over types.
                      [tag_type_checker ww bobj lp mf vtag ty possible_types]
                    | _           -> map fst possible_types

            match possible_types with
                | [] ->
                    let vd = 0
                    vprint vd "L3055 In-scope tags are: "
                    for z in ee.ctor_tags do ignore(vprint vd (z.Key + " ")) done 
                    vprintln vd ""
                    cleanexit(mf() + sprintf ": A tagged union with tag '" + vtag + "' is not in scope")

                | [Bsv_ty_nom(idl, F_tunion(mrecflag, variants, details_), args)] ->
                    let tbcc =
                        let scam1 cc (tag, x) =
                            let _ = ptypecheck_exp ww (ee, tbcc, h0) x
                            let (ee_, tbcc, rt) = muddy "HBO202"
                            let _ = op_assoc tag variants
                            // TODO use rt with a meet.
                            cc
                        List.fold scam1 tbcc amembers


                    let tbcc =
                        let scam2 (x) =
                            let _ = op_assoc vtag variants                            
                            let _ = ptypecheck_exp ww (ee, tbcc, h0) x
                            let (ee_, tbcc, types) = muddy "HBO203"
                            // TODO use rt with a meet.
                            tbcc
                        if nonep soloo then tbcc else scam2 (valOf soloo)

                    let ty = hd possible_types
                    let (tbcc, h0) =
                        let ncp = extend_callpath bobj  "L3055" uid ee.callpath
                        simple_store_type_annotation (ee, bobj) ww ee.genenv "L3055" mf tbcc h0 ncp ty
                    ([tbcc, h0, ty])

                | a::b::_ -> cleanexit(mf() + ": Disambiguation of two or more tagged unions with tag '" + vtag + "' not achieved.")
                | other -> sf( sprintf "fossil %A" other)


        | KB_structPattern _
        | KB_structExprPositional _
        | KB_structExprAssociative _ ->
            let qm = "typecheck build of structPatterns"
            let really_needed = false
            let (uid, ttags, mf) = prep_spa qm arg
            let ttag = hd ttags // TODO we can do better by taking the intersection of the support of the present tags Or even better the type name is given here in the first field!
            let sty =
                match ee.ctor_tags.TryFind ttag with
                | Some[(sty, _)] -> sty
                | Some other -> sf (sprintf "%s: assoc/tag match other length - needs disambig %A" qm other)
                | None ->
                    match ee.genenv.TryFind[ttag] with // Dont do this and lookup_type - need to similarly abstract for tags.
                    | Some(GE_binding(_, _, EE_typef(_, sty))) -> sty
                    | Some other -> sf(mf() + sprintf ": %s assoc/tag match other form %A" qm other)
                    | None ->
                        let vd = 0
                        vprint vd "In-scope tags are: "
                        for z in ee.ctor_tags do ignore(vprint vd (z.Key + " ")) done 
                        vprintln vd ""
                        muddy(mf() + sprintf ": typecheck: no internal type analysis case for tag '%s' - currently please only use in a context where the required type is obvious 1/2" ttag)

            //dev_println(qm + sprintf ": old site: tyo=%s"  (if nonep tyo then "None" else bsv_tnToStr(valOf tyo))) // tyo is ignored 
            let sty = afreshen_sty ww bobj "structPattern-etc" None sty // None is bad.
#if OLDSITE

// Alternatively we can type-reduce against a tyo when present ...
            let (tbcc, h0) =
                let ncp = extend_callpath bobj "L3106top" uid ee.callpath // Not needed!? TODO.  Also better to store it post hydration - this is the fresh version thats not much use. Would want to ty_eval it post cc being formed.
                simple_store_type_annotation (ee, bobj) ww ee.genenv "L3106top-notneeded" mf tbcc h0 ncp sty
#endif

            let rec assoca tag sty = // Lookup a tag in a struct or union type
                match sty with
                    | Bsv_ty_nom(_, F_tunion(_, items, details_), _) -> // delete me? Dont expect associative tunions?
                        //vprintln 0 (sprintf "assoca search in tunion items %A" items)
                        let x = op_assoc tag items
                        let deopt = function
                            | Some None -> None
                            | Some (Some x) -> Some x
                            | None -> None
                        ("tunion", deopt x)
                    | Bsv_ty_nom(_, F_struct(items, details_), _) -> 
                        //vprintln 0 (sprintf "assoca: search for %s in struct items %A" tag items)
                        let x = op_assoc tag items
                        ("struct", x)
                    | Bsv_ty_knot(idl', k2) when not(nonep !k2) -> assoca tag (valOf !k2)
                    | other -> sf (sprintf "assoca: looking up tag %s: other form %A" tag other)
            let (ee, tbcc, h0) =
                match arg with
                    | KB_structPattern(_, lp)            ->
                        muddy "fo1 -structPattern"

                    | KB_structExprPositional(tag, actuals, _)  ->      
                        let rec posert sty =
                            match sty with
                                | Bsv_ty_nom(_, F_tunion(_, items', details_), _) ->
                                    let (tbcc, h0) =
                                        if really_needed then 
                                            let ncp = extend_callpath bobj "L3106z" (uid + "^1^" + tag) ee.callpath // Not needed!? TODO delete me
                                            simple_store_type_annotation (ee, bobj) ww ee.genenv "L3106z" mf tbcc h0 ncp sty
                                        else (tbcc, h0)
                                    // here check actuals is 0 or 1
                                    match op_assoc tag items' with
                                        | None -> sf(mf() + " tag did not exist afterall " + sprintf "F_positional tag=%s yifo %A v %A" tag actuals items')
                                        | Some None when nullp actuals -> (ee, tbcc, h0)
                                        | Some None (* otherwise *) -> cleanexit(mf() + sprintf "Args provided for zero-arity tag '%s'" tag)
                                        | Some (Some (Bsv_ty_id (vp, prec, (aid, bid)))) when length actuals = 1 -> // one is correct for a tagged union.
                                            let ty = Bsv_ty_id (vp, prec, (aid, bid)) // other actuals will be found here!!
                                            match actuals with
                                                | [ast] -> // One for a tagged union.
                                                    //vprintln 0 (sprintf "NN Need to bind fid %A F_positional tag=%s yifo form %A v %A" ast tag actuals items')
                                                //  Valid(exp)
                                                    let _ = ptypecheck_exp ww (ee, tbcc, h0) ast
                                                    let (ee, tbcc, aty) = muddy "HBOMB-205"
// NN Need to bind fid mta_t F_positional tag=Valid yifo other form [KB_id ("arg0",(LP ("Hwmlcore.bsv",324), "Z1044_arg0"))] v [("Invalid", null); ("Valid", Some (Bsv_ty_id (VP_value "mta_t"), Some P_free, ("mta_t",  (*fresh_bindindex*) "-10")))]
                                                    let ee = { ee with genenv=ee.genenv.Add([aid], GE_binding([aid], {g_blank_bindinfo with whom="structExprPositional-tunion"}, EE_typef(G_none,  ty))) }

                                                    let (h0, styx) = sum_type_meet ww bobj mf h0 (ty, aty)
                                                    let (tbcc, h0) =
                                                        let ncp = extend_callpath bobj "L3106a" (uid + "^" + tag) ee.callpath
                                                        simple_store_type_annotation (ee, bobj) ww ee.genenv "L3106a" mf tbcc h0 ncp styx
                                                                         
                                                    (ee, tbcc, h0)
                                                | other -> sf (mf() + sprintf " Cannot bind other form of structExprPositional-tunion arg %A" other)
                                        | q -> muddy (sprintf "F_positional tag=%s yifo other form %A v %A -> %A" tag actuals items' q)


                                | Bsv_ty_nom(_, F_struct(items, details_), _) ->
                                    if length items <> length actuals then cleanexit(mf() + sprintf ": Wrong number of positional elements in structural expression for %s" (bsv_tnToStr sty))
                                    let tycheck_postruct (ee, tbcc, h0) = function
                                        | (((tag:string), tag_sty), ast_exp) ->
                                            let _ = ptypecheck_exp ww (ee, tbcc, h0) ast_exp
                                            let (ee_, cc, lt) = muddy "HBO10"
                                            let (h0, fldsty) = sum_type_meet ww bobj (fun()->mf() + " for tag " + tag) h0 (lt, tag_sty)
                                            let (tbcc, h0) =
                                                if really_needed then 
                                                    let ncp = extend_callpath bobj "L3720" (uid + "^" + tag) ee.callpath
                                                    simple_store_type_annotation (ee, bobj) ww ee.genenv "L3720" mf tbcc h0 ncp fldsty
                                                else (tbcc, h0)
                                            (ee, tbcc, h0)
                                    List.fold tycheck_postruct (ee, tbcc, h0) (List.zip items actuals)

// [(("tpl_1", Bsv_ty_id (VP_value (false,[]),Some P_decl0,"tax_tFTOK194")),  KB_id ("newlisty",(LP ("smalltests/Test8e.bsv",15), "Z26_newlisty")));
// (("tpl_2", Bsv_ty_id (VP_value (false,[]),Some P_decl0,"tbx_tFTOK194")),  KB_stringLiteral "fred")]
                                    

                                | Bsv_ty_knot(idl', k2) when not(nonep !k2) -> posert (valOf !k2)
                                | other -> sf (sprintf "fo3 other posert %A" other)

                        posert sty


                    | KB_structExprAssociative(id, items, (lp, uid)) -> // typecheck clause
                        // We are sometimes building a union variant whose body is a struct. We first find id in items and then look in that. Otherwise we are just building a struct.
                        match assoca id sty with
                            | (form, None) ->
                                if form = "struct" then () else sf(mf() + sprintf ": form=%s +++ unfound tag '%s' in %s" form id (bsv_tnToStr sty))
                                let tycheck_element (ee, tbcc, h0) (tag, ast_exp) =
                                    match assoca tag sty with 
                                        | (form, None) ->
                                             cleanexit(mf() + sprintf ": No such tag '%s' in variant %s of %s" tag id (bsv_tnToStr sty))
                                        | (form, Some sty) ->
                                            let sags = ptypecheck_exp ww (ee, tbcc, h0) ast_exp
                                            let (tbcc, h0, lt) = if length sags = 1 then hd sags else muddy "HBOMB-3033a"
                                            let (h0, asty) = sum_type_meet ww bobj mf h0 (lt, sty)   
                                            let (tbcc, h0) =
                                                if really_needed then 
                                                    let ncp = extend_callpath bobj "L3106c" (uid+ "^" + tag) ee.callpath
                                                    simple_store_type_annotation (ee, bobj) ww ee.genenv "L3106c" mf tbcc h0 ncp asty
                                                else (tbcc, h0)
                                            (ee, tbcc, h0)
                                let _ = // Here we do not check whether exhaustive but we do check for repeats
                                    let rec check_repeats = function
                                        | [] -> ()
                                        | h::tt when memberp h tt -> cleanexit(mf() + sprintf "Tag %s given more than once in associative instance of %s" h (bsv_tnToStr sty))
                                        | _::tt -> check_repeats tt
                                    check_repeats (map fst items)
                                             
                                List.fold tycheck_element (ee, tbcc, h0) items

                            | (form, Some inner_sty) ->
                                let tycheck_element (ee, tbcc, h0) (tag, ast) =
                                    match assoca tag inner_sty with // Here we do not check for exhaustive coverage of tags or repeat tags.
                                        | (form, None) ->
                                            cleanexit(mf() + sprintf ": No such tag '%s' in variant %s of %s" tag id (bsv_tnToStr sty))
                                        | (form, Some sty) ->
                                            let sags = ptypecheck_exp ww (ee, tbcc, h0) ast
                                            let (tbcc, h0, lt) = if length sags = 1 then hd sags else muddy "HBOMB-401" 
                                            let (h0, bsty) = sum_type_meet ww bobj mf h0 (lt, sty)   
                                            let (tbcc, h0) =
                                                if really_needed then 
                                                    let ncp = extend_callpath bobj "L3106d" (uid + "^" + tag) ee.callpath
                                                    simple_store_type_annotation (ee, bobj) ww ee.genenv "L3106d" mf tbcc h0 ncp bsty
                                                else (tbcc, h0)
                                            (ee, tbcc, h0)

                                List.fold tycheck_element (ee, tbcc, h0) items
#if OLDNEWSITE
   // Our afreshen had no kickoff. What we drop here will have spurious free type vars.
   // We must rely on the bev cmd or other caller to make the dropping.
            let (tbcc, h0) =
                dev_println ("new site drop")
                let ncp = extend_callpath bobj "L3106-newistetop" uid ee.callpath // Better to store it here, post hydration. 
                simple_store_type_annotation (ee, bobj) ww ee.genenv "L3106top-notneeded" mf tbcc h0 ncp sty // <-------------- sty not rehydrated despite resolutions
#endif
            ([tbcc, h0, sty])
            
        | KB_query(grd, l, r, (lp, uid)) ->
            let mf() = lpToStr0 lp + ": KB_query ?-: typecheck conditional expression"
            let ww = WF 3 m0 ww (mf())
            let ncp = extend_callpath bobj "L3171" uid ee.callpath
            let (tbcc, h0) = typecheck_bexp ww (ee, tbcc, h0) grd
            let _ = ptypecheck_exp ww (ee, tbcc, h0) l
            let _ = ptypecheck_exp ww (ee, tbcc, h0) r          
            let (ee_, cc, lt) =  muddy "HBO409"
            let (ee_, cc, rt) = muddy "HBO500"
            let (h0, ans_ty) = sum_type_meet ww bobj mf h0 (lt, rt)   
            let (tbcc, h0) = store_type_annotation (ee, bobj) ww ee.genenv "L3194" "" mf tbcc h0 ncp (Tanno_function(ans_ty, [], [], [("LQ", lt); ("RQ", rt)]))
            ([tbcc, h0, ans_ty])

        | KB_valueOf(type_arg, (lp, uid)) -> // We cannot prep valueof/valueOf to a normal function apply since its arg is a type.
            let mf() = lpToStr0 lp + ": typecheck built in functionCall valueOf"
            let ww = WF 3 m0 ww (mf() + " uid=" + uid) 
            let prec = None //Some P_defn1   // TODO correct? - or delete throughout since not used.
            let arg_ty = collect_sum1 ww bobj ee mf prec (VP_value(true, [])) type_arg // Note we want a numeric value here!
            let (tbcc, h0) = simple_store_type_annotation (ee, bobj) ww ee.genenv "L3202" mf tbcc h0 (extend_callpath bobj "L3184" uid ee.callpath) arg_ty
            ([tbcc, h0, Bsv_ty_integer])


        | KB_systemFunction ("$time", [], lp)
        | KB_id("$time", lp) ->  // NB:  no longer parsed as a KB_id
            ([tbcc, h0, mk_bitty 64])

        | KB_systemFunction ("$stime", [], lp)         // Short time is 
        | KB_id("$stime", lp)  ->            
            ([tbcc, h0, mk_bitty 32])

        | KB_bitRange2(item, _, _, _) ->
            let _ = ptypecheck_exp_as ww (ee, tbcc, h0) asReg // TODO need to proviso the return type
            let tc = muddy "HBO666"
            let (ee, tbcc, _) = tc item
            ([tbcc, h0, Bsv_ty_integer])

        | KB_intLiteral(fwo, (dontcares, b, s), lp) ->  ([tbcc, h0, Bsv_ty_integer])
        
        | KB_bitConcat(args, lp) -> // This can be either a bitconcat or a Vector initialisation.
            let mm = lpToStr0 lp + ": typecheck bitConcat/vectorConstant" + sprintf " asReg=%A" asReg
            let ww = WF 3  m0 ww mm
            let (rt, ct) =
                match asReg with
                    //| None -> (Bsv_ty_integer, Bsv_ty_dontcare "bitConcat-contents-1")
                    | Prov_sty (((Bsv_ty_nom(nst, qos, [len_; Tty(ct, s)]))) as vt) when nst.nomid = ["Vector"] ->
                        (vt, ct)

                    | other ->
                        vprintln 3 (sprintf "using another bitConcat use %A" other)
                        (Bsv_ty_integer, Bsv_ty_dontcare "bitConcat-contents-2")
            let sags = ptypecheck_exps ww (ee, tbcc, h0) args // TODO need to proviso the return type
            let (tbcc, h0, _) = if length sags = 1 then hd sags else muddy "HBOMB-2343"
            ([tbcc, h0, rt])

        | KB_tag _ ->
            let mf() = "typecheck_exp path"
            let (ty, uid_, tbcc, h0) = typrep_path ww bobj ptypecheck_exps mf ee tbcc h0 (asReg) arg 
            match ty with
                | ty -> ([tbcc, h0, ty])

        | KB_id(ss, (lp, _)) ->
            let mf() = lpToStr0 lp + " typecheck id + " + ss

            let rec degen1 = function
                | None -> cleanexit(mf() + sprintf ": undefined identifer '%s'" ss)
                | Some(GE_overloadable(prototy, oloads)) ->  // If only one def then use it! refudge. TODO
                match oloads with
                    | [] -> sf (mf() + sprintf " no overloads defined for id %s (L2763)" ss)
                    | items ->
                        let placate (forwot, ((sty, fl_), item)) = sty
                        map placate items

                | Some(GE_binding(_, _, EE_typef(_, sty))) -> [sty]
                | Some(GE_binding(_, _, EE_e(sty, vale)))  -> [sty]
                | Some(GE_mut(idl, sty, whom_, vref))      -> [sty]

                | Some(GE_normthunk(instf, idl, ans, reaped, _,  ff)) when not(nonep !ans) -> degen1 !ans
                | Some((GE_normthunk(instf, idl, ans, reaped, being_done, ff)) as arg) -> // idiomatic kate code - nth copy!
                    if not (nonep !being_done) then
                        //vprintln 0 ("want to make a recursion marker 2/2 for " + htosp idl)
                        [Bsv_ty_sum_knott(idl, ref arg)]
                    else
                    let knot2 = ref None
                    being_done := Some (Bsv_ty_knot(idl, knot2))
                    let env_updates = ff ww { ee with genenv=ee.genenv.Remover [hd idl]; } // [ hd idl ] ?
                    reaped := env_updates
                    being_done := None
                    let kate = function
                        | Aug_env(tyclass, bound_name, (dmtof, who, vale))->
                            let _ = knot2 := Some(ee_sty mf vale)
                            //vprintln 0 (sprintf "ribbish vale 11 %A" (eeToStr vale))
                            ans := Some(GE_binding(bound_name, who, vale)) 
                        | tt -> sf(sprintf "kate L2677 other form '%s' %A" (htosp idl) tt)
                    app kate env_updates
                    degen1(!ans)

                | Some x -> sf (mf() + sprintf ": identifier '%s' strangely bound as %A" ss x)

            let tys_lst = degen1 (ee.genenv.TryFind [ss]) 
            if bobj.dropping_loglevel >= 4 then vprintln 4 (mf() + sprintf ": (L3429) typechecked '%s' to give %s" ss (sfold bsv_tnToStr tys_lst))
            let sags =
                let autoty ty = (tbcc, h0, auto_apply_ty mf asReg rtyo ty)
                map autoty tys_lst
            sags
            
        | KB_subscripted(dotflag, item, subs_lst, m_bitflag, (lp, uid)) -> // typecheck
            let mf() = lpToStr0 lp + ": typecheck subscripted r-mode" // TODO: we want to set bitflag on lmode forms of bit insert too. 
            let ww = WF 3 m0 ww (mf())
            let sags_base = ptypecheck_exp ww (ee, tbcc, h0) item
            if nullp sags_base then sf (mf() + ": no base type")
            let rside (tbcc, h0, bt) = // Possibly this is better than cartesian resolve?
                let (sags_subs) = ptypecheck_exps ww (ee, tbcc, h0) subs_lst            
                map (fun (tbcc, h0, tys) -> (tbcc, h0, bt, tys)) sags_subs
            let prods = list_flatten (map rside sags_base)
            let (tbcc, h0, bt, tys) = if length prods = 1 then hd prods else sf (mf() + sprintf ": HBOMB %i" (length prods))

            let list_subscript_flag = // This is now also inside prep_bitflag but need locally.
                match bt with
                    | Bsv_ty_nom(nst, _, _) when nst.nomid = ["List" ;"List"] -> true
                    | _ -> false
            let bitflag =
                if dotflag || list_subscript_flag then false
                else !m_bitflag || prep_bitflag ww bobj mf lp asReg bt
            if bitflag then m_bitflag := true // belt and braces
            vprintln 3 (mf() + sprintf ": treating as bit extract?  bitflag=%A list_select=%A dotflag=%A" bitflag list_subscript_flag dotflag)
            if dotflag then
                match bt with
                    | Bsv_ty_nom(_, _, arg_types) when length arg_types = 2 -> // Address type then content type. We can use tyderef for this.
                        let ans_ty =
                            match cadr arg_types with
                                | Tty(ty, fid) -> ty
                                | Tid(_, id)   -> muddy (mf() + sprintf " need eval of dot subscript content type. formal=%s" id) // Should not happen since already eval'd ?
                        ([tbcc, h0, ans_ty])
                    | other -> sf (mf() + sprintf ": dotted right subscript applied to other type: type=%A" bt)
            elif list_subscript_flag then
                vprintln 3 (mf() + ": subscripting a list") // The rhs subscription is implemented by calling the List.select function in the standard prelude, whereas, somewhat oddly, the lhs subscription is currently hardwired in the toy compiler.
                if length subs_lst <> 1 then muddy "need to do one subscript at a time here"
                let args = [ item; (hd subs_lst) ]
                //let newast = KB_functionCall(KB_ast(KB_tag(KB_id(g_selectname, (lp, uid)), "List", (lp, uid))), args, (lp, uid))
                // "select" should be a typeclass with an entry for lists
                let newast = KB_functionCall(KB_ast(KB_id(g_selectname, (lp, uid))), args, (lp, uid))                
                ptypecheck_exp_as ww (ee, tbcc, h0) asReg newast // go round again with a call to List.select
            else
                let ncp = extend_callpath bobj "L3310" uid ee.callpath                
                let bt' = (if bitflag then bt else take_stars mf (length tys) bt)
                let ans_ty = auto_apply_ty mf asReg rtyo bt'
                let (tbcc, h0) = store_type_annotation (ee, bobj) ww ee.genenv "L3310subscript" "" mf tbcc h0 ncp (Tanno_function(ans_ty, [], [], [("", bt); ("", Bsv_ty_integer)]))
                ([tbcc, h0, ans_ty])
            
        | KB_interfaceExpr(KB_type_dub(_, name, [], lp), items, nameo, _)
        | KB_interfaceExpr(KB_id(name, (lp, _)), items, nameo, _) ->        
            let mf() = lpToStr0 lp + ": typecheck interfaceExpr+" + name
            let ww = WF 3 m0 ww (mf())
            if nameo<>None && valOf nameo <> name then cleanexit(mf() + ": end name does not match start: " + name + " cf " + valOf nameo)
            match tbcc.rv with
                | Some return_sty ->
                    let (ee_, tbcc, h0, items_, conflicts_) = List.fold (typecheck_module_statement ww (Some return_sty)) (ee, tbcc, h0, [], []) items
                    let args = [] // NOT CORRECT ...? they are inside the sif - please extract - but not sure if we even want this cf afresherz
                    ([tbcc, h0, Bsv_ty_nom({g_blank_nom_typ_ats with nomid=[name]; cls=Prov_ifc}, F_ifc_temp, args)])
                | None -> cleanexit(mf() + ": cannot use an interface expression in this context")
                
        | KB_functionCall(KB_unop "-", [arg], (lp, uid)) ->
            let mm = lpToStr0 lp + " typecheck monadic negate"
            let ww = WF 3 m0 ww mm        
            //let ncp = extend_callpath bobj uid ee.callpath
            //vprintln 0 (mf() + "+++ need to log fcall type unary - resolution " + cpToStr ncp)
            let sags = ptypecheck_exp ww (ee, tbcc, h0) arg
            //let sty = check_proviso "Arith" sty
            sags

        | KB_functionCall(KB_unop "!", [arg], (lp, uid)) ->
            let mm = lpToStr0 lp + ": exp monadic!"
            let ww = WN mm ww
            //let ncp = extend_callpath bobj uid ee.callpath
            //vprintln 0 (mf() + "+++ need to log fcall type unary pling resolution " + cpToStr ncp)
            let sags = ptypecheck_exp ww (ee, tbcc, h0) arg
            sags
            
        | KB_functionCall((KB_dk _)    as path, args, (lp0, uid))
        | KB_functionCall((KB_ast  _) as path, args, (lp0, uid)) -> // typecheck function application.
            let (asregf, display_tasko___, forms, mf, scalar_function_name, lp, uid_us, tbcc, h0) = typrep_functionCall ww bobj ptypecheck_exps m0 ee tbcc h0 (KB_functionCall(path, args, (lp0, uid)))
            let fname_hints = [valOf_or scalar_function_name "(complex name path)"] // TODO this needs to be a mpx of HOF calls - now in forms
            let ww = WF 3 "typecheck_exp: functionCall L3350" ww (mf())
#if SPARE_RECURSION_CHECK
            let _ = 
                match ee.callpath with
                    | CPS(gf, path) ->
                            if memberp uid (map f1o3 path) then sf "recursive _gr"
                    | _ -> ()
#endif
            let arity = length args
            let sags = ptypecheck_exps ww (ee, tbcc, h0) args
            let msgf () = sprintf "Function overload resolution: fname=%A arity=%i hof=%A L3347 %i overloads, %i arg vector types." (hptos fname_hints) arity "hof?" (length forms) (length sags)

            //let (tbcc, h0, a_args_ty) = if length sags = 1 then hd sags else muddy (sprintf "HBOMB-265641 fname=%A arity=%i hof=%A L3347 %i overloads, %i arg vector types." (hptos fname_hints) arity "hof?" (length forms) (length sags))

            // Check cartesian product of arg vector types and defined overloads - hopefully precisely one definition one matches.
            // We freshen all variants (overloads) and then find (hopefully) one that matches.
            let fresh_stys =
                let do_fun_sty_variant (stored_sty, bodyo, hof_route_) =
                    //vprintln 0 (mf() + " stored sty was " + bsv_tnToStr stored_sty)
                    afreshen_sty ww bobj "functionCall" None stored_sty
                zipWithIndex(map do_fun_sty_variant forms)

            let try_each_body cc (fresh_sty, nn) =
                let try_each_arg_sag cc (tbcc, h0, a_args_ty) =
                    //let opts = Bsv_ty_list(mf(), fresh_stys)
                    let synth = Bsv_ty_nom({g_blank_nom_typ_ats with nomid=[(hd fname_hints) + "$synthcorpus"]; }, F_fun(Bsv_ty_dontcare "synth-rt", {g_blank_fid with who="synth"; methodname=(hd fname_hints); fids=map (fun x->sprintf "!FF%i" x) [0..length args-1]; }, a_args_ty), [])                
                    let sols = sum_type_meet_serf ww bobj mf h0 (fresh_sty, synth)
                    ////vprintln 3 (sprintf "choose_check : type compatible check: found ok %s u %s -u-> %s" (bsv_tnToStr tl) (bsv_tnToStr tr) (bsv_tnToStr ans))
                    //let (sols, _) = sty_choose ww bobj mf (fresh_sty, synth)
                    let augment (h0, sty) = (nn, sty, f2o3 forms.[nn], tbcc, h0, a_args_ty)
                    (map augment sols) @ cc
                List.fold try_each_arg_sag cc sags
            let candidates = List.fold try_each_body [] fresh_stys

            let (nn, fty, body, tbcc, h0, a_args_ty) =
                match candidates with
                    | [(nn, fty, body, tbcc, h0, a_args_ty)] ->
                        if length forms > 1 || length sags > 1 then vprintln 3 (mf() + sprintf ": selecting overload %i of %s out of %i x %i forms" nn (hptos fname_hints) (length forms) (length sags))
                        hd candidates
                    | [] ->
                        cleanexit(lpToStr0 lp + sprintf "Function call: No suitable overloads found. %s" (msgf()))
                    | items ->
                        hpr_yikes (sprintf "Function call: Overloads not resolved. %i possible solutions. Arbitrarily taking first one. %s" (length items) (msgf()))
                        hd items

            // Find flow of hof functions in this function body... hofpath template from args
            let find_hofpath (dd, ed) = function // This is just a preliminary scan now --- TODO mostly delete this one of the two copies
                | (Bsv_ty_nom(nst, F_fun(rt, mats, args), _), aot, hof_route) ->
                    //vprintln 0 (mf() + sprintf " hofpath trackdown hof %s %A" mats.methodname hof_route)
                    let hof_name (dd, ed) (ty, nn, fid) =
                        match ty with
                            | Bsv_ty_nom(nst, F_fun(_, mats, _), _) ->
                                vprintln 3 (mf() + sprintf " : hofpath uid=%s uid_us=%s (fid=%s nn=%i) and name=%A" uid uid_us fid nn nst.nomid)
                                ()
                            | other ->
                                //vprintln 0 (sprintf "hof pred no spog fid=%s for %s" fid (bsv_tnToStr other))
                                ()
                        if nonep hof_route then (dd, ed) else ((uid_us, valOf hof_route) :: dd, ed)
                    fun_arity_check ((*"find_hofpath:" +*) mf) mats.methodname mats.fids a_args_ty
                    List.fold hof_name (dd, ed) (List.zip3 a_args_ty [0..length a_args_ty-1] mats.fids)
                | (Bsv_ty_knot(idl, k2), aot, hof_route) ->
                    vprintln 3 (sprintf "Recording recursion ptr in callstring at %s for " uid_us + htos idl)
                    (dd, (uid_us, idl)::ed)
                | (other, _, _) ->
                    //vprintln 0  (sprintf "L3930 find_hof_path other form " + bsv_tnToStr other)
                    (dd, ed)

            let (hofpaths, recpaths) = List.fold find_hofpath ([], []) forms
            let (ishof, isrecf) = (not_nullp hofpaths, not_nullp recpaths)
//TODO when further functions are passed in as args - do they need freshening ?  We only need their bg.

            let ncp =
                if ishof || isrecf then
                    vprintln 3 (sprintf "Special markings are recursion-recf=%A and hof=%A in callstring for " isrecf ishof  + htos fname_hints)
                    let hoftokens = fname_hints
                    if ishof then vprintln 3 (sprintf "hof special callpath token: using %s on %A : making hof cp fname(s)=%s" uid ee.callpath (sfold (hoftokenToStr) hoftokens))
                    let xo = if ishof then Some (hoftokens, evty_create_indirection ee.genenv) else None
                    let ro = if isrecf then 1 else 0// We do not handle more than one rec step TODO count backwards ...
                    extend_callpath2 bobj "L3350HOF" (ro, xo) uid ee.callpath // save 
                else extend_callpath bobj "L3350" uid ee.callpath
                
            let do_fun_variant (ee, tbcc, h0, general_rt) (fresh_sty, bodyo) =
                //let fresh_sty = afreshen_sty ww bobj None stored_sty
                if uid <> uid_us then vprintln 0 (sprintf "+++Must not ignore _uid %A cf %A" uid_us uid)
                //dev_println (mf() + sprintf "typecheck function apply (%s) " uid_us) // + bsv_tnToStr maker_type
                let m_arglog = ref []
                
                let rec tch_fun = function 
                    | Bsv_ty_knot(idl, k) when not(nonep !k) -> tch_fun(valOf !k)
                    | Bsv_ty_dontcare x -> ([tbcc, h0, Bsv_ty_dontcare x])

                    | Bsv_ty_nom(_, F_fun(rt, fats, f_args_ty), _) ->
                        //vprintln 0 (mf() + sprintf ": %s a_args_ty = %A" fats.methodname (sfold bsv_tnToStr a_args_ty)) 
                        fun_arity_check mf fats.methodname f_args_ty a_args_ty
                        let (ee_3, tbcc, h0, hof_bg) =
                            if nullp f_args_ty then (ee, tbcc, h0, []) // varargs (never used with HOF since only used by PLI prims).
                            else  // We deepbind for function applications later, but in this bind, for overload disambiguation, we use another binder, abind, currently - but should really since work is replicated.
                                let abind (ee_2, tbcc, h0, hof_bg) (fid, fty, aty) =
                                    let (tbcc, h0, ty) = newsum_arg ww bobj mf ee m_arglog (tbcc, h0) (fty, aty) // Augments cc with deep binding? then we will get more persistence and less problem from using sum_nf as an type evaluator. We at least need to do the top level!
                                    // TODO think about unification - the args might need re-eval owing to bg or return type unifications.
                                    let (hof_bg, hof) =
                                        match aty with
                                            | Bsv_ty_nom(nst, F_fun(_, mats, _), _) ->
                                                // The mats of this actual could be random owing to arbitrary type meets so dont use them ... TODO - we want instead to save the env and the actual
                                               //vprintln 0 (mf() + sprintf ": function '%s' has higher-order use idl=%s" (mats.methodname) (htos nst.nomid))
                                               let x3 = hof_deco_close uid_us fid nst.nomid (fname_hints, evty_create_indirection ee_2.genenv) ty // cc needs also capture 
                                               //vprintln 0 (sprintf "higher-order 1 bg fid=%s idl=%s is len=%A\n\n" fid (htos nst.nomid) (length mats.body_gamma))
                                               //vprintln 0 (sprintf "higher-order 1 bg is %A\n\n" x3)
                                               (x3 :: hof_bg, Some "TODO-SPURIOUS-INFO-3591-hof")
                                            | xty ->
                                                //vprintln 0 (sprintf "Fail to mark hof %s" (bsv_tnToStr xty))
                                                (hof_bg, None)
                                    //vprintln 0 (mf() + sprintf " :arg abind %s with %s " fid (bsv_tnToStr ty))
                                    if nonep bodyo then (ee_2, tbcc, h0, hof_bg) // skip bind if no body to use it. what about bg though? that will need it - TODO
                                    else
                                        let ee_2 = { ee_2 with genenv=ee_2.genenv.Add([fid], gen_GE_sumtype(fid, {g_blank_bindinfo with whom="abind"; hof_route=hof }, ty)) }
                                        (ee_2, tbcc, h0, hof_bg)

                                List.fold abind (ee, tbcc, h0, []) (List.zip3 fats.fids f_args_ty a_args_ty) // 2nd zip?
//c.fs(4726,43): error FS0001: The type 'bsv_type_t' does not match the type 'string * callpath_t * dropping_t'
                        let find_hofpath2 = function /// A second copy with better ee for putting in SPOG
                            | (Bsv_ty_nom(nst, F_fun(rt, mats, args), _), aot, hof_route) ->
                                //vprintln 0 (mf() + sprintf " hofpath trackdown hof %s %A" mats.methodname hof_route)
                                let hof_name (ty, nn, fid) =
                                        match ty with
                                            | Bsv_ty_nom(nst, F_fun(_, mats, _), _) ->
                                                vprintln 3 (mf() + sprintf " : hofpath spog uid=%s uid_us=%s (fid=%s nn=%i) and name=%A" uid uid_us fid nn nst.nomid)
                                                // A SPOG is a note of a function being passed to another.
                                                // A further, similar mechanisms would be needed if returning of functions were supported...
                                                // The spog collation is unrelated to the hof_route tagging which denotes whether this current function is a hof apply.
                                                let sp = SPOG(("-1", fname_hints), nst.nomid, (ee_3.genenv, tbcc, h0), uid, fid)
                                                dev_println ("Made spog " + spogToStr sp)
                                                if vd>=4 then vprintln 4 ("Made spog " + spogToStr sp)
                                                mutadd g_spogs sp
                                                ()
                                            | other ->
                                                //vprintln 0 (sprintf "hof pred no spog fid=%s for %s" fid (bsv_tnToStr other))
                                                ()
                                fun_arity_check ((*"spog:" +*) mf) mats.methodname mats.fids a_args_ty
                                app (hof_name) (List.zip3 a_args_ty [0..length a_args_ty-1] mats.fids)
                            | (other, _, _) ->
                                //vprintln 0  (sprintf "L2724 find_hof_path 2/2 other form " + bsv_tnToStr other)
                                ()

                        app find_hofpath2 forms 

                        let evty_f ww aa = ty_eval_generic ww bobj ee_3 mf (Some h0) aa
                        
                        //vprintln 0 (mf() + sprintf ": g/eye rt=%A\n rt'=%A" rt rt')
                        //vprintln 0 (mf() + sprintf ": g/eye contribute to parent's gamma using this one %A" fats) //.body_gamma)
                        // During typechecking a function call we do not look at its actual body, just its body droppings.
                        // If a higher-order function we cannot even do that and must ...  TODO explain ... we do manage it but not for polymorphic higher-order function use?

                        let pushdown_gamma (id, bg_cp, dp) = // function call/apply use of pushdown.
                            let dp' = dropapp (evty_f ww) dp                            
                            if vd>=5 then
                                vprintln 5 (sprintf "L3542pushdown fname=%s: id=%s extend newtop=%A with item cp=%A" fats.methodname id (cpToStr ncp) (cpToStr bg_cp))
                                vprintln 5 (sprintf "L3542pushdown :   dp=%s    dp'=%s" (droppingToStr dp) (droppingToStr dp'))
                            let cp' = join_cp bobj "L3452pushdown" ncp bg_cp
                            (id, cp', dp')

                        let new_gam_drops = map pushdown_gamma (if ishof then hof_bg else fats.body_gamma) // Either drop locally or prepare for tanno_hof later via SPOGs.
                        let rt' = evty_f ww rt
                        let xx =
                            if not ishof then Tanno_function(rt', new_gam_drops, [], rev (!m_arglog)) // All of these could be posted till nf called or body typechecked?
                            else Tanno_hof(rt', new_gam_drops, hofpaths, rev (!m_arglog)) // All of these could be posted till nf called or body typechecked?                            

                        // Store them here explicitly locally since the reader does not currently recurse into xx ?
                        let (tbcc, h0) = List.fold (fun (tbcc, h0) (id, cp, drop) -> store_type_annotation (ee, bobj) ww ee.genenv "L3461" id (fun() -> mf() + " body drop") tbcc h0 cp drop) (tbcc, h0) new_gam_drops
                        let (tbcc, h0) = store_type_annotation (ee, bobj) ww ee.genenv "L3462" "" (fun()->mf() + " body drop") tbcc h0 ncp xx
                        let (h0, general_rt') = sum_type_meet ww bobj mf h0 (rt', general_rt)   
                        //vprintln 0 (mf() + sprintf " new variant folded %s x  %s -> %s" (bsv_tnToStr rt') (bsv_tnToStr general_rt) (bsv_tnToStr general_rt'))
                        ([tbcc, h0, general_rt'])

                    | other -> cleanexit(mf() + sprintf ": applied function '%s' was not a function. It had type %A" (hd fname_hints) (bsv_tnToStr fresh_sty))
                tch_fun fresh_sty

            if asregf then
                if length args <> 1 then cleanexit(mf() + ": builtin function asReg requires exactly one argument")
                let sags = ptypecheck_exp_as ww (ee, tbcc, h0) (Prov_asReg_asIfc "12345678") (hd args)
                let (tbcc, h0, ans) = if length sags = 1 then hd sags else muddy "HBO595"
                vprintln 3 (mf() + "'asReg' call : nothing logged under " + cpToStr ncp)
                ([tbcc, h0, ans]) // ans should, of course, be a register interface.
            else do_fun_variant (ee, tbcc, h0, Bsv_ty_dontcare "noneyet") (fty, body)


        | KB_functionCall(ooo, [l; r], (lp, uid)) ->
            let mf() = lpToStr0 lp + sprintf ": typecheck diadic op (uid=%s) %A" uid ooo
            let ww = WF 3 m0 ww (mf())
            let ncp_ = extend_callpath bobj "L3436" uid ee.callpath
            //vprintln 0 (mf() + "+++ need to log fcall type diadic non-AST resolution " + cpToStr ncp)
            typecheck_diadic ww (ee, tbcc, h0) mf ncp_ (ooo, l, r, lp)

        | KB_stringLiteral s -> ([tbcc, h0, mk_string_ty (strlen s)]) // TODO resolve me against the target when proviso (in asReg field) present.
        
        | KB_actionValueBlock(key, nameo1, body, nameo2, (lp, uid)) -> //typecheck expr.
            //We expect a return statement inside an action value block but not inside an action block. 
            let id = funique(if nameo1=None then lpToStr0 lp else valOf nameo1)
            let mf() = lpToStr0 lp + sprintf ": typecheck expr actionValueBlock or actionBlock key=%s  " key + id
            let ww = WF 3 "typecheck_cmd" ww (mf())
            let (ee_, tbcc, h0) = List.fold (typecheck_cmd ww) (ee, tbcc, h0) body
            let sty =
                match tbcc.rv with
                | None when key="action" -> g_action_ty
                | None -> cleanexit(mf() + " return value assignment expected in actionvalue block")
                | Some sty -> sty
            ([tbcc, h0, sty])
            
        | KB_constantAlias(tag, x) ->
            let mf() = "constantAlias+" + tag
            let ww = WF 3 m0 ww (mf())
            // Here's our chance to introduce choice in the hindley that is disambiguated later ...
            let pty_o = 
                match asReg with
                    | Prov_sty ty -> Some ty
                    | _           -> None
            let rty = collect_sum1 ww bobj ee mf pty_o (VP_value(false, [])) (KB_constantAlias(tag, x))
            // resolve me against the target?
            ([tbcc, h0, rty])

        | KB_caseExpr(exp, pattern, lp) -> // This form not tested.  TODO add a test
            let mf() = lpToStr0 lp + ": caseExpr"
            let ww = WF 3 m0 ww (mf())
            let _ = ptypecheck_exp ww (ee, tbcc, h0) exp
            let (ee_, tbcc, exp_sty) = muddy "HBOMB6666 - CASE Expression Please Test"
            let (tbcc, h0, work) = check_casebind ww (exp_sty) (tbcc, h0, []) (exp_sty, pattern) 
            // We do not need to mux the bindings in the typecheck phase: we can union them. Muxing only needed in the execution phase.
            let cbinder2 (ee:genenv_map_t) (l, r) = ee.Add([l], r)
            let ee = { ee with genenv=List.fold cbinder2 ee.genenv work; }
            ([tbcc, h0, g_bool_ty]) // This is a predicate check that can also put fresh bindings into ee.
(* KB_caseExpr
  (KB_id ("taggedReg",(LP ("EdgeFIFOs.bsv",108), "Z92_taggedReg")),
   KB_taggedUnion
     ("Valid",[],Some (KB_patternVar "v"),
      (LP ("EdgeFIFOs.bsv",108), "Z94_Valid")),LP ("EdgeFIFOs.bsv",108))
*)            
        
        | KB_typeAssertion(ast_ty, exp, (lp, uid)) ->
            let ctype = collect_sum1 ww bobj ee mf None (VP_none "KB_typeAssertion") ast_ty
            let ncp = extend_callpath bobj "L3940" uid ee.callpath
            let (tbcc, h0) = simple_store_type_annotation (ee, bobj) ww ee.genenv "L4744" mf tbcc h0 ncp ctype
            let sags = ptypecheck_exp ww (ee, tbcc, h0) exp  // Do we need to call typecheck_exp_as and pass in ctype ?
            let (tbcc, h0, exp_sty) = if length sags = 1 then hd sags else muddy "HBOMB-90qsda"

            ([tbcc, h0, ctype]) 

        | KB_sublang(seqpar, lst, lp) ->
            let mf() = lpToStr0 lp + ":FSM sublanguage expression (expr context)"
            let ww = WF 3 m0 ww (mf())
            typecheck_sublang ww mf (ee, tbcc, h0) lst

        | other -> sf (mf() + sprintf ": typecheck other exp: %A" other + "\n" + astToStr other)

    and typecheck_sublang ww mf (ee, tbcc, h0) aa =
        let rec slcheck (tbcc, h0) = function
            | other ->
                let (ee, tbcc, h0) = typecheck_cmd ww (ee, tbcc, h0) other
                (tbcc, h0)
                
        let (tbcc, h0) = List.fold slcheck (tbcc, h0) aa
        ([tbcc, h0, Bsv_ty_fsm_stmt ""]) // Should use a void variant here.
        
    and typecheck_cmd ww (ee, tbcc, h0) aa =
        let qm = "typecheck module behavioural command"
        if g_enable_drop_debug then
            dev_println(qm + sprintf ": Drop Report %i" (length tbcc.tve))
            let drop_report (cps, ncp, ty) = dev_println(sprintf "  DROP FOR %s %s      \t\t%s"  cps (cpToStr ncp) (droppingToStr ty))
            app drop_report tbcc.tve


        match aa with
        //| KB_varDeclAssign_raw _ -> typecheck_cmd ww (ee, tbcc, h0) (desugar_varDeclAssign ww m0 aa)

        | KB_regWrite_raw _ -> typecheck_cmd ww (ee, tbcc, h0) (desugar_varDeclAssign ww m0 aa)

        | KB_actionValueBlock(key, nameo1, body, nameo2, (lp, uid)) -> //typecheck bev/cmd
            let mm = lpToStr0 lp + ": ActionValue block (bev context)"
            let ww = WF 3 qm ww mm
            let (ee, cc, _) = muddy "HBOM90awd"
            let _ = ptypecheck_exp ww (ee, tbcc, h0) aa // check it using the expression code.
            (ee, tbcc, h0)

        | KB_attribute(ats_, xx, lp) -> typecheck_cmd ww (ee, tbcc, h0) xx
        
        | KB_functionDef3(proto, equatef, body, nameo, lp) -> // bev
            let mm = lpToStr0 lp + (" typecheck bev cmd functionDef3")
            let ww = WF 3 qm ww mm
            let (nf_type, mf_, name) = nf_typecheck_functiondef ww ee tbcc h0 (KB_functionDef3(proto, equatef, body, nameo, lp))
            let ee = { ee with genenv=ee.genenv.Add([name], gen_GE_sumtype(name, {g_blank_bindinfo with whom=mm }, nf_type)) }
            if vd>=5 then vprintln 5 (mf() + " its type is " + bsv_tnToStr nf_type)
            let idl = name :: ee.static_path
            let bin = fresh_bindindex "-fd3a"
            let nb = (Some nf_type, [(idl, bin)], VP_none "a-function")
            let ww = WF 3 qm ww ("finished " + mm)
            dev_println ("bindindex trim needed")
            (ee, tbcc, nb::h0) 


        | KB_block(blockname, lst, nameo, lp) -> // bev
            let mm = lpToStr0 lp + ": behavioural block statement (begin/end) " + valOf_or blockname "anon"
            let ww = WF 3 qm ww mm
            if nameo<>None && blockname<>None && valOf nameo <> valOf blockname then cleanexit(mf() + ": end name does not match start: " + valOf blockname + " cf " + valOf nameo)
            List.fold (typecheck_cmd ww) (ee, tbcc, h0) lst

        | KB_for(s1, g, s2, body, lp) -> // bev typecheck
            let (mm, ww, expanded) = prep_for ww m0 aa
            typecheck_cmd ww (ee, tbcc, h0)  expanded

        | KB_if(grd, tt, ffo, lp) -> // bev typecheck
            let mm = lpToStr0 lp + ": IF statement"
            let ww = WF 3 m0 ww mm
            let (tbcc, h0) = typecheck_bexp ww (ee, tbcc, h0) grd
            let (ee_, tbcc, h0) = typecheck_cmd ww (ee, tbcc, h0) tt
            if nonep ffo then (ee, tbcc, h0)
            else typecheck_cmd ww (ee, tbcc, h0) (valOf ffo)
                
        | KB_while(grd, body, lp) -> // bev typecheck
            let mm = lpToStr0 lp + " " + htosp ee.static_path + " typecheck behavioural while loop "
            let ww = WF 3 m0 ww mm
            let (tbcc, h0) = typecheck_bexp ww (ee, tbcc, h0) grd
            typecheck_cmd ww (ee, tbcc, h0) body

        | KB_regWrite(lhs, r, (lp0, uid)) -> // bev
            let mm = lpToStr0 lp0 + " " + htosp ee.static_path + " typecheck behavioural regWrite (<=) "
            let ww = WF 3 m0 ww mm
            let expanded = KB_functionCall(KB_ast(KB_tag(lhs, g_swrite, (lp0, uid))), [r], (lp0, uid)) // method call in ths instance.
            typecheck_cmd ww (ee, tbcc, h0) expanded
           
        | KB_easc(easc, lp) -> // expression-as-command easc bev/cmd 
           let mm = lpToStr0 lp + " bev/cmd eascStmt"
           let ww = WF 3 m0 ww mm
           let sags = ptypecheck_exp ww (ee, tbcc, h0) easc
           let (tbcc, h0, _) = if length sags = 1 then hd sags else muddy "HBOMB-90a" 
           (ee, tbcc, h0)

        | KB_functionCall(path, args, (lp0, uid)) -> // bev/cmd: builtin function call or (newly) module app (do), but not an interface method call
            let sags = ptypecheck_exp ww (ee, tbcc, h0) (KB_functionCall(path, args, (lp0, uid)))
            let (tbcc, h0, _) = if length sags = 1 then hd sags else muddy "HBOM90b"
            (ee, tbcc, h0)

        | KB_returnStmt(exp, (lp, uid)) ->
            let mm = lpToStr0 lp + " " + htosp ee.static_path + ": typecheck bev returnStmt"
            let ww = WF 3 m0 ww mm
            let sags = ptypecheck_exp ww (ee, tbcc, h0) exp
            let (tbcc, h0, rt1) = if length sags=1 then hd sags else muddy (mm + sprintf ": no=%i HBOMB-90qkret" (length sags))
            match tbcc.rv with
                | Some rt ->
                    let (h0, ret_sty) = sum_type_meet ww bobj mf h0 (rt1, rt)
                    let (tbcc, h0) =
                        let ncp = extend_callpath bobj "L3624ret" uid ee.callpath
                        simple_store_type_annotation (ee, bobj) ww ee.genenv "L3624ret" mf tbcc h0 ncp ret_sty
                    (ee, { tbcc with rv=Some ret_sty }, h0)
                | None -> cleanexit(mf() + ": return statement in context where no return value is expected.")

        | KB_letBinding(id, r, operator, (lp, uid)) -> // bev (TODO share code with bev varDeclAssign ?)
            let mf() = lpToStr0 lp + " " + htosp ee.static_path + sprintf ": typecheck letBind (bev) %s, operator '%s'" id operator
            let idl = [id]
            let ww = WF 3 m0 ww (mf())
            let (tbcc, h0, sty) = 
                let sags = ptypecheck_exp ww (ee, tbcc, h0) r
                let (tbcc, h0, sty) = if length sags = 1 then hd sags else muddy "HBOMB-90d"
                match operator with
                    | "<-" ->
                        match sty with
                            | Bsv_ty_nom(_, F_actionValue av_ty, _) -> (tbcc, h0, av_ty) // TODO: check we dont ever encounter monads in this context?
                            | sty -> muddy (mf() + sprintf " letdo2 %A" sty) // SmallExamples/ex_02_f  "let y <- pipe.receive; $display ("    y = %0h", y);"
                    
                    | "=" -> (tbcc, h0, sty)

                    | other -> sf(mf() + ": bad let operator: " + operator)
            let ncp = extend_callpath bobj "L3940" uid ee.callpath
            let (tbcc, h0) = simple_store_type_annotation (ee, bobj) ww ee.genenv "L3940" mf tbcc h0 ncp sty
            let kv = (idl, gen_GE_sumtype(id, {g_blank_bindinfo with whom=operator}, sty))
            //vprintln 0 (mf() + ": typecheck type=" + bsv_tnToStr sty)
            let ee = { ee with genenv= ee.genenv.Add kv }
            (ee, tbcc, h0)

        | KB_varDeclAssign(tyo, lhs, rhso, lp) -> // typecheck bev
            let mf() = lpToStr0 lp + " typecheck bev DECL-A " + (if nonep tyo then "(nodecl) " else "")
            let ww = WF 3 m0 ww (mf())
            let (ee, tbcc, h0, lhs_sty, m) =
                match tyo with // A variable declaration present?
                    | Some ast_ty ->
                        let (shortname, idl, stars, uid_, lp_) = lhs_path_shortname ww mf lhs
                        let mm = lpToStr0 lp + " typecheck bev declAssign DECL-A " + (if nonep tyo then "nodecl " else "") + shortname
                        //let prec = Some P_defn1   // This is a type definition but a variable declaration!
                        let ct = collect_sum1 ww bobj ee mf None (VP_value(false, [])) ast_ty
                        let lhs_sty = add_stars_decl stars ct
                        //vprintln 0 (sprintf "DECL-A %A '%s' nv augmented" shortname (bsv_tnToStr lhs_sty))
                        //let nv__ = (shortname, ncp, drop)
                        ({ ee with genenv=ee.genenv.Add(idl, gen_GE_sumtype(shortname, {g_blank_bindinfo with whom=mm }, lhs_sty)); }, tbcc, h0, lhs_sty, mf)
                    | None ->
                        let (lhs_sty, _, tbcc, h0) = typrep_path ww bobj ptypecheck_exps mf ee tbcc h0 (Prov_None "DECL-A-lhs1") lhs
                        (ee, tbcc, h0, lhs_sty, mf)

            let (lhs_sty_, lhs_uid, tbcc, h0) = typrep_path ww bobj ptypecheck_exps mf ee tbcc h0 (Prov_None "DECL-A-lhs2") lhs // TODO use this answer - it supports richer forms - well its sometimes done once above.
            vprintln 3 (mf() + "Now typecheck rhs")
            let (tbcc, h0, sty) = 
                match rhso with
                | Some rhs ->
                    let sags = ptypecheck_exp ww (ee, tbcc, h0) rhs
                    let (tbcc, h0, rhs_sty) = if length sags = 1 then hd sags else muddy "HBOMB-90g"
                    //vprintln 0 (mf() + "Again check lhs = rhs typewise! 1/2")
                    let (h0, sty) = sum_type_meet ww bobj mf h0 (lhs_sty, rhs_sty)
                    (tbcc, h0,  sty)
                | None -> (tbcc, h0, lhs_sty)
            // By storing this anotation here at the end, we allow the rhs to affect the type of the lhs even when an explict lhs type is present in the declaration, even if the effect is just unifcation of free type vars (such as list_content_t in List.cons).
            //dev_println (mf() + sprintf ": Final L3652 decl type is %s" (bsv_tnToStr sty))
            let (tbcc, h0) = store_type_annotation (ee, bobj) ww ee.genenv "L3652" "" mf tbcc h0 (extend_callpath bobj "L3632" lhs_uid ee.callpath) (Tanno_function(sty, [], [], [("CT", sty)]))
            (ee, tbcc, h0)                    
        
        | KB_varDeclDo(tyo, lhs, rhs, lp) -> // typecheck the rarer bev/cmd form of do operator.
            let mf() = lpToStr0 lp + " typecheck bev/cmd DECL-D " + (if nonep tyo then "nodecl " else "")
            let ww = WF 3 m0 ww (mf())
            let (ee, lhs_sty, lhs_uid, tbcc, h0) = 
                match (tyo, lhs) with
                    | (Some ast_ty, KB_id(id, (lp, lhs_uid))) ->
                        let mm = lpToStr0 lp + " typecheck bev/cmd DECL-D decl + scalar " + id
                        let ww = WF 3 m0 ww mm
                        //let prec = Some P_defn1   // This is a type definition but a variable declaration!
                        // Check DMTO here please TODO
                        let sum_ty = collect_sum1 ww bobj ee mf None (VP_value(false, [])) ast_ty
                        //dev_println ("NEED TO ADD TO TVE TODO")
                        ({ ee with genenv=ee.genenv.Add([id], gen_GE_sumtype(id, {g_blank_bindinfo with whom=mm }, sum_ty)); }, sum_ty, lhs_uid, tbcc, h0)

                    | (Some _ , path) ->
                        let mm = lpToStr0 lp + " typecheck bev/cmd DECL-D decl + nonscalar "
                        cleanexit(mf() + ": unsupported variable declaration")
                        
                    | (None, path) ->
                        let (lhs_sty, lhs_uid, tbcc, h0) = typrep_path ww bobj ptypecheck_exps mf ee tbcc h0 (Prov_asReg_asIfc "LHS-DECL-D") lhs
                        (ee, lhs_sty, lhs_uid, tbcc, h0)

            let _ = ptypecheck_exp ww (ee, tbcc, h0) rhs
            let (ee_, cc, rt1) = muddy "HBOM90"
            //vprintln 0 (sprintf "pre auto_apply rt1 = %A" rt1)
            let rt2 =
                let hasbeen_auto_applied = false
                if hasbeen_auto_applied then rt1 else unitapp mf rt1 
            let (h0, sty) = sum_type_meet ww bobj mf h0 (lhs_sty, rt2)
            let (tbcc, h0) = store_type_annotation (ee, bobj) ww ee.genenv "L3685" "" mf tbcc h0 (extend_callpath bobj "L3665" lhs_uid ee.callpath) (Tanno_function(sty, [], [], [("CT", sty)]))
            (ee, tbcc, h0)

        | KB_case(exp, arms, lp) ->  // bev typecheck.
            let mf() = lpToStr0 lp + " " + htosp ee.static_path + ": typecheck case statement"
            let ww = WF 3 m0 ww (mf())
            let sags = ptypecheck_exp ww (ee, tbcc, h0) exp
            let (tbcc, h0, exp_sty) = if length sags = 1 then hd sags else muddy "HBOMB-90dw"
            //vprintln 3 (mf() + sprintf " checktop on exp_sty yields %s" (bsv_tnToStr exp_sty))

            let checkarm (tbcc, h0) (tags, hand) =
                // The case arms can bind vars using casebind.
                let (ee, tbcc, h0) =
                    match tags with
                        | [] -> (ee, tbcc, h0) // explict null case - can an arm have no tags?  No, but this at least ensures hd does not fail in other clause and establishes baseline semantic!
                        | tagitems ->
                            let rec bop (tbcc, h0) sofar = function
                                | [] -> ((tbcc, h0), sofar)
                                | tagitem::tt ->
                                    //vprintln 0 (mf() + sprintf ": casebind top %A with %A" exp_sty tagitem)
                                    let (tbcc, h0, items) = check_casebind ww (exp_sty) (tbcc, h0, []) (exp_sty, tagitem) 
                                    bop (tbcc, h0) ((tagitem, items)::sofar) tt
                            let ((tbcc, h0), work) = bop (tbcc, h0) [] tagitems
                            let l = length(snd (hd work))
                            let lencheck (tagitem, items) =
                                if length items <> l then cleanexit(mf() + sprintf " Case tags did not all bind the same number of identifiers (%i cf %i)" (length items) l)
                            app lencheck (tl work)
                            // We do not need to mux the bindings in the typecheck phase: we can union them. Muxing only needed in the execution phase.
                            let binder2 (ee:genenv_map_t) (l, r) = ee.Add([l], r)
                            let binder1 (ee:genenv_map_t) (tagitem_, bindpairs) = List.fold binder2 ee bindpairs
                            let ee = { ee with genenv=List.fold binder1 ee.genenv work }
                            (ee, tbcc, h0)
                            // TODO: If there are multiple terms they should all bind the same identifiers of course.
                            // TODO: when the same id is bound in different terms we should type unify between them.
                let (ee_, tbcc, h0) = typecheck_cmd ww (ee, tbcc, h0) hand
                (tbcc, h0)
            let (tbcc, h0) = List.fold checkarm (tbcc, h0) arms
            if g_enable_drop_debug then
                dev_println(mf() + sprintf ": Drop Report %i" (length tbcc.tve))
                let drop_report (cps, ncp, ty) = dev_println(sprintf "  DROP FOR %s %s      \t\t%s"  cps (cpToStr ncp) (droppingToStr ty))
                app drop_report tbcc.tve

            (ee, tbcc, h0)

        | KB_sublang(seqpar, lst, lp) ->
            let mf() = lpToStr0 lp + ":FSM sublanguage expression (bev context)"
            let ww = WF 3 m0 ww (mf())
            let (ee,cc, _) = muddy "typecheck_sublang ww mf (ee, tbcc, h0) lst"
            (ee, tbcc, h0)

        | KB_await(grd, lp) -> 
            let mf() = lpToStr0 lp + "typecheck FSM await"
            let ww = WF 3 m0 ww (mf())
            let (tbcc, h0) = typecheck_bexp ww (ee, tbcc, h0) grd        
            (ee, tbcc, h0)
            
        | other -> sf (mf() + sprintf ": typecheck other bev/cmd: " + astToStr other) 

    and check_casebind ww diagnose (tbcc, h0, items) (ety, xarg) =
        match (ety, xarg) with 
            | (Bsv_ty_knot(_, k2), _) when not(nonep !k2) -> check_casebind ww  diagnose (tbcc, h0, items) (valOf !k2, xarg)
            | (ety, KB_constantAlias(s, x)) ->
                let _ = ptypecheck_exp ww (ee, tbcc, h0) (KB_constantAlias(s, x))
                let (ee_, cc, xt) = muddy "HBOMB9"
                let (h0, _) = sum_type_meet ww bobj mf h0 (ety, xt) //<---------- TODO want to bind cc from other clauses too
                (tbcc, h0, items)
                
            | (ety, KB_intLiteral(fwo, (dontcares, b, s), lp)) -> // TODO requires exp to be of an integer proviso.  Are all literal constant forms mapped to this form during parsing?
                (tbcc, h0, items)

            | (Bsv_ty_nom(ats, F_tunion(mrecflag, variants, _), _), KB_taggedUnionExpr(vtag, amembers, soloo, (lp, uid)))
            | (Bsv_ty_nom(ats, F_tunion(mrecflag, variants, _), _), KB_taggedUnion(vtag, amembers, soloo, (lp, uid))) ->
                let qm = " typecheck case " + lpToStr0 lp + " tagged union"
                let ww = WF 3 qm ww ("tag " + vtag)
                if not_nullp amembers && not_nonep soloo then sf (mf() + ": Both solo and assoc present")
                let (tbcc, h0, ans) = 
                    match op_assoc vtag variants with
                        | None -> cleanexit(mf() + sprintf "No such tag '%s' in union '%s'" vtag (htosp ats.nomid))
                        | Some inner_ty ->
                            match soloo with
                                | None when nullp amembers -> (tbcc, h0, items)
                                | Some solo when nullp amembers -> check_casebind ww diagnose (tbcc, h0, items) (valOf inner_ty, solo)
                                | None -> muddy "amembers need binding 32"

                let (tbcc, h0) =
                    let ncp = extend_callpath bobj "L3759" uid ee.callpath
                    //vprintln 0 (sprintf "Diddrop L3759 %s " uid + bsv_tnToStr ety + " " + cpToStr ncp)
                    simple_store_type_annotation (ee, bobj) ww ee.genenv "L2959" mf tbcc h0 ncp ety
                (tbcc, h0, ans)

            | (ety, KB_patternVar bv) -> // Perhaps we should check not already bound in this case clause.
                let qm = " typecheck casebind"
                let ww = WF 3 qm ww ("pattern var " + bv)
                let nb = (bv, GE_binding([bv], {g_blank_bindinfo with whom="casebind-mish"}, EE_typef(G_none, ety)))
                (tbcc, h0, nb::items)

            //KB_taggedUnion  ("Valid",[],Some (KB_patternVar "di"),(LP ("Tb.bsv",111), "Z166_Valid")) 
            | (ety, KB_taggedUnionExpr(vtag, amembers, soloo, _))
            | (ety,     KB_taggedUnion(vtag, amembers, soloo, _)) -> 
                if not(nullp amembers) then sf (mf() + ": Both solo and assoc present")
                let (mm, vtag, possible_types, uid) = prep_tagged_expr ww bobj ee xarg
                match possible_types with
                    | [(xty, _)] -> // REPLICATED CODE  - ? Really
                        // Need to freshen, then find the relevant tag and then bind both ends
                        let xty = afreshen_sty ww bobj "taggedUnionExpr/taggedUnion-1" None(*no kickoff*) xty
                        let m1 = "mish1-case-freety"
                        let (h0, todrop) = sum_type_meet ww bobj mm h0 (xty, ety)
                        //vprintln 0 (mf() + sprintf " WINE todrop %s %s" uid (bsv_tnToStr todrop))
                        let (tbcc, h0) =
                            let ncp = extend_callpath bobj "L3783" uid ee.callpath
                            simple_store_type_annotation (ee, bobj) ww ee.genenv "L3783" mm tbcc h0 ncp todrop

                        check_casebind ww diagnose (tbcc, h0, items) (xty, xarg) // TODO need to do regardless of soloo/amembers
                    | _ -> muddy "use shared not replicated copy! TODO"
                 
            | (oo, KB_tuplePattern(strlst, (lp, uid))) ->
                let qm = " typecheck casebind"
                let mf() = lpToStr0 lp + ": tuplePattern"
                let ww = WF 3 qm ww (mf())
                let il = length strlst
                let fields =
                     match oo with
                        | Bsv_ty_nom(nomid, F_struct(fields, details_), _) ->
                             let a = length fields
                             if il<>a then cleanexit(mf() + sprintf ": wrong number of tuple pattern fields for '%s' %i cf %i" (htosp nomid.nomid) a il)
                             fields
                        | Bsv_ty_dontcare _ ->
                            let gen_dummy n = (funique("$gendummy" + i2s n + "_"), Bsv_ty_dontcare ("$gendummy" + mf()))
                            map gen_dummy [1 .. length strlst]
                        | other -> sf(mf() + sprintf ": other %A" other)
                let tycheck_struct_pattern_positional_bindf (tbcc, h0, items) = function
                    | ((tag, ty), KB_dot bv) ->
                        // Could be a letbind?  TODO consider for subx sharing.
                        let nb = (bv, GE_binding([bv], { g_blank_bindinfo with whom="mish2"}, EE_typef(G_none, ty)))
                        let ww = WF 3 qm ww (mf() + ": Created struct case/match binding of " + tag + " for " + bv)
                        (tbcc, h0, nb::items)
                    | ((tag, ty), q (*probably a KB_taggedUnion*)) ->
                        let ww = WF 3 qm ww (sprintf "Creating an inner struct case/match binding for tag '%s' ty=%s" tag (bsv_tnToStr ty))
                        check_casebind ww diagnose (tbcc, h0, items) (ty, q)

                    //((tag, (ty, w, pos)), q) -> sf(sprintf "tycheck_struct_pattern_positional_bindf tagty=%s  other %A" (bsv_tnToStr ty) q)
                List.fold tycheck_struct_pattern_positional_bindf (tbcc, h0, items) (List.zip fields strlst)



            | (Bsv_ty_nom(nomid, F_struct(fields, details_), _), KB_structPattern(strlst, (lp, uid))) ->
                let (tbcc, h0) =
                    let ncp = extend_callpath bobj "L3840top" uid ee.callpath
                    simple_store_type_annotation (ee, bobj) ww ee.genenv "L3840top" mf tbcc h0 ncp ety

                let bind (items, tbcc, h0) = function
                    | (tag, KB_dot bv) ->
                        match op_assoc tag fields with
                            | None -> cleanexit(mf() + sprintf ": no such tag '%s' in structure '%s'" tag (htosp nomid.nomid))
                            | Some fty ->
                                let m1 = "mish3"
                                let ww = WF 3 m0 ww (m1 + ": structPattern: Created struct case/match binding of " + tag + " for " + bv)
                                let nb = (bv, GE_binding([bv], {g_blank_bindinfo with whom=m1}, EE_typef(G_none, fty)))
                                let (tbcc, h0) =
                                    let ncp = extend_callpath bobj "L3840" (uid + "^" + tag) ee.callpath
                                    simple_store_type_annotation (ee, bobj) ww ee.genenv "L3840" mf tbcc h0 ncp fty
                                (nb :: items, tbcc, h0)
                    | (tag, other) ->  muddy ("complex? recursive nesting of structures and unions: " + htosp nomid.nomid)
                let (items, tbcc, h0) = List.fold bind (items, tbcc, h0) strlst
                (tbcc, h0, items)

            | (oo, KB_structPattern(strlst, (lp, uid))) ->
                let mm = lpToStr0 lp + " typecheck bev structPattern"
                let ww = WN mm ww
                muddy (mf() + sprintf " struct pattern outside a struct TODO : in a %s" (bsv_tnToStr oo))

            | (ety, xarg) ->
                //Perhaps expect/use this form more and have fewer local dedicated clauses.
                vprintln 0 (mf() + sprintf ": +++ UNEXPECTED case casebind other form 1 exp_sty=%A et=%A x=%A " (bsv_tnToStr diagnose) ety xarg)
                let sags = ptypecheck_exp ww (ee, tbcc, h0) xarg
                let (tbcc, h0, xt) = if length sags = 1 then hd sags else muddy "HBOMB-55" 
                vprintln 0 (mf() + sprintf ": case casebind other form 2 x=%A " (bsv_tnToStr xt))
                let (h0, rt) = sum_type_meet ww bobj mf h0 (ety, xt)
                vprintln 0 (mf() + sprintf ": case casebind other form 3 x=%A " (bsv_tnToStr rt))
                (tbcc, h0, items)



    //A function in Bluespec is not generally presented to the user as anything like Function^1#(numeric type nogv, type nogv_t) but it needs to be represented
    //with these args internally (as well as storing its name and arg and result types and body).  Then the nogv being free will not be a problem
    //and we will not need to mine or deepbind its args. One needs perhaps a type arg ordering convention of depth first, l-to-r walk order of listing with
    //repeats supressed and starting with the return type.
    and nf_typecheck_functiondef ww ee tbcc h0 (KB_functionDef3(proto, equatef, body, nameo, lp)) =
        let knot = ref None
        let (name, mm, sum_ty, gamma, cc2, h0) =
            match proto with
            | KB_functionProto(return_type_raw, name, formals_raw, provisos, lp) ->
                if nameo<>None && valOf nameo <> name then cleanexit(mf() + ": end name does not match start: " + name + " cf " + valOf nameo)
                let idl = name :: ee.static_path
                let mf() = ":" + name
                let ww = WF 3 "nf_typecheck_functiondef" ww (mf())
                //let prec = Some P_defn1 // proto is just a declaration normally, but here it is inside a definition
                let declared_rt = collect_sum1 ww bobj ee mf None (VP_value(false, [])) return_type_raw
                //dev_println (sprintf "swold: nf_typecheck_function %s:  declared_rt=%A return_type_raw=%A" (hptos idl) declared_rt return_type_raw)
                let formals = map (collect_sum2f ww bobj ee mf) formals_raw
                let provisos' = List.fold (proviso_parse ee) [] provisos
                let fid0 = {g_blank_fid with
                              fids=        map snd formals
                              methodname=  name
                              fprovisos=   provisos'
                              who=         "fid0"
                            }
                let gen_sumty who rt gamma = 
                    let arg_tys = map (fun (ty,fid) -> ty) formals
                    let fl1 = freshlister ww rt arg_tys
                    Bsv_ty_nom({g_blank_nom_typ_ats with nomid=idl; cls=Prov_func}, F_fun(rt, { fid0 with who=who; body_gamma=gamma }, arg_tys), fl1)

                let initial_sty = gen_sumty "fundef3-initial" declared_rt [] // For recursion
                knot := Some initial_sty
                //vprintln 0 (sprintf "Initial sty is  " + bsv_tnToStr initial_sty)
                if memberp name g_builtin_functions && not(nullp body) then cleanexit(mf() + " function is built in to the compiler and must not have a body in its prelude definition")
                let (gamma, rt, cc2) = // Why dont we need a deep bind here? Or else bind the freshlister answer.  We do need one! Please use it.
                    let tycheck_bind_fun_parameter (em:genenv_map_t) = function
                        | (ty, fid) ->
                            // vprintln 0 (sprintf "Want to bind also %A" ty)
                            let em = 
                                match ty with 
                                    | Bsv_ty_id (q, Some P_free, (tyname, bin)) ->
                                        em.Add([tyname], gen_GE_sumtype(tyname, {g_blank_bindinfo with whom="tycheck_bind_free_typram"}, Bsv_ty_id (q, Some P_free, (tyname, bin))))
                                    | _ -> em
                            //vprintln 0 (mf() + sprintf ": bind function formal %s with type %s" fid (bsv_tnToStr ty))
                            em.Add([fid], gen_GE_sumtype(fid, {g_blank_bindinfo with whom="tycheck_bind_fun_parameter_formal"; hof_route=Some fid; }, ty))
                    let ee_body = { ee with genenv=ee.genenv.Add([name], gen_GE_sumtype(name, {g_blank_bindinfo with whom="typecheck functionDe-incase-of-recusion"}, Bsv_ty_knot(idl, knot))); }
                    let ee_body = { ee_body with genenv=List.fold tycheck_bind_fun_parameter ee_body.genenv formals; callpath=CPS(PS_suffix, []) }
                    let cc1 = { tbcc with rv=Some declared_rt; tve=[] }
                    let (ee_, cc2, h0) = List.fold (typecheck_cmd ww) (ee_body, cc1, h0) body
                    match cc2.rv with
                        | None -> cleanexit(mf()  + " no return (but not needed for an Action)") // TODO not needed for an action
                        | Some rt -> (cc2.tve, rt, cc2)
                let (h0, rt) = sum_type_meet ww bobj mf h0 (declared_rt, rt)  
                let g2s (id, cp, ty) = id
                //vprintln 0 (mf() + sprintf " g/eye %i items body_gamma=%s" (length gamma) (sfold g2s gamma))
                let sum_ty = gen_sumty "fundef3-final" rt gamma
                (name, mf, sum_ty, gamma, cc2, h0)

        let nf_type = sum_nf ww bobj ee (mf() + " functionDef site") (h0, Some gamma) sum_ty // gamma put in twice - matters not.
        knot := Some nf_type
        (nf_type, mf, name) 

    and typecheck_module_statement ww (ifc_sty:bsv_type_t option) (ee, tbcc, h0, sofar, conflicts) arg =
        let qm = "typecheck_module_statement"
        if g_enable_drop_debug then
            //dev_println(qm + sprintf ": Drop Report %i" (length tbcc.tve))
            let drop_report (cps, ncp, ty) = dev_println(sprintf "  DROP FOR %s %s      \t\t%s"  cps (cpToStr ncp) (droppingToStr ty))
            app drop_report tbcc.tve


        if vd>=4 then vprintln 4 (qm + sprintf " typecheck_modstmt start. bg^=%i" (length tbcc.tve))
        match arg with                    
        | KB_block(blockname, lst, nameo, lp) ->
            let mm = lpToStr0 lp + ": generative blockStmt " + valOf_or blockname "anon"
            let ww = WF 3 qm ww mm
            if nameo<>None && blockname<>None && valOf nameo <> valOf blockname then cleanexit(mf() + ": end name does not match start: " + valOf blockname + " cf " + valOf nameo)
            List.fold (typecheck_module_statement ww ifc_sty) (ee, tbcc, h0, sofar, conflicts) lst
            
        | KB_for(s1, g, s2, body, lp) ->
            let (mm, ww, expanded) = prep_for ww qm arg
            typecheck_module_statement ww ifc_sty (ee, tbcc, h0, sofar, conflicts) expanded
            
            
        | KB_if(grd, tt, ffo, lp) ->
            let mm = lpToStr0 lp + ": generative IF statement"
            let ww = WF 3 qm ww mm
            let (tbcc, h0) = typecheck_bexp ww (ee, tbcc, h0) grd
            let (ee_, tbcc, h0, items, conflicts) = typecheck_module_statement ww ifc_sty (ee, tbcc, h0, [], conflicts) tt
            if nonep ffo then (ee, tbcc, h0, sofar, conflicts)
            else typecheck_module_statement ww ifc_sty (ee, tbcc, h0, sofar, conflicts) (valOf ffo)

        | KB_while(grd, body, lp) ->
            let mf() = lpToStr0 lp + " " + htosp ee.static_path + " generative while loop "
            let ww = WF 3 qm ww (mf())
            let (tbcc, h0) = typecheck_bexp ww (ee, tbcc, h0) grd
            typecheck_module_statement ww ifc_sty (ee, tbcc, h0, sofar, conflicts) body

        // Do we expect modules in modules or is this always the top one?
        | KB_moduleDef((KB_moduleSig(backdoors, mkname, parameters, iftype, args, provisos, lp)) as siga, body, nameo) ->  
            let mf() = lpToStr0 lp + ":" + qm + ": moduleSig"
            let idl = mkname :: (ee.prefix)
            //let prec = Some P_defn1
            let (mkname, backdoors, provisos, (parameters, args, iftype), (ifc_sty, maker_type, aliases), lp) = prep_module_sig ww bobj idl mf qm ee None siga
            sf "moddef stmt nested inside a module ? here"

          //ToyConnect2:35.
        | KB_easc((KB_functionCall(fn, args, (lp, uid))) as rhs, lp_) -> // Treat this as an alias for Empty e <- fn(args);
            // prep
            let ww = WN "Prefix Empty _ <- to freestanding function apply" ww
            let uid' = "$fix" + uid
            let ty = KB_id("Empty", (lp, uid')) // We need a textualid for this so steal the functionCall one.
            let lhs = KB_id (funique "_anon_", (lp, uid'))
            let arg' = KB_varDeclDo(Some ty, lhs, rhs, lp)
            typecheck_module_statement ww ifc_sty (ee, tbcc, h0, sofar, conflicts) arg'  


        | KB_letBinding(id, r, operator, (lp, uid)) -> // modstmt
            let mf() = lpToStr0 lp + " " + htosp ee.static_path + sprintf ": typecheck letBind (moduleStmt) id='%s' operator is '%s'" id operator
            let idl = [id]
            let ww = WF 3 qm ww (mf())
            let (tbcc, h0, sty) =
                let sags = ptypecheck_exp ww (ee, tbcc, h0) r
                let (tbcc, h0, sty) = if length sags = 1 then hd sags else muddy "HBOMB-6" 
                match operator with
                    | "<-" ->
                        match sty with
                            // The result type of function that is done by a 'do' is preversely its argument type. Modules anyway!
                            // TODO cf with remove an ActionValue wrapper.
                            | Bsv_ty_nom(idl, F_fun(rt, fats, [arg]), _) ->
                                // We should not need another copy of the dropping code!
                                //vprintln 0 (mf() + ": Want to make dropping here " + cpToStr ee.callpath + "\n" + whereAmI_str "\nXX")
                                (tbcc, h0, arg)

                            | other -> muddy(sprintf "letdo3 other form %A" other)                            
                    
                    | "=" -> (tbcc, h0, sty)
 
                    | other -> sf(mf() + ": bad let operator: " + operator)
            let ncp = extend_callpath bobj "L4311" uid ee.callpath
            let (tbcc, h0) = simple_store_type_annotation (ee, bobj) ww ee.genenv "L4311" mf tbcc h0 ncp sty
            let kv = (idl, gen_GE_sumtype(id, {g_blank_bindinfo with whom=operator}, sty))
            //vprintln 0 (mf() + ": typecheck type=" + bsv_tnToStr sty)
            let ee = { ee with genenv= ee.genenv.Add kv }
            (ee, tbcc, h0, sofar, conflicts)

        | KB_varDeclAssign(tyo, lhs, rhso, lp) -> // modstmt typecheck
            //let lpath = typrep_path ww bobj mm ee lhs
            let mf() = lpToStr0 lp + " typecheck_module_statement declAssign-A(a) " + (if nonep tyo then "nodecl " else "")
            let ww = WF 3 qm ww (mf())
            let (lhs_sty, ee, tbcc, h0) =
                match tyo with
                    | Some ast_ty ->
                        let (shortname, idl, stars, uid, lp_) = lhs_path_shortname ww mf lhs
                        let mm = mf() + shortname
                        //let prec = Some P_defn1   // This is a type definition but a variable declaration!
                        let ct = collect_sum1 ww bobj ee mf None (VP_value(false, [])) ast_ty
                        let mty_lhs = add_stars_decl stars ct
                        vprintln 3 (sprintf "typecheck DECL-A %A stars=%i '%s'" shortname stars (bsv_tnToStr mty_lhs))
                        let (tbcc, h0) = store_type_annotation (ee, bobj) ww ee.genenv "L3983" "" mf tbcc h0 (extend_callpath bobj "L3963" uid ee.callpath) (Tanno_function(mty_lhs, [], [], [("CT", ct)]))
                        (mty_lhs, { ee with genenv=ee.genenv.Add(idl, gen_GE_sumtype(shortname, {g_blank_bindinfo with whom=mm }, mty_lhs)); }, tbcc, h0)
                    | None ->
                        let (lhs_sty, lhs_uid, tbcc, h0) = typrep_path ww bobj ptypecheck_exps mf ee tbcc h0 (Prov_asReg_asIfc "LHS-DECL-A") lhs
                        (lhs_sty, ee, tbcc, h0)
            //vprintln 0 (mf() + ": Now do rhs")
            match rhso with
                | Some rhs ->
                    let sags = ptypecheck_exp_as ww (ee, tbcc, h0) (Prov_sty lhs_sty) rhs
                    let (tbcc, h0, rhs_sty) = if length sags = 1 then hd sags else muddy "HBOMB-303"
                    //vprintln 0 (mf() + ": SILLA lhs sty= " + bsv_tnToStr lhs_sty)
                    //vprintln 0 (mf() + ": SILLA rhs pre auto_apply " + bsv_tnToStr rhs_sty)
                    let asReg = (Prov_None "SILLA")
                    let filler = B_hexp(Bsv_ty_integer, xi_num 0) // nonse - we only care about type here.
                    let (ty4, path4_, vale4_) = prep_auto_apply ww bobj mf asReg (Some lhs_sty) lp (rhs_sty, 0, filler) // idiomatic arg set - todo refactor
                    let (h0, _) = sum_type_meet ww bobj mf h0 (lhs_sty, ty4)
                    (ee, tbcc, h0, sofar, conflicts)
                | None -> (ee, tbcc, h0, sofar, conflicts)
        
        | KB_varDeclDo(tyo, lhs, rhs_raw, lp) -> // typecheck modstmt
            let mf() = lpToStr0 lp + " typecheck_module_statementDECL-D " + (if nonep tyo then "nodecl " else "")
            let mm = mf()
            let ww = WF 3 qm ww mm
            let rhs_gens = prep_module_app ww mf rhs_raw
            let (shortname, idl, stars, lhs_uid, lp_) = lhs_path_shortname ww mf lhs
            let mf() = lpToStr0 lp + " typecheck_module_statementDECL-D (declaration/do)" + (if nonep tyo then "nodecl " else "") + " lhs=" + shortname + " "
            let ww = WF 3 qm ww (mf())
            let ee =
                match tyo with
                    | Some ast_ty ->
                        //let prec = Some P_defn1   // This is a type definition but a variable declaration!
                        let mty_lhs = add_stars stars (collect_sum1 ww bobj ee mf None (VP_value(false, [])) ast_ty)
                        if vd >=4 then vprintln 4 (mf() + sprintf ": stosh typecheck DECL-D '%s' mty_lhs=%A " shortname (bsv_tnToStr mty_lhs))
                        { ee with genenv=ee.genenv.Add(idl, gen_GE_sumtype(shortname, {g_blank_bindinfo with whom=mm }, mty_lhs)) }
                    | None -> ee
            let (lpath_ifc_sty, lhs_uid, tbcc, h0) = typrep_path ww bobj ptypecheck_exps mf ee tbcc h0 (Prov_asReg_asIfc "lhs-DECL-D2") lhs
            let outer_ncp = extend_callpath bobj "L4006" (if g_smell then shortname else lhs_uid) ee.callpath
            let m_droppings_to_commit = ref []
            let temp_store_type_annotation ww site mf ncp innerf arg valeo = // Make a tentative dropping in a mutable that can be abandoned if needed.
                //dev_println(mf() + sprintf ": varDeclDo RETCAM dropping made for ncp=%A" ncp)
                mutadd m_droppings_to_commit (site, mm, ncp, innerf, arg, valeo)
            // We want to typecheck all possible rhs forms against this lhs.
            // TODO has grd_ already been checked?
            let rec check_each_gen (tbcc, h0) arg =
                let qm = "check_each_gen"
                match arg with
                | (grd_, (mm, generator, Some mc, provisos', Some(args__, evc, args_sans_evc), rhs_uid)) -> // meta-makes: warnings and so ons.
                    let sags = ptypecheck_exps ww (ee, tbcc, h0) args_sans_evc
                    let (tbcc, h0, a_args_ty) = if length sags = 1 then hd sags else muddy "HBOMB-99"
                    let argpairs = map (fun x -> (funique "meta-makes", x)) a_args_ty
                    temp_store_type_annotation ww "L4037" mf outer_ncp false (Tanno_function(g_ty_sum_void, [], [], argpairs)) None
                    //We do not need need to store the inner_ncp dropping for a pseudo/meta make, just the outer.
                    //let inner_ncp = extend_callpath rhs_uid ee.callpath                    
                    //let _ = temp_store_type_annotation (ee, bobj) ww mm inner_ncp false (Tanno_function(g_ty_sum_void, [], argpairs)) None                    
                    (tbcc, h0)

                | (grd_, (mm, generator, None, provisos', None, uid)) ->
                      check_each_gen (tbcc, h0) (grd_, (mm, generator, None, provisos', Some([], [], []), uid)) // Add a dummy apply and go around again.

                | (grd_, (mm, generator, None, provisos', Some(args__, evc, args_sans_evc), rhs_uid)) ->                    
                    let sags = ptypecheck_exps ww (ee, tbcc, h0) args_sans_evc
                    let (tbcc, h0, a_args_ty) = if length sags=1 then hd sags else muddy "HBOMB-61"

                    let rec degen = function // THIS CAN ALL GO IN PREP ONCE RATHER THAN FOUR COPIES?
                        | Some(GE_overloadable(prototy, [(forwot, ((sty,fl_), item))])) ->  // if only one def then use it! refudge. TODO
                            (sty, Some item)
                        | Some(GE_overloadable(prototy, overloads)) ->
                            let give vd = vprintln vd (sprintf "%i overloads exist. They are for "  (length overloads) + sfold (fun (x, _) -> (sfold tyclassToStr x) + "||") overloads)
                            let pis = " overloaded typeclass discriminate 3/?"
                            let sigtys = map (digest_cooked mf(*() + pis)*)) a_args_ty
                            let candidates = List.filter (fun (pairs, _) -> map fst pairs=sigtys) overloads
                            let no = length candidates
                            let tailer (_, ((sty, _), vale)) = degen (Some(GE_binding([], {g_blank_bindinfo with whom=pis}, EE_e(sty, vale))))
                            if no = 1 then tailer(hd candidates)
                            else
                                let _ = give 0
                                vprintln 0 ("temp sigtys: " + sfold htosp sigtys)
                                vprintln 0 (mf() + sprintf "  --- %i suitable overloads in scope." no)
                                cleanexit(mf() + sprintf ": need exactly one overload for arg types %s but have found %i overloads." (sfold htosp sigtys) no)

                        | Some(GE_binding(_, _, EE_e(sty, vale))) -> (sty, Some vale)

                        | Some(GE_binding(_, _, EE_typef(_, sty))) -> (sty, None)
                        | Some(GE_normthunk(instf, idl, m_ans, reaped, _,  ff)) when not_nonep !m_ans -> degen !m_ans
                        | Some(GE_normthunk(instf, idl, m_ans, reaped, being_done, ff)) when not(nonep !being_done) -> sf ("hit 2 " + htosp idl)
                        | Some(GE_normthunk(instf, idl, m_ans, reaped, being_done, ff)) -> // idiomatic kate code - nth copy!
                            if not_nonep !being_done then sf (mf() + " recursion: being_done L3334")
                            let knot2 = ref None
                            being_done := Some(Bsv_ty_knot(idl, knot2)) //GE_normthunk(instf, idl, ans, reaped, being_done, ff))
                            let env_updates = ff ww { ee with genenv=ee.genenv.Remover [hd idl] } // [ hd idl ] ?
                            reaped := env_updates
                            being_done := None
                            let kate = function
                                | Aug_env(tyclass, bound_name, (dmtof, who, vale))->
                                    knot2 := Some(ee_sty mf vale)
                                    //vprintln 0 (sprintf "ribbish vale 22 %A" (eeToStr vale))
                                    m_ans := Some(GE_binding(bound_name, who, vale)) // TODO check k=hd idl! and check only one
                                | tt -> sf(sprintf "katey other '%s' %A" (htosp idl) tt)
                            app kate env_updates
                            degen !m_ans
                        | Some x -> muddy (sprintf "typecheck 1: rhs_preparer other binding: %A " x)
                        | None -> cleanexit(mf() + sprintf ":  module maker definition '%s' was not in scope" generator)
                    let (generator_sty, valeo) = degen (ee.genenv.TryFind[generator])
                    match generator_sty with // THIS SHOULD BE THE NORMAL FUN APPLY? There is no special code here post ec trimming
                        | Bsv_ty_nom(_, F_fun(Bsv_ty_nom(_, F_fun(monad_fun, fats, [rhs_ifc_virtuous_]), _), _, f_args_ty), _) -> 
                            let m_arglog = ref []
                            let scopebase = deb_baseline()
                            // Inner one wants to log for rhs_uid                            
                            let inner_ncp = extend_callpath bobj "L4070" rhs_uid ee.callpath
                            //let mm = mf() + " non-konstant-form doname=" + fats.methodname
                            //dev_println  (mf() + sprintf ": L2585 need to check lhs=%s  fid_types=%s rhs=%s for generator %s" (bsv_tnToStr lpath_ifc_sty) (sfold bsv_tnToStr f_args_ty) (sfold bsv_tnToStr a_args_ty) (bsv_tnToStr generator_sty))
                            let do_args_ty = [ lpath_ifc_sty ]
                            fun_arity_check mf fats.methodname f_args_ty a_args_ty
                            let (tbcc, h0) =
                                if nullp f_args_ty then (tbcc, h0) // varargs also included in null.
                                else List.fold (newsum_arg_temp ww bobj mf ee m_arglog) (tbcc, h0) (List.zip f_args_ty a_args_ty) // TODO want not temp and to make droppings on body...
                            temp_store_type_annotation ww "L4097" mf inner_ncp true (Tanno_function(lpath_ifc_sty, [], [], rev !m_arglog)) valeo

// legacy / very odd coding style?  Wrong?
                            let (h0, ifc_ty_ans) = (h0, lpath_ifc_sty) // sum_type_meet ww bobj mm ee cc.h (lpath_ifc_sty, lpath_ifc_sty)
                            //vprintln 0 (mf() + sprintf " DECL-D STOSH2 prct already bound?  %A"  (sfold hindToStr h)) 
                            temp_store_type_annotation ww "L4104" mf outer_ncp false (Tanno_function(g_ty_sum_monad, [], [], [(funique "konstant-form-anonarg", ifc_ty_ans)])) valeo
                            (tbcc, unscope scopebase h0)

                        | Bsv_ty_nom(_, F_fun(monad_fun, fats, [rhs_ifc_virtuous_]), _) ->
                            let mf() = mf() + " konstant-form doname=" + fats.methodname
                            //let scopebase = deb_baseline()
                            //vprintln 0 (mf() + sprintf ": 2597 need to check lhs=%s for generator %s" (bsv_tnToStr lpath_ifc_sty)  (bsv_tnToStr generator_sty))
                            //vprintln 0 (mf() + sprintf " DECL-D STOSH2 prct already bound?  %A"  (sfold hindToStr cc.h))
                            let (h0, ifc_ty_ans) = (h0, lpath_ifc_sty) // sum_type_meet ww bobj mm ee cc.h (lpath_ifc_sty, lpath_ifc_sty)
                            temp_store_type_annotation ww "L4109" mf outer_ncp false (Tanno_function(g_ty_sum_monad, [], [], [(funique "konstant-form-anonarg", ifc_ty_ans)])) valeo
                            (tbcc, h0)

                        | other_rhs -> cleanexit(mf() + " unexpected type on rhs of '<-' do operator: " + bsv_tnToStr other_rhs)
            let (tbcc, h0) = List.fold check_each_gen (tbcc, h0) rhs_gens 
            let committer (tbcc, h0) (site, m, ncp, innerf, dropping, valeo) =
                let (tbcc, h0) = store_type_annotation (ee, bobj) ww ee.genenv ("committer L4121:" + site) "" mf tbcc h0 ncp dropping
                (tbcc, h0)
            let (tbcc, h0) = List.fold committer (tbcc, h0) !m_droppings_to_commit
            if false then 

                let drop_report (cps, ncp, _) = dev_println(qm + sprintf ": Post commit drop report site=%s  %s " ""  (cps))
                app drop_report tbcc.tve
            (ee, tbcc, h0, sofar, conflicts)
            
        | KB_rule(pragmas_, rulename, condo, body, nameo, lp) ->
            let mm = lpToStr0 lp + " typecheck_module_statementrule + " + rulename
            let ww = WF 3 qm ww mm
            let scopebase = deb_baseline()
            let (tbcc, h0) = if nonep condo then (tbcc, h0) else typecheck_bexp ww (ee, tbcc, h0) (valOf condo)
            let (ee, tbcc, h0) = List.fold (typecheck_cmd ww) (ee, tbcc, h0) body       
            //let cc = { cc with h=unscope scopebase cc.h } -- commented out since causing regressions, but it should be ok really.
            (ee, tbcc, h0, sofar, conflicts)
            
        | KB_functionDef3(proto, equatef, body, nameo, lp) -> // typecheck function definitions local to a module. Also called for package functions. This does not deploy norm_functiondef because we are just checking types here. 
            let mm = lpToStr0 lp + (" typecheck_module_statementfunctionDef3")
            let ww = WF 3 qm ww mm
            let (nf_type, m, name) = nf_typecheck_functiondef ww ee tbcc h0 (KB_functionDef3(proto, equatef, body, nameo, lp))
            //vprintln 0 (mf() + ": g/eye typechecked (module local or package) function definition : normal'd sum_ty is " + bsv_tnToStr nf_type)
            let scopebase = deb_baseline()
            let ee = { ee with genenv=ee.genenv.Add([name], gen_GE_sumtype(name, {g_blank_bindinfo with whom="typecheck functionDef"}, nf_type)) }
            let bin = fresh_bindindex "-fd3b"
            let idl = name :: ee.static_path
            let nb = (Some nf_type, [(idl,bin)], VP_none "a-function")
            // Here we log it with its free type vars (beyond those bound at the next textually outer scope such as the moduleDef).  And we need to freshen it on retrieve as usual.
            let ww = WF 3 qm ww ("finished " + mm)
            let h0 = unscope scopebase h0
            (ee, tbcc, nb::h0, ([idl], nf_type)::sofar, conflicts)

        | KB_subinterfaceDef1 _
        | KB_subinterfaceDef00 _ ->
            let scopebase = deb_baseline()
            let (mm, name, bodyo, ebodyo) =
                match arg with
                    | KB_subinterfaceDef1(tyo, name, ebody, lp) ->        
                        let mm = lpToStr0 lp + " Assignment-style subinterfaceDef+" + name
                        (mm, name, None, Some ebody)

                    | KB_subinterfaceDef00(package_o, id0__, name, body, nameo, lp) ->
                        // TODO package_o should not be ignored.  Need to seek Jamey advice.
                        // TODO id0_ should not be ignored .. please make a test where this needs to be used to distinguish ... extend Test6 ...        
                        let mm = lpToStr0 lp + " Classical subinterfaceDef+" + name
                        if nameo<>None && valOf nameo <> name then cleanexit(mf() + ": end name does not match start: " + name + " cf " + valOf nameo)
                        (mm, name, Some body, None)
            let ww = WF 3 qm ww mm 

            let rec shouter prefix = function
                | Bsv_ty_nom(ikind, F_subif(iname, ty), tyargs_) ->
                    //vprintln 0 (mf() + " TEMP DISREG " + htosp ikind.nomid)
                    shouter (iname::prefix) ty
                | Bsv_ty_nom(iidl, F_ifc_1 ii, tyargs_) ->
                    //vprintln 0 (mf() + " TEMP PREFIX " + htosp prefix)
                    let sub_if_pred = function
                        | Bsv_ty_nom(sidl, F_subif(tag, _), _) when name=tag -> true
                        | Bsv_ty_nom(sidl, _, _) when sidl.nomid = [name] -> true
                        | _ -> false
                    match List.filter (sub_if_pred) ii.iparts with
                        | [] ->
                            let partname = function
                                | Bsv_ty_nom(sidl, F_subif(tag, _), _) -> "tag=" + tag + " kind=" + htosp sidl.nomid
                                | Bsv_ty_nom(_, F_fun(_, fats, _), _) -> "Method:" + fats.methodname
                                | Bsv_ty_nom(sidl, _, _) -> htosp sidl.nomid
                                | ty -> bsv_tnToStr ty                                

                            vprintln 0 ("Subinterface names available are: " + sfold partname ii.iparts)
                            cleanexit(mf() + sprintf ": Interface '%s' has no subinterface called '%s'" (htosp iidl.nomid) name)
                        | [item] -> item
                        | _ -> cleanexit(mf() + sprintf ": Interface '%s' seemingly has more than one subinterface called '%s'" (htosp iidl.nomid) name)

                | other -> sf (mf() + sprintf ": shouter other form %A" other)
            let ifc_sty' = shouter [] (valOf_or_fail "An outer interface should be provided [L3171]" ifc_sty)
            match bodyo with
                | Some ifc_body ->
                    List.fold (typecheck_module_statement ww (Some ifc_sty')) (ee, tbcc, h0, sofar, conflicts) ifc_body
                | None ->
                    let cc1 = { tbcc with rv=Some ifc_sty' }
                    let sags = ptypecheck_exp ww (ee, cc1, h0) (valOf ebodyo)
                    let (tbcc2, h0, rt1) = if length sags = 1 then hd sags else muddy "HBOMB-2"
                    let h0 = unscope scopebase h0
                    (ee, { tbcc2 with rv=tbcc.rv }, h0, sofar, conflicts)
                                 
        | KB_methodDef1 _
        | KB_methodDef2 _ ->
            let prec = None //Some P_defn1
            let rec tychec_ifc_component ifc_sty arg = // TODO make me a prep function
                match arg with
                | KB_methodDef1(return_type_option, methodname, formals, guard, body, nameo, lp) ->
                    let mf() = lpToStr0 lp + " typecheck methodDef1+" + htosp ee.static_path + ":" + methodname
                    if nameo<>None && valOf nameo <> methodname then cleanexit(mf() + ": end name does not match start: " + methodname + " cf " + valOf nameo)
                    (return_type_option, methodname, formals, guard, mf, Some body, None)

                | KB_methodDef2(return_type_option, methodname, formals, guard, methex, lp) ->
                    let mf() = lpToStr0 lp + " typecheck methodDef2+" + htosp ee.static_path  + ":" + methodname
                    (return_type_option, methodname, formals, guard, mf, None, Some methex)

            let (return_type_option, methodname, formals, guard, mf, bo, mo) = tychec_ifc_component ifc_sty arg
//no no: want lhs name not methodname?
            let ncp = extend_callpath bobj "L4207" methodname ee.callpath
            let ww = WF 3 m0 ww (mf())
            // The guard can contain a caseExpr that will augment the bound variables
            let (tbcc, h0) =
                match guard with
                    | None -> (tbcc, h0)
                    | Some guard -> typecheck_bexp ww (ee, tbcc, h0) (guard)

            let collect_sum3 ww bobj ee mm (declared_formal, formal) =
                match formal with
                | (Some tas, fid) -> (collect_sum1 ww bobj ee mf None (*Some (P_tlab "cs3")*) (gen_VP_value fid) tas, fid)
                | (None, fid)     ->
                    vprintln 0 (sprintf "+++ TODO check whether alpha renamed between then and now : using declared formal %A" declared_formal)
                    let ty =
                        match declared_formal with
                        | ty -> ty
                        // EdgeFIFOs line 91
                    (ty, fid)

            let rec match_ifc_sty = function
              | Bsv_ty_nom(iidl, F_subif(iname, sty), _) -> match_ifc_sty sty

              | Bsv_ty_nom(iidl, F_ifc_1 ii, _) ->
                let scanp = function
                    | Bsv_ty_methodProto(methodname', _,  mth_provisos, proto, _) -> methodname = methodname'
                    | Bsv_ty_nom(_, F_fun(_, fats, _), _) -> methodname = fats.methodname                                 
                    | _ -> false
                match List.filter scanp ii.iparts with
                    | [] ->
                           vprintln 0 (sprintf "Methods in '%s' are: " (htosp iidl.nomid) + sfold bsv_tnToStr ii.iparts)
                           cleanexit(mf() + sprintf ": method '%s' was not declared in exported interface definition " methodname + htosp iidl.nomid)
                    | a::b::_ -> cleanexit(mf() + ": method exists more than once in exported interface definition: " + htosp iidl.nomid)

                             // Dont need both these forms TODO
                    | [Bsv_ty_methodProto(methodname, prats, mth_provisos1, proto, Bsv_oty_fun(sty_, _, rt, args, mth_provisos2, _))] -> (rt, mth_provisos1@mth_provisos2 , map f2o3 args)
#if TEMP_OFFNOW
                    | [Bsv_ty_methodProto(methodname, prats, mth_provisos, proto, Bsv_ty_nom(_, F_fun(rt, _, args), _))] -> (rt, args)
#endif
                    | [Bsv_ty_nom(_, F_fun(rt, methodname', declared_formals), _)] -> (rt, [], declared_formals)
                    | other -> sf(mf() + sprintf ": other entry form %A" other)
              | other -> sf(mf() + sprintf ": typecheck: other form ifc %A" other)

            let (declared_rt, provisos, declared_formals) = match_ifc_sty (valOf_or_fail "An interface type is needed [L3221]" ifc_sty)
            fun_arity_check mf methodname declared_formals formals
            let formals = map (collect_sum3 ww bobj ee mf) (List.zip declared_formals formals)

            let (h0, unified) = unify_local ww bobj mf ee h0 (List.zip formals declared_formals)
            //vprintln 0 (mf() + sprintf ": now do RT %A cf %A" declared_rt return_type_option)
            let (h0, return_sty) = // TODO? somehow put return_sty in h.rv as an initial value?
                match return_type_option with
                    | None -> (h0, None)
                    | Some return_type_raw ->
                        let rt = collect_sum1 ww bobj ee mf prec (VP_value(false, [])) return_type_raw
                        let (h0, rtf) = sum_type_meet ww bobj mf h0 (declared_rt, rt)
                        (h0, Some rtf)
            let bind_method_parameter (m:genenv_map_t) = function
                | (ty, fid) ->
                    //vprintln 0 (sprintf "This dropping is concrete whereas the lhs decl-d one (outer) is not sufficiently back subbed on test6.  Make a dropping needed for method prams cp=%A fid=%s ty=%s" (ee.callpath) (fid) (bsv_tnToStr ty))
                    m.Add([fid], gen_GE_sumtype(fid, {g_blank_bindinfo with whom="bind_method_parameter"}, ty))
                //| other -> sf (sprintf "other fpof %A" other)                
            let swappair(a,b) = (b,a)
            let (tbcc, h0) = store_type_annotation (ee, bobj) ww ee.genenv "L4289" "" mf tbcc h0 ncp (Tanno_function(valOf_or return_sty g_action_ty, [], provisos, map swappair unified))
            let ee_body = { ee with genenv=List.fold bind_method_parameter ee.genenv unified }
            //now typecheck method body if present - need to copy this binding of formals first to 
            let (tbcc, h0) =
                match bo with
                    | None -> (tbcc, h0)
                    | Some body ->
                        let cc1 = { tbcc with rv=return_sty }
                        let (ee_, cc2, h0) = List.fold (typecheck_cmd ww) (ee_body, cc1, h0) body
                        ({ cc2 with rv=tbcc.rv }, h0)
            (ee, tbcc, h0, sofar, conflicts)


        | KB_typedefSynonym(KB_type_dub(_, name, generics, lp), ty, lp_) ->
            let mf() = lpToStr0 lp + " typecheck module typedef"
            let ww = WF 3 m0 ww (mf())
            let augs = operate_typedefSynonym ww vd bobj mf None ee arg  // typecheck_module_statement call site.
            let ee = List.fold (augment_env_entry ww bobj mf "typedefSynonym") ee augs
            (ee, tbcc, h0, sofar, conflicts)

        | KB_returnStmt(e, (lp, uid)) ->
            let mf() = lpToStr0 lp + " " + htosp ee.static_path + ": typecheck_module_statement returnStmt"
            let ww = WF 3 m0 ww (mf())
            let (et, e') = norm_bsv_exp_rmode_tyo ww bobj ee ifc_sty (Prov_None "modstmt returnStmt") e 
            match ifc_sty with
                | None     -> sf (mf() + ": module has no interface yet has a return statement")
                | Some ifc ->
                    //vprintln 0 (mf() + sprintf ": typecheck module resultis %A cf %A" et ifc)
                    let (h0, ret_sty) = sum_type_meet ww bobj mf h0 (et, ifc)
                    let (tbcc, h0) = 
                        let ncp = extend_callpath bobj "L4371" uid ee.callpath
                        simple_store_type_annotation (ee, bobj) ww ee.genenv "L4371" mf tbcc h0 ncp ret_sty
                    (ee, tbcc, h0, sofar, conflicts)

        | KB_scheduleOrder _
        | KB_scheduleMatrix _ ->
            let conflict_spec = tynorm_schedule ww bobj true "typecheck module stmt" arg
            (ee, tbcc, h0, sofar, (ee.static_path, conflict_spec)::conflicts)

        | other -> sf (mf() + sprintf ": typecheck other module stmt: " + astToStr other)

    // Apply to each module statement in turn.
    let ans = List.fold (typecheck_module_statement ww ifc_sty) (ee, g_null_tb, [], [], []) body // end of typecheck_topper 
    ans
    
//---------------o-----------
// Create the interface currency (i.e. the rdy, enable, arg inputs and return value) for each method in an instance of an interface.
//
// If this is internal to a hierarchic structure we declare them as LOCAL and ignore the INPUT/OUTPUT attribute.   
and mk_ifc_currency ww (bobj:bsvc_free_vars_t) pass mf emits_o (toplevel, used) borrowed separate_box ee (interfce_type_, ats1, ifc_name, (Bsv_ty_nom(ikind, (*ats0*) F_ifc_1 ifc_rec, _) as ifc_sty)) =
    let vd = bobj.misc_vd
    let internalf = nonep toplevel
    //let emits_o = if pass.pass_no = 0 then Some emits else None
    let m0 = sprintf "Consider making mk_ifc_currency for %s (internalf=%A) (emits_live=%A" (htosp ifc_name) internalf (not_nonep emits_o)
    let ww = WF 3 m0 ww (mf() + sprintf ": Start. type=" + bsv_tnToStr ifc_sty)

    let g_in    = if internalf then g_net elif used then g_output else g_input
    let g_out   = if internalf then g_net elif used then g_input else g_output   

    let bsv_net_conjoin tag path =
        // We prefer net names such as "EN_count_enable" for Bluespec whereas hpr library normally would create "count_enable_EN"
        if g_bsc_preferred_netnaming then
            if tag = "RV" then path else path @ [tag] // There is no RV suffix for the result bus in the Bluespec preferred way.
        else tag :: path

    let rec mksigma_item prefix igenerics sofar aa =
        match aa with
        | Bsv_ty_dontcare _ -> sofar  
            
        | Bsv_ty_methodProto(methodname, pragmas, provisos', protocol, m_sty) -> muddy "mksigma_item prefix igenerics m_sty"

        | Bsv_ty_nom(_, F_fun(rt_ty, matts, args), _) ->
            let proto      = matts.protocol
            let methodname = matts.methodname
            let iprefix = methodname :: (tacitate bobj.current_synth_target prefix) // aka iname aka dkey aka that1.
            let ww = WF 3 m0 ww (sprintf ": making method " + bsv_tnToStr aa)
            let rec arg_tgetting msg = function
                | Bsv_ty_id (_, _, (fid, _)) ->
                    //vprintln 3 (mf() + sprintf "tgetting iprefix=%A cp=%A ignerics=%A" iprefix ee.callpath igenerics)
                    match ee.genenv.TryFind [fid] with
                        | Some(GE_binding(_, whom, EE_typef(_, t))) ->
                            //vprintln 0 (msg + sprintf ": TGetting whom=%s fid %s from env ty=%s" whom fid (bsv_tnToStr t))
                            arg_tgetting msg t
                        | other -> sf(msg + sprintf ": mk_ifc_currency type formal '%s' had other form %A" fid other)

                | arg ->
                    vprintln 3 (m0 + " method " + mf() + " arg as type " + bsv_tnToStr arg)
                    enc_width_no ww ee mf arg
            // TODO need proper precedence of mth_ats over interface ats - or just disjunction them which is what this will tend to do.
            let local_test_att prat =
                // cf memberp prat (ats1 @ ats0 @ mth_ats)
                (test_att bobj.prags iprefix prat) ||
                (test_att bobj.prags ikind.nomid prat) || 
                (test_att bobj.prags (methodname::ikind.nomid) prat)
                
            let always_enabled = local_test_att (Pragma_alwaysEnabled)
            // Always enabled implies always ready.
            // Always ready does not imply can_be_ignored_for_schedulling since an always-readymethod such as _write take an argument.
            let always_ready = always_enabled || local_test_att Pragma_alwaysReady 

            let multiple_actions_allowed =
                let only_once = local_test_att Pragma_once_per_clock_cycle
                bobj.multiple_scalar_write_per_cycle && methodname = g_swrite && not only_once // Naive .. also LHR check Pragma_once_per_clock_cycle

            let specialmodel =
                if local_test_att Pragma_bluewire then SM_bluewire
                // elif multiple_actions_allowed then  ... // TODO bring ephemeral registers/arrays in here too
                else SM_notspecial 
            let m_ats =
                {
                    always_ready=    always_ready
                    always_enabled=  always_enabled
                    synchronised=    local_test_att Pragma_synchronised
                }
            if vd >= 4 then vprintln 4 (sprintf "create currency entry: ikind=%s prefix=%s methodname=%s Always_ready=%b Always_enabled=%b" (htosp ikind.nomid) (htosp prefix) methodname always_ready always_enabled)
            let (ar, ae) = (always_ready, always_enabled)
            let actionf = match proto with
                            | BsvProtoActionValue _
                            | BsvProtoAction         -> true
                            | BsvProtoValue(tya)     -> false
                            | NoProto -> sf (mf() + ": Proto needed for method used in hardware form.")

            let rv0 = match proto with
                        | BsvProtoActionValue tya
                        | BsvProtoValue(tya) ->
                            let rv0 = arg_tgetting ("width parameter for rv " + methodname) tya
                            //vprintln 0 (sprintf "Enc width for %A was %A" tya rv0)
                            Some rv0
                        | NoProto -> sf (mf() + ": Proto needed for method used in hardware form.")
                        | BsvProtoAction     -> None
            let resats =
                {
                    actionf=                          actionf
                    can_be_ignored_for_schedulling=   multiple_actions_allowed || (always_ready && nullp args)
                    idempotentf=                      not(local_test_att Pragma_non_idempotent)
                    specialmodel=                     specialmodel
                }

            let ae = match proto with
                        | BsvProtoAction  
                        | BsvProtoActionValue _ -> always_enabled
                        | BsvProtoValue _ -> true
            let mk_net_manifest = function
                | (name, tya)   -> (arg_tgetting (methodname + ": width parameter for " + name) tya, name)

            // Create the nets for a standard handshake to a component: includes RDY, EN, return bus and arg busses.
            let count = (if nonep rv0 then 0 else 1) + (if ae then 0 else 1) + (if ar then 0 else 1) + length args

            let cctrl =
                match toplevel with
                    | None -> None
                    | Some None -> Some None
                    | Some (Some pairs) ->
                        match op_assoc methodname pairs with 
                            | Some sigma -> Some(Some sigma)
                            | None -> sf(sprintf "failed looking for method %s in pairs %A" (htosp iprefix) (map fst pairs))
            let nl_args =

                match cctrl with
                    | Some(Some tl_nets) -> // Order is RDY, EN, RV, ARGS
                        let arg_o = (if ar then 0 else 1) + (if ae then 0 else 1) + (if nonep rv0 then 0 else 1)
                        let tl_arg tl_nets x = select_nth (x+arg_o) tl_nets
                        if length tl_nets <> count then hpr_warn(mf() + sprintf ": wrong number of tl_nets provided: should be %i not %i\n %A" count (length tl_nets) tl_nets)
                        //muddy(sprintf "ok %A" tl_nets) // Order is always { RDY, EN, RV, args } but certain elements are often missing.
                        let l = length args
                        map (fun x -> (xToStr x, x, {encol=[]})) (map (tl_arg tl_nets) [0..l-1])
                    | None       //-> map (fun (ty_width, name) -> (name, g_net emits (name :: iprefix, ty_width), X_undef)) (map mk_net_manifest args)
                    | Some(None) ->
                        let p = List.zip matts.fids args
                        map (fun (ty_width, name) -> (name, g_in emits_o (name :: iprefix, ty_width, []), {encol=[]})) (map mk_net_manifest p)

            let en = if ae then None
                     else
                         match cctrl with
                             | Some (Some tl_nets) ->
                                 let en_o = (if ar then 0 else 1)
                                 Some(select_nth en_o tl_nets)
                             | None  //    -> Some(g_net emits_o (bsv_net_conjoin "EN" iprefix, 1))
                             | Some None ->
                                 let ats = [ Nap(g_port_instance_name, hptos iprefix) ]// For restructure to understand the handshake nets, these attributes are needed.
                                 let ats = if bobj.handshake_done_by_restructure then  Nap(g_logical_name, "req") :: ats else ats
                                 Some(g_in emits_o (bsv_net_conjoin "EN" iprefix, 1, ats))

            let rdy = if ar then None
                      else
                          match cctrl with
                              | Some (Some tl_nets) ->
                                  let rdy_o = 0
                                  Some(select_nth rdy_o tl_nets)
                              | None   //   -> Some(g_net emits_o (bsv_net_conjoin "RDY" iprefix, 1))
                              | Some None ->
                                  let ats = [ Nap(g_port_instance_name, hptos iprefix) ]
                                  let ats = if bobj.handshake_done_by_restructure then Nap(g_logical_name, "reqrdy") :: ats else ats // For restructure to understand the handshake nets, these attributes are needed.
                                  Some(g_out emits_o (bsv_net_conjoin "RDY" iprefix, 1, ats))  

            let ty_rv =
                if nonep rv0 then RV_noret
                else
                    match cctrl with
                        | Some(Some tl_nets) ->
                            let rv_o = (if ar then 0 else 1) + (if ae then 0 else 1)
                            //vprintln 0 (sprintf "selecting %i as RV net from %A" rv_o tl_nets)
                            RV_netlevel(rt_ty, select_nth rv_o tl_nets)
                        | None     // -> RV_netlevel(rt_ty, g_net emits (bsv_net_conjoin "RV" iprefix, valOf rv0, []))
                        | Some None -> RV_netlevel(rt_ty, g_out emits_o (bsv_net_conjoin "RV" iprefix, valOf rv0, []))                        

            let proto' = match proto with
                          | BsvProtoAction
                          | BsvProtoActionValue _  ->
                              if en=None then BsvValue None
                              else BsvAction(rdy, [], valOf en)
                          | BsvProtoValue ty       -> BsvValue rdy
            //dev_println (sprintf "%i nl_args. Made trace proto for '%s' = %A" (length nl_args) (htosp iprefix) ty_rv)
            //let hintname = htosp (methodname :: iprefix) + "^2^" + i2s(length nl_args)
            let id3 = (ikind.nomid, iprefix, methodname)
            Sigma(id3, resats, (proto', ty_rv, m_ats), nl_args) :: sofar

        | other -> sf (sprintf "other ifc_currency (not method/subinterface) item %A" other)


    and ifc_lower prefix igactuals cc = function
        | Bsv_ty_nom(ikind, F_ifc_1 ii, igactuals_x) ->
            let reentrantf = internalf && bobj.reentrant_methods && not separate_box
            vprintln 3 (sprintf "Consider make ifc_currency for interface %s  ikind=%s  separate_box=%A  reentrantf=%A" (htosp prefix) (htosp ikind.nomid) separate_box reentrantf)
            //(* + " nf=" + i2s(length igformals)*) + sprintf ": actuals=%A  rpt=" igactuals_x + g_report igactuals)
            let genbinds =
                let fox = function
                    | Tty(ty, s)      -> (s, ty)
                    //| Tid(_id, tyfid) ->
                    | other -> sf (sprintf "genbinds: other fox %A" other)
                map fox igactuals_x

            let (subifs, meths) =
                let subif_pred = function
                    | Bsv_ty_nom(_, F_subif _, _) -> true
                    | _ -> false
                groom2 subif_pred ii.iparts
            vprintln 3 (mf() + sprintf ": ifc_lower: make ifc_currency for '%s' with %i methods and %i subinterfaces" (htosp prefix) (length meths) (length subifs))
            let cc =
                if reentrantf then 
                // perhaps MIRROR  B_subif  of string * bsv_exp_t * bsv_type_t * linepoint_t
                    vprintln 3 ("ifc_lower: Not making ifc_currency for ephemeral interface " + htosp prefix) // MIRROR this should be a method-by-method decision. TODO
                    // If the methods are in too then remapping the TRACE_reentrant is not sufficient
                    cc //(prefix, TRACE_reentrant(prefix, genbinds, B_ephemeral_ifc(ikind, F_ifc_1 ii, igactuals_x)))
                elif not(nullp meths) then
                    vprintln 3 (sprintf "ifc_lower: Create TRACE_fa for %i methods" (length meths))
                    (prefix, Tfa(genbinds, List.fold (mksigma_item prefix igactuals) [] meths))::cc
                else
                    vprintln 3 ("ifc_lower: Preserving empty i/f " + htosp prefix + " in siggy")
                    (prefix, Tfa(genbinds, []))::cc
            //vprintln 0 (sprintf ("ifc_currency new item %A") nitem)
            let col cc = function
                | Bsv_ty_nom(_, F_subif(tag, ty), _) ->
                    let igactuals2 = [] // igenerics // TODO not really - get from subinterface ------------------------- and this is a use site of a 'debug' field
                    let prefix' = tag :: prefix
                    //vprintln 0 (sprintf ("mk_ifc_currency new sub item '%s' %A") (htosp prefix') ty)
                    if memberp prefix' borrowed then
                        vprintln 3 ("col: Not making ifc_currency for borrowed subinterface " + htosp prefix')
                        cc
                    else ifc_lower prefix' igactuals2 cc ty // We here render the meths of a subif.
            // The subinterfaces are all folded up and held in the currency under the interface's monica.
            let ans = List.fold col cc subifs
            let _ = WF 3 m0 ww (sprintf "Finished making ifc_currency for interface %s  nf=%i #^running_items=%i" (htosp prefix) (length igactuals) (length ans))
            ans
        | other -> sf (sprintf "other ifc_lower form %A" other)        
    let igactuals_top = [] // seems wrong but never looked at ?TODO
    let ans = ifc_lower ifc_name igactuals_top  [] ifc_sty
    let _ = WF 3 m0 ww (mf() + sprintf "finished. Total ifc_currency interfaces made  #total_items=%i" (length ans))
    ans // end of mk_ifc_currency



// Generate an hexp to be used as a top-level parameter to a synthesis unit.
and gen_pram_net ww io ty id =
    let ww = WN ("gen_pram_net + " + id) ww
    let width = enc_width_e ww (fun ()->"gen_pram_net") ty
    let ats = []
    let signed = Unsigned
    ionet_w(id, width, io, signed, ats)
    //let idl = [ id ]
    //vectornet_w(htosp idl, width)


// When we set tag to "" we want the tag itself not the field it labels.?
and gen_B_field_select ww bobj spare_upath_ (tag, vale, width, baser) =
    let tc arg = gen_B_field_select ww bobj spare_upath_ (tag, arg, width, baser)
    let gbfs arg = (mk_bitty width, B_field_select(tag, arg, baser, width))//baser,width order deprecated
    match vale with
        | B_hexp(ty, X_undef) -> (ty, vale) // idiomatic en masse
        | B_action(_, (Bsv_eascActionStmt(ae, _))::tt_, _, lp) -> tc ae
        | B_action(_, (Bsv_resultis((ty, ae), _))::tt_, _, lp) -> tc ae
        // TODO action and thunk processing need automating...
        | B_aggr(ty, TU_enum(tag, no, width)) ->
                    sf ("did not expect field extract applied to enumeration type + " + tag)
                 // vprintln 0 (sprintf "+++can simplify now? Field select %s %i %i from %A" tag baser width vale)
            // Cannot get false match where tag names are shared because BSV is strongly typed.

        | B_aggr(ty, TU_tunion(vale, tag', no, totalwidth, tag_width)) ->
            if tag="" then (Bsv_ty_integer, B_hexp(Bsv_ty_integer, xi_num no)) 
            elif tag = tag' then
                //vprintln 0 ("field extracted tu: "  + tag)
                (ty, vale)
            else
                //vprintln 0 (sprintf "field extracted tu: MISMATCHED TAG wanted='%s' got='%s' " tag tag') 
                (ty, vale)
                
        | B_aggr(ty, TU_struct bobs) ->
            match op_assoc tag bobs with
                | None ->
                    //vprintln 0 ("field extracted struct: not present " + tag)
                    gbfs vale

                | Some(ty, w, pos, vale) ->
                    //vprintln 0 ("field extracted struct: " + tag)
                    (ty, vale)

                //Distribute over mux trees.
                //Distribute query over field_aggregate: TODO build this way!?
        | B_query(sf, g, ttl, ffr, rt, m) ->
            let (lt, l) = tc ttl
            let (rt, r) = tc ffr
            (rt, gen_B_query ww bobj (sf, g, l, r, rt, m))

        | B_reveng_l(ty, lst)-> // another distributive law
            let brev (g, v) =
                let (t, v) = tc v
                (t, (g, v))
            let (tys, vales) = List.unzip (map brev lst)
            (hd tys, B_reveng_l(hd tys, vales))
        | vale ->
            //vprintln 0 (sprintf "boring field select %s of %s" tag (bsv_expToStr vale))
            gbfs vale


and gen_B_dgtd ww bobj signed = function
    | ((_, B_hexp(lt, l)), (_, B_hexp(rt, r))) when num_constantp l && num_constantp r ->
        match xi_dltd(r, l) with
            | X_true  -> B_and []
            | X_false -> B_or  []
            | x -> sf (sprintf "dgtd: expected constants here. %A" x)

    | ((lt, l), (rt, r)) -> B_bdiop(V_dgtd signed, [l; r]) 

and gen_B_bdiop ww bobj oo lst =
    let mm = ref "gen_B_bdiop"
    let (tlst, vlst) = List.unzip lst
    //dev_println(sprintf "KB_bdiop oo=%A args=%s" oo (sfold bsv_expToStr vlst))
    match oo with
        | V_dgtd signed -> gen_B_dgtd ww bobj signed (hd lst, cadr lst)
        | V_dled signed -> gen_B_not(gen_B_dgtd ww bobj signed (hd lst, cadr lst))

        | V_deqd -> gen_B_deqd ww bobj (hd vlst, cadr vlst)
        | V_dned -> gen_B_not(gen_B_deqd ww bobj (hd vlst, cadr vlst))

        | V_dltd signed -> gen_B_dgtd ww bobj signed (cadr lst, hd lst)
        | V_dged signed -> gen_B_not(gen_B_dgtd ww bobj signed (cadr lst, hd lst))

        | oo -> B_bdiop(oo, vlst)

and gen_B_subtract bobj ww uid l r = gen_B_diadic bobj ww (fun()->"gen_B_subtract") V_minus uid (l, r)

and gen_B_diadic bobj ww (mf:unit->string) oo ty arg =
    let mm = "gen_B_diadic"
    match arg with
    | ((_, B_format(sl, al)), r) ->
        match r with
            | (_, B_format(sr, ar)) -> B_format(sl @ sr, al @ ar)
            | (ot, other) -> cleanexit(mf() + ": " + mm + ": invalid form to append to a format: " + bsv_expToStr other + " of type " + bsv_tnToStr ot)
    | ((lt_, B_hexp(lt, l)), (rt_, B_hexp(rt, r))) ->
        let prec = infer_prec g_bounda l r
        let unmasked = ix_diop prec oo l r
        let signed = Unsigned // can get from ty ?
        match ty with
            | Bsv_ty_integer -> B_hexp(ty, unmasked)
            
            | Bsv_ty_primbits ui when ui.width > 0 -> B_hexp(ty, ix_mask signed ui.width unmasked)
            //| other when ui.width < 0 ->  B_hexp(ty, unmasked)

            | other ->
                vprintln 3 ("+++ " + xToStr l + " ooo " + xToStr r + " : " + sprintf " Mask of another diadic type ? ty=%s" (bsv_tnToStr other))
                B_hexp(ty, unmasked)

    | ((lt_, B_action(_, [Bsv_resultis((lt, ae), _)], _, _)), r) -> gen_B_diadic bobj ww mf oo ty ((lt, ae), r) // TODO dont want to make this in first place
    | (l, (rt_, B_action(_, [Bsv_resultis((rt, ae), _)], _, _))) -> gen_B_diadic bobj ww mf oo ty (l, (rt, ae)) // ditto.


    | ((lt, B_string l), (rt, B_string r)) -> B_string (l+r)
    | ((lt, l), (rt, r)) ->
        let mm = "gen_B_diadic"
        if bsv_const_exp ww mf l && bsv_const_exp ww mf r 
        then
            let lc = bsv_lower ww bobj l
            let rc = bsv_lower ww bobj r
            let prec = infer_prec g_bounda lc rc
            let ans = ix_diop prec oo lc rc
            B_hexp(lt, ans)
        else B_diadic(oo, l, r, ty)


and gen_B_query ww bobj args =
    let mf() = "gen_B_query"
    let meet ty_l ty_r = ty_l // for now

    let is_undef = function
        | B_hexp(_, X_undef) -> true
        | _                  -> false
    let is_bundef = function
        | B_bexp(X_dontcare) -> true
        | _                  -> false
    let tailer (strictf, g, tt, ff, ty, meta) =

        match (is_bundef g, is_undef tt, is_undef ff) with
            | (_, true, true) -> tt
            | (true, _, _) -> tt
            | (_, true, _) -> ff
            | (_, _, true) -> tt
            | _ ->
                let eq1 = gen_B_deqd ww bobj (tt, ff) // See if the two args are the same.
                let eq = bsv_const_bexp ww mf eq1 && bsv_bmanifest ww bobj mf eq1
                let ans = 
                    if eq then
                        //vprintln 0 (sprintf "manifestly identical true and false query branches for %A" meta)
                        tt // If so then discard the query node entirely.
                    else
                        let cf = bsv_const_bexp ww mf g  // See if a constant guard predicate.
                        if cf && bsv_bmanifest ww bobj mf g then
                            //vprintln 0 (sprintf "manifestly true query branch for %A" meta)
                            tt 
                        elif cf then
                            //vprintln  0 (sprintf "manifestly false query branch for %A" meta)
                            ff
                        else B_query(strictf, g, tt, ff, ty, meta)
                //dev_println (sprintf "gen_B_query strict=%A const_true=%A uid=%A ans=%s" strictf eq meta (bsv_expToStr ans)) // TODO generates too much output
                ans

    match args with // spot common pattern and form disjunction of guards.
        | (s, g, t0, B_query(s1, g1, t1, f1, ty1, meta1), ty, meta) ->
            let eq1 = gen_B_deqd ww bobj (t0, t1)
            let eq = bsv_const_bexp ww mf eq1 && bsv_bmanifest ww bobj mf eq1
            if eq then gen_B_query ww bobj (s, gen_B_or[g; g1], t0, f1, meet ty1 ty, meta)
            else tailer (s, g, t0, B_query(s1, g1, t1, f1, ty1, meta1), ty, meta)

        | (s, g, B_query(s1, g1, t1, f1, ty1, meta1), f2, ty, meta) ->
            let eq1 = gen_B_deqd ww bobj (f1, f2)
            let eq = bsv_const_bexp ww mf eq1 && bsv_bmanifest ww bobj mf eq1
            if eq then gen_B_query ww bobj (s, gen_B_and[g; g1], t1, f1, ty1, meta)
            else tailer (s, g, B_query(s1, g1, t1, f1, ty1, meta1), f2, ty, meta)

        | (s, g, tt, ff, ty, meta) -> tailer (s, g, tt, ff, ty, meta)

and commit_pending_updates ww bobj mf sigma10 = // Commit blocking assignments by copying py all pending updates into statemap.
    vprintln 3 ("Commit_pending_updates " + mf())
    let m_count = ref 0
                
    let pm_commit (statemap:statemap_t) dkey =
        function
            | MS_denot(monica, whom, fg, (argty, argvale), subscript_o, cmd_name) ->
               let np = 
                   match statemap.TryFind dkey with
                       | Some(MS_denot(old_monica, whom, old_grd, (ty, old_vale), old_subscript_o, cmd_name_)) -> 
                           //dev_println(sprintf "Commit pending update %s (with merge) for %s" (mf()) (hptos dkey))
                           name_alias_check mf dkey (subscript_o, old_subscript_o)
                           let grd = ix_or fg old_grd
                           let dummy_meta = (g_builtin_lp, CP_none "denotational-commit-mux1")
                           let vale = gen_B_query ww bobj (false, B_bexp fg, argvale, old_vale, ty, dummy_meta)
                           let dubious = false // FOR NOW - TODO - cannot muti-update and multi-cycle at once currently!
                           let monica_resolved = if monica = old_monica then old_monica else (dubious, [funique "monica-pending-commit-resolved"])
                           MS_denot(monica_resolved, "resolved", grd, (ty, vale), subscript_o, cmd_name)
                       | None ->
                           mutinc m_count 1
                           //dev_println(sprintf "Commit pending update  %s for %s" (mf()) (hptos dkey))
                           MS_denot(monica, whom, fg, (argty, argvale), subscript_o, cmd_name)
               statemap.Add(dkey, np)
            //| other -> sf ("L5809 commit other form")
    let statemap = Map.fold pm_commit sigma10.statemap sigma10.pendingmap //pending goes last.
    vprintln 3 ("Commit_pending_updates " + mf() + sprintf " Committed %i." !m_count)
    { sigma10 with pendingmap=Map.empty; statemap=statemap }


        
and dims_decent ww bobj mf subs_lst =
    let rec vf = function
        | [(ty, a)] ->
            let v1 = bsv_manifest_32 ww bobj (fun ()->mf() + ": dims_decent 1") a
            if v1 < 0 then sf (mf() + sprintf ": Array  index negative in %s" (bsv_expToStr a))
            v1
        | (ty, a)::tt ->
            let v1 = bsv_manifest_32 ww bobj (fun ()->mf() + ": dims_decent 2") a
            if v1 < 0 then sf (mf() + sprintf ": Array  index negative in %s" (bsv_expToStr a))
            let r = vf tt
            r * v1 // just multiply during a declaration
            
        | other ->
            vprintln 0 (mf() + sprintf "----------------other dims %A" other)
            0
    vf subs_lst


and bsv_manifest_32 ww bobj mf a =
    let r = bsv_manifest ww bobj mf a
    in try int(r)
       with _ -> sf ("overflow in convert to int 32 with " + mf())
       
// Evaluate a boolean constant - most likely such expressions were evaluated (constant folded) as formed but this code handles any escapees. 
and bsv_bmanifest ww bobj mf aa =
    let vd = -1 // bobj.generate_loglevel
    let tc = bsv_manifest ww bobj mf
    let btc = bsv_bmanifest ww bobj mf
    if vd>=4 then vprintln 4 ("+++ bsv_bmanifest start " + bsv_bexpToStr aa)

    let backstop(bb) =
        vprintln 0 (sprintf "bsv_bmanifest: +++ will kludge lower on other bdiop %s ---> %s" (bsv_bexpToStr aa) (bsv_bexpToStr bb))
        let ans = bsv_blower ww bobj bb
        muddy "bsv_bmanifest: sho plmonket " // TODO keep me in case of other cases

    let ans = 
        match aa with
            | B_orred x -> tc x <> 0I
            | B_and[]
            | B_bexp X_true  -> true
            | B_or[]
            | B_bexp X_false -> false

            | B_and lst ->
                let rec ander = function
                    | [] -> true
                    | h::t -> btc h && ander t
                ander lst
            | B_or lst  ->
                let rec orer = function
                    | [] -> false
                    | h::t -> btc h || orer t
                orer lst        
            | B_bexp x ->
                if vd>=6 then vprintln 6 (sprintf "about to xi_bmanifest mm x %A" x)
                xi_bmanifest (mf()) x
            | B_not x -> not (btc x)

            | B_bdiop(bo, lst) -> btc (gen_B_bexp(ix_bdiop bo (map (bsv_lower ww bobj) lst) false))

            | other -> backstop(other)        
    if vd>=4 then vprintln 4 (sprintf "+++ bsv_bmanifest answer=%A" ans)
    ans
                  
and bsv_manifest ww bobj (mf:unit->string) aa =
    let tc = bsv_manifest ww bobj mf
    let btc = bsv_bmanifest ww bobj mf
    match aa with
    | B_blift x -> if btc x then 1I else 0I
    | B_hexp(_, n) -> xi_manifest_int "bsv_manifest" n
    | B_action(_, Bsv_resultis((_, ae), _)::_, _, _)
    | B_action(_, Bsv_eascActionStmt(ae, _)::_, _, _) -> // another idiomatic to unify
        bsv_manifest ww bobj mf ae

    | B_aggr(nomty, it) -> bsv_lower_m ww bobj mf aa // On the basis that ...
    | B_string ss -> BigInteger(string_heap_find true ss)

    | B_applyf(PI_biltin _, rt, _, _) -> bsv_lower_m ww bobj mf aa // Use the lower function for all built-ins
    | arg ->
        vprintln 0 ("+++Using lower to manifest constant find in expression " + bsv_expToStr arg)
        bsv_lower_m ww bobj mf aa

//
// Convert a boolean expression to hbexp_t form (lower from bsv form).
//           
and bsv_blower ww (bobj:bsvc_free_vars_t) x =
    let tc = bsv_lower ww bobj
    let btc = bsv_blower ww bobj
    match x with
    | B_or lst    -> List.fold (fun c x -> xi_or(btc x, c)) X_false lst
    | B_and lst   -> List.fold (fun c x -> xi_and(btc x, c)) X_true lst


    | B_bexp x    -> x
    | B_not x     -> xi_not(btc x)

    | B_bdiop(bo, lst) -> ix_bdiop bo (map tc lst) false

    | B_orred x   -> xi_orred(tc x)
    | arg -> sf ("Could not convert boolean to bits expression " + bsv_bexpToStr arg)

and bsv_exp_eq bobj (k,x) =
    let ww = WW_end
    if (bobj.norecurse_flag) then false
    else
        let eq1 = gen_B_deqd ww bobj (k,x)
        let mf() = "built-scanner"
        let eq = bsv_const_bexp ww mf eq1 && bsv_bmanifest ww bobj mf eq1
        eq

// Convert an expression to hexp_t form (lower from bsv form).
// This is a wrapper around bsv_lower_serf.
and bsv_lower ww (bobj:bsvc_free_vars_t) x =

    let xi_cost arg = // logic comb depth cost metric wanted (not logic area?)
        let costvec = bevelab_xi_cost bobj.logic_cost_walker arg
        match costvec with
            | CV_single cost -> cost
            | CV_stranded costvec_entry -> costvec_entry.maxa

    let x' =
        let hh = abs(myhash x)
        let ov = bobj.lowered_expressions1.TryFind (bsv_exp_eq { bobj with norecurse_flag= true; }) hh x
        //vprintln 0 (sprintf " tryfind %x %A" hh (nonep ov))
        match ov with
            | Some ov ->
                ov.refcount := !ov.refcount + 1
                if nonep !ov.cost then ov.cost := Some(xi_cost ov.orig)

                let ans =
                    if valOf !ov.cost < bobj.subevalcost then ov.orig 
                    elif !ov.net <> None then valOf !ov.net
                    else
                        ov.width := Some(xi_width_or_fail "built_expression memoise" ov.orig)
                        ov.name  := Some(funique "toy") // TODO use local name seed
                        ov.net   := Some(vectornet_w (valOf(!ov.name), valOf(!ov.width)))
                        valOf(!ov.net)
                //vprintln 0 ("Lowered " + bsv_expToStr x + " using cached " + xToStr ans)
                ans
            | None ->
                let nomemo = function
                    | B_pliStmt _ -> true // None of these (currently) returns a value and they are not ref-transparent so they cannot be shared subexpressions. A subexpression containing one does at least return a result, even if evaluated at the wrong time (order of $display outputs may be permuted).
                    | _ -> false

                let x' = bsv_lower_serf ww bobj x

                if nomemo x then x'
                else
                    let (already, ov) = bobj.lowered_expressions2.TryGetValue x'
                    let node =
                        if already then // Two source forms have lowered to the same expression. We share the node.
                            mutinc ov.refcount 1
                            // need to make orig a list and append ?
                            ov
                        else
                            let node = {parents = ref[]; orig=x'; cost=ref None; rootflag=ref false; refcount=ref 1; width= ref None; name= ref None; net= ref None; }
                            bobj.lowered_expressions2.Add(x', node) // Reverse index
                            node
                    bobj.lowered_expressions1.Add hh x node // Forward index
                    x'
    //vprintln 0 ("Lowered " + bsv_expToStr x + " giving " + xToStr x')
    //vprintln 0 (tok + " in=" + xbToStr gg + " out=" + xbToStr g)
    x'

// Convert an expression to hexp_t form (lower from bsv form).
and bsv_lower_serf ww bobj arg =
    let tc = bsv_lower ww bobj
    let tct (ty, v) = tc v
    let btc = bsv_blower ww bobj
    let bev_tc = bsv_lower_bev ww bobj
    let forcef = true
    //vprintln 0 ("bsv_lower_serf start " + bsv_expToStr arg)
    match arg with
    
    
    | B_shift(x, n) when n>0 -> ix_lshift (tc x) (xi_num n)
    | B_shift(x, n) when n<0 ->
        let x' = tc x
        ix_rshift (mine_prec g_bounda x').signed x' (xi_num(-n))
    | B_shift(x, n) when n=0 -> tc x

    | B_blift x -> xi_blift(btc x)
    | B_applyf(info, rt, args, tag) ->
        let mf() = lpToStr (fst tag) + " function/method lower to bits: " + pathToS info  
        let arity = length args
        match info with
            | PI_biltin "!" ->
                let a = tc(snd(hd args))
                let _ = cassert(length args = 1, "! needs one argument exactly")
                xi_blift(xi_not(xi_orred a))

// "div" ... etc here ...
            | PI_biltin("extend" as fn)     
            | PI_biltin("truncate" as fn)    
            | PI_biltin("zeroExtend" as fn) 
            | PI_biltin("signExtend" as fn)  -> 
                let _ = cassert(length args = 2, fn + " needs two arguments exactly after preprocessing")
                match map tc (map snd args) with // lower
                    | [(arg); (final)] ->
                        let from_w = xi_width_or_fail "sign extend from width" arg 
                        let to_w = xi_manifest "to width" final
                        //let xi_unsigned arg = ix_casting None { g_default_prec with widtho=None; signed=Unsigned; } CS_typecast arg
                        let signed =
                            let prec = mine_prec g_bounda arg
                            prec.signed=Signed // TODO should be return context that we check?
                        let ans =
                            if fn="extend" ||(signed && fn="signExtend") then
                                if to_w < from_w then hpr_warn(mf() + sprintf ": '%s' target is shorter %i cf %i" fn to_w from_w)
                                sign_extend from_w to_w arg
                            elif fn="zeroExtend" then arg // a nop
                            elif fn="truncate" then
                                if to_w > from_w then hpr_warn(xToStr arg + sprintf ": truncate makes wider! from=%i to=%i" from_w to_w)
                                ix_mask Unsigned to_w arg
                                //sf(sprintf "interim %i %i %A" from_w to_w arg)
                            else muddy("other extend/truncate form:" + fn)
                        ans

            | PI_biltin "warning"  | PI_biltin "error" | PI_biltin "message" // TODO are these forms still used?
            | PI_biltin "warningM"  | PI_biltin "errorM" | PI_biltin "messageM" -> xi_num 0
            
            | PI_biltin "pack"   when arity=1 -> tc(snd(hd args))
            | PI_biltin "unpack" when arity=1 -> tc(snd(hd args))
            | PI_biltin "fromInteger" when arity=1 -> tc(snd(hd args))            
            | other -> sf(sprintf "Cannot lower this function: %s" (bsv_expToStr arg))

#if FFF
                // The following gis rv is not correct for all applies but this does not matter.
                let gis = { rv= -1; args=map (fun _-> -1) args; nonref=true; eis=true; yielding=true; }
                xi_apply ((fname, gis), X_net(htosp ifc) :: (map tc args))
#endif
    | B_query(_, g, t, f, _, uid) ->
        let ans = ix_query (btc g) (tc t) (tc f)
        //vprintln 0 (sprintf " lower query uid=%A to %s" uid (xToStr ans))
        ans
        
    | B_hexp(_, n) -> n 
    | B_pliStmt(s, args, lp) ->
        let rec deformat = function
            | [] -> []
            | (Bsv_ty_nom(_, F_actionValue ty, _), f):: tt -> deformat((ty, f)::tt)
            | (Bsv_ty_format, B_format(s, a)):: tt -> muddy "giot1"
            | (Bsv_ty_format, f):: tt when false   -> muddy "giot - b"

            | (ot, other)::tt ->
                vprintln 0 ("deformat of " + bsv_tnToStr ot + " vvv=" + bsv_expToStr other)
                tc other :: deformat tt
        let tct_verbose (a, x) =
            let ans = tct (a, x)
            //if s = "hpr_write" then vprintln 0 (sprintf "write args   %s --TO---> %s " (bsv_expToStr x) (xToStr ans))
            ans
        let ans = xi_apply(hpr_native s, (map tct_verbose args))
        //let ans = xi_apply(hpr_native s, map (snd |> tc) args)
        ans

    | B_diadic(oo, a, b, ty) ->
        let (a', b') = (tc a, tc b)
        let prec = infer_prec g_bounda a' b'
        let ans = ix_diop prec oo a' b'
        //vprintln 0  (sprintf "uid=%A: diadic lower %A  l=%s r=%s  ans=%A" uid oo (bsv_expToStr a) (bsv_expToStr b) ans)
        ans

    | B_field_select(tag, arg, baser, width) ->  lower_field_select baser width (tc arg)

    | B_bit_replace(ty_, ov, nv, midwidth, baser, lp) ->
        let ov = tc ov
        let nv = tc nv
        let total = xi_width_or_fail "bit replace" ov
        let topwidth = total - baser - midwidth
        let botwidth = baser
        let topmask = if topwidth <= 0 then 0I else  himask total - himask (baser + midwidth)
        let botmask = if botwidth = 0 then 0I else himask botwidth
        let midmask = if midwidth = 0 then 0I else himask (baser+midwidth) - himask baser

        let mask w v = ix_bitand (local_xi_bnum w) v
        let ans = ix_bitor (mask (topmask+botmask) ov) (mask midmask (ix_lshift nv (xi_num baser)))
        ans

    | B_mask(width, e) -> ix_mask Unsigned width (tc e)
    | B_string s -> xi_string s
    | B_aggr(ty, TU_enum(tag, no, width)) -> xi_num no

    | B_aggr(ty, TU_tunion(vale, tag, no, totalwidth, tag_width)) ->
        let v0 = tc vale
        let prec = { widtho=Some totalwidth; signed=Unsigned; }
        ix_diop prec V_bitor v0 (ix_lshift (xi_num no) (xi_num (totalwidth-tag_width)))

    | B_aggr(ty, TU_struct pairs) ->
        let rec aggr_lower_qf = function
            | (tag, (ty, fw, fo, vale))::tt ->
                let v0 = tc vale
                //vprintln  0 (sprintf "qf agr %i %i %s" fo fw (xToStr v0))
                let v1 = if fw=0 then xi_num 0 else ix_lshift (ix_mask Unsigned fw v0) (xi_num fo)
                if nullp tt then v1
                else // if supports bits TODO
                    let prec = { widtho=None; signed=Unsigned; }
                    ix_diop prec V_bitor (aggr_lower_qf tt) v1
        aggr_lower_qf pairs
    | B_reveng_l(ty_, lst) ->
        let rec rwalk = function
            | []        -> xi_num 0 // or undef
            | [(_,x)]   -> tc x
            | (b,x)::tt -> ix_query b (tc x) (rwalk tt)
        rwalk lst

    | B_action(_, lst, _, lp) -> bev_tc lst

    | B_format _ ->muddy "B_format soilo"

    //B_ifc(path, Bsv_ty_nom(ats, _, _)) when ats.id = ["SSRAM_Cambridge_SP1D"; "BRAM"] -> 
    //  sf ("lower pong")

    | B_ifc _ -> sf (sprintf "Could not convert interface to bits expression - should have had an auto_apply of Read method?) %s %A" (bsv_expToStr arg) arg)

    | arg -> sf (sprintf "Could not convert to bits expression (not in bits typeclass?) " + bsv_expToStr arg)

and bsv_lower_bev ww bobj aa = 
    let tc = bsv_lower ww bobj 
    let btc = bsv_blower ww bobj
    let bev_tc = bsv_lower_bev ww bobj

    match aa with
    //| B_action(_, tt1, _, lp)::tt2 -> bev_tc (tt1 @ tt2)
    //| B_action(_, (Bsv_eascActionStmt(ae, a))::tt1, _, lp)::tt2 -> bev_tc (Bsv_eascActionStmt(ae, a) :: tt1 @ tt2)

    | [] -> X_undef
    | Bsv_varDecl(_)::tt  -> bev_tc tt // Variable declarations do not turn into h/w! So skip.

    | Bsv_beginEndStmt(lst, lp)::tt -> bev_tc (lst@tt)

    | [Bsv_eascActionStmt(ae, _)]  -> tc ae
    | Bsv_resultis((a_ty, ae), _)::_ -> tc ae

    | [Bsv_ifThenStmt(g, t, fo, strictf_, lp)] ->
        let g1 = btc g
        let t1 = bev_tc [t]
        let f1 = if nonep fo then X_undef else bev_tc[valOf fo]
        ix_query g1 t1 f1


    | other -> //lower_bev
        let rec checkterm = function
            | Bsv_resultis(ae, _)::_ -> true
            | [Bsv_eascActionStmt(ae, _)]  -> true
            | Bsv_ifThenStmt(g, t, fo, strictf_, lp)::tt -> checkterm [t] && (nonep fo || checkterm [valOf fo])
            | _ -> false
        let isterm = checkterm other
        sf (sprintf "bsv_lower_bev: other (term=%A len=%i): " isterm (length other) + sfold (fun x->"\n\n" + bsv_stmtToStr x) other)

and bsv_lower_m ww bobj mf b =
    let vd = bobj.build_loglevel
    let ww = WN (if vd>=4 then mf() else "bsv_lower_m") ww
    match bsv_lower ww bobj b with 
    //| B_string x -> xi_string x
        | x when int_constantp x -> xi_manifest_int "lower" x
        | other -> sf (mf() + ": bsv_lower_m not constant: other=" + sprintf "%A" other)

and bsv_lower_32 ww bobj mf b =
    let vd = bobj.build_loglevel
    let ww = if vd>=4 then WN ("bsv_lower_32: " + mf()) ww else ww
    match bsv_lower_m ww bobj mf b with 
    | (n:BigInteger) ->
        //vprintln 0 (sprintf "typeof n= %A" (n.GetType()))
        let c = try int(n)
                with overflow_ -> sf(sprintf "bsv_lower_32 overflow: '%A' " n)
        //vprintln 0 (sprintf "typeof b= %A" (b.GetType()))
        c
    //| other -> sf ("bsv_lower_32 other " + sprintf "%A" other)


and bsv_beval_pram bobj ww (env:genenv_map_t) expr = // cf ty_eval? // TODO this always aborts
    let btc = bsv_eval_pram bobj ww env
    let tc = bsv_eval_pram bobj ww env
    let tc1 arg =
        match tc arg with
            | EE_e (t, v) -> v // todo there is the _e versions below - use them
            | other -> sf "other104"
    let tc2 (ty, arg) =
        match tc arg with
            | EE_e (t, v) ->
                // TODO type reduce t with ty
                (ty, v)
            | other -> sf "other103"
    match expr with // TODO this always aborts
        | B_bdiop(V_dgtd signed_, [a;b]) ->
            let [(lv);(rv)] = map tc1 [a;b]
            muddy (sprintf "shighg %A  R=%A" lv rv)
            
        | other -> sf ("other in bsv_beval_pram exp " + bsv_bexpToStr other)


// bsv_eval_pram only serves to handle the somewhat strange Bluespec numeric type expressions.  A numeric value is passed as a type.
// This replicates ewalk code but without an intrinsic guard. Perhaps somewhat redundant since we could use ewalk and ignore the returned guard.
and bsv_eval_pram bobj ww (env:genenv_map_t) expr = 
    let mm = "unidoc_"
    let btc = bsv_beval_pram bobj ww env
    let tc = bsv_eval_pram bobj ww env
    let vd = bobj.build_loglevel
    let tc1 arg =
        match tc arg with
            | EE_e (t, v) -> v // todo there is the _e versions below - use them
            | other -> sf "other101"
    let tc2 (ty, arg) =
        match tc arg with
            | EE_e (t, v) ->
                // TODO type reduce t with ty
                (ty, v)
            | other -> sf "other102"                
    let mf() = ("bsv_eval_pram start " + bsv_expToStr expr)
    let ans =
        match expr with
            | B_abs_lambda _ -> EE_e(Bsv_ty_dontcare g_function_token, expr) // correct? - could close it but this would change the type of the body from ast to bexp.

//            | B_aggr(ty, TU_tunion(vale, tag, no, totalwidth, tag_width)) ->
            
            | B_fsmStmt _  ->  EE_e(Bsv_ty_fsm_stmt "", expr) // TODO need better DSL/reflected code type
            | B_hexp(t, v) ->  EE_e(t, expr)
            | B_diadic(oo, l, r, ty) ->
                match (tc l, tc r) with
                    | (EE_e(lt, l'), EE_e(rt, r')) ->
#if SPSPS
                        match retrieve_type_annotation bobj mf "" (abs_uid) with
                            | (Tanno_function(ans_ty, _, _, [("Ld",lt); ("Rd",rt)])) ->
                                EE_e(ans_ty, gen_B_diadic bobj ww mf oo abs_uid ((lt, l'), (rt, r')))
                            | _ -> sf "fox ohdear"
#endif
                       EE_e(ty, gen_B_diadic bobj ww mf oo ty ((lt, l'), (rt, r')))
            | B_query(_, g, tt, ff, _, meta) ->
                match btc g with
                    | other -> sf (sprintf "yihogo %A" other)
                
            | B_ifc(PI_ifc_hier ded, ty) -> // This is simply a B_var always ?
                match env.TryFind ded with // This is just an eval - please use that code - but this whole eval function is now done in norm?
                | None ->
                    let r = ref []
                    for z in env do mutadd r z.Key done 
                    vprintln 0 (htosp ded + " was not found in the env. All=" + sfold_unlimited htosp !r) // TODO this is ok after a redirect -> goes through ok ?   ifc_hier could just be a B_var
                    vprintln 0 (sprintf "+++no bind in env for ifc '%s': should not happen : except for vector subscript." (htosp ded))
                    EE_e(ty, B_ifc(PI_ifc_flat ded, ty))

                | Some(GE_binding(_, _, EE_e(sty, B_ifc(PI_ifc_flat ifc_idl, ty)))) ->
                    //vprintln 0 ("ifc " + htosp ded + " was found in the env as " + htosp ifc_idl)
                    // TODO here we changed ty but not sty! <--------------------------------------------------------------------
                    EE_e(sty, B_ifc(PI_ifc_flat ifc_idl, ty))
                | other -> sf (sprintf "unrecognised form: ifc_hier mapped to something that is not an interface?: %A" other)
                        
            | B_subif(iname_ss, vale, ty, lp) ->
                 EE_e(ty, expr) // Can do better - can get sumty out of the ifc
                
            | B_ifc(path_, ty) ->
                //vprintln 0 (sprintf "quok recode declaration %A" iidl)
                EE_e(ty, expr) // Can do better - can get sumty out of the ifc

            | B_applyf(PI_bno(e, nameo), rt, args, lp) ->
                let fff = tc e
                let rrr = map tc2 args
                let ty = Bsv_ty_dontcare "ap3509"
                EE_e(ty, B_applyf(PI_bno(tc1 e, nameo), rt, map tc2 args, lp)) // It is a functiontype/dontknow not a dontcare, but ... action requires no eval? It's code? The ifc to make action on may be a var that needs lookup.


            | B_dyn_b(action, ifc) -> EE_e(Bsv_ty_dontcare "dynb", B_dyn_b(action, tc1 ifc)) // its a functiontype/dontknow not a dontcare, but ... action requires no eval? It's code?

            | B_ferref _ 
            | B_var _ ->
                let varwork lp vname =
                    match env.TryFind vname with
                        | None ->
                            cleanexit(lpToStr0 lp + ": Bound=" + " env... " + "\n" + mm + ": unbound port, variable or function name: " + htosp vname)
                        | Some (GE_binding(_, whom, xx)) ->
                            //for z in env do vprintln 0 (sprintf "ITEM %s --> %A" (htosp z.Key) (z.Value)) done
                            if vd>=4 then vprintln 4 (sprintf " eval_iactualt %s whom=%s bound as %s" (htosp vname) whom.whom (eeToStr xx))
                            xx
                        | other ->
                            sf(lpToStr0 lp + ": Bound=" + " env... " + "\n" + mm + sprintf ": %s other form %A" (htosp vname) other)
                match expr with 
                | B_ferref(ty_, fer, lp) ->
                    dev_println (lpToStr0 lp + sprintf ": varwork of fer %A  type %A" fer ty_)
                    varwork lp [fer] // B_ferref should not be expected to work in this context?  We should look up fer in mutable env and during elab only.  If an interface, should already be B_ifc form.
                | B_var(dbi_, vname, lp) -> varwork lp vname 

            | B_string xx -> EE_e(mk_string_ty (strlen xx), B_string xx)
            | other ->
                sf ("+++other form in bsv_eval_pram exp " + bsv_expToStr other)
                //other
    //vprintln 0 (sprintf "bsv_eval_pram -> %A " (eeToStr ans))
    ans

and bsv_eval_pram_e bobj ww env expr =
    match bsv_eval_pram bobj ww env expr with
        | EE_e(t, e) -> e        
        | _ -> sf "value was needed but type found (implied valueof?)"


// Compile one statement in a module - typically an interface or module instance or a rule or a method def.
and bsv_compile_Stmt ww (bobj:bsvc_free_vars_t) pass (ifc_ty, inode, ifc_name) (g0:resource_records_t) toplevel emits_o batch_no (sigma0, ee) (stmt00, stmt_no) =
    let vd = bobj.build_loglevel
    let nprefix = ee.prefix

    let m0 =
        let mm = sprintf "bsv_compile_Stmt passname=%s src=%s" (pass.pass_name) (htosp ee.static_path) + " nprefix=" + htosp nprefix + sprintf " pass=%i stmt_no=%i.%i" pass.pass_no batch_no stmt_no // + bsv_stmtToStr stmt00
        //dev_println(sprintf "mm mm %s" mm)
        //if String.length mm > 301 then m.[0..300] + "..." else mm        
        ("Start " + mm)
    let ww = WF 3 "bsv_compile_Stmt" ww m0
    let m_m = ref m0
    let set_m_m arg =
        //dev_println(sprintf "set m_m %s to %s" m0 arg)
        m_m := arg
    let ncs = false // for now bconstantp g0 && xi_bmanifest m0 g0 = false
    if ncs then
        hpr_warn(!m_m + sprintf ": Statement '%s' can never be reached and is being discarded." m0)
        (sigma0, ee) // The reason for a pair of sigma0 and ee is that sigma is passed as an accumulator over the whole tree walks whereas ee operates in parallel with the callstack with entries deleted on return up the stack (except for ref/dict  mutations).
    else
    let rec walkgen (monica:monica_pair_t) (walkstack:key1_t list) env ((emits_o:emission_t option), firegrd, msg) = // alpha an option or use ix_andl ? TODO.
        let gen_firex alpha =
                { fire=          (if alpha=[] then xi_orred firegrd else ix_and (xi_orred firegrd) (hd alpha))
                  fpath=         (monica, 0)
                  rulename_msg=  msg 
                }

        let rec tl_walk alpha (gg:resource_records_t, sigma10) aa =
            ewalk ww (gg, sigma10) (gen_firex alpha) None aa // TODO only hd here. alpha should be an option.

        and ewalk ww ((gg:resource_records_t), sigma10) firex tyo aa =
            //dev_println (sprintf "ewalk: ig_alpha_find=%s" (ig_alpha_find_in_map gg.igs))
            let mf() = if vd>=5 then ("ewalk of " + bsv_expToStr aa) else "ewalk-L6535"
            let (gg, sigma10, xv) =
                if true || bobj.wverbosef then
                    let tok = funique "ewalk"
                    let ww = if true || vd>=4 then WF 4 "ewalk" ww (mf() + " token=" + tok) else ww
                    let (gg, sigma10, xv) = ewalk_memod (gg, sigma10) firex tyo aa
                    if true || vd>=4 then vprintln 4 (tok + " ewalk end with " + bsv_expToStr (snd xv) + " and type " + bsv_tnToStr (fst xv))
                    (gg, sigma10, xv)
                else ewalk_memod (gg, sigma10) firex tyo aa

            let xv' =
                //let (found, ov) = built_expressions.TryGetValue xv
                xv // FOR NOW
#if BUILT_EXPRESSIONS_CACHE
// turned off for now - the intrinsic guard mechanism needs factorising for cacheing to work.
                if found then
                    ov.refcount := !ov.refcount + 1
                    ov.cost := Some 0 // (xi_cost xv)
                    if valOf !ov.cost < 10 then xv // Make this 10 a parameter TODO
                    elif !ov.net <> None then valOf(!ov.net)
                    else
                        ov.width := Some(xi_width_or_fail "built_expression memoise" xv)
                        ov.name  := Some(funique "s_" + monica) // TODO use local name seed
                        ov.net   := Some(vectornet_w (valOf(!ov.name), valOf(!ov.width)))
                        valOf !ov.net
                else  
                    //built_expressions.Add(xv, {parents = ref[]; cost=ref None; rootflag=ref false; refcount=ref 1; width= ref None; name= ref None; net= ref None; }) 
                    xv

            vprintln 0 ("Built " + bsv_expToStr aa + " giving " + bsv_expToStr xv')
            vprintln 0 (tok + " in=" + xbToStr gg + " out=" + xbToStr gg)
#endif
            ((gg:resource_records_t), sigma10, xv')
        and ewalk_memod (gg, sigma10) firex tyo aa =
            match aa with

            | B_fsmStmt _ -> (gg, sigma10, (Bsv_ty_fsm_stmt "ewalk", aa))
            
            | B_abs_lambda _ -> (gg, sigma10, (Bsv_ty_dontcare "ewalk abs lambda", aa))  // Best to avoid this please.
            
            // First: These two clauses should no longer be needed if all vars as such are eval'd away in norm?
            | B_ferref(ty_, name, lp) ->
                //vprintln 0 ("Using B_ferref/B_var in ewalk " + htosp name)
                ewalk ww (gg, sigma10) firex tyo (bsv_eval_pram_e bobj ww env aa) // What about value from muts? It will be in the env as a redirect (GE_mut form) to a mut during norm, but here in compile everything is in the env.  So B_vector read should not be being used? TODO - CHECK

            // Second: These two clauses should no longer be needed if all vars as such are eval'd away in norm?
            | B_var(dbi_, name, lp) ->
                //vprintln 0 ("Using B_var in ewalk " + htosp name)
                ewalk ww (gg, sigma10) firex tyo (bsv_eval_pram_e bobj ww env aa) 

            | B_lambda(nameo, _, _, fun_ty , _, _) -> (gg, sigma10, (fun_ty, aa)) // These are not substituted out.

            | B_hexp(t, v) -> (gg, sigma10, (t, aa)) // These are not substituted out.

            | B_string s -> (gg, sigma10, (mk_string_ty(strlen s), aa)) // These are not substituted out.

            | B_aggr(ty, TU_tunion(vale, tag, no, totalwidth, tag_width)) ->
                //vprintln 0 ("de_aggr TU_union start "  + tag)
                let tyo = if nonep tyo || true then None  else muddy (!m_m + " we cannot create the expected type here without a structural typing system!")
                let (gx', sx', (_, v0)) = ewalk ww (gg, sigma10) firex tyo vale
                //vprintln 0 ("de_aggr TU_union end "  + tag)
                (gx', sx', (ty, B_aggr(ty, TU_tunion(v0, tag, no, totalwidth, tag_width))))

            | B_aggr(ty, TU_enum(tag, no, width)) ->
                //vprintln 0 ("de_aggr TU_enum start/end "  + tag)
                (gg, sigma10, (ty, B_aggr(ty, TU_enum(tag, no, width))))

            | B_aggr(ty, TU_struct bobs) ->                
                let de_aggr arg (gx, sx, sofar) =
                    //vprintln 0 ("ewalk: de_aggr + "  + fst arg)
                    match arg with
                        | (tag, (ty, fw, fo, vale)) ->
                            //vprintln 0 ("de_aggr TU_struct start "  + tag)
                            let tyo = if nonep tyo || true then None  else muddy (!m_m + " we cannot create the expected type here without a structural typing system!")
                            let (gx', sx', (vt, v0)) = ewalk ww (gx, sx) firex tyo vale
                            //vprintln 0 ("de_aggr TU_struct end "  + tag)                            
                            (gx', sx', (tag, (ty, fw, fo, v0))::sofar)

                let (gx, sigma11, bobs') = List.foldBack de_aggr bobs (gg, sigma10, [])
                //vprintln 0 ("de_aggr end")
                (gx, sigma11, (ty, B_aggr(ty, TU_struct bobs')))

            | B_field_select(tag, vale, baser, width) ->
                let mf() =(sprintf "ewalk: Compile a B_field_select  tag='%s' b=%i w=%i" tag baser width)
                let rec fldsel = function
#if XXXX
 these cases now all inside gen_B_field_select
                    | B_aggr(idl, TU_struct bobs) ->  // an aggregate before eval?  Shortcut?
                        // Hmmm... can this be non-strict, in that implicit guards of never accessed fields are bypassed.  TODO this is never used?
                        match op_assoc tag bobs with
                            | None -> sf (sprintf " tag '%s' not found in aggr %s" tag (bsv_expToStr aa))
                            | Some tunion -> muddy (sprintf "field select %s from  %A" tag tunion)

                    | B_aggr(idl, TU_union(e, tag', _, _, _)) ->  // an aggregate before eval?  Shortcut?
                        // Hmmm... can this be non-strict, in that implicit guards of never accessed fields are bypassed.  TODO this is never used?
                        if tag = tag' then
                            | None -> sf (sprintf " tag select '%s' from a different variant in %s" tag (bsv_expToStr aa))
                        else
                            muddy (sprintf "tag select %s from  tagged %A" tag e)
#endif
                    | oarg ->
                        let tyo = if nonep tyo || true then None  else muddy (!m_m + " we cannot create the expected type here without a structural typing system!")
                        let (g1, sigma11, (ft, a)) = ewalk ww (gg, sigma10) firex tyo oarg
                        let (fld_ty, a1) = gen_B_field_select ww bobj [  ] (tag, a, width, baser) // this SHOULD return the type ... 
                        //vprintln 0 (sprintf "Oarg backstop field select used for tag='%s' giving " tag + bsv_expToStr a1)
                        let ty =
                            match tag with
                                | "$BITSEL" -> mk_bitty 1  // this SHOULD be returned by gen_B_field_select
                                | _ -> fld_ty //muddy (sprintf "get_field_ty tag ft %A" ty_)
                        (g1, sigma11, (ty, a1)) // could use type dropping on this which is why we pass in a uid to gen_B_field_select but that is not used at the moment.
                fldsel vale

            | B_mask(width, arg) ->
                let (g1, sigma11, (ty, a1)) = ewalk ww (gg, sigma10) firex None arg
                let ans = gen_B_mask width a1
                (g1, sigma11, (Bsv_ty_integer, ans))

            | B_shift(arg, amount) ->
                let (g1, sigma11, (ty, a1)) = ewalk ww (gg, sigma10) firex None arg
                let ans = gen_B_shift amount a1
                (g1, sigma11, (Bsv_ty_integer, ans))
            
            | B_query(false, g, t, f, ty, (lp, abs_cp)) ->                 // Non-strict by qualifying the fireguard in each half.  TODO REVIEW PLEASE.
            // TODO obj.aggressive_conditions
                let (gg1, ss1, g') = ebwalk ww (gg, sigma10) firex g
                let (gg_t, ss2, (tty, t')) = ewalk ww (g_empty_resource_record, ss1) {firex with fire=ix_and g' firex.fire} tyo t
                let (gg_f, ss3, (fty, f')) = ewalk ww (g_empty_resource_record, ss2) {firex with fire=ix_and (xi_not g') firex.fire} tyo f
                let split = true // non-strict version
                let gg3 = ig_mux_dist split gg1 g' gg_t gg_f
                //vprintln 0 ("ewalk non-strict B_query " + bsv_expToStr aa + "\n   tt'= " + bsv_expToStr t' + "\n   ff'= " + bsv_expToStr f' + "\n   ans= " + bsv_expToStr ans)   
#if OLD
                let ty = 
                    match retrieve_type_annotation bobj !m_m abs_cp with
                        | Tanno_function(ans_ty, _, _, [("LQ",lt); ("RQ",rt)]) -> ans_ty
                        | other -> sf (sprintf "query (non-stretrieve anno other %A" other)
#endif
                let ans = gen_B_query ww bobj (false, gen_B_bexp g', t', f', ty, (lp, abs_cp))
                (gg3, ss3, (ty, ans))

            | B_query(true, g, t, f, ty, (lp, abs_cp)) ->                         // Strict version
                let (gg1, ss1, g') = ebwalk ww (gg, sigma10) firex g                
                let (g1, sigma11, [t'; f']) = efoldwalk ww (gg1, ss1) firex [(tyo, t); (tyo, f)]
#if OLD
                match retrieve_type_annotation bobj !m_m (abs_cp) with
                    | Tanno_function(ans_ty, _, _, [("LQ",lt); ("RQ",rt)]) ->
                        (g1, sigma11, (ans_ty, gen_B_query ww bobj (true, gen_B_bexp g', snd t', snd f', ans_ty, (lp, abs_cp))))
                    | other -> sf (sprintf "query retrieve anno other %A" other)
#endif
                (g1, sigma11, (ty, gen_B_query ww bobj (true, gen_B_bexp g', snd t', snd f', ty, (lp, abs_cp))))

            | B_reveng_h(ty, lst) -> // Reveng always non-strict.
                let rh_walk (b,x) (gg, ss, sofar) =
                    let (gg1, ss1, b') = ebwalk ww (gg, ss) firex b
                    // reduce tyo with ty here ? let tyo = if nonep tyo then Some ty else 
                    let (gg_x, ss2, (xt, x')) = ewalk ww (g_empty_resource_record, ss1) firex (Some ty) x
                    //vprintln 0 (sprintf "walked revenger clause pre  g=%s   v=%s " (bsv_bexpToStr b) (bsv_expToStr x))
                    //vprintln 0 (sprintf "walked revenger clause post g=%s   v=%s " (xbToStr b') (bsv_expToStr x'))
                    let split = true // non-strict.
                    (ig_mux_dist split gg1 b' gg_x g_empty_resource_record, ss2, (b', x')::sofar)
                let (gg, sigma11, rlst) = List.foldBack rh_walk lst (gg, sigma10, [])
                (gg, sigma11, (ty, B_reveng_l(ty, rlst)))

            | B_reveng_l(ty, lst) -> // Strict or non-strict?  Reveng always non-strict.
                let rwalk (b,x) (gg, ss, sofar) =
                    // reduce tyo with ty here ? let tyo = if nonep tyo then Some ty else                     
                    let (gg_x, ss2, (xt, x')) = ewalk ww (g_empty_resource_record, ss) firex (Some ty) x
                    //vprintln 0 (sprintf "walked revenger clause post g=%s   v=%s " (xbToStr b) (bsv_expToStr x'))
                    let split = true//non-strict
                    (ig_mux_dist split gg b gg_x g_empty_resource_record, ss2, (b, x')::sofar)
                let (gg, sigma11, rlst) = List.foldBack rwalk lst (gg, sigma10, [])
                (gg, sigma11, (ty, B_reveng_l(ty, rlst)))

            | B_pliStmt(ss, args, lp) ->
                let (g1, sigma11, args) = efoldwalk ww (gg, sigma10) firex (map (fun (ty, x)-> (Some ty, x)) args)
                let pli_format_string_munge arg = // This needs to be done post rule elaboration so that the superscalar cardinal number is inserted on the per-elaboration basis. 
                    match arg with
                        | (ty, B_string ss) -> (ty, B_string(ss.Replace("%r", firex.rulename_msg))) // TODO: protect %% 
                        | _ ->
                            //vprintln 3 (sprintf "pli_format_string_munge other %A" other)
                            arg

                let args = if length args > 0 then pli_format_string_munge(hd args) :: tl args else args
                //dev_println (lpToStr0 lp + ": ewalk: walked pli " + ss)
                (g1, sigma11, (Bsv_ty_av_partial(*there may be a ret type*), B_pliStmt(ss, args, lp)))

            | B_subif(ss, b, ty, lp) -> // This is a component in the hierarchic path 
                let mf() = "subif " + ss
                let (g11, sigma11, b') = ewalk ww (gg, sigma10) firex None (b)
                match b' with
                    | (ty, B_ifc(PI_ifc_flat b1, iidl)) ->
                        let iface = find_ifc mf (ss::b1) sigma11
                        let ifc2 = 
                            match iface with // check for a redirection at this point...
                                | other -> muddy (sprintf "RETCAM fixme me %A" other)
#if WIP
                                | TRACE_redirect(new_s, B_ifc(PI_ifc_flat np, _)) ->
                                    //dev_println (sprintf "MIRROR: observe TRACE_redirect from %s to %s" (hptos (ss::b1)) (hptos (new_s::np)))
                                    let iface = find_ifc mf (new_s::np) sigma11 
                                    B_ifc(PI_ifc_flat(new_s::np), iidl)

                                | _ -> B_ifc(PI_ifc_flat(ss::b1), iidl)
#endif
                        (g11, sigma11, (ty, ifc2)) // TODO is this ty correct?
                    | (ty, other) -> sf(sprintf "B_subif other form %A" other)
                    
            | B_ifc_immed(items) -> (gg, sigma10, (Bsv_ty_dontcare "L3730", B_ifc_immed(items)))

            | B_vector (_, (ded, iidl))  // B_vector needs its ifc name expanding ... like ded : this is a pre/post flatten...
            | B_ifc(PI_ifc_hier ded, iidl) -> // Flatten a hierarchial interface element.
                let _ = sf ("still used for B_vector?")
                let ifc2 = 
                    match env.TryFind ded with // This is just an eval - please ensure that that code is always used and never this copy TODO.
                        | None ->
                            let r = ref []
                            for z in env do mutadd r z.Key done 
                            vprintln 0 (htosp ded + " was not found in the env. All=" + sfold_unlimited htosp !r) // TODO this is ok after a redirect -> goes through ok ?   ifc_hier could just be a B_var
                            vprintln 0 (sprintf "+++no bind in env for ifc '%s': should not happen : except for vector subscript." (htosp ded))
                            (iidl, B_ifc(PI_ifc_flat ded, iidl))

                        | Some(GE_binding(_, _, EE_e(ty, B_ifc(PI_ifc_flat ifc_idl, iidl)))) ->
                            //vprintln  0 ("ifc " + htosp ded + " was found in the env as " + htosp ifc_idl)
                            (ty, B_ifc(PI_ifc_flat ifc_idl, iidl))
                        | other -> sf (sprintf "unrecognised form: apply function to something that is not an interface?: %A" other)
                (gg, sigma10, ifc2)



            | B_applylam(acp, info, args, lp, busyflg, ans) when not(nonep !busyflg) -> cleanexit(!m_m + ": Infinite method/function loop post elaborate " + valOf !busyflg)
            // There is no recursion post elaborate in Bluespec.
            // We expect applylam to be recoded to another form on outermost encounter and so this is a going round again.

            | B_applylam(_, info, args, lp, busyflg, ans) when not(nonep !ans)  ->
                vprintln 3 (!m_m + "using cached answer : TODO correct ? correct if the cache is just a denorming" + bsv_expToStr (snd (valOf !ans)))
                // TODO we'd like to eval the expanded B_applylam here instead of at its various use sites ... possibly need more info from typechecker to do it now?
                // The expaning clause below DOES MAKE SUCH A TAILCALL SO IT IS OK!? - well it would come here and be ignored so makes no difference.
                (gg, sigma10, valOf !ans)
                
            | B_applylam(ncp, info, args, lp, busyflg, ans) (*when nonep !ans *) ->
                let mf() = lpToStr0 lp + " ewalk B_applylam"
                let ap_ToStr (ty, vale) = (bsv_expToStr vale)
                let lam_arg_bind (ty, x) =
                    //vprintln 0 (mf() + " arg prior to eval is + " + bsv_expToStr x) 
                    (Some ty, x)
                let (g11, sigma10, args') = efoldwalk ww (gg, sigma10) firex (map lam_arg_bind args)
                //vprintln 0 (mf() + " args post eval are " + sfold ap_ToStr args')
                // we need to deep bind args into formals here to augment closure_ee which run_func_application should do for us
                let dc = ee.dynamic_chain
                let ans1 =
                    match info with
                        | PI_bno(lam, nameo) ->
                            match lam with // This match is just extracting nameo as a hint - the rest of this reveng_sel code is rubbish? Should define a specific B_lambda_reveng is really want to use this approach?
                            | B_lambda(nameo, formals1_, body_, fun_ty_, closure_ee_, (lp_, abs_cp___)) ->
                                let fid = valOf_or nameo "$ANONYMOUS"
                                let ww = WF 3 "B_applylam-lambda" ww fid
                                let m1 = "B_applylam-lambda: " + " fid=" + fid
                                busyflg := Some m1 // Set busyflg once we know the hint name.
                                let ans1 = run_func_application ww bobj lp (Some true) (evty_create_indirection_flt "L5438") (dc, ncp) mf nameo args' lam // this is expecting compile-time eval through forced norming - no sigma currency changes.
                                let ans1 = (f1o3 ans1, f3o3 ans1)
                                ans := Some ans1
                                ans1
                busyflg := None // clear flag when done
                // Now we would walk the function body - but
                // run_func_application has rewritten it as a B_applylam with actuals substituted rather than using env bindings for the formals.
                //vprintln 0 (mf() + " post norm lambda wholething not body is " + bsv_expToStr(snd ans1))
                ewalk ww (g11, sigma10) firex tyo (snd ans1)                
                
            | B_applyf(info, rt, args, tag) -> // ewalk
                let mf() = taggedlpToStr0(tag) + " B_applyf function/method application: " + pathToS info
                let ww = if vd >=4 then WF 4 "ewalk" ww (mf()) else ww
                let getrt info =
                    match tyo with
                        | Some t1 -> rt // Should do a type meet with t1?
                        | None ->  rt // cleanexit(mf() + sprintf "no expected return type in context tyo=%A %A" tyo info)
                match info with
                    | PI_biltin("warning" as mc)  | PI_biltin("error" as mc) | PI_biltin("message" as mc)
                    | PI_biltin("warningM" as mc)  | PI_biltin("errorM" as mc) | PI_biltin("messageM" as mc)  -> 
                    // These can come in as declDos to an implicit Empty that was made explicit by the parser processing.
                        let rv = exec_reporters ww mf (lpToStr0 (fst tag)) mc ee.dynamic_chain args
                        (gg, sigma10, rv)
                                
                    | PI_biltin("await") ->
                        let (g1, sigma10, args') = efoldwalk ww (gg, sigma10) firex (map (fun (_, x)->(Some g_bool_ty, x)) args)
                        ({gg with igs=gg.igs.Add((false, [funique "await"]), IG_alpha(bsv_blower ww bobj (B_orred(snd(hd args')))))}, sigma10, (g_bool_ty, B_hexp(g_bool_ty, xi_num 1))) // nominal 1
                    | PI_biltin "!" -> 
                        let (g1, sigma10, args') = efoldwalk ww (gg, sigma10) firex (map (fun (ty, x)->(Some ty, x)) args)
                        cassert(length args' = 1, "! needs one argument exactly")
                        (g1, sigma10, (g_bool_ty, B_applyf(info, g_bool_ty, args', tag_this_lp (fst tag))))

//                    | PI_biltin "$format" ->

                    | PI_biltin "fromInteger"  // This should not be built in and should do something ?
                    | PI_biltin "unpack" ->  // These are nops once in bits form, but we do need to change the return type.
                        let (g1, sigma10, args') = efoldwalk ww (gg, sigma10) firex (map (fun (ty, x)->(Some ty, x)) args)
                        cassert(length args' = 1, "this builtin needs one argument exactly")
                        let rt = getrt info
                        (g1, sigma10, (rt, snd(hd args')))


                    | PI_biltin "pack" ->   // These are nops once in bits form, but we do need to change the return type.
                        let (g1, sigma11, args') = efoldwalk ww (gg, sigma10) firex (map (fun (ty, x)->(Some ty, x)) args)
                        cassert(length args' = 1, "this biltin needs one argument exactly")
                        let rt = getrt info
                        (g1, sigma10, (rt, snd(hd args')))


                    | PI_biltin("extend" as fn)      // latefunctions - implemented in lower.
                    | PI_biltin("truncate" as fn)    
                    | PI_biltin("zeroExtend" as fn) 
                    | PI_biltin("signExtend" as fn)  -> // Certain functions are implemented in lower - pass them on as applications here - TODO implement a specific latefunction codification.
                        let (g11, sigma10, args') = efoldwalk ww (gg, sigma10) firex (map (fun (ty, x)->(Some ty, x)) args)
                        (g11, sigma10, (Bsv_ty_integer, B_applyf(info, Bsv_ty_integer, args', (tag_this_lp(fst tag)))))

                    | PI_bno(B_dyn_b("noAction", _), _) -> (gg, sigma10, (g_unit_ty, B_hexp(Bsv_ty_integer, xi_num 0)))

                    | PI_bno(B_dyn_b(fname, ifc), _) ->
                        // We need to ensure another eval of the guard if we comment out the next line...  where is this?  This is an expression and gg is merely a continuation passed in by the caller, not something that needs eval here.
                        let (g11, sigma10, ifc') = (gg, sigma10, ("spare", ifc))//ewalk ww (gg, sigma10) firex None (ifc)
                        let (g11, sigma10, actuals) = efoldwalk ww (g11, sigma10) firex (map (fun (ty, x)->(Some ty, x)) args) // Eval actual args.
                        //dev_println (sprintf "ewalk: %s dyn_b_ post efoldwalk_find=%s" fname (ig_alpha_find_in_map g11.igs))
                        let rec calla (g11, sigma10) firex count arg =
                            match arg with
                            // Does Bluespec have dynamic (higher-order-like) dispatch and is it at compile time or run time?  Invoking a method on a dynamically-selected method?
                            // Can an interface be re-assigned so the same interface handle refers to different hardware interfaces?
                            | B_var(dbi_, vdl, lp) -> // Because we did not include iactuals in ee for the deabstract of the module we must insert them here.
                                // I am not sure why we have special code for a variable here? The go-round-again catch-all should have the same effect?
                                match env.TryFind vdl with
                                    | Some(GE_binding(_, _, EE_e(_, ifc))) -> calla (g11, sigma10) firex count ifc
                                    | None -> cleanexit(mf()+ sprintf " call of '%s' : object reference is unbound variable '%s'" fname (htosp vdl))
                                    | q -> muddy (sprintf "malformed shi var '%s' %A" (htosp vdl) q)
                            | B_ifc(PI_ifc_flat that1, _) ->
                                let (key1, concrete_item_o, eph_item_o, error_o) = find_meth mf sigma10 (that1) fname
                                if not_nonep error_o then
                                    dev_println (sprintf "error_o: Site L6476")
                                    cleanexit(valOf error_o)
                                let resname = key1

                                if not_nonep eph_item_o then
                                    match eph_item_o with
                                        | Some(Sigma_e(name_, rt, proto, formals, callee_explicit_condition, actions, lp)) -> // Commission of an ephemeral method (inline the body).
                                            let retval =
                                                match proto with // TODO - copied-out code needs abstracting
                                                | BsvProtoActionValue(tya)
                                                | BsvProtoValue(tya) -> RV_highlevel(rt)
                                                | NoProto ->
                                                    hpr_yikes(mf() + ": Proto needed for method used in hardware form.")
                                                    RV_noret
                                                | BsvProtoAction     -> RV_noret
                                            //(sprintf "retval %s rt %A" (resname) rt retval)
                                            let callguard = xi_blift firex.fire
                                            let ephbind (env:genenv_map_t) ((actual_ty, actual_vale), (so_, ty_, fid)) =
                                                vprintln 3 ("Binding ephemeral formal fid=" + fid + " in " + mf())
                                                env.Add([fid] (* :: prefix ? *), GE_binding([fid], { g_blank_bindinfo with whom="ephemeral-method-elabf"}, EE_e(actual_ty, actual_vale)))
                                            let env1 = List.fold ephbind env (List.zip actuals formals) // The eval'd args are called actuals.

                                            // For debug purposes we augment walkstack path  with resname on compile-time stack but need to pass down monica unchanged to get resources tallied for commissioning rule/thread.
                                            let (g11, sigma10, rv) = commission ww "ephemeral method" env1 monica (resname::walkstack) callguard (Some callee_explicit_condition) (emits_o, g11, sigma10) (gen_B_action "Commission L6796" ee.fbody_context (fname, actions, retval, lp)) // Ephemeral method body expansion.
                                            //println (sprintf " ephemeral stop callguard=%s guard_final=%A rv=%s" (xToStr callguard) (igMapToStr g11.igs) (bsv_expToStr (snd rv)))
                                            //dev_println (sprintf " completed ephemeral stop fname=%s resname=%s" fname (hptos(kToStr resname)))
                                            (g11, sigma10, rv)
                                else
                                let (sigcur) = valOf_or_fail "L6593" concrete_item_o
                                let bind4 (Sigma(id3, resats, (proto, rvo, m_ats), arginputs)) =

                                    let chosyn = function
                                        | (fpath, count) when m_ats.synchronised -> (fpath, count+1) // increment fpath count field when synchronised.
                                        | (fpath, count)                         -> (fpath, count)
                                    let (igx, g11) =
                                        let (igx, fpx) =
                                            match proto with
                                                | BsvValue None  // We do not really need to record a value access that is always-ready unless tracking synchronised fpath region map.
                                                | BsvAction(None, _, _)     -> (IG_resource(resname, htosp(kToStr resname), X_true, resats), firex.fpath)
                                                | BsvValue (Some rdy)
                                                | BsvAction(Some rdy, _, _) -> (IG_resource(resname, htosp(kToStr resname), xi_orred rdy, resats), chosyn firex.fpath)
                                        if resats.actionf then log_conflict_record emits_o mf (firex.fire, monica, kToStr key1, fname, actuals, resats) 
                                        let ig' =
                                            { g11 with
                                                 fpaths= g11.fpaths.Add(key1, singly_add fpx (valOf_or_nil(g11.fpaths.TryFind key1)))
                                                 igs=    g11.igs.Add((false, kToStr key1), ig_combine igx (g11.igs.TryFind (false, kToStr key1))) // Currently some redundancy: we have confict_log and similar extra info in ig log.
                                            }
                                        //dev_println (sprintf "bind4 pre find=%s" (ig_alpha_find_in_map g11.igs))
                                        //dev_println (sprintf "bind4 post find=%s" (ig_alpha_find_in_map ig'.igs))                                            
                                        (igx, ig')
                                    //dev_println ("MIRROR ewalk + " + mf() + sprintf " ig'=%A" ig')

                                    let (sigma10, g11, denotational_read_ans) =
                                        match resats.specialmodel with
                                            | SM_bluewire ->
                                                let ty = match rvo with
                                                                | RV_netlevel(ty, retval) -> ty
                                                                | _                       -> g_ty_sum_void
                                                let (sigma10, g11, rv) = bluewire_operation ww bobj pass mf ee sigma10 g11 id3 ty actuals firex.fire
                                                (sigma10, g11, rv)
                                            | _ -> // TODO fold other types inside this disjoint switch
                                                let denotational_read_ans = // TODO MIRROR need to check all guard support etc for second rule sees effects of first ...
                                                    if fname = g_sread then // TODO use predicate on resats.specialmodel=SM_ephemeral instead
                                                        let ans =
                                                            match sigma10.statemap.TryFind that1 with
                                                                | None ->
                                                                    if vd>=5 then vprintln 5 (sprintf "Forwarding/denotational: no answer for %s" (hptos that1))
                                                                    None
                                                                | Some(MS_denot(monica_, whom, write_guard, (ty_, vale), subscript_o, cmd_name)) ->
                                                                    //dev_println (sprintf "Denotational answer %s is %s" (hptos that1) (bsv_expToStr vale))
                                                                    Some(write_guard, vale)
                                                        ans
                                                    else None
                                                (sigma10, g11, denotational_read_ans)

                                    let state_mapping_method_o =
                                        let lower (ty_, arg) = bsv_lower ww bobj arg
                                        if bobj.multiple_scalar_write_per_cycle && fname = g_swrite && length actuals = 1 then // TODO use predicate on resats.specialmodel
                                            Some (that1, None, hd actuals)
                                        elif bobj.multiple_array_write_per_cycle && fname = s_native_write && length actuals = 2 then

                                            Some(that1, Some(lower (hd actuals)), cadr actuals) // rhs = cadr args
                                        else None

                                    let argvale_o =
                                        if fname <> g_swrite then None
                                        else
                                            if length actuals <> 1 then sf (sprintf "need precisely one arg stagemapped method %s" g_swrite) 
                                            Some(hd actuals)

                                    let sigma10 = // Either update pending map (case SM_denot) or else make directly in hard currency (case SM_notspecial).
                                        match state_mapping_method_o with
                                            | None ->
                                                let sendarg((ht, here), (fn, net, old)) =
                                                    if vd>=4 then vprintln 4 ("Sendarg " + msg + ":" + fname + " " + xToStr net + " := " + bsv_expToStr here)
                                                    let here' = bsv_lower ww bobj here
                                                    if vd>=4 then vprintln 4 ("Sendarg " + msg + ": lowered")
                                                    //let nv = (if old.enco=X_undef then here' else ix_query firex.fire here' old.enco)
                                                    (fn, net, { old with encol= (monica, firex.fire, here')::old.encol })
                                                if length actuals <> length arginputs then cleanexit(mf() + sprintf ": Wrong number of input args for method wiring '%s'  %i <> %i (internal comiler error)" (hptos(f2o3 id3)) (length actuals) (length arginputs))
                                                let new_sigcur =
                                                    let proto' = alpha_fire monica firex.fire proto
                                                    Sigma(id3, resats, (proto', rvo, m_ats), map sendarg (List.zip actuals arginputs))

                                                { sigma10 with methmap=sigma10.methmap.Add(key1, new_sigcur) }
                                            | Some(dkey, nv_subsc_o, (rhs_ty, written_value)) ->
                                                let (nv_monica, nv_fire, nv_vale) =
                                                    match sigma10.pendingmap.TryFind dkey with
                                                        | Some(MS_denot(old_monica, whom, old_grd, old_vale, subscript_o, cmd_name)) -> 
                                                            //vprintln 4 (sprintf "Multiple pending updates exist for %s" (hptos dkey))
                                                            name_alias_check (fun () -> "multiple pending updates") dkey (nv_subsc_o, subscript_o)
                                                            let grd = ix_or firex.fire old_grd
                                                            let ty = fst old_vale
                                                            let dummy_meta = (g_builtin_lp, CP_none "denotational-mux1")
                                                            let vale = gen_B_query ww bobj (false, B_bexp firex.fire, written_value, snd old_vale, ty, dummy_meta)
                                                            let monica_resolved = if monica = old_monica then old_monica else (fst monica, funique "monica-resolved"::snd monica)
                                                            (monica_resolved, grd, (ty, vale))
                                                        | None ->
                                                            //vprintln 4 (sprintf "Create pending update for %s" (hptos dkey))
                                                            (monica, firex.fire, (rhs_ty, written_value))
                                                let pendingmap = sigma10.pendingmap.Add(dkey, MS_denot(nv_monica, "basic-assign", nv_fire, nv_vale, nv_subsc_o, fname))
                                                { sigma10 with pendingmap=pendingmap }

                                    let xr = match rvo with
                                               | RV_netlevel(ty, retval) -> (ty, B_hexp(ty, retval))
                                               | _ -> (Bsv_ty_dontcare "L3916", B_hexp(Bsv_ty_dontcare "L3916", X_undef))

                                    let sigma10 =
                                        match sigma10.fwdpaths.TryFind(that1) with
                                            | Some fwdpath_rec when fname = g_swrite ->
                                                if nonep argvale_o then sf("no written value for " + fname)
                                                let (ty_, written_value) = valOf argvale_o // Generalise please -
                                                dev_println "need superscalar array read please"
                                                let fwdpath_rec = 
                                                    if fwdpath_rec.register = X_undef then // Fill in the underlier register net-level form if missing.
                                                        let (_, concrete_item_o, eph_item_o, error_o) = find_meth mf sigma10 (that1) g_sread
                                                        match concrete_item_o with
                                                            | Some(Sigma(name_, resats, (proto', RV_netlevel(rt_ty, underlier), m_ats), nl_args)) ->
                                                                { fwdpath_rec with register=underlier }
                                                            | other ->
                                                                hpr_yikes(sprintf "L6905 concrete register net item expected not %A." other)
                                                                fwdpath_rec
                                                    else fwdpath_rec
                                                    //let underlier =   B_applyf(PI_bno(B_dyn_b(g_sread, fwdpath_rec.register), None), Bsv_ty_dontcare "L3431", [], tag_this_lp g_builtin_lp)
                                                        
                                                let fwdpath_rec = fwdpath_add_src ww fwdpath_rec ee.attribute_settings.dirinfo tag (*snd xr*) firex written_value
                                                { sigma10 with fwdpaths=sigma10.fwdpaths.Add(that1, fwdpath_rec) }
                                            | _ -> sigma10
                                    let xr =
                                        match denotational_read_ans with
                                            | None -> xr
                                            | Some(gg, ll) ->
                                                let gg:hbexp_t = gg
                                                //dev_println(sprintf "have denot_guard for %s" (hptos key1))
                                                //dev_println(sprintf "denot_guard for %s is %s  oldgrd=%A" (hptos key1) (xbToStr gg) oldgrd)
                                                let rr = snd xr
                                                // fst xr is dubious as ig ?
                                                (fst xr, gen_B_query ww bobj ((*strictf*)false, B_bexp gg, ll, rr, rt, (g_builtin_lp, CP_none "denotational-mux2")))
                                    (g11, sigma10, xr)

                                bind4 sigcur

#if NONEED
                                // All of these just go round again if an unrecognised interface form?
                            | B_subif(ss, b, ty, lp) -> // This is a hierarchic path 
                                let (g12, sigma12, (t, reveng_l)) = ewalk (g11, sigma10) firex None (B_subif(ss, b, ty, lp))
                                calla (g12, sigma12) firex count reveng_l

                                // The reveng_l used to be encounted but now we get the _h form, which is a bit nicer at least. TODO deleted _l clause if no longer live.
                            | B_reveng_h(ty, lst) -> // unmake a reveng - TODO dont really want... also ig is getting strict here
                                let (g12, sigma12, (t, reveng_l)) = ewalk (g11, sigma10) firex None (B_reveng_h(ty, lst))
                                calla (g12, sigma12) firex count reveng_l
#endif
                            | B_reveng_l(ty, lst) -> // unmake a reveng - TODO dont really want... also ig is getting strict here ... // is this a clause handlable on first count
                                let return_ty = ref None
                                let rec sq (g_sq, sigma_sq) firex = function
                                    | (grd, ifc)::tt ->
                                        let (g_sq, sigma_sq, (rty_, rx)) = calla (g_sq, sigma_sq) {firex with fire=ix_and grd firex.fire} count ifc
                                        return_ty := Some rty_ // TODO meet on all ????
                                        if nullp tt then (g_sq, sigma_sq, rx)
                                        else
                                            let (gf, sf, rxf) = sq (g_sq, sigma_sq) firex tt
                                            let rx' = gen_B_query ww bobj (false, gen_B_bexp grd, rx, rxf, ty, (g_builtin_lp, CP_none "L4589")) // COULD RETURN THIS MUXTREE AS A REVENGE - 
                                            (gf, sf, rx')
                                let (g11, sigma10,ans) = sq (g11, sigma10) firex lst
                                let ty' = valOf_or_fail "reveng empty?" !return_ty
                                (g11, sigma10, (ty', ans))

                            | other_finfo when count < 2 -> // go-round-again clause
                                //vprintln 0 (sprintf "other finfo - go-round-again %A" (bsv_expToStr other_finfo))
                                let (g11, sigma10, (nt, nv)) = ewalk ww (g11, sigma10) firex None other_finfo
                                calla (g11, sigma10) firex (count+1) nv

                            | other -> sf(mf() + sprintf ": unrecognised interface form for method '%s' '%A" fname (bsv_expToStr other))
                        calla (g11, sigma10) firex 0 (snd ifc')

                      | other -> sf(mf() + sprintf ": unrecognised form (2) %A" other)


             | B_subs(dotflag, baser, subs, lp) -> // Subscription operator
                 let mf() = "B_subs - L7089"
                 let (g1, sigma10, b') = ewalk ww (gg, sigma10) firex None baser
                 let (g2, sigma12, subs') = ewalk ww (g1, sigma10) firex (Some Bsv_ty_integer) subs
                 
                 let rec subser grds (g5, s5, cc) = function
                     | (B_vector(vs, (ifc_name, ty))) ->
                         // TODO move 'vectors' into a norm1 per module scope.
                         muddy "vectors should now be env entries!"
#if OLDCODE
                         let bv = valOf(ee.vectors.lookup vs) // No this is for norm only: here we expect to find its value in the env.
                         //vprintln 0 (sprintf "%s: Doing a reveng %s on subs=%s"  (lpToStr0 lp) (htosp bv.iname) (bsv_expToStr subs))
                         //Form a reveng by scanning all locations. ---- NOW IS THis ALWAYS NEEDED? TODO
                         let rangel = map (fun x -> (B_hexp(Bsv_ty_integer, xi_num x), bv.data.TryFind x)) [0..bv.len-1]
                         let rec reveng c = function
                             | (q, Some v) ->
                                 vprintln 0 ("Got from vector  " + bsv_expToStr v)
                                 (gen_B_and(gen_B_deqd ww bobj (q, subs')::grds), v)::c // TODO MANIFEST CONSTANTS HERE PLS
                             | (q, None) -> c // Silently ignore unset entry
                        (g5, s5, List.fold reveng cc rangel)
#endif
                     | (B_reveng_l(ty_, lst)) ->
                         let qrr (g8, s8, cc) (g, v) =
                             let (g9, s9, n0) = subser [] (g8, s8, []) v
                             let n = map (fun (gg, vv) -> (ix_andl (gg::grds), vv)) n0
                             (g9, s9, n @ cc)
                         List.fold qrr (g5, s5, cc) lst

                     | (B_ifc(PI_ifc_flat ifc, ity)) ->
                        if dotflag then sf "BRAM forwarding should have been handled in norm."
                            
                        else
                            hpr_yikes("Ignored subscription operator! subs=" + bsv_expToStr(snd subs')) // Does this happen? 
                            let rr = (ix_andl grds,  B_ifc(PI_ifc_flat  ifc, ity)) //  TODO!!! extend by catenating i2s of subs ... ?
                            hpr_yikes("Encountered 00000000000 subscriptable " + htosp ifc)
                            (g5, s5, rr::cc)
                         
                     | B_query(_, g, tt, ff, _, meta_) ->  // We do need to cope with query trees here because the user's source code might have them, but we prefer to use the reveng form.
                         let (g6, s6, grd) = ebwalk ww (g5, s5) firex g
                         let (g7, s7, cc) = subser (grd :: grds) (g6, s6, cc) tt
                         subser (xi_not grd :: grds) (g7, s7, cc) ff

                     | other ->
                         sf (lpToStr0 lp + ": subscript other form " + bsv_expToStr other + " in " + !m_m)

                 let ct = tyderef mf (fst b')
                 let (g9, s9, items)  = subser [] (g2, sigma12, []) (snd b')
                 (g9, s9, (ct, B_reveng_l(ct, items)))

            | B_bit_replace(ty, ov, nv, width, baser, lp) ->
                let (gg', sigma10, [(t0, v0);(t1, v1)]) = efoldwalk ww (gg, sigma10) firex [(tyo, ov); (tyo, nv)]  
                let nv = B_bit_replace(ty, v0, v1, width, baser, lp)
                (gg', sigma10, (ty, nv))
                
            | B_diadic(oo, a, b, ans_ty) ->
                let mf() = "B_diadic oo"
                let (gg', sigma10, lst') = efoldwalk ww (gg, sigma10) firex [(tyo, a); (tyo, b)]
                let (tlst, vlst) = List.unzip lst'
                let ans = gen_B_diadic bobj ww mf oo ans_ty (hd lst', cadr lst') // TODO use this if force callsite deleted
#if DDDDD
                match retrieve_type_annotation bobj mf (abs_cp) with // bit heavy - can't gen_B_diadic return its type?
                    | Tanno_function(ans_ty, _, _, [("LD",lt); ("RD",rt)]) ->
                        //vprintln 0 (sprintf "diadic walk %A  l=%s r=%s  ans=%s" oo (bsv_expToStr a) (bsv_expToStr b) (bsv_expToStr ans))
                        //in (gg', sigma10, (ty, bsv_force ww env obj ans)) // TODO check consistent use of force (haha)
                        (gg', sigma10, (ans_ty, ans))
                        
                    | other -> sf (sprintf "diadic retrieve anno other for %s %A" (cpToStr abs_cp) other)
#endif
                (gg', sigma10, (ans_ty, ans))


            | B_blift bexp -> let (gg, sigma10, e') = ebwalk ww (gg, sigma10) firex bexp in (gg, sigma10, (g_bool_ty, B_hexp(g_bool_ty, xi_blift e')))

            | B_valof(lst, ret_tyo, lp) -> // Convert a valof to a pseudo action block by making up a rule name and ... TODO unify ?
                let newtag = lpToStr0 lp + "_valof"
                let fireguard = xi_blift firex.fire
                //let monica = rulename::nprefix
                let retval = if nonep ret_tyo then RV_noret else RV_highlevel(valOf ret_tyo)
                //vprintln 0 (rulename + " B_valof using commission instead of an implied action block ?")
                let (gg, sigma10, rv) = commission ww "valOf" env monica ((IIS "", newtag)::walkstack) fireguard None (emits_o, gg, sigma10) (gen_B_action "B_valof L6315" ee.fbody_context (newtag, lst, retval, lp)) 
                (gg, sigma10, rv)

            | B_action(ruleissue, lst, retvalo, lp) ->
                let mm = lpToStr0 lp + "_rule:" + ruleissueToStr ruleissue
                let fireguard = xi_blift firex.fire
                let newtag = ruleissueToStr0 false ruleissue :: nprefix
                //let ee = { ee with rulename_issue=Some ruleissue }
                //vprintln 0 (mf() + ": B_action using commission instead of an implied action block ?")                
                let (gg, sigma10, rv) = commission ww "action" env monica ((IIS "", hptos newtag)::walkstack) fireguard None (emits_o, gg, sigma10) (gen_B_action "B_action L6324" ee.fbody_context (f1o3 ruleissue, lst, retvalo, lp)) // TODO yuck: improve on this f1o3 ?
                (gg, sigma10, rv)
                
            // constants | other -> if constantp other then (gg, sigma10, other) else
                
            | B_maker_l(mkname, _, _, _, _, _, _) -> cleanexit(!m_m + sprintf ": The module generator '%s' cannot be used unapplied in generated hardware" (htosp mkname))
            | B_subif(ss, b, ty, lp) -> cleanexit(!m_m + sprintf ": The subinterface '%s' cannot be used as an expression out of context" ss)
            | B_ifc(ifc_name, ty) ->
                //cleanexit (!m_m + sprintf ": The interface '%s' cannot be used as an expression out of context" (pathToS ifc_name))
                (gg, sigma10, (ty, aa))
                
            | other -> sf (sprintf "ewalk: other expression form: %s\n\nA=%A" (bsv_expToStr other) other)

        and tl_bwalk (ig, sigma20) aa =
            //vprintln 0 ("Start ebwalk " + htosp utag)
            let ans = ebwalk ww (ig, sigma20) (gen_firex []) aa
            //vprintln 0 ("  end ebwalk " + htosp utag)
            ans
        
        and ebwalk ww (gg, sigma10) firex aa =
            //let ww = WF 3 "ebwalk" ww (!m_m + bsv_bexpToStr aa) // skip this for speed
            //vprintln 0 (!m_m + sprintf ": ebwalk gg=%A" gg + " arg=" + bsv_bexpToStr aa)
            match aa with
            | B_not(e) -> let (gg', sigma10, e') = ebwalk ww (gg, sigma10) firex e in (gg', sigma10, xi_not e')

            | B_bdiop(nodeop, lst) ->
                let (gg', sigma10, lst') = efoldwalk ww (gg, sigma10) firex (map (fun x-> (None, x)) lst)
// we argue that this must now be in class bits and hence lowerable.
                let rebuilt = gen_B_bdiop ww bobj nodeop lst'
                let ans = bsv_blower ww bobj rebuilt
                (gg', sigma10, ans)

            | B_bdiop(V_orred, [x])
            | B_orred x   ->
                let (gg', sigma10, x') = ewalk ww (gg, sigma10) firex None x
                (gg', sigma10, bsv_blower ww bobj (B_orred(snd x')))

            | B_and(lst)  ->
                let (gg', sigma10, lst') = efoldbwalk ww (gg, sigma10) firex lst
                (gg', sigma10, List.fold ix_and X_true lst')
            | B_or(lst) ->
                let (gg', sigma10, lst') = efoldbwalk ww (gg, sigma10) firex lst
                (gg', sigma10, List.fold (fun c x -> xi_or(c,x)) X_false lst')
            | B_bexp x  -> (gg, sigma10, x)
            | B_firing rulename ->
                let fireguard = gen_fireguard emits_o (htosp (rulename :: nprefix))
                //if not found then sf ("fireguard for rule " + x + " not yet made")
                (gg, sigma10, xi_orred fireguard)
            //| other ->  sf ("ebwalk: other form: " + bsv_bexpToStr other)
            

        and efoldwalk ww (gg, sigma10) firex lst =
            let rec fw1 (gg, sigma10, vs) = function
                | [] -> (gg, sigma10, rev vs)
                | (xc, (tyo, x))::xs ->
                    let (gg, sigma10, v) = ewalk ww (gg, sigma10) firex tyo x
                    fw1 (gg, sigma10, v::vs) xs
            fw1 (gg, sigma10, []) (List.zip [1..length lst] lst)

        and efoldbwalk ww (gg, sigma10) firex lst =
            let rec fw1 (g, s, vs) = function
                | [] -> (g, s, rev vs)
                | (xc, x)::xs ->
                    let (g, s, v) = ebwalk ww (g, s) firex x
                    fw1 (g, s, v::vs) xs
            fw1 (gg, sigma10, []) (List.zip [1..length lst] lst)

        (tl_walk, tl_bwalk)



    and commission_iactuals (env:genenv_map_t) (mf:unit->string) =
        //This is usded for instance actuals. These should all be constant expressions (mainly interfaces) ans so have not intrinsic guards or require forwarding etc..
        //This is not used for method call actuals since they generally require code generation and can block.
        let token = funique ("instance_actual_eval")
        let fireguard = xi_num 1//vectornet_w(token + "_UNUSED_FIREGUARD", 1)
        let mf() = mf() + ":" + token
        let monica = (false, [token])
        let emits = gec_blank_emission "iactuals" // We could just pass none here?
        let (tl_walk, bwalk) = walkgen monica [(IIS "", token)]  env (emits_o, fireguard, token) 
        if not_nullp !emits.rulectl then cleanexit(mf() + " args with intrinsic guards passed to structural instance")
        if not_nullp !emits.nets then cleanexit(mf() + " args with intrinsic guards passed to structural instance")
        if not_nullp !emits.comb_logic then cleanexit(mf() + " args with intrinsic guards passed to structural instance")
        if not_nullp !emits.children then cleanexit(mf() + " args with intrinsic guards passed to structural instance")

        let eval_pram x =
            let (ig1, s1, v) = tl_walk [] (g_empty_resource_record, { ifcmap=Map.empty; methmap=Map.empty; pendingmap=Map.empty; statemap=Map.empty; fwdpaths=Map.empty; bluewires=Map.empty; exordering=[] }) x
            // TODO check answer blank
            v
        eval_pram


    // Commission creates a code generator that can compile Bsv_ behavioural and action statements.
    // Commission forms walk and bwalk closures that do the same for  B_ expressions appearing in the statements.
    // If a code generator is returned, this must be a lambda and be applied somewhere else? It does not work that way now.
    and commission ww site (env:genenv_map_t) monica walkstack fireguard explicit_condition_o (emits, (ig00:resource_records_t), sig00) arg =
        let _:monica_pair_t = monica
        let (rulename_string, comph, retval_o, lp) =
            match arg with
                | B_action(ruleissue, comph, retvalo, lp) ->
                    (ruleissueToStr ruleissue, comph, retvalo, lp)
                | B_applyf(_, _, iactuals, (lp, _)) -> // If not an action, wrap as one.
                    //dev_println (sprintf "Applyf form commission: wrap as RV_noret")
                    let rulename = htosp (snd monica)
                    let comph = [Bsv_eascActionStmt(arg, Some lp)]
                    (rulename, comph, RV_noret, lp)
                | other ->
                    //dev_println (sprintf "Other form commission: RV_noret")                        
                    let rulename_string = htosp (snd monica)
                    let comph = [Bsv_eascActionStmt(arg, None)]
                    (rulename_string, comph, RV_noret, g_builtin_lp(*WEAK!*)) // TODO an lp has surely come in with the arg and can be mined for within the arg.
//                  dev_println (sprintf "commission forcef=%A other form %A" ee.forcef other)


        let (tl_walk, bwalk) = walkgen monica walkstack env (emits, fireguard, rulename_string)  // We don't like multiple calls of walkgen on same monica, but happens now for ephemeral method.  Nicer to refactor.
            // can pop this call to bwalk into each conjuncted guard processed by foldwalk_ag...
        //vprintln 0 (htosp monica + " Commission start " + xbToStr ig00)

        let (ig00, sig00, explicit_condition2) =
            match explicit_condition_o with
                | Some explicit_condition ->
                    let og = match ig00.igs.TryFind monica with
                                | Some(IG_alpha og) -> og
                                | None              -> X_true
                                | _ -> sf "og1x form other"
                    let (ig00, sig00, explicit_condition2) = bwalk (ig00, sig00) (explicit_condition) // This will also get the intrinsic guard of the explicit condition.
                    //dev_println ("walkstack=" + (sfold hptos walkstack))
                    //dev_println (htosp (snd monica) + sprintf ": site=%s: bwalk of explict_condition %s gives %s" site (bsv_bexpToStr explicit_condition) (xbToStr explicit_condition2))
                    let g2 = ix_and og explicit_condition2
                    ({ ig00 with igs=ig00.igs.Add(monica, IG_alpha g2) }, sig00, explicit_condition2)
                | None -> (ig00, sig00, X_true) // A true explicit condition does not means its implicit condition is true.


        let rec bev_item (alpha, ig, s0, rv, result) bb =
           
            let (alpha2, ig2, s2, rv2, result2, stopf) = bev_item_ntr (alpha, ig, s0, rv, result) bb
            (alpha2, ig2, s2, rv2, result2, stopf)

        and bev_item_ntr (alpha, ig, s0, rv, result) stmt =
            match stmt with
                | Bsv_varDecl _   // Variable declarations do not need to be kept for second pass since only interface definitions are relevant post elaboration and the interface currency is made on the first pass (at the moment).
                | Bsv_skip _ -> (alpha, ig, s0, rv, result, false)

                | Bsv_resultis((_, stmts), lpo) 
                | Bsv_eascActionStmt(stmts, lpo) ->
                    let liveflow = match stmt with
                                     | Bsv_resultis(stmts, lpo)       ->
                                         if vd>=4 then set_m_m (m0 + " resultis " + (if lpo=None then "anon" else lpToStr0 (valOf lpo)))
                                         X_false
                                     | Bsv_eascActionStmt(stmts, lpo) ->
                                         if vd>=4 then set_m_m (m0 + " action statement " + (if lpo=None then "anon" else lpToStr0 (valOf lpo)))
                                         alpha
                    //vprintln 0 (sprintf "Body at L2553 is %A" (sfold_crlite bsv_stmtToStr stmts))
                    if not_nonep lpo then
                        let mm = lpToStr0 (valOf lpo)
                        let ww = WN mm ww
                        ()
                    let (ig, s0, vv) = tl_walk [alpha] (ig, s0) stmts  // eascActionStmt wrapper removed.
                    let p = match vv with
                            | (ty, B_pliStmt(s, args, lp)) -> // TODO: abstract this as to whether the form needs to update the currency.
                                match bsv_lower ww bobj (snd vv) with
                                    | W_apply((fname, gis), _, args, _) ->
                                        vprintln 3 ("Added pli to output " + fname)
                                        let pli_item = XIRTL_pli(None, alpha (* :: ig1 *), ((fname, gis), None), args)
                                        if not_nonep emits_o && pass.pass_no=2 then mutadd (valOf emits_o).synch_logic (ee.attribute_settings.dirinfo, [pli_item]) // Clearly synch_logic is accumulated backwards - this matters for PLI
                                        true
                            | _ -> false // Other material has side effects on currency ...
                    let rv = match vv with
                                | (_, B_hexp(_, X_undef)) -> rv
                                | _ when p -> rv
                                | _ -> (alpha, vv)::rv // We might flag action statements as to whether they have a return value and skip this if they are pure action.
                    (liveflow, ig, s0, rv, result, false) // TODO check rv has correct ordering for precedence... A tree not a list...

                | Bsv_beginEndStmt(lst, lp) -> // Meerly a List.fold that stops if alpha becomes manifestly dead.
                    if vd>=4 then set_m_m (lpToStr0 lp + " begin/end block")
                    let rec seq (alpha45, ig45, s45, rv45, result45) = function
                        | x when nullp x || alpha45=X_false -> (alpha45, ig45, s45, rv45, result45, false)
                        | h::tt ->
                            let (alpha2, ig2, s2, rv2, result2, stopf) = bev_item (alpha45, ig45, s45, rv45, result45) h
                            if stopf then (alpha2, ig2, s2, rv2, result2, true)
                            else seq (alpha2, ig2, s2, rv2, result2) tt
                    seq (alpha, ig, s0, rv, result) lst

                // Bsv_whileStmt(g, body, lp) -> sf "BSV_while should have disappeared during in elaboration."
                    
                | Bsv_ifThenStmt(gg, t, f, strictf, lp) ->
                    if vd>=4 then set_m_m(lpToStr0 lp + " if/then block")
                    let (ig, s1, gg) = bwalk (ig, s0) gg // ideally returns an alpha' !
                    let split = not strictf
                    if vd>=4 then set_m_m(lpToStr0 lp + " if/then block nn=" + i2s(xb2nn gg))
                    let ans = 
                      match xbmonkey gg with
                        | Some true                    -> bev_item (alpha, ig, s1, rv, result) t
                        | Some false when nonep f      -> (alpha, ig, s1, rv, result, false) // TODO is stopf=false correct?
                        | Some false when not_nonep f  -> bev_item (alpha, ig, s1, rv, result) (valOf f)
                        | None when nonep f ->
                            //vprintln 3 (!m_m + ": single-handed")
                            let (att, igt, s2, rv2, result2, stopf) = bev_item (ix_and alpha gg, g_empty_resource_record, s1, rv, result) t
                            let aff = ix_and alpha (xi_not gg)
                            let igf = g_empty_resource_record
                            let ig3 = ig_mux_dist split ig gg igt igf
                            //vprintln 0 (!m_m + " single-handed IF t=" + bsv_stmtToStr t + " and igt=" + xbToStr igt)
                            //vprintln 3 (!m_m + ": single handed 2")
                            (ix_or att aff, ig3, s2, rv2, result2, false)                            
                        | None when not_nonep f ->
                            //vprintln 3 (!m_m + ": two-handed 1")                            
                            let (att, igt, s2, rv2, result2, stop2) = bev_item (ix_and alpha gg, g_empty_resource_record, s1, rv, result) t
                            //vprintln 3 (!m_m + ": two-handed 2")                            
                            let (aff, igf, s3, rv3, result3, stop3) = bev_item (ix_and alpha (xi_not gg), g_empty_resource_record, s2, rv2, result2) (valOf f)
                            //vprintln 3 (!m_m + ": two-handed 3")          
                            let ig3 = ig_mux_dist split ig gg igt igf
                            //vprintln 3 (!m_m + ": two-handed 4")
                            (ix_or att aff, ig3, s3, rv3, result3, stop2||stop3)

                    vprintln 3 (!m_m + ": done")
                    ans
                | other -> sf (sprintf "commission bev_item other form " + bsv_stmtToStr other)


        // This is not doing a backdoor rule split because the fireguard, which is the schedulable item, is not being replicated.       
        //let alpha11t = ix_and (xi_orred fireguard) explicit_condition2 // For ephemeral method this is not needed.  Not ever needed since done in scheduler or capoff?

        let alpha11t = xi_orred fireguard
        //dev_println (sprintf "commission: bev_item start ig=" + ig_alpha_find_in_map ig00.igs)
        let (alpha_final_, guard_final, sig_final, rv2, result2_, stop_) = bev_item (alpha11t, ig00, sig00, [], []) (bsv_mkBlock lp comph)
        //vprintln 0 ("commission bev_item result ig=" + ig_alpha_find_in_map guard_final)
        let return_val = // rv2 is backwards so List.fold works well
            let grc (ty0_, c) (alpha, (ty, vale)) =
                let v = gen_B_query ww bobj (false, gen_B_bexp alpha, vale, c, ty0_, (g_builtin_lp, CP_none "L4874"))
                //vprintln 0 ("mux " + xbToStr alpha +  " on " + bsv_expToStr vale + " giving " + bsv_expToStr v) 
                (ty, v)
            List.fold grc (Bsv_ty_dontcare "L4224x", B_hexp(Bsv_ty_dontcare "L4224x", X_undef)) (rev rv2)
        //reportx 0 "commission bev_rv" (fun (g, tv)-> xbToStr g + ":   " + bsv_expToStr(snd tv)) rv2
        //vprintln 0 ("commission bev_item (" + i2s(length rv2) + ")return_val=" + bsv_expToStr(snd return_val)) 
        if not_nonep emits_o then // Drive return value net in currency when there is one.
            match retval_o with
                | RV_noret ->
                    // Can give commission stack here: TODO.
                    //hpr_warn(!m_m + ":" + htosp(snd monica) + ": return/resultis statement invalid context (e.g. inside an Action instead of an ActionValue): returned value=" + bsv_expToStr (snd return_val)) // This message is mostly spurious so commented out.
                    //dev_println (!m_m + ":" + htosp monica + ": end of commission with RV_noret")
                    ()
                | RV_netlevel(ty_, n)  when not(undefp(snd return_val)) -> //Bsv_ty_nom(_, F_action, _)
                    // This can tend to be unevaled lambdas... without the undefp - better staging needed - use the forcef!?
                    //dev_println "end of commission with RV_netlevel"
                    //dev_println (!m_m + ":" + hptos(snd monica) +  sprintf ": rv2=%A" rv2)
                    let return_valx =
                        //vprintln 0 "Lowering return val"
                        bsv_lower ww bobj (snd return_val)
                    mutadd (valOf emits_o).comb_logic (gen_bufif(n, return_valx, explicit_condition2)) // Or alpha0 ? TODO.
                | RV_highlevel rvt ->
                    vprintln 3 (!m_m + ":" + htosp(snd monica) + sprintf ": high-level return")
                    ()
                | other -> sf (!m_m + ":" + htosp(snd monica) + sprintf ": +++ Ignoring retvalo ty=%A vale=%s" (other) (bsv_expToStr (snd return_val)))
        (guard_final, sig_final, return_val) // end of commission.


    let compile_decl_or_rule_etc sigma0 stmt = // 
        match stmt with
        | Bsv_varDecl(sv, ty, vidl, dimso, (lp, uid)) when pass.pass_no=2 || sv -> (sigma0, ee)
        | Bsv_varDecl(sv, ty, vidl, dimso, (lp, uid)) ->
            //Variable declaration: the only variables of interest post elaboration are interface definitions and the currency has already been made augmented by the module flattening code.
            //We do not have the ifc_alias (bound_name) (from the decl lhs) here since all should be flat names now.
            //bvl is a list since vector and struct element names are composed. We here autogenerate vector index pairs whereas structs were inlined during norm and the sv flag set for the vestigial .
            let ifc_name = vidl // Always an interface! 
            let m1 = lpToStr0 lp + ": varDecl vidl=" + htosp vidl + " ifc_name=" + htosp ifc_name
            let ww = WF 3 "bsv_compile_decl_or_rule_etc" ww (!m_m + ": " + m1)
            let dmto = ee.genenv.TryFind vidl
            if not(nonep dmto) then cleanexit(!m_m + sprintf ": variable '%s' is defined more than once %A" (htosp vidl) stmt)
            let interface_lst =
                match ty with // Currently we only need the interfaces
                   | (Bsv_ty_nom(kindl, F_ifc_1 items, _)) ->
                       //vprintln 0 (!m_m + sprintf " the_interface spire is %A" (bsv_tnToStr ty))
                       //let ifc = Bsv_ty_nom(kindl, ats, (igenerics', sty), items, lp)
                       [(ty, ty)]  // wierd but ok - the_ifc is really just its name ... can simplify.

                    | Bsv_ty_nom(_, F_vector, _) -> [(ty, ty)] // Vector has ct bound already.
                    
                    | other ->
                        if vd>=8 then vprintln 8 (!m_m + sprintf "non ifc: other is %A" (bsv_tnToStr other))
                        []
            //vprintln 0 (!m_m + "Allx= " + sfold (fun (a, (b))-> htosp a + "->" + bsv_expToStr b) env)

            let env_add (ee:genenv_map_t) (ifc_sty, ifc) =
                // This is just the first step: making an interface reference in the env. Builder creates the currency and nets.
                vprintln 3 (sprintf "env+add: declared interface bound_idl='%s' with  ifc_name=%A" (htosp vidl) (hptos ifc_name))
                let envitem = (vidl, GE_binding(vidl, {g_blank_bindinfo with whom="L2695ifc-ref"}, EE_e(ifc_sty, B_ifc(PI_ifc_flat ifc_name, ifc))))
                ee.Add envitem
            (sigma0, { ee with genenv=List.fold env_add ee.genenv interface_lst })


        |Bsv_eascActionStmt(B_moduleInst _, _) when pass.pass_no=2 -> (sigma0, ee)
        //|Bsv_methodDef _ when pass.pass_no<2 -> (sigma0, ee)            
        |Bsv_eascActionStmt(B_moduleInst _, _)
        |Bsv_resultis _
        |Bsv_varAssign _ 
        |Bsv_methodDef _ ->
            let rec wire_methods ww subif (sigma0, env, sofar) = function
                | Bsv_eascActionStmt(B_moduleInst(bv, (evc, ifc_name, outer_cp), iname, arg, (lp, uid)), _) -> // This is the primary recursion to instantiate lower level modules in design hierarchy.
                    let mf() = "wire_methods"
#if SPARE
                    let new_bsv_eval_iactualt_ (ty, aa) = // Eval of actuals or pram's? -- silly function - delete me TODO  - a version of ewalk that returns the type should not be needed now we use droppings 
                        match aa with
                            | B_action _
                            | B_abs_lambda _ -> EE_e(ty, aa) 
                            | aa ->
                                //vprintln 0 ( sprintf "heres where we discard ty=%s for aa=%A" (bsv_tnToStr ty) aa)
                                bsv_eval_pram bobj ww ee.genenv aa 
#endif
                    let eval_pram_core mf = commission_iactuals (env:genenv_map_t) mf // These should all be constant expressions (mainly interfaces) ans so have not intrinsic guards, but we use commission anyway.
                    let silly_to_lee (ty, vale) = EE_e(ty, vale) // TODO better lift dety outside inst_module to avoid this silly
                    let eval_pram (ty, arg) =
                        match arg with
                            | B_abs_lambda _ -> silly_to_lee(ty, arg) 
 // Pass in ty .... ty____ (* TODO PASS THIS ON AND IN new_bsv_eval_iactualt_ *), 
                            | other -> EE_e(eval_pram_core mf other)
                            
                    let (ww, mod_mk, iactuals') =
                        match arg with
                            | B_thunk(PI_bno(mod_mk, nameo), args) -> 
                                let mf() = (lpToStr0 lp + ": ifcDo thunk " + htosp ifc_name + (if ifc_name<>iname then "/" + htosp iname else ""))
                                let ww = WF 3 "bsv_compile_decl_or_rule_etc - recursive elaborate thunk" ww (if vd>=4 then mf() (*+ ": " + ms*) else " -- ")
                                //let ww = WF 3 !m_m ww ms
                                //m_m := mf
                                //| Some _ -> muddy ("shi clos " + bsv_expToStr arg) // do we need to check iactuals'
                                (ww, mod_mk, map silly_to_lee args) // better use NONE here and keep optional onwards to deabs.

                            | B_applyf(PI_bno(mod_mk, _), _, iactuals, (lp, _)) -> 
                                let mf() = (lpToStr0 lp + ": ifcDo applyf " + htosp ifc_name + (if ifc_name<>iname then "/" + htosp iname else ""))
                                let ww = WF 3 "bsv_compile_decl_or_rule_etc - recursive elaborate module" ww (mf() (* + ": " + ms*))
                                //m_m := mf
                                let iactuals' =
                                    let eval_iactualt (tyvale) = eval_pram tyvale
                                    map eval_iactualt iactuals
                                (ww, mod_mk, iactuals')

                            | other -> sf (sprintf "late elab other %A" other)

                //  let new_item = run_monad ww bobj (ifc_alias, ifc_ty) mod_mk iactuals 
                //  let run_monad ww bobj (ifc_alias, ifc_ty) mod_mk iactuals =
                    let the_ifc =
                        match ee.genenv.TryFind ifc_name with
                            | None ->
                                let m_r = ref []
                                for z in ee.genenv do mutadd m_r z.Key done 
                                vprintln 0 ("All=" + sfold_unlimited htosp !m_r)
                                cleanexit(sprintf "Interface alias '%s' for iname='%s' ifc_name='%s' is not in scope when connecting its implementation module" (htosp ifc_name) (htosp iname) (htosp iname))
                            | Some(GE_binding(_, _, EE_e(ty, B_ifc(iidl, pa))))  -> B_ifc(iidl, pa)
                            | Some other -> sf (sprintf "late elab some other %A" other)
                    let ww' = WF 3 "bsv_Compile_Stmt - recursive elaborate" ww (!m_m + " elaborate " + htosp ee.static_path + " : "  + htosp iname)
                    // TODO this could be an action that is being fired by a behavioural <- operator ...
                    let mod_mk_name =
                        let rec gg = function
                            | B_var(dbi_, idl, _) -> idl
                            | B_applyf(PI_bno(a, _), _, _, _) -> gg a
                            | B_applyf(PI_biltin mc, rt_, args, lp) -> [mc]
                            | B_thunk(PI_bno(gx, _), args) -> gg gx  // All forms are partial applications - the monad term may be computed or still to run, but it is not 'done'
                            | B_maker_l(idl, _, _, _, _, _, _) -> idl
                            | B_maker_abs(idl, _, _, _, _, _, _, _) -> idl                       
                            | x -> sf (sprintf "2xxx other apply form  %A" x)
                        gg mod_mk
                    let get_ty = function 
                        | B_ifc(_, ifcty) -> ifcty

                    let newitem = (bv, GE_binding(bv, {g_blank_bindinfo with whom="L2736newitem"}, EE_e(get_ty the_ifc, the_ifc))) // not currently ultimately used

                    //vprintln 0 (sprintf "varAssignD '%s' bound GE_binding/EE_e to %s" (htosp ifc_alias) (bsv_expToStr the_ifc)) // This binding is perhaps not needed : the lhs is the full name and the rhs is the correct monad element (i.e. the interface).
                    let bbox = memberp mod_mk_name (map fst !bobj.m_synths)
                    //establish_log false ("next_target:" + htosp iname)             // do this before eval iactuals if at all
                    let ((new_inode, sigma0), new_env_items_) = bsv_instantiate_module ww' bbox bobj pass uid None lp emits_o (sigma0, ee) (evc, the_ifc, ifc_name, (iname, outer_cp), iactuals') mod_mk
                    let sigma0 =
                        //println(sprintf "Save new inode %A under name %s" new_inode (hptos ifc_name))
                        carenv_write bobj pass "L7829" sigma0 ifc_name new_inode  // Save the interface inode in the interface map.  

                    if bbox then
                        //vprintln 0 (sprintf "bbox save2 %A" currency)
                        let cvx = find_ifc mf iname sigma0 
                        let getused cc = function
#if WIP_RETCAM
                            | EE_e(ity_, B_ifc(PI_ifc_flat used_name, ity__)) ->
                                let xx = find_ifc mf used_name sigma0
                                (used_name, LT_curr xx)::cc
#endif
                            | EE_e(ty_, B_hexp(ty, hexp)) ->
                                let _ = 0 //"parameters not supported for separate synthesis at the moment ..." + xToStr hexp)
                                ([], LT_pram(B_hexp(ty, hexp)))::cc
                            | other -> sf (sprintf "bbox other %A" other)
                        let prams_and_etc = List.fold getused [] iactuals'
#if SPARE
                        let sigma__ =
                            match cvx with
                                | _ when false -> ((sf "holding") : bsv_sigma_t list)
                                | TRACE_ifc(items) -> // Do not use items directly: look each up separately.  This might be unnecessary in this context.
                                    let trag2 arg cc =
                                        match arg with
                                            | Sigma(id, _, (proto, retval, m_ats), args) ->
                                                let key = id :: iname
                                                dev_println (sprintf "MIRROR blackbox formal find from %A" key)
                                                match sigma9.ifcmap.TryFind key with
                                                    | Some(TRACE_currency currency) ->
                                                        map snd currency @ cc 
                                                    | other  -> sf(sprintf "trag2 other sigma lookup form %A" other)
                                            | other ->  sf(sprintf "trag2 other form %A" other)
                                    let trag1 arg cc =
                                        match arg with
                                            | (_, TRACE_conc0 items) ->List.foldBack trag2  items cc
                                            | other ->  sf(sprintf "trag1 other form %A" other)
                                    let currency  = List.foldBack trag1 items []
                                    currency
                                    
                                | other ->
                                    sf (sprintf "Missing hard currency (net-level contacts) for bbox iname=%s. Found other %A" (hptos iname) other)
#endif
                        if not_nonep emits_o then mutadd bobj.m_bboxes_encountered (mod_mk_name, get_ty the_ifc, iname, ee.attribute_settings.dirinfo, prams_and_etc)
                    (sigma0, ee.genenv.Add newitem, sofar)


                | Bsv_resultis((e_ty, earg), lp) -> // Result of an interface xformer. or ex_05_g 'return m;'
                    let m1 = (if nonep lp then "" else lpToStr0(valOf lp)) + ":YA monad resultis/return insert"
                    let ww = WF 3 "bsv_compile_Stmt - resultis" ww ("Start " + m1)
                    // Return of an interface adds it to the monad (is an assign to it...)
                    match (e_ty, earg) with
                        // happy to roll on with maker_abs - > just need to boot to a B_moduleInst
                        | (_, B_thunk(PI_bno(B_maker_abs(idl, ats, aprams3, provisos, body, ee_inner, backdoors, lp), aa_), args)) -> //toyconnect4

                        //This elebaoration should be staged before commission.
                        //WHY HAS THIS STOPPED WORKING _ I JUST MADE UP THE NEXT BIT : June 2014.
                            // call deabs4 etc??? .... well no we are happy with a maker_abs later on
                            let bv = [funique "resultis_iname"]// B_ifc(PI_ifc_flat ifc_name,  ifc_ity)
                            hpr_yikes (sprintf " strangely using %A instead of %A" bv ifc_name)

                            // ... overwrite above edit
                            let bv = ifc_name
                            let outer_cp = ee.callpath
                            // Dont : Apply hd here to get last part since the prefix gets added on again
                            let siz = B_moduleInst(bv, (ee.attribute_settings.dirinfo, ifc_name, outer_cp), bv, earg, (lp, "this is unused - passed to instantiate module which then ignores it ..TODO delete me ... need uid L6238"))
                            //vprintln 0 (sprintf "+++ %s: thunkated (toyconnect4) in use - please check circuit ! #args=%i " (htosp idl) (length args))
                            (wire_methods ww subif) (sigma0, env, sofar) (Bsv_eascActionStmt(siz, Some lp))


                        | (_, B_ifc_immed(il)) -> // not used? looks like primary consumer of this form hence strange not used at all ... the resultis clause above should go round again and pick up this form possibly, but actually such ActionStmts come in from outside and need to be handled in the outer wire_methods call so either approach works.
                            hpr_yikes (m1 + "+++ used wiz that was believed not used?")
                            let (sigma0, env, sofar) = List.fold (wire_methods ww subif) (sigma0, env, sofar(*if used, please insert null here and bind result*)) il
                            (sigma0, env, sofar) 
                        | other ->                            
                            vprintln 0 (m1 + sprintf " xy-other ifc_ty=%A e_ty=%A e=%A" ifc_ty e_ty earg)
                            muddy (sprintf "xy other form: %A" other)

                | Bsv_varAssign(borrowed_, name, B_ifc_immed(items), whom, lp) -> // An immediate list of interface components.
                    let m1 = lpToStr0 lp
                    let extended = name @ subif // This is tested in ["gauche"; "xxx"; "test6m"; "Test6"]
                    let ifc_name1 = extended @ nprefix // Append path component names for true subinterface name.
                    let (sigma0, env, contents) = List.fold (wire_methods ww extended) (sigma0, env, []) items // Prefer a foldBack here?
                    let contents = map snd contents
                    vprintln 3 (m1 + sprintf ": Bsv_varAssign B_ifc_immed make an entry for %s with %i contents." (hptos ifc_name1) (length contents))
                    let hintname = hptos ifc_name1
                    let inode = primary_iss sigma0 hintname
                    let (sigma0) = sigma_add_methods_core "site-Bsv_varAssign" ifc_name1 inode sigma0 contents
                    (sigma0, env, sofar) // Perhaps augment sofar and return unmodifed env?

                | Bsv_varAssign(borrowed_, name_to_bind, B_ifc(PI_ifc_flat path, ty), whom, lp) -> //  This is the old TRACE_redirect(str, vale) clause.
                    let m1 = lpToStr0 lp
                    let boundpath = name_to_bind @ subif @ nprefix
                    //let msg = sprintf "Bsv_icf_assoc subif=%s name_to_bind=%s ..." (hptos subif) (hptos name_to_bind) + m1
                    let msg = m1 + sprintf ":  Bsv_icf_assoc: subinterface via assignment (or alias establishment): binding ifc %s as %s" (htosp boundpath) (hptos path) 
                    vprintln 3 msg
                    let vale = // subinterface via assignment (or alias establishment)
                        match sigma0.ifcmap.TryFind path with
                            | Some vale ->
                                vale:interface_inode_name_t
                            | None _ ->
                                cleanexit (lpToStr0 lp + sprintf ":  interface/subinterface via assignment (or alias establishment: binding ifc %s with rhs=%s - rhs interface not found" (htosp name_to_bind) (hptos path))
                    let nb = (boundpath, vale)
                    let sigma0 = carenv_write bobj pass "Bsv_varAssign" sigma0 boundpath vale  // Save the interface inode in the interface map.  
                    (sigma0, env, sofar)

                | Bsv_varAssign(borrowed_, name, B_applylam(acp, info, args, lp, flg, ans), whom, lp_) ->
                    vprintln 3 (sprintf "Bsv_ifc_assoc/varAssign B_applylam...")
                    let _ =
                        if false then //TODO delete this
                            match info with
                                | PI_bno(B_lambda(nameo, formals1, body, fun_ty, closure_ee, lp1), nameo_) ->
                                    // we need to deep bind args into formals here to augment closure_ee
                                    let vd = 0
                                    vprint vd "In-scope is (sosis) : "
                                    for z in closure_ee.genenv do ignore(vprint vd (htosp z.Key + " ")) done 
                                    vprintln vd ""
                                    ()
                    let (tl_walk, bwalk) = walkgen (false, ifc_name) (* "nottobeused-ifc-monica"*) [(IIS "", hptos ifc_name)] env (emits_o, X_undef, "nottobeused=rulename") 
                    let (g__, sigma0, (ians_ty_, ians)) = tl_walk [] (g_empty_resource_record, sigma0)  (B_applylam(acp, info, args, lp, flg, ans)) // TODO or other exp
                    //vprintln 0 (sprintf "ifc_assoc method arg=%s ans=%s" (bsv_expToStr(B_applylam(acp, info, args, lp, flg, ans))) (bsv_expToStr ians))
                    let (sigma0, env, contents_) = wire_methods ww subif (sigma0, env, []) (Bsv_varAssign(borrowed_, name, ians, whom, lp_))
                    // Here augment sigma with a new bind as per other Bsv_varAssign clauses?
                    (sigma0, env, sofar)
                    
                // A varAssign, also called an ifc_assoc, implements a redirection so that one interface has more than one name.
                | Bsv_varAssign(borrowed_, name, B_subif(ss, vale, ty, lp), whom, lp_) -> // This route no longer used?
                    //0 bsv_compile_Stmt iname=Test6c.test6m.xxx trotski <--- B_subif:gauche:B_ifc:IFC-the_myareg:interface:Test6c.Abstract_regA1.<absdata_t:motadata_t>
                    //ty=Left, name=trotski, ss=gauche, vale=B_ifc(PI_ifc_flat  [the_myarg], ...) // Need ded lookup? That's done in final apply? May be wrong place to do it.
                    vprintln 3 (sprintf "Bsv_icf_assoc B_subif ...")
                    let lhs = name @ subif @ ifc_name
                    let rhs = match vale with
                               | B_ifc(PI_ifc_flat ifc1, _) -> vale
                               | B_ifc(PI_ifc_hier ifc1, _) when false -> // no longer used ? // TODO remove this PI_ifc_flat  before making B_subif ?
                                   match env.TryFind ifc1 with
                                    | Some(GE_binding(_, _, EE_e(_, B_ifc(PI_ifc_flat  vale1, ty)))) -> B_ifc(PI_ifc_flat vale1, ty)
                                    | Some other -> sf (sprintf "other form of subinterface base %A" other)
                                    | None -> cleanexit(!m_m + sprintf ": subinterface '%s' has an undefined base '%s'" ss (bsv_expToStr vale))
                               | other -> cleanexit(sprintf "subinterface assign: not an interface ?  %s" (bsv_expToStr other))
                    vprintln 3 (sprintf "Added interface alias to env: '%s' -> '%s'" (htosp lhs) (ss + " on " + bsv_expToStr rhs)) 
                    // Here augment sigma with a new bind as per other Bsv_varAssign clauses instead of a TRACE_redirect ?
                    muddy "RETCAM 333"
#if WIP_RETCAM
                    let sigma0 = carenv_write bobj ...
                    let sigma0 = { sigma0 with ifcmap=sigma0.ifcmap.Add(lhs, TRACE_redirect(ss, rhs)) }
#endif
                    (sigma0, env, sofar)
                // Non-ephemeral method def does not expand the currency sigma - it instead connects implementation logic to existing interface wiring.
                // Ephemeral/reentrant method def does nothing at this point and is elaborated lazily on demand as many times as needed.
                | Bsv_methodDef(mood, mname, proto, potentially_ephemeralf_, rt, formals, explicit_condition, actions, lp) ->
                    let methodname = hd mname // mname is wierdly flat at this point but subinterface stupidly missing - just take final element (first on reversed list).
                    let ifc_name1 = subif @ nprefix
                    let monica = (true, methodname :: ifc_name1) // An external method.
                    let mf() = (lpToStr0 lp + sprintf ": methodDef %s potentially_ephemeral=%A with instance nprefix=%s interface=%s" (htosp mname) potentially_ephemeralf_ (htosp nprefix) (htosp ifc_name1))
//Note we now ignore potentially_ephemeralf and rely toplevel to have put the external interfaces into the hard currency already.
                    if vd>=4 then set_m_m(mf()) // Don't like this m_m anymore - please delete.
                    let ww = WF 3 "bsv_compile_Stmt" ww (if vd>=4 then mf() else "methodDef " + methodname)
                    let sigma0 =
                        //println(sprintf "Save method/new inode %A under name %s" inode (hptos ifc_name1))
                        carenv_write bobj pass "methodDef-L7998" sigma0 ifc_name1 inode // Save the interface inode in the interface map.   This is replicated in toplevel?


                    match find_meth mf sigma0 ifc_name1 methodname with 
                        | (key_, None, Some _, _) when pass.pass_no < 2 -> cleanexit(lpToStr lp + ": DMTO: method defined more than once. name=" + hptos(snd monica)) // already defined as ephemeral
                        | (key1, None, eph_o, _) when nonep eph_o || pass.pass_no = 2 -> // If concrete, should still be there from first pass. No, we've just called mk_currency in instantiate_module actually.
                            //| (key_, None, None, _)   ->  // Neither concrete nor ephermal: enter as ephemeral, regardless of potentially_ephemeralf_.
                            //dev_println (sprintf "old key1=%A   old_eph_o=%A" key1 eph_o)
                            let key1 = (inode, methodname) // key1 returned from find_method is invalid when missing? Now just added ten lines above.
                            vprintln 3 (sprintf "Making ephemeral map entry for fname=%s under inode %A on pass %i. toplevel=%A" (hptos(snd monica)) key1 pass.pass_no toplevel)
                            if not_nonep toplevel then sf(sprintf "Should not be making ephemeral toplevel/external method! %s" methodname)
                            let nb1 = (key1, Sigma_e(methodname, rt, proto, formals, explicit_condition, actions, lp))
                            let sigma0 = { sigma0 with methmap=sigma0.methmap.Add nb1 } 
                            (sigma0, env, nb1::sofar)

                        | (key_, Some(Sigma(id3, _, (proto, retval, m_ats), args)), _, None) -> // Concrete/external method definition.
                            //let _ = okmeet ww [] (rt, retval) // now done during typecheck/norm?
                            // Concrete (ie non-ephemeral) method will already have its nets in the currency
                            let bind (env:genenv_map_t) ((formal_name, net, cv), (so, ty, id)) =
                                //vprintln 0 ("Binding formal protocol=" + formal_name + " formal=" + id + " in " + mm)
                                env.Add([id] (* :: prefix ? *), GE_binding([id], {g_blank_bindinfo with whom="L2822methoddef"}, EE_e(ty, B_hexp(ty, net))))  // TODO do we want nets in the env?
                            let env1 = List.fold bind env (List.zip args formals)
                            let (callguard, rdy) = match proto with
                                                   | BsvAction(None, [], en)            -> (en, xi_num 1)
                                                   | BsvAction(Some rdy, [], en)        -> (en, rdy)                                               
                                                   | BsvValue(Some rdy) -> (xi_num 1, rdy)
                                                   | BsvValue(None) -> (xi_num 1, xi_num 1)
                                                   | other -> muddy (sprintf "callguard/rdy other %A" other)

                            vprintln 3 (!m_m + ":" + hptos(snd monica) + ": commission concrete method body")
                            let (guard_final, sigma0, rv) = commission ww "concrete method" env1 monica [(IIS "", hptos(snd monica))] callguard (Some explicit_condition) (emits_o, g0, sigma0) (gen_B_action "Commission L6796" ee.fbody_context (methodname, actions, retval, lp))
                            let sigma0 = commit_pending_updates ww bobj mf sigma0
                            // If the copy to result bus is on end of commission then it's a bit late committing updates here.  Fortunately we are not using concrete methods with pending updates in any of our tests.  Should ban explicitly to fence off a future trap.
                            //dev_println (!m_m + ":" + hptos monica +  sprintf ": currency_retval=%A rvf=%A" retval rv)
                            // If lhs net is a constant (1) then do not assign to it!
                            if not_nonep emits_o then
                                let ids = htosp((f3o3 id3)::nprefix)
                                if pass.pass_no = 2 then
                                    vprintln 3 (sprintf "log RC_methbuf for " + ids)
                                    mutadd (valOf emits_o).rulectl (RC_methbuf((f3o3 id3)::nprefix, ids, callguard, rdy, guard_final))
                            (sigma0, env, (* for completeness could make an entry for a non ephemeral method here nb::sofar*) sofar)

                        | other -> muddy (mf() + sprintf ": methodDef other form: %A" other) // 

                | other -> sf (sprintf "other stmt in ifc or sub-interface %A" other)
            let (sigma0, env, _) = List.fold (wire_methods ww []) (sigma0, ee.genenv, []) [stmt]
            (sigma0, { ee with genenv=env })

            //| Bsv_rule _ when pass.pass_no<2 -> (sigma0, ee)
        | Bsv_rule(rulerec, explicit_condition, rulebody, keyname, lp) ->
            let mf() = lpToStr0 lp + sprintf ": Rule '%s'" rulerec.name
            let ww = WF 3 "bsv_compile_Stmt" ww ("Start " + !m_m + ": " +  mf())
            if vd>=4 then set_m_m(mf()) // Don't like this
            let monica = (false, rulerec.name::nprefix)
            let monica_s = htosp (snd monica)
            let no_implicit_conditions = test_att bobj.prags keyname Pragma_no_implicit_conditions
            let fire_when_enabled = test_att bobj.prags keyname Pragma_fire_when_enabled // These test_att forms would be better of inside rulerec then we dont need keyname?
            let fireguard = gen_fireguard emits_o monica_s
            //dev_println (mf() + " xguard=" + bsv_bexpToStr explicit_condition)
            //let ee_inner = { ee with rulename_issue=Some(=rulename_string, superscalar_issue_no) }
            let (guard_final, sigma0, rv_) = commission ww "rule" ee.genenv monica [(IIS "", hptos(snd monica))] fireguard (Some explicit_condition) (emits_o, g0, sigma0) (gen_B_action "From Rule L6820" ee.fbody_context (rulerec.name, rulebody, RV_noret, lp))
            let shedats =
                let m_r = ref []
                if fire_when_enabled then mutadd m_r SF_fire_when_enabled
                if no_implicit_conditions then  mutadd m_r SF_no_implicit_conditions // TODO check what this is supposed to do and do we do it?
                !m_r
#if MOVEME
                //If no implicit conditions, the explicit condition will be unqualified  (I . E) |!E should be a tautology when I is such.
                match my_false(ix_or (guard_final) (ix_not explicit_condition)) with
                    | other -> cleanexit(ms + "Rule was declared to have no implicit conditions but infact has an intrinsic guard of " + xbToStr other)
            //dev_println (mf() + sprintf ": rule compiled. explicit_condition=%s guard_final=%s find=%s" (bsv_bexpToStr  explicit_condition) (igMapToStr guard_final.igs) (ig_alpha_find_in_map guard_final.igs))
            let ncf = false // False here since now reported in the scheduller?  Handy to check here too.
            if ncf then
                hpr_warn(!m_m + sprintf ": Rule '%s' can never fire (intrinsic guard invalid) and is being discarded." rulerec.name)
                (sigma0, ee) // This return value is the discard operation... do we still like this?
#endif                
            let sigma0 = commit_pending_updates ww bobj mf sigma0
            let rinfo = { fireguard=fireguard; resource_records=guard_final; rulename= snd monica; shedats=shedats; srcfileref=lp; rulerec=rulerec }
            if pass.pass_no = 2 && not_nonep emits_o then
                vprintln 3 (sprintf "log RC_rule for " + hptos rinfo.rulename)
                mutadd (valOf emits_o).rulectl (RC_rule rinfo)
            (sigma0, ee)

            //| Bsv_primBuffer _ when pass.pass_no<2 -> (sigma0, ee)
        | Bsv_primBuffer(l, r, lp) ->
            let mf() = lpToStr0 lp + ": primBuffer "
            let ww = WF 3 "bsv_compile_Stmt - primBuffer" ww ("Start " + (if vd>=4 then !m_m + ":" + mf() else ""))
            if vd>=4 then set_m_m(mf())
            let bufname = "primBuffer"
            let monica = (false, bufname::nprefix)
            let ww = WF 3 !m_m ww bufname
            let callguard = gec_X_net "primbuffer_fireguard_notused" // Not expecting a prim buffer to be guarded
            let (tl_walk, bwalk) = walkgen monica [(IIS "", hptos(snd monica))] ee.genenv (emits_o, callguard, "primBuffer") 
            let (g1, sigma0, (_, l')) = tl_walk [] (g0, sigma0) l
            let (g2, sigma0, (_, r')) = tl_walk [] (g1, sigma0) r
            if not_nonep (emits_o) then mutadd (valOf emits_o).comb_logic (gen_buf(bsv_lower ww bobj l', bsv_lower ww bobj r'))
            (sigma0, ee)

        | other -> sf (sprintf "compile_decl_or_rule_etc bsv_compile_Stmt other %A" other)

    //vprintln 0 (sprintf "compile_decl_or_rule_etc bsv_compile_Stmt start %A" (bsv_stmtToStr stmt00))
    let (sigma0, env) = compile_decl_or_rule_etc sigma0 stmt00
    //vprintln 0 (sprintf "compile_decl_or_rule_etc bsv_compile_Stmt end")        
//    vprintln 0 (!m_m + "Allenv post op is " + sfold (fun (a, b)->htosp a + "->" + eeToStr b) env)
    (sigma0, env) // end of bsv_compile_Stmt

// Instantiate is a function application which generates circuitry as a side effect.
// We call instantiate_module with the iactuals eval'd already - standard call by value.
// One new item is returned to be added to the parent local bindings - typically an interface.
// This is an execute of the build routine for the module. Its instance name is the prefix.
// This is called for the top-level and then recursively.
and bsv_instantiate_module ww bbox (bobj:bsvc_free_vars_t) pass uid_ toplevel lp emits_o (sigma, ee) (dirinfo, B_ifc(PI_ifc_flat ifc_name, ((Bsv_ty_nom(i_kind, F_ifc_1 protos, dx)) as ifc_ty)), ifc_alias, (iname, outer_cp), iactuals0) mod_mk =
    let vd = bobj.normalise_loglevel
    let mpactuals = [] // Found by jiggle/partial application
    let prefix' = iname 
    // First make interface

    let hintname = hptos ifc_name
    let inode = primary_iss sigma (hintname)

    let mf() = lpToStr0 lp + ": instantiate module (primary) " (* "kind =" + htosp idl *)  + " ifc_name=" + htosp ifc_name + ", ifc type=" + bsv_tnToStr ifc_ty + ", iname=" + htosp iname + " outer_cp=" + cpToStr outer_cp
    let dc_entry = ([mf()], outer_cp)
    let ee = { ee with dynamic_chain = dc_entry :: ee.dynamic_chain; }
    //vprintln 0 (sprintf "iactuals0 = %A" iactuals0)
    let ww = WF 3 "bsv_instantiate_module start" ww (mf())
    let rec scan_borrowed pref cc = function // We rather crudely look for borrowed interfaces and avoid re-making currency for them.
        | Bsv_varAssign(false, name, mval, whom, lp)              -> (name @ pref)::cc // not a borrowing - just append parts to get path name
        | Bsv_varAssign(true, name, B_ifc_immed(items), whom, lp) -> List.fold (scan_borrowed (name@pref)) cc items
        | _ -> cc

        
    let ee = { ee with callpath=CPS(PS_prefix "L6389", map (fun x->(x, 0, None)) prefix') }
    //let ee = { ee with callpath=outer_cp } 
   //vprintln 0 (mf() + " iactuals (post eval) = " + sfold eeToStr iactuals0)

    let build_maker_l ww iactuals (B_maker_l(idl, mod_ats, (mpformals, iused, iexported, hardstate), stmts, ifc_provisos, backdoors, lp)) =
        let (ms, sl) = (not_nonep (op_assoc idl !bobj.m_synths), memberp idl !bobj.m_compilation_stoplist)
        let separate_box = ms || sl
        let separate_box = (separate_box || hardstate)
        if separate_box then vprintln 2 (sprintf "Treating %s as blackbox owing to one of: synth_marked=%A, compilation_stoplist=%A, hardstate=%A." (hptos idl) ms sl hardstate)
        let msg = ((if not_nonep toplevel then "making top-level instance of " else "making an instance of kind=") + htosp idl + sprintf ", ifc_name=%s separate_box=%A" (htosp ifc_name) separate_box)
        let ww = WF 3 "build_maker_l" ww ("Start build " + msg)
        
        //let ((descending_urgency_, conflict_free_, mutually_exclusive_), execution_order_) = degroom_schedulling_atts ww !bobj.m_shed_control_list

        let borrowed = List.fold (scan_borrowed prefix') [] stmts
        //vprintln 0 ("Borrowed interfaces = " + sfold htosp borrowed)
        let env = ee.genenv
        // First make interface : modified by pragma ats from module..
        //let if_emits = gec_blank_emission()
        // Currency nets might now be added now by toplevel.subcomponent_instance
        let if_emits_o = emits_o
        let siggy = mk_ifc_currency ww bobj pass mf if_emits_o (toplevel, false) borrowed separate_box ee (iexported, mod_ats, ifc_name, Bsv_ty_nom(i_kind, F_ifc_1 protos, dx))

        // Then instantiate module
        // Simple constants are sometimes types and sometimes parameters. If we have too few parameters and too many types then adjust...
        let n_mpf = length mpformals
        let n_if = length iused
        let rec jiggle(n_mpa, mpactuals, n_ia, iactuals) =
               if (n_mpa < n_mpf && n_ia > n_if) 
               then
                   let mpactuals' = mpactuals @ [(*valueof*)(hd iactuals)]
                   vprintln 4 ("Jiggle args to parameters: now have prams=" + sfold eeToStr mpactuals')
                   jiggle(n_mpa+1, mpactuals', n_ia-1, tl iactuals)
               else (n_mpa, mpactuals, n_ia, iactuals)
        let (n_mpa, mpactuals, n_ia, iactuals) = jiggle(length mpactuals, mpactuals, length iactuals, iactuals)

        if n_mpf <> length mpactuals then
               vprintln 0 (mf() + ": Too many/few i-parameters in instantiation of " + htosp idl + ": #f=" +  i2s n_mpf + " <> #a=" + i2s(length mpactuals))
               cleanexit("")
        if (n_if) <> length iactuals then
               vprintln 0 (mf() + ": Too many/few arguments in instantiation of " + htosp idl + ": #f=" +  i2s n_if + "<> #a=" + i2s(length iactuals))
               cleanexit("")

        if bbox then 
            vprintln 3 (mf() + ": Leaveout at this point as a subcomponent or black box ")
            let sigma = sigma_add_methods "bbox" ifc_name inode sigma siggy // Its methods still need to be callable (ie we still generate code to drive its interfaces).
            (inode, sigma)
        else
            let argfun_e ((so, formal_type, formal_name), actual) = muddy "(formal_name, (formal_type, TL_v actual))" //TODO this always aborts, but currently no natives imported interfaces.
            let (further_stmts) =
               if memberp "cambridge" backdoors then
                   let scowering_envfun ((so, formal_type, formal_name), actual) = (formal_name, (formal_type, actual))
                   match g_kernels_builtin.lookup idl with
                       |  None -> sf ("supposedly builtin kernel not defined "  + htosp idl) 
                       |  Some (mk, arities_) ->
                           let imap = map argfun_e          (List.zip iused iactuals)
                           let mpmap = map scowering_envfun (List.zip mpformals mpactuals)
                           let further_stmts = mk (WN ("kernel mk elaborate " + htosp prefix') ww) bobj if_emits_o ee (dirinfo, prefix', mpmap, imap, siggy)
                           further_stmts
                else []

            // TODO env is not augmented in mk application.
            // TODO make these use the same type bindings!
            let argfun_lt ((so, formal_type, formal_name), actual) =
               if vd>=4 then vprintln 4 (mf() + sprintf ": bind generator formal '%s' with %s" (formal_name) (eeToStr actual)) 
               ([formal_name], GE_binding([formal_name], {g_blank_bindinfo with whom="argfun_lt"}, actual))
            let mpmap = map argfun_lt (List.zip mpformals mpactuals)
            let imap = map argfun_lt (List.zip iused iactuals)
            let env = List.fold (fun (env:genenv_map_t) (k,v) -> env.Add(k,v)) env (mpmap @ imap) // bind prams and iactuals
            // This makes a loop: do not do it.
            //if not_nonep emits_o then mutadd (valOf emits_o).children (valOf_or_fail "L7860" if_emits_o)
            let g0 = g_empty_resource_record

            let ww = WF 3 "build_maker_l" ww ("Compile statements: iname/callpath=" + htosp prefix')
            let sigma =
                let new_emits_o =
                    if nonep emits_o then None
                    else
                        let emitbin = gec_blank_emission "sigmash"
                        //dev_println (sprintf "Rez emitter %s" emitbin.uid)
                        Some emitbin 
                let ee1 = { ee with genenv=env; prefix=prefix'; callpath=CPS(PS_prefix "L6457", map (fun x->(x, 0, None)) prefix'); module_path=idl; static_path=idl } 

                let neg_num msg = 0 - next_stmt_blk_index msg
                if not_nullp further_stmts then mutadd pass.m_statements (next_stmt_blk_index "further", further_stmts, [], (ifc_ty, ifc_name, toplevel, ee1))

                //TODO further statements when from the FSM sublang need sorting.
                let numbered_statements = // Statements that are subject to execution order sorting are given positive numbers: these are rules and (external) method definitions.  These should/could be a different type from behavioural statements such as resultis and if/then ?
                    let rec add_stmt_numbers = function
                        | [] -> []
                        | arg::tt ->
                            match arg with
                                | Bsv_skip _         -> (add_stmt_numbers tt)
                                | Bsv_rule _
                                | Bsv_methodDef _   -> (next_stmt_blk_index "rule/method", arg) :: (add_stmt_numbers tt)
                                | Bsv_primBuffer _  -> (neg_num "primBuffer", arg) :: (add_stmt_numbers tt)
                                | Bsv_varDecl _     -> (neg_num "varDecl", arg) :: (add_stmt_numbers tt) 
                                | Bsv_varAssign _   -> (neg_num "ifc_assoc", arg) :: (add_stmt_numbers tt)
                                | Bsv_ifThenStmt _  ->
                                    hpr_yikes(sprintf "if/then stmt needs numbering")
                                    (neg_num "ifThenStmt", arg) :: (add_stmt_numbers tt)
                                | Bsv_beginEndStmt _ ->
                                    hpr_yikes(sprintf "begin/end stmt needs numbering")
                                    (neg_num "beginEndStmt", arg) :: (add_stmt_numbers tt)
                                | Bsv_eascActionStmt _ -> (neg_num "eascActionStmt", arg) :: (add_stmt_numbers tt)
                                | arg ->
                                    hpr_yikes(sprintf "other form stmt needs numbering: %s" (bsv_stmtToStr arg))
                                    (neg_num "other form stmt", arg) :: (add_stmt_numbers tt)
                            
                    add_stmt_numbers stmts
                // Here optionally make a listing of elaborated code -- TODO to a file
                if bobj.make_a_listing then
                    let show_stmt (no, stmt) = dev_println (sprintf "%i||: " no +  bsv_stmtToStr stmt)
                    app show_stmt numbered_statements
                else vprintln 3 (sprintf "No listing of elaborated code made.")

                //let all_stmts = insert_superscalar_rule_repetitions // why commented out?  Done for second pass, not here.

                let sigma = sigma_add_methods "L7480" ifc_name inode sigma siggy
                let (sigma, gully_env_) =
                    let compile_numbered_block (sigma, ee1)  (no, stmt) =
                        //let ww = WF 2 ("bsv_compile_Stmt") ww (sprintf "Start first pass compile, pre-sortm, batch of %i statements" (length all_stmts))
                        // First/zeroth compile site.  
                        //if vd>=4 then vprintln 4 (sprintf "numbered statement old site %i: Show body stmt: " no +  bsv_stmtToStr stmt)
                        let (sigma, ee2) = (bsv_compile_Stmt ww bobj pass (ifc_ty, inode, ifc_name) g0 toplevel new_emits_o 0) (sigma, ee1) (stmt, no)
                        let (eocs, sigma) = (sigma.exordering, { sigma with exordering=[] })
                        mutadd pass.m_statements (no, [stmt], eocs, (ifc_ty, ifc_name, toplevel, ee1))
                        (sigma, ee2)
                    List.fold compile_numbered_block (sigma, ee1) numbered_statements

                if not_nonep emits_o then
                    //dev_println(sprintf "attach emits %s to %s" (valOf new_emits_o).uid (valOf emits_o).uid)
                    mutadd (valOf emits_o).children (valOf_or_fail "L7881" new_emits_o)
                sigma
            vprintln 3 ("Finished add of module " + htosp idl + " to the system with interface ifc_name=" + htosp ifc_name)
            //if vd>=4 then vprintln 4 (mf() + ": currency_item=" + sfold (fun (idl, _) -> htosp idl) siggy)
            let ww = WF 3 "build_maker_l" ww ("Finished build " + msg)
            (inode, sigma)

    let rec module_elaborate ww iactuals2 mod_mk =
        let absurd_cvt_to_lee (ty, vale) = EE_e(ty, vale)
        match mod_mk with
        | B_applyf(PI_biltin mc, rt_, args, (lp, _)) ->
            ignore(exec_reporters ww mf (lpToStr0 lp) mc ee.dynamic_chain args) //TODO use valOf iactuals2 to be neat
            (IIS "", sigma)

        | B_thunk(PI_bno(B_maker_abs(idl, ats, aprams3, provisos, body, ee_inner, backdoors, lp), aa_), args)
            // the difference between these two forms, thunk and applyf, should be whether the args are eval'd already.
        | B_applyf(PI_bno(B_maker_abs(idl, ats, aprams3, provisos, body, ee_inner, backdoors, lp), aa_), (*rt*)_, args, _(*lp1*)) ->            
            let mod_mk = B_maker_abs(idl, ats, aprams3, provisos, body, ee_inner, backdoors, lp)
            if not(nullp iactuals2) then sf "internal error: second rate applyf: args and iactuals both present."             
            let iactuals2 = map absurd_cvt_to_lee args
            module_elaborate ww iactuals2 mod_mk

        | B_maker_l(idl, mod_ats, (mpformals, iused, iexported, hardstate), stmts, ifc_provisos, backdoors, lp) ->
            build_maker_l ww iactuals2 mod_mk

        | B_maker_abs(idl, ats_, p3a_, provisos_, body_, ee_inner_, backdoors_, lp) ->
            let msg = ("Deabstraction of " + hptos idl)
            vprintln 3 msg
            let uid = (*smell uid_*) hd ifc_name
            //dev_println (sprintf "swold: mod_mk prev=%A\nEnd of prev print." mod_mk)
            //g_hl_call_trace := true
            let mod_mk = deabs4_exp ww bobj vd (Some uid) ee iname msg mod_mk ((ifc_ty, outer_cp, iactuals2)) // The only remaining call site for deabs4_exp
            //dev_println (sprintf "swold: mod_mk x=%A\nEnd of mod_mk x print." mod_mk)
            module_elaborate ww iactuals2 mod_mk

        | pother -> sf (sprintf "module_elaborate: expected module monad, not '%s'  details=%A" (bsv_expToStr pother) pother)
    let (new_inode, sigma) = module_elaborate ww iactuals0 mod_mk

    let ifc_lee = GE_binding(ifc_alias, {g_blank_bindinfo with whom="L2908instmod"}, EE_e(ifc_ty, B_ifc(PI_ifc_flat ifc_name, ifc_ty)))
    let envitems = [(ifc_alias, ifc_lee)] // if ifc is generated before calling this then why do we have an envitem ... for compile1's env  The provisos are needed for the actual interface nets but its signature is trivial to make. Also we alpha convert here ... do we ?  Don't need to alpha convert in reality since, unlike an interface, there is only one maker's variable bindings in scope at any one time.  Nonetheless they might potentially clash with other identifiers? No they cant since everyone else is freshly alpha converted.
    ((new_inode, sigma), envitems) // end of bsv_instantiate_module


    
// Preamble defines:

and ee_clone (ee:ee_t) =
    let copy = new muts_t("muts-copy")
    let copyout k v = copy.add k v
    for z in ee.muts do ignore(copyout z.Key z.Value) done     
    (ee, { ee with muts=copy; }) // Sometimes we will rely on the lhs return val being ee_orig.


and gen_B_reveng_l ww bobj mf ty (ns_, gx, tv, fv) =
    let vd = -1
    let scone gg = function
        | B_reveng_l(ty, its) -> map (fun (g, v) -> (ix_and gg g, v)) its
        | other -> [(gg, other)]

    let i1 = scone gx tv
    let i2 = scone (xi_not gx) fv
    let rec conjoin = function     // collate on identical values
        | [] -> []
        | (g,v)::tt ->
            let fw (a,b) =
                if vd>=4 then vprintln 4 ("Doing l_eq of " + bsv_expToStr b + " and " + bsv_expToStr v)
                let r1 = gen_B_deqd ww bobj (b, v)
                let rr = bsv_const_bexp ww mf r1 && bsv_bmanifest ww bobj mf r1
                if vd>=4 then vprintln 4 (sprintf "Done %A" rr)
                rr 
            let (y,n) = groom2 fw tt
            (ix_orl(g :: (map fst y)), v) :: conjoin n
        
    B_reveng_l(ty, conjoin (i1 @ i2))

and gen_B_reveng_h ww bobj (mf:unit->string) ty (gx, tv, fv) =
    let scone gg = function
        | B_reveng_h(ty, its) -> map (fun (g, v) -> (gen_B_and[gg; g], v)) its
        | other -> [(gg, other)]

    let i1 = scone gx tv
    let i2 = scone (gen_B_not gx) fv
    let rec conjoin = function     // collate on identical values
        | [] -> []
        | (g,v)::tt ->
            let fw (a,b) =
                ////vprintln 0 ("Doing h_eq of " + bsv_expToStr b + " and " + bsv_expToStr v)
                let r1 = gen_B_deqd ww bobj (b, v)
                let r = bsv_const_bexp ww mf r1 && bsv_bmanifest ww bobj mf r1
                r 
            let (y,n) = groom2 fw tt
            (gen_B_or(g :: (map fst y)), v) :: conjoin n
        
    B_reveng_h(ty, conjoin (i1 @ i2))
    // TODO delete trivial reveng of one item.

and tyderef mf arg = 
    match tyderef_o mf arg with
        | None -> cleanexit(mf() + sprintf ": tyderef other form: (not a collection or pointer type) %s" (bsv_tnToStr arg))
        | Some x->x
// Merge the muts from a pair of environments.
//    
and ee_mutmux ww bobj g (e_tt:ee_t) (e_ff:ee_t) =
    let mf() = "ee_mutmux"
    let non_strict = true // From a control-flow statement (IF/CASE/etc) hence non-strict.
    let copyback k fv =
        match e_tt.muts.lookup k with
            | None -> e_tt.muts.add k fv // Dont bother guarding it
            | Some (ty, tv) -> e_tt.muts.add k (ty, gen_B_reveng_h ww bobj mf ty (g, tv, (snd fv))) // Reveng type field contains content type.
    for z in e_ff.muts do ignore(copyback z.Key z.Value) done     
    e_tt

and ifc_bootstrap ww bobj ee mm prec arg =
    let ww = WN "bootstrap" ww
    let rec groundboot arg =
        match arg with
           | Bsv_ty_integer // TODO use is_grounded instead.
           | Bsv_ty_format
           | Bsv_ty_primbits _
           | Bsv_ty_intconst _
           | Bsv_ty_intexpi _  -> arg

           | Bsv_ty_knot(_, k2) -> arg // if got this far then ok on this branch - following the knot would loop for ever.

//           | Bsv_ty_nom(lr, _, tys) when is_grounded ww arg -> arg // this throws rather than returns false!
           | Bsv_ty_nom(lr, _, tys) when not(disjunctionate (maid_plaster ww) tys) -> arg // if last arg is empty or all of the plaster type then grounded

           | other -> sf (sprintf "grounded_ty other %A" other)
    groundboot (collect_sum1 ww bobj ee mm prec (VP_value(false, [])) (*TODO not all top args need be values? *) arg)


and top_boot ww bobj ee mm (formal_pram) =
    //let prec = Some P_defn1
    //let ifc_ty = ifc_bootstrap ww bobj ee mm prec arg
    let boot_xa2 (so, ast_ty, fid) =
        let (ty, _) = collect_sum2f ww bobj ee mm (so, ast_ty, fid)
        //let it = B_var(dbi_, [fid], g_builtin_lp)
        (so, ty, fid)

    let a2s (so, ast_ty, fid) = fid
    //let rep() = sprintf "[#arg_pairs=(%i+%i=%i) TOP-LEVEL ifc_ty=%s]" n_drops (n_total-n_drops) n_total  (bsv_tnToStr ifc_ty) + sprintf " prampairs=%s argpairs=%s " (sfold a2s formal_prams) (sfold a2s formal_args)
    //vprintln 0 (rep())
    boot_xa2 formal_pram
    // not really correct if there are prams to top level! ok for just now TODO


// deabs4_exp changes an abstract expression and its types to a concrete one by supplying concrete types for the unbound type variables and other parameters. Do we also supply values (instance names) for imported interfaces or just their concrete types?
// deabs4_exp has one call site. It is called from instantiate module as the main work item, but in principle can operate on any expression.
// It takes a binding, uses deepbind to create an env and then hydrates the relevant type droppings for the expression argument evaluated at the given callstring.
//
// Detailed steps:       
//   1. bind args to formals using deepbind or similar to create a composite type/value environment and matching droppings.
//   2. -
//   3. norm the body items within that composite environment.
//   4. optionally write a report.
//
// deabs4_exp currently has one call site: it is called from instantiate module as the main work item, but in principle can operate on any expression.
// In its current use it converts a maker_abs into a maker_l whose statements will be passed by the caller in turn to bsv_compile_Stmt.
//
and deabs4_exp ww bobj vd uid ee iname msg exp binding_info = //
    let vd = bobj.sum1_loglevel
    let ids = hptos iname
    let ww = WF 3 "deabs4_exp" ww ("start iname=" + ids)
    if vd>=5 then vprintln 5 (sprintf "Deabs4 of %s" (bsv_expToStr exp))
    let dety = function
        | EE_e(ty, vale) -> ty
        | EE_typef(_, ty) -> ty
        //| other -> sf (sprintf "dety other lee form %A" other)

    let ans =
        match exp with
        | B_maker_abs(idl, ats, (rootmarked, formal_prams, formal_args, formal_iftype, aliases), provisos, (maker_sty, maker_body), ee_inner, backdoors, lp) ->
            // Leaf tech/BVI components and separate compilation units are hardstate.
            let hardstate =
                match g_hardstate_notes.lookup idl with
                    | Some _ -> true
                    | _      -> false
            vprintln 3 (sprintf "Consulting whether module %s is hardcoded inside compiler (hardstate) ans is %A." (hptos idl) hardstate)

            let mf() = msg  + ": deabs4_exp B_maker_abs:" + htosp idl
            let ww = WF 3 "deabs4" ww (mf())
            let new_call_path = CPS(PS_prefix "L6710", map (fun x->(x, 0, None)) iname)
            let ee_inner = { ee_inner with callpath=new_call_path; }
            let anon_ifc_iname = funique "P4LUGH"
            let n_drops = length formal_prams
            let fids = formal_prams @ formal_args
            let n_total = length fids
            
            let iactualToStr((so, fty, fid), lee) = sprintf "fid=%s:%s///%s" fid (bsv_tnToStr fty) (bsv_tnToStr (dety lee))
            let prams3 = // bind fids (formal identifiers) with iactuals
                match binding_info with 
                | (ifc_ty, outer_cp, iactuals) -> 
                    let zig ((so, fty, fid), lee) = ((so, dety lee, fid), lee)
                    arity_check0 msg "" fids iactuals
                    let arg_pairs = map zig (List.zip fids iactuals)
                    let rep() = sprintf "[#arg_pairs=(%i+%i=%i) outer_cp=%s ifc_ty=%s]" n_drops (n_total-n_drops) n_total (cpToStr outer_cp) (bsv_tnToStr ifc_ty) + sprintf "argpairs=" + (sfold iactualToStr arg_pairs) 
                    //vprintln 0 (mf() + rep())
                    let (prams, args) = list_split n_drops arg_pairs
                    ((prams, args, ifc_ty))
            if vd>=4 then vprintln 4 (sprintf "deabs4: iname=%s, kind=%s: deabstract expression type OK OK OK formals=%A\n(nf=%i cf " ids (htosp idl) formal_args (length formal_args))
            let (p3p, p3a, p3i) = prams3 
            let plugh = ((SO_none anon_ifc_iname, p3i, anon_ifc_iname), EE_typef(G_none, p3i))
            let args_all = plugh::p3p@p3a // We catenate the exported interface, the parameter bindings and the imported interfaces to form args_all.

            let chooser = function // We can integrate this with type_meet soon but keep standalone to help debug for now.
                | ((a, b, fid), EE_typef(G_none, sty)) ->
                    let (_, sty') = sty_choose ww bobj mf (sty, b)
                    ((a, b, fid), EE_typef(G_none, sty'))

                | ((a, b, fid), EE_e(sty, vale)) ->
                    let (dec_, sty') = sty_choose ww bobj mf (sty, b)
                    ((a, b, fid), EE_e(sty', vale))
                | ((_, _, fid), otherlee) -> sf (sprintf "chooser other lee")

// TODO instead of all this choosing we could bind a met value here instead of sty?
//          let args_all = map chooser args_all
            //let evty_m ww aa = ty_eval_generic ww bobj ee msg (None (*Do we want a hindley here?*)) aa // same as evty_f
            let evty_i = evty_create_indirection ee_inner.genenv
            let hoftokens  = [ "TODO-replacethismarker-L6740" ]
            let ee_inner =
                let rec deepbind_arg_ty (em:genenv_map_t) = function // This variant invokes grave_deepbind on each argument from args_all.
                    | ((a, b, fid), EE_typef(G_none, sty)) ->  //Used for the exported interface.
                        if vd>=4 then vprintln 4 (sprintf "p3p bind: binding fid '%s' with aty=%A fty=%A" fid (bsv_tnToStr sty) (bsv_tnToStr b))
                        grave_deepbind_arg_ty_list ww vd mf uid aliases em (*hoftokens, evty_i*) [((a, b, fid), sty)] []

                    | ((a, b, fid), EE_e(sty, vale)) ->  // Used for the imported interfaces and parameters
                        //let b' = sty_choose ww bobj mm (b, sty)
                        let mm = grave_deepbind_arg_ty_list ww vd mf uid aliases em (*hoftokens, evty_i*) [((a, b, fid), sty)] []

                        // Done by grave but we redo with a stronger EE_e bind that includes the value: could be better done via the trip list instead of the pair list?
                        let mm =
                            let hof = get_hof "deabs4-L8410" fid sty
                            em.Add([fid], GE_binding([fid], {g_blank_bindinfo with whom="genty1a"; hof_route=hof }, EE_e(sty, vale))) // TODO check plastered grounded - must be if has a value? We dont have abstract values do we!
                        if vd>=4 then vprintln 4 (sprintf "p3p bind: (genty1a) binding fid '%s' as %s: recurse with aty=%s fty=%s" fid (bsv_expToStr vale) (bsv_tnToStr sty) (bsv_tnToStr b)) // because entered also as a typef?
                        mm
                    | ((_, _, fid), otherlee) -> sf (sprintf "deepbind_arg_type fid='%s' non-EE_sum other ty/vale=%A" fid (otherlee))

                let genenv_inner = List.fold deepbind_arg_ty ee_inner.genenv args_all
                { ee_inner with prefix=iname; genenv=genenv_inner; static_path=idl; module_path=idl; } 

            // We now further augment ee with the maker type vars. This is called the sillate stage.
            let ee_inner =
                match p3i with
                    | Bsv_ty_nom(i_kind, F_ifc_1 protos, _) ->
                         match maker_sty with
                             | Bsv_ty_nom(_, F_fun(rt, fats, maker_arg_tys),  _) -> 
                                 if vd>4 then vprintln 4 (msg + sprintf ": SILLATE START :::: " + sfold bsv_tnToStr maker_arg_tys)
                                 let plugh_sillate = ((SO_none anon_ifc_iname, p3i, anon_ifc_iname), EE_typef(G_none, p3i))  // same as plugh?
                                 let sillater (em:genenv_map_t) (ty, ((so, f_ty, fid), lee)) =  // sillater binds the maker type vars as opposed to the iactual values already bound to formals for rehydrating the body gamma.
                                     if vd>=4 then vprintln 4 (sprintf "sillate: binding \n   1.ty=%s \n with fid=%s\n  2.f_ty=%s \n   3.lee=%s" (bsv_tnToStr ty) fid (bsv_tnToStr f_ty) (eeToStr lee))
                                     match ty with
                                         | Bsv_ty_id(a, b, (ty_fid, _)) -> // TODO again the topf code to avoid dety could be used in this clause? Indeed looks wrong without it
                                             //vprintln 0 (sprintf "Not doing (wierd) sillate id bind %s with lee" ty_fid) // TODO check whether has a sensible binding alreday and dont do if so - this step tends to supply a non-imperial binding.
                                             em
                                             // recent: grave_deepbind_arg_ty_list ww vd msg aliases mm [((a, b, ty_fid), dety lee)] []
                                             // older:deepbind instead of em.Add([ty_fid], GE_binding([ty_fid], "sillate", lee)) // may want to dety and rety as tyonly here.
                                         | Bsv_ty_nom(_, F_fun(Bsv_ty_nom(_, F_fun _, _),  _, args_), _) -> sf "sillate double func - applied to wrong apply (sic)"
                                         | Bsv_ty_nom(_, F_fun(monad_rt_, _, [thearg]), _) ->                                                
                                             if vd>=4 then vprintln 4 (sprintf "sillate nom fun ifc form thearg=%A" (bsv_tnToStr thearg))
                                             let rec local_deepbind topf (em:genenv_map_t) = function
                                                 | (Bsv_ty_id(a, b, (fid, _)), ty) ->
                                                     let ans = (if topf then lee else EE_typef(G_none, ty)) // Need EE_e form from lee at toplevel for numeric types and parameters so dip up to the lee for the top bind.
                                                     //vprintln 0 (mf() + sprintf ": sillate: topf=%A binding fid '%s' to %s (lee=%s)" topf fid (bsv_tnToStr ty) (eeToStr ans))
                                                     em.Add([fid], GE_binding([fid], {g_blank_bindinfo with whom="sillate_local_deepbind"}, ans))

                                                 | (Bsv_ty_nom(f_idl, F_ifc_1 f_items_, f_vxl),   Bsv_ty_nom(a_idl, F_ifc_1 a_items_, a_vxl)) ->  
                                                       let splig = function
                                                           | Tty (ty, _) -> ty
                                                           | Tid (_id, tyfid) -> sf ( "bsvc.fs: ifc unbound formal splig2 " + tyfid)
                                                           //| other -> sf "bsvc.fs: ifc unbound formal splig"
                                                       List.fold (local_deepbind false) em (List.zip (map splig f_vxl) (map splig a_vxl))

                                                 | (Bsv_ty_nom(f_idl, _, f_vxl),   Bsv_ty_nom(a_idl, _, a_vxl)) ->  
                                                       let splig = function
                                                           | Tty (ty, _) -> ty
                                                           | other -> sf "splig"
                                                       List.fold (local_deepbind false) em (List.zip (map splig f_vxl) (map splig a_vxl))
                                                     
                                                 | (Bsv_ty_intconst _, _)                                             | (Bsv_ty_intexpi _, _)
                                                 | (Bsv_ty_integer, _)                                                | (Bsv_ty_dontcare _, _)
                                                 | (Bsv_ty_format, _)                                                 | (Bsv_ty_primbits _, _) -> em // manifestly grounded forms

                                                 | (Bsv_ty_int_vp vp, y) ->
                                                     //vprintln 0 (sprintf "Imperial ignored deepind int_vp op %s bound with %s " (vfToStr true vp) (bsv_tnToStr y))
                                                     em
                                                 
                                                 | (Bsv_ty_stars(mm, ty), d) ->
                                                     //vprintln 0 (sprintf "+++ sillate (wrongly) ignored starbind for '%s' of %s for %s" fid (bsv_tnToStr ty) (bsv_tnToStr d))
                                                     em
                                                       
                                                 | (x, y) -> cleanexit(msg + sprintf "local deepbind unexpected type used\n  x=%A\n  y=%A" (bsv_tnToStr x) y) 

                                             // Using grave does not work... please clarify why we need a local_deepbind please.
                                             //grave_deepbind_arg_ty_list ww vd msg aliases em [((SO_none "$plover", thearg, "$plover"), dety lee)] []
                                             //local_deepbind true mm (sty_choose ww bobj msg (thearg, dety lee), dety lee) // TODO dont need this choose call
                                             local_deepbind true em (thearg, dety lee) // TODO dont need this choose call
                                             
                                         | other ->
                                             //vprintln 0 (msg + sprintf ": sillate other form (so=%A) %A" so (bsv_tnToStr ty))
                                             em

                                 let ee_inner1 =
                                     if false then ee_inner // OLD: do not sillate at top level
                                     //lse { ee_inner with genenv=List.fold sillater ee_inner.genenv (List.zip (rt::maker_arg_tys) (plugh_sillate::p3p@p3a)) }// need to silly bind against other formal names.
                                     else { ee_inner with genenv=List.fold sillater ee_inner.genenv (List.zip (rt::maker_arg_tys) args_all) }// need to silly bind against other formal names.

                                // Here we are resolving a final concrete type annotation. Two approaches work: make concrete droppings or form and return type environment (cc).
                                 let tyenv_final_ =
                                     let hydrate_gamma_nonabs (id, cp, gm) =  // Only to be done once per synthesis root? Or put another way, when stored as a body anno not to be stored as well!
                                         let tok = funique "bgh"
                                         let cp' = join_cp bobj "L6572hydrate-nonabs" new_call_path cp
                                         if vd>=4 then vprintln 4 (sprintf "hydrate body_gamma %s start (grounded=%A) %s %s + %s :\n    pre=%s" tok (groundedp new_call_path) id (cpToStr cp) (cpToStr cp') (droppingToStr gm))
                                         let bg_hydrate arg =
                                             if vd>=6 then vprintln 6 (sprintf "   bg_hydrate body_gamma nonabs start ty_eval %s %s -> ...\n" id (bsv_tnToStr arg))
                                             let ans = ty_eval ww bobj ee_inner1 mf arg
                                             if false then
                                                 let bb = (bsv_tnToStr arg) 
                                                 let aa = (bsv_tnToStr ans)
                                                 if aa <> bb then vprintln 0 (sprintf "   bg_hydrate body_gamma nonabs ty_eval %s %s -> %s" id bb (if bb=aa then "SAME" else aa))
                                             ans 
                                         let gm' = dropapp bg_hydrate gm  // nonabs: all the bindings are in the env - legit call
                                         if vd>=4 then vprintln 4 (sprintf "hydrate body_gamma %s post (aka dropping) %s: \n   post=%s" tok id (droppingToStr gm'))
                                         (id, cp', gm')

                                     let lowered_drops = map hydrate_gamma_nonabs fats.body_gamma
                                     let dropit (tbcc, h0) (id, xcp, dropping) = store_type_annotation (ee, bobj) ww ee.genenv "L6584" id mf tbcc h0 xcp dropping
                                     let tyenv00 = ({ g_null_tb with tve=lowered_drops(*i think - but not used*) }, [])  // We dont need since used ty_eval to achieve the same - prefer to optionally not provide this
                                     List.fold dropit tyenv00 lowered_drops
                                 vprintln 3 (sprintf "Completed hydrate body_gamma for " + fats.methodname)// pre=%s post=%s fats.body_gamma lowered_drops)
                                 cassert(groundedp new_call_path, "cc.tve being discarded when not grounded")                                    
                                 // We have made the concrete droppings as side effects so need not return tyenv_final_
                                 ee_inner1
                             | other ->
                                 vprintln 2 (sprintf "+++ Strangely skip hydrate body_gamma for %A" other) // hydrate
                                 ee_inner
                         
                    | Bsv_ty_dontcare _ ->
                        vprintln 0 (msg + sprintf ": Failed to get ifc_sty from dontcare") // temporary backstop
                        ee_inner
                    | other -> sf (msg + sprintf ": Failed to get ifc_sty from %A" other)
            let prams3c = (map fst p3p, map fst p3a, p3i, hardstate)
            let b2 = rev(snd(norm_body_items ww bobj (Some p3i) (ee_inner, []) maker_body)) //call site in deabs4
            if bobj.wverbosef then reportx 0 "deabs: Normed module" bsv_stmtToStr b2
            B_maker_l(idl, ats, prams3c, b2, provisos, backdoors, lp)
                                                                          
        | other -> sf (sprintf "deabs4 exp: other %A " other)
    let ww = WF 3 "deabs4_exp" ww ("finished iname=" + ids)
    ans // end of deabs4_exp

// ideally call this only from KB_typeId clause of norm_bsv_type, but also used without prams for tags.
and lookup_type ww bobj (ee:ee_t) (mf:unit->string) so (site, typen:string, nf, pramso, lp) =
    let vd = bobj.afreshen_loglevel
    // We have doubled up on so and numberf here!
    let ww = if vd>=4 then WF 4 "lookup_type" ww (typen + sprintf " numberf=%A site=%s name=%s" so site typen) else ww
    let legacy_tc typen pramso = lookup_type ww bobj ee mf so (site, typen, nf, pramso, lp)
    match typen with
    | "int"        -> g_canned_int_ty
    | "bit"        -> g_canned_bit_ty
    | "module"     -> g_maker_sty_ty
    | "$unit_type" -> g_unit_ty 
    | typen ->
    let prams = valOf_or_nil pramso


    let rec type_dege aa =
        //vprintln 0 (sprintf " type_dege %A " aa)
        match aa with
        | GE_formal(SO_none _, id, ty) -> cleanexit("Unbound (type) formal " + id)

        | GE_formal(SO_parameter, id, ty) -> Bsv_ty_intexpi(B_hexp(Bsv_ty_integer, gen_pram_net ww LOCAL ty id)) // Really a local?

        | GE_normthunk(instf, idl, ans, reaped, _, ff) when not(nonep !ans) -> type_dege(valOf !ans)
        | GE_normthunk(instf, idl, ans, reaped, being_done, ff) when not(nonep !being_done) ->
            vprintln 0 ("want to make a recursion marker 3/3 for " + htosp idl)
            (valOf !being_done)
        | GE_normthunk(instf, idl, ans, reaped, being_done, ff) -> // idiomatic kate code - nth copy!
            if not (nonep !being_done) then sf (mf() + " recursion being_done L5860")           
            let knot = ref None
            being_done := Some(Bsv_ty_knot(idl, knot))
            let env_updates = ff ww ee // { ee with genenv=ee.genenv.Remover (hd idl); } // [ hd idl ]
            being_done := None
            reaped := env_updates
            let kate = function
                | Aug_env(tyclass, bound_name, (dmtof, who, vale))->  ans := Some(GE_binding(bound_name, who, vale))
                | Aug_tags newkeys when nullp newkeys -> ()
                | Aug_tags newkeys ->                     
                    let keys_augment (c:ctor_tags_t) (s, (st, n)) =
                        match c.TryFind s with
                            | None -> c.Add(s, [(st, n)])
                            | Some lst -> c.Add(s, (st, n)::lst)
                    vprintln 0 (sprintf "+++++++++tags ignored in %i %A" (length env_updates) newkeys)
                    let _ = muddy "doit - never used?"
                    // in { ee with ctor_tags=List.fold keys_augment ee.ctor_tags newkeys; }
                    () // TODO update env_updates?

                | tt -> vprintln 0 (sprintf "katey other '%s' %A" (htosp idl) tt)
            app kate env_updates
            type_dege (valOf !ans)

//      | GE_type_sty_knot(idl, Some(GE_normthunk(instf, idl, ans, reaped, _, ff)) -> muddy "wiggle"            

        | GE_binding(bound_name, whom, EE_typef(_, sty)) -> repaint whom sty // who calls freshen on this ?

        | GE_binding(bound_name, whom, EE_e(sty, vale)) -> // numeric form.
            //vprintln 0 (sprintf "gen_lookup %s so=%A-> %A" (htosp bound_name) so aa) 
            match bsv_eval_pram bobj ww ee.genenv vale with
                | EE_e(sty, vale) ->
                    vprintln 0 (sprintf "lookup type numeric form %s, %s bound as %s " (htosp bound_name) whom.whom (bsv_expToStr vale)) 
                    match so with // repaint_anno
                        | SO_numeric ->  Bsv_ty_intexpi vale
                        | _ -> repaint whom sty

        | other -> sf(sprintf "Other form in type_dege so=%A lookup_type %A  GA=%A" so other prams)

    // This repaint and imperial code has rather strange names because I am not quite sure what it should do!
    // BSV has a concept type arithmetic and types embodying integers.  This is supposed to handle these but it is not quite sound yet.
    and repaint whom sty =
        match (so, nf) with // repaint_anno
            | (_, VP_value(true, ftags))
            | (SO_numeric, VP_value(_, ftags)) when not(nullp ftags) && hd ftags <> "" ->
                //vprintln 0 (sprintf "nf=%A repaint_anno needed on " nf + whom.whom + " " + typen)
                match sty with
                    | Bsv_ty_intexpi _ -> sty
                    //o  UInt#(16) on ex_05_g.
                    | Bsv_ty_integer -> Bsv_ty_int_vp (VP_value(true, ftags))
                    | sty ->
                        vprintln 3 (sprintf "+++ Could not repaint numeric %s with ftags='%s' " typen (htosp ftags))
                        sty
                             
            | _ -> sty
       
    let rec dego1_tagsearch s lp =
        match ee.ctor_tags.TryFind s with
            | Some(((Bsv_ty_nom(ats, F_enum(miu, mapping), _) as ty), 0)::tt) ->
                    if not(nullp tt) then cleanexit("enum tag occurs more than once but cannot (currently) be disambiguated : " + s)
                    else
                        let nm = htosp ats.nomid
                        let r = valOf(op_assoc s mapping) //TODO disambiguate or flag
                        // This is a totally silly interface for closing the enum group!
                        let grpname = vectornet_w(nm + "_ename_root", 32)
                        let enum p =
                            let r = enum_add "EN" grpname "EN" p
                            let _ = xi_deqd(grpname, r)
                            r
                        //close_enum "ENUM3633" grpname
                        let ans =
                            match so with
                            | SO_numeric -> Bsv_ty_intexpi(B_hexp(ty, enum r))
                            | _ -> ty
                        ans
            | Some x ->
                let rec consult c ty =
                    match ty with 
                        | Bsv_ty_knot(idl, k2) when not(nonep !k2) -> consult c ty
                        | Bsv_ty_nom(idl, F_tunion(mrecflag, variants, _), _) -> // Found by tag
                            let pred = function
                                (tag, ty_) -> tag=s
                            match List.filter pred variants with
                            | [] -> c
                            | [item] ->(ty, item, variants)::c // Don't need an alpha convert since was presumably grounded.
                            | _ -> sf("two tags with same name " + s) // unlikely error at this point
                        | other -> sf (sprintf "other tag matcher %A" other)

                match List.fold consult [] (map fst x) with // discard arity for now but it would help disambiguate?
                   | [] -> sf ("internal error: tag not found tag=" + s)
                   | [(ty, (id, tyo), variants)] ->
                       match so with
                        | SO_numeric -> // applies for a tunion but surely not a struct - if details present then already numbered.
                            // Ideally here also do the enum call for a strobe group
                            let rec finder n = function
                                | [] -> sf "tag not found"
                                | (tag, ty)::tt when tag=s -> n
                                | _::tt -> finder (n+1) tt
                            mk_intansi(xi_num(finder 0 variants)) // count which
                        | _ -> ty
                   | a::b::_ -> cleanexit("union tag occurs more than once but cannot (currently) be disambiguated : " + s)
            | None -> dego2 lp s

    and dego2 lp = function
        | "int"        -> g_canned_int_ty
        | "bit"        -> g_canned_bit_ty
        | "module"     -> g_maker_sty_ty
        | "$unit_type" -> g_unit_ty 
        | typen -> 
            match ee.genenv.TryFind [typen] with // this time regardless of case!
            | None ->
                let aa =
                    if nullp prams then Bsv_ty_id(toVP so typen, Some P_free, (typen,  (*fresh_bindindex*) "-11"))
                    else
                        let x pp = (SO_none "xyz", Some pp, "$$fid") // want unreverted prams please
                        muddy (sprintf " really undefined type dego2: Bsv_sum_type_id(false, typen, map x prams, lp) %A" typen)
                let vd = 0
                vprint vd "In-scope identifiers are (L3024): "
                for z in ee.genenv do ignore(vprint vd (htosp z.Key + " ")) done 
                vprintln vd ""
                vprint vd "In-scope tags are: "
                for z in ee.ctor_tags do ignore(vprint vd (z.Key + " ")) done 
                // this is a straightforward alpha rename and there will be a post-hoc unification we hope
                vprintln 0 (mf() + " dego2 type unbound " + typen)
                aa
                
            | Some ge ->
                //vprintln 0 (sprintf "dege for: '%s' looked up -> %A" typen ge) 
                let ans = type_dege ge
                //vprintln 3 (sprintf "dege post: %A looked up %s -> %s" so typen (bsv_tnToStr ans))  // WHO CALLS FRESHEN ON THIS
                ans
    match None with //(!g_predef_types).TryFind [typen] with
        | Some x ->
            let prams2 = map // (fun x->Fresher_ty x) prams
            hydrate_type_builtins ww mf typen prams x

        | None -> dego1_tagsearch typen lp



// All fields to be given fresh names based on utag.  REALLY todo
// This can be more cleanly built off the sty now instead of the ast!
and norm_stunion_type ww bobj ee genbinds_ recf ((key, items, idl) as arg) sty =
    let msg = key + sprintf " '%s' <%s>" (htosp idl) (sfold (gaToStr) genbinds_) // only for debug
    let ww = WF 3 "norm_stunion_type " ww msg
    muddy "REALLY-norm_stunion_type"


and check_isstring ww ee mf = function
    | (ot, other) ->
        cleanexit(mf() + " String expected: not " + bsv_expToStr other + " of type " + bsv_tnToStr ot)
    
// Tree walk for generic formals as part of a pre-norm. 
// augments ee with abstract type formals for a prototype. does types only. 
// 
and tbindxx_new ww bobj (mf:unit->string) ee f1 = 
    let chk_rebind ftid tyl c =
        match op_assoc ftid c with
        | None -> ()
        | Some ov ->
            vprintln 0 (sprintf "Perhaps rebind of '%s' since in there already (o=%s prams=%A)" ftid (bsv_tnToStr ov) (sfold astToStr tyl))
            () 

    let tbindzz cc ftid n so arg =
        if type_is_def ee true arg || not_nonep(op_assoc ftid cc) then cc
        else
            (ftid, Bsv_ty_id(toVP so ftid, None, (ftid, (*fresh_bindindex*) "-12")))::cc

    let rec tbindxyo cc = function
        | (n, so, Some arg) -> tbindxy cc (n, so, arg)
        | (n, so, None) -> cc

    and tbindxy cc (n, so, arg) =        
        match arg with    
        | KB_typeNat s -> cc

        | KB_intLiteral(fwo, (dontcares, b, s), lp) -> cc

        | KB_type_dub(_, ty_fid, tyformals, lp) ->
            let cc = tbindzz cc ty_fid n so arg
            //vprintln 0 (mf() + sprintf " tbind identifier: '%s' |<_>|=%i" ty_fid (length tyformals))
            let tyformals = map (kludge_formal_name_to_type2 mf) tyformals
            List.fold (tbindxyo) cc (List.zip3 [1..length tyformals] (map f1o3 tyformals) (map f2o3 tyformals))


        | KB_functionProto(return_type1, name1, formals1, provisos1, lp1) ->
            let cc = tbindxy cc (0, SO_none "$rt", return_type1)
            //vprintln 0 (mf() + sprintf " tbind functionProto: Return type done %A %i" return_type1 (length c'))
            List.fold (tbindxyo) cc (List.zip3 [1..length formals1] (map f1o3 formals1) (map (kludge_formal_name_to_type_o mf) formals1))
                // Want to do return type after args, but fold will reverse so do first here.
            
        //| KB_typeFormal(SO_numeric as so, ftid)
        //| KB_typeFormal(SO_formal as so, ftid)
        | Let (SO_none "$uq_idl") (so, KB_id(ftid, _))   ->
            chk_rebind ftid [] cc
            tbindzz cc ftid 1234 so arg
        | other -> sf (mf() + sprintf "functionProto tbindxx other %A" other)
    let tbinds = List.fold (tbindxyo) [] f1
    //vprintln 0 (mf() + sprintf ": %i tbinds collected" (length tbinds))
    let augment_ety (em:genenv_map_t) = function
        | (id, ty) ->
            //vprintln 0 (mf() + sprintf " fp tbindxx %s as %s" (gfaToStr(card, id)) ( bsv_tnToStr ty))
            em.Add([id], GE_binding([id], {g_blank_bindinfo with whom="tbinxy"}, EE_typef(G_none, ty)))
    let ee = { ee with genenv=List.fold augment_ety ee.genenv tbinds }
    (ee, tbinds)


// Deabstract and abstract function that infact has no typevars.    1/2 
and perhaps_norm_functionProto_1 ww bobj ee prec sty proto =
    match proto with
    | KB_functionProto(ast_return_type, name, formals, provisos, lp) ->
        let mf() = lpToStr0 lp + ": functionProto_1+" + name
        let ww = WN (mf()) ww
        let (ee, tbinds_) = tbindxx_new ww bobj mf ee [(0, SO_none "L8521", Some proto)]
        vprintln 3 (mf() + " " + i2s(length tbinds_) + " generic count (>0 means will stay abstract for now")
        //let sty = Bsv_ty_sum_fun(collect_sum1 ww bobj ee mf prec (VP_value _) ast+return_type, {g_blank_fid with who="perhaps_norm_functionProto"; fids=map get_fidder formals; methodname=name; fprovisos=provisos'}, map (collect_sum2x ww bobj ee mf dprec) formals)
        ee

    | other -> sf (sprintf "Not a function proto %A" other)

// an aux 2nd-rate collect type... please tidy and use the main code.
and perhaps_norm_functionProto_2 ww bobj ee prec proto =
    match proto with
    | KB_functionProto(ast_return_type, name, formals, provisos, lp) ->
        let mf() = lpToStr0 lp + ": functionProto_2+" + name
        let ww = WN (mf()) ww
        let provisos' = List.fold (proviso_parse ee) [] provisos
        let sty =
            let dprec = Some P_decl0
            let rt = collect_sum1 ww bobj ee mf prec (VP_value(false, [])) ast_return_type
            let arg_tys = map (collect_sum2x ww bobj ee mf dprec) formals
            let idl = name :: ee.static_path
            //dev_println (sprintf "swold: perhaps_norm_functionProto_2  %s" (hptos idl))
            Bsv_ty_nom({g_blank_nom_typ_ats with nomid=idl; cls=Prov_func; }, F_fun(rt, {g_blank_fid with who="perhaps_norm_functionProto"; fids=map get_fidder formals; methodname=name; fprovisos=provisos'}, map f2o3 arg_tys), freshlister ww rt (map f2o3 arg_tys))
        sty

    | other -> sf (sprintf "Not a function proto %A" other)
    
and gen_B_deqd ww bobj (l ,r) =
    let tc = gen_B_deqd ww bobj
    let mf() = "gen_B_deqd" 
    //if vd>=7 vprintln 7 (sprintf "gen_B_deqd commence: l=%s r=%s" (bsv_expToStr l) (bsv_expToStr r))
    //let token = funique "deqd-token"
    //dev_println (sprintf "gen_B_deqd %s: commence: l=%s r=%s" token (bsv_expToStr l) (bsv_expToStr r))    
    let ans =
        match (l, r) with // if we have private equals here types are allowed etc in actions so remove ref on ret val TODO.
        | (B_action(_, bev_l, _, _), B_action(_, bev_r, _, _)) ->
            //vprintln 3 (sprintf "gen_B_deqd compare actions: keep symbolic l=%s r=%s" (bsv_expToStr l) (bsv_expToStr r))
            B_bdiop(V_deqd, [l; r])

        | (B_hexp(_, X_undef), B_hexp(_, X_undef)) -> B_and[] // undef will only match itself (and even that is perhaps doubtful).

        | (_, B_hexp(_, X_undef))

        | (B_hexp(_, X_undef), _) -> B_or[]

        | (B_string sl, B_string sr) -> if sl<>sr then B_or[] else B_and[]
        
        | (B_mask(il, el), B_mask(ir, er)) -> if il<>ir then B_or[] else tc (el, er)
        | (B_shift(el, il), B_shift(er, ir)) -> if il<>ir then B_or[] else tc (el, er)

        | (B_aggr(nomidl, TU_enum(vl, _, _)), B_aggr(nomidr, TU_enum(vr, _, _))) -> if (* nomidl=nomidr type equ too expensive && *) vl=vr then B_and[] else B_or[]

        | (B_aggr(noml, TU_tunion(vl, tagl, _, _, _)), B_aggr(nomr, TU_tunion(vr, tagr, _, _, _))) -> if tagl=tagr then tc(vl, vr) else B_or[]

        | B_aggr(noml, TU_struct itemsl), B_aggr(nomr, TU_struct itemsr) ->
            if (*noml=nomr type eq is too expensive on stack! && *) length itemsl = length itemsr then
                let pairs = map (fun (l, (_, _, _, v)) -> (l, v, op_assoc l itemsr)) itemsl
                let comp = function
                    | (tag, lv, rvo) when nonep rvo -> raise Invalid
                    | (tag, lv, Some(_, _, _, rv)) -> tc (lv, rv)
                let ans = try B_and(map comp pairs)
                          with Invalid -> B_or[]
                ans
            else B_or[]

        | (B_pliStmt(sl, argsl, lpl), B_pliStmt(sr, argsr, lpr)) ->
            if sl=sr && lpl=lpr then B_and[] // TODO need a tail call on lists here
            else B_or[]
        | (B_pliStmt(sl, argsl, lpl), _) ->  B_or[]
        | (_, B_pliStmt(sl, argsl, lpl)) ->  B_or[]

        | (B_vector(al_str, _), B_vector(ar_str, _)) -> if al_str=ar_str then B_and[] else B_or[] // Same contents in vectors still leaves them unequal.
        //| (B_bexp xl, xr) -> tc (B_hexp(xi_blift xl), xr)
        //| (xl, B_bexp xr) -> tc (xl, B_hexp(xi_blift xr))        
        | (B_hexp(lt, xl), B_hexp(rt, xr)) -> gen_B_bexp(xi_deqd(xl, xr))
        //| (l, r) -> sf (mf() + ": deqd other " + bsv_expToStr l + " cf " + bsv_expToStr r)

        | (B_blift (B_bexp l), B_blift(B_bexp r)) when bconstantp l && bconstantp r -> 
            match (l,r) with
                | (X_true, X_true)   
                | (X_false, X_false) -> B_and[] // ie true
                | (X_true, X_false) 
                | (X_false, X_true)  -> B_or[]  // ie false
                | (l, r) -> sf (mf() + ": Invalid hbexp constants: " + xbkey l + " cf " + xbkey r)


        // TODO - DONT MAKE THESE!
        | (B_action(_, [Bsv_eascActionStmt(ae, _)], _, _), r) -> tc (ae, r)
        | (l, B_action(_, [Bsv_eascActionStmt(ae, _)], _, _)) -> tc (l, ae)
        | (l, r) ->
            if bsv_const_exp ww mf l && bsv_const_exp ww mf r  // These need to be constants that do not contain strings.?
            then
                try
                   let l1 = bsv_manifest ww bobj mf l
                   let r1 = bsv_manifest ww bobj mf r
                   let ans = l1=r1  // Using the built-in equals may well have false negatives?
                   let report() = 
                       vprintln 0 (sprintf "++++Using const deqd of l=%s  cf r=%s" (bsv_expToStr l) (bsv_expToStr r))
                       vprintln 0 (sprintf "++++Using deqd 2/2 on const deqd of %A cf %A " l1 r1)
                       vprintln 0 (sprintf "++++Using deqd 3/3  ans=%A" ans)
                       ()

                    //if not ans then report() // Keep an eye out for false negatives for a bit.

                   if ans then B_and[] else B_or[]
                with _ ->
                    vprintln 0 (sprintf "gen_B_deqd untidy trace l=%s r=%s" (bsv_expToStr l) (bsv_expToStr r))
                    B_bdiop(V_deqd, [l; r])
            else B_bdiop(V_deqd, [l; r])
    //vprintln 0 (sprintf "gen_B_deqd gen debug l=%s r=%s ans=%A" (bsv_expToStr l) (bsv_expToStr r) ans)
    //dev_println (sprintf "gen_B_deqd %s: result=%s" token (bsv_bexpToStr ans))    
    ans


and bsv_const_exp ww mf arg =
    let aa = bsv_const_exp_ntr ww mf arg
    //vprintln 0 (mf() + ": const_exp ntr result " + boolToStr aa + "  for " + bsv_expToStr arg)
    aa

and bsv_const_exp_ntr ww (mf:unit->string) = function
    | B_field_select("", B_aggr(_, TU_tunion(e, _, _, _,  _)), _, _) -> true // Testing variant of a given variant is constant. Should never be formed.
    
    | B_field_select(tag, B_aggr(_, TU_tunion(e, vtag, _, _,  _)), _, _) ->
        if tag<>vtag then true // Assume mistagged union extract gives a constant default value.
        else bsv_const_exp ww mf e
        
    | B_field_select(tag, B_aggr(_, TU_struct lst), _, _) ->
        let k = op_assoc tag lst
        nonep k || bsv_const_exp ww mf (f4o4(valOf k)) // Assume missing field gives a constant default value.

        // Simple tail call clauses...
    | B_mask(_, e)
    | B_shift(e, _)    
    | B_field_select(_, e, _, _)
    | B_aggr(_, TU_tunion(e, _, _, _,  _))
    | B_subif(_, e, _, _)   
    | B_dyn_b(_, e)       -> bsv_const_exp ww mf e

    | B_pliStmt(s, lst, _) -> conjunctionate (fun (_, v) -> bsv_const_exp ww mf v) lst

    | B_aggr(nomid, TU_struct lst) -> conjunctionate (fun (_, (_, _, _, e)) -> bsv_const_exp ww mf e) lst

    | B_applyf(PI_biltin fn, rt, args, lp) -> // assume all biltin are ref trans
        conjunctionate (fun (t,v)->bsv_const_exp ww mf v) args
        
    | B_applyf(PI_bno(B_dyn_b(s, bb), _), rt, [rv], lp) ->
        //vprintln 0 (mf() + ": const_exp for apply " + s + " is false")
        false
   
    | B_aggr(nomid, TU_enum _) -> true

    | B_abs_lambda  _
    | B_lambda _ -> true

    | B_ifc(pi,_)  ->
        //vprintln 0 (mf() + ": const_exp for interface " + pathToS pi + " is false") // perhaps not always? TODO.
        false

    | B_string x           -> true
    | B_diadic(oo, a, b, _)   -> bsv_const_exp ww mf a && bsv_const_exp ww mf b  // Not always applied already!
    | B_hexp(_, X_undef)  -> true
    | B_hexp(_, x) -> int_constantp x
    | B_blift b -> bsv_const_bexp ww mf b
    // referentially transparent functions could be included but we hope they are applied already

    | B_action(_, bev, _, lp) -> bsv_const_bev ww mf bev

    | B_reveng_h(_, lst) -> conjunctionate (fun (g,v) -> bsv_const_bexp ww mf g && bsv_const_exp ww mf v) lst
    | B_reveng_l(_, lst) -> conjunctionate (fun (g,v) -> bconstantp g && bsv_const_exp ww mf v) lst
    | B_query(_, g, tt, ff, _, _) -> bsv_const_bexp ww mf g && bsv_const_exp ww mf tt && bsv_const_exp ww mf ff
    | B_ferref _
    | B_var _ -> false

    | B_format(_, args)
    | B_applylam(_, _, args, _, _, _) -> conjunctionate (fun (t,v)-> bsv_const_exp ww mf v) args



    | B_applyf(mi, rt, _, _) ->
        match mi with
            | PI_bno(B_dyn_b(_, _), _) -> false
            | other ->
                vprintln 0 (sprintf "Constant conservative B_apply %A" other)
                false // Conservative

    | other ->
        vprintln 0 (sprintf "Constant conservative %s" (bsv_expToStr other))
        false // Conservative

and bsv_const_bexp ww (mf:unit->string) = function
    | B_bexp x -> bconstantp x
    | B_bdiop(oo, lst) -> conjunctionate (bsv_const_exp ww mf) lst
    | B_and lst
    | B_or lst ->  conjunctionate (bsv_const_bexp ww mf) lst
    | B_orred(x) -> bsv_const_exp ww mf x
    | other ->
        vprintln 3 (sprintf "Constant bool conservative %A" (bsv_bexpToStr other))
        false // Conservative

and bsv_const_bev ww (mf:unit->string) = function
    | Bsv_resultis((_, ae), lp)::tt    -> bsv_const_exp ww mf ae 
    | Bsv_skip(_)::tt                  ->  bsv_const_bev ww mf  tt
    | Bsv_eascActionStmt(ae, _)::tt    ->  bsv_const_exp ww mf ae && bsv_const_bev ww mf tt    
    | Bsv_beginEndStmt(lst, _)::tt     -> bsv_const_bev ww mf (lst @ tt)
    | Bsv_ifThenStmt(g, t, fo, strict_, lp)::tt ->
        let g1 = bsv_const_bexp ww mf g
        if g1 then vprintln 0 (mf() + ": +++ Someone made a constant IF construct " + bsv_bexpToStr g)
        g1 && bsv_const_bev ww mf [t] && (nonep fo || bsv_const_bev ww mf [valOf fo]) && bsv_const_bev ww mf tt
    | [] -> true
    | other::tt ->
        //vprintln 3 (sprintf "Constant bev conservative.")
        vprintln 3 (sprintf "Constant bev conservative %s" (bsv_stmtToStr other))
        false // Conservative


and norm_bsv_bexp_rm ww bobj ee arg =
    let vd = bobj.normalise_loglevel
    let env = []
    //dev_println(sprintf "norm_bsv_bexp_rm %A" arg)
    match arg with
    | KB_functionCall(KB_unop "!", [l], (lp, uid)) ->
        let mf() = lpToStr0 lp + ": monadic!"
        let ww' = WN (mf()) ww
        let  l' = norm_bsv_exp_rmode ww' bobj ee l
        B_orred(B_applyf(PI_biltin "!", g_bool_ty, [l'], tag_this_lp g_builtin_lp))
        
    | KB_systemFunction ("True", [],_) -> gen_B_bexp X_true
    | KB_systemFunction ("False", [],_) -> gen_B_bexp X_false

    | KB_functionCall(KB_bop oo, [l; r], (lp, uid)) -> // scalar*scalar->bool
        let mf() = lpToStr0 lp + sprintf "diadic bop '%s'" (f1o4(xbToStr_dop oo))
        let ww'  = WN (mf()) ww
        let  l' = (norm_bsv_exp_rmode ww' bobj ee) l
        let  r' = (norm_bsv_exp_rmode ww' bobj ee) r
        let resolve() = 
            let (ls, rs) = (is_signed (fst l'), is_signed (fst r'))
            //dev_println (sprintf "Determine comparison signedness on %A given ls=%A rs=%A  l=%A and r=%A" oo ls rs (bsv_expToStr (snd l')) (bsv_expToStr (snd r')))
            let resolved =
                match (ls, rs) with
                    | (false, false) -> Unsigned

                    | (true, true)   -> Signed
                    | other ->
                        // C semantics: either operand to be unsigned for result be unsigned. (Verilog requires both signed to be signed which is the same behaviour, despite the default signnedness of expressions being different between the two languages).
                        //hrp_yikes(printf"Perhaps a type error to compare signed against unsigned?  ls=%A rs=%A l=%s r=%s" ls rs (bsv_expToStr (snd l')) (bsv_expToStr (snd r')))
                        // What is the correct semantic here? Since I am unaware of any 'u' suffix being available in BSV I choose C/Verilog-like semantics: ie one arg being signed is sufficient to make a signed comparison, but a more thorough implementation or error report are two other routes.,,
                        Signed
            //dev_println (sprintf "Resolved=%A" resolved)
            resolved

        let oo =
            match oo with
                | V_deqd   
                | V_dned   
                | V_orred  -> oo
                | V_dltd _  -> V_dltd(resolve()) (* Less than:   < *)
                | V_dled _  -> V_dled(resolve()) (* Less than:   < *)
                | V_dgtd _  -> V_dgtd(resolve()) (* Less than:   < *)
                | V_dged _  -> V_dged(resolve()) (* Less than:   < *)
                // Note, later on gt ge le should not arise owing to normal form.
        let v0 = gen_B_bdiop ww bobj oo [l'; r']
        //if false && bsv_const_exp ww mf l' && bsv_const_exp ww mf r'
        //then B_bexp(bsv_blower ww v0)
        v0 

    | KB_functionCall(KB_ab oo, [l; r], (lp, uid)) -> // bool*bool->bool
        let mf() = lpToStr0 lp + ": KB_diadic ab op" + f1o3(xabToStr_dop oo)
        let ww'  = if vd>=4 then WN (mf()) ww else ww
        let l' = (norm_bsv_bexp_rm ww' bobj ee) l
        let r' = (norm_bsv_bexp_rm ww' bobj ee) r
        let v0 = if oo=V_band then gen_B_and([l'; r']) elif oo=V_bor then gen_B_or([l'; r']) else sf ("other abdiop")
        if false && bsv_const_bexp ww mf l' && bsv_const_bexp ww mf r'
        then gen_B_bexp(bsv_blower ww bobj v0)
        else v0 


    | other as arg ->
        let (t_, a') = norm_bsv_exp_rmode ww bobj ee arg
        gen_B_orred(a')

    // other -> sf ("bexp ast other " + sprintf "%A" other)


and arity_check0 mm m1 gf ga =
    let na = length ga
    let nf = length gf
    let ss _ = "please sfold asty3ToStr gf" // TODO - reinstate or use the other arity_check1.
    if na>nf then cleanexit(sprintf "%s: %s: Too many actuals provided for %s (#f=%i cf #a=%i)" mm m1 (ss()) nf na)
    elif na<nf then cleanexit(sprintf "%s: %s: Too few actuals provided for %s (#f=%i cf #a=%i)" mm m1 (ss()) nf na)
    ()
    
and arity_check1 mf m1 gf ga =
    let na = length ga
    let nf = length gf
    let ss _ = sfold bsv_tnToStr gf
    if na>nf then cleanexit(sprintf "%s: %s: Too many actuals provided for %s (#f=%i cf #a=%i)" (mf()) m1 (ss()) nf na)
    elif na<nf then cleanexit(sprintf "%s: %s: Too few actuals provided for %s (#f=%i cf #a=%i)" (mf()) m1 (ss()) nf na)
    //else vprintln 0 (sprintf "%s: %s: Correct number of actuals provided for %s (#f=%i cf #a=%i)" mf m1 (ss()) nf na)
    ()

and arity_check2 mf m1 gf ga =
    let na = length ga
    let nf = length gf
    let ss _ = sfold astToStr gf
    if na>nf then cleanexit(sprintf "%s: %s: Too many generic actuals provided for %s (#f=%i cf #a=%i)" mf m1 (ss()) nf na)
    elif na<nf then cleanexit(sprintf "%s: %s: Too few generic actuals provided for %s (#f=%i cf #a=%i)" mf m1 (ss()) nf na)
    ()


//
// This now works off the sty not the ast
//    
and norm_ifc_decl ww bobj ee lp iidl  genbinds_ ats (sty, items_) = // for use once generics are bound
    let s = htosp iidl
    let qm = lpToStr0 lp + sprintf ": norm_ifc_decl %s " s
    sty

and build_strunion ww bobj mf ee (ty, amembers, pmembers, soloo, vtago) = // Concrete syntax does not allows both a and p forms at once?
    let qm = "build_strunion"
    let ww = WN qm ww
    let rec bld = function
        | Bsv_ty_knot(idl, k) when not(nonep !k) -> bld (valOf !k)
        | Bsv_ty_nom(ats, F_struct(fields, details_), sl) ->                    
            let (ty_, details) = ensure_struct_details ww bobj "build_strunion-top" ee (fields, ats) ty
            match details with
                | Bsv_struct((width, arity, recf), field_details) ->
                    let fields2 = map (fun ((id, sty), (_, (ft, fw, fo))) -> (id, (sty, ft, fw, fo))) (List.zip fields field_details)
                    let assoc_mem = function
                        | (tag, KB_dot bv) -> sf ("binding needed " + bv)
                        | (tag, e) ->
                            match op_assoc tag fields2 with
                                | Some (sty, ft, fw, fo) ->
                                    let (et, ev) = norm_bsv_exp_rmode_tyo ww bobj ee (Some ft) (Prov_None "L6946") e
                                    (tag, (et, fw, fo, ev))
                                | None -> cleanexit(mf() + sprintf ": no such field/tag as  '%A'" tag)
                    let mm = map assoc_mem amembers
                    let positional_mem (tag, e) =
                        let ov = op_assoc tag fields2
                        let (sty, ft, fw, fo) = valOf ov
                        let (et, ev) = norm_bsv_exp_rmode_tyo ww bobj ee (Some ft) (Prov_None "pos-tag-assic") e
                        (tag, (fw, fo, ev))
                    if pmembers<>[] then muddy ("pmembers:: TODO positional struct member code missing")
                    //let mm = map positional_mem pmembers 
                    B_aggr(ty, TU_struct mm)
                //| other -> sf "not a struct"

            // soloo + vtago used in this clause only - tunions
        | Bsv_ty_nom(nomid, F_tunion(mrecflag, variants, details_), _) ->
            let (ty_, details) = ensure_tunion_details ww bobj qm ee (mrecflag, variants, nomid) ty
            match details with
                | Bsv_tunion(variant_details, (width, tag_width, arity, recflag), mapping) ->
                    let vtag = valOf_or_fail (mf() + ": no vtag specified in tunion build " + htosp nomid.nomid) vtago
                    let vo = op_assoc vtag variant_details
                    if nonep vo then sf (sprintf "No such tag '%s' in union '%s'" vtag (htosp nomid.nomid))
                    let (inner_ty, no) = valOf vo
                    //vprintln 0 (sprintf "make tag entry '%s' with top tag %i<<%i" (htosp nomid) no (width-tag_width))
                    let body =
                        match (amembers, soloo) with
                            | ([], None) -> B_hexp(valOf_or inner_ty (Bsv_ty_dontcare "L6289"), xi_num 0) // The inner type is dontcare for a variant with no contents. 
                            | ([], Some x) ->
                                    let (vt, vv) = norm_bsv_exp_rmode_tyo ww bobj ee (inner_ty) (Prov_None "solo bod") x
                                    // let _ = okmeet bobj ww ee (vt, ...)
                                    vv
                            | (amembers, None) ->
                                    //vprintln 0 ("associative amember call of build_strunion")
                                    build_strunion ww bobj mf ee (valOf inner_ty, amembers, [], None, None)
                    B_aggr(ty, TU_tunion(body, vtag, no, width, tag_width))
                //| other -> sf "not a union"
        | ty -> muddy(sprintf "build_strunion: ty=%A v=%A tago=%A" (bsv_tnToStr ty) amembers vtago) 
    bld ty
    
and bsv_valueof ww mm ee ty =
    let useme = bsv_valueof_core mm ty
    let rty = Bsv_ty_integer
    let olda_() =
        match ty with
            // TODO intexpi here.  DELETE olda please. and see mi_valueof
            | Bsv_ty_intconst n ->  let a = B_hexp(Bsv_ty_integer, xi_num n) in (rty, None, a)
            //| Bsv_ty_primbits s when false ->  let a = B_hexp(Bsv_ty_integer, xi_num s.width) in (rty, None, a) // this is a widthof code!
            | _ -> muddy (sprintf " 'valueof' other  %A " ty)
    if true then (rty, None, useme) else olda_()
    
and norm_bsv_exp_lmode ww bobj ee aa = norm_bsv_exp_lmode_tyo ww bobj ee None (Prov_None "exp_lmode") aa

and norm_bsv_exp_rmode_tyo_utzip ww bobj ee tyo asflag pairlist =
    // can do ww' = WN (i2s n) ww
    let utzip (n, aa) = norm_bsv_exp_rmode_tyo ww bobj ee tyo asflag aa
    map utzip pairlist
    
and norm_bsv_exp_rmode_tyo ww bobj ee tyo (asflag:bsv_proviso_t) (aa:bsv_ast_t) =
    //dev_println (sprintf "norm_bsv_exp_rmode_tyo ty=%s" (if nonep tyo then "none" else bsv_tnToStr(valOf tyo)))
    let (r_ty, _, (b:bsv_exp_t)) = norm_bsv_exp_lmode_tyo ww bobj ee tyo asflag aa
    (r_ty, b)
    
and norm_bsv_exp_rmode ww bobj ee arg =
    let (t, a, b) = norm_bsv_exp_lmode_tyo ww bobj ee None (Prov_None "defaultinger")  arg
    (t, b)

and norm_bsv_exp_lmode_tyo ww bobj ee tyo asReg arg =
    let vd = bobj.normalise_loglevel
    if vd>=9 then vprintln 9 (sprintf " TRC: norm_bsv_exp_lmode %A -> ...\n\n" arg)
    let (r_ty, lmode, rmode) = norm_bsv_exp_lmode_tyo_serf ww bobj ee tyo asReg arg
    if vd>=9 then vprintln 9 (sprintf "  norm_lmode_result AST %A -> giving result -> %s\n\n" (arg) (bsv_expToStr rmode))
    (r_ty, lmode, rmode)

and find_sub_if_type ww mf tyo name =
    match tyo with
        | Some (Bsv_ty_nom(iidl, F_ifc_1 ii, _))  ->
            let scanp = function
                | Bsv_ty_nom(ikind, F_subif(iname, _), _)                  ->  iname = name
                | _ -> false
            let subty =
                match List.filter scanp ii.iparts with
                    | [] -> cleanexit(mf() + ": subinterface was not declared in parent interface definition")
                    | a::b::_ -> cleanexit(mf() + ": subinterface exists more than once in parent interface definition: " + htosp iidl.nomid)
                    | [Bsv_ty_nom(_, F_subif(_, ty), _)]                 -> ty
            //vprintln 0 (sprintf "found subuf '%s' ok %s" name (bsv_tnToStr subty))
            subty
        | Some ot -> sf (mf() + " other ifc type " + bsv_tnToStr ot)
        | None -> sf(mf() + ": no parent interface in scope")                    


  
and norm_bsv_exp_lmode_tyo_serf ww bobj ee tyo (asReg:bsv_proviso_t) aa =
    let vd = 1
    let tc = norm_bsv_exp_lmode_tyo ww bobj ee tyo asReg
    //vprintln 0 (sprintf "Norm lmode_tyo_serf %A" aa + (if asReg=None then "" else valOf asReg))
    let qm = if nonep ee.forcef then "norm" else "SOMEFORCE norm" //ee.forcef then "norm-ee" else "norm-el"
    //let _ = whereami_del (qm) (vprintln 0) ww         
    mutinc ee.elab_steps -1
    if !ee.elab_steps <= 0 then sf (sprintf "Too many elaboration steps. Please increase parameter -elab-limit=n (currently %i)" bobj.elab_limit)
    match aa with
    | KB_interfaceExpr(KB_type_dub(_, name, [], lp), items, nameo, _)
    | KB_interfaceExpr(KB_id(name, (lp, _)), items, nameo, _) ->        
        let mf() = lpToStr0 lp + ": norm interfaceExpr+" + name
        let ww = if vd>=4 then WF 4 qm ww (mf()) else ww
        //vprintln 0 (sprintf " wot_? %A" tyo)
        if nameo<>None && valOf nameo <> name then cleanexit(mf() + ": end name does not match start: " + name + " cf " + valOf nameo)
        let sub_if_type = tyo // already mined. find_sub_if_type ww mm tyo name
        // It would be better to keep abstract and use one golden call site.
        let (ee_, items) = norm_body_items ww bobj (tyo) (ee, []) items // call site in norm_exp for interface expressions
        let ans = rev items
        //reportx 0 "ifc: Normed module" bsv_stmtToStr ans
        (valOf_or_fail "need sub_if_type" sub_if_type, None, B_ifc_immed(ans))

    | KB_constantAlias(ss, (lp, _)) ->
        let mf() = lpToStr0 lp + ": norm constantAlias+" + ss
        let ww = if vd>=4 then WF 4 qm ww (mf()) else ww
        match tyo with
            | Some((Bsv_ty_nom(ats, F_enum(_, maps), dx)) as enum) ->
                match op_assoc ss maps with
                    | None -> cleanexit(mf() + sprintf ": Tag '%s' is not a member of enumeration %s" ss (htosp ats.nomid) + "\nCandidates are: " + sfold fst maps)
                    | Some vale ->
                        let a = B_hexp(enum, xi_num vale)
                        (enum, None, a)
            | _ ->
                let so = SO_numeric  // implied SO_numeric needed
                let nf = VP_none "constant-alias-default"
                let q1 = lookup_type ww bobj ee mf so ("KB_constantAlias", ss, nf, None, lp)
                bsv_valueof ww mf ee q1

    | KB_stringLiteral s -> let a = B_string s in (mk_string_ty(strlen s), None, a)

    | KB_functionCall(KB_unop "-", [arg], (lp, uid)) ->
        let mf() = lpToStr0 lp + " monadic negate"
        let ww = if vd>=4 then WF 4 qm ww (mf()) else ww
        let rhs = norm_bsv_exp_rmode ww bobj ee arg
        let lhs = (Bsv_ty_integer, B_hexp(Bsv_ty_integer, xi_num 0))
        let abs_uid = extend_callpath bobj "L7455" uid ee.callpath
        let ty = fst rhs
        (ty, None, gen_B_diadic bobj ww mf V_minus ty (lhs, rhs))

    | KB_block(blockname, lst, nameo, lp) ->
        let mf() = lpToStr0 lp + " expr blockStmt " + valOf_or blockname "anon"
        let ww = if vd>=4 then WF 4 qm ww (mf()) else WN "expr blockStmt" ww
        if nameo<>None && blockname<>None && valOf nameo <> valOf blockname then cleanexit(mf() + ": end name does not match start: " + valOf blockname + " cf " + valOf nameo)
        // TODO block tag not inserted on prefix like for other blocks.
        let rec eblock = function
            | []     -> (Bsv_ty_dontcare "L6397", None, B_hexp(Bsv_ty_dontcare "L6397", X_undef))
            | [item] -> norm_bsv_exp_lmode ww bobj ee item
            | h::tt  -> (ignore(norm_bsv_exp_lmode ww bobj ee h); eblock tt)
        eblock lst

       
    | KB_query(g, l, r, (lp, uid)) ->
        let mf() = lpToStr0 lp + ": KB_query ?-: conditional expression"
        let ww = if vd>=4 then WF 4 qm ww (mf()) else ww
        let  g' = norm_bsv_bexp_rm ww bobj ee g
        let new_call_path = extend_callpath bobj ("L7474") uid ee.callpath
        let prov = Prov_None "query" // TODO - proviso should be passed down
        match retrieve_type_annotation bobj mf "KB_query" new_call_path with
            | Tanno_function(ans, tve_, _, [(_, lt); (_, rt)]) ->
                let (lt, _, l) = norm_bsv_exp_lmode_tyo ww bobj ee (Some lt) prov l // lt and rt should be the same!
                let (rt, _, r) = norm_bsv_exp_lmode_tyo ww bobj ee (Some rt) prov r
                let strict = not bobj.aggressive_conditions
                let a = gen_B_query ww bobj (strict, g', l, r, ans, (lp, new_call_path))
                (ans, None, a)
            | _ -> sf ("unexpected type annotation on '?:' : " + mf())

    | KB_intLiteral(fwo, (dontcares, b, s), lp) ->
        let mf() = lpToStr0 lp + sprintf ": intLiteral base=%i s=" b + s
        let (ty_in, vale, _) = resolve_intLiteral ww mf (KB_intLiteral(fwo, (dontcares, b, s), lp)) 
        let ty =
            match tyo with
                | None    -> ty_in
                | Some rt -> snd(sum_type_meet ww bobj mf [] (ty_in, rt))
        (ty, None, B_hexp(ty, vale))

    | KB_dontcare lp ->
        // We can annotate dontcares with their linepoint by storing them as 'strings with values'
        let ss = if true then X_undef else xi_stringx (XS_withval X_undef) (lpToStr0 lp + "?dontcare")
        (Bsv_ty_dontcare(lpToStr0 lp), None, B_hexp(Bsv_ty_dontcare(lpToStr0 lp), ss))

    | KB_systemFunction ("$time", [], lp) // TODO differences
    | KB_systemFunction ("$stime", [], lp) ->
        let a = B_hexp(Bsv_ty_integer, g_tnownet) in (mk_uint_ty 64, None, a)
#if DDD
    // TODO no longer parsed this way?
    | KB_id("$stop" as xs, (lp, uid))
    | KB_id("$finish" as xs, (lp, uid)) -> tc (KB_functionCall(KB_dk xs, [], (lp, uid)))
#endif

    | KB_tag(path1, (ss as tag), (lp, uid)) -> // norm
        let mf() = lpToStr0 lp +  ": tag+" + ss
        let ww = if vd>=4 then WF 3 qm ww (mf()) else ww
        let subso = None // for now
        let (ty, _, vale) = norm_bsv_exp_lmode_tyo ww bobj ee None (Prov_asReg_asIfc "tag-inner")  path1 // auto apply within tag path _IS_ allowed?  TODO make a note and be consistent.  SmallExamples ex_09_g demonstrates this.
        let path' = Some vale
        
        let (ty2, path2, vale2) =
            let rec retag = function
                | Bsv_ty_nom(kind, F_actionValue ty, _)  -> retag ty
                | Bsv_ty_knot(idl, k2) when not(nonep !k2) -> retag(valOf !k2)

                | Bsv_ty_nom(nomid, F_tunion(mrecflag, variants, details_), _) ->
                    let (ty_ll, details) = ensure_tunion_details ww bobj "site-9261" ee (mrecflag, variants, nomid) ty
                    match details with
                        | Bsv_tunion(variant_details, (width, tag_width, arity, recflag), mapping) ->
                             let vo = op_assoc tag variant_details
                             if nonep vo then cleanexit(mf() + sprintf ": no such tag '%s' in structure '%s'" tag (htosp nomid.nomid))
                             let (ty, no) = valOf vo
                             let (_, rmode) = gen_B_field_select ww bobj [uid] (tag, vale, width-tag_width, 0)                    
                             let lmode = if nonep path' then None else Some(ty_ll, B_field_select(tag, valOf path', 0, width-tag_width))
                             let rmode =
                                 if bsv_const_exp ww mf vale then
                                    let rec xsog = function
                                        | B_action(_, Bsv_eascActionStmt(ae, _)::_, _, lp)
                                        | B_action(_, (Bsv_resultis((_, ae), _))::_, _, lp) -> xsog ae
                                        | B_aggr(nomty, TU_tunion(body, vtag, no, width, tag_width)) ->
                                             if tag=vtag then body // This is the gen_B_field_select code copied out : TODO unify
                                             else
                                                 vprintln 0 (mf() + sprintf " +++ tag mismatch %s cf %s" tag  vtag)
                                                 B_hexp(valOf_or_fail (tag + " tunion tag type needed") ty, xi_num 0)
                                        | other -> sf(mf() + sprintf ": missing TU constant eval code for %s no=%i" (htosp nomid.nomid) no + " tag=" + tag + " other=" + bsv_expToStr other)
                                    xsog vale
                                 else rmode
                             (valOf_or_fail (tag + " rtag type neede") ty, lmode, rmode)

                | Bsv_ty_nom(ats, F_struct(fields, details_), _)  ->
                    let (ty_, details) = ensure_struct_details ww bobj "L9373" ee (fields, ats) ty
                    match details with
                        | Bsv_struct((width, arity, recf), field_details) ->  
                            let vo = op_assoc tag field_details
                            if nonep vo then cleanexit(mf() + sprintf ": no such tag '%s' in structure '%s'" tag (htosp ats.nomid))
                            let (ty, w, pos) = valOf vo
                            if vd>=4 then vprintln 4 (lpToStr0 lp + sprintf ": norm tag access to struct: ty=%s w=%i pos=%i" (bsv_tnToStr ty) (w) (pos))
                            let (ty_, rmode0) = gen_B_field_select ww bobj [uid] (tag, vale, w, pos)
                            let lmode = if nonep path' then None else Some(ty, B_field_select(tag, valOf path', pos, w))
                            let rmode =
                                if bsv_const_exp ww mf vale
                                then
                                    let rec ssog = function // COPY 2/2 TODO delete
                                        | B_action(_, Bsv_eascActionStmt(ae, _)::_, _, lp)
                                        | B_action(_, (Bsv_resultis((_, ae), _))::_, _, lp) -> ssog ae
                                        | B_aggr(nomty, TU_struct lst) ->
                                            let v = op_assoc tag lst
                                            if nonep v then sf(mf() + sprintf ": the tag is missing in this aggregate: %s tag=%s w=%i no=%i items=%A" (bsv_tnToStr nomty) tag w pos lst)
                                            else f4o4(valOf v)
                                        | other -> sf(mf() + sprintf ": missing S constant eval code for %s tag=%s w=%i no=%i other=%A" (htosp ats.nomid) tag w pos other)
                                    ssog vale
                                else rmode0
                            (ty, lmode, rmode)

                    
                | Bsv_ty_nom(_, F_subif((*iname*)_, Bsv_ty_nom(iidl, F_ifc_1 ii, _)) , _) // interface and subinterface needlessly have different approaches to lists of components - they should be the same type anyway
                | Bsv_ty_nom(iidl, F_ifc_1 ii, _) -> 
                    let pred = function
                        | Bsv_ty_methodProto(methodname, prats, mth_provisos, proto, _) -> methodname = ss
                        | Bsv_ty_nom(_, F_fun(_, matts, _), _)                          -> matts.methodname = ss
                        | Bsv_ty_nom(kind, F_subif(iname, _), _)                        -> iname = ss                        
                        | Bsv_ty_dontcare _ ->
                            vprintln 0 "// Schedule order statements in interfaces leads to this ... perhaps filter from iparts please!"
                            false
                        | other -> sf (sprintf "look for '%s' pred other %A" ss other)
                    let candidates = List.filter pred ii.iparts
                    let rec togo candidates =
                        match candidates with
                        | [Bsv_ty_nom(kind, F_subif(tag_, sty), _)] ->  
                            let m1 = "subinterface kind=" + htosp kind.nomid
                            let ans =
                                match vale with
                                    | B_ifc(PI_ifc_flat path, _) -> B_ifc(PI_ifc_flat(ss :: path), sty)

                                    | B_var _  -> // unbound? var - I get called during typechecking at the moment so this is possible
                                         let subvale = B_hexp(sty, X_undef)
                                         B_subif(ss, subvale, sty, lp)
                                    | B_subif(ss, subvale, sty, lp) -> B_subif(ss, subvale, sty, lp)
                                    | other -> sf (mf() + sprintf ": other subif form on tag '%s' ty=%A vale=%A" ss (hd candidates) other)
                                    // B_subif(ss, vale, ty, g_builtin_lp) // todo null lp not builtin
                            (hd candidates, None, ans)


                        | [Bsv_ty_methodProto(methodname, prats, mth_provisos, proto, ty)] ->
                            let ans = B_dyn_b(ss, vale) // extend vale with this tag - different cat operator needless complexity?
                            (hd candidates, None, ans)

                        | [Bsv_ty_nom(_, F_fun _, _)] -> // actually a method
                            //vprintln 0 (sprintf "waa  waa look for '%s' vale %A" ss vale)
                            let ans = B_dyn_b(ss, vale) // this is the tag concat form
                            (hd candidates, None, ans)
                        | [] ->  cleanexit(mf() + sprintf ": Interface '%s' has no method call or sub-interface named '%s'" (htosp iidl.nomid) ss)
                        | a::b::_ -> cleanexit(mf() + sprintf ": Interface '%s' has overloaded method calls or sub-interfaces named '%s'" (htosp iidl.nomid)  ss)
                        | other -> sf(mf() + sprintf ": Interface '%s' has other items named '%s' %A" (htosp iidl.nomid)  ss other)
                    togo candidates
                | other -> sf (sprintf "looking for field '%s' in unanticipated form of hierarchic path '%s' %A" ss (mf()) other)
            retag ty

        let (ty3, path3, vale3) = prep_auto_apply ww bobj mf asReg tyo lp (ty2, path2, vale2)
        if vd>=5 then vprintln 5 (mf() + sprintf ": Auto_apply %A for tag '%s' was %s" asReg ss (if nonep path3 then "None" else bsv_expToStr (snd (valOf path3))))
        (ty3, path3, vale3)   


    | KB_id(ibaser, (lp, _)) -> // norm_exp
        let mf() = lpToStr0 lp + sprintf ": norm identifier + '%s'" ibaser
        let ww = WF 3 qm ww (mf())
        let dbi = -2020 // for now
        let rec hnav_idv = function
            | GE_normthunk(instf, idl, ans, reaped, _, ff) when not(nonep !ans) -> hnav_idv (valOf !ans)
            | GE_normthunk(instf, idl, ans, reaped, being_done, ff) when not(nonep !being_done) -> muddy("hit 6 " + htosp idl)
            | GE_normthunk(instf, idl, ans, reaped, being_done, ff) -> // idiomatic kate code - nth copy!
                if not (nonep !being_done) then sf (mf() + " recursion being_done L6900")           
                let knot2 = ref None
                being_done := Some(Bsv_ty_knot(idl, knot2)) // GE_normthunk(instf, idl, ans, reaped, being_done, ff))            
                let a1 = ff ww ee // { ee with genenv=ee.genenv.Remover (hd idl); } // [ hd idl ]
                being_done := None
                reaped := a1
                let kate = function
                    | Aug_env(tyclass, bound_name, (dmtof, who, EE_e(ty, vale))) ->
                        knot2 := Some ty
                        //vprintln 0 (sprintf " TODO correct: ribbish vale 33 %A" vale)
                        ans := Some(GE_binding(bound_name, who, EE_e(ty, vale))) // TODO check k=hd idl! and check only one
                    | tt -> muddy (mf() + sprintf (": kate id other form: %A") tt)
                app kate a1
                hnav_idv (valOf !ans)

            | GE_formal(so, id, ty)                  -> // delete me - unnecessary form.
                //vprintln 0 ("+++formal outlet of B_var + " + id)
                //if ee.forcef then sf (mf() + ": variable " + ibaser + " is found only as a formal: it should be bound when forcing") // TODO should this ever happen?
                let dbi = -2002 // Delete me
                (ty, Some(ty, B_var(dbi, [id], lp)), B_var(dbi, [id], lp))


            | GE_mut(idl, ty, whom_, vref) ->
                //vprintln 0 ("mut outlet of B_var + " + htosp idl)
                let lmode = B_var(dbi, idl, lp)
                let v =
                    match ee.muts.lookup vref with
                        | None ->
                            //vprintln 0 (mf() + sprintf ": muts lookup for %s %s are NISH" ibaser vref)
                            lmode
                        | Some v ->
                            //vprintln 0 (mf() + sprintf ": muts lookup for %s %s are %A" ibaser vref (bsv_expToStr(snd v)))
                            snd v
                (ty, Some(ty, lmode), v) 
                
            | GE_binding(bound_name, whom, EE_e(ty, vale)) ->
                if bobj.wverbosef then vprintln 3 (whom.whom + ": lmode outlet of B_var + " + htosp bound_name + " as " + bsv_expToStr vale + ":::" + bsv_tnToStr ty)
                (ty, Some(ty, B_var(dbi, bound_name, lp)), vale)

            | GE_binding(bound_name, whom, EE_typef(_, ty)) -> // deepbind should possibly convert these to typeActuals or else merge these forms
                //vprintln 3 (whom.whom + ": typeactual outlet of B_var + " + htosp bound_name)
                (ty, Some(ty, B_var(dbi, bound_name, lp)), B_var(dbi, bound_name, lp))

            | GE_overloadable(prototy, overloads) -> 
                // This L5081 overload disambiguation code is now replicated above: TODO have one copy!
                let give vd = vprintln vd ("Overloads exist. They are " + sfold (fun (x, _) -> (sfold tyclassToStr x) + "||") overloads)
                let sigtys = // list of argument types as idl stringls.
                    match tyo with
                        | None ->  muddy (mf() + sprintf " we need a type %A " tyo)
                        //| Some(Bsv_ty_monad__(args, _, _, _)) -> map (fun (so_, t) -> t) args // so ignored
                        | Some other -> muddy "need abs fun form TODO"
                let candidates = List.filter (fun (gis, _) -> gis=sigtys) overloads
                let no = length candidates
                let tailer bos = hnav_idv (GE_binding(muddy "deabs tailer", {g_blank_bindinfo with whom="refudge overload selected 2/2"}, EE_e bos))
                if no = 1 then muddy "need deabs tailer(snd(hd candidates))" // TODO this just fails here ... cannot ever be used?
                else
                    let _ = give 0
                    vprintln 0 ("temp additional " + sfold tyclassToStr sigtys)
                    vprintln 0 (mf() + sprintf " found %i suitable overloads in scope." no)
                    cleanexit(mf() + ": need exactly one overload for arg " + sfold (tyclassToStr) sigtys)

            | x -> sf (mf() + sprintf ": norm_id: other identifier binding for '%s': %A" ibaser x)

        let vv = ee.genenv.TryFind[ibaser]
        //vprintln 0 (mf() + sprintf ": Looked up KB_id %s and got %A" ibaser vv)
        match vv with
            | None ->
                    let vd = 0
                    vprint vd "In-scope identifiers are: "
                    for z in ee.genenv do ignore(vprint vd (htosp z.Key + " ")) done 
                    vprintln vd ""
                    cleanexit(mf() + sprintf ": Unbound variable '%s'"  ibaser)
            | Some x ->
                let (ty, l_vale, svale) = hnav_idv x
                prep_auto_apply ww bobj mf asReg tyo lp (ty, l_vale, svale)


    | KB_subscripted(dotflag, item, subs_lst, m_bitflag, (lp, uid)) when !m_bitflag -> // norm_exp - convert to a bit select
        let reversed = rev subs_lst
        let bitsel = hd reversed
        let item =
            match tl reversed with
                | [] -> item
                | lst -> KB_subscripted(dotflag, item, rev lst, ref false, (lp, uid))
        let refactored = KB_bitSelect(item, bitsel, (lp, uid + "RF"))
        tc refactored

    | KB_bitSelect(item, subs, (lp, uid)) -> // norm_exp
        let mf() = lpToStr0 lp + ": bitSelect"
        let ww = WF 3 qm ww (mf())
        let (sty, _, svale) = norm_bsv_exp_lmode_tyo ww bobj ee (Some Bsv_ty_integer) (Prov_None "bit-select-lhs") subs
        let const_subs = bsv_const_exp ww mf svale
        let autoctrl = (Prov_None "bit-select2") // Allow auto_apply within bitselects regardless?
        let (bty, _, bvale) = norm_bsv_exp_lmode_tyo ww bobj ee None autoctrl item 

        let constant_subs idl (lo, hio) =
            let v = try int(bsv_lower_m ww bobj mf svale) // TODO use BigInt
                            with _ -> sf(sprintf " cannot convert %A " (bsv_lower_m ww bobj mf svale))
            if v < 0 || (not(nonep hio) && v > valOf hio) then cleanexit(mf() + (sprintf ": Subscript %i out of range %i..%i to %s" v lo (valOf hio) (htosp idl)))
            v


        let (lv, rv) =
            if const_subs then
                let upper =
                    match enc_width_ev ww ee (fun()->"smatch const_subs bit_extract: " + mf()) bty with
                        | Wsome w when w > 0 -> Some(w-1)
                        | Wsome z -> sf (mf() + "  wsome " + i2s z)
                        | _ -> None
                let baser = constant_subs [] (0, upper)
                //in B_bit_replace(ty, bvale, 1, baser, lp)//a bit extract on rhs...
                let rv = B_field_select("$BITSEL", bvale, baser, 1) // bit select - empty string is pseudo tag for union tag. // TODO use lvale_' for bit insert?  // why is gen_B_field_select not used?
                let lv = rv
                (lv, rv)
            else
                muddy (mf() + ": dynamic bit extract") // TODO implement this!
        (g_bool_ty, Some(g_bool_ty, lv), rv)


    | KB_subscripted(dotflag, item, subs_lst, bitflag, (lp, uid)) -> // norm_exp
        // bitflag only refers to the last (outermost) subscription index of course.
        // Auto_apply refers to the outermost subscription, unless that is a bit select, in which case it applies inside that.
        let mf() = lpToStr0 lp + ": subscripted" + (if !bitflag then " - a bit select" elif dotflag then " dotted - BRAM extension" else "")
        if !bitflag then sf (mf() + " now handled in another clause")
        let ww = if vd>=4 then WF 3 qm ww (mf()) else ww
        let rec doadim autoctrl = function 
            | (bit, [], item) -> norm_bsv_exp_lmode_tyo ww bobj ee None autoctrl item 
            | (bit, subs::tt, item) ->
                let (bty, _, vale) = doadim autoctrl (false, tt, item)
                let (sty, _, svale) = norm_bsv_exp_lmode_tyo ww bobj ee (Some Bsv_ty_integer) (Prov_None "doadim") subs
                let const_subs = bsv_const_exp ww mf svale
                //vprintln 0 ("Constant subscript " + boolToStr const_subs)

                let constant_subs idl (lo, hio) =
                    let v = try int(bsv_lower_m ww bobj mf svale) // TODO use BigInt
                            with _ -> sf(sprintf " cannot convert %A " (bsv_lower_m ww bobj mf svale))
                    if v < 0 || (not(nonep hio) && v > valOf hio) then failwith(mf() + (sprintf ": Subscript %i out of range %i..%i to %s" v lo (valOf hio) (htosp idl)))
                    v

                let rec smatch = function
                    | (bty, vale) when not dotflag && nonep(tyderef_o mf bty) ->  // Old bitselect code - delete me.
                        // If not a collection or pointer then pack to bits and do a bit extract or insert.
                        muddy (mf() + sprintf ": no deref ? bitflag=%A Old bitSelect clause still invoked on " !bitflag + bsv_tnToStr bty)
                        let autoctrl' = Prov_None "L7396"
                        let (bty', lvale', vale') = doadim autoctrl' (false, tt, item)
                        let (lv, rv) =
                            if const_subs then
                                let upper =
                                    match enc_width_ev ww ee (fun()-> "smatch const_subs bit_extract: " + mf()) bty with
                                        | Wsome w -> Some(w-1)
                                        | _ -> None
                                let baser = constant_subs [] (0, upper)
                                let rv = B_field_select("$BITSEL", vale', baser, 1) // bit select - empty string is pseudo tag for union tag. // TODO use lvale_' for bit insert? // why is gen_B_field_select not used for the rv - better to do that please. 

                                let lv = rv
#if SPARE
                                    match lvale' with
                                        | None -> cleanexit(mf() + ": invalid lhs for bit insert")
                                        | Some(lt, other) -> sf(mf() + ": other form lvale for bit insert " + bsv_expToStr other)
                                        | other -> sf(mf() + sprintf ": other form lvale for bit insert %A" other)
#endif
                                (lv, rv)
                            else
                                muddy (mf() + ": dynamic bit extract")
                        (g_bool_ty, Some(g_bool_ty, lv), rv)
                        
                    | (ty, vale) when dotflag ->
                        if not_nullp tt then sf (sprintf "dot subscript norm BRAM multi-dim not supported.  %A" vale)
                        let rty =
                            match ty with // TODO: We can use tyderef for this.
                            | Bsv_ty_nom(_, _, [ _; Tty(content_type, _) ]) -> content_type
                            | other ->  muddy (sprintf "dot subscript norm BRAM %A" other)
                        let ans_ = B_subs(dotflag, vale, svale, lp)
                        // autoapply "native_read" for BRAM native support with forwarding.
                        dev_println(sprintf "Convert to native_read with forwarding. rty=%s" (bsv_tnToStr rty))
                        let ans = B_applyf(PI_bno(B_dyn_b("native_read", vale), None), rty, [(sty, svale)], tag_this_lp lp)
                        (rty, None, ans)
                    | (Bsv_ty_nom(_, F_vector, [Tty(len, _); Tty(vector_content_ty, _)]), vale) ->
                        let len1_ =
                            match getival mf [len] with
                                | Some len1 -> len1
                                | _ -> cleanexit(mf() + " vector with unspecified length not suitable at this point " + bsv_expToStr vale)

                        let rec dig = function // vector dig
                            | B_vector(vs, _) ->
                                let bv = valOf(ee.vectors.lookup vs)
                                let nm = htosp bv.iname + "_e"
                                //vprintln 0 ("Subscripted vector " + nm + " has content type " + bsv_tnToStr bv.ct)
                                // This is a totally silly interface for closing the enum group!
                                let grpname = vectornet_w(nm + "_namingroot", 32)
                                let enum p =
                                    let rr = enum_add "OQ" grpname "OQ" p
                                    let _ = ix_deqd grpname rr
                                    rr
                                let lmode =
                                    //Form a reveng by scanning all locations.
                                    let rangel = map (fun x -> (B_hexp(Bsv_ty_integer, enum x), bv.data.TryFind x)) [0..bv.len-1]
                                    let rec reveng c = function
                                        | (q, None) -> c
                                        | (q, Some v) -> (B_bdiop(V_deqd, [q; svale]), v)::c
                                    B_reveng_h(bv.ct, List.fold reveng [] rangel)  

                                close_enum "REVENG-5026" grpname
                                let rmode = if not const_subs then lmode
                                            else
                                                match bv.data.TryFind(constant_subs bv.iname (0, Some(bv.len-1))) with
                                                    | None   -> lmode
                                                    | Some x -> x
                                (bv.ct, Some(bv.ct, lmode), rmode)
                            | B_query(s, g, tt, ff, _, (ncp, lp)) ->
                                let (a, lt, rt) = dig tt
                                let (b, lf, rf) = dig ff
                                let g = bsv_blower ww bobj g// Do not need ebwalk here since already walked.?? TODO check.
                                let l = if nonep lf then lt elif nonep lf then lt else Some(a, gen_B_reveng_l ww bobj mf a (s, g, snd(valOf lt), snd(valOf lf)))
                                (a, l, gen_B_reveng_l ww bobj mf a (s, g, rt, rf))

                            | B_var(dbi_, qf, lp) ->
                                let a = vector_content_ty // = tyderef mm bty // bty has been tyeval'd // should be same as vector_content_ty - not if we are recursing...
                                let lmode = B_subs(dotflag, vale, svale, lp) // Defer...
                                // let lmode = B_var(qf, lp) // or this...
                                //if ee.forcef then sf (mf() + ": variable " + htosp qf + " should be bound when forcing 2/2") 
                                (a, Some(a, lmode), lmode)

                            | B_reveng_h(ty_, lst) ->
                                let a = vector_content_ty
                                let df (g, v) = (g, snd(valOf (f2o3(dig v))))
                                let lmode = B_reveng_h(a, map df lst)
                                (a, Some(a, lmode), lmode)
                                
                            | B_reveng_h(ty, lst) when false -> //subsumed in previous clause.
                                let a = vector_content_ty
                                let lmode = muddy "B_subs(vale, svale, (lp))" // ok ? TODO                                
                                let q = lmode
                                (a, Some(a, q), q) // FOR NOW ------------------------ DO WE NEED TO EVAL CONSTS HERE...?
                            | vale ->  sf (sprintf " norm select other vector here %A" vale)                    
                        dig vale

                    | (_, _) -> // Find other select overload
                        let args = [(bty, vale); (sty, svale)]
                        // For example "function element_type select(List#(element_type) alist, Integer index);"
                        //vprintln 0 (mf() + sprintf " (select method is hopefuly defined for this) other subscripted form ty=%s\nvale=%s\nsubs=%s\n(c1=%A,c2=%A)" (bsv_tnToStr bty) (bsv_expToStr vale) (bsv_expToStr svale) c1 c2)
                        let reveng_flag _ =  "$REVENG_SEL"
                        let m1 = mf() + " [.] select"
                        let callpath' = extend_callpath bobj "L7860" uid ee.callpath (*This is using the UID from the subscript - there may be more than one select in a single subscript list ... so this is a TODO - augment with cardinals...*)
                        let dc = ee.dynamic_chain
                        let evty_e = (evty_create_indirection_flt "L8040")
                        match ee.genenv.TryFind[g_selectname] with
                            | None -> sf (sprintf "No overloads for '%s' were found for %s" g_selectname (bsv_tnToStr bty)) 
                            //
                            | Some(GE_binding(_, operator, EE_e(_, B_abs_lambda(idl3, ty3, fids, body3, ee3, lp3)))) ->
                                let (new_fun_ty_, deabstracted) = mandrake ww bobj ee uid operator mf (B_abs_lambda(idl3, ty3, fids, body3, ee3, lp3))
                                run_func_application ww bobj lp ee.forcef evty_e (dc, callpath') mf (Some(reveng_flag g_selectname)) args deabstracted

                            | Some(GE_binding(_, operator, EE_e(_, B_lambda(nameo, formals, body, ty, lee, lp3)))) ->
                                run_func_application ww bobj lp ee.forcef evty_e (dc, callpath') mf (Some(reveng_flag g_selectname)) args (B_lambda(nameo, formals, body, ty, lee, lp3))
                                        
                            | Some x ->
                                vprintln 0 (sprintf ("Did not get some lambda? %A") x) // TODO Move to norm_apply
                                let rmode = B_applylam(callpath', PI_dk g_selectname, args, lp, ref None, ref None) // TODO useful? Used for what?
                                (tyderef mf bty, None, rmode)

                smatch (ty_eval ww bobj ee mf bty, vale)

        let autoctrl = (Prov_asReg_asIfc "innermost") // need lmode for type compatibility - but this disables auto_apply of _read in a bit extract so that part is run again if encountered. If we are doing a bit insert? ... TODO check
        let (ty9, l9, r9) = doadim autoctrl (!bitflag, rev subs_lst, item)  // Reverse subscripts: the last one is the outermost. We will peel that off first and recurse inwards.

        // Auto_apply finally done - but this is not correct for a bit select!
        //If not an interface with a read method then cannot automatically apply _read() or unit ().
        prep_auto_apply ww bobj mf asReg tyo lp (ty9, l9, r9)


    | KB_valueOf(x_, (lp, uid)) -> //cannot prep to a normal function apply since its arg is a type. Generally a numeric type that will need 'imperialise' to get its final value.
        // Valueof cannot expect its actual arg to have any meaning after types have been renamed.
        // Fortunately we made a type dropping for that arg which should have been appropriately munged as needed.
        let mf() = lpToStr0 lp + "functionCall valueof"
        let ww = if vd>=4 then WF 4 qm ww (mf()) else ww
        let ncp = extend_callpath bobj "L7890vof" uid ee.callpath        
        let ty = simple_retrieve ww bobj mf "L7890vof" ee uid
        //vprintln 0 (sprintf "+++ valueOf: Need to eval %A" (bsv_tnToStr ty))
        let ty = ty_eval ww bobj ee mf ty
        //vprintln 0 (sprintf "+++post eval %A" ty)                
        bsv_valueof ww mf ee ty

    | KB_functionCall(KB_dk xs, args, (lp, uid)) -> // system function call (aka pli task).
        let mf() = lpToStr0 lp + ": system function call " + xs
        let ww = if vd>=4 then WF 4 qm ww (mf()) else ww
        let args' = map (norm_bsv_exp_rmode_tyo ww bobj ee None (Prov_None "function-arg")) args
        match op_assoc (xs) g_pliTasks with// Weak: this stops pli being higher order!
           | None -> sf "no assoc"
           | Some (r, _) ->
                vprintln 3 (mf() + ": Mapped PLI call " + sprintf " '%s' to pliStmt '%s' "  xs r)
                //let argf((ft, _), a) = norm_bsv_exp_rmode_tyo ww bobj ee (Some ft) false a // cannot use argf since we dont know formal types in this context.
                let args' = map (norm_bsv_exp_rmode ww bobj ee) args
                let aa = B_pliStmt(r, args', lp)
                (g_action_ty, None, aa)

    | KB_functionCall(KB_ast path, args, (lp, uid)) -> // builtin function call or (newly) module app (do), but not an interface method call
        let rec summary sl = function
            | KB_subscripted(dotflag, item, subs_lst, bitflag, lp) -> summary ("[..]"::sl) item
            | KB_tag(b, id, _) -> summary (id::sl) b
            | KB_id(s, (lp, _)) when sl=[] -> ((fun()->lpToStr0 lp + " " + htosp ee.static_path + " normexp functionCall + " + s), Some s, lp)
            | KB_id(s, (lp, _)) when sl<>[] -> ((fun()->lpToStr0 lp + " " + htosp ee.static_path + " normexp functionCall + " + htosp(s::sl)), None, lp)
            | other -> ((fun()->lpToStr0 lp + " " + htosp ee.static_path + " functionCall name non-immediate "), None, lp)
        let (mf, scalar_function_name, lp) = summary [] path
        let ww = if vd>=4 then WF 3 qm ww (mf()) else ww
        match scalar_function_name with
            | Some "asIfc"
            | Some "asReg" ->
                let args' = map (norm_bsv_exp_lmode_tyo ww bobj ee tyo (Prov_asReg_asIfc "genuine-asReg")) args
                hd(args')
#if SPARE
            | Some "toydebugprint_" -> // A djg backdoor message/warn/error TODO unif with error/message/warn
                let args' = norm_bsv_exp_rmode_tyo_utzip ww bobj ee (None) None (List.zip [1..length args], args)) 
                let a = B_applyf(PI_biltin(valOf scalar_function_name), Bsv_ty_dontcare "SFN1", args', lp)
                (Bsv_ty_dontcare "SFN", None, a)
#endif
            | _ ->

                    //These are all ref transp so if arg is constant so should result be !
                let apply_tailer (fns, args) =
                    if memberp fns g_builtin_functions
                    then
                        match (fns, args) with
                            | ("log2" as fn, _)  -> muddy "log2 missing TODO"

                            | ("pack" as fn, [v0])   ->
                                let ncp = extend_callpath bobj "L8404pack" uid ee.callpath
                                match retrieve_type_annotation bobj mf "pack" ncp with
                                    | Tanno_function(r_ty_, _, _, [(_, arg_ty)]) ->
                                       let (ty_, v) = norm_bsv_exp_rmode_tyo ww bobj ee (None) (Prov_None "pack") v0
                                       let w = enc_width_no ww ee (fun()->"pack width") arg_ty // Pack wants encoding width of what it is packing
                                       let rt = mk_int_ty w
                                       vprintln 3 (mf() + sprintf ": builtin pack operation types %s -> %s" (bsv_tnToStr arg_ty) (bsv_tnToStr rt))
                                       (rt, None, B_applyf(PI_biltin "pack", rt, [(arg_ty, v)], tag_this_lp lp))
                                    | _ -> sf ("other L8404pack")
                                    
                            | ("fromInteger" as fn, [v0]) 
                            | ("unpack" as fn, [v0]) ->
                                let ncp = extend_callpath bobj "L8411unpack" uid ee.callpath
                                match retrieve_type_annotation bobj mf "unpack/fromInteger" ncp with
                                    | Tanno_function(r_ty, _, _, [(_, arg_ty)]) ->
                                        let (r_ty_, v) = norm_bsv_exp_rmode_tyo ww bobj ee (None) (Prov_None "unpack") v0
                                        let a = B_applyf(PI_biltin "unpack", r_ty, [(arg_ty, v)], tag_this_lp lp)
                                        (r_ty, None, a)
                                    | _ -> sf ("other L8421unpack")
                                    
                            | ("max" as fn,     [a;b])
                            | ("min" as fn,     [a;b])  ->
                                let ncp = extend_callpath bobj "L7962" uid ee.callpath
                                let prov = Prov_si "Arith"
                                match retrieve_type_annotation bobj mf "max/min" ncp with
                                    | Tanno_function(r_ty, tve_, _, [(_,lt); (_,rt)]) ->
                                        let (ta, va) = norm_bsv_exp_rmode_tyo ww bobj ee (Some lt) prov a
                                        let (tb, vb) = norm_bsv_exp_rmode_tyo ww bobj ee (Some rt) prov b
                                        let signed = Signed//TODO get this from lt/rt.
                                        let r_val = gen_B_query ww bobj (false, (if fn = "max" then gen_B_dgtd ww bobj signed ((ta, va),(tb, vb)) else gen_B_dgtd ww bobj signed ((tb, vb),(ta, va))), va, vb, r_ty, (g_builtin_lp, ncp))
                                        (r_ty, None, r_val)
                                    | _ -> sf ("other L6810")

                            | ("abs", [a])  ->                
                                let abs_uid = extend_callpath bobj "L7973" uid ee.callpath
                                let prov = Prov_si "Arith"
                                match retrieve_type_annotation bobj mf "abs" (abs_uid) with
                                    | Tanno_function(r_ty, tve_, _, [(_,lt)]) ->
                                        let (ta, va) = norm_bsv_exp_rmode_tyo ww bobj ee (Some lt) prov a
                                        let zero = (Bsv_ty_integer, B_hexp(Bsv_ty_integer, xi_num 0))
                                        let r_val = gen_B_query ww bobj (false, gen_B_dgtd ww bobj Signed ((ta,va),zero), va, gen_B_subtract bobj ww ta zero (ta, va), ta, (g_builtin_lp, abs_uid))
                                        (ta, None, r_val)
                                    | _ -> sf ("other L6819")


                            | ("exp" as fn,     [a;b])  ->
                                let ncp = extend_callpath bobj "L7985" uid ee.callpath
                                let prov = Prov_si "Arith"                                
                                match retrieve_type_annotation bobj mf "exp" ncp with
                                    | Tanno_function(r_ty, tve_, _, [(_,lt); (_,rt)]) ->
                                        let (ta, va) = norm_bsv_exp_rmode_tyo ww bobj ee (Some lt) prov a
                                        let (tb, vb) = norm_bsv_exp_rmode_tyo ww bobj ee (Some rt) prov b
                                        let (ia, ib) = try (bsv_lower_m ww bobj mf va, bsv_lower_m ww bobj mf vb)
                                                       with _ -> cleanexit(mf() + sprintf ": cannot exponentiate 'exp' %A " (va, vb))

                                        (Bsv_ty_integer, None, B_hexp(Bsv_ty_integer, local_xi_bnum(powerXI ia ib))) // TODO call corrct bnum constructor


                            | ("extend" as fn,     [v0])  // These builtin functions are monadic at the BSV level
                            | ("zeroExtend" as fn, [v0])  // but we here convert them to functions of pairs for later processing.
                            | ("signExtend" as fn, [v0])  // The new arg is the resultant width now wanted.           
                            | ("truncate" as fn,   [v0])     ->
                                let ncp = extend_callpath bobj "L8481-truncate-extend" uid ee.callpath
                                match retrieve_type_annotation bobj mf fn ncp with
                                    | Tanno_function(r_ty, _, _, [(_, arg_ty)]) ->
                                        //let prov = Prov_asReg_asIfc fn // TODO explain why what was asReg.  It stopped smalltests/Test3.
                                        let prov = Prov_None fn // This should either be retrieved from the type annotation or else looked-up on an fn basis (part of the function's def).
                                        let (old_type_, v) =  norm_bsv_exp_rmode_tyo ww bobj ee tyo prov v0 
                                        vprintln 3 (mf() + sprintf ": builtin '%s' operator on types %s (really %s) -> %s" fn (bsv_tnToStr arg_ty)  (bsv_tnToStr old_type_) (bsv_tnToStr r_ty))
                                        //vprintln 0 (sprintf "Wot it is %A" r_ty)
                                        let tailer w1 ans = (ans, None, B_applyf(PI_biltin fn, ans, [(arg_ty, v); w1], tag_this_lp lp)) 
                                        match enc_width_o ww mf r_ty with
                                            | Wsome w ->
                                                vprintln 3 (fn + sprintf " new width %i" w)
                                                let w1 = (Bsv_ty_integer, B_hexp(Bsv_ty_integer, xi_num w))
                                                tailer w1 r_ty
                                            | _ ->
                                                match enc_width_o ww mf old_type_ with // Do it the old way - TODO explain further please.
                                                    | Wsome w ->
                                                        vprintln 3 (fn + sprintf " new width (old way) %i" w)
                                                        let w1 = (Bsv_ty_integer, B_hexp(Bsv_ty_integer, xi_num w))
                                                        tailer w1 old_type_

                                                    | _ -> cleanexit (mf() + ": Needed something with a specific bit width to apply " + fn)
                                    | _ -> sf ("other L8481truncate-extend")

                            | (fn, args) when exec_reporter_predicate fn ->
                                vprintln 3 (sprintf "System function: message/error/warning : %s" fn)
                                let args' = map (norm_bsv_exp_rmode ww bobj ee) args
                                let rv = exec_reporters ww mf (lpToStr0 lp) fn ee.dynamic_chain args'
                                let rty = simple_retrieve ww bobj mf "L8392" ee uid
                                (rty, None, snd rv)
                                
                            | ("$format", args) ->
                                let args' = map (norm_bsv_exp_rmode ww bobj ee) args
                                // Should be at least one arg which is a string
                                if nullp args then cleanexit(mf() + " $format requires at least one argument which should be a string")
                                else
                                // First arg is a string, others are args
                                    let _ = check_isstring (hd args)
                                    (Bsv_ty_format, None, B_format([snd(hd args')], tl args'))
                            | (fns, args) -> cleanexit(mf() + " supposedly built-in function was not: " + fns)

                    else
                        let rec apply8 ge =
                            //vprintln 0 (sprintf "BDAY apply 8 binding %A" ge)
                            match ge with
                            | GE_binding(k, operator, EE_e(sty, B_knot(idl, fknot))) when not(nonep !fknot) ->
                                let z = valOf !fknot
                                apply8(GE_binding(k, operator, EE_e(fst z, snd z)))

#if NOLONGER
                            | GE_binding(_, operator, EE_e(Bsv_ty_fun _, B_knot(idl, fknot))) when not (nonep !fknot) -> muddy ("Recursive function knot not yet tied for " + htosp idl)
                            | GE_binding(bound_name, operator, EE_ea(pp_, (qq, _), B_abs_lambda(idl, name, fids, body, local_ee, lp1))) ->
                                let mats =
                                    match qq with
                                        | Bsv_ty_nom(_, F_fun(_, mats, _), _) -> mats
                                        | _ -> muddy (sprintf "please give me the matss from the 2nd %A or 3rd %A fld" pp_ qq)
                                let m1 = lpToStr0 lp + " apply8 abstract function proto deabstraction replication OLD " + htosp hidl
                                let ww = WF 3 mm ww mm1
                                //if mats.hof then vprintln 0 ("Want HOF retrieve")
                                let ncp = extend_callpath bobj "L8041" uid ee.callpath
                                match retrieve_type_annotation bobj mf "nolonger" ncp with
                                    | Tanno_function(rt, tve_, _, annos) ->
                                        let reformalise = function
                                            | (fid, (_, ty)) -> (SO_none "$deabsfun", ty, fid)
                                        let formals_final = map reformalise (List.zip mats.fids annos)
                                        let new_fun_ty = Bsv_ty_nom([name], F_fun(rt, mats, map snd annos), freshlister)
                                        //let deabstracted_________ = B_lambda(Some (htosp idl + "$hint"), formals_final, body, new_fun_ty, local_ee, lp1)
                                        apply8(GE_binding(bound_name, "exo2b", EE_e(new_fun_ty, deabstracted)))
                                    | x ->
                                        sf(mf() + sprintf " is now  used? %A lam=%A" x 0)
#endif

                            | GE_overloadable(prototy, [(forwot, ((stype, fl_), item))]) ->
                                apply8(GE_binding([], {g_blank_bindinfo with whom="refudge overload selected solo"}, EE_e(stype, item))) // One item is trivial if we use this approach !

                            | GE_overloadable(prototy, overloads) -> // L5458 replicate of L5081
                                // This overload disambiguation code is now replicated above: TODO have one copy!
                                let give vd = vprintln vd (sprintf "%i overloads exist, being " (length overloads) + sfold (fun (x, _) -> (sfold tyclassToStr x) + "||") overloads)
                                let args' = map (norm_bsv_exp_rmode ww bobj ee) args
                                let sigtys = map (fun (ty, vale) -> digest_cooked (mf(*() + " overloaded typeclass discriminate 1/2"*)) ty) args'
                                let candidates =
                                    let digest_equals al bl =
                                        if vd>=5 then vprintln 5 (sprintf "digest_equals lhs=%s rhs=%s" (sfold hptos al) (sfold hptos bl))
                                        al=bl
                                    List.filter (fun (pairs, _) -> digest_equals (map fst pairs) sigtys) overloads
                                let no = length candidates
                                let tailer ((ty, fl), vale) =
                                    if not(nullp fl) then sf "fl ignored"
                                    apply8(GE_binding([], {g_blank_bindinfo with whom="refudge overload selected 1/2"}, EE_e(ty, vale)))
                                if no = 1 then
                                    vprintln 0 "need deabs 2/2"
                                    tailer(snd(hd candidates))
                                else
                                    let _ = give 0
                                    vprintln 0 ("temp sigtys: " + sfold htosp sigtys)
                                    vprintln 0 (mf() + sprintf " %i suitable overloads in scope." no)
                                    cleanexit(mf() + ": need exactly one overload for arg " + sfold (fun (t,v)->bsv_tnToStr t) args')
        //REFSITE                        
                            | GE_binding(bound_name, operator, EE_e(fun_ty, svale)) -> 
                                match svale with
                                    | B_abs_lambda(idl, name, fids, body, local_ee, lp1) -> // Abstract lambda to concrete lambda deabstraction.
                                        let (new_fun_ty, deabstracted) = mandrake ww bobj ee uid operator mf svale
                                        apply8(GE_binding(bound_name, { operator with whom="exo2b" }, EE_e(new_fun_ty, deabstracted))) // go round again.
                                            
                                    | B_maker_abs(idl, ats, aprams3, provisos, body_, ee_inner, backdoors, lp) -> 
                                        let mm = (htosp idl + " deabstract module in apply8")
                                        let _ = WF 3 "instantiate module (refsite)" ww mm
                                        //let mod_mk = deabs4_exp ww bobj ee iname mm svale (Some(ifc_ty, outer_cp, iactuals2))
                                        let args' = map (norm_bsv_exp_rmode ww bobj ee) args
                                        let nameo = Some fns //0muddy (fns + " <- need nameo from one src or other ->" + htosp idl)
                                        let ans = B_thunk(PI_bno(svale, nameo), args')
                                        (g_maker_sty_ty, None, ans)


                                    | other_non_abs -> // (B_lambda etc): eval now
                                        let (mpx_fun_tys, nameo, formals1, maxargs) =
                                            match svale with
                                            | B_lambda(nameo, formals1, body_, fun_ty, closure_ee_, lp1_) -> ([fun_ty], nameo, formals1, None)

                                            // TODO : a clause to support a mux of possible functions is likely here when conditional indirect calls are used.
#if OLD_OR_SPARE
                                            | B_maker_l(idl, ats, (pparams, iused, iftype, hardstate), stmts, ifc_provisos, backdoors, lp) ->
                                                let mi = funique "PL5UGH"
                                                let nameo = muddy ("need nameo from 22 " + htosp idl)                                                
                                                let allargs = pparams @ iused @ [ (SO_none "$meval", iftype, mi) ] // idiomatic
                                                ([nameo], allargs, Some (length allargs)) // get them all for monolithic apply -- will make work for jiggle?

                                            | B_applyf(PI_bno(fvale, None), return_type, args, lp) ->
                                                let xarg_revert (ty, ex) = (SO_none "dewop-yuck", ty, funique "xarg_dewop")
                                                let nameo = muddy ("need nameo from 33 " + fns)
                                                (nameo, map xarg_revert args, None)

                                            | B_abs_lambda(idl, _, _, body, local_ee, lp1)  ->
                                                // This might get replicated in the following run_func_application?
                                                muddy ("This one apply8 L4804: " + htosp idl)
#endif
                                            | other -> sf (sprintf "other applied-eight form %A" other)

                                        let mm1 = sprintf "function application (apply8) fns='%s' %s" fns (lpToStr0 lp)
                                        let ww = WF 3 "apply8" ww mm1
                                                // Here, in apply8 we need first eval of args before run_func_application
                                        let _ =
                                            match maxargs with
                                               | None -> if length args <> length formals1 then cleanexit(mf() + sprintf ": Wrong number of arguments for call '%s'  #actuals=%i <> #formals=%i" fns (length args) (length formals1))
                                               | Some mm -> if length args > mm then cleanexit(sprintf "Too many args for mondad call '%s'  %i <> %i" fns mm (length formals1))
                                        let callee_hof = operator.hof_route <> None
                                        let argf(xc, raw) = // again sqf_tyform style normaliszation but no eval here ... TODO split sqf accordingly and use one half here.
                                            let (so_, ft, fid) = select_nth (xc-1) formals1
                                            let fid =
                                                if fid <> "$HOF" then fid
                                                else match ft with
                                                        //| Bsv_ty_nom(nst, F_fun _, _) when length nst.id=1 -> hd nst.id
                                                        | other ->
                                                            vprintln 0 (sprintf "$HOF (L8113) should already have been recoded from %A so=%A" (bsv_tnToStr ft) so_)
                                                            let _ = sf "this no longer happens"
                                                            fid
                                            let bg_nominal_fetch = function
// TODO need to collate over multiplexed functions                                                
                                                | B_abs_lambda(idl, _, _, _, _, _) -> Some idl // This is the name/callstring of the real function.
                                                | other ->
                                                    //vprintln 0 (sprintf "+++ bg_nominal_fetch other form %A" other)
                                                    None
                                            // TODO some type meet here ft.
                                            // May need formal binds from one in successor... TODO and tyvars?
                                            // The norming serves as the eval of the actual parameter.
                                            let (ty, a') = norm_bsv_exp_rmode_tyo ww bobj ee (Some ft) (Prov_None "selectstop") raw
                                            let abg = bg_nominal_fetch a'    
                                            let _ =
                                                if operator.hof_route <> None then
                                                    vprintln 3 ("found arg_hof needed for arg " + fid + sprintf " pushdown %A with %A" abg (bsv_tnToStr ty))
                                                    vprintln 3 (mf() + sprintf " norm HOF fun arg in apply8 did=%s  post=%s ty=%s\n\n" fid (bsv_expToStr a') (bsv_tnToStr ty))
                                                    ()
                                            ((ty, a'), (fid, abg))
//TODO all that hof work is now dropped - need to pass it fwd as a bindinfo_t attribute in the arg binding 
                                        let (args', hof_mapping) = List.unzip(map argf (List.zip [1..length args] args))
                                        let evty_i = (evty_create_indirection_flt "L8110")
                                        let hoftokens  = [ "TODO-replacethismarker-L6479" ]
                                        let dc = ee.dynamic_chain
                                        let ncp =
                                            if callee_hof && false then // SITER 1 - apply (when forcing - otherwise a thunk). want to hydrate the hof droppings now
                                                let htig = match muddy "yesnames" with // TODO delete me
                                                           | [[one]] -> one // one-part name too
                                                           | [] -> sf "no callee analysis information present"
                                                           | _ -> muddy "multiple callees not yet implemented"
                                                vprintln 0 (sprintf "not using %s, using htig=%s hof_mapping %A" uid htig hof_mapping)
                                                let recf = 0 // TODO not always false/0 ?
                                                let ncp = extend_callpath2 bobj "L8110hof" (recf, Some(hoftokens, evty_i)) htig ee.callpath  
                                                vprintln 0 ("L8110" + " extend callpath hof prefix to " + cpToStr ncp)
                                                ncp
                                            else extend_callpath bobj "L8110" uid ee.callpath                            

                                        let ans = run_func_application ww bobj lp ee.forcef (evty_i) (dc, ncp) mf(*mm1*) nameo args' svale
                                        //vprintln 0 (sprintf "finally getting %A" (f3o3 ans))
                                        ans

                            | GE_binding(_, operator, EE_e(ty, B_var(dbi, x, lp))) -> // was TODO bind as a B_var here? why not use a GE_formal ?
                                //vprintln 0 (sprintf " var %s normed to itself outlet!" (htosp x))
                                //if ee.forcef then sf ("forcef 3/44 required proper binding here for var " + htosp x)
                                let dbi = -2090 // dbi revale site 
                                (ty, Some(ty, B_var(dbi, x, lp)), B_var(dbi, x, lp)) 

                            | GE_normthunk(instf, idl, ans, reaped, being_done, ff) when not(nonep !ans) -> apply8 (valOf !ans)
                            | GE_normthunk(instf, idl, ans, reaped, being_done, ff) when not(nonep !being_done) -> sf ("hit 7 " + htosp idl)
                            | GE_normthunk(instf, idl, ans, reaped, being_done, ff) -> // idiomatic kate code - nth copy!
                                if not_nonep !being_done then sf (mf() + " recursion being_done L7428")
                                let knot2 = ref None
                                being_done := Some(Bsv_ty_knot(idl, knot2)) // GE_normthunk(instf, idl, ans, reaped, being_done, ff))
                                let mm = "norm1_close"
                                let q = { ee with genenv=ee.genenv.Remover[fns] }
                                let augs = ff ww q
                                being_done := None                        
                                let r = List.fold (augment_env_entry ww bobj mf "normthunk-L7428") q augs 
                                let kate = function
                                    | Aug_env(tyclass, bound_name, (dmtof, who, vale))->
                                        knot2 := Some(ee_sty mf vale)
                                        //vprintln 0 (sprintf "ribbish vale 44 %A" (eeToStr vale))
                                        ans := Some(GE_binding(bound_name, who, vale))
                                    | tt -> sf (mf() + sprintf ": kate func '%s' other %A" (htosp idl) tt)
                                app kate augs
                                apply8 (GE_normthunk(instf, idl, ans, reaped, being_done, ff))

                            | other -> sf(mf() + sprintf ": apply8: Unsuitable expression used as a function '%s': %A " fns other)

                        match ee.genenv.TryFind[fns] with
                            | Some func -> 
                                //vprintln 0 (sprintf "Norming method/function call '%s' - found a body" fns)
                                apply8 func

                            | None when length args = 1 ->
                                cleanexit(mf() + sprintf ": undefined (builtin?) monadic function or deconstructor '%s' " fns) // + sprintf "%s" (sfold bsv_expToStr args'))
                            | None -> cleanexit(mf() + sprintf ": undefined (builtin?) function '%s' or wrong no (%i) of args " fns (length args))// + sprintf "%s" (sfold bsv_expToStr args)) 
                match scalar_function_name with
                    | Some fns -> apply_tailer (fns, args)
                    | None -> // leave unapplied but with args and function pointer eval'd - ignore the force. will be done during ewalk - why do we ever need this please? Is it only for those functions like sign_extend that need to be done during 'lower' ? If so perhaps have a specific denotation.
                        //let mm = if String.length mm > 201 then mm.[0..200] else mm        
                        //let ww = WF 3 qm ww mm        
                        let (ty, _, fvale) = norm_bsv_exp_lmode_tyo ww bobj ee None (Prov_asReg_asIfc "nonscalar-function-name") path
                        let (args', return_type) =
                            match ty with
                            | Bsv_ty_nom(_, F_fun(rt, matts, arg_tys), _) ->
                                let argf(fid_, ft, a) = // TODO unify use run_func_application  proper deep bind etc..
                                    //vprintln  0 (mf() + sprintf " NORM FUN ARG other path: formal=%s  tyo=%A raw actual=%A\n\n" id ft a)
                                    norm_bsv_exp_rmode_tyo ww bobj ee (Some ft) (Prov_None "nonsc=fn") a
                                let args' = map argf (List.zip3 matts.fids arg_tys args)
                                (args', rt)

                            | other -> muddy (mf() + sprintf ": other thing applied as function [L6721] %A" ty)

                        let ans = B_applyf(PI_bno(fvale, None), return_type, args', tag_this_lp lp) // TODO applylam?
                        (return_type, None, ans)


    | KB_functionCall(KB_op oo, [l; r], (lp, uid)) -> // Normal diadic operator like integer xor or addition.
        let mf() = lpToStr0 lp + ":KB_diadic op"  + f1o3(xToStr_dop oo)
        let ww = if vd>=4 then WF 3 qm ww (mf()) else ww
        let  l' = (norm_bsv_exp_rmode ww bobj ee) l
        let  r' = (norm_bsv_exp_rmode ww bobj ee) r
        let abs_uid = extend_callpath bobj "L8217" uid ee.callpath
        let (_, ty) = //depends on operator and its provisos really but can do a meet here?
                 sum_type_meet ww bobj mf [] (fst l', fst r')
                 // Bsv_ty_dontcare // TODO signed/unsigned + width res
                 // let v1 = if bsv_const_exp ww mm l' && bsv_const_exp ww mm r' then B_hexp(ty, bsv_lower ww v0) else v0
        let v0 = gen_B_diadic bobj ww mf oo ty (l', r')
        (ty, None, v0) // xi_bdiop(oo, [hlower l'; hlower r'], false))

    | KB_functionCall(KB_bop _, [l; r], (lp, uid))
    | KB_functionCall(KB_ab _, [l; r], (lp, uid)) -> // bool*bool->bool
        let a = B_blift(norm_bsv_bexp_rm ww bobj ee aa)
        (g_bool_ty, None, a)

    | KB_actionValueBlock(key, nameo1, body, nameo2, (lp, uid)) -> // norm expression copy: TODO unify with statement copy.
        let id = funique(if nameo1=None then lpToStr0 lp else valOf nameo1)
        let mf() = (if nameo1=None then "" else lpToStr0 lp) + sprintf " norm expression actionValueBlock key=%s " key + id
        let ww = if vd>=4 then WF 3 qm ww (mf()) else ww
        if nameo1<>None && nameo2<>None && valOf nameo2 <> valOf nameo2 then cleanexit(key + ": end name does not match start: " + valOf nameo1 + " cf " + valOf nameo2)        
        let (_, body') = List.fold (norm_bsv_bev ww bobj tyo) (ee, []) body
        let rvt =
            match key with
                | "lambda" -> Bsv_ty_nom({g_blank_nom_typ_ats with nomid=["ActionValue"]; }, F_actionValue (valOf_or_fail "type needed for lambda" tyo), [])
                | "actionvalue" ->
                    Bsv_ty_dontcare "norm should no longer be returning types anyway - done in earlier phase"

                | "action" -> g_action_ty
        (rvt, None, gen_B_action "av-block-ast L8773" ee.fbody_context (id, rev body', RV_highlevel rvt, lp))

    | KB_functionCall(KB_unop "!", [l], (lp, uid)) ->
        let mf() = lpToStr0 lp + ": monadic!"
        let ww = if vd>=4 then WF 3 qm ww (mf()) else ww
        let (lt, l') = norm_bsv_exp_rmode ww bobj ee l
        let ans = B_applyf(PI_biltin "!", g_bool_ty, [(lt, l')], tag_this_lp lp)
#if SPARE
        let vv = if not(bsv_const_exp ww mf l') then ans
                 else 
                       muddy (mf() + sprintf " pling not constant. But why should it be? ans=%A" ans)
#endif
        (g_bool_ty, None, ans)

    | KB_taggedUnion(vtag, amembers, soloo, (lp, _))
    | KB_taggedUnionExpr(vtag, amembers, soloo, (lp, _)) ->
        let (mf, vtag, possible_types, uid) = prep_tagged_expr ww bobj ee aa
        let ww = if vd>=4 then WF 3 qm ww (mf()) else ww
        let possible_types =
            match tyo with
                | Some ty ->
                    let ty = tag_type_checker ww bobj lp mf vtag ty possible_types
                    //dev_println (sprintf "We selected %s" (bsv_short_tnToStr ty))
                    [ty]
                | None -> map fst possible_types

        let sty =
            match possible_types with
            | [] ->
                let vd = 0
                vprint vd "In-scope tags are: "
                for z in ee.ctor_tags do ignore(vprint vd (z.Key + " ")) done 
                vprintln vd ""
                cleanexit(mf() + sprintf ": A tagged union with tag '" + vtag + "' is not in scope")
            | a::b::_ -> cleanexit(lpToStr lp + ": Disambiguation of two or more tagged unions with tag '" + vtag + "' not possible in this context.")

            | [sty] ->
                let rec unknot = function
                    | Bsv_ty_knot(idl, k44t) when not(nonep !k44t) -> unknot(valOf !k44t)
                    | Bsv_ty_nom(nomid, F_tunion(mrecflag, variants, details_), args) -> sty
                    | sty -> sf(mf() + ": other form of tag type build: " + bsv_tnToStr sty)
                unknot sty
        let (sty, rmode) = 
            match tyo with
            | Some (Bsv_ty_dontcare _ )
            | None ->
                match sty with
                    | Bsv_ty_nom(nomid, F_tunion(mrecflag, variants, details_), args) -> 
                        let mm = "norm nominal tagged union expression " + htosp nomid.nomid
                        let ww = WN mm ww
                        let fty = afreshen_sty ww bobj "taggedUnion/taggedUnionExpr-2" None(*no kickoff*) sty 
                        (fty, build_strunion ww bobj mf ee (fty, amembers, [], soloo, Some vtag))

            | Some(Bsv_ty_nom(nomid, F_tunion(mrecflag, variants, details_), args)) -> 
                let _ = 
                    match sty with
                        | Bsv_ty_nom(nomid', F_tunion(mrecflag', variants', details_), args) when nomid.nomid = nomid'.nomid -> ()
                        | other -> cleanexit(mf() + sprintf ": Cannot use type '%s' where '%s' expected" (bsv_tnToStr sty) (bsv_tnToStr (valOf tyo)))
                let mm = "norm nominal tagged union expression " + htosp nomid.nomid
                let ww = WN mm ww
                let fty = valOf tyo
                //vprintln 0 (mf() + " WINE USING tyo " + bsv_tnToStr fty)
                (fty, build_strunion ww bobj mf ee (fty, amembers, [], soloo, Some vtag))

            | Some(snugger) -> sf (sprintf "snugger %A" snugger)
            
        let lmode = rmode // TODO aggregate with const code..
        (sty, Some(sty, lmode), rmode)

    | KB_structPattern _
    | KB_structExprPositional _
    | KB_structExprAssociative _ ->
        // There's two flows of control going on at once here: cleaner to separate out the structPattern clause which builds a ...
        let (uid, ttags, mf) = prep_spa "norm" aa
        let qmm = "norm structPatterns L8463"
        let ww = WF 3 qm ww (if vd>=4 then mf() + ": " + qmm else qmm)
        let ttag = hd ttags // TODO we can do better by taking the intersection of the support of the present tags
        //dev_println (qmm + sprintf ": norm_bsv_exp_rmode_tyo ty=%s" (if nonep tyo then "none" else bsv_tnToStr(valOf tyo)))
        let ty =
            if nonep tyo then
                hpr_yikes (sprintf "Know to be old/broken dropping route on " + mf() + ": " + qmm)
                simple_retrieve ww bobj mf qmm ee uid  // Broken old way.
            else valOf tyo // Ths route should always be taken
        dev_println (qmm + sprintf ": Build struct: uid=%s type is %s" uid (bsv_tnToStr ty))
        let tailer ty = 
            let (amembers, soloo, vtago) = 
                match aa with
                    | KB_structExprPositional(ttag, pmembers, lp) when length pmembers=1 && tagarity mf ty = Some 1 -> // Only one member: we know which it is!
                        let solo = hd pmembers
                        ([], Some solo, Some ttag)
                    | KB_structExprPositional(ttag, pmembers, lp) ->
                        let tags = // Convert positional to associative by inserting explicit tags in order.
                            match ty with
                                | Bsv_ty_nom(nst, F_struct(fields, details_), _)  -> map fst fields
                                | other -> cleanexit(mf() + sprintf ": expected a structure type, not %s" (bsv_tnToStr ty))
                        if length tags <> length pmembers then cleanexit(mf() + sprintf ": Wrong number of positional elements in structural expression of type %s" (bsv_tnToStr ty))
                        (List.zip tags pmembers, None, Some ttag)
                    | KB_structPattern(strlst, (lp, uid)) ->
                        //vprintln 0 (sprintf "god %A no struct pattern " strlst + lpToStr1 lp)
                        (strlst, None, None (*no tago for a struct - they are all used*)) 
                    | KB_structExprAssociative(ttag, amembers, lp) -> (amembers, None, Some ttag)
            //vprintln 0 (sprintf "Calling build_strunion for tag '%s' and with ty=%s vtago=%A" ttag (bsv_tnToStr ty) vtago)
            let v = build_strunion ww bobj mf ee (ty, amembers, [], soloo, vtago)
            (ty, None, v) // ditto should have an lmode for assignments to the such. TODO test.

        tailer ty

#if SPARE_OLD__MINER
        let st1 sty =
            let nomid =
                let rec get_nomid = function
                    | Bsv_ty_knot(nomid, k2) when not(nonep !k2) -> get_nomid (valOf !k2)
                    | Bsv_ty_nom(nomid, _, args) -> nomid
                    | other -> sf(mf() + " other form L7526")
                get_nomid sty


            match tyo with
                | None ->
                    // This does happen - for instance in BRAM_configure's default value.
                    tailer sty
                | Some(Bsv_ty_nom(nomid', F_struct _ , args)) when nomid.id <> nomid'.id -> 
                    cleanexit(mf() + sprintf ": (L7532) Cannot use type '%s' where '%s' expected" (bsv_tnToStr sty) (bsv_tnToStr (valOf tyo)))

                | Some(Bsv_ty_nom(nomid', F_tunion _, args))
                // tagged union when struct expected: for example:
                // branch_or_call_dest = Valid(arg0);
                // was parsed as structExprPositional when it is really a tagged union expression of a simple sort.


                | Some(Bsv_ty_nom(nomid', F_struct _, args)) when nomid.id = nomid'.id -> 
                    tailer (valOf tyo)
                | other -> sf(mf() + sprintf ": other form L7539 %A" other)

        match ee.ctor_tags.TryFind ttag with // Dont do this and lookup_type - need to similarly abstract for tags.
            | Some[(sty, _)] -> st1 sty
            | Some other -> sf (sprintf "assoc/tag match other length - needs disambig %A" other)
            | None ->
                match ee.genenv.TryFind [ttag] with 

                | Some(GE_binding(_, _, EE_typef(_, sty))) -> st1 sty
                | Some other -> sf (mf() + sprintf ": used tag '%s'other form=%A" ttag other)
                | None ->
                    let ty =
                        match tyo with
                            | None ->
                                let vd = 0
                                vprint vd ("In-scope tags are: ")
                                for z in ee.ctor_tags do ignore(vprint(vd, z.Key + " ")) done 
                                vprintln vd ""
                                muddy(mf() + sprintf ": no internal type analysis case for tag '%s' - currently please only use in a context where the required type is obvious 2/2 " (sfold (fun x->x) ttags))
                            | Some(Bsv_ty_nom(_, F_tunion _, _))
                            | Some(Bsv_ty_nom(_, F_struct _, _)) -> valOf tyo                                                       

                            | Some other ->  sf (mf() + sprintf ": other external feed tag=%s,  %A" ttag (bsv_tnToStr other))
                    tailer ty
#endif


    | KB_bitRange2(arg, ll, rr, (lp, uid)) -> // norm_exp 
        let mf() = lpToStr0 lp + ": bitRange"
        let ww = WF 3 qm ww (mf())
        let ((aty, lmode, argv), (_, ll), (_, rr)) = (norm_bsv_exp_lmode ww bobj ee arg, norm_bsv_exp_rmode ww bobj ee ll, norm_bsv_exp_rmode ww bobj ee rr)
        let l1 = bsv_manifest_32 ww bobj mf ll
        let r1 = bsv_manifest_32 ww bobj mf rr
        let baser = if l1 < r1 then l1 else r1
        let rw = (abs(l1-r1)+1)
        // TODO bit insert clause is missing?
        let rmode = B_mask(rw, (B_shift(argv, -baser))) // -ve means right shift.
        let lt = mk_uint_ty rw
        let rt = lt
        let lmode1 = if nonep lmode then None else Some(lt, B_mask(rw, (B_shift(snd(valOf lmode), -baser))))  // another lmode form!
        (rt, lmode1, rmode)
        
    | KB_bitConcat(args, lp) ->
        let mf() = lpToStr0 lp + ": bitConcat"
        let ww = WF 3 qm ww (if vd>=4 then mf() else "bitConcat")
        let args' = map (norm_bsv_exp_rmode ww bobj ee) args
        let widths = map (enc_width_no ww ee mf) (map fst args')
        let orate (w, (ty, item)) (sofar, pos)  = (("", (Bsv_ty_dontcare "orate", w, pos, item))::sofar, pos+w)
        let (ans, total_width) = List.foldBack orate (List.zip widths args') ([], 0)
        // TODO check lhs insert  // TODO lmode assign to bitConcat
        let nomid = [mf(); "$ANON"]
        let ty = Bsv_ty_primbits{signed=None; width=total_width; branding="_Bit" }
        let v = B_aggr(ty, TU_struct ans)
        (ty, Some(ty, v), v)

    | KB_typeAssertion(ast_ty, exp, (lp, uid)) -> 
        let mf() = lpToStr0 lp + ": typeAssertion"
        let ww = WF 3 qm ww (if vd>=4 then mf() else "typeAssertion")
        let ctype = simple_retrieve ww bobj mf "L10039" ee uid
        //let rt = ref (Bsv_ty_dontcare "typeAssertion") // We could retieve on uid...
        let (inner_type_, exp) = norm_bsv_exp_rmode ww bobj ee exp

        (ctype, None, exp)


    | KB_sublang(seqpar, lst, lp) ->
        let mf() = lpToStr0 lp + " FSM sublang"
        let ww = WF 3 qm ww (mf())
        let uid = funique (htos ee.prefix) + "_fsm_"
        let pcvals = ref []

        let gen_eascFsmStmt lp h =
            let stepname = funique uid
            mutadd pcvals stepname
            //vprintln 0 (sprintf "Created leaf FSM step %A" h)
            Bsv_eascFsmStmt(h, stepname, lp)

        let rec gen_Bsv_seqFsmStmt lp = function
            | []     -> gen_eascFsmStmt lp (B_hexp(Bsv_ty_dontcare "Fsm", X_undef))
            | [item] -> item
            | a::b::c-> Bsv_seqFsmStmt(a, gen_Bsv_seqFsmStmt lp (b::c), lp)

        let rec gen_Bsv_parFsmStmt lp = function
            | []     -> gen_eascFsmStmt lp (B_hexp(Bsv_ty_dontcare "Fsm", X_undef))
            | [item] -> item
            | a::b::c-> Bsv_parFsmStmt(a, gen_Bsv_parFsmStmt lp (b::c), lp)

        let rec norm_subl aa =
            match aa with
            | KB_await(g, lp) -> 
                let mf() = lpToStr0 lp + " FSM await"
                let ww = WF 3 "norm1" ww (mf())
                let fsm_biltin_action(action, args) = gen_eascFsmStmt lp (B_applyf(PI_biltin action, Bsv_ty_dontcare "await", args, tag_this_lp lp))
                let fsm_await(e) = fsm_biltin_action("await", [(Bsv_ty_integer, B_blift e)]);
                fsm_await(norm_bsv_bexp_rm ww bobj ee g)

            | KB_actionValueBlock _
            | KB_regWrite _ -> 
                let rtyo = None // TODO an action value does have a return value.. 'Some Action'
                let (mf, body') =
                    match aa with
                    | KB_actionValueBlock(key, nameo1, body, nameo2, (lp, uid)) -> // norm fsm sublang site
                        let mf() = lpToStr0 lp + " FSM " + key + " block + " + (if nameo1=None then "sublang:" + key else valOf nameo1)
                        let ww = WF 3 qm ww (if vd>=4 then mf() else " -- ")
                        if nameo1<>None && nameo2<>None && valOf nameo2 <> valOf nameo2 then cleanexit(mf() + ": end name does not match start: " + valOf nameo1 + " cf " + valOf nameo2)        
                        let (_, body') = List.fold (norm_bsv_bev ww bobj rtyo) (ee, []) body
                        (mf, body') 
                    | KB_regWrite _ ->
                        let mf() = "sublang regWrite"
                        let ww = if vd>=4 then WF 3 qm ww (mf()) else ww
                        let (_, body') = norm_bsv_bev ww bobj rtyo (ee, []) (aa)
                        (mf, body')
                let ww = WF 3 qm ww (if vd>=4 then mf() else " -- ")

                let atomicer arg cc =
                    match arg with
                       | Bsv_eascActionStmt(solo, _) ->
                           //let (_, e') = norm_bsv_exp_rmode ww bobj utag ee solo
                           //vprintln 0 (sprintf "normed fsm %A \n\n\n" solo)
                           (gen_eascFsmStmt lp solo)::cc // Collect up parallel actions which advance FSM one step only.
                       | other ->
                           vprintln 0 ("Also have the following in an FSM action step " + bsv_stmtToStr other)
                           cc //other ::cc
                if true then gen_Bsv_parFsmStmt lp (List.foldBack atomicer body' [])
                else
                match body' with// TODO wrap in appropriate atomic conjunction of guards. ... will be done by PAR constructor
                  | [] -> gen_eascFsmStmt lp (B_hexp(Bsv_ty_dontcare "L7394", X_undef))
                  | [Bsv_eascActionStmt(solo, _)] -> gen_eascFsmStmt lp solo
                  | other -> muddy (mf() + sprintf ": multi action .1.. %A" other) // copy 1/2


            | KB_easc(e, lp) ->
                let mm = lpToStr0 lp + " FSM eascStmt"
                let ww = WF 3 qm ww mm                
                let (_, e') = norm_bsv_exp_rmode ww bobj ee e
                let r = gen_eascFsmStmt lp e'
                in r

            | KB_sublang(seqpar, lst, lp) ->
                let mm = lpToStr0 lp + " FSM sublang"
                let ww = WF 3 qm ww mm
                if seqpar = "seq" then gen_Bsv_seqFsmStmt lp (map norm_subl lst)
                elif seqpar = "par" then gen_Bsv_parFsmStmt lp (map norm_subl lst)
                else sf "seqpar was neither"

            | KB_while(g, s, lp)->
                let mm = lpToStr0 lp + " FSM while"
                let ww = WF 3 qm ww mm
                let g' = norm_bsv_bexp_rm ww bobj ee g
                let s' = norm_subl s
                Bsv_whileFsmStmt(g', s', lp)

            | KB_if(g, t, fo, lp)->
                let mf() = lpToStr0 lp + " FSM IF"
                let ww = WF 3 qm ww (mf())
                let g' = norm_bsv_bexp_rm ww bobj ee g
                let t' = norm_subl t
                let fo' = if fo=None then None else Some(norm_subl (valOf fo))
                match g' with
                    | B_and[] -> t'
                    | g' -> Bsv_ifFsmStmt(g', t', fo', lp)
                
            | other -> sf (mf() + sprintf ": sublang other %A" other)
        
        let fsm = match lst with
                   | [solo] -> B_fsmStmt(norm_subl solo, !pcvals)
                   | other  -> sf (mf() + " FSM should have one outermost seq or par construct")
        (Bsv_ty_dontcare "L7434", None, fsm)

    | KB_regWrite(path, r, (lp, uid)) ->
        let mm = match path with
                 | KB_id(s, _) -> lpToStr0 lp + " regWrite + " + s
                 | other   -> lpToStr0 lp + " regWrite (non-scalar)"
        let ww = WN mm ww
        cleanexit (lpToStr0 lp + ": Register write <= not supported as an expression: perhaps enclose in action ... endaction")


    | KB_if(_, _, _, lp)
    | KB_case(_, _, lp)
    | KB_while(_, _, lp)
    | KB_block(_, _, _, lp) ->
        let rt = ref (Bsv_ty_dontcare "SMT-NOT-EXP")
        let rtyo = None
        let (ee_, rr) = norm_bsv_bev (WN "autovalof" ww) bobj rtyo (ee, []) (aa)
        (!rt, None, B_valof(rr, tyo, lp))


    | KB_returnStmt(_, (lp, _)) ->
        let rt = ref (Bsv_ty_dontcare "RTYO") // We can find this dropping if we want but the new coding style is that we no longer rely on the returned type of norm since all required annotations are stored.
        let rtyo = None
        let (ee_, rr) = norm_bsv_bev (WN "autovalof" ww) bobj rtyo (ee, []) (aa)
        (!rt, None, B_valof(rr, tyo, lp))

//    | KB_dot bv ->

    | other -> sf ("norm_lmode exp ast other " + sprintf "%A" other)




//
// A monadic decldo is written                   as    x <- f(y..z); A
// can be expressed with the pipeline operator   as    f(y..z) >>= (\x -> A)
// which means f(y..z) is applied to the currency to return a new currency where the sequal A can be evaluated with x bound.
//
// In BSV terms,   Bit#(X) R <- mkReg(initval) ; R.write(e) is converted to
//                 mkReg(initval) >>= (\R -> R.write(e))
// and the resulting monad is applied to the top-level structural instance name (iname/utag).

//
// - one callsite
//        
and norm_decldo ww bobj mf (ee, cc) bound_path ifc_name (iname: string list) genrf (tyo, lhs, lhs_uid, rhs_raw, lp) =
    // iname is same as ifc_name at the moment although an inode might be passed soon?
    let env = []
    let vd = bobj.normalise_loglevel
    let vidl = ifc_name
    let qm = mf() + ": norm_decldo"
    let ww = WF 3 qm ww "start"
    let lhs_ncp = extend_callpath bobj "L8600" (if g_smell then hd iname else lhs_uid) ee.callpath // use lhs_uid for the declaration type dropping.
    // Please clarify - is this the type of what is assigned or of the identifier mutated, which will not be the same in general (e.g subscripted and tagged lhs expressions).
    let mt44 =
        match retrieve_type_annotation bobj mf "retrieve-mt44" lhs_ncp with // TODO use simple retrieve
        | Tanno_function(_, _, _, [(_, anno)]) -> anno
        | other -> sf (mf() + sprintf " wrong form dropping retrieved for %s  %A" (cpToStr lhs_ncp) other)

    let (ee, cc) =
        match tyo with
            | None -> (ee, cc)
            | Some ast_ty ->
                let ty = mt44
                //vprintln 0 (mf() + sprintf ": using dropped anno=%A\nfor  %A" (bsv_tnToStr mt44) ast_ty)                
                let who = { g_blank_bindinfo with whom="mish10" }
                let decl = Bsv_varDecl(false, ty, vidl, None, (lp, lhs_uid))
                vprintln 3 (mf() + sprintf ": norm_decldo %s:  binding '%s' as %s" who.whom (hptos bound_path) (bsv_stmtToStr decl))
                let kv = (bound_path, GE_binding(bound_path, who, EE_e(ty, B_hexp(ty, X_undef)))) // THIS IS A DUFF BINDING - thats not potentially how a do works ?  Really - please explain.
                ({ ee with genenv= ee.genenv.Add kv }, decl::cc)

    let gens = prep_module_app ww mf rhs_raw
    let rec rhs_select = function
        | (gcond, body)::tt ->
            let rec eval = function
                | [] -> true
                | (pol, gg)::tt ->
                    let g2 = norm_bsv_bexp_rm ww bobj { ee with forcef=Some true; } gg
                    let rec monkey = function
                        | other when bsv_const_bexp ww mf other -> bsv_bmanifest ww bobj mf other // monkey (B_bexp(bsv_blower ww other))
                        | other -> 
                            let em = sprintf ": compile-time-constant while guard needed, not %s" (bsv_bexpToStr other)
                            let _ = whereami_del em (vprintln 0) ww 
                            cleanexit(mf() + em)
                    monkey g2 <> pol && eval tt
            if eval gcond then body else rhs_select tt
        | [] -> sf "no matching body..." // currently not possible - although could make a nop


    let the_rhs = rhs_select gens
            
    let rec rhs_prepare = function
        | (mf, generator, mc, provisos', None, uid) ->
            //muddy "this option is not used now - no 'konstant' g_modgens"
            rhs_prepare (mf, generator, mc, provisos', Some([], [], []), uid) // Add a dummy apply

        | (mf, generator, Some mc, provisos', Some(genargs, evc, args_sans_evc), rhs_uid) ->
            let ww = WN "rhs_prepare dox" ww
            let dox arg =
                // We could pick up an anno on these args but we currently do not bother
                let (ty, _, vale) = norm_bsv_exp_lmode_tyo ww bobj ee (None) (Prov_s provisos') arg
                (ty, vale)
            ignore(exec_reporters ww mf (lpToStr0 lp) mc ee.dynamic_chain (map dox args_sans_evc))
            None
            
        | (mf, generator, None, provisos', Some(genargs, evc, args_sans_evc), rhs_uid) -> 
            let _:unit->string = mf
            let ww = WN (mf()) ww
            let rhs_cp = extend_callpath bobj "L8656" rhs_uid ee.callpath            
//----------delete snip Most of the following can be discarded now but we will need to resurrect the code for replicateM and errorM which are not yet supported.
// snip to nelson hero below 
// Actually now we have types we can use this way? But even better is to use apply8 code
            let (modgen, genargs'') =
                let genargs'' =
                    if nullp args_sans_evc then []
                    else
                        let rhs_pairs =
                            match retrieve_type_annotation bobj mf "nelson" rhs_cp with
                                | Tanno_function(_, _, _, annos) ->
                                    //vprintln 0 (mf() + sprintf ": Got anno=%A\ninstead of %A" annos absmodgen)
                                   let tal = length annos
                                   let argl = length args_sans_evc
                                   let _ =
                                       if tal <> argl then
                                           sf (mf() + sprintf ": Internal error wrong number of droppings  tal=%i argl=%i" tal argl)
                                   //vprintln 0 (mf() + sprintf ": number of droppings %s rhs_tal=%i argl=%i" (cpToStr rhs_cp) tal argl)
                                   List.zip annos args_sans_evc
                                | other -> sf (mf() + sprintf ": rhs other retrieved for '%s' %A " (cpToStr rhs_cp) other)

                        let deg = function // TODO -this must not be used going forward - arg type is needed as per apply8
                            // TODO no type passed in to eval - should be a norm function apply...
                            | ((fid, sty), arg) ->
                                let (ty, _, vale) = norm_bsv_exp_lmode_tyo ww bobj ee (Some sty) (Prov_None "deg") arg
                                (ty, vale) //muddy (sprintf "(ty, x,y) %A" a)
                        map deg rhs_pairs//eval without ifc type is hard. use apply8 later.

                let rec rhs_preparerx = function
                    | Some(GE_binding(_, _, EE_e(ty, vale))) -> (ty, vale)
                    | Some(GE_normthunk(instf, idl, ans, reaped, being_done, ff)) when not(nonep !being_done) -> sf ("hit 15 " + htosp idl)
                    | Some(GE_normthunk(instf, idl, ans, reaped, _,  ff)) when not (nonep !ans) -> rhs_preparerx(!ans)
                    | Some(GE_normthunk(instf, idl, ans, reaped, being_done, ff)) ->
                        if not_nonep !being_done then sf (mf() + " recursion being_done L7910")
                        let knot2 = ref None
                        being_done := Some(Bsv_ty_knot(idl, knot2)) // GE_normthunk(instf, idl, ans, reaped, being_done, ff))
                        let a1 = ff ww ee //{ ee with genenv=ee.genenv.Remover (idl); } // [ hd idl ]
                        reaped := a1
                        being_done := None
                        let katerx = function
                            | Aug_env(tyclass, bound_name, (dmtof, who, vale)) ->
                                let _ =
                                    knot2 := Some(ee_sty mf vale)
                                    ans := Some(GE_binding(bound_name, who, vale)) // TODO check k=hd idl! and check only one
                                ()
                            | tt -> sf (sprintf "katerx other '%s' %A" (htosp idl) tt)
                        app katerx a1
                        rhs_preparerx !ans

                    | Some(GE_overloadable(_, overloads)) ->
                        let sigtys = map (fun (t,v) -> digest_cooked (mf(*() + " overloaded typeclass discriminate 1/2"*)) t) genargs''    
                        let candidates = List.filter (fun (pairs, _) -> map fst pairs = sigtys) overloads
                        if length candidates > 1 || nullp candidates
                        then
                            app (fun (idl, ((sty, fl_), vale)) -> vprintln 0 (sprintf "Candidate=%s sty=%s" (sfold tyclassToStr idl) (bsv_tnToStr sty))) overloads
                            if nullp candidates then cleanexit(sprintf "generator=%s: no suitable typeclass overload found for signature=" generator + sfold htosp sigtys)                    
                            else cleanexit(sprintf "generator=%s: more than one typeclass overload applies: further overload disambiguation needed cf " generator + sfold htosp sigtys) 
                        else
                            let bop (_, ((sty, _), vale)) = Some(GE_binding([], {g_blank_bindinfo with whom="preparerx resolved"}, EE_e(sty, vale)))
                            rhs_preparerx(bop(hd candidates))
                            //snd(hd candidates) - need to freshen?
                            //muddy ("got other than 1 possible overloaded candidates for " + generator)

                    | Some x -> muddy (sprintf "modgen rhs_preparerx other binding: %A " x)
                    | None ->
                            cleanexit(mf() + sprintf ":  module maker definition '%s' was not in scope" generator)
                match generator with 
                    | ("replicate" as mc)  | ("replicateM" as mc) ->
                        //There's no reason why this is built in to the compiler.
                        let mm =  "builtin-replicator-beingused" + lpToStr1 lp
                        muddy (sprintf "builtin-replicator-beingused args=%A" genargs'')

                    | generator ->
                        let (sty_, v) = rhs_preparerx(ee.genenv.TryFind[generator])
                        (v, genargs'')

            let oldway44 =  B_applyf(PI_bno(modgen, None), mt44, genargs'', tag_this_lp lp)  // Is oldway still used?  If so director evc should be passed on.

// An array takes an Integer size whereas a Vector uses a numeric type for its size.
// Vectors are often initialised using 'replicate' or 'replicateM'.

//---nelson hero -------delete to here. snip somewhere above
            let rhs_elabf = function
                | (((Bsv_ty_nom(kind, F_ifc_1 members, _)) as ifc), ifc_name) -> B_ifc(PI_ifc_flat ifc_name, ifc)
                | ((Bsv_ty_nom(_, F_vector, [Tty(len, _); Tty(ct, _)])) as vt, ifc_name) ->
                    let iname = ifc_name // for now on vectors TODO check comment
                    let vs = funique "vector"
                    let i_len =
                        match getival mf [len] with
                            | Some len1 -> len1
                            | _ -> cleanexit(mf() + " vector with unspecified length not suitable at this point " + bsv_tnToStr len)

                    ee.vectors.add vs { iname=iname; len=i_len; ct=ct; vt=vt; data=NoEqMap.empty }
                    B_vector(vs,  (ifc_name, vt))
                | (other, idl) -> cleanexit(sprintf "%s: lhs (%s) is not an interface or vector of interfaces: dexty other rhs_elab %A" (mf()) (htosp idl) (bsv_tnToStr other))
            Some(evc, rhs_elabf, oldway44)
              
    match rhs_prepare the_rhs with
        | None -> (ee, cc)
        | Some(raw_evc, rhs_elab44, gen44) ->
            let dirinfo = refine_dir_info ww vd qm ee.attribute_settings.dirinfo raw_evc
            let rec decl_do_lhs_drill extensions = function
                | KB_tag(subs0, utag, (lp, uid)) ->
                    decl_do_lhs_drill (utag::extensions) subs0

                | KB_subscripted(dotflag, KB_id(name, (lp, _)), subs_lst, bitflag, _) when length subs_lst > 1 || not_nullp extensions -> muddy(mf() + " multi dimensional or mixed tagged/subscripted not supported here yet.")
                | KB_subscripted(dotflag, KB_id(name, (lp, _)), [subs0], bitflag, _) -> // Subscripted assignment (to Vector).
                    let (st, subs) = norm_bsv_exp_rmode ww bobj ee subs0
                    let vidl = ifc_name
                    if bsv_const_exp ww mf subs then
                        let sn = bsv_lower_32 ww bobj mf subs
                        let idl1 = ("_" + i2s sn) :: vidl // Make ordinal tag.
                        let sv = false // Currently this is false for an array since the individual elements are created from the Bsv_varDecls (unlike structs) (for now at least).
                        let decl (bvv:vector_t) = Bsv_varDecl(sv, bvv.ct, idl1, None, (lp, lhs_uid)) // Even when there is no declaration in this construct, we are assigning to an array location with an implicit invokation of the one-line form and hence this declaration is needed
                        let assign ifc_ty = Bsv_eascActionStmt(B_moduleInst(idl1, (dirinfo, idl1, lhs_ncp), idl1, gen44, (lp, lhs_uid)), Some lp)
                        let nval ovs it =
                            let bvv = valOf(ee.vectors.lookup ovs)
                            if sn < 0 || sn >= bvv.len then cleanexit(mf() + sprintf ": subscript %i out of range 0..%i for %s" sn (bvv.len-1) (htosp vidl))
                            let vs = funique "vector"
                            ee.vectors.add vs { bvv with data=bvv.data.Add(sn, it); } // Functional augment ... TODO only when it is constant as well.
                            B_vector(vs, (vidl, bvv.vt))

                        match ee.genenv.TryFind bound_path with 
                            | None ->
                                cleanexit(mf() + sprintf ": variable '%s' is not declared when assigned (L0)" (hptos bound_path))
                            | Some(GE_binding(_, _, EE_e(ty, B_vector(vs, _)))) ->
                                let bvv = valOf(ee.vectors.lookup vs)
                                let it = rhs_elab44(bvv.ct, idl1)
                                let nb = (bound_path, GE_binding(bound_path, { g_blank_bindinfo with whom="mishQ2"}, EE_e(ty, nval vs it)))
                                vprintln 3 (sprintf "functional set up assign to vector %s[0..%i] at %i" (htosp bvv.iname) (bvv.len-1) sn)
                                //vprintln 3 (sprintf "functional set up assignment for location %i of array %A with eponymous interface %A" sn (htosp vidl) (htosp idl1))
                                ({ ee with genenv= ee.genenv.Add nb }, [ assign bvv.ct; decl(bvv) ]) // Have assign before decl since list is constructed reversed.
                                // Why do we have both forms of vector assignment, B and F? TODO explain.

                            | Some(GE_mut(_, ((Bsv_ty_nom(_, F_vector, [Tty(len, _); Tty(ct,_)])) as aty), whom_, fer)) ->
                                let it = rhs_elab44(ct, idl1) 
                                let bvv = 
                                    match ee.muts.lookup fer with
                                        | Some(aty_, B_vector(vs, _)) ->
                                            let bvv = valOf(ee.vectors.lookup vs)
                                            ee.muts.add fer (aty, nval vs it)
                                            bvv
                                        | None ->
                                            let it = rhs_elab44(ct, idl1)
                                            let i_len =
                                                match getival mf [len] with
                                                    | Some len1 -> len1
                                                    | _ -> cleanexit(mf() + " vector with unspecified length not suitable at this point (L8076): " + bsv_tnToStr len)

                                            let bvv = {iname=vidl; ct=ct; len=i_len; vt=aty; data=NoEqMap.empty.Add(sn, it) }
                                            ee.muts.add fer (aty, nvector ee bvv)
                                            bvv
                                        | other -> cleanexit(mf() + sprintf ": (fer=%s) variable '%s' not vector or has non-subscriptable form %A" fer name other)
                                vprintln 3 (sprintf "mutable set up assign to vector %s[0..%i] at %i" (htosp bvv.iname) (bvv.len-1) sn)
                                ( ee, [ assign bvv.ct; decl(bvv) ]) // Have assign before decl since list is constructed reversed.
                            | Some other -> cleanexit(mf() + sprintf ": variable '%s' has non-subscriptable form %A" name other)

                    else muddy(mf() + ": Non-constant subscript subs not supported in structural generate.")

                | KB_id(_, (lp, _)) -> // Scalar assignment elaboration.
                     let (ifc_vname, ifc_ty, ee) = // Assert this is what was is handed in.
                        match ee.genenv.TryFind bound_path with // duff relookup and extensions already embedded in idl. Please tidy.
                        | None ->
                            //dev_println (sprintf "The RETCAM binding of %A is %A" (cadr idl) (ee.genenv.TryFind(tl idl)))
                            cleanexit(mf() + sprintf ": Variable '%s' is not declared when assigned (L1).  idl=%s.  prefix extensions=%s." (hptos bound_path) (hptos iname) (hptos (rev extensions)))
                        | Some(GE_mut(vidl, ty, whom_, fer)) ->
                            let it = rhs_elab44(ty, vidl)
                            match ee.muts.lookup fer with
                                | None ->
                                    //vprintln 3 (mf() + sprintf ": '%s' mutable set" (htosp vidl))
                                    ee.muts.add fer (ty, it)
                                | Some _ -> hpr_warn(mf() + sprintf ": '%s' overwrites existing 'do'" (htosp vidl))
                            (vidl, ty, ee)
                        | Some(GE_binding(bound_path, binfo, EE_e(ty, _))) ->

                            match ty with
                                | Bsv_ty_nom(ats, F_struct(fields, _), args) -> sf "need decldo scone struct RETCAM-wed-eve"
                                | _ -> ()
                            let it = rhs_elab44(ty, vidl)
                            let nb = (bound_path, GE_binding(bound_path, {binfo with whom=binfo.whom + ":mosh2"}, EE_e(ty, it))) // nice binding just the ifc
                            let ee = { ee with genenv= ee.genenv.Add nb }
                            (vidl, ty, ee)                     
                        | Some other -> sf (mf() + sprintf ": other duff id '%s' binding %A" (htosp bound_path) other)

                     let thedo = [ Bsv_eascActionStmt(B_moduleInst(bound_path, (dirinfo, ifc_vname, lhs_ncp), iname, gen44, (lp, lhs_uid)), Some lp) ]
                     vprintln 3 (mf() + sprintf ": Created host set up assignment for interface reference %s" (htosp ifc_vname))
                     (ee, thedo)
                | other -> sf ("varDecl_Do other form lhs: " + sprintf "%A" other)
            let (ee, thedo) = decl_do_lhs_drill [] lhs
            //vprintln 0 (mf() + ":normed to " + sfold bsv_stmtToStr thedo)
            (ee, thedo @ cc) // end of norm_decldo


//
//   
// mandrake converts an abstract lambda expression into a concrete one by substituting the type variables.
// TODO explain why this is different from any other operator - is it the potential higher-order use or is that there all other operators are hardcoded and have no internal droppings.
//
and mandrake ww bobj ee uid (operator:binding_info_t) mf svale = // TODO rename
    let vd = bobj.build_loglevel
    match svale with
        | B_abs_lambda(idl, name, fids, body, local_ee, lp) -> // Abstract lambda to concrete lambda deabstraction.
            let mats = { g_blank_fid with methodname=name } // This does little other than insert the droppings for the current callpath and goes round again as a B_lambda.
            let mf() = mf() + ": " + lpToStr0 lp + " apply8 abstract function proto deabstraction replication REFSITE " + htosp idl
            let ww = WF 3 "mandrake" ww (if vd>=4 then mf() else " -- ")
            // If a HOF call, we give the name (or better index no) of the formal where the function arrived in scope rather than the callsite token.  This works for non-polymorphic higher-order functions since the same droppings will be used for all applications of it and hence they must have a common, fixed type.
            // if not_nonep operator.hof_route then vprintln 3 ("Will handle HOF retrieve " + uid)
            // else vprintln 0 ("not a hof retrieve whom=" + operator.whom + " methodname=" +  mats.methodname + " uid=" + uid)
            let ncp = extend_callpath bobj "L8085" uid ee.callpath
            let (new_fun_ty, deabstracted) =
                match retrieve_type_annotation bobj mf "B_abs_lambda-2" ncp with
                    | Tanno_hof(rt, tve, _, annos)
                    | Tanno_function(rt, tve, _, annos) ->                        
                        let reformalise = function
                             | (fid, (_, ty)) -> (SO_none "$deabsfun", ty, fid)
                        //vprintln 0 (mats.methodname + sprintf " mandrake ret zip %i %i " (length fids) (length annos))
                        let formals_final = map reformalise (List.zip fids annos)
                        let new_fun_ty =
                            let arg_tys = map snd annos
                            if vd>=4 then
                                vprintln 4 (sprintf "new_fun_ty: rt=%s arg_tys=%s" (bsv_tnToStr rt) (sfold bsv_tnToStr arg_tys))
                                vprintln 4 (sprintf "new_fun_ty: hof=%A cp=" operator.hof_route + cpToStr ncp + " TODO: ?  WANT TO UPDATE tve " + sfold anToStr tve)
                            Bsv_ty_nom({g_blank_nom_typ_ats with nomid=idl; cls=Prov_func; }, F_fun(rt, { mats with body_gamma=tve }, arg_tys), freshlister ww rt arg_tys) // has not changed the mats.body_gamma - silly. TODO.
                        if vd>=4 then vprintln 4 (sprintf"new_fun_ty: new_fun_ty=%A" (bsv_tnToStr new_fun_ty))
                        let deabstracted = B_lambda(Some (htosp idl + "$hint"), formals_final, body, new_fun_ty, local_ee, (lp, ncp))
                        (new_fun_ty, deabstracted)
                    | x -> sf (mf() + sprintf " is now just pushdown instead? %A lam=%A" x 0)
            (new_fun_ty, deabstracted)

        | other -> sf (sprintf "mandrake other %A" other)
            
// run_func_application. Args already eval'd.
// Generate a B_applylam if not forced or constant.
// The args are just checked for length and inserted verbatim. If force mode, the body is executed (by norming it).
and run_func_application ww bobj lp0 forcef evty_i (dc, callpath) mf nameo args aa =
    let constant_args_pred() = conjunctionate (fun (t,v)->bsv_const_exp ww mf v) args
    let vd = 3
    //vprintln 0 (sprintf " run_func_application callpath==%A" callpath)
    //if nonep callpath then sf "null callpath L7563"
    let rec apply1 aa =
        match aa with
            | B_maker_l _ ->
            // Leave this as unapplied - would like to return a closure containing env... todo that we need formals1
            // (rt, None, B_applyf(PI_bno(modgen, nameo), rt, args, lp))
            // Modgen will support partial application in three ways (prams, prams+iused, prams+iused+iexported)
            // TODO stick args
                vprintln 0 (sprintf "need to keep app closure %A" nameo)
                // can execute here and it should return a redirect for its binding to the nets
                (g_unit_ty, None, B_thunk(PI_bno(aa, nameo), args))  // it might be a partial apply

            | B_lambda(nameo, formals, body, fun_ty, closure_ee, (lp, abs_cp_l)) ->
                let sname = if nameo=None then "<anon>" else valOf nameo
                let mf() = mf() + " " + sname
                let qm = "run_func_application" // if forcef then "run_func_application-e" else "run_func_application-l"
                let ww = WF 3 qm ww (if vd>=4 then mf() else " -- ")
                if length args <> length formals then cleanexit(mf() + sprintf "Wrong number of args given for call '%s'  %i <> %i" sname (length args) (length formals))
                if length args <> length formals then cleanexit(mf() + sprintf ": Wrong number of args provided for call '%s'  %i <> %i" sname (length args) (length formals))
                //vprintln 0 (mf() + sprintf " formals are %A" formals)
                //vprintln 0 (mf() + sprintf " run_func_application fun_ty is %A" fun_ty)

                let rt = // could get from fun_ty instead of lookup at the moment
                    match retrieve_type_annotation bobj mf "B_lambda" (abs_cp_l) with
                        | Tanno_function(rt, _, _, arg_tys__) -> rt
                        | Tanno_hof(rt, _, _,  arg_tys__) -> rt                        
                        | other -> sf (sprintf "lambda retrieve anno other for %s %A" (cpToStr abs_cp_l) other)

                //
                if false || ((forcef=Some true) || constant_args_pred() ) then // Eager on constants may cause unnecessary infinite loop! The one use site of the forcef flag. TODO.

                    let aliases = [] // FOR NOW - the double lookup --------------------- we may need some if a function proto has separately been declared.
                    let closure_ee =
                        let trips = List.zip formals args
                        //vprintln 0 (mf() + sprintf ": elaborate call to %s with args " sname + sfold (fun ((_, _, fid), (t,v))->bsv_expToStr v + "/" + fid) trips)
                        let sqname = sname + ":  " + sfold (fun ((_, _, fid), (t,v))->bsv_expToStr v + "/" + fid) trips
                        let ncp = callpath
                        let ndc = ([sqname], ncp) :: dc // TODO better to keep stacktrack report lazy...
                        if bobj.wverbosef then give_an_elaboration_stack_trace vd ndc
                        let mm = grave_deepbind_arg_ty_list ww vd mf None aliases closure_ee.genenv (*hoftokens, evty_i*) [((SO_none "should use singles site", fun_ty, ""), rt)] trips // Pass rt as a single to extract any inner bindings. Will now only need one call if pass in pairs
                        { closure_ee with genenv=         mm
                                          forcef=         forcef
                                          callpath=       ncp
                                          dynamic_chain=  ndc
                                          fbody_context=  true
                        }

                    let uid = funique "LU8601"
                    // This is the function body application.
                    let ans = norm_bsv_exp_lmode_tyo ww bobj closure_ee (Some rt) (Prov_None "L8529") (KB_actionValueBlock("lambda", Some ("lambda:" + sname), body, None, (lp, uid)))
                    //vprintln 0 (mf() + " norm run of fuction (owing to force or constant_args_flag) returns " + bsv_expToStr(f3o3 ans))
                    ans
                else
                    vprintln 3 (sprintf " Leaving lazy the function apply of  " + sname + " to " +  sfold (fun (t,v)->bsv_expToStr v) args)
                    (rt, None, B_applylam(callpath, PI_bno(aa, nameo), args, lp0, ref None, ref None))

            | other -> sf(sprintf "apply1 other %A" other)
    apply1 aa


//
// norm_decl_assign: Used in structural and bev/rule contexts. Could pass in a flag.
// Also used as a let and the 'do' variant of a let - but operator style not passed in.
// Two forms of return: modified env and kvl.
// The env contains a mutable mut_t used for mutable assignments and simple let bindings for assigned-once functional forms. Then there's vectors...
// kvl ignored in bev env
//        
// Imperative flow-control is handled by norm_bev combined with mutmux.
//        
and norm_decl_assign ww bobj qm_ tyclass (ee, cc) arg0 =
    let vd = bobj.normalise_loglevel
    let qm = "norm_decl_assign"
    let ww = WF 3 qm ww "Start"
    let mf() = qm
    let (shortname, idl, _, uid_, lp) = lhs_path_shortname ww mf arg0
    
    // See which assigns to render in the elaborated output based on assigned type.
    let intermodule_issue_pred site = function // Interface assigns need passing through to the elaborated output.
        | Bsv_ty_nom(_, F_ifc_1 _, _) -> true
        | Bsv_ty_nom(_, F_subif _, _) -> true

        | _ -> false
    let decl_serf0 mf ee lhs (cca, ccd, ccc) =
        let rec decl_serf1 lp prefix (id, idl, ty, uid) (cca, ccd, ccc) = 
            let bound_name = id::prefix
            match ty with
                | Bsv_ty_nom(ats, F_struct(fields, _), args) ->
                    vprintln 3 (sprintf "norm_decl_assign: F_struct scone needed for %s" (bsv_tnToStr ty))
                    let (ty, details) = ensure_struct_details ww bobj qm ee (fields, ats) ty
                    let recursivef = is_recursive_struct details
                    let scone_struct_decl prefix (id, ty) (cca, ccd, ccc) =
                        let uid' = uid + "_" + id
                        dev_println (sprintf "norm_decl_assign: F_struct scone itemer id=%s  idl=%s  uid=%s" (id) (hptos (id::idl)) uid') 
                        decl_serf1 lp prefix (id, id::idl, ty, uid') (cca, ccd, ccc)
                    // Since fers are imutable handles on mutable data, it does not matter if there are multiple copies/bindpaths for them in the environment.
                    let (augs, ccd, fers) =
                        if recursivef then
                            vprintln 3 (sprintf "Not sconing recursive struct %s type %s" (hptos bound_name) (bsv_tnToStr ty))
                            (cca, ccd, [])
                        else
                            List.foldBack (scone_struct_decl (id::prefix)) fields (cca, ccd, [])
                    let hydrate_one ((id, (ty, width, pos)), (ty, vale)) = (id, (ty, width, pos, vale))
                    // We recurse inside structs on each field so that all leaf identifiers are separate fers and hence separately updatable during elaboration.
                    // This is not possible for recursive structs however, so please explain this properly!
                    let vale =
                        match details with
                            |Bsv_struct((width, arity, recf), fields') ->
                                B_aggr(ty, TU_struct(map hydrate_one (List.zip fields' fers)))
                            //| _ -> sf "L10845"
                    let kv = Aug_env(tyclass, bound_name, (*idl,*) (false(*correct?*), {g_blank_bindinfo with whom="decl_assign scone"}, EE_e(ty, vale)))
                    let decl = Bsv_varDecl(true, ty, bound_name, None, (lp, uid))
                    // As well as the new declaration here, which has sv=true since it is a struct vestigil, we also keep the child_decls.
                    // Only the interface declarations will be looked at in the main compile.
                    (kv::augs, decl::ccd, (ty, vale)::ccc)
                | ty when is_ifc_ty qm ty ->
                    let decl = Bsv_varDecl(false, ty, idl, None, (lp, uid))
                    let vale = B_ifc(PI_ifc_flat idl, ty)
                    let kv = Aug_env(tyclass, bound_name, (*idl,*) (false(*correct?*), {g_blank_bindinfo with whom="decl_assign B_ifc"}, EE_e(ty, vale)))
                    (kv::cca, decl::ccd, (ty, vale)::ccc)                    
                | _ ->
                    let decl = Bsv_varDecl(false, ty, idl, None, (lp, uid)) // We do not need decls for subsumed variables (ie those used only for elaboration, which is all of them for Bluespec). The imperative updates are symbolically elaborated as muts during module norm and their values used in actual parameters passed to register writes and other method calls. But this decl may also be an interface that is to be 'done' in some subsequent conditional assignments.  The now-deleted 'single reduction' parsing approach where it has to be the next line is countered by SmallExamples/ex_06_d where the 'second line' is an IF statement.
                    vprintln 3 (mf() + sprintf ": norm_decl_assign: Declared mutable scalar variable '%s' ty=%s (D4)" (hptos bound_name) (bsv_tnToStr ty))
                    let fer = funique ("mut-" + hptos bound_name) (*mutable_reference_key*)
                    // Rather than return Aug_mut, better to return an Aug_env of the B_ferref please in future edits. Then Aug_mut and related binding mechanisms can be deleted.
                    (Aug_mut(bound_name, idl, (ty, fer, lp))::cca, decl::ccd, (ty, B_ferref(ty, fer, lp))::ccc)


        match lhs with // For a declaration/definition, there is a small set of patterns allowable in terms of lhs forms (just identifiers and 1-D arrays?) whereas for an assignment, arbitrary amounts of subscripting are allowed on the lhs.
        | KB_id(name, (lp, decl_uid)) ->
            let ww = WF 3 qm ww (if vd>=4 then mf() else " decl id " + name)
            let idl = name::ee.prefix
            let abs_cp = extend_callpath bobj "L8917" decl_uid ee.callpath
            let lhs_ty = 
                match retrieve_type_annotation bobj mf "L8917" abs_cp with
                    | Tanno_function(lhs_ty, _, _, [(_, _)]) -> lhs_ty
                    | _ -> sf (mf() + sprintf ": expected decl assign type dropping at abs_cp= %A" abs_cp)                    

            decl_serf1 lp [] (name, idl, lhs_ty, decl_uid) (cca, ccd, ccc)

        | KB_subscripted(dotflag_, KB_id(name, (lp, decl_uid)), ast_subs, bitflag, (lp_, subsc_uid_)) ->     // Definition clause. Creates an array for which we (wrongly?) use the  Vector type.  Semantic question.
            //dev_println(sprintf "array definition: decl_uid=%A  ast=%A" decl_uid lhs)
            let subs = map (norm_bsv_exp_rmode ww bobj ee) ast_subs
            let ncp = extend_callpath bobj "L8918" decl_uid ee.callpath
            let subs_i = dims_decent ww bobj mf subs 
            let (lhs_ty, ct) =
                match retrieve_type_annotation bobj mf "subsc-L8918" ncp with
                    | Tanno_function(lhs_ty, _, _, [(_, ct)]) -> (lhs_ty, ct) 
                    | other -> sf (mf() + sprintf " other form retrieved %A" other)
            let idl = name::ee.prefix
            vprintln 3 (mf() + sprintf "norm_decl_assign: Defined array variable '%s' with content type=%s and full name=%s len=%s (D3)" name (bsv_tnToStr ct) (htosp idl) (sfold i2s [subs_i]))
            let (len_formal_name, ct_formal_name) =
                match lhs_ty with
                    | Bsv_ty_nom(_, F_vector, [a1;a2]) ->
                        let get_formal_name = function
                            | Tty(_, formal_name) -> formal_name
                            | Tid(_, formal_name) -> formal_name
                        (get_formal_name a1, get_formal_name a2)
                    | other ->
                        vprintln 0 (mf() + "+++use old route for vector formal names")
                        ("len8299", "ct8299")
            if false then
                vprintln 0 (sprintf "Muts entry 8299 bug investigate name=%s lhs %A local=%A" name lhs_ty (len_formal_name, ct_formal_name))
                let reportq k fv = vprintln 0 (sprintf "Muts entry 8299 bug investigate %A %A " k fv)
                for z in ee.muts do ignore(reportq z.Key z.Value) done     
                ()
                                    
            let ar_ty = Bsv_ty_nom({g_blank_nom_typ_ats with nomid=["Vector"] }, F_vector, [Tty(bintansi32 subs_i, len_formal_name); Tty(ct, ct_formal_name)])
            let fer = funique "ferV" // mutable_reference_key
            let kv = Aug_mut([name], idl, (ar_ty, fer, lp))
            ee.muts.add fer (ar_ty, nvector ee { iname=idl; ct=ct; len=subs_i; vt=ar_ty; data=NoEqMap.empty })
            let decl = Bsv_varDecl(false, ar_ty, idl, None, (lp, decl_uid))
           
            // Scone-like call for the contents is not needed here since the compile step will expand it.  But what about our next init step? And what makes functional/imperative?
            (kv::cca, decl::ccd, (ar_ty, B_ferref(ar_ty, fer, lp))::ccc) 

        | other -> sf (sprintf "norm_decl_assign: decl other %A" other)

    let ass_imp mf dmtof ee kvl (rhs) lhs =
        let rec ass_eval site arg =
            match arg with
            | KB_id(name, (lp, uid)) -> // This clause returns two more fields than the general clause. Some forms won't compile. TODO test/rationalise.
                match ee.genenv.TryFind [name] with
                    | None -> cleanexit(lpToStr0  lp + sprintf ": Variable '%s' is not declared when used as subscript base (L5e)" name)

                    | Some(GE_binding(bvl, whom_, EE_e(ty, vale))) ->
                        if bvl <> [name] then sf "bvl bound_name mismatch"
                        //println ("EARLT: ass_eval identifier EE_e site")
                        (ty, vale, Some [name], None)

                    | Some(GE_mut(bvl, ty, whom_, fer)) ->
                        let vale =
                            match ee.muts.lookup fer with
                                | Some(_, vale) ->vale
                                | None -> sf (lpToStr0 lp + sprintf ": ass_eval: Missing fer entry %A" fer)
                        //println ("EARLT: ass_eval identifier mut/fer site")
                        (ty, vale, Some [name], Some fer)

                    | other -> muddy (sprintf "ass_eval other bind %A" other)    

            | arg ->
                //sf (sprintf "ass_eval: site=%s: Unsupported from in ass_eval %A" site arg)
                let (res_ty, res_vale) = norm_bsv_exp_rmode_tyo ww bobj ee (None) (Prov_None "ass_eval") arg
                (res_ty, res_vale, None, None)


        let gival mf site subs =
            match  subs with
                | (ty_, vale) ->
                    let v = bsv_manifest ww bobj mf vale
                    (int)v // Could overflow!
                //| _ -> cleanexit(mf() + sprintf ": Integer vector subscript needed, not %s." (sfold (snd>>bsv_expToStr) subs))

        let rec ass_serf prefix lhs =  // TODO prefix not used and can be deleted?
            let vd = bobj.normalise_loglevel
            match lhs with
                | KB_tag(abase, tagv, (lp, uid)) -> // ass_serf (tagv::prefix) path 
                    let (lhs_base_ty, lhs_base, bound_name, fer_o) = ass_eval "ass_serf tag base" abase

                    // Nice code for inserting in an aggregate.
                    let aggr_tag_insert (B_aggr(sq1, TU_struct lst)) =
                        let rec scan_lst = function
                            | [] -> cleanexit(mf() + sprintf ": No field named '%s' in record '%s'" tagv (bsv_tnToStr lhs_base_ty))
                            | (tag, (tag_ty, w, pos, vale))::tt when tag=tagv -> tag_ty
                            | _::tt -> scan_lst tt
                        let tag_ty = scan_lst lst
                        let (rt, rhs') = norm_bsv_exp_rmode_tyo ww bobj ee (Some tag_ty) (Prov_None "ass_imp") rhs

                        let rec update_fields = function 
                            | [] -> cleanexit(mf() + sprintf ": No field named '%s' in record '%s' (Q1)" tagv (bsv_tnToStr lhs_base_ty))
                            | (tag, (ty, fw, pos, vale_))::tt when tag=tagv -> (tag, (ty, fw, pos, rhs')) :: tt
                            | xx :: tt ->  xx :: update_fields tt
                        (tag_ty, B_aggr(sq1, TU_struct(update_fields lst)))


                    // Other forms we treat as an opaque bit vector and do an insert under mask.
                    let rec misc_tag_insert lty ov =
                        match lty with
                        | Bsv_ty_nom(ats, F_struct(fields, None), args) ->
                            let (ty, _) = ensure_struct_details ww bobj "L10995" ee (fields, ats) lty
                            misc_tag_insert ty ov

                        | Bsv_ty_nom(ats0, F_struct(fields, Some(Bsv_struct(_, details))), args) ->                            
                            match op_assoc tagv details with
                                | None -> cleanexit(mf() + sprintf ": No field named '%s' in record '%s'. (Q2)" tagv (bsv_tnToStr lhs_base_ty))
                                | Some (tag_ty, fwidth, basepos) ->
                                    let (rt, rhs') = norm_bsv_exp_rmode_tyo ww bobj ee (Some tag_ty) (Prov_None "ass_imp") rhs
                                    //let nd = gen_B_shift(argv, gen_B_mask(fw, rhs')) // -ve means right shift.
                                    //let od = gen_B_antimask fw pos ov
                                    //(tag_ty, gen_B_bitor nd od)
                                    (tag_ty, B_bit_replace(lty, ov, rhs', fwidth, basepos, lp))
                        | other ->
                            sf (lpToStr0 lp + sprintf ": Unexpected lhs typr =%s for tag update. tag=%s" (bsv_tnToStr other) tagv) // Union case missing. TODO!

                    let (tag_ty, new_vale) = // tag_ty is not the type of new_vale!
                        match lhs_base with 
                            | B_aggr(sq1, TU_struct lst) -> aggr_tag_insert (B_aggr(sq1, TU_struct lst))
                            | other -> misc_tag_insert lhs_base_ty other
                            
                    let issuef = intermodule_issue_pred "tag-update" tag_ty
                    let whom = "ass_imp_tag"
#if DELETE_ME
                    let kvl__ = // new idiom
                        if nonep fer_o
                        then [Aug_env(tyclass, bound_name, (*idl,*) (dmtof, {g_blank_bindinfo with whom=whom}, EE_e(tag_ty, new_vale)))]
                        else
                            ee.muts.add (valOf fer_o) (tag_ty, new_vale)
                            []
#endif                        
                    let overload_flag = false // for now?
                    // Set issuef to issue the assign to compile time - perhaps a stupid approach.
                    let bn2 = if nonep bound_name then None else Some(tagv::(valOf bound_name))
                    (issuef, bn2, overload_flag, fer_o, whom, tag_ty, new_vale, lp)

                | KB_id(name, (lp, uid)) -> 
                    let ww = WF 3 qm ww (if vd>=4 then mf() else " assign id " + name)
                    let bound_name = name::prefix // prefix not used
                    match ee.genenv.TryFind bound_name with
                        | None -> cleanexit(mf() + sprintf ": Variable '%s' is not declared when assigned (L6). prefix=%s" name (hptos prefix))

                        | Some(GE_mut(idl_, ty, whom, fer)) ->
                            if vd>=4 then vprintln 4 (mf() +  sprintf "GE_mut lhs of assign ty=" + (bsv_tnToStr ty))                 
                            let whom = "as-id-mut"
                            let (rt, new_vale) = norm_bsv_exp_rmode_tyo ww bobj ee (Some ty) (Prov_None whom) rhs
                            (false, Some bound_name, false, Some fer, whom, rt, new_vale, lp)

                        | Some(GE_binding(_, whom, EE_e(ty, _))) ->
                            if vd>=4 then vprintln 0 (mf() +  sprintf "GE_binding lhs of assign ty=" + (bsv_tnToStr ty)) 
                            let whom = "as-id"
                            let (rt, new_vale) = norm_bsv_exp_rmode_tyo ww bobj ee (Some ty) (Prov_None whom) rhs
                            (false, Some bound_name, true, None, whom, rt, new_vale, lp)
                            
                        | Some(GE_overloadable(prototy, lst)) -> 
                            if vd>=4 then vprintln 4 (mf() +  sprintf "GE_overloadable of assign") 
                            cassert(not_nonep tyclass, "Overload defined for non-existent typeclass " + name)
                            let whom = "as-id-tyclass"
                            let (rt, new_vale) = norm_bsv_exp_rmode_tyo ww bobj ee (None) (Prov_None whom) rhs
                            let ty_o = None // tyo is None generally (always?) for tagged assigns.
                            (false, Some bound_name, true, None, whom, rt, new_vale, lp)

                        | other -> sf (mf() + sprintf ": ass_imp: other l-value iii %A" other)

                | KB_subscripted(dotflag_, abase, [bitpos], m_bitflag, (lp, uid)) when !m_bitflag -> // Single bit insert, becomes a 1-bit B_bit_replace.
                    let whom = "one-bit-bit-insert-parsed-as-subscription"
                    let (lhs_ty, lhs, bound_name, fer_o) = ass_eval whom abase
                    let isubs =
                        match norm_bsv_exp_rmode ww bobj ee bitpos with
                            | (ty_, vale) ->
                                let v = bsv_manifest ww bobj mf vale
                                (int)v // Could overflow!
#if OTHER
                        match getival mf [subs] with// TODO where is generic code to divert numeric types to values?  Not likely needed here.
                            | Some pos -> pos
                            | _ -> cleanexit(mf() + " bit insert location not compile-time constant. This is needed in this release.  subs=" + bsv_expToStr subs)
#endif
                    let ass_insert ee kvl (width, baser, rhs) (ty, _, lhs) =
                        let rt0 = if width < 2 then g_bool_ty else mk_bitty width//overkill?
                        let (rt, rhs') = norm_bsv_exp_rmode_tyo ww bobj ee (Some rt0) (Prov_None "ass_insert") rhs 
                        // Bluespec seems to support a pair of alternatives, as explained in ex_d, where the lhs can be a register or also a let-bound, bits-capable value. Actually, the two clauses are the same so this is a spurious distinction?
                        match lhs with
                            | B_applyf(PI_bno(B_dyn_b("_read", ifc), _), _, _, _) -> // First alternative if there is a register ifc here so we can do the write. TODO this should be generalised and less pattern based? Or is this a Bluespec quirk?
                                vprintln 3 (sprintf "Doing reg-oriented %s w=%i baser=%i lhs_ifc=%s rhs=%s" whom width baser (bsv_expToStr ifc) (bsv_expToStr rhs'))
                                let new_vale = B_bit_replace(lhs_ty, lhs, rhs', width, baser, lp)
                                (false, bound_name, false, fer_o, whom, rt0, new_vale, lp)
                            | other ->
                                vprintln 3 (sprintf "Doing second alternative %s w=%i baser=%i rhs=%s" whom width baser (bsv_expToStr rhs'))
                                let new_vale = B_bit_replace(lhs_ty, lhs, rhs', width, baser, lp)
                                (false, bound_name, false, fer_o, whom, rt0, new_vale, lp)


                    // TODO tyderef_o bty is used in smatch but here we use is_intlike - and we dont look at bitflag.
                    let width = 1 // Send round again: This subscription is actually a one-bit bit insert.
                    //let _ = KB_bitSelect(KB_id(name, (lp, g_null_uid)), subs, (lp, g_null_uid))
                    ass_insert ee kvl (width, isubs, rhs) (lhs_ty, "idl", lhs)

                | KB_subscripted(dotflag_, abase, ast_subs, m_bitflag, (lp, uid)) when not !m_bitflag ->    
                    let (lhs_base_ty, lhs_base, bound_name, fer_o) = ass_eval "ass_serf subscription base" abase
                    let subs = map (norm_bsv_exp_rmode ww bobj ee) ast_subs // Evaluate all the subscripts.
                    let (assign_ty, length_o) = array_deref mf lhs_base_ty
                    let (rt, rhs') = norm_bsv_exp_rmode_tyo ww bobj ee (Some assign_ty) (Prov_None "ass_imp") rhs                    // Evaluate rhs of assign. 

                    let ass_insert ee kvl (width, baser, rhs) (ty, idl, lhs') = muddy "ass insert reinstate please"   // Where is the test that faults on this please?

                    let ass_one ee xvl (subs, rhs') =
                        //vprintln 3 (qm + "assign one location of " + name)
                        match (lhs_base_ty, lhs_base) with
                            | (ty, B_vector(vs, _)) ->
                                let bv = valOf(ee.vectors.lookup vs) 
                                // Need to aug mut if fer non zero: the tailer will do that
                                // dmtf implied false on fer
                                let whom = "mishD3"
                                let rhs'' = nvector ee { (valOf(ee.vectors.lookup vs)) with data=bv.data.Add(subs, rhs') }
                                vprintln 3 (sprintf "Functional assign to vector %s[0..%i] at %i" (htosp bv.iname) (bv.len-1) subs)  // Semantic question: mutable or functional for Vectors?   It's not clear what level a vector is mutable at - please explain. 
                                (whom, ty, rhs'')::xvl
#if PERHAPS_NOT_NEEDED
                            | Some(GE_mut(_, _, whom_, fer)) ->
                                match ee.muts.lookup fer with
                                    | Some(aty, B_vector(vs, _)) ->
                                        let bv = valOf(ee.vectors.lookup vs)
                                        ee.muts.add fer (aty, nvector ee {(valOf(ee.vectors.lookup vs)) with data=bv.data.Add(subs, rhs') })
                                        vprintln 3 (sprintf "Mutable assign to vector %s[0..%i] at %i" (htosp bv.iname) (bv.len-1) subs)
                                        kvl
                                    | other -> sf (mf() + sprintf ": other mutable %A" other)
#endif
                            | other -> sf (mf() + sprintf ": other ass_one lhs %A" other)


                    // Set up all entries in an array.  Which test uses this please?  Code not called after recent edits!
                    let ass_all ee kvl subs arg =
                        vprintln 3 (qm + "assign all locations")
                        muddy "Find which test hits this please!"
                        let rec init_a_dim = function
                            | [subs1] ->
                                match arg with 
                                | KB_bitConcat(lst, lp) -> // The array init parses as a bitConcat.
                                    let l = length lst
                                    if l < subs1 then vprintln 1 (lpToStr0 lp + sprintf ": Array initialisation is shorter than array %s" shortname)
                                    if l > subs1 then vprintln 1 (lpToStr0 lp + sprintf ": Array initialisation is longer than array %s" shortname)
                                    let ass_all_step kvl (rv, n) = if n >= subs1 then kvl else ass_one ee kvl (n, rv)
                                    let lst = muddy "map (norm_bsv_exp_rmode_tyo ww bobj ee (Some assign_ty) (Prov_None \"ass-imp-array-item\") lst"
                                    List.fold ass_all_step kvl (zipWithIndex lst)
                                | other -> sf(sprintf "Unsuitable expression for complete array initialisation %A" other)
                            | h::tt -> muddy "2-D array init or higher ..."
                        init_a_dim subs


                    let isubs = gival mf "single-bit-insert" (hd subs)
                    let ans = ass_one ee [] (isubs, rhs')
                    if length ans <> 1 then muddy "ass_all multi-assign reinstate"
                    let (whom, ty, rhs') = hd ans
                    let issuef = intermodule_issue_pred "array-update" assign_ty
                    if issuef then sf "Bad issue subscripted" // This issue is the wrong lhs - subscript ignored - TODO fix boundname at least...
                    (issuef, bound_name, false, None, whom, lhs_base_ty, rhs', lp)

                | KB_bitRange2(lhs0, ll, rr, (lp, _)) -> 
                    let whom = "ass_serf bitRange2"
                    let (lhs_base_ty, lhs_base, bound_name, fer_o) = ass_eval whom lhs0
                    let ll  = norm_bsv_exp_rmode ww bobj ee ll
                    let rr = norm_bsv_exp_rmode ww bobj ee rr 
                    let (rt, rhs') = norm_bsv_exp_rmode_tyo ww bobj ee (Some lhs_base_ty) (Prov_None whom) rhs                    // Evaluate rhs of assign.   lhs_base_ty may not be right. None could be better for call and ...
                    let (ll, rr) = (gival mf whom ll, gival mf whom rr)
                    let width = abs(ll-rr)+1
                    let baser = min ll rr
                    let new_vale = B_bit_replace(lhs_base_ty, lhs_base, rhs', width, baser, lp)
                    (false, bound_name, false, fer_o, whom, lhs_base_ty, new_vale, lp)

                | other -> sf (sprintf "norm_decl_assign: assign other form %A" other)

        let (issuef, bound_name, overload_flag, fer_o, whom, rty, new_vale, lp) = ass_serf [] lhs

        let hptos_o arg = if nonep arg then "NONE" else hptos(valOf arg)
        vprintln 3 (lpToStr0 lp + sprintf ": Norm assign part of declAssign. %s bound_name=%s type=%s issuef=%A mutable_fer=%A overload_flag=%A" whom (hptos_o bound_name) (bsv_tnToStr rty) (not_nonep fer_o) (overload_flag) (issuef))
        // This code so far is all about elab env: what about WIP commented out array code?
        // For normal subscripted lhs operators, would need to eval the base and index and then make the assign. Semantic question: is this an elab eval or rutime eval?
        let issue rhs =
            if issuef && not_nonep bound_name then
                let borrowed_flag = true  // This indicates a borrowed interface.
                let x = Bsv_varAssign(borrowed_flag, valOf bound_name, rhs, whom, lp) //  The borrowed flag needs to be in the lifter shim type of the IIS to bexp_t
                dev_println(sprintf "Issue Bsv_varAssign %s" (bsv_stmtToStr x))
                [x]
            else []

        match (overload_flag, fer_o) with 
            | (true, None) ->
                let tyclass_ids = digest_cooked mf rty // For a scalar there is just one type, but put as list for compatiblity with function arg list signature.
                //vprintln 3 (sprintf ": %s: %s Set variable %s to %s (D4a)" (mf()) whom (hptos_o bound_name) (bsv_expToStr new_vale))
                //let nv = GE_overloadable(prototy, ([tyclass_ids], (rt, new_vale))::lst
                if nonep bound_name then (ee, kvl, issue new_vale)
                else
                    let kv = Aug_env(tyclass, valOf bound_name, (dmtof, {g_blank_bindinfo with whom=whom }, EE_e(rty, new_vale)))
                    (ee, kv::kvl, issue new_vale)

            | (false, None) ->
                if nonep bound_name then (ee, kvl, issue new_vale)            
                else 
                    let kv = Aug_env(tyclass, valOf bound_name, (*idl,*) (dmtof, {g_blank_bindinfo with whom=whom }, EE_e(rty, new_vale)))
                    //vprintln 3 (sprintf ": %s: %s Set variable %s to %s (D4b)" (mf()) whom (hptos_o bound_name) (bsv_expToStr new_vale))
                    //vprintln 0 (sprintf "%s: +++ no ee update for set variable %s to %s (D4b)" mm (htosp bvl) (bsv_expToStr new_vale))
                    (ee, kv::kvl, issue new_vale)

            | (false, Some fer) ->
                ee.muts.add fer (rty, new_vale)
                vprintln 3 (sprintf ": %s: Set variable %s to %s (D4c)" (mf()) (hptos_o bound_name) (bsv_expToStr new_vale))
                (ee, kvl, issue new_vale)
            | _ -> sf(mf() + " other")

    let ndas1 ww lp ty_o lhs rhs_o shortname = 
        let mf() = lpToStr0 lp + " norm varDeclAssign + " + shortname + (if nonep ty_o then " (no decl) " elif nonep rhs_o then " (no assign) " else "") + (if ee.fbody_context then "( - decl inside function body - value will not persist between activations)" else "")

        let (ee, cc, kvl1) = // Declaration part, present when tyo<>None.
            match ty_o with
                | None -> (ee, cc, [])
                | Some ast_ty_ ->
                    let (kvl, n, fer_) = decl_serf0 mf ee lhs ([], [], [])
                    let ee = List.fold (augment_env_entry ww bobj mf "norm_decl_assign-L10686") ee kvl  // We augment env as well as return the decl kv.
                    (ee, n @ cc, kvl)                

        if not_nonep ty_o then vprintln 3 (sprintf "norm varDeclAssign %s: decl part complete. Assign part present=%A" shortname (not_nonep rhs_o))
        let (ee, cc, kvl2, assigns) = // Assignment part, present when rhs_o<>None.
            let dmtof = nonep ty_o // Allowed to redefine value if this not its initial declaration. dmtof='define more than once flag'
            match rhs_o with
            | None -> (ee, cc, [], [])
            | Some rhs ->
                let (ee2, kvl2, assigns) = ass_imp mf dmtof ee [] rhs lhs
                //let augment (em:genenv_map_t) (k,v) = em.Add(k, v)
                //Note: being applied to ee here and also returned for later - which used depends on callers needs.
                (List.fold (augment_env_entry ww bobj mf "norm_decl_assign-L10876") ee2 (kvl1@kvl2), cc, kvl2, assigns)
        (ee, assigns@cc, kvl1@kvl2)

    let ans =
        match arg0 with // norm_decl_assign main dispatch.
            | KB_varDeclAssign(ty_o, (KB_id(name, (lp, uid)) as lhs), rhs_o, lp_) ->
                let ww = WF 3 "norm_decl_assign" ww ("scalar variable " + name)
                ndas1 ww lp ty_o lhs rhs_o name

            | KB_varDeclAssign(None, ((KB_tag(path, tagv, (lp, uid))) as tpath), rhs_o, lp_) -> 
                let mf() = lpToStr0 lp + " norm1 varDeclAssign tag/dot path form + " + tagv
                let (shortname, idl, _, path_uid, lp_) = lhs_path_shortname ww mf tpath
                let ww = WF 3 "norm_decl_assign" ww (if vd>=4 then mf() else " -- tagged assign -- ")
                ndas1 ww lp None tpath rhs_o (hptos idl)

            | KB_varDeclAssign(ty_o, Let ("bitRange2", false) ((tok, dotflag), ((KB_bitRange2(lhs, _, _, (lp, _))) as lval)), rhs_o, _) 
            //KB_varDeclAssign(ty_o, Let "bitSelect"           (tok, ((KB_bitSelect(lhs, _, (lp, _))) as lval)), rhs_o, _)
            | KB_varDeclAssign(ty_o, Let "[]"                  (tok, ((KB_subscripted(dotflag, lhs, _, _, (lp, _))) as lval)), rhs_o, _)  -> 
                let mf() = lpToStr0 lp + " norm1 varDeclAssign " + tok
                let (shortname, idl, _, path_uid, lp_) = lhs_path_shortname ww mf lval
                let mf() = lpToStr0 lp + " norm1 varDeclAssign " + tok + " " + shortname
                let ww = WF 3 "norm_decl_assign" ww (if vd>=4 then mf() else tok + " " + shortname)
                ndas1 ww lp ty_o lval rhs_o shortname

            | other -> sf (lpToStr0 lp + sprintf ": norm_decl_assign other %A" other) // end of norm_decl_assign
    let _ = WF 3 qm ww  ("finished " + shortname)
    ans
                                           

and ensure_tunion_details ww bobj site ee ((mrecflag, fields, idl) as arg) sty =
    let vd = 3
    //vprintln 0 ("Ensure_tunion_details of " + bsv_tnToStr sty)
    let rec etd = function
        | Bsv_ty_nom(ats, F_actionValue ty, args) -> etd ty // Unfortunate we need to do this here ... please refactor
        | Bsv_ty_knot(idl, kk) when not(nonep !kk) -> etd (valOf !kk)
        | Bsv_ty_nom(ats, F_tunion(mrecflag, variants, Some details), args) -> (sty, details)

        | Bsv_ty_nom(ats, F_tunion(mrecflag, variants, None), args) ->

            let ww = WF 3 "ensure_tunion_details" ww (sprintf "start. site=%s Recursion flag is initially %A for %s" site !mrecflag (htosp ats.nomid))
            let mf() = "ensure_tunion_details tagged union "+ htosp ats.nomid
            let ww = WF 3 "ensure_tunion_details " ww (mf())
            //vprintln 0 (mf() + " bb " + bsv_tnToStr sty)
            if !mrecflag = MR_unset then mrecflag := MR_pending
            let inners = ref []
            let arity = length variants
            let tag_width = bound_log2(BigInteger arity)
            let data_width  = function
                | (_, None)   -> Wnone
                | (id, Some x) ->
                    match enc_width_ev ww ee mf x with
                    //if (not recflag) && (w=None || w = Some 0) then sf ("zero width union field not good " + id)
                    //| Wsome w -> w
                    | z -> z
            let sq c = function
                | Wnone -> c
                | Wknot _ -> (mrecflag := MR_recursive; c)
                | Wsome a -> if a>c then a else c
            let width =
                if !mrecflag=MR_recursive then -1 // Recursive union has no width.
                else 
                    let widths = map data_width variants
                    tag_width + List.fold sq 0 widths
            let mapping = zipWithIndex variants
            let variants' = map (fun ((tag, ty), no) -> (tag, (ty, no))) mapping
            let tab = create_table(sprintf "Tagged union (w=%i,tw=%i,a=%i)" width tag_width arity + (if !mrecflag=MR_pending then "(pending) " elif !mrecflag=MR_recursive then "(recursive) " else sprintf "(total width %i) " width) + htosp ats.nomid, ["field"; "type"; "tag value"], map (fun (tag, (ty, no)) -> [ tag; (if nonep ty then "-" else bsv_tnToStr(valOf ty)); i2s no ]) variants')
            mutadd bobj.m_tables (mf(), tab)
            if vd >= 2 then app (vprintln 2) tab
            //println (sprintf "Finishing tunion %s, flag is %A"  (htosp ats.nomid) !mrecflag)
            if !mrecflag=MR_pending then
                mrecflag := MR_linear
            let width = if !mrecflag=MR_recursive then -1 else width
            let msg = (sprintf "Finished tunion %s, recursion flag is now %A, width=%i"  (htosp ats.nomid) !mrecflag width)
            vprintln 3 msg
            let details = Bsv_tunion(variants', (width, tag_width, arity, !mrecflag=MR_recursive), mapping)
            //let ww = WF 0 "ensure_tunion_details" ww msg
            (Bsv_ty_nom(idl, F_tunion(mrecflag, variants, Some details), args), details)
        | tother -> sf (sprintf "ensure_tunion_details other %A" tother)
    etd sty

// Some recursive Bluespec structs, such as List.bsv, have variable and potentially infinite bit width and so cannot be used in rendered hardware. They are only for
// elaboration.  The details field cannot be filled in for such types.  Such a struct is always inside a union, and we check for the recursion in the union code.
and ensure_struct_details ww bobj site ee ((fields, ats0) as arg) sty =
    let vd = 3
    let ww = WF 3 "ensure_struct_details" ww ("start on " + bsv_tnToStr sty + " from " + site)
    let recf = false
    let rec esd aa =
        //let ww = WF 0 "ensure_struct_details" ww ("iterate on " + bsv_tnToStr sty)
        match aa with
        | Bsv_ty_nom(ats, F_actionValue ty, args) -> esd ty // Unfortunate we need to do this here ... please re-factor.
        | Bsv_ty_knot(idl, kk) when not(nonep !kk) -> esd (valOf !kk)
        | Bsv_ty_nom(ats, F_struct(fields, Some details), args) -> (sty, details)
        | Bsv_ty_nom(ats, F_struct(fields, None), args) ->
            let mf() = "ensure_struct_details/bld_stunion_type " + htosp ats.nomid
            let ww = WF 3 "ensure_struct_details" ww "start"
            //vprintln 0 (mf() + sprintf ": %s %A" (bsv_tnToStr aa) aa)
            let m_mrecflag = ref false
            let shot = function
                | Wnone -> 0
                | Wknot _ -> (m_mrecflag := true; 0)
                | Wsome x -> x

            match arg with
            | (fields, nomid) ->
                let ffld (id, ty) = 
                    let w = (if recf then Wnone else enc_width_ev ww ee mf ty) // recf always false when tested?
                    if vd>=6 then vprintln 6 (sprintf "ensure_struct_details  recf=%A w=%A for %s" recf w (bsv_tnToStr ty))
                    (id, (ty, shot w))
                let fields' = map ffld fields
                let rec data_width = function
                    | [] -> (0, [])
                    | (id, (ty, w))::tt ->
                        let (c, more) = data_width tt
                        (c+w, (id, (ty, w, c)) :: more) 
                let (width, fields') = data_width fields'
                let recf = recf || !m_mrecflag // Do we still need this mutual recusion check?
                let tab = create_table("Structure " + (if recf then "(recursive) " else sprintf "(total width %i) " width) + htosp nomid.nomid, ["field"; "type"; "width"; "position"], map (fun (id, (p, width, pos)) -> [ id; bsv_tnToStr p; i2s width; i2s pos ]) fields')
                let width = if recf then -1 else width
                mutadd bobj.m_tables (mf(), tab)
                if vd >=2 then app (vprintln 2) tab
                let arity = length fields
                let details = Bsv_struct((width, arity, recf), fields')
                (Bsv_ty_nom(ats0, F_struct(fields, Some details), args) , details)
        | tother -> sf (sprintf "ensure_struct_details unexpected type: " + (bsv_tnToStr tother))
    let ans = esd sty
    //let ww = WF 0 "ensure_struct_details" ww ("end of " + bsv_tnToStr sty)
    ans
    
and norm_bsv_bev ww bobj rtyo (ee, cc) aa =
    let vd = bobj.normalise_loglevel
    let tyclass = None
    let env_ = []
    let qm = "norm_bsv_bev" // if ee.forcef then "norm_bsv_bev-e" else "norm_bsv_bev-l" // TODO delete forcef
    match aa with
    //KB_varDeclAssign_raw _
    | KB_regWrite_raw _    -> norm_bsv_bev ww bobj rtyo (ee, cc) (desugar_varDeclAssign ww qm aa)

    | KB_attribute(pragmats, xx, lp) -> // Bounded scope override of attributes for the next bev statement only.
        let pragmats = ins_ats bobj lp  "attribute" bobj.prags [] pragmats // retrieve on definition path.
        let ee = { ee with attribute_settings=refine_attribute_settings ee ee.attribute_settings pragmats; }
        norm_bsv_bev ww bobj rtyo (ee, cc) xx 

    | KB_while(grd, body, lp) ->
        let mf() = lpToStr0 lp + " " + htosp ee.static_path + " while loop "
        let ww = WF 3 qm ww (mf())
        let rec monkey ww = function // DONT need monkey
            | other when bsv_const_bexp ww mf other -> bsv_bmanifest ww bobj mf other // monkey (B_bexp(bsv_blower ww other))
            | other ->
                let em = sprintf ": compile-time-constant while guard needed, not %s" (bsv_bexpToStr other)
                let _ = whereami_del em (vprintln 0) ww 
                cleanexit(mf() + em)
        let rec unwind_while ee cc lim = 
            let g = norm_bsv_bexp_rm ww bobj { ee with forcef=Some true; } grd
            if lim <= 0 then cleanexit(mf() + sprintf ": Too many unwind attempts. L9877. Please increase whilefor limit (-bsv-whilefor-limit=n). Current value is %i." bobj.whilefor_limit)
            elif monkey ww g
            then
                //vprintln 0 (mf() + " step on body. limit=" + i2s lim)
                let (ee, cc) = norm_bsv_bev ww bobj rtyo (ee, cc) (body)
                unwind_while ee cc (lim-1) //TODO HMMMM need a rev here?
            else (ee, cc)
        unwind_while ee cc bobj.whilefor_limit

    | KB_for(s1, g, s2, body, lp) ->
        let (mm, ww, expanded) = prep_for ww qm aa
        norm_bsv_bev ww bobj rtyo (ee, cc) (expanded)

    | KB_functionDef3(proto, assf(*true*), body, nameo, lp) ->
        let mf() = lpToStr0 lp + " bev functionDef3a (assignment form generally)"
        let ww = WF 3 qm ww (if vd>=4 then mf() else " -- ")
        let ats = []
        let (ifc_sty, tyclass) = (None, None)
        let (kv, _, name) = norm_functionDef ww bobj tyclass mf (ee, []) ifc_sty (KB_functionDef3(proto, assf, body, nameo, lp), None)
        if nameo<>None && valOf nameo <> name then cleanexit(mf() + ": end name does not match start: " + name + " cf " + valOf nameo) // Cannot really happen for the assignment form.
        (augment_env_entry ww bobj mf "functionDef3a" ee kv, cc)


    | KB_varDeclAssign(None, lsh, None, lp) -> muddy "unusual easc form"
        
    | KB_varDeclAssign(tyo, lsh, ro, lp) -> // norm bev
        let mf() = lpToStr0 lp + " bev varDeclAssign"
        let ww = WF 3 qm ww (if vd>=4 then mf() else " -- ")
        let (b1, b2, kvl_) = norm_decl_assign ww bobj (mf(*qm + ":" + *)) tyclass (ee, cc) aa
        (b1, b2)

    | KB_letBinding(id, r, operator, (lp, uid)) -> // norm bev
        // Simple synonym for decl and thence assign. 
        let mf() = lpToStr0 lp + " " + htosp ee.static_path + sprintf ": letBind (bev) bv=%s, operator is '%s' " id operator
        let bound_name = [id]
        let ww = WF 3 qm ww (if vd>=4 then mf() else "letBind " + id)
        let (sty_, r') = norm_bsv_exp_rmode ww bobj ee r
        let sty = simple_retrieve ww bobj mf "L8463" ee uid
        dev_println (mf() + " retrieved type " + bsv_tnToStr sty)
        let rt =
            match operator with
            | "<-" ->
                match sty with
                    | Bsv_ty_nom(_, F_actionValue av_ty, _) ->  av_ty
                    | sty ->
                        vprintln 3 (mf()  + "+++ expected to have to process an actionvalue wrapper here, not " + bsv_tnToStr sty)
                        sty
                    
            | "=" ->  sty
            | other -> sf(mf() + ": bad let operator: " + operator)
        
        let kv = (bound_name, GE_binding(bound_name, {g_blank_bindinfo with whom=operator}, EE_e(rt, r')))
        if vd>=4 then vprintln 4 (mf() + " type=" + bsv_tnToStr rt + " and value=" + bsv_expToStr r')
        let ee' = { ee with genenv= ee.genenv.Add kv }
        (ee', cc)

    | KB_regWrite(path, r, (lp, uid)) -> // bev: This is converted to the _write method call call but only after checking whether a B_bit_replace for bit insertion.
        let (nameo, mf) =
            match path with
                | KB_id(s,_)                                     -> (Some s, fun () -> lpToStr0 lp + sprintf " regWrite (bev) '%s' " s)
                | KB_subscripted(dotflag, KB_id(s, _), _, _, _)  -> (Some s, fun () -> lpToStr0 lp + sprintf " regWrite (bev, subscripted) '%s' " s)
                | KB_bitRange2(KB_id(s, _), _, _, _)             -> (Some s, fun () -> lpToStr0 lp + sprintf " regWrite (bev, bit insert) '%s' " s) // Handle on path please
                | other      -> (None, fun () -> lpToStr0 lp + " regWrite (bev, non-scalar)")
        let ww = if vd>=4 then WF 4 qm ww (mf()) else ww

        // Disambiguate bit inserts crudely - pehaps no hybrid subscriptions and inserts at the moment.  We need some tests for bitinserts and extracts.
        let bitinsert = 
            match path with
                | KB_subscripted(dotflag, ((KB_id(s, lp)) as p), subs, bitflag, _)  ->
                    match ee.genenv.TryFind[s] with
                    | None -> sf (sprintf "Variable '%s' is not declared when assigned (L5a)" s)
                    //| Some(GE_binding(idl, _, EE_e(ty, lhs'))) when is_intlike_type mm ty ->
                    | Some(GE_binding(_, _, EE_e(((Bsv_ty_nom _) as ty), lhs')))->
                        let subs' = dims_decent ww bobj mf (map (norm_bsv_exp_rmode ww bobj ee) subs) // 1-D array or bitSelect or bitRange only - no hybrids currently supported               
                        Some (p, 1, subs')
                    | other ->
                        //vprintln 0 (mf() + sprintf ": not a bitinsert lhs %A" other)
                        None

                | KB_bitRange2(((KB_id(s, _)) as p), ll, rr, _) ->
                    let (_, ll) = norm_bsv_exp_rmode ww bobj ee ll
                    let (_, rr) = norm_bsv_exp_rmode ww bobj ee rr
                    if bsv_const_exp ww mf ll && bsv_const_exp ww mf rr then
                        let ll = bsv_manifest_32 ww bobj (fun()->"bitRange2") ll
                        let rr = bsv_manifest_32 ww bobj (fun()->"bitRange2") rr
                        Some(p, abs(rr - ll) + 1,  min ll rr)
                    else cleanexit(mf() + ": dynamic bit range")
                | other      -> None
        let mf() = if nonep bitinsert then mf() + " fullwidth" else mf() + " bitinsert"
        let ww = if vd >= 4 then WF 4 "pos" ww (mf() + " check regwrite") else ww
        let (lvv, rt, rv) =
            match bitinsert with
            | None ->
                let (lhs_ty, lvv) =
                    // We want the rmode eval of the path base but with asReg applied
                    match norm_bsv_exp_lmode_tyo ww bobj ee (None) (Prov_asReg_asIfc "bitinsert-L10948") path with
                        | (rt, _, rv) -> (rt, rv)
                // TODO abstract this!
                let write_ty =
                    match norm_find_method mf g_swrite lhs_ty with
                        | Bsv_ty_nom(_, F_fun(_, matts, args), _) -> 
                            if length args <> 1 then cleanexit(mf() + sprintf ": the '%s' method takes more than one arg" g_swrite)
                            hd args
                        | Bsv_ty_methodProto(methodname, prats, [], proto, Bsv_oty_fun(sty_, _, _, args, provisos, lp_)) ->
                            if length args <> 1 then cleanexit(mf() + sprintf ": the '%s' method takes more than one arg" g_swrite)
                            f2o3(hd args)
                let (rt, rv) = norm_bsv_exp_rmode_tyo ww bobj ee (Some write_ty) (Prov_None "bit-rt") r
                (lvv, rt, rv)

            | Some (p, width, baser) ->
                let rt0 = if width < 2 then g_bool_ty else mk_bitty width//overkill?
                let (rt, rv) = norm_bsv_exp_rmode_tyo ww bobj ee (Some rt0) (Prov_None "bitinsert-L8978") r
                let (lhs_ty, lvv) =
                    match norm_bsv_exp_lmode_tyo ww bobj ee (None) (Prov_asReg_asIfc "bitinsert-L8980") p with
                        | (rt, None, vv) -> cleanexit(mf() + ": lhs is unsuitable for register write or bit insert")
                        | (rt, Some(lt, lv), rv) -> (lt, lv) 
                vprintln 0 (mf() + "reg bitinsert lhs_ty=" + bsv_tnToStr lhs_ty + "\nlhs=" + bsv_expToStr lvv)

                //We may need to mine for a source of ov deep inside nv to avoid double read ... but actually if always ready not a problem...
                let ov = B_applyf(PI_bno(B_dyn_b(g_sread, lvv), None), Bsv_ty_dontcare "L8145", [], tag_this_lp lp)
                let nv = B_bit_replace(lhs_ty, ov, rv, width, baser, lp)
                (lvv, rt, nv)

        let ww = if vd>=4 then WF 4 "pos" ww (mf() + " finished") else ww
        let r = Bsv_eascActionStmt(B_applyf(PI_bno(B_dyn_b(g_swrite, lvv), None), g_action_ty, [(rt, rv)], tag_this_lp lp), None)
        // assert vt = false - write to let-bound variable                
        //vprintln 0 (mf() + ": normed to " + bsv_stmtToStr r)
        (ee, r ::cc)

    | KB_block(blockname, lst, nameo, lp) ->
        let mf() = lpToStr0 lp + ": bev blockStmt " + valOf_or blockname "anon"
        let ww = if vd>=4 then WF 4 qm ww (mf()) else ww
        if nameo<>None && blockname<>None && valOf nameo <> valOf blockname then cleanexit(mf() + ": end name does not match start: " + valOf blockname + " cf " + valOf nameo)
        List.fold (norm_bsv_bev ww bobj rtyo) (ee, cc) (lst) // cc is still reversed

            
    | KB_if(g, tt, ffo, lp) ->
        let mf() = lpToStr0 lp + " " + htosp ee.static_path + ": bev ifStmt"
        let ww = if vd>=4 then WF 4 qm ww (mf()) else ww
        let g' = norm_bsv_bexp_rm ww bobj ee g
        let cf = bsv_const_bexp ww mf g'
        // We can either record split information inside the 'If' statement and compile it with non-strict semantics (ie more-or-less as though split) or we can apply the explict_rule_splitter to a rule and then compile the shards as normal.
        
        let strictf = not ee.attribute_settings.split_default // Default 'if' statement semantic is no case split hence strict.
        match g' with
            | x when cf ->
                let x = bsv_bmanifest ww bobj mf g'
               
                vprintln (if bobj.wverbosef then 0 else 4) (mf() + sprintf ": manifest if-condition -> %s -> %A "  (bsv_bexpToStr g') (x))
                if x then norm_bsv_bev ww bobj rtyo (ee, cc) (tt)
                elif ffo<>None then norm_bsv_bev ww bobj rtyo (ee, cc) (valOf ffo)
                else (ee, cc)
            | e' ->
               vprintln (if bobj.wverbosef then 0 else 4) (mf() + sprintf ": non-manifest if-condition -> %s "  (bsv_bexpToStr g'))
               let conv e x = bsv_mkBlock lp (rev(snd(norm_bsv_bev ww bobj rtyo (e, []) x)))
               let (e_tt, e_ff) = ee_clone ee
               let t = Bsv_ifThenStmt(e', conv e_tt tt, (if ffo=None then None else Some(conv e_ff (valOf ffo))), strictf, lp)::cc
               (ee_mutmux ww bobj e' e_tt e_ff, t)
        
    | KB_case(gexp, arms, lp) ->  // bev
        let mf() = "normbev " + lpToStr0 lp + " " + htosp ee.static_path + ": caseStmt"
        let ww = WF 3 qm ww (if vd>=4 then mf() else "KB_case...")
        let (ety, e') = norm_bsv_exp_rmode ww bobj ee gexp

        // With new typechecker et returned can  be ignored and each tag can perhaps have its own dropping to be retrieved? Perhaps recode that way.
        // A case statement is better called a match-with-bindings: it binds parameter names to values from the guard expression. 
        let rec mkbindings ww stk (g0, cc) (ty, x) e' =
            dev_println ("mkbindings recurser  " + sfold id stk)
            match (ty, x) with 
                | (Bsv_ty_knot(hidl, k2), x) when not(nonep !k2) -> mkbindings ww stk (g0, cc) (valOf !k2, x) e'

                | (Bsv_ty_nom(_, F_actionValue ty, _), x) ->  mkbindings (WN "F_actionValue" ww) ("F_actionValue"::stk) (g0, cc) (ty, x) e'
                 
                | (Bsv_ty_nom(ats, F_enum(miu, variants), []), KB_constantAlias(tag, lp)) ->
                     let no =
                         match op_assoc tag variants with
                             | None -> cleanexit(mf() + sprintf "value '%s' is not a member of enum %s" tag (htosp ats.nomid))
                             | Some no -> no
                     let g' = gen_B_and[g0; gen_B_deqd ww bobj (B_hexp(ty, xi_num no), e')]
                     (g', cc)

                     
                | (Bsv_ty_nom(nst, F_tunion(mrecflag, variants, None), args), x) ->
                     let recf = ref !mrecflag // why passed in separately?
                     let (ty, _) = ensure_tunion_details ww bobj "case-mkbindings" ee (recf, variants, nst) ty
                     mkbindings (WN "F_tunion" ww) ("F_tunion"::stk)  (g0, cc) (ty, x) e'

                | (Bsv_ty_nom(ats, F_struct(fields, None), args), x) ->
                     let (ty, _) = ensure_struct_details ww bobj "mkbindings-L11504" ee (fields, ats) ty
                     mkbindings (WN "F_struct" ww) ("F_struct"::stk) (g0, cc) (ty, x) e'

                | (Bsv_ty_nom(ats, F_struct(fields, Some(Bsv_struct((width, arity, recf), fields'))), args), KB_tuplePattern(strlst, (lp, uid))) ->
                      let mf() = lpToStr0 lp + ": tuplePattern"
                      let ww = if vd>=4 then WF 4 qm ww (mf()) else ww
                      let il = length strlst
                      if il<>arity then cleanexit(mf() + " " + sprintf ": wrong number of tuple pattern fields for '%s' %i cf %i" (htosp ats.nomid) arity il)
                      let struct_pattern_positional_bindf = function
                         | ((tag, (ty, w, pos)), KB_dot bv) -> // Formal name is bound here.
                             // Could be a letbind?  TODO consider for subx sharing.
                             let (ty_, e) = gen_B_field_select ww bobj [ uid ] (tag, e', w, pos)
                             let n = (bv, GE_binding([bv], { g_blank_bindinfo with whom="mish2"}, EE_e(ty, e)))
                             //let ww = WF 3 mm ww ("Created struct case/match binding of " + tag)
                             (B_and[], [n]) 
                         | ((tag, (ty, w, pos)), q (*KB_taggedUnion*)) ->
                             let (ty_, e) = gen_B_field_select ww bobj [ uid ] (tag, e', w, pos)
                             mkbindings (WN "tuplePattern" ww) ("tuplePattern"::stk) (B_and[], []) (ty, q) e

                         //((tag, (ty, w, pos)), q) -> sf(sprintf "struct_pattern_positional_bindf tagty=%s  other %A" (bsv_tnToStr ty) q)
                      let (gdlst, nbs) = List.unzip(map struct_pattern_positional_bindf (List.zip fields' strlst))
                      (gen_B_and gdlst, (list_flatten nbs) @ cc)

                //| (Bsv_ty_nom(ats, F_struct(fields, Some(Bsv_struct((width, arity, recf), fields'))), args), KB_structPattern(strlst, (lp, uid))) ->
                | (ty, KB_structPattern(strlst, (lp, uid))) ->                     
                    let mf() = lpToStr0 lp + ": structPattern"
                    let (nst, fields') = 
                        let ety =
                            let _ = simple_retrieve ww bobj mf "L9675" ee uid  // This is a bad dropping - do not drop or use.
                            ty
                        let rec scrip = function
                            | Bsv_ty_nom(nst, F_struct(fields, Some(Bsv_struct((width, arity, recf), fields'))), args) -> (nst, fields')
                            | Bsv_ty_nom(nst, F_struct(fields, None), args) ->
                                let (ty, _) = ensure_struct_details ww bobj "structPattern" ee (fields, nst) ty
                                scrip ty

                            | other -> sf (sprintf "F9688 dropping ty=%s" (bsv_tnToStr other))
                        scrip ety

                    let bind cc = function
                         | (tag, KB_dot bv) ->  // A formal name is here introduced and bound. 
                             let vo = op_assoc tag fields'
                             if nonep vo then cleanexit(mf() + sprintf ": no such tag '%s' in structure '%s'" tag (htosp nst.nomid))
                             let (ty, w, pos) = valOf vo
                             let ww = WF 3 "bind-L11095" ww ("Created struct case/match binding of " + tag)
                             let (ty_, e) = gen_B_field_select ww bobj [ uid ](tag, e', w, pos)
                             let n = (bv, GE_binding([bv], { g_blank_bindinfo with whom="mish3"}, EE_e(ty, e)))
                             n :: cc
                         | (tag, other) ->  muddy ("complex? recursive nesting of structures and unions: " + htosp nst.nomid)
                    let cc' = List.fold bind cc strlst
                    (g0, cc')
                    
                | (ty, KB_taggedUnionExpr(vtag, _, inner, (lp, uid))) 
                | (ty, KB_taggedUnion(vtag, _, inner, (lp, uid))) ->
                    let qmm = ": caseStmt, tagged union"
                    let mf() = "normbev " + lpToStr0 lp + " " + htosp ee.static_path + qmm

                    let (width, tag_width, variants', nst) = 
                        let ety =
                            let _ = simple_retrieve ww bobj mf "L9692" ee uid  // This is a bad dropping - do not drop or use.
                            dev_println(sprintf "%s  cf %s  against %s" qmm (bsv_tnToStr ty) (bsv_tnToStr ety))
                            ty
                            
                        let rec scrop = function
                            | Bsv_ty_nom(nst, F_tunion(mrecflag, variants, Some(Bsv_tunion(variants', (width, tag_width, arity, recf), mapping))), args)->(width, tag_width, variants', nst)
                            
                            | Bsv_ty_nom(nst, F_tunion(mrecflag, variants, None), args) ->
                                let recf = ref !mrecflag
                                let (ty, _) = ensure_tunion_details ww bobj "site-11543" ee (recf, variants, nst) ety
                                scrop ty

                            | other -> sf (sprintf "F9700 ty=%s" (bsv_tnToStr other))
                        scrop ety
                    let (_, discriminating_tag) = gen_B_field_select ww bobj [uid ] ("", e', tag_width, width-tag_width) // Note: vtag not passed in because ...
                    //let ww = WF 3 mm ww ("tag " + vtag)
                    let vo = op_assoc vtag variants'
                    if nonep vo then sf (sprintf "No such tag '%s' in union '%s'" vtag (htosp nst.nomid))
                    let (inner_ty, no) = valOf vo
                    let g' = gen_B_and[g0; gen_B_deqd ww bobj (B_hexp(Bsv_ty_integer, xi_num no), discriminating_tag)]
                    if nonep inner then (g', cc) else
                        let (inner_ty_, e) = (gen_B_field_select ww bobj [ uid ] (vtag, e', width-tag_width, 0))
                        mkbindings (WN "taggedUnion" ww) ("taggedUnion"::stk) (g', cc) (valOf inner_ty, valOf inner) e


                    //| (ty, KB_dot bv) ->
                | (ty, KB_patternVar bv) ->
                    //let ww = WF 3 mm ww ("pattern var " + bv)
                    let n = (bv, GE_binding([bv], { g_blank_bindinfo with whom="mish"}, EE_e(ty, e')))
                    (g0, n::cc)

                | (oo, KB_tuplePattern(strlst, (lp, uid))) ->
                    let mf() = lpToStr0 lp + " bev structPattern"
                    //let ww = WN mm ww
                    cleanexit(mf() + sprintf "tuple pattern used outside a struct TODO : in a %s" (bsv_tnToStr oo))

                | (oo, KB_structPattern(strlst, (lp, uid_))) ->
                    let mf() = lpToStr0 lp + " bev structPattern"
                    //let ww = WN mm ww
                    cleanexit (mf() + sprintf " struct pattern used outside a struct TODO : in a %s" (bsv_tnToStr oo))

                | (et, KB_constantAlias(s, text)) ->
                    let (xt, x') = norm_bsv_exp_rmode_tyo ww bobj ee (Some et) (Prov_None "constantAlias") x
                    let q = gen_B_deqd ww bobj (e', x')
                    (gen_B_and[q; g0], cc)
                        
                | (et, ((KB_intLiteral(fwo, (dontcares, b, s), lp)) as x)) ->
                        let pred = 
                            if dontcares then
                                let mm = mf() + ": composite/fancy case"
                                match resolve_intLiteral ww mf (KB_intLiteral(fwo, (dontcares, b, s), lp)) with
                                    | (ty, vale, None) -> gen_B_deqd ww bobj (e', B_hexp(ty, vale))
                                    | (_, _, Some(b, items)) ->
                                        let checker (offset, width, vale) =
                                            gen_B_deqd ww bobj (B_hexp(Bsv_ty_integer, local_xi_bnum vale), gen_B_mask (width * b) (gen_B_shift(-offset * b) e'))

                                        gen_B_and(map checker items)
                                        //muddy(mf() + sprintf ": splots %A" splots)
                            else
                                let (xt, x') = norm_bsv_exp_rmode_tyo ww bobj ee (Some et) (Prov_None "intLit") x
                                gen_B_deqd ww bobj (e', x')
                        (gen_B_and[pred; g0], cc)
                        
//              | (Bsv_ty_nom(_, F_actionValue ty, _), x) ->
                        
                | (Bsv_ty_nom _, x) -> // keep this clause after those above, because, for example, intLiterls are coming in as F_actionvalue style!
                        let ty = muddy(mf() + sprintf ": (case stmt) need to downconvert other nominal form: %A\nx=%A" ty x)
                        mkbindings ww ("above"::stk) (g0, cc) (ty, x) e'

                        
                | (et, x) ->
                        vprintln 0 (mf() + sprintf ": case mkbindings other pairing et=%A x=%A " (bsv_tnToStr et) x)
                        let (xt, x') = norm_bsv_exp_rmode_tyo ww bobj ee (Some et) (Prov_None "L9145") x
                        let q = gen_B_deqd ww bobj (e', x')
                        (gen_B_and[q; g0], cc)

        let rec stage1_prepare_case def = function
            | ([], q)::t when nonep def -> stage1_prepare_case (Some(B_and[], [], q, lp)) t // TODO put lp in tags
            | ([], _)::t -> cleanexit(mf() + ": Two defaults in one case statement")
            | ([tagitem], cmd)::t ->
                let (g, b) = mkbindings ww ["stage1a"] (B_and[], []) (ety, tagitem) e'
                (g, b, cmd, lp) :: stage1_prepare_case def t

            | (taglist, cmd)::t ->
                let fc c tag =
                    let (g, b) = mkbindings ww ["stage1b"] (B_and[], []) (ety, tag) e'
                    match b with
                        | [] -> g::c //singly_add g c - would need equality
                        | comps  -> muddy(mf() + sprintf ": composite case tag with bindings missing support %A" comps)
                let g = List.fold fc [] taglist
                (gen_B_or g, [], cmd, lp) :: (stage1_prepare_case def t)
            | [] when nonep def -> []
            | [] -> [valOf def] // Return the default action at the end of the list.
        let r1 = stage1_prepare_case None arms            


        let conv ee bindings x =
            let ee' = List.fold (fun (ee:ee_t) (k,v) -> { ee with genenv=ee.genenv.Add([k],v)}) ee bindings
            bsv_mkBlock lp (rev(snd(norm_bsv_bev ww bobj rtyo (ee',[]) x)))

        let strictf = not ee.attribute_settings.split_default // Default 'case' statement semantic is no case split hence strict.
        let rec step2 ee = function // step2 generates Bsv_if/then statements from stage1 preparations.
            | [(g, b, cmd, lp)] ->  // end of list
                let cf = bsv_const_bexp ww mf g
                if cf && bsv_bmanifest ww bobj mf g then (ee, Some(conv ee b cmd))
                elif cf then (ee, None)
                else
                    let (e_tt, e_ff) = ee_clone ee
                    let ans = Bsv_ifThenStmt(g, conv e_tt b cmd, None, strictf, lp)
                    let e' = ee_mutmux ww bobj g e_tt e_ff
                    (e', Some ans)

            | (g, b, cmd, lp)::tt ->
                let cf = bsv_const_bexp ww mf g
                if cf && bsv_bmanifest ww bobj mf g then (ee, Some(conv ee b cmd)) // discard tail others - ... WARN ?
                elif cf then step2 ee tt
                else
                    let (e_tt, e_ff) = ee_clone ee
                    let ans = Bsv_ifThenStmt(g, conv e_tt b cmd, snd(step2 e_ff tt), strictf, lp)
                    let e' = ee_mutmux ww bobj g e_tt e_ff
                    (e', Some ans)

        if nullp r1 then (ee, cc)
        else
            let (ee', ans) = step2 ee r1
            (ee', (if nonep ans then cc else (valOf ans)::cc))


    | KB_functionCall(path, args, (lp, uid)) -> // Parser did not wrap this - see the cons call in tinytestbench my_program
        let mm = lpToStr0 lp + " bev functionCall as statement"
        let ww = WF 3 qm ww mm
        norm_bsv_bev ww bobj rtyo (ee, cc) (KB_easc(aa, lp))

    | KB_easc(e, lp) ->
        let mm = lpToStr0 lp + " bev eascStmt"
        let ww = WF 3 qm ww mm
        let (_, e') = norm_bsv_exp_rmode ww bobj ee e
        let r = match e' with
                   // | B_action(B _ -> e'   TODO avoid double nest (action   Bsv_eascActionStmt (action ... ))?
                   | other      -> Bsv_eascActionStmt(other, Some lp)
        //vprintln 0 (mf() + ":normed to " + bsv_stmtToStr r)
        (ee, r::cc)

        // TODO there is also return from functions via assign to function name using single equals.
    | KB_returnStmt(e, (lp, uid)) ->
        let mf() = lpToStr0 lp + " " + htosp ee.static_path + ": bev returnStmt"
        let ww = WF 3 qm ww (if vd>=4 then mf() else "returnStmt")
        let tyo = rtyo
        let et = simple_retrieve ww bobj mf "L9867return" ee uid
        // ...  perhaps pass this in as tyo and see what happens ... TODO?
        //vprintln 3 ("L9867return " + bsv_tnToStr et)
        let ww = WN ("return has annotation " + bsv_tnToStr et) ww
        let (et__, e') = norm_bsv_exp_rmode_tyo ww bobj ee tyo (Prov_None "bev returnStmt") e 
        //vprintln 0 (mf() + " normed to Bsv_resultis of vale=" + bsv_expToStr e' + " ty=" + bsv_tnToStr et)
        let r = Bsv_resultis((et, e'), Some lp)
        (ee, r::cc)

       
    | KB_actionValueBlock(key, nameo1, body, nameo2, (lp, uid)) -> // bev norm site
        let id = funique(if nameo1=None then "anon:" + key else valOf nameo1)
        let mf() = lpToStr0 lp + " key " + " actionBlock or actionValueBlock + " + id
        let ww = WF 3 qm ww (if vd>=4 then mf() else "actionBlock or actionValueBlock")
        if nameo1<>None && nameo2<>None && valOf nameo2 <> valOf nameo2 then cleanexit(mf() + ": end name does not match start: " + valOf nameo1 + " cf " + valOf nameo2)        
        let (_, body') = List.fold (norm_bsv_bev ww bobj rtyo) (ee, []) body 
        let rty =
            if key = "actionvalue" then
                match rtyo with
                | Some ty -> ty
                | None -> muddy(mf() + sprintf " tyo=%A actionvalue type needed copy 2/2 + " rtyo + key)
            else g_ty_action // TODO replicate other site
        //vprintln 0 (sprintf "Body' at L4667 is %A" body') // parsed in correct order...
        let x = Bsv_eascActionStmt(gen_B_action "as-AV-block2 L10250" ee.fbody_context (id, rev body', RV_highlevel rty, lp), Some lp)
        (ee, x::cc)

    | KB_varDeclDo(tyo, lhs, rhs, lp) ->  // One-line make using the do '<-' symbol is when there is a rhs and also a tyo.
        let mf() = lpToStr0 lp + " varDeclDo <- "
        let (shortname, idl, _, uid_, lp_) = lhs_path_shortname ww mf lhs
        let mm = lpToStr0 lp + " bev/rule " + (if nonep tyo then "nodecl " else "") + " varDeclDo <- " + shortname
        let ww = WF 3 qm  ww mm 
        let aa = KB_varDeclAssign(tyo, lhs, Some rhs, lp) // Thus is a stylised let bind where the rhs is non-idempotent.
        let (ee, cc, kvl_) = norm_decl_assign ww bobj (qm + ":" + mm) tyclass (ee, cc) aa
        (ee, cc)
       
    | other -> sf ("bev ast other " + sprintf "%A" other)


//
// The 'norm' functions convert from the src code AST into the main internal representation type (prefix KB_ is converted to prefix Bsv_).
// The output from norming is finally compiled by the 'compile_statement' code.
//
       
and norm_body_items ww bobj (if_typeo:bsv_type_t option (*option since subifc code is missing and supplies none ,,, wont work for return applied to a subinterface?*)) (ee:ee_t, cc) lst =
    let (ee, items) = List.fold (norm_moduleStmt ww bobj if_typeo) (ee, cc) lst
    // Consider returning '(ee, rev items)' since nearly all callers want this and the one that does not may be a bug.
    (ee, items)
    
and norm_moduleStmt ww bobj if_typeo (ee:ee_t, cc) aa =
    let qmm = "ast0:norm_moduleStmt"
    let tc aa = norm_moduleStmt ww bobj if_typeo (ee, cc) (aa)
    let tyclass = None
    let vd = 3
    match aa with
    | KB_subinterfaceDef00(package_o, id0__, name, body, nameo, lp) -> // TODO id0_ should probably not be ignored .. please make a test where this needs to be used to distinguish ... extend Test6 ...
        let qm = " Classical subinterfaceDef+" 
        let mf() = lpToStr0 lp + qm + name
        let ww = WF 3 qmm ww (if vd>=4 then mf() else "Classical subinterfaceDef")
        if nameo<>None && valOf nameo <> name then cleanexit(mf() + ": end name does not match start: " + name + " cf " + valOf nameo)
        let sub_if_type = find_sub_if_type ww mf if_typeo name
        let (_, items) = norm_body_items ww bobj (Some sub_if_type) (ee, []) body   // A recursive call.
        dev_println ("Why us this classical subinterfaced marked borrowed? name=" + name)
        let borrowed_flag_ = true  // This indicates a borrowed interface.  Why is this form marked thus? Is it not the primary owner of the associated currency? Or was that recursively enumerated by the parent interface?
        let mval = B_ifc_immed(rev items)
        let ans = Bsv_varAssign(borrowed_flag_, [name], mval, qm, lp) //  The borrowed flag needs to be in the lifter shim type of the IIS to bexp_t
        (ee, ans::cc)

    | KB_subinterfaceDef1(tyo, name, ebody, lp) ->        
        let qm = " Assignment-style subinterfaceDef+" 
        let mf() = lpToStr0 lp + qm + name
        let ww = WF 3 qmm ww (if vd>=4 then mf() else "Assignment-style subinterfaceDef")
        let sub_if_type = find_sub_if_type ww mf if_typeo name        
        let item = // will be an interace expression ...
          match norm_bsv_exp_rmode_tyo ww bobj ee (Some sub_if_type) (Prov_asReg_asIfc "raw-subif") ebody with
              // TODO re-nest the borrowed flag ... or make fd on subsequent arg.
            | (ty_, B_applylam(acp, PI_bno(s, nameo), args, lp1, flg, ans)) -> Bsv_varAssign(true, [name], B_applylam(acp, PI_bno(s, nameo), args, lp1, flg, ans), qm, lp) // the true here is arb! TODO re-nest so the flag is supplied later.

            // Having three forms seems overkill? No that is ok, but the flagger is implied by the form used.
            | (ty_, B_ifc_immed(items))             -> Bsv_varAssign(true, [name], B_ifc_immed(items), qm, lp) // Borrowed flag is set for this assignment of foreign currency.
            | (ty_, B_ifc(path, ty))                -> Bsv_varAssign(true, [name], B_ifc(path, ty), qm, lp)
            | (ty_, B_subif(ss, vale, ty, lp))      -> Bsv_varAssign(false, [name], B_subif(ss, vale, ty, lp), qm, lp) // Why false if a subinterfaceDef? TODO.
 

            | (ty_, vale) -> sf (mf() + sprintf ": raw-subif other form %A" vale)
        (ee, item::cc)

    | KB_letBinding(id, r, operator, (lp, uid)) -> // norm1
        // 
        let mf() = lpToStr0 lp + " " + htosp ee.static_path + sprintf ": norm of letBind (moduleStmt) %s %s" id operator
        let bound_name = [id]
        let ww = WF 3 qmm ww (if vd>=4 then mf() else "norm of letBind (moduleStmt)")
        let (rt_, r') =
            match operator with // TODO simplify this ....
                | "<-" ->
                    // Does this 'norm' actually 'run' the monad? If staging is correct such that none is done during ewalk/commission then yes it does, so binding its result into the env is fine.
                    let (rt, r') = norm_bsv_exp_rmode ww bobj ee r
                    //let iname  = funique (id + "$resultis_anon")
                    //let bv = [iname]// B_ifc(PI_ifc_flat ifc_name,  ifc_ity)
                    //let outer_cp = ee.callpath
                    // Should wrap this up here so it is not fudged later in wire methods! TODO
                    //let siz = B_moduleInst(bv, (ifc_name, outer_cp), bv, arg, lp)
                    //let nos = Bsv_eascActionStmt(siz, Some lp)   
                    //bv is lhs ifc
                    //muddy (mf() + sprintf ": letdo %A\nty=%A\niftypeo=%A" r' rt if_typeo)
                    (rt, r')
                    
                | "=" -> norm_bsv_exp_rmode ww bobj ee r

                | other -> sf(mf() + ": bad let operator: " + operator)

        let rt = simple_retrieve ww bobj mf "L10198" ee uid
        let kv = (bound_name, GE_binding(bound_name, {g_blank_bindinfo with whom=operator}, EE_e(rt, r')))
        vprintln 3 (mf() + " type=" + bsv_tnToStr rt + " and value=" + bsv_expToStr r')
        let ee' = { ee with genenv= ee.genenv.Add kv }
        (ee', cc)

    | KB_varDeclAssign(tyo, lhs, ro, lp) ->
        let mf() = lpToStr0 lp + " structural-1 varDeclAssign"
        let ww = WF 3 qmm ww (if vd>=4 then mf() else " structural-1 varDeclAssign")
        let monica = ee.prefix
        let (ee, cc, kvl_) = norm_decl_assign ww bobj (mf(*() + ":" + mm*)) tyclass (ee, cc) aa
        (ee, cc)
        
    | KB_while(grd, body, lp) ->
        let mf() = lpToStr0 lp + " " + htosp ee.static_path + " generative KB_while"
        let ww = WF 3 qmm  ww (mf())
        
        let rec monkey ww = function // TODO two copies of monkey in this prog.
            | other when bsv_const_bexp ww mf other -> bsv_bmanifest ww bobj mf other // monkey (B_bexp(bsv_blower ww other))
            | other -> 
                let em = sprintf ": compile-time-constant while guard needed, not %s" (bsv_bexpToStr other)
                let _ = whereami_del em (vprintln 0) ww 
                cleanexit(mf() + em)
        let rec unwind_while ee cc lim =
            let g = norm_bsv_bexp_rm ww bobj { ee with forcef=Some true } grd
            if lim <= 0 then cleanexit(mf() + sprintf ": Too many unwind attempts. L10398. Please increase whilefor limit (-bsv-whilefor-limit=n). Current value is %i." bobj.whilefor_limit)
            elif monkey ww g
            then
                let (ee', cc') = norm_moduleStmt ww bobj if_typeo (ee, cc) (body)
                unwind_while ee' cc' (lim-1) // TODO HMMMM need a rev here!
            else (ee, cc)
        unwind_while ee cc bobj.whilefor_limit
        
    | KB_block(blockname, lst, nameo, lp) ->
        let mf() = lpToStr0 lp + " " + htosp ee.static_path + " gen blockStmt " + valOf_or blockname "anon"
        let ww = WF 3 qmm ww (mf())
        // Put block name in satic path and prefix
        if nameo<>None && blockname<>None && valOf nameo <> valOf blockname then cleanexit(mf() + ": end name does not match start: " + valOf blockname + " cf " + valOf nameo)
        let ee = if nonep blockname then ee else { ee with static_path= arity_aug_static_path (valOf blockname) ee.static_path;  }
        let ans = norm_body_items ww bobj if_typeo (ee, cc) lst // recursive call site 
        ans // no rev?

    | KB_varDeclDo(tyo, lhs, rhs, lp) ->  // One-line form using '<-' symbol is when there is a rhs and also a type option (tyo): module ast item.
        let mf() = lpToStr0 lp + " varDeclDo one-line form"
        let (bv_, bound_path, _, lhs_uid, lp_) = lhs_path_shortname ww mf lhs
        let iname = bound_path @ ee.prefix
        let mf() = lpToStr0 lp + " varDeclDo one-line form" + (if nonep tyo then " do only (no declaration) " else " ") + ", iname=" + htosp iname
        let ww = WF 3 qmm ww (if vd>=4 then mf() else "varDeclDo")
        let genrf = true
        norm_decldo ww bobj (mf(*() + ":" + mm*)) (ee, cc) bound_path iname iname genrf (tyo, lhs, lhs_uid, rhs, lp)

    | KB_for(s1, g, s2, body, lp) ->
        let (mm, ww, expanded) = prep_for ww qmm aa
        norm_moduleStmt ww bobj if_typeo (ee, cc) (expanded)   // Second callsite in norm_moduleStmt

        
    | KB_rule(pragmas, rulename, condo, body, nameo, lp) ->
        let mf() = lpToStr0 lp + sprintf " rule '%s'"  rulename
        let (ee_orig, ee) = ee_clone ee;
        let ww = WF 3 qmm ww (mf())
        if nameo<>None && valOf nameo <> rulename then cleanexit(mf() + ": end name does not match start: " + rulename + " cf " + valOf nameo)
        let keyname = rulename::ee.static_path
        let monica = rulename::ee.prefix
        let ms = "Rule " + hptos monica
        let pragmats = ins_ats bobj lp "rule" bobj.prags keyname pragmas // retrieve on definition path.
        let ee = { ee with attribute_settings=refine_attribute_settings ee ee.attribute_settings pragmats }
        let guard = if condo=None then gen_B_bexp X_true else norm_bsv_bexp_rm ww bobj ee (valOf condo)
        let (_, body') = List.fold (norm_bsv_bev ww bobj None) (ee, []) body 
        let body' = rev body'
        let rate_target =
            let v1 = ee.attribute_settings.fire_rate
            if not_nonep v1 then vprintln 2 (sprintf "rule_rate target: rulename=%s rate %A" rulename v1)
            v1


        let copycount =  // Copying here means it gets typechecked multiple times too. But now we replicate rules only during the second pass.
           if bobj.respect_rule_repeat_counts then
                    let count = test_att_int bobj.prags 1 keyname (Pragma_rule_repeat_count 0)
                    //let count = ee.attribute_settings.superscalar_rate
                    if count >= 1 && count <= 100 then
                        vprintln 2 (ms + sprintf ": rule repeat count %i" count)
                        count
                    else cleanexit(ms + sprintf " infeasible repeat count requested %i" count)
           else 1

        let rulerec = { name=rulename; rate_target=rate_target; copycount=copycount }
        let rule_fn superscalar_no =
            let tailer (rulerec:bsv_rulerec_t) g body =
                let rulerec = if copycount > 1 then { rulerec with name=rulerec.name + sprintf "_%i" superscalar_no } else rulerec
                Bsv_rule(rulerec, g, body, keyname, lp) // Note a split rule looses its name hence we preserve they keyname so that pragmas/attributes do not become detached. Better to marshal them here.
            let explicit_splitter = true // TODO control of this needed ? should be in rulerec? From strict Pragma_split/Pragma_nosplit ? - we have belt and braces currently ...
            if explicit_splitter then
                // can split rule to a given depth
                let start_depth = 10000 // Maximally aggressive for now

                vprintln 3 (sprintf "Split rule %s with depth %i (0 is no splitting)" rulename start_depth)

                let rec split_block depth name g body cc = body

                and explicit_split_rule depth rulerec g body cc =            
                    if depth <= 0 then (tailer rulerec g body)::cc
                    else match body with
                            // Do we want to split an atomic block!  
                          | [] 
                          | [Bsv_beginEndStmt([], _)]     -> cc
                          | [Bsv_beginEndStmt([item], lp)] -> sf (mf() + ": internal error: singleton rule block should have been eliminated") 
                          // Do we want to split an atomic block - would not be atomic then!  
                          //| Bsv_beginEndStmt(lst, lp) -> List.foldBack (split_rule depth name g) lst cc
                          | [Bsv_beginEndStmt(lst, lp)] -> explicit_split_rule (depth) rulerec g lst cc

                          | [Bsv_ifThenStmt(g1, t, f, strictf_, lp)] -> // Simple syntax-driven splitter.
                            let cc1 = explicit_split_rule (depth-1) { rulerec with name=rulerec.name + "_L" } (gen_B_and[g1; g]) [t] cc
                            if nonep f then cc1
                            else
                                let rulerec' = { rulerec with name=rulerec.name + "_r" }
                                explicit_split_rule (depth-1) rulerec' (gen_B_and[gen_B_not g1; g]) [valOf f] cc1
                          | _ -> (tailer rulerec g body)::cc
                let rulerec = { name=rulename; rate_target=None; copycount=copycount }
                let rules = explicit_split_rule start_depth rulerec guard (body') []
                vprintln 3 (sprintf "Split rule %s with depth %i resulted in %i rules" rulename start_depth (length rules))        
                rules
            else [ tailer rulerec guard body' ]

        let rules = list_flatten (map (fun superscalar_no -> rule_fn superscalar_no) [0..copycount-1]) // This is the old replicator site.  Delete it please.
        (ee_orig, rules@cc)

        
    | KB_functionDef3(proto, assf, body, nameo, lp) ->
        let name = 
                match proto with
                    | KB_functionProto(return_type_raw, name, formals_raw, provisos, lp) -> name
                    | _ -> sf "L9275"
        let mf() = lpToStr0 lp + sprintf " module functionDef '%s'" name + (if assf then " by assignment" else "")
        let ww = WN (if vd>=4 then mf() else "functionDef3b") ww
        let ats= []
        let (ifc_sty, tyclass) = (None, None)
        let (kv, cc_, name) = norm_functionDef ww bobj tyclass mf (ee, cc) ifc_sty (KB_functionDef3(proto, assf, body, nameo, lp), None)
        if nameo<>None && valOf nameo <> name then cleanexit(mf() + ": end name does not match start: " + name + " cf " + valOf nameo)
        (augment_env_entry ww bobj mf "functionDef3b" ee kv, cc)

    | KB_methodDef1(return_type_option, methodname, formals, guard, body, nameo, lp) ->
        let ee = { ee with fbody_context=false; static_path= methodname :: ee.module_path }
        let mf() = lpToStr0 lp + " methodDef1+" + htosp ee.static_path  + ":" + methodname
        let ww = WF 3 qmm ww (if vd>=4 then mf() else "methodDef1")
        if nameo<>None && valOf nameo <> methodname then cleanexit(mf() + ": end name does not match start: " + methodname + " cf " + valOf nameo)
        let hardstate = false // User-defined methods are never hardstate (only leaf tech/BVI components can be).        
        let mm = norm_define_method_or_subifc ww bobj if_typeo hardstate return_type_option methodname formals guard lp (Some body, None) ee
        (ee, mm::cc)

    | KB_methodDef2(return_type_option, methodname, formals, guard, methex, lp) ->
        let mm = lpToStr0 lp + " methodDef2+" + htosp ee.static_path  + ":" + methodname
        let ww = WF 3 qmm ww mm
        let ee = { ee with fbody_context=false; static_path= methodname :: ee.module_path }
        let hardstate = false // User-defined methods are never hardstate (only leaf tech/BVI components can be).        
        let mm = norm_define_method_or_subifc ww bobj if_typeo hardstate return_type_option methodname formals guard lp (None, Some methex) ee
        (ee, mm::cc)

    | KB_easc(e, lp) when false -> // 
        let mm = lpToStr0 lp + " module eascStmt"
        let ww = WF 3 qmm ww mm
        let (_, e') = norm_bsv_exp_rmode ww bobj ee e
        let r = match e' with
                   // | B_action(B _ -> e'   TODO avoid double nest (action   Bsv_eascActionStmt (action ... ?
                    | other      -> Bsv_eascActionStmt(other, Some lp)
        (ee, r::cc)

    | KB_returnStmt(e, (lp, uid)) ->
        let mm = lpToStr0 lp + " " + htosp ee.static_path + ": module returnStmt"
        let ww = WF 3 qmm ww mm
        // Here we are returning an (unapplied? - depends on the operator used in the let binding) monad.
        let (et, e') = norm_bsv_exp_rmode_tyo ww bobj { ee with forcef=Some true;} (if_typeo) (Prov_None "mod returnStmt") e 
        //vprintln 0 (mf() + " YOG " + bsv_expToStr e' + " ty=" + bsv_tnToStr et)
        let r = Bsv_resultis((et, e'), Some lp)
        (ee, r::cc)

    | KB_if(g, t, fo, lp)->
        let mf() = lpToStr0 lp + " AST IF"
        let ww = WF 3 qmm ww (mf())
        let g' = norm_bsv_bexp_rm ww bobj ee g
        if not(bsv_const_bexp ww mf g') then cleanexit(mf() + ": Constant IF condition required for structural elaborate")
        elif bsv_bmanifest ww bobj mf g' then tc t
        elif nonep fo then (ee, cc)
        else tc (valOf fo)

    | KB_typedefSynonym(KB_type_dub(_, name, generics, lp), ty, lp_) ->
        let mf() = lpToStr0 lp + " norm module typedef"
        let ww = WF 3 qmm ww (if vd>=4 then mf() else " norm module typedef") 
        let augs = operate_typedefSynonym ww vd bobj mf tyclass ee aa
        let ee = List.fold (augment_env_entry ww bobj mf "typedefSynonym-L11852") ee augs
        (ee, cc)

    | KB_scheduleOrder _
    | KB_scheduleMatrix _ ->
        let _ = tynorm_schedule ww bobj false "module ast stmt" aa
        (ee, cc)

    | other -> sf ("module ast stmt other " + sprintf "%A" other)

// Define a method or sub-interface. For a method, definition prefix and static_path are the same. Different at use sites.
and norm_define_method_or_subifc ww bobj if_typeo hardstate return_type_option methodname formals guard lp (bo, eo) ee =
    // mname is wierdly flat at this point but subinterface part is stupidly missing
    let mname = methodname::ee.prefix
    let mf() = lpToStr0 lp + ": norm methodDefintion+" + htosp mname
    let ww = WF 3 "norm_define_method" ww (hptos mname)
    let ee = { ee with forcef=Some false; static_path=methodname::ee.static_path } // Cannot force on methods, only on functions.
    let (rt_, proto, arg_tys) =
        match if_typeo with
            | Some (Bsv_ty_nom(iidl, F_ifc_1 ii, _))  ->
                let scanp = function
                    | Bsv_ty_methodProto(methodname', _,  mth_provisos, proto, _) -> methodname = methodname'
                    | Bsv_ty_nom(_, F_fun(_, matts, _), _) -> methodname = matts.methodname
                    | _ -> false
                match List.filter scanp ii.iparts with
                    | [] ->
                        vprintln 0 (sprintf "Methods in '%s' are: " (htosp iidl.nomid) + sfold bsv_tnToStr ii.iparts)
                        cleanexit(mf() + ": method was not declared in exported interface definition [L8354]")
                    | a::b::_ -> cleanexit(mf() + ": method exists more than once in exported interface definition: " + htosp iidl.nomid)
                    | [Bsv_ty_methodProto(methodname', mth_ats, mth_provisos, proto, _)] -> muddy " older proto"
                    | [Bsv_ty_nom(_, F_fun(rt, matts, args), _)] -> (rt, matts.protocol, args)
            | None -> sf(mf() + ": no parent interface in scope")

    //dev_println (sprintf "%A && not %A" bobj.reentrant_methods (not hardstate))
    // A module may appear both at the top-level and non-toplevel, so reentrantf could be set both ways within a single compilation for a given method definition.
    // So this a potentially_ephemeral        
    let potentially_ephemeralf = bobj.reentrant_methods && not hardstate
    let (atomicf, rt) = // We insert action/endaction or actionvalue/endactionvalue around the body if missing and the method return type is an action or actionvalue respectively.
        match proto with
            | BsvProtoAction        -> (true, g_action_ty)
            | BsvProtoActionValue rt-> (true, rt)
            | BsvProtoValue rt      -> (false, rt)


    let no_formal ((ast_ty, id), ty) =
        //muddy (mf() + sprintf " want to retrieve a dropping for '%s' from ast=%A ty=%A" id ast_ty ty)
        (SO_none "no_formal", ty, id)

    let formals' = map no_formal (List.zip formals arg_tys)

    let fadd (em:genenv_map_t) (so, ty, id) =
        //vprintln 0 (mf() + " temp bind formal " + id)
        em.Add([id], GE_formal(so, id, ty)) // method formal - the only rez site
        
    let local_ee = { ee with genenv= List.fold fadd ee.genenv formals' } 
    let guard' = if guard=None then gen_B_bexp X_true else norm_bsv_bexp_rm ww bobj local_ee (valOf guard)
    match (bo, eo) with
        | (Some body, None) ->
            // TODO see if atomicf holds and body is not an actionblock - need to insert an implied actionblock.
            let (_, body') = List.fold (norm_bsv_bev ww bobj (Some rt)) (local_ee, []) body
            vprintln 3 (sprintf "Normed  methodDef (with body) %s potentially_ephemeralf=%A" (htosp mname) potentially_ephemeralf + " with return type " + bsv_tnToStr rt) 
            Bsv_methodDef("method", mname, proto, potentially_ephemeralf, rt, formals', guard', rev body', lp)
        | (None, Some expr)->
            let (e1_ty, e1) = norm_bsv_exp_rmode_tyo ww bobj local_ee (Some rt) (Prov_None "L9529") expr
            vprintln 3 (sprintf "Normed methodDef (no body) %s potentially_ephemeralf=%A" (htosp mname)  potentially_ephemeralf + " with return type " + bsv_tnToStr rt) 
            Bsv_methodDef("method", mname, proto, potentially_ephemeralf, rt, formals', guard', [Bsv_resultis((e1_ty, e1), Some lp)], lp)            
        | (Some _, Some _)
        | (None, None) -> sf (mf() + " both or neither not expected")

and norm_functionDef ww bobj tyclass mf (ee:ee_t, cc) ifc_sty (KB_functionDef3(proto, assf, body, nameo, lp), handy_type_info) =
    let (name, fids) =
        match proto with
            | KB_functionProto(return_type_raw, name, formals_raw, provisos, lp) -> (name, map get_fidder formals_raw)
            | _ -> sf "L9848"
                    
    // New, correct: do not re-typecheck or retrieve any check - sufficient type info should be available from callsite dropping.
    // Package functions will have had their own types checked and passed in as the handy arg.
    let fun_ty =
        match handy_type_info with
            | Some(_, _, _, [(id, sty)]) -> sty
            | Some _ -> sf "L9848"
            | None -> Bsv_ty_dontcare "Function type is available from callsite dropping"
    let idl = name::ee.static_path
    let ee_inner = { ee with static_path= idl; }
    let nf = B_abs_lambda(idl, name, fids, body, ee_inner, lp)
    let dmtof = false
    (Aug_env(tyclass, [name], (*idl,*) (dmtof, {g_blank_bindinfo with whom="norm_functionDef"}, EE_e(fun_ty, nf))), cc, name)


// Normalise a typedef statement.
// General example:   stypedef Bop#(f, 15T, f) Mybop#(f);
// Prams can be repeated or in different orders or replaced with constant types.
and operate_typedefSynonym ww vd bobj (mf:unit->string) tyclass ee = function
    | KB_typedefEnum(item_list, id, derives, lp) -> // enumeration.
        let vd = 3
        let mf() = mf() + " typeDef enumeration " + id
        let mm = mf()
        let ww = WF 3 "operate_typedefSynonym" ww mm
        let idl = id :: ee.prefix
        let dmtof = false // Not allowed to redefine
        let exists =
            match ee.genenv.TryFind[id] with
                //| Some(GE_normthunk(true, _, _, _, being_done, _)) when !being_done -> ()
                | Some _ -> vprintln 3 (mf() + sprintf ": enum '%s' defined more than once. [L7258]" id)
                | None -> ()


        let items =
            let mf () = cleanexit(sprintf "Illegal enum initialisation for %s" (hptos idl))
            let parse_enum = function
                | (enumtag, None)     -> (enumtag, None)
                | (enumtag, Some ast) -> (enumtag, Some(bsv_manifest_32 ww bobj mf (snd(norm_bsv_exp_rmode ww bobj ee ast))))
            let mapped = map parse_enum item_list
            //dev_println(sprintf "mapped enum is %A" mapped)
#if SPARE
            let values_in_use =
                let viu cc = function
                    | (_, None)   -> cc
                    | (_, Some a) -> a::cc
                List.fold viu [] mapped
            let rec skipover pos = if memberp pos values_in_use then skipover (pos+1) else pos
#endif
            let rec allocate_enum_values pos = function
                | [] -> []
                | (tag, Some vale)::tt -> (tag, vale)::allocate_enum_values (vale+1) tt // Unspecified gets the previous field's value plus one.
                | (tag, None)::tt ->
                    (tag, pos) :: allocate_enum_values (pos+1) tt
            allocate_enum_values 0 mapped

        let max_in_use = List.fold (fun cc (x, y) -> max y cc) 0 items
        let arity = length(item_list)            
        let ty = gen_ty_enum({g_blank_nom_typ_ats with nomid=idl; cls=intstyle_tyclasses }, arity, max_in_use, items) // TODO - not standard BSV - in what way?
        
        let _ =   // DONT WANT TO DO THIS FOR BOOL TYPE REALLY - THAT CAN BE PUSHED LOWER STILL...  TODO.
            // This is a totally silly interface for closing the enum group!
            let grpname = vectornet_w(htosp idl + "_enum_root", 32)
            let enum (s, p) =
                let dr = enum_add "enum grp" grpname s p
                let _ = xi_deqd(grpname, dr)  
                dr
            let _ = map enum items
            close_enum mm grpname

        let width = bound_log2 (BigInteger (max_in_use))        
        let tab = create_table(sprintf "Enumeration, arity %i (total width %i)." arity width + htosp idl, ["tag"; "encoding"], map (fun (id, pos) -> [ id; i2s pos ]) items)
        mutadd bobj.m_tables (mm, tab)
        if vd >= 2 then app (vprintln 2) tab
        let newtags = map (fun (ss, _) -> (ss, (ty, 0))) item_list                
        [Aug_env(tyclass, [id], (*idl,*) (dmtof, {g_blank_bindinfo with whom="ef" }, EE_typef(G_none, ty))); Aug_tags newtags ]

    | KB_typedefSynonym(KB_type_dub(true, name, generics, lp), ast_ty, lp_) -> // non enumeration.
        let prec = None
        let arity = length generics
        let mf() = lpToStr0 lp + " typeDefSynonym " + name + (if arity=0 then "" else "<" + i2s arity + "...>") + sprintf " generic arity=%i" (length generics)
        let ww = WF 3 "operate_typedefSynonym" ww (mf())
        let idl = name :: ee.prefix
        let exists =
            match ee.genenv.TryFind[name] with
                | Some x ->
                    //cleanexit
                    vprintln 3 (mf() + sprintf ": synonym '%s' defined more than once [L9907]:" name + olddef_ge x) // TODO this is being raised spuriously at the moment - please fix and make this a hard error again.
                | None -> ()
        //vprintln 0 (mf() + sprintf ": check exists=%A" exists)
        let dmtof = false

        let synargs =
            //  List.foldBack (collect_sum2_tid ww bobj ee msg (Some P_decl0)) [] generics []
            let synoarg = function // Expect purely formals in this context.
                | (so, None, fid) -> Tid(toVP so fid, fid)
                | other -> sf (mf() + sprintf ": synoarg other %A" other)
            map synoarg generics // collectsum1 does not care whether these formals are bound for the body parsing, so we here leave the resolution to use contexts.

        let latent_tyargs = // Selectively add latents that were not explict.
            let latents = 
                let latents = mine_latent_generics ww mf ast_ty []
                //dev_println (msg + sprintf " latents type args are %A" latents)
                latents

            let add (so, fid) cc =
                // Those discovered latent that are also explict can be ignored and each latent needs listing only once.
                let rec dismember = function
                    | Tid(_, fid1)::_ when fid=fid1 -> true
                    | _::tt -> dismember tt
                    | [] -> false
                if dismember synargs || dismember cc then cc
                else
                    // dev_println (sprintf "swold munge add latent %s" fid)
                    let vp = VP_type // swold TODO fill this in correctly
                    cc @ [ Tid(vp, fid) ]

            List.foldBack add latents []

        let inner_sty = collect_sum1 ww bobj ee mf prec (VP_type) (*TODO think about this *) ast_ty

        let kv =
            if true || nullp generics then  // should check synargs not generics here... but disabled for now!
                //let ty' = norm_bsv_type ww bobj msg ee (SO_none m) ast_ty
                
                if vd>=4 then vprintln 4 (mf() + " defined abstract regardless " + htosp idl + " as collected " + bsv_tnToStr inner_sty)
                //dev_println (sprintf "swold: G_some synargs lengths explicit^=%i and latent^=%i" (length synargs) (length latent_tyargs))
                Aug_env(tyclass, [name], (*idl,*) (dmtof, {g_blank_bindinfo with whom="typedefSynonym";(* synonymf=true *) }, EE_typef(G_some(synargs, latent_tyargs), inner_sty(*_post_trim*))))
            else
                sf "no longer used - should perhaps reinstate once synargs are being collected properly and tested. - Jan 2019 this is fixed now?"
                match inner_sty with
                    | Bsv_ty_nom(temp_name_, inner, args_) -> // TODO explain why args_ is ignored.
                        let dprec = Some P_decl0
                        let sty1 = Bsv_ty_nom({g_blank_nom_typ_ats with nomid=idl; cls=muddy"ssssspssp" }, inner, List.foldBack (collect_sum2_tid ww bobj ee mf dprec) generics [])
                        vprintln 3 (mf() + "defined " + htosp idl + sprintf  ": new typedef made %s" (bsv_tnToStr sty1))
                        let sty2 = afreshen_sty ww bobj "kv" None sty1
                        if vd>=4 then vprintln 4 (mf() + " defined " + htosp idl + " post freshen " + bsv_tnToStr sty2)
                        Aug_env(tyclass, [name], (*idl,*) (dmtof, {g_blank_bindinfo with whom="typedefSynonym22"}, EE_typef(G_none, sty2)))

                    | sty when is_grounded ww sty ->  // concrete types (flagged with G_none) will require no further normalisation.
                        if vd>=3 then vprintln 3 (mf() + " defined concrete " + htosp idl + " post freshen " + bsv_tnToStr sty)
                        Aug_env(tyclass, [name], (*idl,*) (dmtof, {g_blank_bindinfo with whom="typedefSynonym33"}, EE_typef(G_none, sty)))

                    | other -> sf(mf() + sprintf ": unexpected type to bind in typedef: %A" other)
        [kv]  // cannot tie knot till get actuals: no but please make the knot nicer?

    | KB_typedefConst(id, exp, lp) -> // constant alias
        let mf() = mf() + " typeDef constant " + id
        let ww = WF 3 "operate_typedefSynonym" ww (mf())
        let idl = id :: ee.prefix
        let dmtof = false // Not allowed to redefine        
        let (ty_, a') = norm_bsv_exp_rmode_tyo ww bobj ee (None) (Prov_None "typedef const") exp // 
        let n = bsv_manifest ww bobj mf a'
        //vprintln 3 (mf() + sprintf ": has definition %A" n)
        let ans = Bsv_ty_intexpi(B_hexp(Bsv_ty_integer, local_xi_bnum n))
        [Aug_env(tyclass, [id], (*idl,*) (dmtof, {g_blank_bindinfo with whom="ef-const"}, EE_typef(G_none, ans))) ]


// Experimental forwarding path scanner (prescan) to support pipelined operators such as synchronous SRAMs (BRAMs).
// TODO - ideally need a generic walker for this please, Could use commission but better to abstract that as well.
// We need to scan all places where a register or other state holder assignment can be made and check whether it is from a dotted subscript or reftran function thereof.
// The sources that will later need to use the forwarded values are readers of such state holders.
// The sinks that consume forwarded values are reads of registers flagged as supplied with late data.    
and forwarding_paths_prescan ww bobj emits sigma (statemap, fwdpaths) stmts =
    let _:bsv_stmt_t list = stmts
    let mf () = "forwarding_paths_prescan"
    // TODO explain: bool expressions certainly can be forwarded over ... The ix_query guard is not such a site however.
    let rec fpath_scan_bexp mf lp (cc, cd) = function // Syntax-directed forwarding path scan for bools.
        | B_not arg ->
            // Boolean 'not' is allowable on a forwarding path. We essentially compose (fun x->ix_not x) with the existing forwarding function.
            // It will be handy to support quite a few referentially transparent primitives, such as pack/unpack on these paths as well.
            let ash_not arg = xi_blift(xi_not (xi_orred arg)) // Augment lightweight forwarding path.
            let apply_ash_not ((lp, rhs_tag), lvv, rdbus, fwd_func) = ((lp, rhs_tag), lvv, rdbus, fwd_func>>ash_not)
            let (cc, cd) = fpath_scan_bexp mf lp (cc, cd) arg
            (map apply_ash_not cc, cd)

        | B_and lst
        | B_or lst ->
            let cd = List.fold (fpath_scan_bexp_out_of_context_s "boolean arg2" lp) cd lst
            ([], cd)
        | B_bdiop(_, lst)    ->
            let cd = List.fold (fpath_scan_exp_out_of_context_s "boolean arg1" lp) cd lst
            ([], cd)
        | B_orred x          ->  fpath_scan_exp mf lp (cc, cd) x
        | other ->
            hpr_yikes (sprintf "L3522 TODO other boolean scan sites needed?  %A" other)
            (cc, cd)

    and fpath_scan_bexp_out_of_context_s msg lp cd arg =
        let (cc, cd) = fpath_scan_bexp (fun ()->msg) lp ([], cd) arg
        if not_nullp cc then sf(msg + ": bool exp operates on expressions that first needs registering: " + lpToStr0 lp)
        cd

    and fpath_scan_exp_out_of_context_s msg lp cd arg =
        fpath_scan_exp_out_of_context (fun ()->msg) lp cd arg
        
    and fpath_scan_exp_out_of_context mf lp cd arg =
        let (cc, cd) = fpath_scan_exp mf lp ([], cd) arg
        if not_nullp cc then sf(mf() + ": operator is applied to an expressions that first need registering: " + lpToStr0 lp)        
        cd

    and fpath_scan_exp msg lp (cc, cd) = function // Syntax-directed forwarding path scan for non-bools.

    // Please explain about native_write being forwarded over as a write destination.
        | B_applyf((PI_bno(B_dyn_b(fname, lhs), _)), rt, args, (lp, lhs_tag)) when fname = g_swrite -> // TODO use write_pred ?
            cassert(length args = 1, "length swrite args is unity")
            let (ty, arg0) = hd args
            let (bound, cd) = fpath_scan_exp (fun () -> "in-swrite") lp ([], cd) arg0
            if not_nullp bound then
                match lhs with
                    | B_ifc(PI_ifc_flat idl, ty_) -> 
                        let nb = (fname, idl, lhs, bound, ty, (lp, lhs_tag))
                        (cc, (nb::cd)) // Here, end of the path grown in cc now logged into cd.
                    | _ ->
                        vprintln 3 (sprintf "fpath_scan_exp: silent ignore L3264")
                        (cc, cd)
            else (cc, cd)

        // Example of an op that can be forwarded, and indeed cannot be used without forwarding - flag later as error if used that way. TODO.
        | B_applyf((PI_bno(B_dyn_b(fname, lvv), _)), rt, args, (lp, rhs_tag)) when fname = "native_read" -> // TODO use native_read_pred and TODO on top of that, forwarding_required_pred.
            let mf() = "prescan forwarded operator result read"
            cassert(length args = 1, "native_read arg count unity.")
            let (_, subs) = hd args
            //let (cc, cd) = List.fold (fpath_scan_exp (fun ()->"native_read_arg") lp) (cc, cd) (map snd args)
            let cd = fpath_scan_exp_out_of_context (fun()->"native_read") lp (cd) subs // TODO, in general map over all args not assume just one .
            dev_println (sprintf "Address/subscript arg to native_read is %s" (bsv_expToStr(snd(hd args))))
            dev_println (sprintf "Instance subject to native_read llv %s" (bsv_expToStr lvv))
            let fwd_func = (fun x->x) // The forwarding path comb function starts off as the identity.
            let that1 =
                match lvv with
                    | B_ifc(PI_ifc_flat that1, _) -> that1
                    | _ -> sf (mf() + "L3361")
            let rdbus =
                let (key1, concrete_item_o, eph_item_o, error_o) = find_meth mf sigma that1 fname
                let _:interface_inode_name_t * method_name_t = key1
                match concrete_item_o with
                    | Some(Sigma(name_, resats, (proto', RV_netlevel(rt_ty, underlier), m_ats), nl_args)) ->
                        underlier
                    | other ->
                        sf(sprintf "rdbus needed for %A not %A." (hptos that1) other)

            (((lp, rhs_tag), lvv, rdbus, fwd_func)::cc, cd)

        | B_blift bexp ->
            let cd = fpath_scan_bexp_out_of_context_s "blift operator" lp cd bexp // TODO actually we should forward over this operator since it is just 'wiring'.
            (cc, cd)

        | B_query(sf, grd, tt, ff, rt, (lp, uid)) ->
            let cd = fpath_scan_bexp_out_of_context_s "tertiary/query expression guard" lp cd grd
            let mf() = "operand to tertiary operator at " + lpToStr0 lp
            fpath_scan_exp mf lp (fpath_scan_exp mf lp (cc, cd) tt) ff // Unlike the guard, this is a valid context: mux-trees are allowed in forwarding paths.

        | B_applyf(info, rt, args, (lp, _)) -> 
            let mf() = sprintf "function apply of %s at %s" (pathToS info) (lpToStr0 lp)
            let scanarg cd (ty_, arg) = fpath_scan_exp_out_of_context mf lp cd arg
            let cd = List.fold scanarg cd args
            (cc, cd)


        //B_diadic(oo, ll, rr, ty) ->  fpath_scan_bexp msg (fpath_scan_bexp msg cccd ll) rr
        | B_diadic(oo, ll, rr, ty) ->
            let mf () = sprintf "Diadic operator %A at %s" oo (lpToStr0 lp)
            let cd = fpath_scan_exp_out_of_context mf lp cd rr
            let cd = fpath_scan_exp_out_of_context mf lp cd rr
            (cc, cd) // In the paper we mention we support simple ref-trans functions within the forwarding paths.  In the future they can also included diadic ops like this and it would be handy to abstract them out but we leave that out in this release.
        //| B_applylam
        | B_var _
        | B_format _
        | B_ifc _
        | B_ifc_immed _
        | B_subif _
        | B_hexp _
        | B_pliStmt _
        | B_maker_abs _  
        | B_maker_l _ 
        | B_applylam _

        | B_aggr _
        | B_reveng_h _  // TODO these are multiplexors we can forward over ...
        | B_reveng_l _      

        | B_moduleInst _ -> (cc, cd)
        
        | B_field_select(tag, arg, baser, width) -> //baser,width order deprecated
            // We support forwarding over field selects: they just turn into wiring that is 'free' when forwarded.
            // We augment lightweight forwarding path by composing a field select operator so that it applies after the existing forwarding function.
            let field_sel_select xx = lower_field_select baser width xx
            let apply_field_sel_select ((lp, rhs_tag), lvv, rdbus, fwd_func) = ((lp, rhs_tag), lvv, rdbus, fwd_func>>field_sel_select)
            let (cc, cd) = fpath_scan_exp mf lp (cc, cd) arg
            (map apply_field_sel_select cc, cd)

        //| B_wnode -- TODO say why commented out please
            
        | other ->
            //if bobj.forwarding_enabled ...
            dev_println (lpToStr0 lp + sprintf ": Ignored other expression form in forwarding_paths_prescan %A" (bsv_expToStr other))
            (cc, cd)


    let rec fpath_scan cd = function
        | Bsv_ifThenStmt(g, stm_t, stm_fo, strictf_, lp) ->
            let cd = fpath_scan cd stm_t
            let cd = if nonep stm_fo then cd else fpath_scan cd (valOf stm_fo)
            cd
        | Bsv_beginEndStmt(stmts, lp) -> List.fold fpath_scan cd stmts
        | Bsv_methodDef(mood, mname, proto, ephemeralf, rt, formals, guard, stmts, lp) ->
            let cd = fpath_scan_bexp_out_of_context_s "method-explicit-guard" lp cd guard
            List.fold fpath_scan cd stmts
        | Bsv_rule(iname, guard, stmts, keyname, lp) ->
            let cd = fpath_scan_bexp_out_of_context_s "rule-explicit-guard" lp cd guard
            List.fold fpath_scan cd stmts
        | Bsv_primBuffer(lhs, rhs, lp)    -> fpath_scan_exp_out_of_context_s "primbuf-l" lp (fpath_scan_exp_out_of_context_s "primbuf-r" lp cd lhs) rhs
        | Bsv_resultis(ee, lp_o)      ->
            let lp = if not_nonep lp_o then valOf lp_o else g_builtin_lp
            fpath_scan_exp_out_of_context_s "resultis" lp cd (snd ee)
        | Bsv_eascActionStmt(ae, lp_o) ->
            let lp = if not_nonep lp_o then valOf lp_o else g_builtin_lp
            fpath_scan_exp_out_of_context_s  "eascAction" lp cd ae
        | Bsv_varAssign _
        | Bsv_skip _
        | Bsv_varDecl _ -> cd
        //| other -> muddy (sprintf "Ignored other form in forwarding_paths_prescan %A" (bsv_stmtToStr other))


    let paths_raw = List.fold fpath_scan [] stmts
    //if not_nullp untrapped then hpr_yikes(sprintf "Found %i forwarding networks outside of regWrite contexts." (length untrapped))
    vprintln 3 (sprintf "Found %i forwarding networks need to be created from BRAM extensions" (length paths_raw)) 
    if not_nullp paths_raw then
        let report_fwding_network (cmd_name, lhs_idl, lhs, rhs, ty, lp) =
            let prhs (lp, bip, _, fwd_func) = sprintf "%s.[%s]" (bsv_expToStr bip) cmd_name // (bsv_expToStr subs)
            taggedlpToStr0 lp + " FWD:" + hptos lhs_idl (*bsv_expToStr lhs*) + " rhs=" + sfold prhs rhs
        reportx 3 "register forwarding paths needed:" report_fwding_network paths_raw

//WE NEED THE pipelined operator tag FROM THE RHS LIST I THINK

    // Prepare the datastructure for post render. This is also a collate operation.
    let add_forwarding_path (mm:fwdpaths_map_t) (cmd_name, lhs_idl, lhs, rhs_list, ty, (lp, tag_no)) =
        let name = funique(htos_net lhs_idl)
        let width = enc_width_e ww (fun()->"forwarding path width") ty
        let that = lhs_idl
        let record =
            match mm.TryFind that with
                | Some fwdpath_rec -> fwdpath_rec
                | None ->
                    let fwdpath_rec =
                        {
                            ty=       ty
                            //yval=   B_hexp(ty, g_net emits (["yval"; name], width))
                            register= X_undef
                            assigns=  []
                            cmd_name= cmd_name
                        }
                    fwdpath_rec
        let assigns =
            let add_assign cc ((lp, tag_no), bip, rdbus, fwd_func) =
                let flop = g_net emits (["scoreflop"; name], 1, [])
                let assign = 
                    {
                        rhs_tag=  (lp, tag_no)
                        rdbus=    rdbus
                        name=     name
                        scorebd=  flop
                        closed=   None // No activation expression yet
                        fwd_func= fwd_func
                        //fwd_func= fwd_func                
                    }
                assign::cc
            List.fold add_assign record.assigns rhs_list
        mm.Add(that, { record with assigns=assigns })
    let fwdpaths = List.fold add_forwarding_path fwdpaths paths_raw

    //let ww = WF 3 "rez_fwd_sigma_c_preloads" ww (sprintf  "Start for %s" msg)

    // Prepare the forwarding datastructure for main compilation.
    // We generate an initial entry in sigma_c for each forwarding path.
    let rez_fwd_sigma_c_preloads (sm:statemap_t) keyx (record:fwdpath_rec_t) =
        //  basis = B_applyf(PI_bno(B_dyn_b(g_sread, fwdpath_rec.register), None), Bsv_ty_dontcare "L3431", [], tag_this_lp g_builtin_lp)
        let add_route (sofar_guard, sofar_vale) assigner =
            let guard = ix_or sofar_guard (xi_orred assigner.scorebd)
            let rhs = (assigner.fwd_func)(assigner.rdbus)
            let vale = ix_query (xi_orred assigner.scorebd) rhs sofar_vale
            (guard, vale)
        //let basis = fwdpath_rec.register
        let (comp_alpha, comp_vale) = List.fold add_route (X_false, X_undef) record.assigns
        let dubious = false // FOR NOW - TODO - cannot forward and multi-cycle at once currently!
        let c1 = MS_denot((dubious, ["fwd_preloader"; "monica"]), "rez_fwd_sigma_c", comp_alpha, (record.ty, B_hexp(record.ty, comp_vale)), None, record.cmd_name)
        dev_println("done incrementally?")
        if nullp record.assigns then hpr_yikes(sprintf "rez_fwd_sigma_c forward path, bypassing %s, had zero items." (xToStr record.register))
        sm.Add(keyx, c1) // This straight add assumes called once - if done incrementatlly need a merge

    let statemap = Map.fold rez_fwd_sigma_c_preloads Map.empty fwdpaths // Create an initial statemap (aka sigma_c) from forwarding networks.
    (statemap, fwdpaths) // end of forwarding_paths_prescan 

and fwdpath_add_src ww fwdpath_rec dirinfo tag firex written_vale =
    //vprintln 0 (sprintf "fwdpath_add_src %s" (taggedlpToStr0 fwdpath_rec.tag))
    let rec closer = function
        | assign::tt->
            if ((snd assign.rhs_tag):int) = snd tag then
                let closure =
                  {
                    rhs=              written_vale
                    dirinfo=          dirinfo
                    fwdtrigger=       firex.fire
                  }
                let nv = { assign with closed=Some closure }
                nv :: tt
            else assign::closer tt
        | [] -> sf (sprintf "no assign close path found for %A" tag)

    { fwdpath_rec with assigns=closer fwdpath_rec.assigns } 



// End of main mutual recursion.
//=============================================================

//    
// For a module, first pre-augment the env with thunks for all definitions and then run them in turn.
//
let rec norm1_package_total ww bobj ee body =
    //dev_println (sprintf "norm1_package_total  static_path=%s"   (hptos ee.static_path))
    let premaps = norm1_lst ww bobj None ee body
    let pre_augment (em: genenv_map_t) (lp, k, v) =
        let instf = match v with
                     // This means instances are not available ahead of declaration textually.
                    | GE_normthunk(true, _, _, _, _, _) -> true
                    | _ -> false
        if instf then em // Skip instances at this point.
        else
            vprintln 3 (sprintf "norm1_total: preaugment: bound name '%s'" k)
            match em.TryFind [k] with
                | None -> em.Add([k], v)
                | Some x ->
                    if memberp k !g_toy_bsv_builtins then em
                    else cleanexit(lpToStr0 lp + sprintf ": pre-augment failed on '%s' - item defined more than once?  %A"  k x)

    // Rather crude means of handling out-of-order definitions: first add all premaps except instances to env...
    let ee = { ee with genenv=List.fold pre_augment ee.genenv premaps; } 
    vprintln 3 "preaugment done"
    // then 
    let norm1u ee = function
        | (lp, k, GE_normthunk(instancef, idl, ans, reaped, being_done, ff)) when nonep !ans ->
            let mf() = "norm1u " + htosp idl
            let ww = WF 2 "norm1u" ww (htosp idl)
            if not (nonep !being_done) then sf (mf() + " recursion being_done L9278")           
            let knot2 = ref None
            being_done := Some(Bsv_ty_knot(idl, knot2)) // GE_normthunk(instancef, idl, ans, reaped, being_done, ff))
            let mm = ("norm1u: thunk: " + htosp idl)
            let ww = WF 2 "norm1u" ww mm
            let qq = ee // if instancef then ee else { ee with genenv=ee.genenv.Remover[k]; }
            let augs = ff ww qq
            being_done := None            
            reaped := augs
            let kate_x1 arg cc =
                match arg with
                | Aug_env(tyclass, bound_name, (dmtof, who, vale)) ->
                    vprintln 3 ("kate_x1: defined " + htosp idl)
                    knot2 := Some(ee_sty mf vale)
                    ans := Some(GE_binding(bound_name, who, vale))
                    arg :: cc
                | other ->
                    //vprintln 0 (mf() + sprintf ": kate_x1 not nice %A" other)
                    arg ::cc // This only supports thunking for the regular env entries and not tags or tyclasses.

            //vprintln 0 (sprintf "norm1u: kate_x1 '%s' instancef=%A" k instancef)
            let augs' = List.foldBack kate_x1 augs []
            let ee = List.fold (augment_env_entry ww bobj mf "norm1u-L9278") qq augs'
            //vprintln 0 (sprintf "norm1u: Reinserted k=%A ok" k)
            ee
            
        | (lp, k, GE_normthunk(instf, idl, ans, reaped, being_done, ff)) when not(nonep !being_done) -> muddy("hit 25 " + htosp idl)            

        | (lp, k, GE_normthunk(instf, idl, ans, reaped, _, ff)) -> // Run already - a simple tidy up.
            let mf() = "norm1u RunAlready " + htosp idl + ":" + lpToStr0 lp
            let ww = WF 2 "norm1u" ww (mf())
            //vprintln 0 (mf() + sprintf ": Did remove k=%A and reaped augs are %A  instf=%A" k !reaped instf)
            let ee = if instf then ee else { ee with genenv=ee.genenv.Remover[k]; }
            let kate_augment1 (em:genenv_map_t) (k,v) =
                //vprintln 0 (mf() + sprintf ": kate_augment1 '%s' instf=%A" k instf)
                em.Add([k], v) // why use this and not augment_env_entry?  = no dmto errors  ...?
            //in { ee with genenv=kate_augment1 ee.genenv (k, valOf !ans); }
            List.fold (augment_env_entry ww bobj mf "normthunk-reaped") ee (!reaped)
    let ans = List.fold norm1u ee premaps
    ans
    
// norm1_lst makes a thunk for each member of a package that will call norm1a on that item.
and norm1_lst ww bobj tyclass ee lst =
    // Need to cope with forward references so introduce each definition into the env as a normable thunk. This will not handle mutual recursion?
    let thunkgen lp instf idl ats item =
        let _ = ins_ats bobj lp "package" bobj.prags idl ats // The attributes and pragmas are stored globally here.
        //vprintln 10 (sprintf "Preload a definition int the env for %s in case of fwd reference." (htosp idl))
        let _ = 
            match ee.genenv.TryFind[hd idl] with
                //| Some(GE_normthunk _) -> ()
                | Some x -> vprintln 0 (htosp idl + " +++ defined more than once error [L8980]. TODO a spurious errror ? " + olddef_ge x) // A second check! Always? a spurious error

                | None -> ()
        (lp, hd idl, GE_normthunk(instf, idl, ref None, ref [], ref None, fun ww ee -> norm1a ww bobj tyclass ee item))

    let rec premap ats item cc =
        match item with
            | KB_attribute(ats', mm, lp) -> premap (ats'@ats) mm cc
            | KB_typedefEnum(string_list, id, derives, lp) -> 
                let idl = id :: ee.prefix
                (thunkgen lp false idl ats item)::cc
            | KB_typedefConst(id, exp, lp) -> 
                let idl = id :: ee.prefix
                (thunkgen lp false idl ats item)::cc
            | KB_typeclassInstanceDef(name, sifgs, provisos, vflist, nameo, lp) ->
                let idl = name :: ee.prefix
                (thunkgen lp true idl ats item)::cc
            |  KB_typeclassDef(tyclass_id, generics, provisos, contents, nameo, lp) ->
                let idl = tyclass_id:: (ee.prefix)
                (thunkgen lp false idl ats item)::cc
            | KB_moduleDef(KB_moduleSig(backdoors, mkname, parameters, iftype, args, provisos, lp), body, nameo) ->  
                let idl =  mkname :: (ee.prefix)
                (thunkgen lp false idl ats item)::cc
            | KB_typedefSynonym(KB_type_dub(true, name, generics, lp), ty, lp_) ->
                let idl = name :: ee.prefix
                (thunkgen lp false idl ats item)::cc

            | KB_interfaceDecl(KB_type_dub(true, name, generic_formals, lp), items, nameo, lp_) ->
                let idl = name :: ee.prefix
                (thunkgen lp false idl ats item)::cc
            | KB_typedefSTUnion(key, members_or_variants, KB_type_dub(true, id, generics, lp), derives, lp_) ->
                let idl = id :: ee.prefix
                (thunkgen lp false idl ats item)::cc
            | KB_varDeclAssign(tyo, lhs, ro, lp) -> 
                match lhs with
                    | KB_subscripted(dotflag, KB_id(id, _), _, _, _)
                    | (Let false (dotflag,  KB_id(id, _))) -> //note: scpu imem was defined before imemsize without this despite being in sensible textual order...
                        let idl = id :: ee.prefix
                        (thunkgen lp false idl ats item)::cc
                    | other -> cc

            //| KB_functionDef3(proto, true, body, _, lp) -> cc // TODO why ignored? silly.
            | KB_functionDef3(proto, _(*false*), body, _, lp) -> 
                let name =
                    match proto with
                        | KB_functionProto(return_type, name, formals, provisos, lp) -> name
                let idl = name :: ee.prefix
                (thunkgen lp false idl ats item)::cc
            | other  -> sf(sprintf "Premap other form: %A\n\n" other)
    let premaps = List.foldBack (premap []) lst []
    premaps

//
// Process a module definition. Typecheck it and store it as a generator and in the env.
//    
and perhaps_norm_moduleDef ww bobj ee msg em tyclass ats idl (siga, bodyo) =
    let mf() = "perhaps_norm_moduleDef + " + hptos idl
    let mm = mf()
    let vd = bobj.normalise_loglevel
    let prec = None// Some P_defn1
    let (mkname, backdoors, provisos, (parameters, args', iftype'), (ifc_sty, maker_type, aliases), lp) = prep_module_sig ww bobj idl msg mm ee prec siga

    // If the new structure has no free/formal tyvars then it is dispatched on first encounter and the result saved for any subsequent encounters.
    // (That's just an optimisation of the general procedure)
    // else, if we are ungrounded (definition context) then we need make and save and abstract binding,
    // else, when we are grounded (use context) (have a concrete callpath) we retrieve an earlier abstract binding and rehydrate.
    // The same goes for other polymorphic definitions, which include interfaces and functions.


    let (grounded', m0) = // Check grounded: ie no unbound type vars or parameters.
        let freshlist = get_freshlist ww maker_type [] // 1. A get_freshlist should be end-bracketed by saving an abstract binding. 2. cf freshlister?.
        // We don't like that get_freshlist is called here and in the generic afreshen.  Modules are different since they only appear at the top level of a package and packages dont seem to have parameters, but nonetheless, a uniform treatment is preferable. We're just seeig if grounded though.
        let grounded' = ee.grounded && nullp freshlist
        let m0 = sprintf  "'%s' freshlist=^%i grounded'=%A" mkname (length freshlist) grounded' 
        (grounded', m0)

    let ww = WF 3 "perhaps_norm_moduleDef" ww ("Start " + m0)

    //let fx = ((if iftype=None then [] else [valOf iftype]) @ map f2o3 (parameters @ args))
    if vd>=5 then
        vprintln 5 (sprintf "%s  iftype'=%A prams=%A args=%A grounded'=%A" m0 (bsv_tnToStr ifc_sty) parameters args' grounded')
        vprintln 5 (m0 + mf() + mkname + sprintf "siga=%A" siga)

    let ee = if grounded' then { ee with callpath=CPS(PS_prefix "L10626", map (fun x->(x, 0, None)) ee.static_path); } // The call paths in a grounded module can possibly be based on the static path and we need only deabs it once - but watch out for numeric types if you try this!
             else ee
    let provisos' = List.fold (proviso_parse ee) [] provisos

    let rootmarked = memberp Pragma_synthesize ats 
    if rootmarked then
        mutaddonce bobj.m_synths (idl, (lp, ee.skip_synthesise_directives))
        vprintln 1 (sprintf "Module %s designated as a/the synthesis root. skip_it=%A" (htosp idl) ee.skip_synthesise_directives) 

    match bodyo with
        | Some body -> 
            vprintln 3 (sprintf "norm_moduleDef: %s: saved abstract regardless" (htosp idl))
            // It would be better if prep had done a recast.
            let deepbind_module_pram (em:genenv_map_t) pairs = grave_deepbind_arg_ty_list ww vd mf None aliases em (*[], evty_create_indirection_flt "L10502"*) pairs []

            let ee_body = // Body either has type ifc->$M or else (_ ->(ifc->$M)) - we prefer the latter always with a dummy unit apply being inserted by the front end (parser/prep).
                match maker_type with
                | Bsv_ty_nom(_, F_fun(Bsv_ty_nom(_, F_fun _, _), _, argsig_formals), _) ->
                    let allformals = parameters @ args'
                    arity_check1 mf "L8341" argsig_formals allformals
                    { ee with genenv=deepbind_module_pram ee.genenv (List.zip allformals argsig_formals); }
                | Bsv_ty_nom(_, F_fun(rt, _, [ifc]), _) ->
                    let allformals = parameters @ args'
                    arity_check1 mf "L8363" [] allformals
                    ee 
                | other ->
                    cleanexit(mf() + ": inappropriate form applied as a module maker : " + bsv_tnToStr other)

            let ee_body = { ee_body with callpath=CPS(PS_suffix, []) }
            let (ee_, tbh, h0, items, conflicts) = typecheck_topper ww bobj ee_body mf (Some ifc_sty) body
            let ww = WF 3 m0 ww ("typechecked - needs normalising")
            let body_gamma = tbh.tve
            //dev_println (mf() + sprintf " tbcc dropings are %s" (sfoldcr_lite (fun (a,b,c)->cpToStr b) tbh.tve))
           
            let nf_maker_type = sum_nf ww bobj ee (mf() + " moduleDef site") (h0, Some body_gamma) maker_type 

            let _ =
                if false then
                    vprintln 0 (mf() + ": typechecked module : got " + bsv_tnToStr maker_type + " with constr " + sfold hindToStr h0)
                    vprintln 0 (mf() + ": typechecked module : normal'd to " + bsv_tnToStr nf_maker_type)
                    vprintln 0 (mf() + ": typechecked module : body_gamma=" + sfold (fun (id,b,c)->id + ":" + cpToStr b + ":::" + droppingToStr c) body_gamma)
            let rec nofreetypes = function
                | other -> sf (mf() + sprintf ": nofreetypes other form: " + bsv_tnToStr other)
            let rec nofreetypes_h = function
                | (None, _, _)    -> false
                | (Some ty, _, _) -> nofreetypes ty
            let groundling = // TODO delete this bit - currently/now always returns None.
                match nf_maker_type with
                    | Bsv_ty_nom(_, F_fun(Bsv_ty_nom(_, F_fun(_, _, ifc), _), _, argsig_formals), _) when false ->
                        let allformals = parameters @ args'
                        let f____ = (nonep tbh.rv || nofreetypes (valOf tbh.rv)) && conjunctionate (nofreetypes_h) h0
                        Some(ifc, argsig_formals)

                    | other -> None
                    //| other -> sf (mf() + sprintf ": gounded maker had other signature: " + bsv_tnToStr other)
            let (exists, ov_) = bobj.m_modgens.TryGetValue idl
            let mod_mk = B_maker_abs(idl, ats, (rootmarked, parameters, args', iftype', aliases), provisos', (nf_maker_type, body), ee, backdoors, lp)
            let ww = WF 3 m0 ww ("typechecked - post normalise")
            let nb =
                match groundling with
                | None ->
                    vprintln 3 (sprintf "%s adding to g_modgens database" (htosp idl))
                    if not exists then bobj.m_modgens.Add(idl, mod_mk)//This global dictionary is a backdoor only used to find top-level synthesis roots. These should be grounded.
                    (false, {g_blank_bindinfo with whom="generatoraddgrounded"}, EE_e(maker_type, mod_mk))                    
                | Some(ifc_ty, iactuals2) -> muddy "deabs4_exp call no longer made here ..."
            // KLUDGE! TODO - assume those that need g_modgens are not typeclasses...
            vprintln 3 (sprintf "perhaps_norm_moduleDef: %s added to g_modgens database. type=%s  bg^=%i" (htosp idl) (bsv_tnToStr nf_maker_type) (length body_gamma))
            let items = [ Aug_env(tyclass, [mkname], (*idl,*) nb) ]
            (maker_type, items)

        | None ->
            //vprintln 3 (sprintf "perhaps_norm_moduleDef: %s silent ignore ... no body" (htosp idl))
            (maker_type, [])


// Some norm functions are invoked directly as the AST is read in (under cmdline or import control).  This is suitable for grounded types.
// Other norm functions are only invoked on demand as type and parameter bindings become available.            
// The invoked norm functions vary as to whether they are used elsewhere and hence do not contain a typechecking call and hence do not need one here.
//            
and norm1a ww bobj (tyclass:tyclass_marker_t) ee aa =
    let qm = "norm1a"
    let qmf() = qm //if ee.forcef then "norm1a-e" else "norm1a-l"
    let ifc_sty = None // No interface in scope here - this is for a package.
    let vd = bobj.normalise_loglevel
    //dev_println (sprintf "%s %s  static_path=%s"  qm qm (hptos ee.static_path))
    match aa with
        
    | KB_varDeclAssign(tyo, lhs, ro, lp) ->
        let mf() = lpToStr0 lp + " structural-2 varDeclAssign"
        let ww = WF 3 qm ww (mf())
        let (ee, tbcc_, h0_, items_, conflicts_) = typecheck_topper ww bobj ee mf ifc_sty [aa] 
        let (ee_, cc_, kvl) = norm_decl_assign ww bobj (qm + ":" + mf()) tyclass (ee, []) aa
        kvl
        
    | KB_functionDef3(proto, assf, body, nameo, lp) ->
        let name = 
            match proto with
                | KB_functionProto(return_type_raw, name, formals_raw, provisos, lp) -> name
                | _ -> sf "L10226"

        let mf() = lpToStr0 lp + sprintf " package functionDef '%s'" name + " " + (if assf then " by assignment" else "")
        let ww = WF 3 qm ww (mf())
        // A package is not already typechecked so must typecheck each item prior to processing it further, like these package functions.
        let ee_float = { ee with callpath=CPS(PS_suffix, []); }
        let (ee_, tbh22, h22, items22, conflicts_) = typecheck_topper ww bobj ee_float mf ifc_sty [aa]
        let (kv, _, name) = norm_functionDef ww bobj tyclass mf (ee, []) ifc_sty (KB_functionDef3(proto, assf, body, nameo, lp), Some(name, tbh22, h22, items22))
        if nameo<>None && valOf nameo <> name then cleanexit(mf() + ": end name does not match start: " + name + " cf " + valOf nameo)
        vprintln 3 (mf() + " finished")
        [kv]

    | KB_typedefSTUnion(key, members_or_variants, KB_type_dub(true, id, generics, lp), derives, lp_) ->
        if memberp id !g_toy_bsv_builtins then []
        else
        let prec = None
        let bound_name = id :: ee.prefix // realy ?
        let idl = bound_name // YES.
        let arity = length generics
        let mf() = lpToStr0 lp +  " defining Struct/TaggedUnion " + key + (if arity=0 then " " else "<" + i2s arity + "...> ") + htosp idl
        let ww = WF 3 "norm1"  ww (if vd>=4 then mf() else "defining Struct/TaggedUnion")
        let x = ee.genenv.TryFind[id]
        let _ =
            match x with 
            | Some(GE_normthunk _) -> vprintln 3  (mf() + ": defined more than once. [L7490]. ") // + olddef_ge(valOf x))
            | Some x ->
                cleanexit(mf() + ": defined more than once. [L7490]. " + olddef_ge x)
                ()
            | None -> ()
                        
        let gg_newkey sty c = function 
            | KB_builtin(_) -> c
            | KB_structMember(_, tag) -> c // sub-unions ? TODO
            | KB_unionMember(_, tag)
            | KB_unionSubStruct(_, tag) ->
                //vprintln 0 ("New tag " + tag)
                (tag, (sty, -1)) :: c // this -1 is the constructor arity (always 1?)TODO 
            | other -> sf (mf() + sprintf "gg_newkey other %A" other)

        let augs =
            if arity > 0 then
                let k44t = ref None // we dont need a knot here if sty has its own?
                let sty1 = Bsv_ty_knot(idl, k44t)
                let newtags_aug = Aug_tags(List.fold (gg_newkey sty1) [] members_or_variants)
                let ee_i1 = { ee with genenv=ee.genenv.Add(bound_name, GE_binding(bound_name, {g_blank_bindinfo with whom="stunion def"}, EE_typef(G_none, sty1))); }
                // now aug this ee with newtags_aug ... could use this to add the knot as well.
                let ee_i2 = augment_env_entry ww bobj mf "augs-L12385" ee_i1 newtags_aug
                let sty2 =
                    let sty191 = collect_sum1 ww bobj (ee:ee_t) mf prec (VP_value(false, [])) (KB_typedefSTUnion(key, members_or_variants, KB_type_dub(true, id, generics, lp), derives, lp))
                    // vprintln 0 (sprintf "dont like putting in temp for '%s' when we have to hand %A " (htosp idl) sty191)
                    sty191
                let a = Aug_env(tyclass, [id], (*idl,*) (false, {g_blank_bindinfo with whom="mi1001"}, EE_typef(G_none, sty2)))
                //
                k44t := Some (sty2)
                //let ne = GE_type_abs(idl, nt, lp) // Use the GE_ty_knot here?   Very complex! If not expanded here why have this ? TODO  why do we have two sites!!!!!!!!!!!!1 for deabstracting strun? and why does other not use ge_type_knot?
                //let _ = k44e := Some ne  //<--   Dont do this till untied!
                [ newtags_aug; a ]
            else
                // Cannot be recursive if has no parameters. TODO really ? let ee = { ee with genenv=ee.genenv.Add(id, GE_type_lo(idl, Bsv_ty_knot(idl,knot), na)); }
                // What about lasso form?
                // TODO What about inner abstract types that may need alpha convert when used?
                let recf = false 
                let sty191 = collect_sum1 ww bobj (ee:ee_t) mf prec (VP_value(false, [])) (KB_typedefSTUnion(key, members_or_variants, KB_type_dub(true, id, generics, lp), derives, lp))

                if true then
                    let a = Aug_env(tyclass, [id], (*idl,*) (false, {g_blank_bindinfo with whom="mi1001"}, EE_typef(G_none, sty191)))
                    let rec sty_get_tags cc = function
                        | Bsv_ty_sum_knott(_, x) -> ge_get_tags cc !x
                        | Bsv_ty_nom(idl, F_tunion(mrecflag, variants, details_), sl) ->
                            let xx cc = function
                                | (tag, _) -> tag::cc
                            List.fold xx cc variants
                        //| x -> sf(sprintf "w3iggle %A" x)
                        | _ -> cc
                    and ge_get_tags cc = function
                        | GE_normthunk(instf, idl, ans, reaped, _, ff) when not(nonep !ans) -> ge_get_tags cc (valOf !ans)
                        | x -> sf(sprintf "w4iggle %A" x)
                    let newkeys = List.fold sty_get_tags [] [sty191]
                    let newkeys = map (fun s -> (s, (sty191, -1))) newkeys
                    [Aug_tags newkeys; a]
                    
                else []
        augs


    | KB_interfaceDecl(KB_type_dub(true, name, generic_formals, lp), items, nameo, lp_) ->  // This is really a type and so could be fully checked in a prior typechecking phase but we dont have one for package-level declarations. Perhaps we should norm package items in two passes to support better forward references.
        let q0 = "typecheck_interfaceDecl"
        let prec = None//Some P_decl0 
        let iidl = name :: ee.prefix
        let arity = length generic_formals
        let mf() = lpToStr0 lp + " interface declaration + " + htosp iidl + (if arity=0 then "" else "<" + i2s arity + "...>")
        let ww = WF 3 q0 ww (mf())
        let sty1 = collect_sum1 ww bobj ee mf prec (VP_type) aa
        vprintln 3 ("Defined interface " + htosp iidl + " pre freshen " + bsv_tnToStr sty1)
        // Having fresh tyvar names sure helps with debugging but not really needed.
        let sty2 = afreshen_sty ww bobj q0 None sty1
        //vprintln 3 ("Defined interface " + htosp iidl + " post freshen " + bsv_tnToStr sty2)
        let n =
            if arity > 0 then
                let ans = fun () -> muddy "Bsv_ty_abs_ifc(iidl, (generic_formals, sty2), items, ee, lp) //TODO --- never matched in any code - we use sty2 now."
                if iidl = ["Reg"; "Prelude"] then g_reg_backdoor := Some(sty2)

                Aug_env(tyclass, [name], (*iidl,*) (false, {g_blank_bindinfo with whom="interfaceDecl"}, EE_typef(G_none, sty2)))
            else
                let ats = ats_get bobj.prags iidl
                let ans = norm_ifc_decl ww bobj ee lp iidl  [] ats (sty2, items)
                Aug_env(tyclass, [name], (*iidl,*) (false, {g_blank_bindinfo with whom="interfaceDecl"}, EE_typef(G_none, ans))) // TODO false? just made that up!!!!
        [n]


    | KB_typedefSynonym(KB_type_dub(true, name, generics, lp_), ty, lp) -> // norm1e call site 1
        let mf() = lpToStr0 lp + "typedefSynonym norm1e"
        operate_typedefSynonym ww vd bobj mf tyclass ee aa

    | KB_typedefConst(id, exp, lp) -> // norm1e call site 2
        let mf() = lpToStr0 lp + "typedefConst norm1e"
        operate_typedefSynonym ww vd bobj mf tyclass ee aa
        
    | KB_typedefEnum(string_list, id, derives, lp) -> // norm1e call site 3
        let mf() = lpToStr0 lp + "typedefEnum norm1e"
        operate_typedefSynonym ww vd bobj mf tyclass ee aa
        
    | KB_moduleDef((KB_moduleSig(backdoors, mkname, parameters, iftype, args, provisos, lp)) as siga, body, nameo) ->  
        // We can set a static callpath if we are gounded, meaning there are no outer free typevars and none in the current signature . hence we can make a squirrel 
        // TODO does a tyclass effect this ?
        // TODO should the same simply apply for all of norm0
        let idl =  mkname :: (ee.prefix)
        let ee = { ee with static_path= idl; module_path= idl; } 
        let mm = htosp idl
        let mf() = lpToStr0 lp + " moduleDef " + sfold (fun x->x) backdoors + mm
        let ww = WF 3 qm ww (mf() + " start")
        if nameo<>None && valOf nameo <> mkname then cleanexit(mf() + ": end name does not match start: " + mkname + " cf " + valOf nameo)
        let ats = ats_get bobj.prags idl
        let (ty_, r) = perhaps_norm_moduleDef ww bobj ee mf mm tyclass ats idl (siga, Some body)
        r
        
    |  KB_typeclassDef(tyclass_id, generics, provisos, contents, nameo, lp) ->
        let prec = None
        let idl = tyclass_id:: (ee.prefix)
        let mm = htosp idl
        let mf() = lpToStr0 lp + " typeclass definition + " + mm
        let ww = WF 3 qm ww (mf())
        if nameo<>None && valOf nameo<>tyclass_id then cleanexit(mf() + ": end name does not match start: " + tyclass_id + " cf " + valOf nameo)
        let provisos' = List.fold (proviso_parse ee) [] provisos
        // Contents are normally functionProtos but makers and values are also possible as in the DefaultValue typeclass.
        let (ee', tbinds) = tbindxx_new ww bobj mf ee (List.zip3 [1..length generics] (map f1o3 generics) (map f2o3 generics))
        let protofun (c, bodies) = function
            | KB_varDeclAssign(Some ast_ty, KB_id(name, (lp, uid)), None, lp_) -> // Error if init is not None ?
                let mf() = lpToStr0 lp + " typeclass declAssign + " + name
                let ww = WF 3 qm ww (mf())
                let ty' = collect_sum1 ww bobj (ee:ee_t) mf prec (toVP (SO_none "tyckass/-declAssign") name)  ast_ty 
//                    muddy ("norm_bsv_type ww bobj mm ee' (SO_none \"protofun\") ty      // This is too much norming at this point ....? get anno" + uid)
                let s1 = (name, ty')
                //in muddy (mf() + sprintf "make an abs var here...? %A" ty')
                (ty' ::c, s1::bodies)

            | (KB_moduleSig(backdoors, mkname, parameters, iftype, args, provisos, lp)) as siga ->
                let ats = [] // abstract monad has no attributes
                let ww = WN ("tyclass moduleSig + " + mkname) ww
                let (ty, kvl) = perhaps_norm_moduleDef ww bobj ee mf mm tyclass ats idl (siga, None)
                vprintln 3 (lpToStr0 lp + sprintf ": Overloaded module mk defined '%s'" mkname)
                let s1 = (mkname, ty)
                // module type should be added to c and as a pair to bodies
                (ty::c, s1::bodies)

            | (KB_functionProto(return_type, name, formals, provisos, lp)) as siga -> // norm1a tyclass
                let ww = WN ("tyclass functionProto + " + name) ww
                //
                let (fun_ty) = perhaps_norm_functionProto_2 ww bobj ee' prec siga
                let s1 =
                    match fun_ty with
                        | Bsv_ty_nom(_, F_fun(rt, mats, formals), _) ->
                        //| (Bsv_ty_fun(sty, nameo, rt, formals, provisos, lp_)) ->
                            let _ = 0//if mats.methodnameo = None then cleanexit(mf() + " name needed for overloaded function")
                            in (mats.methodname, fun_ty)

                        | other -> sf (sprintf "plit9148 %A" other)

                (fun_ty :: c, s1::bodies)
            | other -> cleanexit(mf() + sprintf ": unsupported form of typeclass entry %A" other)

        match ee.typeclasses.TryFind tyclass_id with
            | None   -> ()
            | Some x -> cleanexit(mf() + " defined more than once error [L8761].") // + olddef_ge x

        let (bodies, defs) = List.fold protofun ([], []) contents
        
        [ Aug_tycl(tyclass_id, idl, bodies, defs, lp) ]

    | KB_typeclassInstanceDef(name, sifgs, provisos__, item_list, nameo, lp) ->   // provisos currently ignored.
        let idl = name :: ee.prefix
        let mm = lpToStr0 lp + " : norm1: typeclassInstanceDef " + htosp idl
        let ww = WF 3 qm ww (mm + ": start")
        let mf() = mm
        if not_nonep nameo && valOf nameo <> name then cleanexit(mm + ": end name does not match start: " + name + " cf " + valOf nameo)        
        if not_nonep tyclass then cleanexit(mm + " nested instance declarations not allowed.")
        let marshal = function
            | (Let [] (args, KB_id(sbn, (lp, _))))
            | KB_type_dub(_, sbn, (*these tyvar args should equally be eval'd here in same env *)args, lp) ->                
                //vprintln 0 (mm + sprintf ": +++ skipped eval of instanceDef pram arg %A" args) // TODO please make a test to exercise this.
                match ee.genenv.TryFind[sbn] with
                    | Some (GE_binding(bound_name, _, EE_typef(_, ty)))  ->
                        let idl = digest_cooked mf ty
                        (idl, args)
                    | Some a -> sf(mm + sprintf ": Encountered other parameter form %A" a)
                    | None ->
                        let vd = 0
                        vprint vd "In-scope identifiers are: "
                        for z in ee.genenv do ignore(vprint vd (htosp z.Key + " ")) done 
                        vprintln vd ""
                        cleanexit(mm + sprintf ": type identifier not bound '%s'" sbn)
            | other -> sf(mm + sprintf ": other form %A" other)
        let tyclass1 = Some(map marshal sifgs)
        let rr = list_flatten (map (norm1a ww bobj tyclass1 ee) item_list)
        let ww = WF 3 qm ww (mm + ": finished")
        rr
        
    | other -> sf ("toplevel norm1 ast other " + sprintf "%A" other)




let render_forwarding_activations ww lower emits msg (fwdpaths:fwdpaths_map_t) =
    let ww = WF 2 "render_fowarding_logic" ww (sprintf  "Start for %s" msg)
    let render_fwd_activation _ key_ fwdpath_rec =
        let add_act assigner =
            match assigner.closed with
                | Some closed ->
                    let s1 = gen_dflop(X_true, assigner.scorebd, xi_blift closed.fwdtrigger)
                    mutadd emits.synch_logic (closed.dirinfo, [s1]) 
                | None ->
                    hpr_yikes(sprintf "skipping unclosed forwarding path for %s" msg)
        app add_act fwdpath_rec.assigns
    let _ = Map.fold render_fwd_activation () fwdpaths
    ()
    

//typedef enum {False, True} Bool deriving (Eq, Bits, Bounded);   Bsv_ty_enum(["Bool"], 2, [ ("False", 0); ("True"; 1) ])
//function Bool not(Bool x);
//function Bool (&&)(Bool x, Bool y);
//function Bool (||)(Bool x, Bool y);

//Used for ptional values.
//    typedef union tagged {        void Nothing;        a Just;        } Maybe #(type a) deriving (Eq, Bits);
    
// Variables like 'rd', 'rs1', 'rs2' etc. are of type 'RegNum', whereas arrays are indexed by numeric types such as integers, bits, and so on.  By applying 'pack', we convert a 'RegNum' type into a 'Bit#()' type, which is legal as an index.

//typeclass Bits#(a, n);
//    function Bit#(n) pack     (a x);
//    function a unpack (Bit#(n) y);
//endtypeclass
type predef_t = string list * (bsv_type_t * bsv_proviso_t list * int)

// Uppercase native types built in to the compiler.
let predef_lst:predef_t list =
    [
        //typedef Bit#(32) Nat; // is now in the preamble.
        // OInt#(n) is an n-bit unary (one hot) integer encoding.
        // Note Bit#(16) is a synonym for bit[15:0] 
    // (["Bool"],    (g_bool_ty, [], 0)); // Now in Prelude.bsv
    (["Stmt"],    (Bsv_ty_fsm_stmt "Stmt", [], 0));    
    (["Fmt"],     (Bsv_ty_format,  [], 0));        
    (["Integer"], (Bsv_ty_integer, [], 0));  // This is used in elaboration only and has no bound.

    // String should be instead defined in the Prelude: typedef Vector#(?, Int#(8)) String;
    (["String"],  (g_string_ty, [], 0));

    // "module" might be included here, as well as Action ?
    // Empty is in the Prelude.bsv
    
    // UInt and Int do not need to be built in. They can be defined in the preamble from more-basic constructs.
    // and they implement all the intstyle_tyclasses

    (["UInt"],  (Bsv_ty_nom({g_blank_nom_typ_ats with nomid=["UInt"]; cls=intstyle_tyclasses; },    F_ntype, [Tid(VP_value(true, ["width"]), "width")]), [],  1)); // Int and UInt take width parameters always. 
    (["Int"],   (Bsv_ty_nom({g_blank_nom_typ_ats with nomid=["Int"]; cls=intstyle_tyclasses; },     F_ntype, [Tid(VP_value(true, ["width"]), "width")]), [], 1)); // TODO defined the same here!

    // The type bit[m:0] is synonymous with Bit#(mf() + 1)
    (["Bit"],    (Bsv_ty_nom({g_blank_nom_typ_ats with nomid=["Bit"]; cls=intstyle_tyclasses; },    F_ntype, [Tid(VP_value(true, ["width"]), "width")]), [], 1)); 
    (["Vector"], (Bsv_ty_nom({g_blank_nom_typ_ats with nomid=["Vector"]; }, F_vector,  [Tid(VP_value(true, ["vec_length"]), "vec_length"); Tid(VP_type, "vec_ct")]),      [], 2));


    // Void needs adding

    ]


let insert_predefs e0 =
    let augment (g0 : predef_map_t) (idl, (ty, ats, numeric_arity)) =
        vprintln 3 ("Insert builtin type " + htosp idl)
        g0.Add(idl, ty) // , numeric_arity, g_builtin_lp))
    List.fold augment e0 predef_lst

// "Integer"s and "numeric type"s are treated as separate entities by the bluespec compiler. Integers cannot be converted to numeric types, but numeric types can be converted to Integers (using valueof and sizeof). My strategy therefore is to define "constants" like data_len as numeric types rather than Integers. 


// norm0 is called for the command line files and for Import statements.
// norm0: Top-level function that receives an abstract syntax tree for a file from the parser. This needs to store all definitions
// in data structures ready to be retrieved once we start processing synthesis targets.
// There is no call path here since nothing is being called.
let rec norm0 ww0 bobj (ee:ee_t, existing_constags) args =
    let qm = "norm0" //if ee.forcef then "norm0-e" else "norm0-l"
    let ee =
       { ee with
             // These ones are fresh for each rule or method module ; TODO but here we make them new for each package! hmmm.
             vectors=    new OptionStore<string, vector_t>("vectors")
             muts=       new muts_t("muts")
             elab_steps= ref bobj.elab_limit
       }

    let incdir_path = bobj.incdir_path
    let rec nest_flatten arg cc =
        match arg with
            | KB_nestlist lst -> List.foldBack nest_flatten lst cc
            | other -> other :: cc

    let process_import ww (ee, existing_constags) = function
        | KB_import(pname, None, lp) -> // import all of
            // BSV supposedly has some syntax for selective import: not just everything, which we might implement one day.  See KB_packageIdentifier.
            let mf() = lpToStr0 lp + " : import " + pname
            if memberp pname ee.imported then
                vprintln 3 (sprintf "Skip import of '%s' since already in scope" pname)
                (ee, existing_constags)
            else
                let qm = "norm0 import " + pname
                let ww = WF 2 qm ww0 (mf() + ": start")
                let (filename, _) = pathopen incdir_path 6 [';'; ':'] pname "bsv"
                if filename=None || not (existsFile(valOf filename)) then cleanexit(mf() + ": BSV path=" + incdir_path + "\nCannot open for import filename=" + pname + "\nInclude of separate files for compilation is implemented but import of pre-compiled modules is not")
                let imported_ast = lexbsv_parse ww false bobj.print_alltrees existing_constags (valOf filename)
                let ww = WF 2 qm ww0 (mf() + ": parsed")
                match imported_ast with
                    | None -> (ee, existing_constags)
                    | Some (imported_ast, constags) ->
                        let ww = WF 1 "norm0" ww ("Parsed " + valOf filename + " and now processing")
                        let ee1 = { ee with skip_synthesise_directives=true }
                        let (ee', existing_constags) = norm0 ww bobj (ee1, constags@existing_constags) imported_ast
                        let ww = WF 1 "norm0" ww ("Processed " + valOf filename)
                        let ee = { ee' with skip_synthesise_directives=ee.skip_synthesise_directives }
                        (ee, existing_constags)
        | x -> muddy (sprintf "Package import of a subset of components %A" x)
    let import_pred = function
        | KB_import _ -> true
        | _ -> false

    let rec norm0l ww ee args =
        let (imports, body) = groom2 import_pred (List.foldBack nest_flatten args [])
        let ww1 = WF 1 "norm0" ww (sprintf "%i imported packages to process" (length imports))
        let (ee, existing_constags) = List.fold (process_import ww1) (ee, existing_constags) imports
        let ww1 = WF 1 "norm0" ww (sprintf "%i imported packages all processed" (length imports))        
        let (ans, _) = List.fold (norm01 ww1) (ee, existing_constags) body
        let _ = WF 2 "norm0" ww ("finished package")
        (ans, existing_constags)
        
    and norm01 ww (ee00, existing_constags) arg00 =
        let ee = ee00
        let restore_ee ee = { ee with prefix=ee00.prefix; static_path=ee00.static_path }
        match arg00 with
        | KB_package(name, exports, body, nameo) ->
            if nameo<>None && valOf nameo <> name then sf ("package end name is not the same as start: " + name + " cf " + valOf nameo)
            if exports<>[] then vprintln 0 ("+++ " + name + " export ignored for now")
            let ww = WF 2 qm ww0 (sprintf ": package %s opened" name)
            let (imports, body) = groom2 import_pred (List.foldBack nest_flatten body [])
            let (ee, existing_constags) = List.fold (process_import ww) (ee, existing_constags) imports 
            let ee = restore_ee ee
            let ee = { ee with static_path= [name]; module_path= [name]; prefix= [ name ]; imported=name::ee.imported }
            let ww = WF 2 qm ww0 (sprintf ": package %s start body items (no=%i)" name (length body))
            let ee = norm1_package_total ww bobj ee body
            let ww = WF 1 qm ww0 ("Bsv package " + name + " parsed and assimilated ok.")
            (restore_ee ee, existing_constags)

        | other ->
            let ww = WF 2 qm ww0 (sprintf ": start body item - no package name.")
            let ee = { ee with static_path= []; prefix=[]; module_path= [] } // Discard name of last package or prelude that may be hanging around in ee.        
            let ee = norm1_package_total ww bobj ee [other] // hmmm handle items outside a package in textual order one at a time
            let ww = WF 1 qm ww0 ("body item parsed and assimilated ok.")
            (restore_ee ee, existing_constags)
    let ans = norm0l ww0 ee args
    ans





let setup_extra_recursion bobj =
    g_e_recs := Some(fun ww mm v -> bsv_manifest ww bobj mm v)
    ()



(*

 After all the source code is parsed and typechecked we do the main
 compilation stage as follows.

 Main compilation step for a synthesis root.


The naive model of Bluespec staging is that, in the first stage, the
haskell-like functions are all run to elaborate the design to result
in a static structure of B_ expressions.  These range over method
calls, that are turned into hardware interface operations, but no
function calls remain.

 We do a depth-first tree walk of the module makers using bsv_instantiate_module. The leaves are
 primitive components and the rest is mainly wiring and control
 multiplexors and the odd ALU where rule and method bodies perform
 arithmetic.  For each instantiated module we generate its 'currency'
 which is a list of nets that form its exported interface. We then recurse on each
 component it instantiates and then we do a 'wire_methods' and 'compile_local_rules'
 which generates the method contents.

 The currency for an interface includes all of its subinterfaces. These have additional
 dotted components in their name. But the subinterfaces are all folded up and held in
 the currency under the interface's monica and may not be listed in an error report.

The execution_order attribute is used to ... this is an old write up where the execution order could not be respected. It is critical for bluewires and now we instead ...

 When an interface is assigned or returned and then assigned we can either get wiring
 to be placed between it and where it is instantiated or we can insert a redirect in the
 currency so that modules that attempt to connect to the assigned interface instead bind
 to the underlying implementation directly.

The scheduler makes use of the descending_urgency, conflict_free and mutually_exclusive attributes.
 One variation is that the scheduller is now a post processing step which is run
 to generate additional master control logic after the main compilation.


When a function is applied to the result of a method it cannot be fully evaluated. An example
is subscripting a list by a register, which in the cth-tiny is the assembled program indexed
by the IPL load address register. Hence the functions can only be symbolically elaborated to yield
an expression tree and this is potentially infinite when functions recurse.

Our solution is to defer elaboration of function calls ... so ewalk has to handle them ... not the full story.



Semantic question: Does the package construct defined a namespace-like prefix that is prepended to the full name of anything defined
inside the package, or does the package name not really effect the names of its contents?
 *)


// eof
