// CBG SQUANDERER : Rudimentary/Toy Bluespec Verilog Compiler: Coded in F#.
// (C) 2012 DJ Greaves, University of Cambridge. All rights reserved.
// This file (lex_bsvc.fs) may be one day released under LGPL library license.
//
// No warranty of liability for infringement or direct or consequential loss is implied or accepted.
//
//
module lex_bsvc

open System.Collections.Generic



open yout
open moscow
open linepoint_hdr
open opath

open bsv_grammar

let keyword_tokens =
    [|  S_clocked_by; S_reset_by; S_seq; S_par; S_await; S_endseq; S_endpar; S_endcase; S_while;  S_void;  S_union;  S_type; S_typedef;  S_typeclass;  S_tagged; S_struct;  S_rules;  S_rule;  S_return;  S_provisos;  S_package;  S_module;  S_method;  S_matches; S_let; S_int; S_interface; S_instance;  S_import;  S_if;  S_for; S_function;  S_export;  S_enum;  S_endtypeclass;  S_endrules;  S_endrule;  S_endpackage;  S_endmodule;  S_endmethod;  S_endinterface;  S_endinstance;  S_endfunction;  S_endactionvalue;  S_endaction;  S_end;  S_else;  S_default; S_dependencies; S_deriving;  S_case;  S_bit;  S_begin;  S_actionvalue;  S_action;  S_noAction; S_schedule; S_parameter; S_numeric; S_valueOf; S_valueof;
        S_TAdd
        S_TSub
        S_TMul
        S_TDiv
        S_TLog
        S_TExp
        S_TMax
        S_TMin
        S_TAbs
        S_SizeOf
     |]

let preproc_tokens =    [| P_ifdef; P_endif; P_else; P_define; P_include; |]

let type_ide_convert active_pockets arg =
    match arg with
        | S_IDL ss ->
            let yesf = memberp ss !gm_ty_stack || disjunctionate (memberp ss) active_pockets
            
            if yesf then
                //py("type "  + !buf)
                let ans = S_TY(ss)
                ans
            else arg
        | arg -> arg


// The eternal question of whether macro flags persist into subsequent compilation units
// is confused by the issue of global defs on the command line.  The command line certainly introduces a global aspect.      





let macro_token_to_string site fname flag_to_check =
    let ss =
        match flag_to_check with
            | SP_stringLiteral ss -> "\"" + ss + "\""
            | S_NAT ss
            | S_SYSID ss
            | S_TY ss
            | S_IDL ss
            | S_IDC ss -> ss
            | other -> cleanexit(sprintf "bsv_preproc: %s Unexpected symbol used as argument to bsv preproc ifdef (or similar).  arg=%A" fname other)
    ss

// Check preprocessor flag: see if it is defined.
let check_macro_is_defined fname flag_to_check =
    let ss = macro_token_to_string "check_macro_is_defined" fname flag_to_check 
    let ans =
        match g_opath_preproc_macro_values.lookup ss with
            | Some _ -> true
            | _      -> false
    //dev_println (sprintf "check_macro_is_defined: flag_to_check=%A ans=%A" flag_to_check ans)
    ans

    
type bsv_lexer_core_t(vd: int, lextrace:bool, fname: string) = class
    let identifier_dict = new Dictionary<string, token option>()
    let preproc_token_dict = new Dictionary<string, token option>()    
    let linenum = ref 1
    let py x = if lextrace then vprintln 0 (i2s !linenum + ":" + x) 
    let c1 = ref '.'
    let last_linenum = ref 1
    let last_token = ref (S_IDL "<START OF FILE>")
    let install preprocf x =
        let ss = bsv_grammar.token_to_string x
        //dev_println (sprintf "preprocf=%A install token >%A<" preprocf ss)
        (if preprocf then preproc_token_dict else identifier_dict).Add(ss.[2..], Some x) // Remove the S_ prefix (or P_).
        ()
    let _ = for x in keyword_tokens do install false x done
    let _ = for x in preproc_tokens do install true x done        

    let _ = identifier_dict.Add("$BUILTIN", Some DS_BUILTIN)
    let inc() =
        linenum := !linenum + 1
        log_linepoint(LP(fname, !linenum))
        ()
    let eof = System.Convert.ToChar 255
    let cis =
        let t = try new System.IO.StreamReader(fname:string)
                with _ ->  cleanexit("cannot open input file " + fname)
        if t.Peek() = -1 then cleanexit("cannot open input file " + fname)
        c1 := System.Convert.ToChar(t.Read())
        t

    let intbasechar = function
        | 'h' | 'H' -> Some 16
        | 'd' | 'D' -> Some 10
        | 'o' | 'O' -> Some 8
        | 'b' | 'B' -> Some 2
        | _ -> None

    let m_finished = ref false

    let x_ch() = !c1

    let x_rdch0() =
        let c = cis.Peek()
        if c = -1 then eof
        else 
        let ch = System.Convert.ToChar(cis.Read())
        if (ch = '\n') then inc()
        ch


    let x_rdch =
        let rec rdch() =
            let c = !c1
            c1 := x_rdch0()
            let rec skip_block_comment nest =
                if nest <= 0 then ()
                else
                    let c = !c1
                    c1 := x_rdch0()
                    // TODO : will be confused by nested comment delimters inside strings.
                    if (c = '/' && !c1 = '*') then (c1 := x_rdch0(); skip_block_comment(nest+1))
                    elif (c = '*' && !c1 = '/') then (c1 := x_rdch0(); skip_block_comment(nest-1))
                    elif (c = eof) then sf "file ends within block_comment"
                    else skip_block_comment nest
                
            let rec skip_eol() =
                let c = !c1
                c1 := x_rdch0()
                if (c <> '\n' && c <> eof) then skip_eol() else ()
            if (c = '/' && !c1 = '/') then (skip_eol(); ignore("eol skipped " + System.Convert.ToString(!c1)); rdch())
            elif (c = '/' && !c1 = '*') then (skip_block_comment(1); rdch())
            else c
        rdch

    let x_lex_id preprocf sysf c0 =
        let cts (x:char) = System.Convert.ToString x
        let capital = (c0 >= 'A' && c0 <= 'Z')
        let gen = function
            | "$time"   -> DS_time
            | "$stime"  -> DS_stime
            | "$format" -> DS_format
            | ss ->
                if preprocf then P_macro ss
                elif sysf then S_SYSID ss elif capital then S_IDC ss else S_IDL ss
        let m_buf = ref (cts c0)
        let p c = (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9') || c = '_' || c = '$'
        let rec scan () =
            let c = x_ch()
            if p c then (m_buf := !m_buf + cts(x_rdch()); scan())

        scan()
        let (found, ov) = (if preprocf then preproc_token_dict else identifier_dict).TryGetValue(!m_buf)
        let ans =
            if found && ov <> None then valOf ov
            elif found then gen !m_buf
            else
                //py ("New id " + (!m_buf))
                identifier_dict.Add(!m_buf, None)
                gen !m_buf
        let ans = type_ide_convert [] ans
        last_token := ans
        py(sprintf " Token in %s  identifier buf=%s" (bsv_grammar.token_to_string ans) !m_buf)
        ans


    let x_intliteral baser c0__ =
        let dontcares = ref false
        let cts (x:char) = System.Convert.ToString x
        let buf = ref ""
        let p16 c = (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F') || (c >= '0' && c <= '9') 
        let p10 c = (c >= '0' && c <= '9') 
        let p8  c = (c >= '0' && c <= '7') 
        let p2  c = (c >= '0' && c <= '1') 
        let rec scan () =
            let c = x_ch()
            let xzq = c = '?'|| c = 'x' || c = 'X' || c = 'z' || c = 'Z' 
            let _ = if xzq then dontcares := true
            let p = (if baser=2 then p2 elif baser=8 then p8 elif baser=16 then p16 else p10)
            if c = '_' || p c || xzq then (buf := (!buf) + cts(x_rdch()); scan())
        let _ = scan()
        let ans = SX_INTLITERAL(!dontcares, baser, !buf)
        last_token := ans        
        py(sprintf " Token in Intliteral baser=%i buf=%s " baser !buf + bsv_grammar.token_to_string ans)
        ans




    let x_lex_num10 (baser) c0 = // used only for denary?
        let cts (x:char) = System.Convert.ToString x
        let buf = ref (cts c0)
        let p16 c = (c >= 'a' && c <= 'F') || (c >= 'A' && c <= 'F') || (c >= '0' && c <= '9') || c = '_' || c = '?'
        let p10 c = (c >= '0' && c <= '9') || c = '_' || c = '?'
        let p8  c = (c >= '0' && c <= '7') || c = '_' || c = '?'
        let p2  c = (c >= '0' && c <= '1') || c = '_' || c = '?'        
        let rec scan () =
            let c = x_ch()
            let p = (if baser=2 then p2 elif baser=8 then p8 elif baser=16 then p16 else p10)
            if p c then (buf := (!buf) + cts(x_rdch()); scan())
        let _ = scan()
        let ans = S_NAT(!buf)
        last_token := ans        
        py(sprintf " Token in num %s buf=%s" (bsv_grammar.token_to_string ans) !buf)
        ans

    member x.alldone() = !m_finished
        
    member x.lex_string c0 =
        let cts (x:char) = System.Convert.ToString x
        let buf = ref ""
        let rec scan () =
            let c = x_ch()
            if c = '\\' then
                let c1 = x_rdch()
                if c1=eof then (buf := (!buf) + cts(x_rdch()); scan())
                else (buf := (!buf) + cts c + cts(x_rdch()); scan())
            elif c<>eof && c <> c0 then (buf := (!buf) + cts(x_rdch()); scan())

        let _ = scan()
        let _ = x_rdch()
        let ans = SP_stringLiteral(!buf)
        last_token := ans
        py(!buf + " Token " + bsv_grammar.token_to_string ans)
        ans

        

    member x.lexer1 buf_ =
        last_linenum := !linenum
        let cts (x:char) = System.Convert.ToString x
        let c = x_rdch()
        if (c = ' ' || c = '\t' || c = '\r' || c = '\n') then (x.lexer1 buf_)
        elif (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c = '_' then x_lex_id false false c
        elif (c = '$') then x_lex_id false true c
        elif (c = '`') then // Backquote - preproc keyword
            let c = x_rdch()
            x_lex_id true false c

        elif (c >= '0' && c <= '9') then x_lex_num10 (10) c
        elif (c = '\"') then x.lex_string c
        else
        let ans =            
            if (c = ';') then (SS_SEMI)
            elif (c = ',') then (SS_COMMA)
            elif (c = '\'') then
                match intbasechar (x_ch()) with
                    | None ->
                        if (x_ch() >= '0' && x_ch() <= '9') then x_intliteral 10 (x_ch())
                        else SS_FWDQUOTE // FWDQUOTE is used in type assertions and is followed by whitespace, an lpar or lbrace in such contexts.


                    | Some baser -> (ignore(x_rdch()); x_intliteral baser (x_ch()))
            elif (c = '#') then (SS_HASH)
            elif (c = '?') then (SS_QUERY)
            elif (c = ')') then (SS_RPAR)
            elif (c = '{') then (SS_LBRACE)
            elif (c = '}') then (SS_RBRACE)
            elif (c = '[') then (SS_LBRA)
            elif (c = ']') then (SS_RBRA)
            elif (c = '/') then (SS_SLASH)
            elif (c = '%') then (SS_PERCENT)
            elif (c = '+') then (SS_PLUS)
            elif (c = '-') then (SS_MINUS)
            elif (c = '^') then (SS_CARET)
            elif (c = '.') then
                let c1 = x_ch()
                if c1 = '.' then (ignore(x_rdch()); SS_DOTDOT) else SS_DOT1
            elif (c = '=') then
                let c1 = x_ch()
                if c1 = '=' then (ignore(x_rdch()); SS_DEQD) else SS_EQUALS
            elif (c = '&') then
                if x_ch() <> '&' then SS_AMP
                else
                    let _ = x_rdch()
                    if x_ch() = '&' then (ignore(x_rdch()); SS_AMPAMPAMP) else SS_AMPAMP
            elif (c = '\\') then
                if x_ch() <> '=' then (ignore(x.error(sprintf "Illegal backslash escaped character '%c'" (x_ch()))); S_IDL "")
                else
                    let _ = x_rdch()
                    if x_ch() = '=' then (ignore(x_rdch()); SS_SLASHEQUALS2) else (ignore(x.error(sprintf "Illegal backslash escaped character '%c'" (x_ch()))); S_IDL "")
            elif (c = '|') then
                let c1 = x_ch()
                if c1 = '|' then (ignore(x_rdch()); SS_STILE2) else SS_STILE
            elif (c = '!') then
                let c1 = x_ch()
                if c1 = '=' then (ignore(x_rdch()); SS_DNED) else SS_PLING
            elif (c = '<') then
                let c1 = x_ch()
                if c1 = '<' then (ignore(x_rdch()); SS_LSHIFT)
                elif c1 = '-' then (ignore(x_rdch()); SS_LEFTARROW)
                elif c1 = '=' then (ignore(x_rdch()); SS_DLED) else SS_DLTD
            elif (c = '>') then
                let c1 = x_ch()
                if c1 = '>' then (ignore(x_rdch()); SS_RSHIFT)
                elif c1 = '=' then (ignore(x_rdch()); SS_DGED) else SS_DGTD            
            elif (c = ':') then
                let c1 = x_ch()
                if c1 = ':' then (ignore(x_rdch());SS_COLONCOLON) else SS_COLON
            elif (c = '*') then
                let c1 = x_ch()
                if c1 = ')' then (ignore(x_rdch()); SS_STARRPAR)
                elif c1 = '*' then (ignore(x_rdch()); SS_STARSTAR)                
                else SS_STAR
            elif (c = '(') then
                let c1 = x_ch()
                if c1 = '*' then (ignore(x_rdch()); SS_LPARSTAR) else SS_LPAR
            elif (c = eof) then (m_finished := true; ignore(py "End of file"); SSS_EOF)
            else
                ignore(x.error ("bad input char " + cts c))
                S_IDL ""
        py(" Token in " + bsv_grammar.token_to_string ans)
        last_token := ans
        ans

    member x.lineno () = !last_linenum
    member x.next_lineno () = !linenum
        
    member x.error msg =
        let wf = function
            | SP_stringLiteral s -> "\"" + s + "\""
            | S_NAT s -> s
            | S_IDC s
            | S_IDL s -> s
            | S_TY  s -> s + "/type"
            | x -> bsv_grammar.token_to_string x
        let m0 = "syntax error: " + msg + " " + fname + ":" + i2s (!last_linenum) + " last token and next two are: " + wf !last_token
        let _ = x.lexer1 ()
        let m1 = " " + wf !last_token
        let _ = x.lexer1 ()
        let m2 = " " + wf !last_token
        let m3 =
            if nullp !gm_ty_stack then ""
            else "\ntype identifier_dict in scope are: " + sfold (fun x->x) !gm_ty_stack

        cleanexit(m0 + m1 + m2 + m3)
        
end


type aux_parser_t =
    {  m_tdef_state:   int ref
       m_tdef_nest:    int ref
       m_type_ides:    string list ref option ref
       //m_pockets:      Dictionary<int, token list ref>
    }

type auxiliary_parsing_markup_t =
    | AP_none
    | AP_pop
    | AP_pocket of string list ref


//
let auxiliary_parsing vd fname l0 apt token =
    match token with
        | S_typedef -> // Regularish grammar for parsing typedef typevars.
            apt.m_tdef_state := 1
            if vd>=4 then vprintln 4 (sprintf "auxiliary_parsing pocket start %s %i on token %A" fname l0 token)
            let pocket = ref []  // Type identifiers will be put in this pocket.
            apt.m_type_ides := Some pocket
            //apt.m_pockets.Add(l0, pocket) // Pockets are indexed with starting line number.  Several on one line will cause dictionary abend.
            AP_pocket pocket

        | SS_LBRACE ->
            mutinc apt.m_tdef_nest 1
            AP_none

        | SS_RBRACE ->
            mutinc apt.m_tdef_nest -1
            AP_none
            
        | SS_SEMI when !apt.m_tdef_nest = 0 ->
            if !apt.m_tdef_state>0 then
                if vd>=4 then vprintln 4 (sprintf "auxiliary_parsing off on %A" token)
                apt.m_tdef_state := 0
                AP_pop
            else AP_none
                
        | S_type ->
            if !apt.m_tdef_state>0 then
                if vd>=4 then vprintln 4  (sprintf "auxiliary_parsing trigger on %A" token)
                apt.m_tdef_state := 2
            AP_none
                

        | S_IDL ss ->
            if !apt.m_tdef_state>1 then
                if vd>=4 then vprintln 4 (sprintf "auxiliary_parsing capture type identifier %s %i on %A" fname l0 token)
                apt.m_tdef_state := 1
                match !apt.m_type_ides with
                    | None -> sf "no pocket open"
                    | Some ptr -> mutaddonce ptr ss
            AP_none

        | S_numeric -> AP_none

        | _ ->
            if !apt.m_tdef_state>1 then
                if vd>=4 then vprintln 4 (sprintf "auxiliary_parsing missed trigger %s %i on %A" fname l0 token)
                apt.m_tdef_state := 1
            AP_none
            
//
// Wrapper class with nominally the same signature as the core lexer pre-lexes the whole file to support read-ahead in auxiliary parsing.
//
type bsv_lexer_t(vd:int, lextrace:bool, fname: string) = class
    let lcore = new bsv_lexer_core_t(vd, lextrace, fname)
    let lex_all_ahead = true

    let m_prelexed_lines = ref None
    let m_prelexed_tokens = ref []
    let m_lineno = ref -1
    let m_last_token = ref (S_IDL "<START OF FILE>")

    let apt =  // Auxiliary parsing of type identifiers in typdef etc forms.
        {
             m_tdef_state=  ref 0
             m_tdef_nest=   ref 0
             m_type_ides=   ref None
        }


    let prelex lexbuf = 
        let lexline () = // Read one line as a backwards list.
            let l0:int = lcore.next_lineno()
            let m_tokens = ref []
            let rec rex () =
                if lcore.next_lineno() <> l0 then ()
                else
                    let token = lcore.lexer1 lexbuf
                    mutadd m_tokens token
                    if token <> SSS_EOF then rex ()
            rex()

            // Here is something nasty where wer process the line backwards, since SLR(1) cannot parse KB_typeAttributes while keeping types and expressions separate, we insert an S_TYPEASSERT_WARNING marker before the start of the type in the >type'exp< form.
            // This means typeAttributes cannot be multi-line constructs and use of an FSM here means the construct will not nest, but I won't worry about that for now.
            let tokens =
                let rec reverse_parse (trig, paren_nest, a3) sofar = function
                    | [] -> sofar
                    | h::tt ->
                        // pri (sprintf "Reverse parser %i %i %i %A" trig paren_nest a3 h)
                        let (trig, paren_nest, a3) =
                            if h = SS_HASH then (trig, paren_nest, a3)
                            elif h = SS_FWDQUOTE then (1, 0, 0)
                            elif paren_nest=0 then (trig, paren_nest, a3+1)
                            else (trig, paren_nest, a3)
                        let paren_nest =
                            if h = SS_RPAR then paren_nest+1
                            elif h = SS_LPAR then paren_nest-1
                            else paren_nest
                        let sofar = h::sofar
                        let (sofar, trig, paren_nest, a3) =
                            if trig=1 && paren_nest=0 && a3=1 then
                                // pri (sprintf "Insert S_TYPEASSERT_WARNING")
                                (S_TYPEASSERT_WARNING::sofar, 0, 0, 0)
                            else (sofar, trig, paren_nest, a3)
                        reverse_parse (trig, paren_nest, a3) sofar tt

                reverse_parse (0, 0, 0) [] !m_tokens
            // pri (sprintf "Input line tokens post revers parse are %A" tokens)
            let aux_parse token =
                let ap = auxiliary_parsing vd fname l0 apt token
                (ap, token)
            map aux_parse tokens
        


        let rec prelux() = // Read all lines of the source file.
            if lcore.alldone() then
                vprintln 2 (sprintf "prelex of %s finished at line %i." fname (lcore.lineno()))
                []
            else
                let tokens = lexline()
                if vd>=6 then vprintln 6  (sprintf "prelex of %s line %i: %A" fname (lcore.lineno()) tokens)
                (lcore.lineno(), tokens) :: prelux()
        prelux()

    let m_active_pockets = ref []

    let dispatch_ap = function
        | AP_none          -> ()
        | AP_pop           ->
            if nullp !m_active_pockets then hpr_yikes "No active prelex pockets open when asked to pop."
            else m_active_pockets := tl !m_active_pockets
        | AP_pocket pocket ->
            if lextrace then vprintln 0 (sprintf "Activate type identifier prelex pocket containing %s" (sfold id !pocket))
            m_active_pockets := !pocket :: !m_active_pockets

            
    let lexer_zero lexbuf = // The main entry point.
        let ans =
            if lex_all_ahead then
                if nonep !m_prelexed_lines then
                    m_prelexed_lines := Some(prelex lexbuf)
                    m_prelexed_tokens := []

                let rec preload () =
                    if nullp !m_prelexed_tokens then
                        match !m_prelexed_lines with
                            | Some ((lineno, tokens)::tt) ->
                                m_lineno := lineno
                                if vd>=6 then vprintln 6 (sprintf "Dequeue line %i: %A" lineno tokens)
                                m_prelexed_tokens := tokens
                                m_prelexed_lines := Some tt
                                log_linepoint(LP(fname, lineno))
                                preload()
                            | _ ->
                                ()
                preload()
                match !m_prelexed_tokens with
                    | [] -> SSS_EOF
                    | (ap, hh)::tt ->
                        dispatch_ap ap
                        m_prelexed_tokens := tt
                        hh

            else lcore.lexer1 lexbuf
        ans

    let m_macro_expand_buffer = ref []
    let m_condition_stack:bool list ref = ref [] // The stack of nested ifdefs.

    let tokens_to_eol lexbuf = // This does not support conditional compilation in the body of multi-line defines.
        let start_line = linepoint_lineno()
        let rec git sofar = 
            let tok = lexer_zero lexbuf
            if start_line = linepoint_lineno() then git(tok :: sofar)
            else (tok, rev sofar)
        git []


    let rec lexer_one lexbuf = // The main entry point.
        let tok = lexer_zero lexbuf
        match tok with
            | P_macro body ->
                if nullp !m_macro_expand_buffer then
                    m_macro_expand_buffer := explode body
                    muddy ("bsv preproc: macro expansion of " + body) // TODO finish ...
                else sf(sprintf "bsv preproc: macro_buffer busy when new macro encountered " + body)
                lexer_one lexbuf

            | P_ifdef ->
                let flag_to_check = lexer_zero lexbuf
                let definedf = check_macro_is_defined fname flag_to_check
                //dev_println (sprintf "bsv preproc: check ifdef preproc_id=%A ans=%A" flag_to_check definedf)
                m_condition_stack := definedf :: !m_condition_stack
                lexer_one lexbuf

            | P_else ->
                match !m_condition_stack with
                    | [] -> cleanexit(sprintf "bsvc: preproc: `else outside of `ifdef in %i:%s" (lcore.lineno()) fname) // TODO fname may be part of an include!?
                    | enablef::tt ->
                        //dev_println (sprintf "bsv preproc: toggle top of ifdef stack")
                        m_condition_stack := (not enablef) :: tt
                        lexer_one lexbuf                        

            | P_endif ->
                match !m_condition_stack with
                    | [] -> cleanexit(sprintf "bsvc: preproc: `endif outside of `ifdef in %i:%s" (lcore.lineno()) fname) // TODO fname may be part of an include!?
                    | _::tt ->
                        //dev_println (sprintf "bsv preproc: pop ifdef stack")
                        m_condition_stack := tt
                        lexer_one lexbuf                        

            | P_include  ->
                let running = conjunctionate id !m_condition_stack // All entries must hold true.
                let file_name = lexer_zero lexbuf                
                let file_name = macro_token_to_string "P_include" fname file_name
                let (next_tok, _) = tokens_to_eol lexbuf
                if running then
                    muddy ("bsvc: preproc: include directive " + file_name)
                next_tok
                
            | P_define ->
                let running = conjunctionate id !m_condition_stack // All entries must hold true.
                let macro_name = lexer_zero lexbuf                
                let macro_name = macro_token_to_string "P_define" fname macro_name
                let (next_tok, arg) = tokens_to_eol lexbuf
                if running then
                    let arg = map (macro_token_to_string "macro body" fname) arg
                    opath_define_macro "P_define" macro_name (strings_flatten arg)
                next_tok

            | tok ->
                let running = conjunctionate id !m_condition_stack // All entries must hold true.
                //dev_println (sprintf "bsv preproc: length ifdef stack = %i.  Running=%A" (length !m_condition_stack) running)
                if running || tok=SSS_EOF then
                    let ans = type_ide_convert !m_active_pockets tok
                    if lextrace then vprintln 0 (sprintf "Lex trace %A" ans)
                    m_last_token := ans
                    ans
                else lexer_one lexbuf

    // exported members commence
    member x.lexer1 lexbuf = lexer_one lexbuf

    member x.error msg =
        if lex_all_ahead then
            let wf = function
                | SP_stringLiteral ss -> "\"" + ss + "\""
                | S_NAT ss -> ss
                | S_IDC ss
                | S_IDL ss -> ss
                | S_TY  ss -> ss + "/type"
                | x -> bsv_grammar.token_to_string x
            let m0 = "syntax error: " + msg + " " + fname + ":" + i2s !m_lineno + " last token and next two are: " + wf !m_last_token
            let _ = x.lexer1 ()
            let m1 = " " + wf !m_last_token
            let _ = x.lexer1 ()
            let m2 = " " + wf !m_last_token
            let m3 =
                if nullp !gm_ty_stack then ""
                else "\ntype identifiers in scope are: " + sfold (fun x->x) !gm_ty_stack

            cleanexit(m0 + m1 + m2 + m3)
        else lcore.error msg


end


// Entry point to read, lex and parse a file.
let lexbsv_parse ww lextrace printtree preloaded_constags fname =
    let vd = 3
    let pp = vprintln 0
    let ww = WF 2 "CBG BSV Parser" ww ("start parsing filename=" + fname)
    let mylex = new bsv_lexer_t(vd, lextrace, fname)    
    //Microsoft.FSharp.Text.Parsing.Flags.debug <- true
    //let knot = ref None:((int -> Microsoft.FSharp.Text.Parsing.Tables<_>)ref)
    //Again, this has changed name and the old name needs to be removed.
    //let lexbuf = Microsoft.FSharp.Text.Lexing.LexBuffer<char>.FromString "this lexbuf is not used"
    let lexbuf = FSharp.Text.Lexing.LexBuffer<char>.FromString "this lexbuf is not used"

    gm_constag_in_scope_list := preloaded_constags 
    let lexer filename =
        let ans = mylex.lexer1 filename
        ans

    // If you have done --define:__DEBUG during the compile of Parsing.fs then you can set the debug flag inside the SLR(1) engine.
    // Parser.Flags.debug <- true
    // OR let mutable debug = true
        
    let as_tree =
                  try bsv_grammar.packageStmt_list lexer lexbuf
                  with
                      |  _ -> sf("Caught " + mylex.error(""))


    let constags = !gm_constag_in_scope_list
    gm_constag_in_scope_list := []
    // pri (sprintf "Constructor tags defined in %s were %s" fname (sfold id constags))

    let ww = WF 2 "CBG BSV Parser" ww ("finished parsing filename=" + fname)
    if printtree then vprintln 0 (sprintf "Parse tree is %A" as_tree)
    //if nonep asot then sf "No parse tree generated"
    Some (as_tree, constags)


let verilog_literal_parse mf fw def_fw ((b:int), sex) = 

    let rec intlit baser sofar = function
        | '_'::t -> intlit baser sofar t           
        | '0'::t -> dig 0I baser sofar t
        | '1'::t -> dig 1I baser sofar t
        | '2'::t -> dig 2I baser sofar t
        | '3'::t -> dig 3I baser sofar t
        | '4'::t -> dig 4I baser sofar t
        | '5'::t -> dig 5I baser sofar t
        | '6'::t -> dig 6I baser sofar t
        | '7'::t -> dig 7I baser sofar t
        | '8'::t -> dig 8I baser sofar t
        | '9'::t -> dig 9I baser sofar t
        | 'a'::t | 'A'::t -> dig 10I baser sofar t            
        | 'b'::t | 'B'::t -> dig 11I baser sofar t            
        | 'c'::t | 'C'::t -> dig 12I baser sofar t            
        | 'd'::t | 'D'::t -> dig 13I baser sofar t            
        | 'e'::t | 'E'::t -> dig 14I baser sofar t            
        | 'f'::t | 'F'::t -> dig 15I baser sofar t            
        | other::t -> cleanexit(sprintf "bad digit int literal '%c':" other + mf())
        | [] -> sofar 
    and dig d baser sofar t =
        if d >= baser then cleanexit(sprintf "digit too large for base in int literal: " + mf())
        intlit baser ((sofar * baser) + d) t
    let ans = intlit (System.Numerics.BigInteger b) 0I sex
    let ifw = if fw="" then def_fw else System.Convert.ToInt32 fw
    //vprintln 0 (sprintf "%s: verilog literal parse, fw=%i, base=%i, ans=%A" s ifw b ans)
    (ifw, ans)
// eof
