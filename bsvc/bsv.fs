// CBG SQUANDERER : Toy Bluespec Verilog Compiler: Coded in F#.
// (C) 2012-15 DJ Greaves, University of Cambridge. All rights reserved.


(* All rights reserved, except as licensed by LGPL below.
 * DJ Greaves, University of Cambridge, Computer Laboratory.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *)


//
// This is the top-level file for the CBG Toy Bluespec Compiler.
// This wrapper makes the Bluespec-like compiler into a plugin for orangepath.
//
//

// Need to do auto plugin scanning
open verilog_gen
open cpp_render
open opath
open hprxml


[<EntryPoint>]
let main (argv : string array) =
    yout.g_version_string := tydefs_bsvc.cbg_toy_bsv_banner()
    loadPlugins() // This is supposed to autoload most of the below!
    ignore(conerefine.install_coneref())
    //let _ = bevelab,install_bevelab()
    toplevel_bsvc.bsvc_used ()
    install_verilog()
    restructure.install_restructure()
    diosim.install_diosim()
    install_c_output_modes()
    let rc = opath_main ("bsvc", argv)
    rc


(* eof *)


