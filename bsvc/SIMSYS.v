

module SIMSYS();

   reg CLK, RST_N;
   
  initial begin
     CLK = 0;
     forever #5 CLK = !CLK;
  end
   
  initial begin
     RST_N = 0;
     #30
     RST_N = 1;
  end
   
  a dut (CLK,		  RST_N);

endmodule