//http://wiki.bluespec.com/Home/Interfaces/Multiplier-Example

// Interface Definition

interface Mult_ifc;
  method Action start(int x, int y);
    method int result ();
  endinterface: Mult_ifc

// Module Providing Mult interface

(* synthesize *)
module mkMult1 (Mult_ifc);
   Reg#(int) product  <- mkReg(0);
   Reg#(int) d  <- mkReg(0);
   Reg#(int) r   <- mkReg(0);

  rule cycle (r != 0);
       if ((r&1) !=0) product <= product + d;
       d <= d << 1;
       r <= r >> 1;
  endrule

  // method result () if (r == 0);  // <--- The type no longer needs to be given, but parser rejects it?
  method int result () if (r == 0);
       return product;
  endmethod


  method Action start (x, y) if (r == 0);
     d <= x; r <= y; product <= 0;
  endmethod


endmodule: mkMult1

// eof