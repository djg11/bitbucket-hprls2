// $Id: vsys.v,v 1.4 2012-11-22 19:54:58 djg11 Exp $
//
// vsys.v A test wrapper for simulating very simple tests with clock and reset.
//

module SIMSYS();
   reg clk, reset;
   initial begin reset = 1; clk = 1; # 2220 reset = 0; end
   always #500 clk = !clk;
   dut dut(.CLK(clk), .RST_N(!reset));

   wire [31:0] count, vol;
   wire finished;  
   reg start; 
//   DUT dut(reset, clk, count, finished, start, vol);
   initial # 700000 $finish;

   initial begin
   $dumpfile ("vsys.vcd"); // Change filename as appropriate.
   $dumpvars(1, SIMSYS);
   $dumpvars(1, SIMSYS.dut);
      //$dumpvars(1, SIMSYS.dut.mkFir4_TestBench_dut);
      //$dumpvars(1, SIMSYS.dut.SimpleProcessorTb_mkTb_sp);            
      //$dumpvars(1, SIMSYS.dut.Test1i_mkTest1iBench_fbar);            
   end
endmodule
// eof
