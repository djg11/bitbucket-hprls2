// CBG SQUANDERER : CBG-BSV Toy Bluespec Verilog Compiler: Coded in F#.
// (C) 2012-15 DJ Greaves, University of Cambridge. All rights reserved.


(* All rights reserved, except as licensed by LGPL below.
 * DJ Greaves, University of Cambridge, Computer Laboratory.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *)

module tydefs_bsvc

let cbg_toy_bsv_banner() = "CBG-BSV TOY COMPILER VERSION 0.49 ALPHA 22nd-Sept-2019"

open System.Collections.Generic
open System.Numerics

open moscow
open meox
open yout
open opath_hdr
open hprls_hdr
open linepoint_hdr
open abstract_hdr
open abstracte
open parser_types


// Join hierarchic parts with a dot.
let htosp = htos1 "."

exception Invalid


// Various pragma modifiers for an interface or method etc.:
type bsv_pragma_t =
    | Pragma_no_implicit_conditions
    | Pragma_fire_when_enabled
    | Pragma_shed_control of string * string list list

    | Pragma_doc of string
    | Pragma_alwaysReady
    | Pragma_alwaysEnabled

    | Pragma_synthesize
    | Pragma_split   (*Non-conflicting branches of otherwise conflicting rules can be scheduled  concurrently.  off by default but can be set on cmd line too*)
    | Pragma_nosplit (*Needed to restore default behaviour in inner nested scope*)
    | Pragma_clocked_by of string
    | Pragma_reset_by of string    

    | Pragma_bluewire            // Part of a PulseWire, RWire and so on.

    // Toy compiler semantic extensions added by DJ Greaves.
    | Pragma_fire_rate of string // Added by DJ Greaves for real-time Bluespec project.
    | Pragma_non_idempotent      // 
    | Pragma_once_per_clock_cycle  // Per-site disable ephemeral history register behaviour.
    | Pragma_arbiter_type of string
    | Pragma_rule_repeat_count of int
    | Pragma_arbiter_shares of string
    | Pragma_synchronised        // Added by DJ Greaves for resilant Bluespec project. What?
    | Pragma_compilation_root    // Added by DJ Greaves to assist with incremental compilation.
    

    // RSTN=
    // CLK=
    // bit_blast
    // scan_insert
    // RDY=
    // EN=
    // noinline

type utag_t = string list

type spare_bindindex_t = string // This is not needed can can be deleted.

type bindindex_t =
    {
        brun_tag: string
        brun_idx: int
    }
type prags_t = ListStore<string list, bsv_pragma_t>

type specialmodel_t = // Certain leaf components are hardwired in the core compiler (rather than being in cambridgetech, other Bluespec modules or third-party IP blocks).
    | SM_notspecial
    | SM_bluewire
    | SM_ephemeral_register
    | SM_ephemeral_RAM // Not a forwarded RAM, but a RAM where multiple writes per cycle are allowed provided addresses are decidibly disjoint/coincident.
    

type resource_attributes_t =
    {
        actionf:                                 bool
        can_be_ignored_for_schedulling:          bool
        idempotentf:                             bool // When idempotent, two calls can be treated as one call if have the same ar1gs.  
        specialmodel:                            specialmodel_t
    }        

let resat_resolve ra rb =
    cassert(ra.specialmodel=rb.specialmodel, "L111 specialmodel disagree")
    {
        specialmodel=                     ra.specialmodel
        actionf=                          ra.actionf || rb.actionf
        can_be_ignored_for_schedulling=   ra.can_be_ignored_for_schedulling && rb.can_be_ignored_for_schedulling
        idempotentf=                      ra.idempotentf && rb.idempotentf 
    }


let ats_get (prags:prags_t) (iname: string list) = prags.lookup iname

let test_att (prags:prags_t) (iname: string list) prag = // These should be marshalled before main compile step please
    let a = ats_get prags iname
    let r = memberp prag a
    //vprintln 3 (sprintf "test_att %s %A r=%A" (htosp iname) prag r)
    r

let test_att_int (prags:prags_t) defaultv (iname: string list) prag = // These should be marshalled before main compile step please
    let alst = ats_get prags iname
    let rec scan = function
        | hh::tt ->
            match (hh, prag) with
                | (Pragma_rule_repeat_count ans, Pragma_rule_repeat_count _) -> ans
                | _ -> scan tt
        | [] -> defaultv
    scan alst

let test_att_string (prags:prags_t) defaultv (iname: string list) prag = // These should be marshalled before main compile step please
    let alst = ats_get prags iname
    let rec scan = function
        | hh::tt ->
            match (hh, prag) with
                | (Pragma_fire_rate ans, Pragma_fire_rate _)           -> ans
                | (Pragma_arbiter_shares ans, Pragma_arbiter_shares _) -> ans
                | (Pragma_arbiter_type ans, Pragma_arbiter_type _)     -> ans
                | _ -> scan tt
        | [] -> defaultv
    scan alst


    
let g_show_type_formalpram_namehints = ref false // Show formal names for types in debugging output - these are occasionally useful for debugging.

let g_hydrate_being_done = "hydrate_being_done"
    
let g_bsc_preferred_netnaming = true // We prefer net names such as "EN_count_enable" for Bluespec whereas hpr library normally would create "count_enable_EN"

let g_add_kind_to_formals = false



// 
// Open hash table - rather poor!
// Please say why we need our own form of this.
type MyDict<'k_t, 'v_t when  'k_t :> obj and 'v_t :> obj>() = class

   let len = 32768*32
   let (hashtab:('k_t * 'v_t) list array) = Array.create len []
   member x.Add (h:int) k v =
       let h1 = h % len
       Array.set hashtab h1 ((k,v) :: hashtab.[h1])

   member x.TryFind eq h k1 =
       let rec scan = function
            | [] -> None
            | (k, v)::tt ->
                if eq(k, k1) then Some v
                else scan tt
       scan hashtab.[h % len]
   end



[<NoComparison;NoEquality>]
type NoEqMap<'k_t, 'v_t when 'k_t : comparison and 'v_t :> obj >(themap:Map<'k_t, 'v_t>) = class

//  error FS0693: The type 'genenv_map_t' is not a type whose values can be enumerated with this syntax, i.e. is not compatible with either seq<_>, IEnumerable<_> or IEnumerable and does not have a GetEnumerator method
   interface IEnumerable<KeyValuePair<'k_t, 'v_t>>
       with
             member x.GetEnumerator() = (seq { for z in themap -> z }).GetEnumerator() :> IEnumerator<KeyValuePair<'k_t, 'v_t>>

         //  member x.GetEnumerator() = themap.GetEnumerator() :> System.Collections.IEnumerator
             member x.GetEnumerator() = (seq { for z in themap -> z }).GetEnumerator() :> System.Collections.IEnumerator            


   static member empty = NoEqMap<'k_t, 'v_t>(Map.empty)

   member x.Remover      k = new NoEqMap<'k_t, 'v_t>(themap.Remove k)
   member x.Add     (k, v) = new NoEqMap<'k_t, 'v_t>(themap.Add(k,v))
   member x.TryFind (k)    = themap.TryFind k

end


[<NoComparison;NoEquality>]
type NoEqOptionStore<'a, 'b when 'a : equality and 'a : comparison> = OptionStore<'a, 'b>



type fstack_t = string list

type raw_evc_ctrl_t = (string * bsv_ast_t * linepoint_t) list    

type interface_inode_name_t = IIS of string // All hard currency interfaces have a unique inode name that is referred to by potentially several interface aliases.


// A monica is the name of a thread that executes a rule or exported method.  All rulse and exported methods have unique monicas and they may be concurrent.
type monica_t = string list

let kToStr = function
    | (IIS inode, method_name) -> [ method_name; inode; "IIS" ]

type monica_pair_t = (bool *  monica_t) // External method flag and restructuring domain/thread for multi-cycle mode.

type fpath_t = monica_pair_t * int // keep track of synchronised accesses through the design. TODO please explain.

// We need to know precedence of identifiers when type resolving - a definition should take precedence over a declaration.
type dprec_t = P_decl0 | P_defn1 | P_tlab of string | P_free

type tagged_linepoint_t = linepoint_t * int // // A tagged linepoint has a unique natural number paired alongside a source file reference.

let taggedlpToStr0 (lp, tagno) = sprintf "T%i:" tagno + lpToStr0 lp

type bsv_rulerec_t =
    {
        name:        string
        rate_target: string option
        copycount:   int            // Number of times to replicate the rule in manually-applied src code markup.
    }

// Where two formals have been unified, we must select which name to use.
let leftprec token = function
    | (None, Some _) -> false
    | (None, None) 
    | (Some _, None) -> true
    | (Some l, Some r) ->
        vprintln 3 (sprintf "precchox active at " + token)
        match (l, r) with
            | (_, P_defn1) -> false
            | _            -> true


type rulename_issue_t = string * string list * int  // Name of current rule, instance name and superscalar issue no(not used now?)  when inside a rule body.

type dbi_t = int

type bsv_exp_t =
    | B_aggr of bsv_type_t * tunion_t
    | B_ferref of fer_t
    | B_bit_replace of bsv_type_t * bsv_exp_t * bsv_exp_t * int * int * linepoint_t // These form a linked list sorted in ascending baser on the first field:  width, baser int ordering approved. Good to unify with the data structure for a field_select.
    | B_field_select of string * bsv_exp_t * int * int  //baser,width order deprecated
    | B_mask of int * bsv_exp_t
    | B_subs of bool * bsv_exp_t * bsv_exp_t * linepoint_t
    | B_shift of bsv_exp_t * int (* positive offset is left shift *)

    | B_hexp of (bsv_type_t * hexp_t)

    | B_blift of bsv_bexp_t
    | B_reveng_h of bsv_type_t * (bsv_bexp_t * bsv_exp_t) list
    | B_reveng_l of bsv_type_t * (hbexp_t * bsv_exp_t) list
    | B_query of bool(*strict flag*) * bsv_bexp_t(*guard*) * bsv_exp_t * bsv_exp_t * bsv_type_t * (linepoint_t * callpath_t)
    | B_var of dbi_t * string list * linepoint_t
    | B_string of string    


    | B_fsmStmt of bsv_exprFsmStmt_t * string list
    | B_vector of string * (string list * bsv_type_t) // Reference string into muts table(for norm) and unflattend ifc name for compile
    | B_subif  of string * bsv_exp_t * bsv_type_t * linepoint_t
    | B_thunk of path_item_t * (bsv_type_t * bsv_exp_t) list


//merge these three?
    | B_moduleInst   of string list * (director_info_t * string list * callpath_t) * string list * bsv_exp_t * (linepoint_t * textual_uid_t)
    | B_maker_abs    of string list * bsv_pragma_t list * bsv_abs_moduleParams_t * bsv_proviso_t list * (bsv_type_t * bsv_ast_t list) * ee_t * string list * linepoint_t 
    | B_maker_l      of string list * bsv_pragma_t list * bsv_moduleParams_t * bsv_stmt_t list * bsv_proviso_t list * string list * linepoint_t


//merge us: too many function forms!
    | B_abs_lambda of string list * string * string list * bsv_ast_t list * ee_t * linepoint_t
    | B_lambda of string option * formals_t * bsv_ast_t list (*bsv_stmt_t list*) * bsv_type_t * ee_t * (linepoint_t * callpath_t)


//merge us: too many apply forms!
    | B_applylam of callpath_t (*abs callpath*) * path_item_t * (bsv_type_t * bsv_exp_t) list * linepoint_t * string option ref * (bsv_type_t * bsv_exp_t) option ref
    | B_applyf   of path_item_t * bsv_type_t * (bsv_type_t * bsv_exp_t) list * tagged_linepoint_t



    | B_format of bsv_exp_t list * (bsv_type_t * bsv_exp_t) list
    | B_rule of string // TODO these should be used and first class ... they are mostly?


    | B_ifc of path_item_t * bsv_type_t        // A named/nominal interface
    | B_ifc_immed of bsv_stmt_t list   // A structural interface
    | B_ephemeral_ifc of nom_typ_ats_t * tystruct_form_t * bsv_sumtypepram_t list   // Named (nominal) type with template arguments.  Currently if an interface is ephemeral are all of its members? It is a matter of instances: some intances of a given interface may be ephemeral in which case all the methods of that instance are...  TODO CAMRET tidy
    
    | B_diadic of x_diop_t * bsv_exp_t * bsv_exp_t * bsv_type_t
    | B_pliStmt of string * (bsv_type_t * bsv_exp_t) list * linepoint_t      // for $display and so on

//merge us:
    | B_dyn_b of string * bsv_exp_t // An action
    | B_action of rulename_issue_t * bsv_stmt_t list * retval_t * linepoint_t

    | B_valof of bsv_stmt_t list * bsv_type_t option * linepoint_t

    | B_knot of string list * (bsv_type_t * bsv_exp_t) option ref

// Boolean expressions
and bsv_bexp_t =
    | B_firing of string // Backdoor access to the composite guard for any rule.
    | B_not  of bsv_bexp_t   
    | B_and  of bsv_bexp_t list  // B_and[] means true.
    | B_or   of bsv_bexp_t list  // B_or[] means false        
    | B_bexp of hbexp_t // Pre-compiled forms
    | B_bdiop of x_bdiop_t * bsv_exp_t list // Equality, less than and orreduce where list is a singleton.
    | B_orred of bsv_exp_t
// Integer expressions
and tunion_t =
    | TU_struct of (string * (bsv_type_t * int * int * bsv_exp_t)) list (*ty, width, pos, value*)
    | TU_enum of string * int * int option
    | TU_tunion of bsv_exp_t * string * int * int * int


// Map of constructor tags to types that define them and arity of that tag (zero for enum constant).  Unlike ML, but like C++, tags can be reused in different enums and structs. 
and ctor_tags_t = Map<string, (bsv_type_t * int) list>

and tyclass_marker_t = (string list * bsv_ast_typeformal_t list) list option

and fer_t = bsv_type_t * mutable_reference_key_t * linepoint_t

and envaug_t =
    | Aug_tags of (string * (bsv_type_t * int)) list
    | Aug_tycl of string * string list * bsv_type_t list * (string * bsv_type_t) list * linepoint_t
    | Aug_env of tyclass_marker_t * string list (*bound name*)* (bool(*dmto*) * binding_info_t * lee_t)
    | Aug_mut of string list(*bound name*) * string list(*flat/abs name*) * fer_t // deprecate wrt Aug_env of B_ferref

and binding_info_t =
    {
      whom:        string   // Who made the binding
      synonymf:    bool     // A simple synonym (has already been freshened and hydrated, so no further needed within current scope).
      hof_route:   string option   // Whether bound via an indirection (a function may not be higher-order itself, but this indicates that it is used/passed in a higher-order manner and the intra-function route.
    }

and mutable_reference_key_t = string
      
and genenv_t =
    | GE_normthunk of bool * string list * genenv_t option ref * envaug_t list ref * bsv_type_t option ref * (WW_t -> ee_t -> envaug_t list)
    | GE_binding of string list(*bound_name*) * binding_info_t * lee_t // First arg is a list for vector subscript purposes
    | GE_formal of typeso_t * string * bsv_type_t // unbound formal for values.
    | GE_mut of string list * bsv_type_t * string(*site or debug string*) * mutable_reference_key_t // deprecate w.r.t a binding of B_ferref.
    | GE_tyclass of string list * bsv_type_t list 
    | GE_overloadable of bsv_type_t * ((string list * bsv_ast_typeformal_t list) list * ((bsv_type_t * freshlist_t) * bsv_exp_t)) list


and freshlist_t = (bool * string) list

and lee_t = 
    | EE_e      of bsv_type_t * bsv_exp_t                  // Env entry: pair with type and value
    | EE_typef  of grounded_indicator_t * bsv_type_t       // Env entry: just a type (and grounded indicator)

and grounded_indicator_t =
    | G_none                            // Grounded (no unbound/free type parameters).
    | G_some of bsv_sumtypepram_t list * bsv_sumtypepram_t list  // Ungrounded, explict and latent type parameters. Needed for synonyms such as mkRegFile.RegFileSize.

and genenv_map_t = NoEqMap<string list, genenv_t>             
and typeclasses_t = NoEqMap<string, (string list * bsv_type_t list)>

and pathstatus_t =
    | PS_prefix of string
    | PS_suffix

and evty_fun_t = WW_t -> bsv_type_t -> bsv_type_t    

and evty_fun_indirect_t = string // Need something with equality property as dictionary key, so indirect here.

and intra_function_hof_token_t = string

and callpath_t =
    | CP_none of string
    | CPS of pathstatus_t * (string * int * (intra_function_hof_token_t list * evty_fun_indirect_t) option) list // The leading bool denotes grounded.  The options on the elements indicate recursive loops and then higher-order function sites (hof sites) where the pair is a list of hof tokens (formals used as origins and local pseuodnonymous functions also as origins) and the environment for rehydration.

and director_info_t =
    {
        dclk:         string
        dreset:       string
        dcen:         string
        clkpos:       bool
        rstpos:       bool
        assynchreset: bool
        monica_o:     monica_pair_t option  // Name of an associated thread (for handshaking to hpr IP blocks).
    }



and attribute_settings_t =
    {
      dirinfo:          director_info_t        // Clock domain and so on.
      split_default:    bool                   // Split IF statements inside a rule into sepearate rules unless pragma'd otherwise.      
      fire_rate:        string option          // Target real-time rate, or
      superscalar_rate: int                    // Manual annotation of repeat count
    }

and ee_t = // Evaluation environment for elaboration stage.
    {
      //rulename_issue:     rulename_issue_t option// Name of current rule, instance name and issue no  when inside a rule body. Now in firex.
      attribute_settings: attribute_settings_t
      hof_float:          bool           // Whether to supress the insertion of the static code point (for formal function types).
      grounded:           bool
      callpath:           callpath_t     // The 'callstring' or call site stack to the current elaboration point (like a runtime stack but recursive calls never appear). Needed to name the result of typingdecisions which may vary at a callsite according the route to that callsite.
      static_path:        string list      // The textually surrounding context at definition
      module_path:        string list      // The textually surrounding module (same as static_path for a module).
      prefix:             string list           // The hierarchic prefix name during elaboration. Same as static_path during definition.
      forcef:             bool option           // Staging control - needs a little work - all functions were supposed to be elaborated before ewalk is invoked but we interleave that currently, controlled by this flag.
      elab_steps:         int ref
      muts:               muts_t              // Mutable variable elaboration values
      imported:           string list         // Already imported items.
      genenv:             genenv_map_t        // Further environment for the build stage.
      ctor_tags:          ctor_tags_t
      skip_synthesise_directives: bool
      typeclasses:        typeclasses_t
      vectors:            OptionStore<string, vector_t>         // No GC but maintains bsv_exp as an equality type.      
      dynamic_chain:      (string list * callpath_t) list // The elaboration stack dynamic to static mapping - stops recursion cycles in the dynamic chain being rendered in the callstring.
      fbody_context:      bool                // Holds when in a function body.
    }

and utypes_t = OptionStore<utag_t, bsv_type_t> 

// Method prototype protocol has one of these three forms:
and bsv_protoprotocol_t =
    | BsvProtoValue of bsv_type_t
    | BsvProtoActionValue of bsv_type_t
    | BsvProtoAction
    | NoProto
   

and uflags_t =
    { signed:   netst_t option
      branding: string
      width:    int
    }      // Leaf/primitive run-time type : signed/unsigned/none and optional width.

and bsv_proviso_t =
    | Prov_sty of bsv_type_t
    | Prov_asReg_asIfc of string
    | Prov_None of string
    | Prov_s of bsv_proviso_t list
    | Prov_si of string
    | Prov_root
    | Prov_func
    | Prov_method
    | Prov_ifc    


and genbind_t = string * bsv_type_t

and genbinds_t = genbind_t list

and schedule_cstr_t = string * (string * string) list

and ifc_rec_t =  // An interface type.
    {
        // bool               // Holds when non-reentrant, such as the register interface. OR holds when a blackbox around BVI leaf or separate compilation unit  NO MIRROR explain/delete
        iparts:      bsv_type_t list               // The methods of the interface (denoted as functions)
        imeta:       schedule_cstr_t list          // Schedulling directives
    }
and mrec_t =
    | MR_unset
    | MR_pending
    | MR_recursive
    | MR_linear
    
and tystruct_form_t =
    | F_vector
    | F_ntype
    | F_tyfun of string
    | F_ifc_1 of ifc_rec_t           // An interface
    | F_ifc_temp                     // placeholder till fully defined by prelude
    | F_subif of string * bsv_type_t // ... its not clear if a subinterface really needs to be a separate form.
    | F_action
    | F_actionValue of bsv_type_t    // The type should be in the args not here?
    | F_enum of int * (string * int) list
    | F_monad
    | F_module
    | F_struct of (string * bsv_type_t) list * bsv_struct_details_t option
    | F_tunion of mrec_t ref * (string * bsv_type_t option) list * bsv_tunion_details_t option // Bool is recursive flag.

    | F_fun of bsv_type_t * sum_fun_ats_t * bsv_type_t list // TODO why does this not have a fresh Tid list ? etc..
    
and bsv_struct_details_t = Bsv_struct of (int * int * bool) * (string * (bsv_type_t * int * int)) list (*ty, width, pos*)

and bsv_tunion_details_t = Bsv_tunion of (string * (bsv_type_t option * int)) list * (int * int * int * bool) * ((string * bsv_type_t option) * int) list    

and bsv_sumtypepram_t =
    | Tid of valueprop_t * string // generic type numeric flag and identifier
    | Tty of bsv_type_t * string  // needed for concrete types in say ActionValue#(concrete, abstract) ... but can alter F_actionValue to take the args instead if all concrete - done now?... numeric flag is implicit

and hofspog_t =
    // HOF_VAL_PATH: A note of higher-order function types being passed through the value graph as a function actual arg.
    SPOG of(string *  string list) * string list * (genenv_map_t * tb_t * hindley_t) * string * string 


// Hindley-milner unification takes a pair of gammates and returns a third that is their unification.  The type is the unified type and the list contains identifiers known to have that type (or is it alias identifiers for that type? or both?).  The identifiers may also have values used in type expressions such as TExp.
and gammate_t = (bsv_type_t option * (string list * bindindex_t) list * valueprop_t) // Type and identifiers bound to that type where the bindindex_t is a scope token. Type name is probably never a slit. Formal name is needed when tyvars must become numeric, as in (index_t low_index, index_t high_index) where having just the type 'index_t' is useless - we need to apply tyfuns to the actual args which will just be "named" formals where the names are the values.


and hindley_t = gammate_t list

and tb_t = // Typechecking bindings.
    { //h0:            hindley_t                // The hindley: contains types and type vars and is the work space for Hindley-Milner unification.
      tve:             tve_t                    // Type annotations
      rv:              bsv_type_t option        // Type of any return value
      recursion_stack: string list
      tb_static_path:  string list              // Callstring.
    } 

// 
and dropping_t =
    | Tanno_rhs of (string * bsv_type_t) list
    | Tannot of ((string) * bsv_type_t) list
    | Tanno_do of bsv_type_t * (string * bsv_type_t) list * (string * bsv_type_t) list
    | Tanno_function of bsv_type_t * tve_t * bsv_proviso_t list * (string * bsv_type_t) list
    | Tanno_hof of bsv_type_t * tve_t * (textual_uid_t * string) list  * (string * bsv_type_t) list    

// A tve is a type annotation. 
and tve_t = (string * callpath_t * dropping_t) list  // Fields are:   a unique identifier, the static call string, the annotated type(s).

and sum_fun_ats_t = // Function attributes.
    {
      //hof:      bool;  // Holds when this is a formal parameter type that is a higher-order function.
      body_gamma: tve_t   // Type annotations for declarations in the function body. When a call is made we should have bindings for the free variables in here and we use these to build the body_gamma for our parent.  The top-level call will have no free type parameters and can make concrete body droppings.

      fids:       string list      // Formal parameter names
      protocol:   bsv_protoprotocol_t
      methodname: string           // Or function name.
      fprovisos:  bsv_proviso_t list
      who:        string           // For debugging only.
    }


and valueprop_t =
    | VP_none of string
    | VP_type 
    | VP_value of bool * string list // Bool indiciates a numeric type is need. String list has at most one infact - more than one is a latent error - indicating the name of a an identifier in late bind scope for numeric value lookup.

and bsv_ntype_t =
    | Bsv_ntype of string list * bsv_sumtypepram_t list // builtin types in raw form
    | Bsv_xntype of bsv_type_t
    
and bsv_type_t =
    | Bsv_ty_list of string * bsv_type_t list
    | Bsv_ty_format
    | Bsv_ty_fsm_stmt of string
    
// TODO delete one of these two forms.
    | Bsv_ty_knot       of string list * bsv_type_t option ref
    | Bsv_ty_sum_knott  of string list * genenv_t ref    

     //Integer-like forms:
    | Bsv_ty_intconst of int // for use when a type is an integer constant as in "typedef Int#(32) int;" This should have cls="Literal" explicit or implicit.
    | Bsv_ty_int_vp   of valueprop_t
    | Bsv_ty_intexpi of bsv_exp_t // hmmm - possibly this should not exist and we should be firmer on requiring value parameter - use intconst intead after generalising to big int.
    | Bsv_ty_integer    
    | Bsv_ty_primbits of uflags_t
    
    | Bsv_ty_dontcare of string
    | Bsv_ty_av_partial


    // Method prototype: has name, provisio/pragmas, return type and method formals (pairs of formal and actuals so-far bound).
    | Bsv_ty_methodProto of string * bsv_ast_t list * bsv_proviso_t list * bsv_protoprotocol_t * bsv_otype_t

    | Bsv_ty_id of valueprop_t * dprec_t option * (string * spare_bindindex_t) // Variables only - valueprop encodes numeric type details


    // We remove one star in the subscripting operation since it is a dereference.
    // A star is a reference. The type of an array base has one extra star (ie typically plus one star) compared with the number of its content type (ie typically zero). The type of a subscripted expression has one fewer stars than its left operand (its right operand is a natural number).  An array base has one star a over its content type ... The built-in Vector and List types add one star to their content type so that the subscription operations remove one.
    | Bsv_ty_stars of int * bsv_type_t

    | Bsv_ty_nom of nom_typ_ats_t * tystruct_form_t * bsv_sumtypepram_t list   // Named (nominal) type with template arguments.

    // Next form deprecated : the branded form only to be used now, in methodProto or whatever
    //| Bsv_ty_fun of bsv_type_t * string option * bsv_type_t * (typeso_t * bsv_type_t * string) list * bsv_proviso_t list * linepoint_t // string option is a name for error msgs only... also TODO: for overloading want a digest here

and nom_typ_ats_t =
    {
      nomid:  string list
      cls:    bsv_proviso_t
    }

and bsv_otype_t =
    // Next form is branded for use only in methodProto.
    | Bsv_oty_fun of bsv_type_t * string option * bsv_type_t * (typeso_t * bsv_type_t * string) list * bsv_proviso_t list * linepoint_t // string option is a name for error msgs only... also TODO: for overloading want a digest here


    
and digest_t = string option * string list

and bsv_sigma_protocol_t =
    | BsvValue  of hexp_t option // Ready net - some values are not always ready.
    | BsvAction of hexp_t option * (monica_pair_t * hbexp_t) list * hexp_t  //  (en-net if needed) * (fire) *  (rdy-net)


and retval_t =
    | RV_netlevel  of bsv_type_t * hexp_t         // Function or method with a net-level result bus.
    | RV_highlevel of bsv_type_t                  // Function or method with ephemeral result.
    | RV_noret                                    // Function or method with no return value.

and method_ats_t =
     {
         always_ready:   bool
         synchronised:   bool
         always_enabled: bool
     }

and method_static_currency_t = 
           bsv_sigma_protocol_t * // Not static currently - fires get orred in
           retval_t *
           method_ats_t




and integrity_t =
     { encol:    (monica_pair_t * hbexp_t * hexp_t) list // We now defer making the mux tree from this list of args
     }
     
and actuals_t  = (string * (bsv_type_t * bsv_type_t)) list
and gactuals_t = (string * (bsv_type_t * lee_t)) list

and formals_t = (typeso_t * bsv_type_t * string) list

// bsv_stmt_t should perhaps be split into those found inside and outside of rules/methods.
// After all elaboration, these are the core constructs for actual compilation to logic.
// After all elaboration, vars outside of rules/methods will be constants with type interface. All local vars in rules/methods are just let binds for expression sharing.
and bsv_stmt_t =  // Some of these are declarations, not sortable/executable statements.
    | Bsv_varDecl        of bool(*structvestige*) * bsv_type_t * string list * int option * (linepoint_t * textual_uid_t)

    | Bsv_varAssign      of bool * string list * bsv_exp_t * string * linepoint_t // Used mainly/entirely? for interface assignments between elaborated modules.

    | Bsv_rule           of bsv_rulerec_t * bsv_bexp_t * bsv_stmt_t list * string list * linepoint_t

    | Bsv_eascActionStmt of bsv_exp_t * linepoint_t option

    | Bsv_beginEndStmt   of bsv_stmt_t list * linepoint_t
    | Bsv_ifThenStmt     of bsv_bexp_t * bsv_stmt_t * bsv_stmt_t option * bool * linepoint_t   // If/then/else control flow.
    //Bsv_whileStmt      of bsv_bexp_t * bsv_stmt_t * linepoint_t
    | Bsv_skip           of linepoint_t
    
    | Bsv_resultis       of (bsv_type_t * bsv_exp_t) * linepoint_t option // Only in actionvalue blocks and valof blocks. (can use eascActionStmt instead?)
    | Bsv_methodDef      of string * string list * bsv_protoprotocol_t * bool * bsv_type_t * formals_t * bsv_bexp_t * bsv_stmt_t list * linepoint_t
    | Bsv_primBuffer     of bsv_exp_t * bsv_exp_t (* Only for internal implementation *) * linepoint_t



// There are three lots of 'parameters' to the typical module as stored in this triple.
// They are the parametric type args of the exported interface, the parameters to the generate
// function and the list of interfaces, which consists of those used plus the single interface exported.

and bsv_abs_moduleParams_t = bool(*rootmarked*) * (typeso_t * bsv_type_t * string) list * (typeso_t * bsv_type_t * string) list * bsv_ast_t * (string * bsv_type_t) list

and bsv_moduleParams_t = (typeso_t * bsv_type_t * string) list * (typeso_t * bsv_type_t * string) list * bsv_type_t * bool(*hardstate*)

and bsv_n_moduleParams_t = (bsv_type_t * string) list * (bsv_type_t * string) list * (bsv_type_t list -> bsv_type_t)


// Vector is a Bluespec primitive aggregate type (unlike List which is (mostly?) defined in Bluespec itself).
// A Vector is indexed with a natural number.  These do not convert to hardware? They are just used for elaboration.
and vector_t =
    {
      iname:   string list 
      ct:      bsv_type_t           // Content type
      vt:      bsv_type_t           // The vector type (Vector of content type)
      len:     int                  // Length - currently static always?
      // Now why is the env used for scalars but vector assigns seem to be in the vector itself? TODO explain mutability.
      data:    NoEqMap<int, bsv_exp_t> 
    }
   

and muts_t = NoEqOptionStore<string, bsv_type_t * bsv_exp_t> // Mutable scalar variables and vectors are stored this way, but not let binds.

and func_data_t =
    {
        name: path_item_t;
    }


and path_item_t =
    | PI_subif of string
//    | PI_lambda of bsv_exp_t * string option // variant on PI_b : todo unify
    | PI_dk of string    
// .| PI_var of bsv_type_t * string list
    | PI_biltin of string    
    | PI_ifc_hier of string list

    | PI_iis of interface_inode_name_t
    | PI_ifc_flat of string list    // delete this please

    | PI_bno of bsv_exp_t * string option // with optional name
    
// Finite-state machine sub language.
and bsv_exprFsmStmt_t =
 | Bsv_seqFsmStmt    of bsv_exprFsmStmt_t * bsv_exprFsmStmt_t * linepoint_t
 | Bsv_parFsmStmt    of bsv_exprFsmStmt_t * bsv_exprFsmStmt_t * linepoint_t
 | Bsv_ifFsmStmt     of bsv_bexp_t * bsv_exprFsmStmt_t * bsv_exprFsmStmt_t option  * linepoint_t
 | Bsv_whileFsmStmt  of bsv_bexp_t * bsv_exprFsmStmt_t * linepoint_t
 | Bsv_repeatFsmStmt of bsv_exprFsmStmt_t  * linepoint_t
 | Bsv_eascFsmStmt   of bsv_exp_t * string * linepoint_t
 | Bsv_breakFsmStmt  of linepoint_t
 | Bsv_continueFsmStmt of linepoint_t

and rulectl_t = // The main rule abstraction
    {
        rulename:               string list
        fireguard:              hexp_t
        resource_records:       resource_records_t // This includes the explicit condition, its own intrinsic guards and the intrinsic guards of all args and children.
        shedats:                bsv_sched_flag_t list
        srcfileref:             linepoint_t
        rulerec:                bsv_rulerec_t
    }

and bsv_sched_flag_t =
    | SF_fire_when_enabled
    | SF_no_implicit_conditions
    
and rc_t =
    | RC_methbuf of string list * string * hexp_t * hexp_t * resource_records_t // Fields are id in both forms, callguard, rdy, final guard, result.
    | RC_rule of rulectl_t



and method_name_t = string           


// Current wiring for a method on an interface - a currency entry plus meta info.
and bsv_sigma_t =
    | Sigma of // concrete/hard currency.
           (string list (* kind *) * string list(*resource name primary alias*) * method_name_t) *
           resource_attributes_t *
           method_static_currency_t *
           (string * hexp_t * integrity_t) list // For hardstate, the net-level arguments: formal name, net name and current binding.

    | Sigma_e of eth_t // Ephemeral method elaborated on demand (no hard currency).
           
and ifcmap_t = Map<string list, interface_inode_name_t>                   // 1/2 entry in qmaps_ta

and key1_t = interface_inode_name_t * method_name_t // Primary key for a method on an interface.

and methmap_t = Map<key1_t, bsv_sigma_t>  // 2/2 entry in qmaps_t           
           
and eth_t = string * bsv_type_t * bsv_protoprotocol_t * formals_t * bsv_bexp_t * bsv_stmt_t list * linepoint_t // Ephemeral method.

and bsv_currency_t = // Transient/legacy form used just for hardcoded module mks ?
    | Tfa of genbinds_t * bsv_sigma_t list   // Concrete method with nets.  genbinds not used post type checking? Perhaps for numeric type values.

// Interfaces and methods need to be of the same type since an interface contents can be a free mix of sub-interfaces or methods.
and trace_t =
    | TRACE_filler // CAMRET all of this can be deleted now.
    //| TRACE_conc0 of bsv_sigma_t list    // Just a place holder and not to be recoded or looked at since sendargs rewrites TRACE_currency: TODO dont have twice MIRROR
    //| TRACE_ifc of (string list * trace_t) list                // Normal interface, but we do not always include the contents unless anonymous because we tend to look them up in the ifc_map. We could be a bit stricter?
    //| TRACE_redirect of string * bsv_exp_t                     // Maps interface name or alias to another interface or sub-interface. DELETE ME NOW WE HAVE SPLIT ifcmap and methmap.

    

and lowerterm_t = // CAMRET instead of using this, lift the trace forms into bsv_Exp
    | LT_curr of trace_t
    | LT_pram of bsv_exp_t (* anon ? *)

and commissioner_t = // aka firex_t
    {
       fire:           hbexp_t  // An expression that holds when a rule is being fired.
       fpath:          fpath_t  // Firepath
       rulename_msg:   string
    }

and orderer_t =
    | RO_read_before_write
    | RO_write_before_read
    | RO_dontcare
    
and execution_order_constraint_t =
    {
      ekey:      string list
      fname:     string
      unsafef:   bool
      orderer:   orderer_t
      writef:    bool
      kind:      string list
    }


and resource_records_t =  //
    {
        igs:        iguards_t                                  // Intrinsic guards
        fpaths:     Map<key1_t, fpath_t list>                  //
    }
and iguards_t = Map<monica_pair_t, iguard_t>

and iguard_t =
    | IG_resource of key1_t * string * hbexp_t * resource_attributes_t // resource/method name * resource/method name again * guard * conflict_attributes
    | IG_alpha of hbexp_t        // This is the FIRE signal from the scheduler.
    | IG_mux of hbexp_t * iguards_t * iguards_t
// A blift of an orred is only an identity operation if the argument was one-bit wide.


type exaux_t = bsv_type_t * string list * (string * hexp_t list) list option option * ee_t

// We have a multi-pass compiler
type pass_t =
    {
        pass_no:      int
        pass_name:    string
        m_statements: (int * bsv_stmt_t list * execution_order_constraint_t list * exaux_t) list ref
    }

type conflict_record_t = // (activation * ext method/rulename, resname, fname, actuals, idempotentf/ignorable etc)
     hbexp_t * monica_pair_t * string list * string * (bsv_type_t * bsv_exp_t) list * resource_attributes_t

type emission_t =
    { nets:            hexp_t list ref
      rulectl:         rc_t list ref
      comb_logic:      xrtl_t list ref
      synch_logic:     (director_info_t * xrtl_t list) list ref
      children:        emission_t list ref
      conflict_log:    conflict_record_t list ref
      uid:             string
    }



let rec igItemToStr = function
    | IG_alpha gg                 -> sprintf "alpha(%s)" (xbToStr gg)
    //| IG_mux(gg, tt, ff)        -> sprintf "mux..."
    | IG_mux(gg, tt, ff)          -> sprintf "cond(%s, %s, %s)" (xbToStr gg) (igMapToStr tt) (igMapToStr ff)
    | IG_resource(idl, ids, x, resats) ->
        if resats.can_be_ignored_for_schedulling then "(" + ids + ")" else ids

and igMapToStr arg =
    //dev_println (sprintf "tcheck %s %A %A" (hptos monica) k_ (igItemToStr v))
    let igiToStr _k vale cc =
        let ss = igItemToStr vale
        if cc="" then ss else ss + "," + cc
    Map.foldBack igiToStr arg ""

let rec ig_alpha_find_in_map_serf = function
    | IG_alpha gg                 -> sprintf "alpha(%s)" (xbToStr gg)
    | IG_mux(gg, tt, ff)          -> ig_alpha_find_in_map tt + ig_alpha_find_in_map ff
    | IG_resource(idl, ids, x, resats) -> ""

and ig_alpha_find_in_map arg =
    let igiToStr _k vale cc =
        let ss = ig_alpha_find_in_map_serf vale
        if cc="" then ss elif ss="" then cc else ss + "," + cc
    Map.foldBack igiToStr arg ""


let gec_blank_emission tag =
    // Need fresh ref cells on each call.
    { nets=ref []; rulectl=ref[]; comb_logic=ref[]; synch_logic=ref[];  children=ref[]; uid=funique tag; conflict_log = ref [] }

//    | Bsv_ty_abs_stun _
//    | Bsv_ty_abs_ifc _
//    | Bsv_ty_abs_syno _ -> false


    // Schedulling annotations
let g_schedulling_annotation_key =     
  [ ("C",   "conflicts")
    ("CF",  "conflict-free")
    ("SB",  "sequence before")
    ("SBR", "sequence before restricted (cannot be in the same rule)")
    ("SA",  "sequence after")
    ("SAR", "sequence after restricted (cannot be in the same rule)")
  ]

let g_allowable_schedulling_tokens = map fst g_schedulling_annotation_key

// TODO fill me automatically: and unify with predef_t.
let g_toy_bsv_builtins = ref  [ "Vector"; ]

let m_next_stmt_blk_index = ref 1

let next_stmt_blk_index msg =
    let rr = !m_next_stmt_blk_index
    //vprintln 4 (sprintf "next_stmt_blk_index %i %s" rr msg)
    mutinc m_next_stmt_blk_index 1
    rr

    
let g_blank_nom_typ_ats = {  cls=Prov_root; nomid=[] }

let g_action_ty = Bsv_ty_nom({g_blank_nom_typ_ats with nomid=["Action"] }, F_action, [])

let g_unit_ty = g_action_ty

let g_unit_ty_raw = KB_id("$unit_type", (g_builtin_lp, g_null_uid))

let g_canned_Clock_name = [ "Clock"; "Toy-Builtin" ]
let g_canned_Clock_type = Bsv_ty_nom({ nomid=g_canned_Clock_name; cls= Prov_None "canned" }, F_ifc_temp, [])

let g_canned_Reset_name = [ "Reset"; "Toy-Builtin" ]

let g_canned_Reset_type = Bsv_ty_nom({ nomid=g_canned_Reset_name; cls= Prov_None "canned" }, F_ifc_temp, [])

let g_canned_cbusitem_type_name = "ToyBSV_OPAQUE_CambridgeCBusItem" // Configuration bus for memory-mapped I/O
let g_canned_cbusitem_type =
    let a1 = Tid(VP_value(true, []), "address_width")
    let a2 = Tid(VP_value(true, []), "data_width")
    let ifc = { iparts= []; imeta=[] }
    Bsv_ty_nom({ nomid=[g_canned_cbusitem_type_name]; cls= Prov_None "canned" }, F_ifc_1 ifc, [a1;a2])

let g_canned_CRAddr_type_name = "ToyBSV_OPAQUE_CambridgeCRAddr"  // Configuration bus for memory-mapped I/O
let g_canned_CRAddr_type =
    let a1 = Tid(VP_value(true, []), "address_width")
    let a2 = Tid(VP_value(true, []), "data_width")
    let ifc = { iparts= []; imeta=[] }
    Bsv_ty_nom({ nomid=[g_canned_CRAddr_type_name]; cls= Prov_None "canned" }, F_ifc_1 ifc, [a1;a2])

let gfpToStr (card, id) = sprintf "!%i/%s" card id

let nf_numberp = function // Please delete other similar function and use this one.
    | VP_value(true, _) -> true
    | _ -> false

let olddef_ge = function
    | x -> "<old definition>"
    //| x -> (sprintf ": old definition was %A" x)

let gen_B_bexp = function
    | X_true  -> B_and []
    | X_false -> B_or []
    | x -> B_bexp x


let gen_B_blift = function
    | B_orred x when false -> x // TODO when width=1
    | x -> B_blift x

// An orred of a blift never has any effect.
let gen_B_orred = function
    | B_blift x -> x
    | x -> B_orred x
    

let gen_B_lambda(nameo, formals1, body, fun_ty, closure_ee, lp) =
         B_lambda(nameo, formals1, body, fun_ty, closure_ee, lp)


// Create a new vector ...    
let nvector ee (bv:vector_t) =
    let vs = funique "vector"
    ee.vectors.add vs bv
    B_vector(vs, (bv.iname, bv.vt))

let gen_B_not = function
    | B_not x -> x
    | B_or[] -> B_and[]
    | B_and[] -> B_or[]
    | other -> B_not other
    

let gen_B_and lst =
    let rec scan_taut = function
        | [] -> false
        | B_or[] :: tt -> true
        | _ :: tt -> scan_taut tt

    let rec scand = function
        | (B_and x)::tt -> x @ scand tt
        | other::tt     -> other :: scand tt
        | [] -> []
    let a = scand lst
    if scan_taut a then B_or[]    
    else match a with
            |[item] -> item
            | other -> B_and other

// Detect manifestly false or true constant booleans.
let myfalse bexp = (xbmonkey bexp = Some false)
let mytrue bexp = (xbmonkey bexp = Some true)    

let gen_B_or lst =
    let rec scan_inv = function
        | [] -> false
        | B_and[] :: tt -> true
        | _ :: tt -> scan_inv tt
    let rec scor = function
        | (B_or x)::tt  -> x @ scor tt
        | other::tt     -> other :: scor tt
        | [] -> []
    let a = scor lst
    if scan_inv  a then B_and[]
    else match (a) with
            | [item]->item
            | other -> B_or other


let g_pliTasks =
    [
        // These first ones are implemented in subsequent hpr recipe stages.
        ( "$finish",    ( "hpr_sysexit" , ""))        
        ( "$stop",      ( "hpr_sysexit" , ""))        
        ( "$display",   ( "hpr_writeln" , ""))
        ( "$displayb",  ( "hpr_writeln" , ""))
        ( "$displayo",  ( "hpr_writeln" , ""))
        ( "$displayh",  ( "hpr_writeln" , ""))
        ( "$write",     ( "hpr_write" , ""))
        ( "$writeb",    ( "hpr_write" , ""))
        ( "$writeo",    ( "hpr_write" , ""))
        ( "$writeh",    ( "hpr_write" , ""))

        // Locally handled pli tasks (inside bsv recipe stage).
        ( "$format",    ( "$format" , ""))        

   ]

type memo_t =
    {
      rootflag:    bool ref
      refcount:    int ref
      cost:        int64 option ref
      width:       int option ref
      name:        string option ref
      net:         hexp_t option ref
      parents:     int list ref
      orig:        hexp_t
    }


type lowered_expressions1_t = MyDict<bsv_exp_t, memo_t>
type lowered_expressions2_t = Dictionary<hexp_t, memo_t>
type e_roots_t = OptionStore<hexp_t, string>


type bsvc_free_vars_t = // aka bobj_t. Global settings and caches for one BSV compilation (many module definitions and instantiations).
    {   stagename:                     string
        utype_gen:                     int ref
        current_synth_target:          string list
// Verbal diorosity. 
        misc_vd:                       int         // Generic loglevel - should avoid using...
        wverbosef:                     bool        // Verbose over walk details aka vdp

        normalise_loglevel:            int // generate/elaborate.

        afreshen_loglevel:             int  // typechecking loglevels
        sum1_loglevel:                 int 
        dropping_loglevel:             int


        build_loglevel:                int  // Build/lower verbosity.
        sched_loglevel:                int  // Scheduling verbosity     

        write_tables:                  string option
        print_alltrees:                bool // Print all parse trees.
        write_normed_logs:             bool
        make_a_listing:                bool
        
        // Cmdline/recipe settings
        handshake_done_by_restructure:   bool      // Experimental advanced multi-cycle extensions
        scheduler_name:                  string
        add_simple_arbiters:             bool      // When set, allow little arbiters to be deployed to avoid starvation. A more sensible implementation and hard real time to follow.
        respect_rule_repeat_counts:      bool
        report_rule_fire_rates:          bool
        round_robin_mode:                bool
        multiple_scalar_write_per_cycle: bool      // When set, allow super-scalar writes per reg per clock cycle with effects composed.
        multiple_array_write_per_cycle:  bool      // When set, allow super-scalar writes per BRAM etc per clock cycle with effects composed.
        idempotent_actions:              bool      // Allow leaf actions not marked as not_idempotent to be shared over sites within a rule and concurrent rules.
        reentrant_methods:               bool      // When set, the new default, hard currency is only made for externally-callable methods and local methods called by rules inside this compilation are elaborated as many times as they are invoked.
        unif_iterations:                 int
        utypes:                          utypes_t
        prags:                         prags_t   // Pragmas from the command line
        incdir_path:                   string       
        norecurse_flag:                bool
        whilefor_limit:                int
        elab_limit:                    int        
        subevalcost:                   int64
        keep_fires:                    bool
        aggressive_conditions:         bool        
        forwarding_paths_support:      bool
        bram_subscription_support:     int // no of ports allowed
        textual_reverse:               bool      // Invert precedence of rules arising from textual ordering in source file.  Don't know which way round should be the default however!
        // Misc
        logic_cost_walker:             anal_queries_t    
                // Mutables
        m_compilation_stoplist:        (string list) list ref  // module names to be left to other compilations unless specifically named
        lowered_expressions1:          lowered_expressions1_t
        lowered_expressions2:          lowered_expressions2_t        
        e_roots:                       e_roots_t
        callgraph_recursions:          ListStore<string, string list * int>
        m_synths:                      (string list * (linepoint_t * bool)) list ref // List of (*synthesize*) annotated modules associated with skip_synthesise_directives flag.
        //m_targets:                   (string list * linepoint_t) list ref// 
        m_tables:                      (string * string list) list ref    // Tables for printing out
        m_bboxes_encountered:          (string list(*kind*) * bsv_type_t * string list(*iname*) * director_info_t * (string list * lowerterm_t) list) list ref     // Instantiations of separate synthesis targets or leaveouts met during current compilation.
        m_modgens:                     Dictionary<string list, bsv_exp_t>
        m_shed_control_list:           (bsv_pragma_t * string list) list ref
    }

type kernel_mk_t = WW_t -> bsvc_free_vars_t -> emission_t option -> ee_t -> director_info_t * string list * gactuals_t * genbinds_t * (string list * bsv_currency_t) list ->  bsv_stmt_t list


type widthinfo_t =
    | Wnone
    | Wsome of int
    | Wknot of string

(*    Some leaf helper routines now follow after thees types *)




// Attribute and pragma parsing.  The modifiers in (* ... *) form quotation.
let ins_ats bobj lp site (prags:prags_t) (iname:string list) items =
    let prefix = iname
    let pragma_parse cc arg =
        let des keyk = function
            | KB_stringLiteral s -> s
            | other -> cleanexit(lpToStr lp + sprintf ": Pragma '%s' expected a string argument" keyk)
        let des_int keyk = function
            | KB_stringLiteral ss when strlen ss > 0 && (isDigit ss.[0] || ss.[0] = '-') -> atoi32 ss
            | KB_intLiteral(field_width_o_, (dontcares_, baser, digits), lp) when baser=10 -> atoi32 digits
            | other -> cleanexit(lpToStr lp + sprintf ": Pragma '%s' expected an integer argument" keyk)

        let deso keyk = function
            | None -> prefix
            | Some(KB_stringLiteral s) -> s::prefix
            | other -> cleanexit(lpToStr lp + sprintf ": Pragma '%s' expected a string argument" keyk)
        let desof keyk = function
            | Some(KB_stringLiteral ss) -> ss
            | other -> cleanexit(lpToStr lp + sprintf ": Pragma '%s' expected a string argument not %A" keyk other) 

        let split_prag_comma k sl =
            let t (s:string) = s.Trim()
            let lst = map t (split_string_at [','] sl)
            let chknone (s:string) = if strlen s = 0 then cleanexit("zero length element in argument to " + k)
            app chknone lst
            map (fun x->[x]) lst
        match arg with
        | KB_attr_spec("doc", None) -> cc
        | KB_attr_spec("doc" as keyk, Some ss)    -> (Pragma_doc(des keyk ss), prefix) :: cc

        | KB_attr_spec("bluewire" as ss, so)       -> (Pragma_bluewire, deso ss so) :: cc
        | KB_attr_spec("always_ready" as ss, so)   -> (Pragma_alwaysReady, deso ss so) :: cc
        | KB_attr_spec("always_enabled" as ss, so) -> (Pragma_alwaysEnabled, deso ss so) :: cc

        | KB_attr_spec("non_idempotent" as s, so)       -> (Pragma_non_idempotent, deso s so) :: cc
        | KB_attr_spec("once_per_clock_cycle" as s, so) -> (Pragma_once_per_clock_cycle, deso s so) :: cc

        | KB_attr_spec("synchronized" as s, so) 
        | KB_attr_spec("synchronised" as s, so)   -> (Pragma_synchronised, deso s so) :: cc

        | KB_attr_spec("compilation_root" as ss, so) ->
            let idl = deso ss so
            vprintln 3 (sprintf "Noted %s as a compilation stoplist item." (hptos idl))
            mutadd bobj.m_compilation_stoplist idl
            (Pragma_compilation_root, idl) :: cc

        | KB_attr_spec("synthesise" as ss, so) 
        | KB_attr_spec("synthesize" as ss, so) ->
            let idl = deso ss so
            vprintln 3 (sprintf "Noted %s as a synthesize root-marked item." (hptos idl))
            (Pragma_synthesize , idl) :: cc

    //Say why these are commented out here?  They are present in Test10.bsv afterall.
    //They are now processed instead by refine_dir_info without passing through Pragma_ form.
    //Please tidy up.
    //|KB_attr_spec("clocked_by" as keyk, sl)
    //|KB_attr_spec("reset_by" as keyk, sl) -> 
    //|KB_attr_spec("clock_prefix", Some x) ->
    //|KB_attr_spec("reset_prefix", Some x) ->
    
        | KB_attr_spec("no_implicit_conditions" as s, so) -> (Pragma_no_implicit_conditions, deso s so) :: cc
        | KB_attr_spec("fire_when_enabled" as s, so) -> (Pragma_fire_when_enabled, deso s so) :: cc    
        | KB_attr_spec("split" as keyk, so)   -> (Pragma_split, deso keyk so) :: cc
        | KB_attr_spec("nosplit" as keyk, so) -> (Pragma_nosplit, deso keyk so) :: cc 

        | KB_attr_spec("fire_rate" as keyk, so) -> (Pragma_fire_rate(desof keyk so), prefix) :: cc  // Need to parse when and relative clauses here etc...

        | KB_attr_spec("arbiter_shares" as keyk, so) ->    (Pragma_arbiter_shares(desof keyk so), prefix) :: cc
        | KB_attr_spec("arbiter_type" as keyk, so) ->      (Pragma_arbiter_type(desof keyk so), prefix) :: cc
        | KB_attr_spec("rule_repeat_count" as keyk, Some ss) -> (Pragma_rule_repeat_count(des_int keyk ss), prefix) :: cc 


        //Example: (* descending_urgency = "r1, r2, r3" *)
        | KB_attr_spec("preempts" as keyk, sl)
        | KB_attr_spec("descending_urgency" as keyk, sl)
        | KB_attr_spec("conflict_free" as keyk, sl)
        | KB_attr_spec("execution_order" as keyk, sl)
        | KB_attr_spec("mutually_exclusive" as keyk,  sl) ->
            let addprefix ss =
                // Where a rule is named in the attribute to a rule, there will be a repetition of rule names in the context.
                // Likely the same goes for a method ... need to check.
                let prefix = if (site = "rule" || site = "method") && not_nullp prefix then tl prefix else prefix
                ss @ prefix
            let shed_control = 
                if nonep sl then cleanexit(sprintf "pragma '%s' needs an argument after an equals sign" keyk)
                else (Pragma_shed_control(keyk, map addprefix (split_prag_comma keyk (des keyk (valOf sl)))), prefix)
            //vprintln 3 (sprintf "site=%s   : store shed_control=%A" site shed_control)
            //   if pass.pass_no = 0 then
            mutaddonce bobj.m_shed_control_list shed_control
            // We have stored this in the global list so do not need to pass on.
            if true then cc else shed_control::cc




    //(* doc = "This sub-module does something else" *)
    //(* clock_ancestors = "clk1 AOF clk2" *)
    //(* clock_ancestors = "clk1 AOF clk2 AOF clk3, clk1 AOF clk4, clka AOF clkb" *)
    //(* clock_family = "clk1, clk2, clk3" *)

    //(* preempts = "r1, r2" *) If r1 will fire, r2 will not.

        //|KB_attr_spec("CLK", Some clk) -> Pragma_clk(clk) :: c    

        // Defaults can be safely ignored:

        | other ->
            hpr_warn(htosp iname + sprintf "+++ site=%s  prefix=%s pragma ignored: %A" site (hptos prefix) other)
            cc
            
    let rr = List.fold pragma_parse [] items 
    let mut_store_attribute (att, key) =
        if not(nullp key) then
            vprintln 3 (sprintf "Pragma/attribute store %s  %A" (htosp key) att)
            prags.add key att
            ()
    app mut_store_attribute rr
    rr // end of ins_ats

//
// Create a begin/end block but only when needed.
// 
let rec bsv_mkBlock lp = function
    | [item] -> item
    | Bsv_beginEndStmt(x, lp_) :: lst -> bsv_mkBlock lp (x @ lst)
    | lst -> Bsv_beginEndStmt(lst, lp)
       

    
let is_stringty = function // Yuck - brand somehow instead... TODO
    | Bsv_ty_nom(_, F_vector, [_; Tty(Bsv_ty_primbits f, _)]) when f.signed=None && f.width=8
    //&& is_bitty f.oldname
        -> true
    | _ -> false

// Convert back to type name from a type.
// Mainly used for overload naming.    
let rec digest_cooked mf = function
    | Bsv_ty_nom(ats, F_actionValue ty, _) -> digest_cooked mf ty   // Ignore ActionValue modifier for digest purposes.
    | Bsv_ty_nom(ats, _, args) -> [underscore_fold ats.nomid; sprintf "^%i" (length args)]
    | Bsv_ty_intconst _ -> [ "Int"     ]
    | Bsv_ty_integer    -> [ "Integer" ]
    | Bsv_ty_intexpi(B_hexp(ty, _)) -> digest_cooked mf ty
    //| Bsv_ty_intexpi _ -> 
    | Bsv_ty_primbits f    -> [ (if f.signed=Some Signed then "^bit#(" else "^ubit#(") + i2s f.width + ")" ]


    | Bsv_ty_dontcare site ->
        hpr_warn(mf() + ":Using dontcare in a type digest: site=" + site)
        []
    | other ->
        if is_stringty other then ["String"] // Yuck! - please put in a digest for vectors = TODO much nicer.
        else cleanexit(mf() + ": type name not clean: " + bsv_tnToStr other)

and digest_cooked_pram mf = function
    | Tty (ty, _)  -> digest_cooked mf ty
    | Tid (vp, ss) -> [ss]

and bsv_protoprotoToStr = function
    | BsvProtoValue tn       -> "$(" + bsv_tnToStr tn + "$)"
    | BsvProtoActionValue tn -> "ActionValue(" + bsv_tnToStr tn + ")"
    | BsvProtoAction         -> "Action"

and gaToStr fa =
    let q (s, t) = s + "<-->" + bsv_tnToStr t 
    match fa with 
    | (s, t) -> ".A1.<" + q(s,t) + ">"

and bsv_tnToStr tn = bsv_tnToStr1 [] tn

and vfToStr verbosef = function // hindley value printing: may be none, a type or a set of values with/without numeric flag.
    | VP_none s              -> (if verbosef then "N:" + s + ":" else "")
    | VP_type                -> "T::"
    | VP_value(b, sl)        -> (if verbosef || not(nullp sl) || b then (if b then "NV[|" else "V$[|") + sfold (fun x->x) sl + "|]" else "")

and gmm vp (tn, bindindex_) = // hindley printing: use double backslash with type first and value second.
    match vp with
        | VP_none _ -> htosp tn
        | _         -> htosp tn + "\\\\" + vfToStr true vp


and bsv_short_tnToStr = function // Brief printing of type names.
    | Bsv_ty_nom(ats, F_actionValue ty, args) when ats.nomid = ["ActionValue"] -> sprintf "ActionValue(%s)" (bsv_short_tnToStr ty)
    | Bsv_ty_nom(nst, _, _) -> htos nst.nomid 
    | other -> bsv_tnToStr1 [] other
    
and bsv_tnToStr1 stack tn =    
    let g3ToStr = function
        | (None, ty, id)   -> id
        | (Some m, ty, id) -> m + ":" + id        
    let genprt = function
        | [] -> ""
        | f_generics -> "#(" + sfold g3ToStr f_generics + ")"
    match tn with

    | Bsv_ty_list(id, lst) -> sprintf "Bsv_ty_list^%i(%s, " (length lst) id + sfold (bsv_tnToStr1 stack) lst + ")"

//    | Bsv_ty_sum_knot(idl, kr) when nonep !kr -> "knot(untied," + htosp idl + ")"
//    | Bsv_ty_sum_knot(idl, kr)                -> "knot(tied," + htosp idl   + ")"

    | Bsv_ty_stars(m, ct) ->
//        let rec qq m = if m < 1 then "&&&" elif m = 1 then "" else "," + qq (m-1)
//        bsv_tnToStr ct + "[" + qq m + "]"
        sprintf "Stars(%i, %s)" m (bsv_tnToStr ct)

    | Bsv_ty_id(vp, prec, (s, _)) -> s + "\\" + vfToStr false vp

    | Bsv_ty_nom(ats, F_monad, []) -> "$MONAD" // Never has any args etc and is always called $M or essentially anonymous.
    | Bsv_ty_nom(ats, F_vector, args) when ats.nomid = ["Vector"] -> "Vector#(" + sfold tidToStr args + ")"
    | Bsv_ty_nom(ats, F_action, []) when ats.nomid = ["Action"] -> "Action"
    | Bsv_ty_nom(ats, F_actionValue ty, args) when ats.nomid = ["ActionValue"] -> sprintf "ActionValue(%s)" (bsv_tnToStr ty) + (if nullp args then "" else "'#(" + sfold tidToStr args + ")")
    | Bsv_ty_nom(ats, F_ntype, args) ->  htosp ats.nomid +  "'#(" + sfold tidToStr args + ")"
    | Bsv_ty_nom(ats, cat, [])   -> catToStr cat + "-named-" + htosp ats.nomid
    | Bsv_ty_nom(ats, cat, args) -> catToStr cat + "-named-" + htosp ats.nomid + "#(" + sfold tidToStr args + ")"

//TODO for an F_fun kill the tyargs and use an abstract designation...
    | Bsv_ty_nom(_, F_fun(rt, matts, []), [])     -> matts.methodname + ":" + " unit -> " + bsv_tnToStr rt
    | Bsv_ty_nom(_, F_fun(rt, matts, [item]), []) -> matts.methodname + ":" + bsv_tnToStr item + " -> " + bsv_tnToStr rt    
    | Bsv_ty_nom(_, F_fun(rt, matts, items), [])  -> matts.methodname + ":" + "(" + sfold bsv_tnToStr items + ") -> " + bsv_tnToStr rt

// More verbose listing needed from some debugging - want to see the args.
    | Bsv_ty_nom(_, F_fun(rt, matts, []), args)     -> matts.methodname + "#(" + sfold tidToStr args + ")" + ":" + " unit -> " + bsv_tnToStr rt
    | Bsv_ty_nom(_, F_fun(rt, matts, [item]), args) -> matts.methodname + "#(" + sfold tidToStr args + ")" + ":" + bsv_tnToStr item + " -> " + bsv_tnToStr rt    
    | Bsv_ty_nom(_, F_fun(rt, matts, items), args)  -> matts.methodname + "#(" + sfold tidToStr args + ")" + ":" + "(" + sfold bsv_tnToStr items + ") -> " + bsv_tnToStr rt



    | Bsv_ty_methodProto(id, _, _, protoproto, Bsv_oty_fun(sty_, _, rt, args, provisos, lp)) ->
        let f2s (so, ty, name) = bsv_tnToStr ty + " " + name
        let rv = bsv_protoprotoToStr(protoproto)
        "method " + rv + " " + id + "(" + sfold f2s args + ")"

    | Bsv_ty_knot(idl, k2) when nonep !k2   -> "Untied(" + htosp idl + ")"
    | Bsv_ty_knot(idl, k2) when nullp stack -> "Knotted(" + htosp idl + ", " + bsv_tnToStr1 (idl::stack) (valOf !k2) + ")"
    | Bsv_ty_knot(idl, k2) -> "Knotted(" + htosp idl + ", ...)"
    | Bsv_ty_intconst n -> i2s n + "T"
    | Bsv_ty_primbits(f)   ->
        let t = (if f.signed=Some Signed then "^bit#"  else "^ubit#")
        if true then t + i2s f.width // Short form. Branding not currently used.
        else t + "(" + i2s f.width + "," + f.branding + ")"
    | Bsv_ty_av_partial -> "ActionValue#(<>)"
    | Bsv_ty_integer -> "Integer"
    | Bsv_ty_dontcare site -> "<dontcare>" + site    

    //| Bsv_ty_fun(sty_, nameo, rt, formals, provisos, lp) ->
    //    let f2s (so, ty, name) = (soToStr false so) + bsv_tnToStr ty + " " + name
    //    sprintf "Bsv_ty_fun(%s, rt=%s, formals=[%s], ...)" (if nameo=None then "anon" else valOf nameo) (bsv_tnToStr rt) (sfold f2s formals)

    | Bsv_ty_fsm_stmt s  -> sprintf "Bsv_ty_fsm_stmt(%s)" s
    | Bsv_ty_format -> "Bsv_ty_format"
    | Bsv_ty_int_vp vp -> "AnnoInteger\\" + vfToStr false vp
    | Bsv_ty_intexpi x -> "Bsv_ty_intexpi(" + bsv_expToStr x + ")"
    | s -> (sprintf "??tn%A" s)


and is_intlike_type m = function      //Integer-like forms:
    | Bsv_ty_intconst _
    | Bsv_ty_intexpi _
    | Bsv_ty_integer    
    | Bsv_ty_primbits _ -> true
    | _ -> false


and catToStr = function // type structures - why are functions and methods handled differently?
    | F_ifc_1 ii ->
        let rec simp firstf = function
            | [] -> ""
            | [item] -> bsv_tnToStr item // Print full details of first item, remaining methods in summary form.            
            | Bsv_ty_nom(_, F_fun(_, mats, args), _)::tt when not firstf -> sprintf "M:%s^%i" mats.methodname (length args) + "," + simp false tt
            | other::tt -> bsv_tnToStr other + "," + simp false tt
        if true then sprintf "ifc:{^%i ...}" (length ii.iparts)// concise form.
        else "ifc:{" + simp true ii.iparts  + "}"        
    | F_subif (iname, contents) -> "subifc:" + iname + "{" + bsv_tnToStr contents  + "}"    
    | F_enum(max_in_use, items) -> "$enum" + i2s(length items)
    | F_monad      -> "$MONAD"
    | F_module               -> "$module"    
    | F_action               -> "Action"
    | F_actionValue t        -> "ActionValue#(" + bsv_tnToStr t + ")"
    | F_tunion(br, forms, _) -> "tunion:{"  + sfold fst forms + "}"
    | F_struct(forms, _)     -> "struct:{"  + sfold fst forms + "}"    
    | F_fun(rt, mats, args)  -> sprintf "fun:%s^%i{" mats.methodname (length args) (*if mats.hof then "$HOF" else ""*) + sfold bsv_tnToStr args + "->" + bsv_tnToStr rt + "}"
    | F_ntype                -> "$NTYPE"
    | F_vector               -> "$VEC"
    | F_tyfun fname          -> fname  // Type function, such as SizeOf to TAdd.
    | other -> sprintf "cat?%A" other


//
// Note: a numeric type is a subset of the full set of types that avoids all the structures like unions and interfaces and functions. The nf flag is just a polymorphism constraint.
//
// A more interesting distinction is between value parameters and type parameters.  A value parameter will take on the value of an expression (which itself will have a type) whereas 
// a type parameter will take on a type. These are only found inside types, such as the inner type of a Maybe or a Vector (its second parameter).  The first parameter to a Vector is a value parameter that happens to be numeric.  Ditto the typep "Bit" which is really a bit vector.
//
and tidToStr = function
    | Tid(valuepram, s) -> "@" + s    
    | Tty(sty, s) when !g_show_type_formalpram_namehints -> bsv_tnToStr sty + "/" + s
    | Tty(sty, s) -> bsv_tnToStr sty

and pathToS x =
    let rec pos = function
        | PI_ifc_hier ifc  ->  "IFCH-" + htosp ifc
        | PI_iis(IIS str)  ->  sprintf "B_iss(%s)" str
        | PI_ifc_flat ifc  ->  "IFCF-" + htosp ifc
        | PI_biltin s -> "BI-" + s
        | PI_dk s -> s
        | PI_bno(s, Some name) -> "Bno-" + bsv_expToStr s + "-\"" + name + "\""
        //| PI_lambda(s, None) -> "lam-" + bsv_expToStr s
        | PI_bno(b, None) -> "B-" + bsv_expToStr b
        | PI_subif(s)   -> "SIFC-" + s
        //| PI_var(ty, s) -> "V-" + htosp s        
        //| other -> sprintf "???PI%A" other
    and pos1 = function
        | [] -> "^"
        | [item] -> pos item
        | h::tt -> pos1 tt + "." + pos h
    pos x

and eeToStr = function
    | EE_typef(_, a)  -> "EE_typef:" + bsv_tnToStr a
    | EE_e(t, a)      -> sprintf "EE_e(%s,%s)"  (bsv_tnToStr t) (bsv_expToStr a)
    //| other -> sprintf "???EE%A" other


and bsv_expToStr = function
    | B_thunk(pi, args) -> sprintf "B_thunk(%s, %s)" (pathToS pi) (sfold (fun (tv, v) -> bsv_expToStr v) args)
    | B_lambda(None, _, _, _, _, _) -> sprintf "B_lambda(<anon>..."
    | B_lambda(Some s, _, _, _, _, _) -> sprintf "B_lambda(\"%s\")" s
    | B_reveng_l(ty, its)  ->
        let l = length its
        let pop (g, v) = "g=" + xbToStr g + ", v=" + bsv_expToStr v
        if l > 32 then sprintf "Reveng_l(%s, %i further entries ...)" (pop(hd its)) (l-1)
        else sprintf "Reveng_l<%i entries>(" l + sfold pop its + ")"

    | B_reveng_h (ty, its)  ->
        let l = length its
        let pop (g, v) = "g=" + bsv_bexpToStr g + ", v=" + bsv_expToStr v
        if l > 32 then sprintf "Reveng_h(%s, %i further entries ...)" (pop(hd its)) (l-1)
        else sprintf "Reveng_h<%i entries>(" l + sfold pop its + ")"

    | B_shift(x, a)      ->  "B_shift(" + bsv_expToStr x + "," + i2s a + ")"
    | B_mask(w, x)       ->  "B_mask(" + i2s w + "," + bsv_expToStr x + ")"
    | B_subs(df, x, s, lp)->  bsv_expToStr x + (if df then "." else "") + "[" + bsv_expToStr s + "]"
    | B_blift x          ->  bsv_bexpToStr x
    | B_query(false, x, t, f, _, _)   ->  "NS(" + bsv_bexpToStr x + ")?" + bsv_expToStr t + ": " + bsv_expToStr f
    | B_query(true, x, t, f, _, _)    ->  "S(" + bsv_bexpToStr x + ")?" + bsv_expToStr t + ": " + bsv_expToStr f
    | B_var(dbi, s, lp)               ->  sprintf "var:%iL%s" dbi (htosp s)
    | B_dyn_b(s, b)      -> "dyn_b:" + s + ":" + bsv_expToStr b
    | B_vector(s, (idl, Bsv_ty_nom(_, F_vector, Tty(len, _)::_)))  -> sprintf "VEC-%s-" (bsv_tnToStr len) + htosp idl
    | B_string s         -> "\"" + s + "\""

//merge us:
    | B_maker_abs(idl, ats, (rootmarked, formal_prams, formal_args, formal_iftype, aliases), provisos, body, ee_inner, backdoors, lp) ->
        sprintf "B_maker_abs(%s, ...)" (htosp idl)

    | B_moduleInst(bv, (evc, ifc_name, ifc_ty), iname, rhs, lp) -> sprintf "B_moduleInst (DO) %s/%s <- (" (htosp bv) (htosp ifc_name) + (if iname=ifc_name then "" else " iname=" + htosp iname) + ", " + bsv_expToStr rhs + ")"
    | B_maker_l(name, ats, (pformals, ised, iexported, hardstate_), stmts, provisos, backdoors, lp) ->
        let f2s (so, ty, name) = (soToStr false so) + bsv_tnToStr ty + " " + name
        let g2s (name, ty) = name + ":" + bsv_tnToStr ty
        let p = if nullp provisos then "" else "[" + sfold (fun x->x.ToString()) provisos + "]"
        let q = if nullp pformals then "" else "#(" + sfold f2s pformals + ") " 
        in
          "B_maker_l(" + htosp name + q +  ", [" + sfold f2s (ised @ [(SO_none "", iexported, "ifc")]) + "])" + p + ";\n" +
          sfoldcr bsv_stmtToStr stmts +
          ")"
    | B_fsmStmt(x, _)       -> sprintf "B_fsmStrm(%s)" (bsv_fsmStmtToStr x)
    | B_diadic(oo, l, r, _) -> bsv_expToStr l + " op" + f1o3(xToStr_dop oo) + (bsv_expToStr r)
    | B_action(id, lst, rvo_, lp) -> sprintf "(B_action [len=%i]" (length lst) + sfold bsv_stmtToStr lst + " endaction)"

    //| B_applyD(mkname, iactuals, lp) -> "B_applyD(" + bsv_expToStr  mkname + "(" + sfold eeToStr iactuals + ")"
    | B_applyf(it, rt, args, lp)  -> "B_applyf(" + pathToS it + ", rt=" + bsv_tnToStr rt + ", [" + sfold (fun (t,v)->bsv_expToStr v) args + "])"    

    | B_applylam(acp, it, args, lp, f1, f2)  -> "B_applylam(" + pathToS it + ", [" + sfold (fun (t,v)->bsv_expToStr v) args + "])busy=" + sprintf "%A" !f1 + " ans=" + optToStr !f2

    | B_format(strings, args) -> "FORMAT(" + sfold (bsv_expToStr) strings + ")"
    | B_ifc(path, ty) -> "B_ifc(" + pathToS path + ":" + bsv_tnToStr ty + ")"
    | B_ifc_immed(items) -> (if false then "B_ifc_imed(SUBINTERFACE, " else "B_ifc_imed(")  + sfold bsv_stmtToStr items + ")"
    | B_subif(ss, b, ty, lp)     -> "B_subif:" + ss + ":" + bsv_expToStr b
    | B_hexp (t, x)           -> "hexp:" + xToStr x
    | B_pliStmt(id, args, lp)-> id + "(" + sfold bsv_expToStr (map snd args) + ")"
    | B_field_select(tag, it, baser, width) -> "B_field_select(" + tag + ", " + bsv_expToStr it  + ")" //baser,width order deprecated
    | B_bit_replace(ty_, ov, nv, width, baser, lp) -> sprintf "B_bit_replace(ov=%s, nv=%s, w=%i, b=%i)" (bsv_expToStr ov) (bsv_expToStr nv) width baser // width, baser int ordering approved.
    | B_ferref(ty_, fer, lp) -> "@" + fer
    | B_aggr(tidl, bobs) ->
        let bobToStr = function
            | TU_struct pairs -> "struct:" + sfold (fun (tag, (ty_, w, pos, vale)) ->  "." + tag + "(" + bsv_expToStr vale + ")") pairs
            | TU_tunion(vale, tag, no, _, _) -> "tunion:" + tag + "(" + bsv_expToStr vale + ")"
            | TU_enum(tag, _,  _) -> "enum:" + tag
        "B_aggr(" + bsv_short_tnToStr tidl + "," + bobToStr bobs + ")"


    | B_abs_lambda(idl3, ty3, fids, body3, ee3, lp3) -> sprintf "B_abs_lambda(%s ...)" (htosp idl3)

    | other -> "???e " + other.ToString()    

and envaugToStr = function
    //| Aug_tags of (string * (bsv_type_t * int)) list
    //| Aug_tycl of string * string list * bsv_type_t list * (string * bsv_type_t) list * linepoint_t
    | Aug_mut(bound_name, idl, (ty, fer, lp)) -> sprintf "Aug_mut(%s/%s)" fer (hptos bound_name)
    | other -> sprintf "envaug???%A" other

and tyclassToStr (idl, args) = htosp idl


and bsv_fsmStmtToStr = function
    | Bsv_seqFsmStmt (a, b, lp) -> sprintf "seq(%s,%s)" (bsv_fsmStmtToStr a) (bsv_fsmStmtToStr b)
    | Bsv_parFsmStmt (a, b, lp) -> sprintf "par(%s,%s)" (bsv_fsmStmtToStr a) (bsv_fsmStmtToStr b)    
    | Bsv_whileFsmStmt(x, s, lp) -> "while(" + bsv_bexpToStr x + "){" + bsv_fsmStmtToStr s + "}" 
    | Bsv_ifFsmStmt(x, s, None, lp) -> "if(" + bsv_bexpToStr x + "){" + bsv_fsmStmtToStr s + "}" 
    | Bsv_ifFsmStmt(x, s, Some sf, lp) -> "if(" + bsv_bexpToStr x + "){" + bsv_fsmStmtToStr s + "}else{" + bsv_fsmStmtToStr sf + "}"
    | Bsv_eascFsmStmt(x, id, lp) -> "FsmStmt(" + id + "," + bsv_expToStr x + ")" // Expression as command (easc).    
    | s -> "???s:" + s.ToString()  

and bsv_bexpToStr = function
    | B_orred x ->  bsv_expToStr x
    | B_or []   -> ":False"
    | B_and []  -> ":True"
    | B_or lst  -> "or(" + sfold bsv_bexpToStr lst + ")"
    | B_and lst -> "and(" + sfold bsv_bexpToStr lst + ")"
    | B_not x   -> "!(" + bsv_bexpToStr x + ")"        
    | B_bexp x  -> xbToStr x
    | B_bdiop(bo, lst) -> "B_bdiop(" + f3o4(xbToStr_dop bo) + ", " + sfold bsv_expToStr lst + ")"
    | B_firing s -> sprintf "B_firing(%s)" s
    //| other    -> sprintf "???b %A " other + other.ToString()




and bsv_stmtToStr = function
    | Bsv_methodDef(mood, mname, proto, ephemeralf, rt, formals, guard, cmds, lp) ->
        let f2s (so, ty, name) = bsv_tnToStr ty + " " + name
        "Bsv_methodDef:" + mood + " " + htosp mname + (if ephemeralf then "<ephemeral>" else "") + " " + bsv_tnToStr rt + " ( " +  sfold f2s formals  +  ")" +
             //(if guard=None then "" else " if ( " +  bsv_bexpToStr(valOf guard))  +  ")\n" +
        " if ( " +  bsv_bexpToStr guard + ")\n" +             
        sfoldcr bsv_stmtToStr cmds + "\n" +
        "endmethod " + htosp mname + " \n"

    | Bsv_resultis((et, e), lpo) ->  "   resultis (" + bsv_expToStr e + ");\n"

    | Bsv_eascActionStmt (e, lp) -> "  Bsv_eascActionStmt " + bsv_expToStr e + ";\n"

    | Bsv_rule(rulerec, guard, stmts, keyname, lp) ->
        "rule " + htosp keyname + " ( " +  bsv_bexpToStr guard  +  ")\n" +
        sfoldcr (fun x -> "   " + bsv_stmtToStr x) stmts + "\n" +
        "endrule " + rulerec.name + " \n"

    | Bsv_primBuffer(l, r, lp) -> bsv_expToStr l + " :== " + bsv_expToStr r + ";\n" 

    | Bsv_ifThenStmt(x, s, None, strict, lp)    -> "if(" + bsv_bexpToStr x + "){" + bsv_stmtToStr s + "}" 
    | Bsv_ifThenStmt(x, s, Some sf, strict, lp) -> "if(" + bsv_bexpToStr x + "){" + bsv_stmtToStr s + "}else{" + bsv_stmtToStr sf + "}"
    | Bsv_varDecl(sv, ty, idl, None, lp)      -> (if sv then "varDecl(SV, " else "varDecl(")  + bsv_tnToStr ty + " : "  + htosp idl + ";"
    | Bsv_varDecl(sv, ty, idl, Some subs, lp) -> (if sv then "varDecl(SV, " else "varDecl(")  + bsv_tnToStr ty + "[...] : " + htosp idl + ";"     
    | Bsv_beginEndStmt(lst, lp) -> "begin ... " + i2s(length lst) + " ... end "
    | Bsv_varAssign(real, idl, exp, whom_, lp) -> "real=" + boolToStr real + " " + htosp idl + " <-ifc-- " + bsv_expToStr exp // The whom field is just for debug.

    | Bsv_skip _ -> "skip"
    // other -> sf("???ms:" + sprintf "%A" other)


let ty_untie = function
    | Bsv_ty_knot(idl, k2) when nonep !k2   -> valOf !k2
    | other -> other
    
// subexpression share
let rec bsvc_walker_gen ww vd bobj (dict:lowered_expressions2_t) = 
    let vdp = false
    let unitfn nn =
        bobj.e_roots.add nn "" // If encountered then mark as a 'root' expression.
        // set rooflag let _ = 
        ()
        
    let null_lfun strict clkinfo arg rhs = ()
    let null_bsonchange costs nodeinfo nn (a,b) = b

    let null_sonchange costs nodeinfo nn (a,b) = // TODO costs and uses are in the walker as well - use them ?!
        let (found, ov) = dict.TryGetValue a
        //let _ = assertf (found, fun () -> "walked unfound expression " + xToStr a)
        if not found then () // vprintln 0 ("walked unfound expression " + xToStr a)        
        else mutaddonce ov.parents nn
        b
    let opfun arg N bo xo a b = a 
    let (pina_, sS) = new_walker vd vdp (true, opfun, (fun (_) -> ()), null_lfun, unitfn, null_bsonchange, null_sonchange)
    sS


let g_integer_width = 32


let leaf_grounded_type_pred = function
    | Bsv_ty_fsm_stmt _ 
    | Bsv_ty_format
    | Bsv_ty_dontcare _
    | Bsv_ty_intconst _
    | Bsv_ty_intexpi _
    | Bsv_ty_int_vp _      //Watch out for int_vp - it may get imperialised and so is not grounded for eval etc - TODO explain please.  
    | Bsv_ty_integer    
    | Bsv_ty_primbits _ -> true
    | _ -> false

//        | Bsv_ty_primbits _ // uint is not really grounded  ?? 



let kren flst idl =  idl// for now  need to squirrel TODO


//
// Encoding width: number of bits needed to represent an expression.
//
let enc_width_o ww (mf:unit->string) arg =


    let m_rec_flag = ref false
    
    let rec enc_width_os trial stack aa =
        //dev_println (sprintf "enc_width_o %A start" aa)
        //vprintln 0 (m + sprintf " taking enc_width_os trial=%A m_rec_flag=%A of %s" trial !m_rec_flag (bsv_tnToStr aa))
        match aa with
        | Bsv_ty_integer ->
            vprintln 3 (mf() + ": Deprecated width of Integer taken!") // Better to return WNone?
            Wsome g_integer_width // TODO - needed for Maybe#(Integer) to be lowered but perhaps better to say Integer is not in bits class. Check for commented out fromInteger?

        | Bsv_ty_id(so, _, (fid, _))   ->
            if trial then (m_rec_flag := true; Wnone)
            else sf (mf() + ": Need width for a type formal: "  +  fid)

        | Bsv_ty_knot(idl, k2) when not(nonep !k2) ->
            let key = kren "" idl
            if memberp key stack then
                vprintln 3 ("enc_width: recursoove " + htosp idl)
                Wnone
                else enc_width_os trial (key::stack) (valOf !k2)
        | Bsv_ty_knot(idl, k2) -> Wknot "$UNTIED" // TODO are all uses for knots unsized?  Infinite structures...?

        | Bsv_ty_intconst n when n>0     -> Wsome (bound_log2 (BigInteger n))

        | Bsv_ty_primbits(s) -> Wsome s.width    

        | Bsv_ty_nom(_, F_action, [_; Tty(ct, _)]) -> Wnone // g_void_prec

        | Bsv_ty_nom(_, F_vector, [_; Tty(ct, _)]) ->enc_width_os trial stack ct

        | Bsv_ty_nom(_, F_actionValue rt, _) -> enc_width_os trial stack rt

        | Bsv_ty_nom(idl, F_enum(max_in_use, items), [])  ->
            // For instance, the Bool type uses values 0 and 1.  1 is the maximum in use.  1 bit is sufficient to hold the max in use.
            // Alternatively, for an enum where the max in use is 12 we need four bits.
            Wsome (bound_log2 (BigInteger max_in_use))

        | Bsv_ty_nom(ats, F_struct(items, _), _) ->
            if memberp ats.nomid stack then
                m_rec_flag := true
                vprintln 3 ("Recursive struct for width find : " + htosp ats.nomid) 

            let stack' = ats.nomid :: stack
            let m = mf() + " struct item"
            let structwidth sofar (tag, arg_ty) =
                match sofar with
                    | Wsome w ->
                        match enc_width_os trial stack' arg_ty with
                            | Wsome v -> Wsome (v+w)
                            | _ -> Wnone
                    | _ -> Wnone
            List.fold structwidth (Wsome 0) items 

        | Bsv_ty_nom(ats, _, []) when ats.nomid = g_canned_Clock_name -> Wsome 1
        | Bsv_ty_nom(ats, _, []) when ats.nomid = g_canned_Reset_name -> Wsome 1            

        | Bsv_ty_nom(ats, F_ntype, _) ->
            vprintln 3 (sprintf "+++strange ask for bitwidth of %s" (bsv_tnToStr aa))
            Wnone
            
        | Bsv_ty_nom(ats, F_tunion(br, variants, details), _) ->
            match !br with
                | MR_pending   -> Wknot "width-pending"
                | MR_recursive -> Wnone // Recursive unions cannot be rendered in h/w for now.
                | _ ->
                    let max2 a b = if a>b then a else b
                    let stack' = ats.nomid :: stack
                    if memberp ats.nomid stack then
                        m_rec_flag := true
                        if trial then ()
                        else cleanexit("recursive unions are not supported in generated hardware (infinite encoding width problem); they must be elaborated away at compile time! name=" +  htos ats.nomid)
                    let unionwidth sofar (tag, arg_ty) =
                        match arg_ty with
                            | None -> sofar
                            | Some sty ->
                                match enc_width_os trial stack' sty with
                                    | Wsome v -> max2 v sofar
                                    | _ -> sofar

                    let field_width = List.fold unionwidth 0 variants
                    Wsome(field_width + bound_log2(BigInteger(length variants)))


        | Bsv_ty_nom(ats, F_ifc_1 ii, [Tty(thearg, fid_)]) when ats.nomid = [ "Reg"; "Prelude" ] ->
            
            dev_println(sprintf "evc: strange interface width request on %A - perhaps for clock prams? TODO" (bsv_tnToStr aa))
            //enc_width_os trial (["evckey"] :: stack)  thearg
            Wsome 1

        | Bsv_ty_dontcare s ->
            if s=g_hydrate_being_done then m_rec_flag := true
            Wnone
            
        | Bsv_ty_format   -> Wnone        

        | other -> cleanexit(mf() + sprintf ": other form enc_width in %A\nraw=%A" (bsv_tnToStr other) other)

    // Make a trial run to see if recursive
    let a0 = enc_width_os true [] arg
    
    let a1 = if !m_rec_flag then Wnone else enc_width_os false [] arg
    a1


// encoding width_ev : calls enc_width_o
let enc_width_ev ww (ee:ee_t) mf arg =
    let rec worker stack = function
        | Bsv_ty_id(_, _, (fid, _)) ->
            //let fid = if fid = "a49ty10"  || fid = "a50ty10" then "index_t" else fid // debug kludge
            match ee.genenv.TryFind [fid] with
                | None ->
                    let vd = 0
                    vprint vd "Inner in-scope identifiers are: "
                    for z in ee.genenv do ignore(vprint vd (htosp z.Key + " ")) done
                    //for z in ee.genenv do vprintln vd (sprintf ">%A<" z.Key) done
                    vprintln vd ""
                    cleanexit(mf() + sprintf ": encoding width: unbound type formal %s" fid)

                | Some(GE_binding(x, whom, EE_typef(_, ty))) ->
                    let loop_key = (fid, whom.whom)
                    if memberp loop_key stack then
                        vprintln 3 ("encoding width returned as none since only placeholder binding for formal " + fid)  // bound as self since no further info available yet
                        Wnone
                    else
                        //vprintln 0 ("fish enc_width go round again on " + fid)
                        worker (loop_key::stack) ty

                | Some other -> muddy (sprintf "enc_width (pis22) other form: %A" other)

        | other -> enc_width_o ww mf other 

    worker [] arg

let enc_width_no ww (ee:ee_t) mf a =
        match enc_width_ev ww ee mf a with
            | Wsome x -> x
            | _ -> cleanexit(mf() + sprintf ": (_no) no enc_width for %A" a)


let enc_width_e ww mf a = // e for error
    match enc_width_o ww mf a with
        | Wsome x -> x
        | _       -> sf (mf() + sprintf ": (_e) no enc_width for %A" a)



let find_action_or_method id = function
    | Tfa(ig_, sigma) ->
        let rec scan lst =
            match lst with
                | Sigma((_, _, mname), _, _, _)::_ when id=mname -> hd lst
                | _::tt -> scan tt
                | [] -> sf(sprintf "find_action_or_method: not found %s in %A" id sigma) 
        scan sigma
    //| other -> sf(sprintf "find_action_or_method: want %s from %A" id other)   

let find_methname = function
    | Sigma(id, _, _, _) -> id

let find_en_o = function
    | Sigma(id, _, (BsvAction(_, _, en), _, _), _) -> Some en
    | other -> None

let find_en = function
    | Sigma(id3, _, (BsvAction(_, _, en), _, _), _) -> en
    | Sigma(id3, _, _, _) -> sf (sprintf "%s has no enable since not an Action" (f3o3 id3))
    
let find_rdy = function
    | Sigma(id, _, (BsvValue(Some rdy), _, _), _)
    | Sigma(id, _, (BsvAction(Some rdy, _, _), _, _), _) -> rdy
    | Sigma(id, _, (BsvAction(None,  _, _), _, _), _)
    | Sigma(id, _, (BsvValue None, _, _),_) -> xi_num 1
    | Sigma_e _ -> xi_num 1    

let find_args site = function
    | Sigma(id, _, _, args) -> args
    | Sigma_e(name_, rt, proto, formals, callee_explicit_condition, actions, lp) ->
        sf (site + ": Should not call find_args on ephemeral method: " + name_)

let find_rv mm = function
    | Sigma(id, _, (_, RV_netlevel(ty, rv), _), _) -> rv
    | (Sigma(id3, _, _, _)) as other ->
        //vprintln 0 (sprintf "It was %A" other) // TODO delete me
        cleanexit(mm + sprintf ": no return value provided in method %s of %s" (f3o3 id3) (hptos(f1o3 id3)))

let rec find_rvo = function
    | Sigma(id, _, (_, RV_netlevel(ty, rv), _), _) -> Some rv
    | Sigma_e _ 
    | Sigma(_, _, _, _) -> None

and gen_bufif (l, r, en) = XIRTL_buf(en, l, r)        // Generate a bufif1 (tri-state continuous assignment).

and gen_buf (l, r) = XIRTL_buf(X_true, l, r)          // Generate a buffer (continuous assignment) - add to synch logic.

and gen_dflop (g, l, r) = gen_XRTL_arc(None, g, l, r)  // Create a D-type flip flop in current clock domain. Reset and global clock enables are part of the domain.

let g_blank_ii = { iparts=[]; imeta=[] }
let g_ty_action = g_action_ty
let g_ty_empty = Bsv_ty_nom({g_blank_nom_typ_ats with nomid=["Empty"; "Prelude" ]; }, F_ifc_1 g_blank_ii, [])    
let g_ty_sum_monad = Bsv_ty_nom({g_blank_nom_typ_ats with nomid=["$M"]; }, F_monad, [])
let g_ty_sum_void  = Bsv_ty_nom({g_blank_nom_typ_ats with nomid=["$V"]; }, F_monad, []) 

// Convert a rule name to string form.
let ruleissueToStr0 fullpathf (name, path, issue_no) =
    let ss = if not fullpathf || nullp path then name else hptos path + "_" + name
    if issue_no >= 0 then ss + sprintf "_%i" issue_no else ss

let ruleissueToStr ruleissue = ruleissueToStr0 true ruleissue


let gen_B_action site funcf (rulename, lst, retval, lp) =
    // This cannot always be used since commision is fussy. TODO. use alternate to resultis?
    // This code is somewhat abused - function returns need to be a single expression but this could potentially be
    // an actionvalue block.  The problem is we currently use the action wrapper around a list of elaborated items and
    // seem to allow variable declarations inside function bodies.  We sort this out for now using the funcf flag that
    // should ensure that we trim off excess variable declarations and so on from function bodies that tend to stop
    // compile-time constant folding (i.e. running functions at compile time (or elab time pre ewalk)).


    let ruleissue = (rulename, [], -1) // for now
    let rec functidy arg =
        match arg with
            | [] -> []
            | Bsv_resultis _  :: tt_     -> [hd arg]    // Delete unreachable code beyond a return statement.
            | Bsv_eascActionStmt _ :: tt -> functidy tt // Might want this under some coding styles if there is no resultis - i.e. genuinely using a statement as a return value but a function should have a proper resultis statement. 
            | Bsv_varDecl _ :: tt        -> functidy tt // delete variable declarations
            | [other] ->  [other]
            | other :: tt ->
                vprintln 0 (sprintf " +++ Not sure how to tidy function body item %s" (bsv_stmtToStr other))
                other :: (functidy tt)
                
    let lst = if funcf then
                  //vprintln 3 ("site" +   whereAmI_str "\n")
                  reportx 3 ("function body tidy of " + ruleissueToStr ruleissue) bsv_stmtToStr lst
                  functidy lst
              else lst            
    match lst with
        | [Bsv_resultis((_, re), lp)] when false -> // This drops the retval of top-level concrete methods, so disabled for now. But soon commission will not lower its rv
            vprintln 3 (sprintf "gen_B_action site %s rulename=%s funcf=%A - cancel with solo resultis" site (ruleissueToStr ruleissue) funcf)
            re
        | lst ->
            vprintln 3 (sprintf "gen_B_action site %s rulename=%s funcf=%A - workme " site (ruleissueToStr ruleissue) funcf)
            let retval2 =
                let unwrap = function
                    | Bsv_ty_nom(_, F_actionValue rt, _)  -> rt
                    | other -> other
                match retval with
                    | RV_highlevel ty -> RV_highlevel(unwrap ty) // further stupid TODO
                    | RV_noret -> RV_noret
                    | RV_netlevel(ty, rv) -> 
                        vprintln 3 (sprintf "netlevel clause also TODO needs deaction ? %A" (bsv_tnToStr ty))
                        RV_netlevel(unwrap ty, rv)
            let ans = B_action(ruleissue, lst, retval2, lp)
            //let msg = (funique "CDUMP L1882")
            //dev_println msg
            //if msg = "CDUMPL1882_2980" then sf "hitit"
            ans



let hindToStr = function
    | (None, names, vp) -> " unified name group={" + sfold (gmm vp) names + "}"
    | (Some sty, names, vp) -> bsv_tnToStr sty + " hind bound to {" + sfold (gmm vp) names  + "}"
                    

// Hindley only contains lower- case type variable names and formal parameters for the currently entity being typechecked. 
let tcc_dig (cc:hindley_t) id =
    let rec memberp3 id = function
        | [] -> false
        | (tag, bindindex)::tt  when id = tag -> true
        | _ :: tt -> memberp3 id tt

    let rec dig sofar = function
        | [] -> (None, sofar)
        | (item, names, vp)::tt when memberp3 id names -> (Some(item, names, vp), tt @ sofar)
        | other::tt -> dig (other::sofar) tt
    dig [] cc

#if SPARE
let tcc_dig_nf_basis (cc:hindley_t) id =
    let rec dig sofar = function
        | [] -> (None, sofar)
        | (item, names)::tt when memberp3 id names -> (Some(item, names), tt @ sofar)
        | other::tt -> dig (other::sofar) tt
    dig [] cc
#endif


let tcc_tagfind (tb:hindley_t) tag = // Simple lookup in uncommitted to ee tags
    let rec memberp3 id = function
        | [] -> false
        | (tag, bindindex)::tt  when id = tag -> true
        | _ :: tt -> memberp3 id tt
    let rec dig = function
        | [] -> None
        | (item, names, vp)::tt when memberp3 tag names -> Some(item)
        | other::tt -> dig tt
    dig tb

let m_debreun = ref 50 // Arbitrary start value.

let deb_baseline () = !m_debreun

// Allocate a stack marker so that we can do a 'pop' operation on a map, by removing all bindings added above a given level.
let fresh_bindindex tag =
    let nv = !m_debreun 
    mutinc m_debreun 1
    { brun_tag=  tag
      brun_idx=  nv
    }

// Remove inner scope identifiers from the resolution hindley.
// Inner scope identifiers will have brun_idx greater than the starting value stored in scope base.    
let unscope scopebase hindley =
    let unscope_worker (type_o, names, vp) =
        let unname (fid, debrun) cc =
            if debrun.brun_idx >= scopebase then
                //vprintln 4 (sprintf "unscope/unname fid %s with %i>=%i for %s"  (hptos fid) debrun.brun_idx scopebase debrun.brun_tag)
                cc
            else (fid, debrun)::cc
        let names = List.foldBack unname names []
        
        (type_o, names, vp) // Can perhaps delete the entry when names are null but makes little difference.
    map unscope_worker hindley

    
let bidToStr debrun = sprintf "%s:%i" debrun.brun_tag debrun.brun_idx
    
let tcc_add_binding (h0:hindley_t) vp (tag, sty) =
    // muddy ("tcc-tagadd " + tag)
    let nb = (Some sty, [([tag], fresh_bindindex "tcc_add_binding")], vp)
    nb::h0

let tcc_add_tag (h0:hindley_t) vp (tag, sty) = // same code as add_binding
    // muddy ("tcc-tagadd_tag " + tag)
    let nb = (Some sty, [([tag], fresh_bindindex "tcc_add_tag")], vp)
    nb::h0



let rec dropapp ff arg =  // pre function
    let sil0 (s,t) = (s, ff t)
    match arg with
    | Tanno_hof _ -> arg // these are done at post site
    | Tannot lst -> Tannot(map sil0 lst)    
    | Tanno_rhs lst -> Tanno_rhs(map sil0 lst)
    | Tanno_do(a, l1, l2) -> Tanno_do(a, map sil0 l1, map sil0 l2)
    | Tanno_function(rt, tve, provisos, args) ->
        let sil (id, cp, drop) = (id, cp, dropapp ff drop)
        Tanno_function(ff rt, map sil tve, provisos, map (fun (s,t) -> (s, ff t)) args)
    //| other -> sf (sprintf "dropapp other form %A" other)


let add_stars n = function
    | Bsv_ty_stars(m, a) ->
        let q = m+n
        if q=0 then a else Bsv_ty_stars(q, a)
    | a -> if n=0 then a else Bsv_ty_stars(n, a)
    


let vp_resolve = function
        |  (nf, VP_none _) -> nf
        |  (VP_none _, nf) -> nf

        |  (VP_type , VP_type ) -> VP_type 

        |  (VP_value(b1, s), VP_value(b2, t)) ->
            VP_value(b1 || b2, lst_union s t) // More than one is a latent error.


        |  (x, VP_type) 
        |  (VP_type, x) -> x

#if SPARE        
        //|  (x, VP_value(b, []))
        //|  (x, VP_value(b, [""]))        
        //|  (VP_value(b, [""]), x) 
        //|  (VP_value(b, []), x) ->
            match x with
                | VP_value(b1, y) -> VP_value(b || b1, y)
                | other when b -> sf (sf "need insert numf in other %A" other)
                | other -> other
#endif

        //|  (nf_l, nf_r) -> sf (sprintf "other form vp_resolve %A" (nf_l, nf_r))



let protoapp ff arg =
    match arg with
    | BsvProtoAction         -> BsvProtoAction
    | BsvProtoActionValue ty -> BsvProtoActionValue(ff ty)
    | BsvProtoValue ty       -> BsvProtoValue(ff ty)                
    | NoProto                -> NoProto
    //| other -> sf (sprintf "freshen_proto other form %A" other)


// The builtins are stored in a table as typep and converted on first use as follows:
// The rest of the compiler should not be littered with this form.        
// This serves as a freshen-on-retrieve for any unbound tyvars, such as the length field in a String or Vector.
let rec hydrate_type_builtins ww mf typen prams ty =  // Stupid args format to this function.
    let q2 = bsv_tnToStr ty

    let revert x = x
    let ans =
        if nullp prams then ty // cannot need hydrating if constant.
        else
        match ty with
        | Bsv_ty_nom(ats, F_ntype, [_]) when ats.nomid = ["Bit"] -> 
            let ww = WN "bit parameter" ww
            if length prams <> 1 then cleanexit(typen + q2 + " type requires one argument")
            match getival mf (map revert prams) with // TODO Fold here since one might need a previous - only one!
                | None -> Bsv_ty_nom(ats, F_ntype, [Tty(revert(hd prams), "bitwidth")])
                | Some w ->  Bsv_ty_primbits{ signed=None; branding= q2; width=w }

        | Bsv_ty_nom(ats, F_ntype, [_]) when ats.nomid = ["UInt"] -> 
            let ww = WN "UInt form..." ww
            if length prams <> 1 then cleanexit(typen + q2 + " type requires one argument")
            match getival mf (map revert prams) with // TODO Fold here since one might need a previous - only one!
                | None ->    Bsv_ty_nom(ats, F_ntype, [Tty(revert(hd prams), "uint_width")])
                | Some w ->  Bsv_ty_primbits{signed=Some Unsigned; branding= q2; width=w; }
                        
        | Bsv_ty_nom(ats, F_ntype, [_]) when ats.nomid = ["Int"] -> 
            // typedef Int#(numeric type width);
            let ww = WN "Int form..." ww
            if length prams <> 1 then cleanexit(typen + q2 + " type requires one argument")
            match getival mf (map revert prams) with // TODO Fold here since one might need a previous - only one!
                | None -> Bsv_ty_nom(ats, F_ntype, [Tty(revert(hd prams), "int_width")])
                | Some w ->  Bsv_ty_primbits{signed=Some Signed; branding= q2; width=w; }
                        

        // Vector is a little bit of an anomoly being here. String is implemented as a vector and needs some help however..
        | Bsv_ty_nom(ats, F_vector, [Tid(_, l_fid); Tid(_, ct_fid)]) when ats.nomid = ["Vector"] -> 
            // typedef struct Vector#(type numeric vsize, type element_type);
            if length prams <> 2 then cleanexit(typen + " Vector type requires two arguments")
            //let prams = map temp_revert prams
            let len = revert (hd prams)
            //if (len < -1) then cleanexit(m + " Negative length Vector attempt made")
            //if (len > 12345678) then cleanexit(m + " Too long Vector attempt made")
            let ct = revert (cadr prams)
            let ww = WF 3 "hydrate_type_builtins" ww (sprintf "Vector form %s len=%s ct=%s" (htosp ats.nomid) (bsv_tnToStr len) (bsv_tnToStr ct))
            // Dont need to freshen formals if returning Tty since the name in a Tty is just a debugging hint.
            Bsv_ty_nom(ats, F_vector, [Tty(len, l_fid); Tty(ct, ct_fid)]) // no special action

        | other -> sf (sprintf "ntype %s other form %A. args=%A" typen other prams)
    //dev_println (sprintf "hydrated lookup of %s is %s" typen (bsv_tnToStr ans))
    ans

and intansi v = Bsv_ty_intexpi(B_hexp(Bsv_ty_integer, v))

and bintansi i = intansi(xi_bnum i)
and bintansi32 i = intansi(xi_num i)


and getival mf = function // TODO lift this to return bigInteger 
// cf mi_valueOf etc...
        | [arg] ->
            match arg with
                | Bsv_ty_integer    -> Some -1 // unspecified length
                | Bsv_ty_intconst n -> Some n // TODO call manifester
                | Bsv_ty_intexpi(B_hexp(_, constv)) -> Some (xi_manifest (mf() + ": getival3571") constv)
                | other -> None
        | []     -> cleanexit(mf() + ": A width argument was expected for %s")
        | other  -> sf (sprintf "getival other 2v %A" other)



// There is a difference between binding and unification:
// One variable may be bound to another which has no concrete binding. In this case the lhs of the
// hindley has form "Some (Bsv_ty_id tyvar)".    
// Alternatively, there may be more than one variable bound to the same concrete type or unbound variable and these all sit on the rhs of the hindley


// Get content type and length option for an array/vector type.
let array_deref mf ty =
    match ty with
        | Bsv_ty_nom(_, F_vector, [Tty(len, _); Tty(vector_content_ty, _)]) ->
            (vector_content_ty, getival mf [len]) // Type and int option for the length is returned.

        | other ->
            vprintln 0 (sprintf "Other=%A" other)
            sf(mf() + ": vector or other subscriptable type: dereference not supported. 2-d or higher array not yet supported.")


// TODO we do not need both array_deref and tyderef_o. Please merge.
            
// We remove one star in the subscripting operation since it is a dereference.
let tyderef_o (mf:unit->string) = function
    // Two forms of name are in use here? TODO tidy up.
    | Bsv_ty_nom(ats, _, [_; Tty (ct, _)]) when ats.nomid = ["SSRAM_Cambridge_SP1D"; "BRAM"]-> Some ct // PONG TODO tidy
    | Bsv_ty_nom(ats, _, [Tty (ct, _)]) when ats.nomid = ["List";"List"] || ats.nomid = [ "List" ]-> Some ct
    | Bsv_ty_nom(_, F_vector, [subscript_ty_; Tty(ct, _)]) -> Some ct
    | Bsv_ty_stars(ss, ct) when ss = 1 ->  Some ct
    | Bsv_ty_stars(ss, ct) when ss > 1 ->  Some(Bsv_ty_stars(ss-1, ct))
    | Bsv_ty_stars(ss, ct) ->  muddy "take one star"    
    | arg ->
        // Nasty hardwired function?  Perhaps cope with any such abstract type here?
        //dev_println(sprintf "other=%A" arg)
        vprintln 0 (sprintf ": tyderef other form: (not a collection or pointer type) %s" (bsv_tnToStr arg))
        None


let (g_e_recs:(WW_t -> (unit->string) -> bsv_exp_t -> BigInteger) option ref) = ref None


let rec powerXI ia n = if n=0I then 1I else ia * powerXI ia (n-1I)
let rec power2I n = if n=0I then 1I else 2I * power2I (n-1I)

//
// Evaluate (perhaps symbolically) a builtin 'type' function.
//
let run_type_fun ww ee mf fname args =
    let bsv_manifest1 = valOf !g_e_recs
    let rpred = function
        | (Bsv_ty_intexpi _) 
        | (Bsv_ty_intconst _) -> true
        | _ -> false

    if fname = "SizeOf" then
        if length args <> 1 then cleanexit(mf() + " SizeOf takes just one argument which is a type")
        match enc_width_ev ww ee (fun()->mf() + ":run_type_fun:sizeOf") (hd args) with
            | Wsome x -> intansi(xi_num x)
            | _       -> Bsv_ty_nom({g_blank_nom_typ_ats with nomid=[fname]; }, F_tyfun fname, map (fun ty -> Tty(ty, "")) args)
       // SizeOf for an enum gives the encoding width.
       // We do not also find valueOf here since it is not a type, it is a value whose arg is a type.

    elif conjunctionate rpred args then
        let mi_valueof arg = // TODO call getival
            match arg with
                | (Bsv_ty_intexpi be) -> bsv_manifest1 ww mf be
                | (Bsv_ty_intconst n) -> BigInteger(n)
                | other -> cleanexit(mf() + sprintf ": non-manifest: other form in mi_valueof %s" (bsv_tnToStr other))

        let args = map mi_valueof args
        let lbintansi arg =
            //vprintln 0 (sprintf "eval tyfun %s giving %A" fname arg)
            bintansi arg
            
        match fname with
            | "TLog" -> // This should round up.
                if length args <> 1 then cleanexit(mf() + sprintf " type function %s requires exactly one argument" fname)
                let ans = hd args
                let rec logto a p = if a >= ans then p else logto (a*2I) (p+1)
                if ans < 0I then cleanexit(mf() + " negative argument")
                else
                    let ans = logto 1I 0
                    //vprintln 3 (sprintf "TLog %A gives %A" arg ans)
                    intansi(xi_num ans)

            | "TExp" ->
                if length args <> 1 then cleanexit(mf() + sprintf ": type function '%s' requires exactly one argument" fname)
                lbintansi(power2I(hd args))

            | "TAbs" -> // A toy-compiler extension?
                if length args <> 1 then cleanexit(mf() + sprintf ": type function '%s' requires exactly one argument" fname)
                let ans = if hd args < 0I then 0I - hd args else hd args
                lbintansi(ans)
                        
            | "TAdd" -> lbintansi((hd args)+(cadr args))
            | "TSub" -> lbintansi((hd args)-(cadr args))
            | "TMul" -> lbintansi((hd args)*(cadr args))
            | "TDiv" ->
                let num = hd args
                let den = cadr args
                if den = 0I then cleanexit(mf() + sprintf ": type function '%s' has zero denominator." fname)
                lbintansi((num+den-1I)/den) // This primitive rounds up instead of down.
            | "TMax" -> lbintansi(if(hd args) > (cadr args) then (hd args) else (cadr args))
            | "TMin" -> lbintansi(if (hd args) < (cadr args) then (hd args) else (cadr args))
            
            | _ -> muddy (sprintf "Diadic type function '%s'  %A" fname args)
    else Bsv_ty_nom({g_blank_nom_typ_ats with nomid=[fname] }, F_tyfun fname, map (fun ty -> Tty(ty, "")) args)

let g_blank_bindinfo =
    {
        whom=       "anonymous"
        hof_route=   None
        synonymf=    false
    }

let rec list_first_occurrences ff = function // Delete subsequent occurrences of an item in a list.
    | [] -> []
    | h::tt ->
        if disjunctionate (fun x -> ff h x) tt then list_first_occurrences ff tt
        else h :: list_first_occurrences ff tt
        
let droppingToStr = function
    | Tanno_hof(rt, _, hofpaths, args)  -> "Tanno_hof(" + sfold (fun(x,y)->x + ":" + y) hofpaths + "," +  sfold (fun (aa, sty) -> aa + ":as:" + bsv_tnToStr sty) args + sprintf ", rt=%s)" (bsv_tnToStr rt)
    | Tannot args -> sfold (fun (aa, sty) -> aa + ":as:" + bsv_tnToStr sty) args
    | Tanno_function(rt, _, provisos_, args) ->
        if nullp args then "simple(" + bsv_tnToStr rt + ")"
        else "Tanno_function[RT=" + bsv_tnToStr rt + sprintf " ARGS(^%i)=" (length args) + sfoldcr (fun (aa, sty) ->"     " + aa + ":as:" + bsv_tnToStr sty) args + "]"




let g_freshen_no = ref 0 // make 2-d with token in future

(* When a phrase is defined and first type checked, it has dropping annotations made to be stored with it.
 * We avoid using type closures for their free vars by rewriting the types occuring in body of a phrase. Ditto its droppings.
 *
 * When a phrase is recalled, we recall its dropping annotations as well. We have actuals which might be fresh type varss or paramterised types, rather than concrete.
 * You might think afreshen only needs to be done on store and that doing it on every load is equivalent but less efficient, but the same phrase may be recalled more than once in the same scope with different tyactuals, so having each recall fresh is preserved. Matching afreshen for the phrase and droppings is needed.  We then
 * rewrite the phrase and its doppings with these actuals.  Can the afreshen rewrite be replaced with the direct substitution of the tyactuals? I can never quite remember why not. Maybe its because of some hindley/resolution later on for type variables not listed as formals but which are nominally free and fresh.
 *)
// sum_nf (aka tynormalise): Convert a type to a normal form whether there is just one name for each type identifier equivalence class.
// We use a tynormalise on save and freshen-on-retrieve policy for type variable name management.
// The input_ee is only used for imperial args to type functions (please explain).
let sum_nf ww bobj input_ee msg (the_hindley:hindley_t, body_gam_option) arg =
    let vd = bobj.sum1_loglevel // -1 for debug off ... 
    let operation_idx = funique "sum_nf" // Each sum_nf normalisation has a unique operational index which is only used in the log report for debug assistance.
    let ww = WF 3 "tynormalise (aka sum_nf)" ww (sprintf "Start operation_idx=%s for %s" operation_idx (bsv_tnToStr arg))
    //dev_println (sprintf "sum_nf input hindley for %s is %A" operation_idx the_hindley)
    let ordinal_name = function
        //| 0 -> "alpha"
        //| 1 -> "beta"
        //| 2 -> "gamma"
        //| 3 -> "delta"
        | n -> sprintf "_a%ity" n // Higher-arity names

    let eep = ref input_ee // This can get extended as new bindings are encountered, hence hold mutably.
   
    let next_id nf = // This creates names such as a49ty10 ... The funique aspect seems redundant?
        let new_name = funique(ordinal_name !g_freshen_no)
        mutinc g_freshen_no 1
        if vd>=6 then vprintln 6 (msg + sprintf ": next_id returns '%s'" new_name)
        new_name
        
    let index = new OptionStore<string, bsv_type_t option * string * valueprop_t>("index") // mutable store

    let kkk ss =
        if vd>=6 then vprintln 6 (msg + " swold: want to not ultimately discard info on " + ss) // TODO: say what is being discarded here?
        ss

    let mkindx (tyo, names, nf) =  // Insert elements of cc into the index (an OptionStore) mapping the old name to a fresh name. Each item has a fresh name. This is an equivalence class and all names in the group should be replaced with a common name.
        // TODO say whether we should ever expect to insert something not in cc? It's also a matter of whether it occurs in the top nom args.
        let resolver nf0 = function
            | (_, bindindex_) ->
                let resolved_numeric_flag = vp_resolve(nf0, nf)
                //vprintln 0 (sprintf "Resolving %A to %A" nf resolved_numeric_flag)
                resolved_numeric_flag
        let nf = List.fold resolver (VP_none "") names
        let fresh_name = next_id nf // This is outside the app since all names in the equiv class now map to this.
        let add_to_index = function
            | ([old_name], bindindex_) ->
                if vd>=6 then vprintln 6 (operation_idx + ":" + msg + sprintf "mkind: %s -> %s ty=%s (all names=%s)" old_name fresh_name (if nonep tyo then "None" else bsv_tnToStr (valOf tyo)) (sfold (gmm nf) names))
                let nst = Bsv_ty_id(nf, None, (fresh_name, (*fresh_bindindex*) "-19"));
                eep := { !eep with genenv= (!eep).genenv.Add([fresh_name], GE_binding([fresh_name], {g_blank_bindinfo with whom="nf_mkind"}, EE_typef(G_none, nst))) }
                // possibly we should check for repetitions of old_name?
                index.add (kkk old_name) (tyo, fresh_name, nf)
            | (idl, _) ->
                // Not a scalar name, so assume a global that needs no work.  TODO what about vector pairs?
                //sf ("mkindx: not a scalar name " + htosp idl)
                ()
        app add_to_index names

    let m_copied_knots = ref []
    let m_saved = ref []

    let rec taarg_so topf numberf stack = function // Replace type vars with the fresh names and type expressions recusively so.
        | Tid(nf, fid) ->
            //if numberf then vprintln 3 (sprintf "slfun %s perhaps whack3 %A" fid nf)
            match index.lookup fid with
                | None ->
                    let fresh_name = next_id nf
                    if vd>=5 then vprintln 5 (operation_idx + ":" + msg + sprintf " mkind taarg: subs %s ->  %s\n" fid fresh_name)
                    // length is the actual free numeric type variable.  This is numeric, so we do not bail on that case as a temporary rule. TODO please fix - is this related to the latent_tyvars or has gone now the synonymf is used.
                    //
                    let numberf' = numberf || nf_numberp nf
                    if not topf && vd>=5 then vprintln 5 (operation_idx + ":" + msg + sprintf "   mkind NON-TOP taarg: subs %s -> %s\n" fid fresh_name)
                    if not topf && not numberf' then vprintln 0 (msg + sprintf "   mkind NON-TOP and non-numeric taarg: subs %s -> %s nf=%A\n" fid fresh_name nf)
                    let nf' = nf // Could OR in numberf' here.
                    index.add fid (None, fresh_name, nf')
                    Tid(nf', fresh_name) // It is possibly redundant to use a new name on save/normalise since we will freshen on retrieve. But it helps with debugging.

                | Some(Some ty, fresh_name, nf) ->
                    match nf with
                        | VP_value(false, _) when numberf -> muddy ("should update flag on " + fid)   // TODO update if numberf and not nf
                        | _ -> ()
                    Tty(ty, fid)
                | Some(None, fresh_name, nf) -> Tid(nf, fresh_name)

        | Tty(ty, fid) ->
            let ans = tc_verbose numberf stack ty
            if numberf then if vd>=5 then vprintln 5 (operation_idx + sprintf ": slfun %s whack4-Tty numberf %s -> %s" fid (bsv_tnToStr ty) (bsv_tnToStr ans)) 
            Tty(ans, fid)

    and taarg_numeric topf = taarg_so topf true

    //and tc numberf stack arg = tcx numberf stack (None, false) arg  // No body_gamma_o on recursive (non-top) calls.

    //and tc__ = tc_verbose // Seg fault if used?

    and tc_verbose numberf stack arg = // A variant that prints some debug
        if vd>=6 then vprintln 5 (operation_idx + sprintf ":  tc_verbose %s start" (bsv_tnToStr arg))
        let ans = tcx numberf stack (None, false) arg     // No body_gamma_o on recursive calls.
        if vd>=5 then vprintln 5 (operation_idx + sprintf ":  tc_verbose %s -v-> %s" (bsv_tnToStr arg) (bsv_tnToStr ans))
        ans

    and tcx numberf stack (bgo, topf) arg = // bgo is a body_gamma to insert in a top function - saves using separate code to do that.
        match arg with
        | Bsv_ty_stars(n, sty) -> add_stars n (tc_verbose numberf stack sty)

        | Bsv_ty_id(nf0, prec, (fid, bidx)) -> 
            match index.lookup fid with
                | None ->
                    let new_name = next_id nf0
                    //if vd>=5 then vprintln 5 (operation_idx + ":" + msg + sprintf " sum_nf mkind: free ty_id, subs %s -> %s  numberf=%A nf0=%A\n" fid new_name numberf nf0)
                    let numberf' = numberf || nf_numberp nf0
                    if (* not topf && *) vd>=5 then vprintln 5 (msg + sprintf "   mkind: NON-TOPF free ty_id, subs %s -> %s\n" fid new_name)
                    if (* not topf && *) not numberf' && vd>=5 then vprintln 5 (msg + sprintf "   mkind: NON-TOPF free ty_id and non-numeric taarg, subs %s -> %s nf=%A\n" fid new_name nf0)
                    let nf' = nf0 // Could OR in numberf' here.
                    let nst = Bsv_ty_id(nf', None, (new_name, bidx));
                    eep := { !eep with genenv= (!eep).genenv.Add([new_name], GE_binding([new_name], {g_blank_bindinfo with whom="nf_mkind"}, EE_typef(G_none, nst))) }
                    index.add fid (None, new_name, nf')
                    if vd>=5 then vprintln 5 ("mkind: going round again on fid=" + fid)
                    tc_verbose numberf stack arg
                    
                | Some(None, new_name, nf) -> // may want to resolve precedence here - but put in NONE for now.
                    if numberf then if vd>=5 then vprintln 5 (operation_idx + sprintf ": %s: numeric:  no whack44a %s->%s  %A -> %A " fid  fid new_name  nf0 nf)
                    Bsv_ty_id(nf0, None, (new_name, bidx))  // DO not resolve - keep old annotation nf0
                | Some(Some ty, new_name, nf) ->
                    //if numberf && vd>=5 then vprintln 5 (operation_idx + sprintf ": %s: numeric: no whack44b %s->%s ;;  nf0=%A  " fid  fid new_name  nf0)
                    let repaint_anno = function
                        | Bsv_ty_integer ->
                            if vd>=5 then vprintln 5 (operation_idx + sprintf ": whack44b: fid='%s' can annotate Integer with %A" fid nf0)                            
                            Bsv_ty_int_vp nf0 // Note, preserve original nf0; do not use new nf.
                        | Bsv_ty_int_vp nf_already ->
                            match nf_already with
                                | VP_value(f, lst) ->
                                    match nf0 with
                                        | VP_value(f', lst') when f' = f && lst' = lst -> Bsv_ty_int_vp nf0 // already painted this way.
                                        | other -> sf (sprintf " whack44b-2: fid='%s' cannot annotate other for %A with %A" fid (other) nf0)
                                | other -> sf (sprintf " whack44b-3: fid='%s' cannot annotate other for %A with %A" fid (other) nf0)
                        | other ->
                            match nf0 with
                                | VP_value(_, x) when not (nullp x) -> sf (sprintf " whack44b: fid='%s' cannot annotate other for %A with %A" fid (other) nf0)



                                | VP_value(_, x) when not (nullp x) -> sf (sprintf " whack44b: fid='%s' cannot annotate other for %A with %A" fid (bsv_tnToStr other) nf0)
                                | _ -> other
                    //dev_println (sprintf "For %s retrieved %s" fid (bsv_tnToStr ty))
                    if numberf then repaint_anno ty else ty

        | Bsv_ty_knot(idl, ans) when memberp idl stack ->
            match op_assoc idl !m_copied_knots with
                | None ->
                    let pp = ref None
                    let ans = Bsv_ty_knot(idl, pp)
                    mutadd m_copied_knots (idl, pp) // need to tie this at the end
                    ans
                | Some pp -> Bsv_ty_knot(idl, pp)
                
        | Bsv_ty_knot(idl, ans) when nonep !ans ->
            //let ans = Bsv_ty_knot(idl, ref None)
            //mutadd m_copied_knots (idl, pp)
            sf ("untied_knot in normalise - should not happen" + htos idl)

        | Bsv_ty_knot(idl, ans) (* otherwise - knot body is processed *) ->  tc_verbose numberf (idl :: stack) (valOf !ans)

        //| Bsv_ty_sum_knott(idl, ans) -> when nonep !ans -> sf("no answer in sumtype normal form of " + htosp idl)
        | Bsv_ty_sum_knott(idl, ans) (* otherwise *)-> arg

        | Bsv_ty_nom(nst, F_ntype, args) ->
            let prams =  map (taarg_numeric topf stack) args
            let revert = function
                | Tty(ty, fid) -> ty
            let ans = hydrate_type_builtins ww (fun ()->msg) (hd nst.nomid) (map revert prams) arg
            mutadd m_saved (nst.nomid, ans) /// TODO squirrel -------------------- <--------------------- we need a hashcode of the args in case more than one mutually recusive type is involved.
            ans

        | Bsv_ty_nom(nst, F_tyfun fname, args) ->
            let args' = map (taarg_so topf true stack) args
            let lifted =
                let has_tid = function
                    | Tty(ty, fid) -> false
                    | Tid(_, s) -> true
                disjunctionate has_tid args'
            
            let ans =
                if lifted then Bsv_ty_nom(nst, F_tyfun fname, args')
                else
                    let revert = function
                        | Tty(ty, fid) -> ty
                        | Tid(_, s) -> sf (sprintf " still lifted despite check for fid=%s in type %s " (s) (bsv_tnToStr arg))
                    let mf () = "L2240-grounded"
                    run_type_fun ww (!eep) mf fname (map revert args')
            mutadd m_saved (nst.nomid, ans) /// TODO squirrel -------------------- <---------------------
            ans

        | Bsv_ty_list(id, lst) -> Bsv_ty_list(funique id, map (tc_verbose numberf stack) lst)

        | Bsv_ty_nom(nst, form, args) ->
            let repf x = function
                | Tty(ty, s) -> false // TODO what about bound as identifier forms - are they fully banned - better to ban them in the type system
                | Tid(_, s) ->
                    match x with
                        | Tid(_, s') -> s = s'
                        | _ -> false
            let ans = Bsv_ty_nom(nst, nomtog nst.nomid numberf stack bgo form, list_first_occurrences repf (map (taarg_so topf false stack) args))
            mutadd m_saved (nst.nomid, ans) /// TODO squirrel -------------------- <---------------------
            ans
            
        | Bsv_ty_methodProto(id, x, y, protoproto, sty) -> Bsv_ty_methodProto(id, x, y, protoproto, muddy "tc numberf stack sty")

        | ty when leaf_grounded_type_pred ty ->arg

        | other -> sf (sprintf "sumtype normal other form %A" other)

    and nomtog idl numberf stack body_gamma_o tystruct = // body_gamma_o is for top level insert only - not passed on a recurse.
        match tystruct with 
        | F_subif(iname, x) -> F_subif(iname, tc_verbose numberf stack x)
        | F_actionValue x   -> F_actionValue (tc_verbose numberf stack x)
        | F_ifc_1 ii        -> F_ifc_1 { ii with iparts=map (tc_verbose numberf stack) ii.iparts }

        | F_fun(rt, mats, args) ->  //F_fun of bsv_type_t * string list * bsv_type_t list
            let bg' =
                let bgf (id, cp, drop) =
                    let ans = dropapp (tc_verbose numberf stack) drop
                    //vprintln 0 (operation_idx + sprintf ":  body gamma: %s %s bg now %s" (htosp idl) id (droppingToStr ans))
                    (id, cp, ans)
                match body_gamma_o with
                    | None    -> map bgf mats.body_gamma
                    | Some bg -> map bgf bg // older bg now overwritten with this new.
            // Seem to have found a bug in fsharpc version 4.1 June 2019: even though above I have a let tc = tc_verbose, using tc instead of tc_verbose gives a dot net seg fault.
            let mats' = { mats with body_gamma=bg'; protocol= protoapp (tc_verbose numberf stack) mats.protocol }
            //vprintln 0 (sprintf "sum_nf %s new body_gamma is %A" mats.methodname mats'.body_gamma)
            F_fun(tc_verbose numberf stack rt, mats', map (tc_verbose numberf stack) args)

        | F_ifc_temp // placeholder till fully defined by prelude
        | F_action
        | F_enum _

        | F_vector
        | F_monad
        | F_module
            -> tystruct

        | F_tyfun _
        | F_ntype -> sf (sprintf "should be matched above")
            
        | F_tunion(br, args, details_) -> F_tunion(ref !br, map (tigo numberf stack) args, None)
        | F_struct(args, details_)     -> F_struct(map (tig numberf stack) args, None) // TODO check - we do not want numberf but false shold be passed in on recursive calls

    and tigo numberf stack = function
        | (id, None)   -> (id, None)
        | (id, Some t) -> (id, Some(tc_verbose numberf stack t))

    and tig numberf stack (id, t) =
        let ans = tc_verbose numberf stack t
        if vd>=5 then vprintln 5 (sprintf "tig sum_nf %A struct field '%s' detail %s -> %s" numberf id (bsv_tnToStr t) (bsv_tnToStr ans))
        (id, ans)

    let stack = []
    app mkindx the_hindley
    if vd>=4 then vprintln 4 (sprintf "sum_nf: normalise the_hindley inserted")

#if OLD_CODE_PLEASE_DELETE_ME
    let _ =
        match arg with
            | Bsv_ty_nom(ats, _, args) ->
                //vprintln 3 (sprintf "sum_nf: Insert explicit free args of %s (len=%i)" (htosp ats.id) (length args))
                let _ = map (taarg_so true false stack) args // This done anyway by first encounter with topf holding
                match op_assoc ats.id !m_copied_knots with
                    | None -> ()
                    | Some f -> sf "tieut"
                ()
            | _ -> ()
#endif
    let ans = tcx false stack (body_gam_option, true) arg

    let _ =
        let tie_copied(idl, pp) =
            match op_assoc idl !m_saved with
                | None ->            sf ("tie_copied - nothing was saved " + htos idl)
                | Some ans ->
                    pp := Some ans
                    ()
                    // ("tie_copied found " + htos idl)
        app tie_copied !m_copied_knots
    //let ww = WF 3 operation_idx ww (sprintf "tynormalise done %s ---TO---> %s" (bsv_tnToStr arg) (bsv_tnToStr ans))
    let ww = WF 3 operation_idx ww (sprintf "tynormalise done yielding %s" (bsv_tnToStr ans))    
    ans // end of sum_nf (aka tynormalise).



//
//
let cpToStr = function
    | CPS(g, pairs) ->
        let rec pf = function
            | [] -> ""
            | (s, recf, hof)::tt ->  pf tt + "." + (if recf > 0 then sprintf "RR%i$$" recf else "") + (if nonep hof then "" else "HOF^^") + s
        let gToStr = function
            | PS_suffix -> ""
            | PS_prefix _ -> "//"
        //(sprintf "%A//" g) + pf pairs
        gToStr g + pf pairs        



let anToStr (token, cp, dr) = token + ":" + cpToStr cp + ":" + droppingToStr dr
    
//let g_grounded_marker_ = "GGG" // Insert this in grounded callpaths to make clearer. Except top-level.


    
let join_cp bobj csite newtop bg = // Pushdown the body gamma below the new top.
    match (newtop, bg) with
        | (CPS(gl, lst_l), CPS(gr, lst_r)) -> // 
            if gr <> PS_suffix then vprintln 0  ("++++ BAD TODO " + csite + sprintf ": grounded extend\n  newtop=%s\n   bg=%s" (cpToStr newtop) (cpToStr bg))
            let rec_arity = f2o3 (hd lst_r)
            let aol = lst_r @ lst_l
            let ans = CPS(gl, aol)
            let recs = if rec_arity > 0  then "recursive" else ""
            //vprintln 3 (csite + sprintf ": %s join_cp to get " recs + cpToStr ans)
            if rec_arity > 0 then bobj.callgraph_recursions.add (f1o3(hd aol)) (map f1o3 (tl aol), rec_arity)
            ans
//        | other -> sf (sprintf "join_cp - second not abstract - other patterns %A" other)

// extend_callpath: push a new item on the top of a callpath.
// The only time we should need a kind as callpath component is at the root where there is no iname, but we have less unique work to do if we use kinds on the callpath if that kind has no typeprams.
let extend_callpath2 bobj csite (reccall, hof) uid = function
    | CP_none s ->
        vprintln 3 (csite + ": +++ extend_callpath: callpath none: no extend with id/tag=" + uid)
        CP_none s

    | CPS(gf, pl) ->
        let tailer () = 
            let ans = CPS(gf, (uid, reccall, hof) :: pl)
            //vprintln 10 (csite + sprintf ": extend_callpath: grounded=%A: extend with id/tag=" gf + uid + " to " + cpToStr ans)
            ans

        if gf = PS_suffix then tailer()
        else
        match bobj.callgraph_recursions.lookup uid with
            | [] -> tailer ()
            | possibles ->
                let relevant_site(idl, rec_arity) = (idl = map f1o3 pl)
                match possibles with
                    | [] -> tailer()
                    
                    | [(idl, rec_arity)] ->                
                        vprintln 3 (sprintf "trim back edge: in recursive callgraph for %s, unwind arity=%i to %s" uid rec_arity (htosp idl))
                        if rec_arity = 1 then CPS(gf, pl) // Simply drop the new potential extension.
                        else muddy "shs greater arity recursion TODO"
                        
                    | multi ->
                        vprintln 3 (sprintf "trim back edge: more than one for %s (no=%i) ! %A" uid (length multi) multi)
                        // Press on anyway with arith unity for now. TODO tidy
                        CPS(gf, pl) // Simply drop the new potential extension.

let extend_callpath bobj csite uid = extend_callpath2 bobj csite (0, None) uid 
    
let groundedp = function
    | CPS(gf, _) -> gf <> PS_suffix
    | _ -> false

let arity_check_hard site ty2ty mf idl gf sl =
    if length sl <> length gf then
        if ty2ty then
            vprintln 0 (sprintf "site=%s: F1=%A\nF2=%A" site gf sl)
            cleanexit(mf() + sprintf ": mismatch in number of arguments for %s: oneside expected=%i otherside expected=%i" (htosp idl) (length gf) (length sl))
        else
            vprintln 0 (sprintf "site=%s: F=%A\nA=%A" site gf sl)
            cleanexit(mf() + sprintf ": wrong number of arguments for %s: expected=%i provided=%i" (htosp idl) (length gf) (length sl))

let un_protoprotocol cc = function
    | BsvProtoValue tt -> tt::cc
    | BsvProtoActionValue tt -> tt::cc
    | BsvProtoAction  -> cc


let gen_ty_enum(ats, arity, max_in_use, mapping) = Bsv_ty_nom(ats, F_enum(max_in_use, mapping), [])

let cander_ifc tag_id = function
    | Bsv_ty_nom(_, F_subif(itag, _), _) when tag_id=itag -> true
    | Bsv_ty_nom(_, F_fun(_, fats, _), _) -> fats.methodname=tag_id
    | Bsv_ty_nom(ats, _, _) when ats.nomid = [tag_id] -> true
    | Bsv_ty_methodProto(s, _, _, _, _) -> s=tag_id 

    | _ -> false 


let bsv_ntToStr = function
    | Bsv_xntype s -> "XN:" + bsv_tnToStr s
    | Bsv_ntype(s, [])  -> "P:" + htosp s
    | Bsv_ntype(s, lst) ->
        let f2s (ty, name) = bsv_tnToStr ty + " " + name
        "P:" + htosp s  + "#(" + sfold tidToStr lst + ")"       


let g_blank_fid = { fprovisos=[]; protocol=NoProto; body_gamma=[]; fids=[]; methodname=""; who="anon" }



let g_builtin_functions =
    [
         "pack";
         "unpack";
         "fromInteger";
         "message";
         "error";
         "warning";
         "$format";
          "div";
         "quot";
         "mod";
         "rem";
         "max";
         "min";
         "exp";
         "zeroExtend";
         "signExtend";
         "extend";
         "truncate";
         "abs";
         "sizeof";
         "SizeOf";
     ]



let gen_GE_sumtype(fid, whom, sty) = GE_binding([fid], whom, EE_typef(G_none, sty))
                                                  

let parse_schedule msg lst = // BVI syntax or Cambridge syntax
    dev_println  (msg + " Parsing schedule " + sfold (fun x->x) lst)
    let rec splitter sofar = function
        | [] -> cleanexit(msg + ": Malformed schedule statement  "  + sfold (fun x->x) lst)
        | "("::t
        | ")"::t -> splitter sofar t
        | h::t when memberp h g_schedule_operators -> (rev sofar, h, t)
        | h::t -> splitter (h::sofar) t
    let (l, oo, r) = splitter [] lst
    (oo, cartesian_pairs l r)

let gen_Bsv_ty_list = function
    //let rec flatten = function // cannot flatten owing to id tags.
    //| [] -> nil
    //    | Bsv_ty_list(id, lst) -> lst @ 
    | [item] -> item
    | lst    -> Bsv_ty_list(funique "TYSW", lst)


  

let provToStr = function
    | Prov_sty ty -> "Prov_sty(" + bsv_tnToStr ty + ")"
    | Prov_si s -> s
    | other -> sprintf "%A" other


let rose = function
    | Tty(ty, ss) -> ty
    | Tid(nf, ss) -> Bsv_ty_id(nf, None, (ss, (*fresh_bindindex*) "rose"))


let arity_aug_static_path (x:string) arg = 
    let isdigit (x:char) = (x >= '0' && x <= '9')
    let atoi32 (x:string) = System.Convert.ToInt32(x)
    match arg with
        | [] -> [x]
        | x1 :: y :: tt when x1 = x && isdigit y.[0] -> x1 :: i2s(atoi32 y + 1) :: tt
        | x1 :: zz when x1 = x -> x1 :: "2" :: zz
        | zz -> x::zz


let set_hof_flag_ = function // DELETE ME a nop
    | Bsv_ty_nom(sty, F_fun(rty, mats, arg_tys), ty_args) ->
        let sty' = sty // with xhof=muddy "Some true hof" }
        let mats' = mats; 
        Bsv_ty_nom(sty', F_fun(rty, mats', arg_tys), ty_args)

// We only need to tie knots on recursive types.  Recursive functions are not
// copied out bodily: they are evaluated until the recursion bottoms out.        
let ee_sty m = function
    | EE_typef(_, sty) -> sty
    | EE_e(sty, vale) -> sty



// Functions cannot be used as dictionary keys, so we use a string key as an indirect key.

let g_evty_dictionary = new Dictionary<evty_fun_indirect_t, genenv_map_t option>() // TODO evty_not correct name now!

//
let evty_create_indirection evty =
    let key = funique "evty_ptr"
    g_evty_dictionary.Add (key, Some evty)
    key

let evty_create_indirection_flt key =
    let key = funique "evty_flt_ptr" + key
    g_evty_dictionary.Add (key, None)
    key

let evty_lookup key =
    let (found, ov) = g_evty_dictionary.TryGetValue(key)
    if not found then sf ("evty key not defined " + key)
    if nonep ov then sf ("used faulting evty indirection " + key)
    valOf ov


let hoftokenToStr x = x // already a simple string (at the moment)

let spogToStr = function     // HOF_VAL_PATH: A note of higher function types being passed through the value graph within a function.
    | SPOG((whom, forwhom), passed_function_idl, body, callsite, fid) -> sprintf "SPOG([%s,%s], %s, %s, %s)" whom (htos forwhom) (htos passed_function_idl) callsite fid

let boolstyle_tyclasses  =
  Prov_s [
    Prov_si "Eq";
    Prov_si "Ord";
  ]


let intstyle_tyclasses  =
  Prov_s [
//    Prov_si "Literal";
    Prov_si "Eq";
    Prov_si "Arith";
    Prov_si "Ord";
    Prov_si "Bounded";
    Prov_si "Bitwise";
    Prov_si "BitReduction";
    Prov_si "BitExtend" 
  ]


let g_function_token = "$evald-lambda"

let string_tyclasses =
 Prov_s
  [
       Prov_si "Literal";
       Prov_si "Eq";
       Prov_si "Ord";
  ]

let g_bool_ty   = gen_ty_enum({g_blank_nom_typ_ats with nomid=["Bool"; "Prelude"]; cls=boolstyle_tyclasses }, 2, 1, [ ("False", 0); ("True", 1) ])
let g_bool_sty = g_bool_ty

let mk_bitty w = Bsv_ty_primbits{signed=None;        width=w; branding="_Bit"; }

let g_char_ty = Bsv_ty_primbits{signed=None;         width=8; branding="_Char"; }

// typedef Int#(8) Char;
// typedef Vector#(?, Char) String;
let g_string_ty = Bsv_ty_nom({g_blank_nom_typ_ats with nomid=["String"]; cls=string_tyclasses; }, F_vector, [Tid(VP_value(true, ["stringlength_ub"]), "stringlength_ub"); Tty(g_char_ty, "string_ct")])

let mk_intansi v = Bsv_ty_intexpi(B_hexp(Bsv_ty_integer, v))

let mk_string_ty len = Bsv_ty_nom({g_blank_nom_typ_ats with nomid=["String"]; cls=string_tyclasses; }, F_vector, [Tty(mk_intansi(xi_num len), funique "stringlength"); Tty(g_char_ty, funique "string_ct")])
    
let mk_int_ty w  = Bsv_ty_primbits{signed=Some Signed;   width=w; branding="_Integer"; }
let mk_uint_ty w = Bsv_ty_primbits{signed=Some Unsigned; width=w; branding="_Integer"; }

// Note that Nat is Bit#(32) and int is Int#(32)

// The lower-case type 'int' is a synonym for "Int#(32)" and is the not same as the elaboration type Integer.
let g_canned_int_ty = mk_int_ty 32

let g_canned_bit_ty = mk_bitty 1


let g_net emits_o (idl, width, ats__) =
    vprintln 3 (sprintf "Created net '%s' w=%i (emits_live=%A)" (htosp idl) width (if not_nonep emits_o then (valOf emits_o).uid else "-dead-"))
    let rreg = vectornet_w(htosp idl, width)
    if not_nonep emits_o then mutadd (valOf emits_o).nets rreg
    rreg

//
let g_input emits_o (idl, width, ats) =
    let rreg = ionet_w(htosp idl, width, INPUT, Unsigned, Nap(g_control_net_marked, "true")::ats)
    vprintln 3 (sprintf "Created input '%s' w=%i" (htosp idl) width)    
    if not_nonep emits_o then mutadd (valOf emits_o).nets rreg
    rreg

let g_output emits_o (idl, width, ats) =
    let rreg = ionet_w(htosp idl, width, OUTPUT, Unsigned, Nap(g_control_net_marked, "true")::ats)
    vprintln 3 (sprintf "Created output '%s' w=%i" (htosp idl) width)        
    if not_nonep emits_o then mutadd (valOf emits_o).nets rreg
    rreg


// TODO avoid duplication here ...
// These are attributes for the current compilation module as a whole?  No, just for local scope.
let refine_attribute_settings ee (ass:attribute_settings_t) pragmats =
    let refine (ass:attribute_settings_t) (a, idl) =
        match a with
        | Pragma_split _        -> { ass with split_default=true }
        | Pragma_nosplit _      -> { ass with split_default=false }
        | Pragma_clocked_by ss  -> { ass with dirinfo= { ass.dirinfo with dclk=ss }}
        | Pragma_reset_by ss          -> { ass with dirinfo= { ass.dirinfo with dreset=ss }}
        | Pragma_fire_rate ss         -> { ass with fire_rate= Some ss }
        | Pragma_rule_repeat_count nn -> { ass with superscalar_rate=nn }
        | other ->
            dev_println (sprintf "refine_attribute_settings: context %A ignore other attribute %A" ee.module_path other)
            ass // Assume not part of the attribute settings.
    let ans = List.fold refine ass pragmats 
    //dev_println(sprintf "evc: refined to %A" pragmats)
    ans

// These are attributes for a director which may be different on a per module instance module within the current module. ??? TODO check.
let refine_dir_info ww vd msg clkinfo raw_evc =
    let refine_evc dirinfo = function
        | ("clocked_by", KB_id(nd,_), lp) ->
            if vd >=4 then vprintln 4 (sprintf "evc attribute set %s to %s" "clk" nd)
            //dev_println ("evc set clock : " + sprintf "evc attribute set %s to %s" "clk" nd)
            { dirinfo with dclk=nd }

        | ("reset_by", KB_id(nd,_), lp) ->
            if vd >=4 then vprintln 4 (sprintf "evc attribute set %s to %s" "reset" nd)
            //dev_println ("evc set reset : " + sprintf "evc attribute set %s to %s" "reset" nd)
            { dirinfo with dreset=nd }

        | ("gated_by", KB_id(nd,_), lp) ->
            if vd >=4 then vprintln 4 (sprintf "evc attribute set %s to %s" "cen" nd)
            { dirinfo with dcen=nd }

        | (other_directive, _, lp) ->
            hpr_yikes(sprintf "Ignored evc event control directive %s at %s" other_directive (lpToStr lp))
            dirinfo

    let ans = List.fold refine_evc clkinfo raw_evc 
    //dev_println (msg + sprintf ": refine_dir_info finally %A" ans)
    ans



// The order ratifier validates that no pairwise ordering constraints in an execution_order or descending_urgency list contradict each other by forming a strongly-connected component.

let order_ratifier ww keyk plst =
    let vd = 4 // for now ... want 3
    if nullp plst then ()
    else
    let ll = length plst
    let edges = new ListStore<string list, string list>("edges")
    let edges_lst =
        let scannate cc = function
            | (Pragma_shed_control(k, sublst), codepoint) when k=keyk ->
                let rec to_xition cc = function
                    | []
                    | [_] -> cc
                    | a::b::tt ->
                        if vd>=4 then vprintln 4 (sprintf "%s edge %s -> %s" keyk (hptos a) (hptos b))
                        edges.add a b
                        to_xition ((a,b)::cc) (b::tt)
                to_xition cc sublst
            | other -> sf (sprintf "Bad or unexpected groom prep keyk=%s for order_ratifier. other=%A" keyk other)
        List.fold scannate [] plst
    let nodes = List.fold (fun cc (a,b) -> singly_add a (singly_add b cc)) [] edges_lst
    let pof arg = sprintf "Loop found in %s %A" keyk arg // Could be much nicer using hptos
    let scc = moscow.tarjan1 ("bsvc: check for loops in " + keyk) (None) 3 (nodes, edges)
    vprintln 2 (sprintf "Found %i loops in %s pragmas." (length scc) keyk)
    vprintln 2 (sprintf "Ratified %s. %i entries. OK" keyk ll)
    ()


let degroom_schedulling_atts ww ats =
    let ww = WF 2 "degroom_schedulling_atts" ww (sprintf "Start on %i attributes" (length ats))
    let select_at keyk ats =
        let pre_pred = function
            | (Pragma_shed_control(k, _), codepoint) -> k=keyk
            | _                                     -> false
        let (selected, remnants) = groom2 pre_pred ats
        vprintln 2 (sprintf "   Attributes/pragms of form '%s' number %i." keyk (length selected))
        (selected, remnants)
        
    let (preempts, ats)           = select_at "preempts" ats
    let (descending_urgency, ats) = select_at "descending_urgency" ats
    let (conflict_free, ats)      = select_at "conflict_free" ats
    let (mutually_exclusive, ats) = select_at "mutually_exclusive" ats
    let (execution_order, ats)    = select_at "execution_order" ats

    // The preempts attribute is equivalent to forcing a conflict and adding descending_urgency.
    // There is more complex syntax here when more that two rules are named
    // (* preempts = "r1, r2" *)
    // (* preempts = "(r1, r2), r3" *)
    let preempt_expand newk = function
        | (Pragma_shed_control(k, [a1; a2]), codepoint) when k = "preempts" -> (Pragma_shed_control(newk, [a1; a2]), codepoint)
            
        | other ->  muddy (sprintf "parsing of preempt groups with parenthesis is missing, as in (* preempts = \"(r1, r2), r3\" *).  other=%A" other)
    let descending_urgency = map (preempt_expand "descending_urgency") preempts @ descending_urgency
    let mutually_exclusive = map (preempt_expand "mutually_exclusive") preempts @ mutually_exclusive

    if not_nullp ats then
        let pq arg = sprintf "%A" arg
        hpr_yikes(sprintf "Schedulling attributes unrecognised and ignored: " + sfold pq ats)

    order_ratifier ww ("descending_urgency") descending_urgency
    order_ratifier ww ("execution_order") execution_order

    let ww = WF 2 "degroom_schedulling_atts" ww ("Finished")
    ((descending_urgency, conflict_free, mutually_exclusive), execution_order)

let g_hardstate_notes = new OptionStore<string list, int>("hardstate_set") // Just a mutable set.

let register_hardstate idl =
    g_hardstate_notes.add idl 1
    vprintln 3 (sprintf "register_hardstate for idl=" + hptos idl)
    idl


let is_recursive_struct = function
    | Bsv_struct((width, arity, recursivef), fields) -> recursivef
    //_ -> sf "L2895"

let is_ifc_ty site = function
    |  Bsv_ty_nom(_, F_ifc_1 _, _)
    |  Bsv_ty_nom(_, F_subif _, _)
    |  Bsv_ty_nom(_, F_ifc_temp, _) -> true    
    | other                         -> false


// eof
