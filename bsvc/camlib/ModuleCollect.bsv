//
// Toy BSV Compiler Library File.
// Interface signatures (C) Bluespec Inc.
// Module implementations (C) 2012 DJ Greaves, University of Cambridge, Computer Laboratory.
//

package ModuleCollect;

interface ModuleCollect#(type modulepoly, type pinchate_);
  // The body is hardwired inside the toy bluespec compiler.
endinterface

endpackage
