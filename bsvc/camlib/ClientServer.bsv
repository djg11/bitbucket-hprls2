//
// Toy BSV Compiler Library File.
// Interface signatures (C) Bluespec Inc.
// Module implementations (C) 2012 DJ Greaves, University of Cambridge, Computer Laboratory.
// $Id: ClientServer.bsv,v 1.4 2012-12-07 09:01:38 djg11 Exp $

package ClientServer;

import GetPut :: *;

interface Client#(type req_type, type resp_type); // type variables are not capitalised
  interface Get#(req_type) request;
  interface Put#(resp_type) response;
endinterface: Client


interface Server#(type req_type, type resp_type);
  interface Put#(req_type) request;
  interface Get#(resp_type) response;
endinterface: Server


endpackage


