//
// Toy BSV Compiler Library File.
// Interface signatures (C) Bluespec Inc.
// Module implementations (C) DJ Greaves, University of Cambridge, Computer Laboratory.
// $Id: CompleteionBuffer.bsv,v 1.1 2013-02-07 10:42:08 djg11 Exp $
//
package CompletionBuffer;

import GetPut :: *;

interface CompletionBuffer #(numeric type nix, type element_type);

   interface Get#(CBToken#(nix)) reserve;

   interface Put#(Tuple2 #(CBToken#(nix), element_type)) complete;

   interface Get#(element_type) drain;

endinterface: CompletionBuffer

typedef Bit#(TLog#(nox)) CBToken#(numeric type nox);

//typedef union tagged { ... } CBToken #(numeric type n) ...;

//The mkCompletionBuffer module is used to instantiate a completion
//buffer. It takes no size arguments, as all that information is
//already contained in the type of the interface it produces.
module mkCompletionBuffer(CompletionBuffer#(n, element_type)) provisos (Bits#(element_type, element_size));


endmodule



endpackage
