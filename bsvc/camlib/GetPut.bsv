//
// Toy BSV Compiler Library File.
// Interface signatures (C) Bluespec Inc.
// Module implementations (C) DJ Greaves, University of Cambridge, Computer Laboratory.
// $Id: GetPut.bsv,v 1.10 2013-01-23 20:48:52 djg11 Exp $
//
package GetPut;

import FIFO::*;
import FIFOF::*;

// The type keyword not used as standard?   TODO - implied for typeclass?
                                
interface Put#(type putarg_t);            // define interface type Put
  method Action put(putarg_t value);
endinterface: Put

interface Get#(type getarg_t);            // define interface type Get
  method ActionValue#(getarg_t) get();
endinterface: Get

// There will be a number of overloaded definitions of the following functions:
typeclass ToGet#(type pg_a_ty, type pg_b_ty);
  function Get#(pg_b_ty) toGet(pg_a_ty gax);
endtypeclass


// ToPut defines the class to which the function toPut can be applied to create an associated Put interface.
typeclass ToPut#(type pg_a_ty, type pg_b_ty);
  function Put#(pg_b_ty) toPut(pg_a_ty pax);
endtypeclass

//
// Here is the main typedef : the GetPut entity is just a pair of simple interfaces.
//
typedef Tuple2#(Get#(element_type), Put#(element_type)) GetPut#(type element_type);

// Function to transform a FIFO interface into a Put interface
function Put#(element_type_t) fifoToPut(FIFO#(element_type_t) derfifo);
   return (interface Put;
              method Action put(element_type_t datumus);
		      //message("Put method debug ", datumus);
                      derfifo.enq(datumus);
              endmethod: put
           endinterface);
endfunction


// Function to transform a FIFO interface into a Get interface
function Get#(ftg_cty_t) fifoToGet (FIFO#(ftg_cty_t) rot_fifo);     
   return (interface Get;
              method ActionValue#(ftg_cty_t) get();
                    //$display("Getting fifoToGet amanxa d=%h", rot_fifo.first());
                    rot_fifo.deq();
                    return rot_fifo.first();
              endmethod: get
           endinterface);
endfunction: fifoToGet

instance Get#(FIFO#(fitg_cty_t));
   function Get#(fitg_cty_t) toGet (FIFO#(fitg_cty_t) lefifo); 
     return fifoToGet(lefifo);
   endfunction
endinstance

instance Put#(FIFO#(fitp_cty_t));
   function Put#(fitp_cty_t) toPut (FIFO#(fitp_cty_t) refifo); 
     return fifoToPut(refifo);
   endfunction
endinstance

// Function to transform a FIFOF interface into a Put interface
// This needs to check the guards since they are not implicit for FIFOF.
function Put#(element_type_t) fifofToPut(FIFOF#(element_type_t) derfifo);
   return (interface Put;
              method Action put(element_type_t datumus) if (derfifo.notFull());
                     derfifo.enq(datumus);
              endmethod: put
           endinterface);
endfunction: fifofToPut

// Function to transform a FIFOF interface into a Get interface
// This again needs to check the guards since they are not implicit for FIFOF.
function Get#(ftg_cty_t) fifofToGet (FIFOF#(ftg_cty_t) dat_fifo);     
   return (interface Get;
              method ActionValue#(ftg_cty_t) get() if (dat_fifo.notEmpty());
                    dat_fifo.deq();
                    return dat_fifo.first();
              endmethod: get
           endinterface);
endfunction: fifofToGet


instance Get#(FIFOF#(fitg_cty_t));
   function Get#(fitg_cty_t) toGet (FIFOF#(fitg_cty_t) lefifof); 
     return fifofToGet(lefifof);
   endfunction
endinstance

instance Put#(FIFOF#(fitp_cty_t));
   function Put#(fitp_cty_t) toPut (FIFOF#(fitp_cty_t) refifof); 
     return fifofToPut(refifof);
   endfunction
endinstance

endpackage
