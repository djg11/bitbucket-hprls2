//
// Toy BSV Compiler Library File.
// Interface signatures (C) Bluespec Inc.
// Module implementations (C) 2012 DJ Greaves, University of Cambridge, Computer Laboratory.
// $Id: FIFO.bsv,v 1.6 2013-01-20 10:10:13 djg11 Exp $

package FIFO;

interface FIFO #(type facontent_type); // FIFO without flag methods and with implicit conditions.
  (* synchronized, non_idempotent *) method Action enq(facontent_type x1);
  (* synchronized, non_idempotent *) method Action deq();
  (* synchronized                 *) method facontent_type first();
  (* synchronized                 *) method Action clear();
endinterface
                                                                                                            
module mkFIFO (FIFO#(fcontent_type1)) provisos (Bits#(fcontent_type1));
// FIFO or FIFOF of depth 1

  Reg#(fcontent_type1) datumf <- mkRegU();
  Reg#(Bool) valid <- mkReg(0);


   method Action enq(fcontent_type1 x1) if (!valid);
      datumf <= x1;
      //$display("Fifo wrote %h", x1);
      valid <= True;
   endmethod

   method Action deq() if (valid);
     valid <= False;
   endmethod

   method fcontent_type1 first() if (valid);
     return datumf;
   endmethod

   method Action clear();
     datumf <= False;
   endmethod


endmodule

//module mkFIFO1(FIFO#(type content_type)) provisos (Bits#(content_type, type width_any));

//endmodule


//
//
module mkSizedFIFO#(Integer depth)(FIFO#(content_type1)) provisos (Bits#(content_type1));

  Reg#(content_type1) datumf <- mkRegU;
  Reg#(Bool) valid <- mkReg(0);

   method Action enq(content_type1 x1) if (!valid);
      datumf <= x1;
      //$display("Fifo wrote %h", x1);
      valid <= True;
   endmethod

   method Action deq() if (valid);
     valid <= False;
   endmethod

   method content_type1 first() if (valid);
     return datumf;
   endmethod

   method Action clear();
     datumf <= False;
   endmethod

  
  if (depth != 1)  warningM ("other sizes not implemented yet");

endmodule

endpackage


