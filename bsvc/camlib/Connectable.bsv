//
// Toy BSV Compiler Library File.
// Interface signatures (C) Bluespec Inc.
// Module implementations (C) DJ Greaves, University of Cambridge, Computer Laboratory.
// $Id: Connectable.bsv,v 1.8 2013-03-16 09:01:28 djg11 Exp $
//
package Connectable;

import GetPut::*;
import ClientServer::*;

// Connect a client to a server (flow of requests and flow of responses)
//
//       mkConnection (stimulus_gen, dut);
// is short for
//       Empty e <- mkConnection (stimulus_gen, dut);

typeclass Connectable#(type con_a_t, type con_b_t);
  module mkConnection#(con_a_t x1, con_b_t x2) (Empty);
endtypeclass



module mkConnection_GetPut(Get#(gp_data_pumped_t) gg, Put#(gp_data_pumped_t) pp, Empty the_empty_ifc);

  rule get_put_data_pump_connection;
      pp.put(gg.get());
  endrule

endmodule

instance Connectable#(Get#(g_element_type), Put#(p_element_type));

  module mkConnection(con_a_t qqa1, con_b_t qqa2, Empty the_empty_ifc); //  Do these identifiers con_a_t and con_b_t have to be the same as used in the typeclass declaration?
   Empty eifc2 <- mkConnection_GetPut(qqa1, qqa2);
  endmodule

endinstance



module mkConnection_ClientServer#(Client#(dati, dato) cl, Server#(dati, dato) sv) (Empty);

  rule client_server_data_pump_forward_connection;
      cl.response.put(sv.response.get());
  endrule

  rule client_server_data_pump_reverse_connection;
      sv.request.put(cl.request.get());
  endrule

endmodule

instance Connectable#(Client#(dati, dato), Server#(dati, dato));

  module mkConnection#(Client#(dati, dato) cl2, Server#(dati, dato) sv2) (Empty);
    mkConnection_ClientServer(cl2, sv2);
  endmodule

endinstance

//instance Connectable#(Tuple2#(con2_a_t, con2_c_t), Tuple2#(con2_b_t, con2_d_t))
//   provisos (Connectable#(con2_a_t, con2_b_t), Connectable#(con2_c_t, con2_d_t));
//endinstance
// TODO Ditto other size tuples


// instance Connectable#(ListN#(n, a), ListN#(n, b)) provisos (Connectable#(a, b));

//Action, ActionValue An ActionValue method (or function) which produces a value can be
//connected to an Action method (or function) which takes that value as an argument.

// instance Connectable#(ActionValue#(ic_a_t), function Action f(ic_a_t x));
//endinstance

// instance Connectable#(function Action f(ia_aa_t x), ActionValue#(ic_aa_t));
//endinstance

// A Value method (or value) can be connected to an Action method (or function) which takes that value as an argument.

//instance Connectable#(a, function Action f(a x));
//endinstance

//instance Connectable#(function Action f(a x), a);
//endinstance


endpackage
