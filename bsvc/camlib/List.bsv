//
// Toy BSV Compiler Library File.
// Interface signatures (C) Bluespec Inc.
// Module implementations (C) 2012 DJ Greaves, University of Cambridge, Computer Laboratory.
// $Id: List.bsv,v 1.19 2013-01-17 14:58:05 djg11 Exp $

package List;


typedef union tagged 
{
  void Nil;
  struct 
  { list_contents_t car;
    List #(list_contents_t) cdr;
  } Cons;
} List #(type list_contents_t);


function List#(item_typeL) cons (item_typeL item, List#(item_typeL) tailer);
   List#(item_typeL) cons_rr = Cons { car:item, cdr:tailer };
   return cons_rr;
endfunction


function item_typeH head (List#(item_typeH) listy);
     case (listy) matches
       tagged Cons { car: .head_item, cdr: .tailv } : return head_item;
//       default: error("take head of empty list");
           
     endcase
endfunction


function List#(item_typeT) tail (List#(item_typeT) longer);
     case (longer) matches
       tagged Cons { car: .head_item, cdr: .tailv } : return tailv;
  //     default: error("take tail of empty list");
     endcase
endfunction

function element_t select(List#(element_t) alist, Integer index);
  // Bluespec compiler does not allow actions in a pure function
  //message("List select ", index);
  // if (alist == tagged Nil) error ("do not edit this string: off end of list");
  case (alist) matches
  tagged Nil: return ?;
  tagged Cons { car: .hd, cdr: .tl }: begin
      if (index==0) return hd;
      else return select(tl, index-1);
  end
  endcase
endfunction



function List#(range_type) map (function range_type mapped_func(domain_type xarg_), List#(domain_type) listx);
  case (listx) matches
  tagged Nil: return tagged Nil;
  tagged Cons { car: .hd, cdr: .tl }:
     return cons(mapped_func(hd), map(mapped_func, tl));
  endcase
endfunction


function Integer length (List#(lc_type) lista);
  case (lista) matches
  tagged Nil: return 0;
  tagged Cons { car: .hd, cdr: .tl }: return 1 + length(tl);
  endcase
endfunction

//function List#(c_type) zipWith (function c_type func(a_type x, b_type y), List#(a_type) listx,List#(b_type) listy );

function List#(Tuple2 #(ta_type, tb_type)) zip (List#(ta_type) lista, List#(tb_type) listb);
  case (tuple2(lista, listb)) matches
     { tagged Nil, tagged Nil }: return tagged Nil;
     default: return cons(tuple2(head(lista), head(listb)), zip(tail(lista), tail(listb)));
  endcase
endfunction


function Tuple2#(List#(tauz_type), List#(tbuz_type)) unzip (List#(Tuple2 #(tauz_type, tbuz_type)) listab);
  return tuple2(map(tpl_1, listab), map(tpl_2, listab));
endfunction


endpackage // List.bsv



