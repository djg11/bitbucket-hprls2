//
// Toy BSV Compiler Library File.
// Interface signatures (C) Bluespec Inc.
// Module implementations (C) DJ Greaves, University of Cambridge, Computer Laboratory.
// $Id: BRAM.bsv,v 1.15 2013-02-01 09:23:42 djg11 Exp $
//

// Warning : temporary, poorman's implementation that does not map to BRAM actually.
// But now that the wire types are supported this can be finished off with its output FIFOs.

package BRAM;

import GetPut       :: *;
import ClientServer :: *;
import DefaultValue :: *;

typedef union tagged {
  void None;
  String Hex;
  String Binary;
} LoadFormat deriving (Eq, Bits);


typedef struct {
   Integer memorySize;
   Integer latency;  // 1 or 2 can extend to 3
   LoadFormat loadFormat; // None, Hex or Binary
   Integer outFIFODepth;
   Bool allowWriteResponseBypass;
} BRAM_Configure;


typedef struct {
   Bool    write;
   Bool    responseOnWrite;
   addr_t  address; 
   data_t  datain;
   }  BRAMRequest#(type addr_t, type data_t) deriving (Bits, Eq);

typedef Server#(BRAMRequest#(addr_t55, data_t55), data_t55) BRAMServer#(type addr_t55, type data_t55);

typedef Client#(BRAMRequest#(addr_t22, data_t22), data_t22) BRAMClient#(type addr_t22, type data_t22);


interface BRAM1Port#(type addr_t1, type data_t1);
   interface BRAMServer#(addr_t1, data_t1) portA;
   method Action portAClear();
endinterface: BRAM1Port


//
// Note: this has latency zero and maps to combinational RAM that is mapped to distributed/LUT RAM in FPGA.
//
interface SSRAM_Cambridge_SP0D#(type address_tci, type content_tci);
(* always_ready *)  method ActionValue#(content_tci) native_read(address_tci adr); // always_enabled infact.
(* always_ready *)  method Action native_write(address_tci adr, content_tci data);
endinterface

//
// Note: this has latency one and maps to BRAM.
//
interface SSRAM_Cambridge_SP1D#(type address_tci, type content_tci);
(* always_ready *)  method ActionValue#(content_tci) native_read(address_tci adr); // always_enabled infact.
(* always_ready *)  method Action native_write(address_tci adr, content_tci data);
endinterface


//
module $BUILTIN "cambridge"  mkSSRAM_Cambridge_SP0D(SSRAM_Cambridge_SP0D#(address_tc, content_tc));
 //  provisos (Bits#(address_t4, content_t4));
 // The body of this is built in to the toy compiler.
 //
 
endmodule

module $BUILTIN "cambridge"  mkSSRAM_Cambridge_SP1D(SSRAM_Cambridge_SP1D#(address_tc, content_tc));
 //  provisos (Bits#(address_t4, content_t4));
 
 // The body of this is built in to the toy compiler.
 //
endmodule


 // The BSV front end first makes a simple RTL array, but note that Orangepath has a 'restructure' module that will remove the RTL array and instead render vendor RAMs, but Put/Get paradigm used in BSV for BRAM means ... see paper in prep ...


//
// We here ignore read FIFO depth spec and always have unity.  
//
module mkBRAM1Server_use_in_future#(BRAM_Configure cfg) (BRAM1Port#(address_t4, content_t4)) provisos (Bits#(address_t4, address_sz), Bits#(content_t4, content_sz));

  Reg#(address_t4) lastloc <- mkRegU;
  Reg#(content_t4) lastread <- mkRegU;
  Reg#(Bool) valid <- mkReg(False);

  SSRAM_Cambridge_SP1D#(address_t4, content_t4) synch_ram <- mkSSRAM_Cambridge_SP1D(); // This makes a real BRAM

  method Action portAClear();
       $display("Cleared");
  endmethod

  interface BRAM::BRAMServer portA;



  // This current coding does not work at all well - it has a spurious read pipeline stage that wrongly returns the previous answer and it drops data if not called in time!
 // Missing - we need a rule to read from the RAM the cycle after the read is requested and to load a bypass FIFO with the result.
 
interface PutGet::Get response;
       method ActionValue#(content_t4) get() if (valid);
         valid <= False;
	 let ans = lastread;
         //$display("BRAM GET %h %h", lastloc, ans);
         return ans;
       endmethod 
    endinterface

  interface PutGet::Put request;
       method Action put(BRAMRequest#(address_t4, content_t4) pkt);// if(!valid);
         lastloc <= pkt.address;
         if (pkt.write) begin	 
                  synch_ram.native_write(pkt.address, pkt.datain);
	          // $display("BRAM PUT  write=%i %h %h", pkt.write, pkt.address, pkt.datain);
                  end
       	 else begin
              let v <- synch_ram.native_read(pkt.address);  // The new forwarding extensions use this API natively?
              lastread  <= v;
              valid <= True;
              end
       endmethod
    endinterface
  endinterface

endmodule


//
// Best not to use this one.  But it is a bit simpler if you want to debug things and it does work whereas the one above needs fixing with dwires or something.
//
module mkBRAM1Server#(BRAM_Configure cfg) (BRAM1Port#(address_t4, content_t4)) provisos (Bits#(address_t4, address_sz), Bits#(content_t4, content_sz));

   Reg#(address_t4) lastloc <- mkRegU;
   Reg#(content_t4) lastread <- mkRegU;
   Reg#(Bool) valid <- mkReg(False);

   SSRAM_Cambridge_SP0D#(address_t4, content_t4) theram <- mkSSRAM_Cambridge_SP0D(); // Use combinational RAM with our own register on the output.

  method Action portAClear();
       $display("Cleared");
  endmethod

  interface BRAM::BRAMServer portA;

// This is just a get-started poorman's implementation:
//  1. We have a FIFO depth of one.
//  2. We are making access to distributed RAM not to a BRAM.
// Overall, this gives us the worst of all possible design points!
   interface PutGet::Get response;
       method ActionValue#(content_t4) get() if (valid);
         valid <= False;
	 let ans = lastread;
         //$display("BRAM GET %h %h", lastloc, ans);
         return ans;
       endmethod 
    endinterface

  interface PutGet::Put request;
       method Action put(BRAMRequest#(address_t4, content_t4) pkt);// if(!valid);
         lastloc <= pkt.address;
         if (pkt.write) begin	 
                  theram.native_write(pkt.address, pkt.datain);
	          // $display("COMB PUT  write=%i %h %h", pkt.write, pkt.address, pkt.datain);
                  end
       	 else begin
              let v <- theram.native_read(pkt.address);  
              lastread  <= v;
              valid <= True;
              end
       endmethod
    endinterface
  endinterface

endmodule



instance DefaultValue #(BRAM_Configure);
   defaultValue = BRAM_Configure 
     { memorySize: 1024, // was 0 ??
       latency: 1, // No output reg
       outFIFODepth: 3,
       loadFormat: None,
      allowWriteResponseBypass : False };
endinstance


/*
   commented out ... why?
typedef struct {
   Bit#(n) writeen;
   Bool    responseOnWrite;
   addr_t  address; 
   data_t  datain;
   }  BRAMRequestBE#(type addr_t, type data_t, numeric type n) deriving (Bits, Eq);

typedef Server#(BRAMRequestBE#(addr, data, n), data) BRAMServerBE#(type addr, type data, numeric type n);

typedef Client#(BRAMRequestBE#(addr, data, n), data) BRAMClientBE#(type addr, type data, numeric type n);
    
    
interface BRAM1PortBE#(type addr, type data, numeric type n);
   interface BRAMServerBE#(addr, data, n) portA;
   method Action portAClear;
endinterface: BRAM1PortBE

    
interface BRAM2Port#(type addr, type data);
  interface BRAMServer#(addr, data) portA;
   interface BRAMServer#(addr, data) portB;
   method Action portAClear;
   method Action portBClear;
endinterface: BRAM2Port

interface BRAM2PortBE#(type addr, type data, numeric type n);
  interface BRAMServerBE#(addr, data, n) portA;
  interface BRAMServerBE#(addr, data, n) portB;
  method Action portAClear;
  method Action portBClear;
endinterface: BRAM2PortBE
   
   
*/

endpackage
