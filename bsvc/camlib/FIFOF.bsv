//
// Toy BSV Compiler Library File.
// Interface signatures (C) Bluespec Inc.
// Module implementations (C) 2012 DJ Greaves, University of Cambridge, Computer Laboratory.
// $Id: FIFOF.bsv,v 1.5 2013-01-17 14:58:05 djg11 Exp $

package FIFOF;


interface FIFOF #(type ffdata_type);  // FIFO with additional methods for status checking.  This implementation has no implict guards.  Also we should implement GFIFOF for user-defined implict guarding.
  method Action enq(ffdata_type x1);
  method Action deq();
  method ffdata_type first();
  method Bool notFull();
  method Bool notEmpty();
  method Action clear();
endinterface: FIFOF



module mkFIFOF (FIFOF#(cdata_type)) provisos (Bits#(cdata_type, cdata_size));

  // FIFO or FIFOF of depth 1
  Reg#(cdata_type) datumq <- mkRegU;
  Reg#(Bool) valid <- mkReg(False);

   method Action enq(cdata_type x1); // Can be written even if full.
      datumq <= x1;
      valid <= True;
   endmethod

   method Action deq(); // Same as the clear method for size 1.
     valid <= False;
   endmethod

   method cdata_type first(); // can be read even if not valid;
     return datumq;
   endmethod

   method Action clear();
     valid <= False;
   endmethod

  method Bool notFull();
    return !valid;
  endmethod

  method Bool notEmpty();
    return valid;
  endmethod

endmodule

//module mkFIFOF1(FIFOF#(data_type)) provisos (Bits#(data_type));
//endmodule


//module mkSizedFIFOF(Integer n)(FIFOF#(data_type)) provisos (Bits#(data_type));
//endmodule

endpackage


