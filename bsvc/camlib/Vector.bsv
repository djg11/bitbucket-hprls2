//
// Toy BSV Compiler Library File.
// Interface signatures (C) Bluespec Inc.
// Module implementations (C) DJ Greaves, University of Cambridge, Computer Laboratory.
// $Id: Vector.bsv,v 1.5 2012-12-21 22:38:32 djg11 Exp $
//
package Vector;


typedef struct { $BUILTIN } Vector#(numeric type sizev, type ctype);

/*
function Vector#(vsize, Integer) genVector(); // Create a vector filled with zeros
   Vector#(vsize, Integer) gend;
     for (Integer j = 0; j < valueof(vsize); j = j + 1) gend[j] = 0;
   return gend;
endfunction

function m#(Vector#(vsize, element_type)) replicateM( m#(element_type) genfun) provisos (Monad#(m));
   Vector#(vsize, element_type) genm;
   for (Integer j = 0; j < valueof(vsize); j = j + 1) genm[j] = genfun();
   return genm;
endfunction


function Vector#(vsize, element_type) genWith(function element_type genfunc(Integer x1));
   Vector#(vsize, element_type) genw;
   for (Integer j = 0; j < valueof(vsize); j = j + 1) genw[j] = genfunc(j);
   return genw;
endfunction


function Vector#(sizev0, ctype) newVector(numeric type sizev0, type ctype);
endfunction


function Vector#(sizev1, ctype) replicate(numeric type sizev1, ctype c);
endfunction


function Vector#(sizev2, ctype) genWith(numeric type sizev2, function ctype func(Integer x1));
endfunction

*/

endpackage