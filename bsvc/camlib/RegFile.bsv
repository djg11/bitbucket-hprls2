//
// Toy CBG-BSV Compiler Library File.
// Interface signatures (C) Bluespec Inc.
// Module implementations (C) 2012 DJ Greaves, University of Cambridge, Computer Laboratory.
// $Id: RegFile.bsv,v 1.1 2013-04-22 17:29:00 djg11 Exp $

package RegFile;

interface RegFile#(type iindex_t, type irdata_t);
  method Action upd(iindex_t addr11, irdata_t d);
  method irdata_t sub(iindex_t addr11);
endinterface: RegFile


interface RegFileRO#(type index_t, type data_t);
  method data_t sub(index_t addr11);
endinterface: RegFile


module mkRegFile#(index_t lo_index, index_t hi_index)( RegFile#(index_t, data_t) ) provisos (Bits#(index_t), Bits#(data_t));

  typedef TExp#(SizeOf#(index_t)) RegFileSize;

  Vector#(TAdd#(TSub#(hi_index, lo_index),1), Reg#(data_t)) rf_vector;

  // This definition gave a typechecking error which is now fixed?
  //  for (Integer j = 0; j < RegFileSize; j = j + 1) rf_vector[j] <- mkRegU;

  for (Integer j = lo_index; j <= hi_index; j = j + 1) rf_vector[j-lo_index] <- mkRegU;

  method Action upd(index_t w_addr22, data_t w_data);
    rf_vector[w_addr22-lo_index] <= w_data;
  endmethod

  method data_t sub(index_t r_addr33);
    return rf_vector[r_addr33-lo_index];
  endmethod

endmodule


module mkRegFileFull#(RegFile#(index_t, data_t) rff) () provisos (Bits#(index_t), Bits#(data_t), Bounded#(index_t));
  error("Not implemented yet");
endmodule


// conflict free
module mkRegFileWCF#(index_t lo_index, index_t hi_index) (RegFile#(index_t, data_t)) provisos (Bits#(index_t), Bits#(data_t));


  error("Not implemented yet");
endmodule


module mkRegFileLoad#(String file, index_t lo_index, index_t hi_index) (RegFile#(index_t, data33_t)) provisos (Bits#(index_t), Bits#(data33_t));

  typedef TAdd#(TSub#(hi_index, lo_index), 1) NoEntries;

  Vector#(NoEntries, Reg#(data33_t)) rf_vector;

  for (Integer j = 0; j <= hi_index-lo_index; j = j + 1) rf_vector[j] <- mkRegU;

  method Action upd(index_t addr44, data33_t wd44); 
    rf_vector[addr44 - lo_index] <= wd44;
  endmethod

  method data33_t sub(index_t addr55);
    return rf_vector[addr55 - lo_index];
  endmethod

  message("RegFileLoad : Load file not implemented yet");


endmodule

module mkRegFileLoad1#(String file, index_t lo_index, index_t hi_index) (RegFileRO#(index_t, data33_t)) provisos (Bits#(index_t), Bits#(data33_t));

  typedef TAdd#(TSub#(hi_index, lo_index), 1) NoEntries1;

  Vector#(NoEntries1, Reg#(data33_t)) rf_vector;

  for (Integer j = 0; j <= hi_index-lo_index; j = j + 1) rf_vector[j] <- mkRegU;

  method data33_t sub(index_t addr55);
    return rf_vector[addr55 - lo_index];
  endmethod

endmodule


module mkRegFileFullLoad#(String file) (RegFile#(index_t, data_t)) provisos (Bits#(index_t), Bits#(data_t), Bounded#(index_t) );
  error("Not implemented yet");
endmodule


module mkRegFileWCFLoad#(String file, index_t lo_index, index_t hi_index) (RegFile#(index_t, data_t)) provisos (Bits#(index_t), Bits#(data_t));
  error("Not implemented yet");
endmodule

endpackage
// eof
