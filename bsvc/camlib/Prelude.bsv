//
// Toy BSV Compiler Library File.
// Interface signatures (C) Bluespec Inc.
// Module implementations (C) 2012 DJ Greaves, University of Cambridge, Computer Laboratory.
// $Id: Prelude.bsv,v 1.30 2013-08-27 10:41:17 djg11 Exp $

package Prelude;

interface Empty;
endinterface: Empty


typedef enum {False, True} Bool deriving (Eq, Bits, Bounded);  

// typedef Vector#(?, Int#(8)) String;

typedef Bit#(32) Nat;


//
(* always_ready *)
interface  Reg#(type prct); 
   method Action _write(prct din); 
   (* always_enabled *) method prct _read(); 
   //   schedule (_read) SB (_write);
endinterface

// Here we define the Wire to to be the same as the Reg but with the bluewire annotation currently.
// If one is built-in, why not the other too? That would be cleaner.
// See mk_kernel_Wire and mk_kernel_Reg
(* bluewire *) 
typedef Reg#(welement_t) Wire#(type welement_t);  

(* bluewire *) 
interface PulseWire;
  method Action send();
  method Bool _read();
endinterface

// The type int is just a synonym for Int#(32). Unusually, it has a lower case name.  This is now hardwired in the lexer.
// typedef Int#(32) int;


// Synchronous reset register
module $BUILTIN "cambridge" mkReg#(prct initial_value) (Reg#(prct)) provisos (Bits#(prct));
  // Component is implemented by the compiler directly.
  schedule "_read _write : CF SB SA SBR";
endmodule

//
module $BUILTIN "cambridge" mkPulseWire(PulseWire);
  // Component is implemented by the compiler directly.
endmodule
//

module $BUILTIN "cambridge" mkPulseWireOR(PulseWire);
  // Component is implemented by the compiler directly.
endmodule
//


//
module mkUnsafeWire(Wire#(prct)) provisos (Bits#(prct));
  // Monad is implemented by the compiler directly.
endmodule
//


module $BUILTIN "cambridge" mkBypassWire(Wire#(element_type)) provisos (Bits#(element_type));
  // Monad is implemented by the compiler directly.
endmodule

module $BUILTIN "cambridge" mkDWire#(element_type defaultval) (Wire#(element_type)) provisos (Bits#(element_type));
  // Monad is implemented by the compiler directly.
endmodule

module $BUILTIN "cambridge" mkUnsafeDWire#(element_type defaultval)(Wire#(element_type)) provisos (Bits#(element_type));
  // Monad is implemented by the compiler directly.
endmodule

// A wire is the same as an RWire but without the maybe type wrapper.
module mkWire(Wire#(prct)) provisos (Bits#(prct));
  // Monad is implemented by the compiler directly.
endmodule



  // Asynchronous reset register
module $BUILTIN "cambridge" mkRegA#(prct initial_value) (Reg#(prct)) provisos (Bits#(prct));
  // Monad is implemented by the compiler directly.
  schedule "_read _write : CF SB SA SBR";
endmodule

  // Without reset register
module $BUILTIN "cambridge" mkRegU (Reg#(prct)) provisos (Bits#(prct));
  // Monad is implemented by the compiler directly.
endmodule

//-------------------------------------------------------------
// MayBe option types.
typedef union tagged {
  void Invalid; // Capital V in void?
  mta_t Valid;
} Maybe#(type mta_t) deriving (Bits);

function Bool isValid(Maybe#(data_a_t) arg);
	case (arg) matches
	   tagged Valid:  return True;
           tagged Invalid:  return False;
         endcase
endfunction

function data_m_t fromMaybe(data_m_t fromMaybe_default_formal, Maybe#(data_m_t) valarg) ;
	case (valarg) matches
	   tagged Valid .kval :  return kval;
           tagged Invalid:  return fromMaybe_default_formal;
         endcase
endfunction

// typedef enum {  LT, EQ, GT } Ordering deriving (Eq, Bits, Bounded);
//-------------------------------------------------------------
// Anon Tuples
// The tuple type accessors functions are overloaded for all types containing that field.



typedef struct{
  tax_t tpl_1;
  tbx_t tpl_2;
} Tuple2 #(type tax_t, type tbx_t) deriving (Bits, Eq, Bounded);

function Tuple2#(tay_t, tby_t) tuple2 (tay_t xarg1, tby_t xarg2);
  Tuple2 #(tay_t, tby_t) rr = Tuple2 { tpl_1: xarg1, tpl_2: xarg2 };
  return rr;
endfunction


typedef struct{
tta tpl_1;
ttb tpl_2;
ttc tpl_3;
} Tuple3 #(type tta, type ttb, type ttc) deriving (Bits, Eq, Bounded);

function Tuple3#(ta1_t, ta2_t, ta3_t) tuple3 (ta1_t xarg1, ta2_t xarg2, ta3_t xarg3);
  Tuple3 #(ta1_t, ta2_t, ta3_t) rr = Tuple3 { tpl_1: xarg1, tpl_2: xarg2, tpl_3: xarg3 };
  return rr;
endfunction


typedef struct{
tta tpl_1;
ttb tpl_2;
ttc tpl_3;
ttd tpl_4;
} Tuple4 #(type tta, type ttb, type ttc, type ttd) deriving (Bits, Eq, Bounded);

function Tuple4#(ta1_t, ta2_t, ta3_t, ta4_t) tuple4 (ta1_t xarg1, ta2_t xarg2, ta3_t xarg3, ta4_t xarg4);
  Tuple4 #(ta1_t, ta2_t, ta3_t, ta4_t) rr = Tuple4 { tpl_1: xarg1, tpl_2: xarg2, tpl_3: xarg3, tpl_4: xarg4 };
  return rr;
endfunction


typedef struct{
tta tpl_1;
ttb tpl_2;
ttc tpl_3;
ttd tpl_4;
tte tpl_5;
} Tuple5 #(type tta, type ttb, type ttc, type ttd, type tte) deriving (Bits, Eq, Bounded);

typedef struct{
tta tpl_1;
ttb tpl_2;
ttc tpl_3;
ttd tpl_4;
tte tpl_5;
ttf tpl_6;
} Tuple6 #(type tta, type ttb, type ttc, type ttd, type tte, type ttf)

deriving (Bits, Eq, Bounded);



typedef struct{
tta tpl_1;
ttb tpl_2;
ttc tpl_3;
ttd tpl_4;
tte tpl_5;
ttf tpl_6;
ttg tpl_7;
} Tuple7 #(type tta, type ttb, type ttc, type ttd, type tte, type ttf, type ttg) deriving (Bits, Eq, Bounded);


typedef struct{
tta tpl_1;
ttb tpl_2;
ttc tpl_3;
ttd tpl_4;
tte tpl_5;
ttf tpl_6;
ttg tpl_7;
tth tpl_8;
} Tuple8 #(type tta, type ttb, type ttc, type ttd, type tte, type ttf, type ttg, type tth ) deriving (Bits, Eq, Bounded);


typeclass TupleAccess #(type ttuple_t, type field_t); // dependencies (ttuple_t determines field_t);
  function field_t tpl_1(ttuple_t arg);
  function field_t tpl_2(ttuple_t arg);
  function field_t tpl_3(ttuple_t arg);
  function field_t tpl_4(ttuple_t arg);
endtypeclass

instance TupleAccess#(Tuple2#(tz1_t, tz2_t));
  function ta tpl_1 (Tuple2 #(tz1_t, tz2_t) tw_arg);
    return tw_arg.tpl_1;
  endfunction

 function ta tpl_2 (Tuple2 #(tz1_t, tz2_t) tw_arg);
    return tw_arg.tpl_2;
  endfunction
endinstance

// Re-insert removed please. Done.

instance TupleAccess#(Tuple3#(tz1_t, tz2_t, tz3_t));
  function ta tpl_1 (Tuple3 #(tz1_t, tz2_t, tz3_t) tw_arg);
    return tw_arg.tpl_1;
  endfunction

 function ta tpl_2 (Tuple3 #(tz1_t, tz2_t, tz3_t) tw_arg);
    return tw_arg.tpl_2;
  endfunction

function ta tpl_3 (Tuple3 #(tz1_t, tz2_t, tz3_t) tw_arg);
    return tw_arg.tpl_3;
  endfunction
endinstance


instance TupleAccess#(Tuple4#(tz1_t, tz2_t, tz3_t, tz4_t));
  function ta tpl_1 (Tuple4 #(tz1_t, tz2_t, tz3_t, tz4_t) tw_arg);
    return tw_arg.tpl_1;
  endfunction

 function ta tpl_2 (Tuple4 #(tz1_t, tz2_t, tz3_t, tz4_t) tw_arg);
    return tw_arg.tpl_2;
  endfunction

function ta tpl_3 (Tuple4 #(tz1_t, tz2_t, tz3_t, tz4_t) tw_arg);
    return tw_arg.tpl_3;
  endfunction

function ta tpl_4 (Tuple4 #(tz1_t, tz2_t, tz3_t, tz4_t) tw_arg);
    return tw_arg.tpl_4;
  endfunction
endinstance

instance TupleAccess#(Tuple5#(tz1_t, tz2_t, tz3_t, tz4_t, tz5_t));
  function ta tpl_1 (Tuple5 #(tz1_t, tz2_t, tz3_t, tz4_t, tz5_t) tw_arg);
    return tw_arg.tpl_1;
  endfunction

 function ta tpl_2 (Tuple5 #(tz1_t, tz2_t, tz3_t, tz4_t, tz5_t) tw_arg);
    return tw_arg.tpl_2;
  endfunction

function ta tpl_3 (Tuple5 #(tz1_t, tz2_t, tz3_t, tz4_t, tz5_t) tw_arg);
    return tw_arg.tpl_3;
  endfunction

function ta tpl_4 (Tuple5 #(tz1_t, tz2_t, tz3_t, tz4_t, tz5_t) tw_arg);
    return tw_arg.tpl_4;
  endfunction

function ta tpl_5 (Tuple5 #(tz1_t, tz2_t, tz3_t, tz4_t, tz5_t) tw_arg);
    return tw_arg.tpl_5;
  endfunction
endinstance



//-------------------------------------------------------------
//


/*
typeclass BitExtend #(numeric type m, numeric type n, type x);
  function x#(n) extend (x#(m) d);
  function x#(n) zeroExtend (x#(m) d);
  function x#(n) signExtend (x#(m) d);
  function x#(m) truncate (x#(n) d);
endtypeclass
*/

/*typeclass Bits#(type a, type sa) /* dependencies (a determines sa) */;
  function Bit#(sa1p) pack(a1p x);         // Converts may forms to bits
  function a2up unpack (Bit#(sa2up) x);    // Converts them back again
endtypeclass
*/

// TODO move these into the typeclass that's currently commented out.
  function Bit#(sa1up) pack(a1up x); endfunction
  function a2up2 unpack (Bit#(sa2up2) x); endfunction
  function a2up2 truncate (Bit#(sa2up2) x); endfunction
  function a2up2 extend (Bit#(sa2up2) x); endfunction
  function a2up2 zeroExtend (Bit#(sa2up2) x); endfunction
  function a2up2 signExtend (Bit#(sa2up2) x); endfunction


//-------------------------------------------------------------
// Literal and SizedLiteral typeclasses

typeclass Literal#(type a);
  function a fromInteger (Integer x);
  function Bool inLiteralRange(a target, Integer i);
endtypeclass: Literal

// SizedLiteral defines the class of types which can be created from integer literals with a specifiedsize.

typeclass SizedLiteral#(type data_t, type size_t); //  dependencies (data_t determines size_t);
  function data_t fromSizedInteger(Bit#(size_t) x);
endtypeclass


instance Literal#(Bit#(www));
  function Bit#(www) fromInteger(Integer x);
    // This function is built into the Toy Compiler.
  endfunction
endinstance

// Note asReg is built in to the Toy Compiler.
/*
function Reg#(a_typer) readReg(Reg#(a_typer) regIfc);
  return asReg(regIfc)._read();
endfunction

function Action writeReg(Reg#(a_atypew) regIfc, a_typew din);
  asReg(regIfc)._write(din);
endfunction

*/


// The following are currently built in to the toy compiler but need their prototypes to be defined.
// We might lift them out and define them fully here in future.
function Int#(m) signedMul(Int#(n) x, Int#(k) y) provisos (Add#(n,k,m)); endfunction
function UInt#(m) unsignedMul(UInt#(n) x, UInt#(k) y) provisos (Add#(n,k,m)); endfunction
function a_type abs(a_type x) provisos (Arith#(a_type), Ord#(a_type)); endfunction
function a_type min(a_type x, a_type y) provisos (Ord#(a_type)); endfunction
function a_type exp(a_type x, a_type y) provisos (Ord#(a_type)); endfunction
function a_type max(a_type x, a_type y) provisos (Ord#(a_type)); endfunction
function Integer div(Integer x, Integer y); endfunction
function Integer mod(Integer x, Integer y); endfunction
function Integer quot(Integer x, Integer y); endfunction
function Integer rem(Integer x, Integer y); endfunction


//-------------------------------------------------------------
// Errors and warnings etc..


function a_type error(String s);
 // Function body is hardcoded inside Toy BSV Compiler
endfunction


function a_type warning(String s, a_type v);
 // Function body is hardcoded inside Toy BSV Compiler
 // Returns its second argument.
endfunction

function a_type message(String s, a_type v);
 // Function body is hardcoded inside Toy BSV Compiler
endfunction


//-------------------------------------------------------------
// Arith typeclass


/*

// THIS_WILL_NOT_PARSE_YET

typeclass Arith #(type data_t)

provisos (Literal#(data_t));
  function data_t \+ (data_t x, data_t y);
  function data_t \- (data_t x, data_t y);
  function data_t negate (data_t x);
  function data_t \* (data_t x, data_t y);
  function data_t \/ (data_t x, data_t y);
  function data_t \% (data_t x, data_t y);
endtypeclass
*/

//-------------------------------------------------------------
// Wires - here called bluewires

(* bluewire *) 
interface RWire#(type prct) ;
  method Action wset(prct datain) ;
  method Maybe#(prct) wget() ;
endinterface: RWire


// 
// Do not rename prct - it is hardwired by the compiler.
module $BUILTIN "cambridge" mkRWire (RWire#(prct)) provisos (Bits#(prct));
  // Monad implemented by the compiler directly.
endmodule

//  wset and wget can be in the same rule for an UnsafeRWire.
module $BUILTIN "cambridge" mkUnsafeRWire (RWire#(prct)) provisos (Bits#(prct));
  // Monad is implemented by the compiler directly.
endmodule


//-------------------------------------------------------------
//  ...


endpackage
