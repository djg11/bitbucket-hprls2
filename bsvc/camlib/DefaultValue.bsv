//
// Toy BSV Compiler Library File.
// Interface signatures (C) Bluespec Inc.
// Module implementations (C) DJ Greaves, University of Cambridge, Computer Laboratory.
// $Id: DefaultValue.bsv,v 1.1 2012-12-21 22:38:32 djg11 Exp $
//
package DefaultValue;

typeclass DefaultValue #(type tdef_t);
 tdef_t defaultValue ;
endtypeclass



endpackage


