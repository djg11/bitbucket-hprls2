//
// Toy BSV Compiler Library File.
// Interface signatures (C) Bluespec Inc.
// Module implementations (C) DJ Greaves, University of Cambridge, Computer Laboratory.
//
package BuildVector;

// Examples from the online reference guide:
//    Vector#(3, Bool) v1 = vec(True, False, True);
//    Vector#(4, Integer) v2 = vec(2, 17, 22, 42);

// Create a Vector of size n from n arguments.
function rr_t vec(aa_t x); //  provisos (BuildVector#(aa_t, rr_t, 0));

  // error "BuildVector::vec not implemented yet"

endfunction

endpackage