//
// Toy CBG-BSV Compiler Library File.
// Interface signatures (C) Bluespec Inc.
// Module implementations (C) DJ Greaves, University of Cambridge, Computer Laboratory.
// $Id: StmtFSM.bsv,v 1.2 2013-02-08 10:06:45 djg11 Exp $
//
package StmtFSM;


interface FSM;
  method Action start();
  method Action waitTillDone();
  method Bool done();
  method Action abort(); // Warning - not all of these methods are wired up in the current mkFSM implementation in CBG-BSV
endinterface: FSM


module $BUILTIN "cambridge"  mkFSM #(Stmt seq_stmt) (FSM the_fsm);
   // The body of this is built in to the toy compiler.
endmodule

endpackage