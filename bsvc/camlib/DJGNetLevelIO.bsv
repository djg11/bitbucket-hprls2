//
// Toy BSV Compiler Library File.
// Module implementations (C) 2015 DJ Greaves, University of Cambridge, Computer Laboratory.
//

// Instantiating these registers makes blocking and non-blocking I/O.

package DJGNetLevelIO;


// Instantiating these registers will create simple I/O terminals on the design.
// They have no schedulling order since they are all simplex from the Bluespec point of view.
// This can be considered a temporary/backdoor mechanism unitl CBus implementation is finished?

// Both the input and output registers can have handshake or be always ready.  The ones with handshake are called 'streaming'.

interface DJGNonblockingInputReg#(type prct); 
   (* always_enabled *) method prct _read(); 
endinterface


interface DJGStreamingInput #(type facontent_type); 
  (* synchronized *) method Action deq();
  (* synchronized *) method facontent_type first();
endinterface


interface DJGStreamingOutput #(type facontent_type); 
  (* synchronized *) method Action enq(facontent_type x1);
endinterface

module $BUILTIN "cambridge" mkStreamingInput (DJGStreamingInput#(prct)) provisos (Bits#(prct));
  // Monad is implemented by the compiler directly.
endmodule

module $BUILTIN "cambridge" mkStreamingOutput (DJGStreamingOutput#(prct)) provisos (Bits#(prct));
  // Monad is implemented by the compiler directly.
endmodule

module $BUILTIN "cambridge" mkNonBlockingInputReg (DJGNonblockingInputReg#(prct)) provisos (Bits#(prct));
  // Monad is implemented by the compiler directly.
endmodule

module $BUILTIN "cambridge" mkBlockingOutputReg#(prct initial_value) (Reg#(prct)) provisos (Bits#(prct));
  // Monad is implemented by the compiler directly.
endmodule

module $BUILTIN "cambridge" mkNonBlockingOutputReg#(prct initial_value) (Reg#(prct)) provisos (Bits#(prct));
  // Monad is implemented by the compiler directly.
endmodule


endpackage
