// $Id: vsys.v,v 1.2 2012-12-05 15:42:00 djg11 Exp $
//
// vsys.v A test wrapper for simulating very simple tests with clock and reset.
//

module SIMSYS();
   reg clk, reset;
   initial begin 
      $dumpfile("/home/djg11/Dropbox/a.vcd");
      $dumpvars(0, dut);
      //$dumpvars(0, dut.tiny_dm_memory);
      //$dumpvars(0, dut.tiny_im_memory);            
      reset = 1; 
      clk = 1; 
      # 400 reset = 0; end

   always #1000 clk = !clk;

   // Simon's:
   //   mkTestBenchA dut(.CLK(clk), .RST_N(!reset));

   // CBG-BSV:
   cth dut(.CLK(clk), .RST_N(!reset));

 initial # 1000000 $finish;
endmodule
// eof
