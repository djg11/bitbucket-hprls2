package TinyTestBench;

import FIFO::*;
import List::*;
import GetPut::*;
import StmtFSM::*;
import TinyTypes::*;
import TinyAsm::*;
import Tiny3::*;
import BRAM::*;


// The test program is coded in an assembly language entirely written in Bluespec. It is a simple program that writes the integers 9 down to 0 to the output FIFO. 

(* synthesise *) // DJG added that.
module mkTestBenchA(Empty);

   /********** the program we wish to assemble **********/

   // Programs are are coded as a function that are passed the findaddr function as an argument.

   function List#(AsmLineT) example_program(function ImmT findaddr(String label)) =
/*0*/      cons(asmi("",    0, 0),  // Keep register 0 clear.
/*1*/      cons(asmi("",    1, 10), // put 10 in r1
/*2*/      cons(asmi("",    2, findaddr("loop")), // loop address in r2
/*3*/      cons(asm("loop", OpOut, FDECb, ShiftNone, SkipNever, 1, 0, 1), // r1-- and output
/*4*/      cons(asm("",     OpJump, FaORb, ShiftNone, SkipZero, 3, 2, 0), // jump to r2 if not zero
/*5*/      cons(asm("",     OpReserved, Freserved, ShiftNone, SkipNever, 0, 0, 0), // stop processor
      tagged Nil))))));
/*
  00 80000000 Load immediate 0 to R0
  01 8100000a Load immediate 10 to R1
  02 82000003 Load immediate 3 (address of loop) to R2
  03 01000583 Output R1 instruction and decrement r1.
  04 03040296 Jmp to R2 if non-zero
  05 00000387 Stop processor
*/

   function List#(AsmLineT) short_program(function ImmT findaddr(String label));
      return      cons(asmi("",    0, 0), // put 0 in r0
           tagged Nil);
   endfunction

   function List#(Integer) short_xprogram(Integer x);
       return
        cons(x,         cons(x,
           tagged Nil));

   endfunction

   InstructionROM_T irom = assemblerf(example_program); // Compile the program

   function Action reveng(Integer ios);
     //  for (Integer ios=0;ios<10;ios=ios+1)  
     $display("Revenger rom %d %h %d", ios, irom[ios], irom[ios]);
   endfunction

  Reg#(bit) print_done <- mkReg(0);

  rule debug_rom0 (!print_done);
    let hv1 = select(irom, 1);
    let hv2 = irom[2];
    $display("Debug rom 0 %h %h %h", irom[0], hv1, hv2);
    print_done <= 1;
  endrule
 

   TinyCompIfc tiny <- mkTinyComp(irom);       // Instantiate the core


   rule handle_output;
      let out <- tiny.out.get(); // This will block until out has something in it.
      $display("%t: output = %d\n", $time, out);
   endrule

endmodule



endpackage: TinyTestBench
// eof
