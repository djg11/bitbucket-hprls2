// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 12e
// mkOnce
// ================================================================

package Tb;

import StmtFSM::*;

(* synthesize *)
module mkTb (Empty);

   ////////////////////////////////////////
   // you could use a FSM for this with one action
   // but this is more compact
   Once trigger <- mkOnce (action
                              $display("tiggered at ", $time);
                           endaction );

   ////////////////////////////////////////
   // trigger whenever ready to trigger
   // but rule below will only reset it every 5 cycles
   // so this sequence will take 4 * 5 cycles +
   Stmt test =
   seq
      trigger.start();
      trigger.start();
      trigger.start();
      trigger.start();
   endseq;
   
   mkAutoFSM( test );

   ////////////////////////////////////////
   Reg#(int) cntr <- mkReg(0);
   
   rule tryToStart;
      // bump counter every cycle
      cntr <= cntr + 1;

      // but only reset the "once" state machine
      // every 5 cycles, so the test sequence will
      // stall until this resets
      if ( cntr % 5 == 0 )
         trigger.clear();
   endrule

endmodule
   
endpackage
