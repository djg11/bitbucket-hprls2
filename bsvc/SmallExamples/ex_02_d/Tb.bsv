// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 2d
// "Rules, Registers, and FIFOs"
// Multiple registers in a rigid synchronous pipeline, now reusing
// the Reg#() interface, and therefore also reusing the syntax shorthands
// for register reads and writes.
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package Tb;

(* synthesize *)
module mkTb (Empty);

   Reg#(int) x    <- mkReg ('h10);
   Reg#(int) pipe <- mkPipe;

   rule fill;
      pipe <= x;
      x <= x + 'h10;
   endrule

   rule drain;
      let y = pipe;
      $display ("    y = %0h", y);
      if (y > 'h80) $finish(0);
   endrule
endmodule


(* synthesize *)
module mkPipe (Reg#(int));

   Reg#(int) x1    <- mkRegU;
   Reg#(int) x2    <- mkRegU;
   Reg#(int) x3    <- mkRegU;
   Reg#(int) x4    <- mkRegU;

   rule r1;
      x2 <= x1 + 1;
      x3 <= x2 + 1;
      x4 <= x3 + 1;
   endrule

   rule show;
      $display ("    x1, x2, x3, x4 = %0h, %0h, %0h, %0h", x1, x2, x3, x4);
   endrule

   method Action _write (int a);
      x1 <= a;
   endmethod

   method int _read ();
      return x4;
   endmethod

endmodule: mkPipe

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

       Reg#(int) pipe <- mkPipe;

Instantiate a mkPipe module; bind its interface to variable 'pipe' of type Reg#(int).
Was:    Pipe_ifc  pipe <- mkPipe;

          pipe <= x;

Writes the value of x to the pipe.  Shorthand for:    pipe._write (x);
Was:    pipe.send (x);

          let y = pipe;

Reads a value out of 'pipe'.  Shorthand for:    let y = pipe._read();
Was:    let y = pipe.receive();

We no longer define    'interface Pipe_ifc'.

    module mkPipe (Reg#(int));

The module has a standard register interface.

       method Action _write (int a);

Definition of the _write() method.
Was:    method Action send (int a);

       method int _read ();

Definition of the _read() method.
Was:    method int receive ();

* ================================================================
*/
