// Copyright 2008 Bluespec, Inc.  All rights reserved.

// section 8c -  Bit-vectors and square-bracket notation

package Tb;

import Vector::*;

//////////////////////////////////////////////////////////////////////
// skip this for now (see section 11 for more detail on polymorphic functions)
// these may be part of the Bluespec library soon anyway.
//
// The library defines "rotateBy" but left or right doesn't
// really make sense with a vector, so define out our own
// "up"   (meaning 0->1 1->2 2->3, .... n->0)
// "down" (meaning n->n-1, n-1->n-2, ... 1->0, 0->n)
// we will use these later 
function Vector#(n,td) rotateUp( Vector#(n,td) inData, UInt#(m) inx) 
   provisos( Log#(n,m) );
   return rotateBy( inData, inx );
endfunction

function Vector#(n,td) rotateDown( Vector#(n,td) inData, UInt#(m) inx) 
   provisos( Log#(n,m) );
   UInt#(m) maxInx = fromInteger(valueof(n)-1);
   return rotateBy( inData, maxInx-inx );
endfunction
//////////////////////////////////////////////////////////////////////

(* synthesize *)
module mkTb (Empty);

   // like verilog, we can access the bits of Bit#() data types,
   // but not of Int#() and UInt#() (not without recasting them to
   // Bit anyway)..  The idea is to use Int and UInt for mathematical
   // counter operation where it doesn't really make since to extract
   // bits.. That being said, yes, we know hardware guys love to do
   // all sorts of data specific tricks with numbers like "is this number
   // divisible by 4, or bits[1:0] == 0)..
   // Regardless you should try to use Bit#() only for things that are bit
   // type structures and not counters, etc..
   

   // Anyway, if you define a Bit type, you can use [] to get at a particular bit
   rule test1;
      Bit#(5) foo1 = 5'b01001;
      $display("bit 0 of foo1 is ", foo1[0]);
      $display("bit 1 of foo1 is ", foo1[1]);
      $display("bit 2 of foo1 is ", foo1[2]);
      $display("bit 3 of foo1 is ", foo1[3]);
      $display("bit 4 of foo1 is ", foo1[4]);

      // You can also get a range of bits:
      Bit#(6) foo2 = 'b0110011;
      $display("bit 2:0 of foo2 is %b", foo2[2:0]);

      // and note that this returns a type Bit of the new size
      Bit#(3) foo3 = foo2[5:3];
      $display("bit 5:3 of foo2 is %b", foo3);

      // you can also set bits as you'd expect
      foo3 = 3'b111;
      $display("set foo3[2:0] to 1s => %b", foo3);
      foo3[1:0] = 0;
      $display("set foo3[1:0] to 0  => %b", foo3);

      // BUT you can set them in reverse order or you'll
      // get the warning:
      //
      // Warning: "Tb.bsv", line 45, column 17: (T0035)
      //    Bit vector of unknown size introduced near this location.
      //    Assuming a size of 0. This behavior may be removed in a future release.
      //
      // foo3[0:1] = 0;  // uncomment this and see.

      // anotherway to use and think of this rather than create a Bit#(8)
      // is to create a vector of bit (where bit is and alias for Bit#(1))
      Vector#(8, bit) bar1 = replicate(0);

      //////////////////////////////////////////////////////////////////////
      // Why do this, because then we have all sorts of vector processing functions
      // available to us, so lets set a bit to 1 if it is divisable by 3
      // see reference manual for more information.
      function bit isInteresting( Integer i );
         return (i % 3 == 0) ? 1 : 0;
      endfunction
      
      Vector#(32,bit) bar2 = genWith( isInteresting );
      $display("bar2 is %b", bar2);
      
      //////////////////////////////////////////////////////////////////////
      // we can also do some builting rotates, shifts, etc..

      Vector#(32,bit) bar3 = 32'b0101_0001_0000_0000_0000_0000_1000_1001;
      $display("bar3         is %b", bar3);

      $display("reverse bar3 is %b\n", reverse( bar3 ));
      
      $display("bar3         is %b", bar3);
      $display("rotate1 bar3 is %b", rotate( bar3 ));
      $display("rotate2 bar3 is %b\n", rotate(rotate( bar3 )));

      // rotate left or right doesn't necessarily make since
      // or at least I liked the idea of rotateUp and rotateDown
      // (you could of course wrap it in your own function with these)
      $display("bar3             is %b",   bar3);
      $display("rotate up 1 bar3 is %b",   rotateUp( bar3, 1 ));
      $display("rotate up 2 bar3 is %b",   rotateUp( bar3, 2 ));
      $display("rotate up 3 bar3 is %b\n", rotateUp( bar3, 3 ));

      $display("bar3             is %b",   bar3);
      $display("rotate dn 1 bar3 is %b",   rotateDown( bar3, 1 ));
      $display("rotate dn 2 bar3 is %b",   rotateDown( bar3, 2 ));
      $display("rotate dn 3 bar3 is %b\n", rotateDown( bar3, 3 ));

      // and you can still quickly pack and unpack them between a Bit
      Bit#(32) val = 25;
      bar3 = unpack( val );
      $display("bar unpacked = %b / %d", bar3, bar3);
      
      bar3[17] = 1;
      
      // and back
      Bit#(32) val2 = pack( bar3 );
      $display("bar packed   = %b / %d", val2, val2);

      $finish (0);
   endrule

endmodule: mkTb

endpackage: Tb

