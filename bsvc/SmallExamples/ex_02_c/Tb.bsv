// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 2c
// "Rules, Registers, and FIFOs"
// Multiple registers in a rigid synchronous pipeline
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package Tb;

(* synthesize *)
module mkTb (Empty);

   Reg#(int) x    <- mkReg ('h10);
   Pipe_ifc  pipe <- mkPipe;

   rule fill;
      pipe.send (x);
      x <= x + 'h10;
   endrule

   rule drain;
      let y = pipe.receive();
      $display ("    y = %0h", y);
      if (y > 'h80) $finish(0);
   endrule
endmodule

interface Pipe_ifc;
   method Action send (int a);
   method int    receive ();
endinterface

(* synthesize *)
module mkPipe (Pipe_ifc);

   Reg#(int) x1    <- mkRegU;
   Reg#(int) x2    <- mkRegU;
   Reg#(int) x3    <- mkRegU;
   Reg#(int) x4    <- mkRegU;

   rule r1;
      x2 <= x1 + 1;
      x3 <= x2 + 1;
      x4 <= x3 + 1;
   endrule

   rule show;
      $display ("    x1, x2, x3, x4 = %0h, %0h, %0h, %0h", x1, x2, x3, x4);
   endrule

   method Action send (int a);
      x1 <= a;
   endmethod

   method int receive ();
      return x4;
   endmethod

endmodule: mkPipe

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

       Reg#(int) x    <- mkReg ('h10);

Initial value in register is hex 10 (i.e., decimal 16).

       Pipe_ifc  pipe <- mkPipe;

Instantiate a mkPipe module; bind its interface to variable 'pipe' of
type Pipe_ifc.

       rule fill;
          pipe.send (x);
          x <= x + 'h10;
       endrule

Always, send x into the pipe and increment it by hex 10.

       rule drain;
          let y = pipe.receive();            // L
          $display ("    y = %0h", y);
          if (y > 'h80) $finish(0);
       endrule

Always, receive y from the pipe and display it.  Stop when y > 'h80.
The line L declares a local variable y and initializes it to the result of pipe.receive().
The line could also have been written:    int y = pipe.receive();
i.e., we could have explicitly declared y by giving its type, int.
When we use 'let' instead, the compiler infers the type for y, which must
be int because that is the type of the right-hand side.

    interface Pipe_ifc;
       method Action send (int a);
       method int    receive ();
    endinterface

Interface for the pipe module, with send and receive methods.

       Reg#(int) x1    <- mkRegU;
       Reg#(int) x2    <- mkRegU;
       Reg#(int) x3    <- mkRegU;
       Reg#(int) x4    <- mkRegU;

Four int registers. mkRegU makes a reg 'uninitialized', i.e., without
specifying a reset value.  Bluespec typically uses 'haaaaaaaa.

       rule r1;
          x2 <= x1 + 1;
          x3 <= x2 + 1;
          x4 <= x3 + 1;
       endrule

This is a rigid synchronous pipeline, a rigid shift register.  On each
rule firing, each register is incremented and shifted into the next
register, and all these happen in parallel.

       method Action send (int a);
          x1 <= a;
       endmethod

Whenever the parent module invokes 'send', the int argument is stored in x1.

       method int receive ();
          return x4;
       endmethod

Whenever the parent module invokes 'receive', this returns the contents of x4.

When you execute the program, you'll see the pipeline shift register
behavior.  In the first few clocks, before the pipeline fills up,
you'll see that the values being shifted, and the value returned, are
the default reset values in the registers.

* ================================================================
*/
