package EdgeFIFOs;

// ================================================================
// Copyright (c) Bluespec, Inc., 2005-2008
//
// This package contains a collection "Edge FIFOs" that are useful
// at the edges of a BSV design to communicate with external
// circuitry that is typically written in Verilog/VHDL

// These FIFOs are 1-element FIFOs.  There are two versions:
//   mkPipelineFIFO, unguarded enq (1-tick latency, for incoming data)
//   mkPipelineFIFO, unguarded deq (1-tick latency, for outgoing data)

// These FIFOs differ from the BSV library FIFOs in two dimensions,
//     Scheduling, and Unguarded ends
// which described in more detail below.

// Scheduling
//   - a PipelineFIFO allows a simultaneous enq and deq when full,
//     behaving like a deq followed by enq, so that the value already
//     in the FIFO is returned by the deq, and the new enq'd value
//     then occupies the FIFO, and the FIFO remains full.
//     Note: every value spends at least 1 cycle in the FIFO
//
//     Thus, this FIFO is useful for incoming "registered" data,
//     where the expectation is a latency of 1 tick due to regstering.
//
//   The BSV library provides 'mkLFIFOF' also known as a "loopy FIFO"
//       and this is just a 1-element PipelineFIFO (with guarded ends)

// Unguarded ends
//   When dealing with external circuits, the BSV methods in the
//   interface often have to be "always ready, always enabled".
//
//   Thus, if the method tries to enq into or deq out of a FIFO
//   inside the BSV design, it can't have any implicit conditions.
//   So, these methods have to be "unguarded", i.e., they need
//   explicit 'notFull' and 'notEmpty' methods, and the
//   'enq' and 'deq' methods should only be used after explicitly
//   testing these conditions, because they don't have any
//   implicit conditions.
//
//   On an edge FIFO, only the end of the FIFO pointing towards
//   the external circuit needs to be unguarded.  The other end
//   of the FIFO, pointing into the BSV design, should have the
//   usual BSV guarded FIFO semantics
//
//   The BSV library provides FIFOs that are unguarded at both ends

// ================================================================

import RWire::*;
import FIFOF::*;

// ================================================================
// 1-element "pipeline FIFO"
// It's a 1-element FIFO (register with Valid/Invalid tag bit), where
//   - if empty, can only enq, cannot deq, leaving it full
//   - if full, can
//     - either just deq, leaving it empty
//     - or deq and enq simultaneously (logically: deq before enq), leaving it full

// ----------------
// Version with "unguarded enq"

module mkPipelineFIFO_ug_enq (FIFOF#(a))
  provisos (Bits#(a,sa));

  // STATE ----------------
   
  Reg#(Maybe#(a))   taggedReg <- mkReg (tagged Invalid); // the FIFO
  RWire#(a)         rw_enq    <- mkRWire;                // enq method signal
  PulseWire         pw_deq    <- mkPulseWire;            // deq method signal

  Bool enq_ok = ((! (isValid (taggedReg))) || pw_deq);
  Bool deq_ok = isValid (taggedReg);

  // RULES ----------------
   
  rule rule_update_final (isValid(rw_enq.wget) || pw_deq);
     taggedReg <= rw_enq.wget;
  endrule

  // INTERFACE ----------------
   
  method Bool  notFull;
    return enq_ok;
  endmethod

  method Action enq(v);
    if (enq_ok)
      rw_enq.wset(v);
    else
      $display ("mkPipelineFIFO_ug_enq: error: enq called when not enq_ok");
  endmethod

  method Bool  notEmpty;
    return deq_ok;
  endmethod

  method Action deq()  if (deq_ok);
     pw_deq.send ();
  endmethod

  method first()       if (taggedReg  matches  tagged Valid .v);
     return v;
  endmethod

  method Action clear();
    taggedReg <= tagged Invalid;
  endmethod
endmodule: mkPipelineFIFO_ug_enq

// ----------------
// Version with "unguarded deq"

module mkPipelineFIFO_ug_deq (FIFOF#(a))
  provisos (Bits#(a,sa));

  // STATE ----------------
   
  Reg#(Maybe#(a))   taggedReg <- mkReg (tagged Invalid); // the FIFO
  RWire#(a)         rw_enq    <- mkRWire;                // enq method signal
  PulseWire         pw_deq    <- mkPulseWire;            // deq method signal

  Bool enq_ok = ((! (isValid (taggedReg))) || pw_deq);
  Bool deq_ok = isValid (taggedReg);

  // RULES ----------------
   
  rule rule_update_final (isValid(rw_enq.wget) || pw_deq);
     taggedReg <= rw_enq.wget;
  endrule

  // INTERFACE ----------------
   
  method Bool  notFull;
    return enq_ok;
  endmethod

  method Action enq(v) if (enq_ok);
    rw_enq.wset(v);
  endmethod

  method Bool  notEmpty;
    return deq_ok;
  endmethod

  method Action deq();
    if (deq_ok)
      pw_deq.send ();
    else
      $display ("mkPipelineFIFO_ug_deq: error: deq called when not deq_ok");
  endmethod

  method first();
     if (deq_ok)
       return fromMaybe (?, taggedReg);
     else
       return ?;
  endmethod

  method Action clear();
    taggedReg <= tagged Invalid;
  endmethod
endmodule: mkPipelineFIFO_ug_deq

// ================================================================

endpackage:  EdgeFIFOs
