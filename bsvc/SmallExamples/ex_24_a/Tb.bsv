// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 24a
// "Impedance matching" at a module boundary--
//     unguarded fifos, interface transformater functions from fifos
//     to bus-socket interfaces, removing RDY and ENA wires.
// ================================================================

package Tb;

// ----------------
// Imports from BSV libs

import FIFO:: *;
import FIFOF:: *;
import GetPut:: *;
import ClientServer:: *;
import Connectable:: *;

// ----------------
// Imports for the design

import Bus_socket_ifc::*;
import EdgeFIFOs::*;

// ----------------------------------------------------------------
// The top-level testbench

(* synthesize *)
module mkTb (Empty);

   // Instantiate master and slave;
   Master_ifc  m  <- mkMaster ();
   Slave_ifc   s  <- mkSlave ();

   // Make the connections
   mkConnection (m.tlm_master_socket,  s.tlm_slave_socket);
   mkConnection (m.bus_master_socket,  s.bus_slave_socket);
endmodule: mkTb

// ----------------------------------------------------------------
// Master module

interface Master_ifc;
   interface Get#(M2S_Packet)       tlm_master_socket;    // TLM interface
   interface Bus_socket_master_ifc  bus_master_socket;    // Bus socket interface
endinterface

module mkMaster (Master_ifc);

   // ----------------
   Reg#(int) cycle <- mkReg (0);

   FIFO#(M2S_Packet)   fout1  <-  mkFIFO;
   FIFOF#(M2S_Packet)  fout2  <-  mkPipelineFIFO_ug_deq;

   // ----------------
   // RULES

   // Maintain cycle count
   rule count_cycles;
      cycle <= cycle + 1;
   endrule

   // Rule to generate packets (except every seventh cycle) 
   rule gen_pkts ((cycle & 'b111) != 'b111);
      let pkt = M2S_Packet { op: OP, otherdata: cycle };
      fout1.enq (pkt);    // send via TLM interface
      fout2.enq (pkt);    // send via bus socket interface
      $display ("Cycle %0d: master: master sending packet %0d", cycle, cycle);
   endrule

   interface tlm_master_socket = fifoToGet (fout1);
   interface bus_master_socket = fifo_to_bus_socket_master_ifc (fout2);
endmodule

// ----------------------------------------------------------------
// Slave module

interface Slave_ifc;
   interface Put#(M2S_Packet)      tlm_slave_socket;    // TLM interface
   interface Bus_socket_slave_ifc  bus_slave_socket;    // Bus socket interface
endinterface

module mkSlave (Slave_ifc);

   Reg#(int) cycle <- mkReg(0);

   FIFO#(M2S_Packet)  fin1  <-  mkFIFO;
   FIFOF#(M2S_Packet) fin2  <-  mkPipelineFIFO_ug_enq;

   // ----------------
   // RULES

   rule count_cycles;
      cycle <= cycle + 1;
   endrule

   // Receive packets on the TLM interface
   rule drain1;
      let pkt1 = fin1.first();  fin1.deq();
      if (pkt1.op == NOP) $display ("Cycle %0d: Slave tlm: error received a noop", cycle);
      else                $display ("Cycle %0d: Slave tlm: received %0d", cycle, pkt1.otherdata);
   endrule

   // Receiving packets on the Bus socket interface
   rule drain2;
      let pkt2 = fin2.first();  fin2.deq();
      if (pkt2.op == NOP) $display ("Cycle %0d: Slave bus: error received a noop", cycle);
      else                $display ("Cycle %0d: Slave bus: received %0d", cycle, pkt2.otherdata);

      if ((pkt2.op != NOP) && (pkt2.otherdata > 10)) $finish(0);
   endrule

   interface tlm_slave_socket = fifoToPut (fin1);
   interface bus_slave_socket = fifo_to_bus_socket_slave_ifc (fin2);

endmodule

// ================================================================

endpackage: Tb
