// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 24a
// "Impedance matching" at a module boundary--
//     unguarded fifos, interface transformater functions from fifos
//     to bus-socket interfaces, removing RDY and ENA wires.
// ================================================================

package Bus_socket_ifc;

// ----------------
// Imports from BSV libs

import FIFOF::*;
import Connectable:: *;

// ----------------
// Packets traversing a bus socket

typedef enum { NOP, OP } M2S_Packet_Op   // A special 'no op' opcode, and 1 or more real opcodes
        deriving (Bits, Eq);

typedef struct {
  M2S_Packet_Op  op;
  int            otherdata;   // Just a placeholder for other fields in a packet
} M2S_Packet
  deriving (Bits);

// ================================================================
// Generic Bus Socket Interfaces
// These interfaces will be "always ready, always enabled"
// so that there are no extra signal wires beyong what is implied by
// the argument and result types of the methods

// ----------------
// A master interface

(* always_ready *)
interface Bus_socket_master_ifc;
   method M2S_Packet_Op    getPktOp ();
   method int              getPktOther ();

   method Action           pktAccept ();
endinterface: Bus_socket_master_ifc

// The following function produces a bus socket master interface,
// given the interface of a fifo yielding those packets

function Bus_socket_master_ifc
         fifo_to_bus_socket_master_ifc (FIFOF#(M2S_Packet)  pkts);
   return
     (interface Bus_socket_master_ifc;
         method M2S_Packet_Op  getPktOp ();
            return pkts.notEmpty ? pkts.first.op : NOP ;
         endmethod
         method int  getPktOther ();
            return pkts.first.otherdata ;
         endmethod

         method Action  pktAccept ();
            if (pkts.notEmpty)
               pkts.deq ();
         endmethod
      endinterface);
endfunction: fifo_to_bus_socket_master_ifc

// ----------------
// A slave interface

(* always_ready, always_enabled *)
interface Bus_socket_slave_ifc;
   method Action  putPkt (M2S_Packet_Op  op,
                          int            otherdata);
   method Bool    pktAccept ();
endinterface: Bus_socket_slave_ifc

// The following function produces a bus socket slave interface,
// given the interface of a fifo to receive those packets

function Bus_socket_slave_ifc
         fifo_to_bus_socket_slave_ifc (FIFOF#(M2S_Packet)  pkts);
   return
     (interface Bus_socket_slave_ifc;
         method Action  putPkt (M2S_Packet_Op  op,
                                int            otherdata);
            if ((op != NOP) && (pkts.notFull)) begin
               let pkt = M2S_Packet { op   : op,
                                      otherdata: otherdata };
               pkts.enq (pkt);
            end
         endmethod

         method Bool pktAccept ();
            return pkts.notFull;
         endmethod
      endinterface);
endfunction: fifo_to_bus_socket_slave_ifc

// ================================================================
// Connections for the bus socket master and slave interfaces

instance Connectable #(Bus_socket_master_ifc, Bus_socket_slave_ifc);
   module mkConnection #(Bus_socket_master_ifc  master,
                         Bus_socket_slave_ifc   slave)
                       (Empty);

      (* no_implicit_conditions, fire_when_enabled *)
      rule relay_pkt;
         slave.putPkt (master.getPktOp,
                       master.getPktOther);
      endrule

      (* no_implicit_conditions, fire_when_enabled *)
      rule relay_pkt_accpt (slave.pktAccept) ;
         master.pktAccept ;
      endrule
      
   endmodule
endinstance

// This is the symmetric slave<->master connection, and just uses
// the previous connectable definition by flipping the arguments
instance Connectable #(Bus_socket_slave_ifc, Bus_socket_master_ifc);
   module mkConnection #(Bus_socket_slave_ifc   slave,
                         Bus_socket_master_ifc  master)
                       (Empty);
      mkConnection (master, slave);
   endmodule
endinstance

// ================================================================

endpackage: Bus_socket_ifc
