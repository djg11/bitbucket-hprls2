// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 3f
// "Module hierarchy and interfaces"
// Actions: defining an Action function, and invoking it from multiple rules.
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package Tb;

(* synthesize *)
module mkTb (Empty);

   Reg#(int) x <- mkReg (0);
   Reg#(int) y <- mkReg (0);

   function Action incr_both (int dx, int dy);
      return
         action
            x <= x + dx;
            y <= y + dy;
         endaction;
   endfunction

   rule r1 (x <= y);
      incr_both (5, 1);
      $display ("(x, y) = (%0d, %0d); r1 fires", x, y);
      if (x > 30) $finish (0);
   endrule

   rule r2 (x > y);
      incr_both (1, 4);
      $display ("(x, y) = (%0d, %0d): r2 fires", x, y);
   endrule

endmodule

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

       function Action incr_both (int dx, int dy);
          return
             action
                x <= x + dx;
                y <= y + dy;
             endaction;
       endfunction

This function creates an Action that represents the incrementing of x
by dx and y by dy.  Rules represent atomic transactions.  Rule bodies
specify the collection of Actions that are performed atomically when
the rule executes.  This function constructs and returns an Action
(using the action-endaction block) that can be used in a rule body, in
the same way that an Action method constructs an Action that can be
used in a rule body. However, unlike an Action method, an Action
function is not part of an interface, does not explicitly specify any
condition, and is just considered an abbreviation for the Actions in
the function body.  In short, functions allow you to construct
parameterized Actions.

The function is defined inside the module as a local function. It is
not visible at all outside this module.  Functions can also be defined
at the top level, inside other functions and, in general, in any
scope.  Normal scoping rules apply--for example, the use of 'x' and
'y' inside the function body refer to the registers x and y in the
function's surrounding scope, namely in the module Tb.

       rule r1 (x <= y);
          incr_both (5, 1);
       ...
       rule r2 (x > y);
          incr_both (1, 4);
          ...

The 'incr_both' function is invoked in both the rules.  The semantics
are directly equivalent to inlining the function where it is invoked,
binding the function arguments suitably-- (5,1) to (dx,dy) in the
first case and (1,4) to (dx,dy) in the second case.

This function is another illustration of how all types in BSV,
including Action, are 'first class'.  Other examples will show how one
can create functions that return modules, interfaces, rules, and so
on, resulting in extremely powerful parameterization.  In fact, since
functions can even return other functions, BSV is termed a
'higher-order' language, and this kind of code is called 'higher-order
design' or 'higher-order programming'.

* ================================================================
*/
