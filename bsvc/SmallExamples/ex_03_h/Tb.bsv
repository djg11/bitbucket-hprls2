// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 3h
// "Module hierarchy and interfaces"
// Standard ``TLM'' interfaces Get, Put; standard mkConnection; standard
//        interface transformers fifoToGet(), fifoToPut().
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package Tb;

import FIFO::*;
import GetPut::*;
import ClientServer::*;
import Connectable::*;

// ----------------------------------------------------------------
// A top-level module connecting the stimulus generator to the DUT

//(* synthesize *)
module mkTb (Empty);

   Client#(int,int) stimulus_gen <- mkStimulusGen;
   Server#(int,int) dut          <- mkDut;

   // Connect a client to a server (flow of requests and flow of responses)
   mkConnection (stimulus_gen, dut);

// The AltTb.bsv contains an
//alternative form:  Empty eftemp <-  mkConnection_ClientServer(stimulus_gen, dut);

endmodule: mkTb

// ----------------------------------------------------------------
// The DUT

//(* synthesize *)
module mkDut (Server#(int,int));

   FIFO#(int) g_in  <- mkFIFO;    // to buffer incoming requests
   FIFO#(int) g_out <- mkFIFO;    // to buffer outgoing responses

   rule compute;
      let x = g_in.first; g_in.deq;
      let y = x+1;                   // Some 'server' computation (here: +1)
      if (x == 20) y = y + 1;        // Modeling an 'occasional bug'
      g_out.enq (y);
   endrule

   interface request  = fifoToPut (g_in);
   interface response = fifoToGet (g_out);

endmodule: mkDut

// ----------------------------------------------------------------
// The stimulus generator

interface Tester;
  interface Get#(int) tester_get;
endinterface

(* synthesize *)
//module mkStimulusGen (Client#(int,int));

module mkStimulusGen (Tester);

   Reg#(int)  x          <- mkReg (0);            // Seed for stimulus
   FIFO#(int) f_out      <- mkFIFO;               // To buffer outgoing requests
   FIFO#(int) f_in       <- mkFIFO;               // To buffer incoming responses
   FIFO#(int) f_expected <- mkFIFO;               // To buffer expected responses

   rule gen_stimulus;
      f_out.enq (x);
      x <= x + 10;
      f_expected.enq (x+1);    // mimic functionality of the Dut 'server'
   endrule

   rule check_results;
      let y     = f_in.first; f_in.deq;
      let y_exp = f_expected.first; f_expected.deq;
      $write ("(y, y_expected) = (%0d, %0d)", y, y_exp);
      if (y == y_exp) $display (": PASSED");
      else            $display (": FAILED");
      if (y_exp > 50) $finish(0);
   endrule

// DJG temp avoid xformer for the moment using these two lines:
//   interface request  = fifoToGet (f_out);
//   interface response = fifoToPut (f_in);

// instead of
//   return fifosToClient (f_out, f_in);

// Inlining the body of the ifc xformer we have a further alternative:
//   return (
  //        interface Client;
  //           interface Get request  = fifoToGet (f_out);
   //          interface Put response = fifoToPut (f_in);
   //       endinterface);
   return (interface Tester;
              interface Get tester_get = fifoToGet (f_out);
           endinterface
          );
 
endmodule: mkStimulusGen

// ----------------
// An interface transformer (a function with interface arguments and results)

function Client#(req_t, resp_t) fifosToClient (FIFO#(req_t) f_reqs, FIFO#(resp_t) f_resps);
   return interface Client;
             interface Get request  = fifoToGet (f_reqs);
             interface Put response = fifoToPut (f_resps);
          endinterface;
endfunction: fifosToClient

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)


Interfaces for 'getting an item' and 'putting an item', and the means
to connect them, are so common in system design that BSV provides many
convenient parameterized library interfaces, modules and functions for
the purpose.  This example redoes the previous example by using these
library elements instead of defining them from scratch, and it
reflects a common paradigm in BSV coding--use BSV's powerful library
elements to put together models and designs very quickly.

In the literature, and in the industry, using these kind of interfaces
is often called "Transaction Level Modeling", or "TLM" for short.

    import FIFO::*;
    import GetPut::*;
    import ClientServer::*;
    import Connectable::*;

These import BSV libaries for FIFOs, Get and Put interfaces, Client
and Server interfaces, and mkConnection (see below).

    module mkTb (Empty);
       Client#(int,int) stimulus_gen <- mkStimulusGen;
       Server#(int,int) dut          <- mkDut;

The type Client#(req_t,resp_t) is a client interface (see previous
example for details) yielding requests of type req_t via a Get
subinterface and accepting responses of type resp_t via a Put
subinterface.

The type Server#(req_t,resp_t) is a server interface (see previous
example for details) accepting requests of type req_t via a Put
subinterface and yielding responses of type respt_t via a Get
subinterface.

       // Connect a client to a server (flow of requests and flow of responses)
       mkConnection (stimulus_gen, dut);

This connects a Client to a Server, in both direction.  Technically,
it instantiates a module that takes these two interfaces as
parameters, and the module contains the two rules seen in the previous
example, i.e., a rule to get() a request from the client and put() it
into the server, and a rule to get() a response from the server and
put() it into the client.

[ If 'mkConnection' is instantiating a module, then what is its
  interface?  'mkConnection' has been defined with an Empty interface,
  so technicaly this statement could have been written as follows:

       Empty e <- mkConnection (stimulus_gen, dut);

  but since there is no subsequent use for the Empty interface 'e', we
  are allowed to write this in the shorter form shown in the actual
  code.
]

    module mkDut (Server#(int,int));
       ...
       interface request  = fifoToPut (f_in);
       interface response = fifoToGet (f_out);

The state and rules of mkDut are the same as in the previous example.
Here, the subinterfaces request and response are defined using the
assignment syntax.  'fifoToPut()' is a library function which we
sometimes call an "interface transformer" that takes a FIFO interface
argument (here: f_in) and returns a Put interface result.  Similarly,
fifoToGet() is a library interface transformer from a FIFO to a Put.
There is nothing magic about these interface transformers--they are
ordinary BSV functions, and you can write such functions yourself.  In
fact, below, we'll see another example of such a interface
transformer, fifosToClient.

    module mkStimulusGen (Client#(int,int));
       ...
       return fifosToClient (f_out, f_in);

Here we've made the interface definition even more succinct, a
one-liner.  Following the example of mkDut, we could have defined the
interface using two lines like this:

       interface request  = fifoToGet (f_out);
       interface response = fifoToPut (f_in);

Instead, we directly build the entire interface, which includes the
two subinterfaces, by calling the interface transformer
'fifosToClient()', defined next.

    function Client#(req_t, resp_t) fifosToClient (FIFO#(req_t) f_reqs, FIFO#(resp_t) f_resps);
       return interface Client
                 interface Get request  = fifoToGet (f_reqs);
                 interface Put response = fifoToPut (f_resps);
              endinterface;
    endfunction: fifosToClient

This is just a function that takes two FIFO interfaces and returns a
Client interface that gets requests from the first fifo and puts
responses into the second fifo.  In addition, note that it is
POLYMORPHIC, i.e., it is parameterized by types, i.e., it works for
any request type req_t and any response type resp_t.

Using polymorphism, parameterization, and higher-order programming,
BSV allows you do define very powerful and highly reusable components
(types, interfaces, modules, functions, and so on).

EXERCISE:

(1) Write a 'fifosToServer()' interface transfomer taking two FIFO
    interfaces and returning a Server interface, and use it in mkDut
    to replace the two subinterface definitions with a one-liner like
    the one in mkStimulusGen.

* ================================================================
*/
