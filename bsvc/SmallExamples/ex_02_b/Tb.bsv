// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 2b
// "Rules, Registers, and FIFOs"
// Two register of type Reg#(int), rules that update them.
// Composition of multiple Actions into a single atomic action (parallel composition)
// vs.
// Composition of multiple rules, i.e., multiple atomic actions-- (urgency)
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package Tb;

(* synthesize *)
module mkTb (Empty);

   Reg#(int) x1    <- mkReg (10);
   Reg#(int) y1    <- mkReg (100);

   Reg#(int) x2    <- mkReg (10);
   Reg#(int) y2    <- mkReg (100);

   rule r1;
      $display ("r1");
      x1 <= y1 + 1;
      y1 <= x1 + 1;
   endrule

   rule r2a;
      $display ("r2a");
      x2 <= y2 + 1;
   endrule

   rule r2b;
      $display ("r2b");
      y2 <= x2 + 1;
   endrule

//   (* descending_urgency = "r2a, r2b" *)

   rule show_and_eventually_stop;
      $display ("    x1,y1 = %0d,%0d;    x2,y2 = %0d,%0d", x1, y1, x2, y2);
      if (x1 >= 105) $finish (0);
   endrule

endmodule: mkTb

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

       rule r1;
          $display ("swap");
          x1 <= y1 + 1;
          y1 <= x1 + 1;
       endrule

The body of this rule contains two register write Actions.
These actions occur simultaneously.  After the rule executes,
x1 has 1 + the old value of y1, and y1 has 1 + the old value of x1.
It's a 'swap' of x1 and y1, along with some incrementing.
This is called 'parallel composition'--two Actions are composed
in parallel to form a more complex Action.
It is implemented with the natural hardware structure, i.e.,
  Q output of reg y1 goes through a +1 adder into D input of reg x1,
  Q output of reg x1 goes through a +1 adder into D input of reg y1.

       rule r2a;
          $display ("r1");
          x2 <= y2 + 1;
       endrule

       rule r2b;
          $display ("r2");
          y2 <= x2 + 1;
       endrule

These two rules 'conflict'--they can't be composed in parallel, Since
rules are atomic with respect to each other, either r2a executes
before r2b or vice versa.  Specifically, atomicity means that all
reads and writes of one rule either precede all reads and writes of
the other, or vice versa.

From the initial state (x2=10,y2=100), if we executed r2a followed by r2b,
    the final state would be (x2=101, y2=102)
Conversely, if we executed r2b followed by r2a,
    the final state would be (x2=12, y2=11)
These are the legal final states of the rules, with the atomicity constraint.

On the other hand, if we executed both in parallel within a single clock,
using the natural hardware structure for the rule bodies, i.e.,
  Q output of reg y2 goes through a +1 adder into D input of reg x2,
  Q output of reg x2 goes through a +1 adder into D input of reg y2,
then the final state would be (101,11), which is not a legal final state.
In particular, parallel composition would interleave reads and writes
of the rules in ways that violate atomicity.

When compiled, we get message like this from the compiler:

    Warning: "Tb.bsv", line 16, column 8: (G0010)
      Rule "r2b" was treated as more urgent than "r2a". Conflicts:
        "r2b" cannot fire before "r2a": calls to y2.write vs. y2.read
        "r2a" cannot fire before "r2b": calls to x2.write vs. x2.read
    Warning: "Tb.bsv", line 30, column 9: (G0021)
      According to the generated schedule, rule "r2a" can never fire.

Explanation: Both rules cannot fire in the same clock because they conflict.
Thus, in each clock, one rule has to be chosen; since the user has not specified
this choice, the compiler picks rule r2b over r2a.
Further, since both rules are enabled on every clock,
and r2b is always preferred over r2a,
thus the rule r2a can never fire.

       (* descending_urgency = "r2a, r2b" *)

Try uncommenting this 'attribute definition', building and executing again.
You will see that the 'more urgent' warning goes away, because now
you have specified an urgency.
The second warning will still be present, but it will now tell you that
rule r2b can never fire, since the specified urgency, r2a over r2b,
is the opposite of what the compiler picked previously.
You'll also see the difference in the outputs for x2 and y2.


Also try running this code in Bluesim with various flags of interest,
either individually or in combination:
    -cc    show clock info on every cycle
    -c     show clock info at end of simulation only
    -m <N> stop simulation after <N> cycles
    -ss    show state on every cycle
    -r     show all rule firings
    -help  to show all the available flags

With these flags, the $display()s in the code, and even the
show_and_eventually_stop rule, are not strictly necessary.

* ================================================================
*/
