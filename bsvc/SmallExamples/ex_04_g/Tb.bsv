// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 4g
// "RWires"
// An example illustrating an "atomicity paradox" arising due to
// the introduction of RWires
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package Tb;

import FIFO::*;

// ----------------------------------------------------------------
// A top-level module connecting the stimulus generator to the DUT

(* synthesize *)
module mkTb (Empty);

   FIFO#(int) f <- mkPipelineFIFO;    // Instantiate a FIFO (see module def below)

   // ----------------
   // Step 'state' from 1 to 10

   Reg#(int) state <- mkReg (0);

   rule step_state;
      if (state > 9) $finish (0);
      state <= state + 1;
   endrule

   // ----------------
   // Enqueue and dequeue something on every cycle (in every state)

   rule e;
      let x = state * 10;
      f.enq (x);
      $display ("State %0d: Enqueue %0d", state, x);
   endrule

   rule d;
      let y = f.first ();
      f.deq ();
      $display ("State %0d: Dequeue %0d", state, y);
   endrule

   // ----------------
   // Also clear the FIFO in state 2

   rule clear_counter (state == 2);
      f.clear ();
      $display ("State %0d: Clearing", state);
   endrule

endmodule: mkTb

// ----------------------------------------------------------------
// A 1-element "pipeline" FIFO

// (* synthesize *)
module mkPipelineFIFO (FIFO#(int));

   // STATE ----------------

   // The FIFO   
   Reg#(Bool)        full      <- mkReg (False);
   Reg#(int)         data      <- mkRegU;

   RWire#(int)       rw_enq    <- mkRWire;                // enq method signal
   PulseWire         pw_deq    <- mkPulseWire;            // deq method signal

   Bool enq_ok = ((! full) || pw_deq);
   Bool deq_ok = full;

   // RULES ----------------
   
   // This rule does all the work, taking into account that 'enq()' and
   // 'deq()' may be called simultaneously when the FIFO is already full
   rule rule_update_final (isValid(rw_enq.wget) || pw_deq);
      full <= isValid(rw_enq.wget);
      data <= fromMaybe (?, rw_enq.wget);
   endrule

   // INTERFACE ----------------
   
   method Action enq(int v) if (enq_ok);
      rw_enq.wset(v);
   endmethod

   method Action deq()  if (deq_ok);
      pw_deq.send ();
   endmethod

   method int first()       if (full);
      return data;
   endmethod

   method Action clear();
      full <= False;
   endmethod

endmodule: mkPipelineFIFO

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)


The module 'mkPipelineFIFO' implements a 1-element FIFO.  As with any
FIFO, one can enqueue data when empty.  When full, one can not only
dequeue the old data, but in the same cycle one can also enqueue new
data (hence the 'pipeline' in the name, because it behaves like an
interlocked pipeline register).

Just like in the up/down counter examples, when two methods have to
read and write common state (here, the 'full' bit), if those methods
are to run simultaneously in the same clock, we use the technique of
RWires, i.e., each method sets an RWire, and then a common rule (here,
'rule_update_final') does the actual work.

Looking at the methods in 'mkPipelineFIFO', both 'enq()' and 'deq()'
read the 'full' register (as part of 'enq_ok' and 'deq_ok'). The
'clear()' method writes the 'full' register.  So, clearly, the
'clear()' method must be scheduled AFTER the 'enq()' and 'deq()'
methods.

In module mkTb, we instantiate such a FIFO, and enqueue and dequeue
some data on every cycle.

In addition, in cycle 2, we also invoke the 'clear' method to forcibly
empty the FIFO.  In fact, in cycle 2, all four methods 'enq()',
'first()', 'deq()' and 'clear()' are invoked.  As we saw before,
'clear()' is scheduled last.  In which case, we would expect that, at
the end of cycle 2, the FIFO is left empty (cleared).

But a transcript of the run shows:

    State 0: Enqueue 0
    State 1: Dequeue 0
    State 1: Enqueue 10
    State 2: Dequeue 10
    State 2: Enqueue 20
    State 2: Clearing
    State 3: Dequeue 20
    State 3: Enqueue 30
    ...

i.e., in state 3, we successfully dequeue 20, which indicates that the
FIFO did not get cleared after all (the 'State 2: Clearing' message
notwithstanding)--the value 20 actually got enqueued in state 2!

This looks like a bug!  The schedule says 'enq(20)' precedes
'clear()', but after this the FIFO contains 20 and is not cleared!

The resolution of this paradox is that we mistakenly imagined that the
entire enqueue operation was atomic.  However, the enqueue operation
is now split into two different rules, one which calls the 'enq()'
method, and later the 'rule_update_final' rule, i.e., the "logical"
enqueue operation is no longer a single atomic action.  Indeed, what
happened here is that the 'clear()' method got scheduled in between
those two rules.  In fact, during compilation, we are told about this:

    Warning: "Tb.bsv", line 64, column 8: (G0036)
      Rule "clear" will appear to fire before "rule_update_final" when both fire
      in the same clock cycle, affecting:
        calls to full.write vs. full.write

Thus, even though the 'enq()' method preceded 'clear()', the final
enqueue action came later, and we see its effects!

Bottom line: when you split the actions across multiple rules,
communicating using RWires, you cannot assume they are atomic any
longer!  Be careful when using RWires!

----------------------------------------------------------------
EXERCISE:

(1) Fix the above problem.

    HINT (one possible approach): in the 'enq()' method, set the
    'full' and 'data' registers.  In 'rule_update_final', just
    set 'full' to False whenever 'deq()' but not 'enq()' is invoked.
    In this case there would still be the choice of whether the rule
    or the clear method fired first; but this wouldn't matter any
    more, as they would both be assigning the same value to 'full'.

* ================================================================
*/
