// Copyright 2008 Bluespec, Inc.  All rights reserved.

// section 8 d - Meaning of bit-range update of registers (whole register update).

package Tb;

(* synthesize *)
module mkTb (Empty);

   // Meaning of bit-range update of registers (whole register update)
   
   // lets define a register of bits
   Reg#(Bit#(8)) foo  <- mkReg(0);
   Reg#(Bit#(8)) foo2 <- mkReg(0);
   
   (* preempts = "up1, up2" *)

   rule up1;
      // now using [] to access bits in a register is slightly different
      // than in verilog.  Reading any number of bits in any form that
      // is legal is fine as there are no scheduling issues.
      $display("foo[1:0] = %b", foo[1:0]);
      $display("foo[2]   = %b", foo[2]);
      // etc etc

      // and writing a single bit within a rule fine,
      // BUT, bluespec considers this register one entity.
      // This means that we really write all the bits all the time
      // and we just mux appropriately..  But the compiler
      // allows only one write to this register..
      foo[1] <= 1;

      // if we also try to write another bit separately, as we might in verilog
      // we get the following compiler error:
      //
      // Error: "Tb.bsv", line 14, column 9: (G0004)
      //  Rule `RL_update1' uses methods that conflict in parallel:
      //    foo.write(...)
      //  and
      //    foo.write(...)
      //  For the complete expressions use the flag `-show-range-conflict'.
      
      // foo[2] <= 1;  // uncomment this to see error
      
      // if you need to do this, use a temporary variable...
      let tmp = foo2;
      tmp[1] = 1;
      tmp[3] = 1;
      tmp[7:6] = 3;
      foo2 <= tmp;

   endrule

   // this also applies to bits across rules..
   // one rule can't update foo[1] while another updates foo[2].

   // in this case, up1 and up2 have a resource conflict on
   // the register foo.  Even though foo has 8 bits and
   // one rule wants to write bit 2 an the other bit 1,
   // bluespec considers the register to be one indivisible
   // piece of state.  // multiple-register updates fix this --- djg!

   rule up2;
      foo[2] <= 1;
   endrule

   // So what do you do if you really really really really
   // need to do this ?  Consider creating separate bit registers
   // and concat the bits together when you read or write the whole
   // register.
   
   Reg#(bit) b1 <- mkReg(0);
   Reg#(bit) b2 <- mkReg(0);
   
   rule up3;
      b1 <= 1;
      b2 <= 1;
   endrule
   
   rule done (b2 == 1);
      $display( "b12 = %b", {b1,b2} );
      $finish;
   endrule

endmodule: mkTb

endpackage: Tb

