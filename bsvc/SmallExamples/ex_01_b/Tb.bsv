// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 1b
// A testbench module communicating with a DUT module via its interface
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package Tb;

(* synthesize *)
module mkTb (Empty);

   Ifc_typea  ifc_dt <- mkModuleDeepThought;

   rule theUltimateAnswer;
      $display ("Hello World! The answer is: %0d",  ifc_dt.the_answer (32'h0A, 32'd15, 17));
      $finish (0);
   endrule

endmodule: mkTb

interface Ifc_typea;
   method int the_answer (int x, int y, int z);
endinterface: Ifc_typea


(* synthesize *)
module mkModuleDeepThought (Ifc_typea);

   method int the_answer (int x, int y, int z);
      return x + y + z;
   endmethod

endmodule: mkModuleDeepThought


endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

       Ifc_type  ifc <- mkModuleDeepThought;

Instantiate (create an instance of) the module mkModuleDeepThought.
The variable 'ifc' of type 'Ifc_type' is bound to the interface
provided by the instantiated module.

          $display ("Hello World! The answer is: %0d",  ifc.the_answer (10, 15, 17));

Invoke the method 'the_answer' of the interface 'ifc' with arguments
10, 15 and 17, and display the result.

    interface Ifc_type;

Declare a new interface type, 'Ifc_type' (note first letter is
upper-case, because it is a user-defined type.

       method int the_answer (int x, int y, int z);

Declare a method 'the_answer' within the interface, with three int
arguments x, y and z, and returning an int result

    module mkModuleDeepThought (Ifc_type);

Declare a module 'mkModuleDeepThought' that provides an interface of
type 'Ifc_type' when instantiated.

       method int the_answer (int x, int y, int z);
          return x + y + z;
       endmethod

Define the method 'the_answer' needed to define the interface of type
'Ifc_type'.  The result is the sum of the arguments.

* ================================================================
*/
