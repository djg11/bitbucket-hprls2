// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 11b
// "Simple Polymorphic modules"
// ================================================================

package Tb;

//////////////////////////////////////////////////////////////////////
// I am going to reuse an existing interface for this
// example. Reg#(td) takes certain types of data,
// but since I am writing to a register, and often
// we are doing something with this module, it's rare
// that we don't need some provisos, and Bits is
// the one the Reg#() needs (it provides pack and unpack).
//
// this is basically just hiding a register under a new name.
// But notice, at this time, we can synthesize this boundary
// because it is polymorphic.  In the near future, Bluespec
// will allow some generation of parameterize verilog,
// but most of what bluespec is capable of parameterizing
// can not be expressed in verilog.  Some examples in the next
// section.  For now, just remember - you can synthesize
// polymorphic blocks..

// (* synthesize *)
module mkPlainReg( Reg#(tx) ) provisos(Bits#(tx,szTX));
   Reg#(tx) val <- mkRegU;

   // a note: the interface is Reg#(tx) to mkPlainReg
   // AND val has a Reg#(tx) interface, so I can
   // just return that interface. since they are identical
   // interface types (and that's what I want to do here)
   return val;

endmodule

// Bit is predefined (size/width is not), but Bit#()
// means we have pack/unpack, +,-, etc, etc
module mkPlainReg2( Reg#(Bit#(n)) );
   Reg#(Bit#(n)) val <- mkRegU;
   return val;
endmodule

//////////////////////////////////////////////////////////////////////
// If you wrap your polymorphic module with a specific data type
// then you can synthesize it

(* synthesize *)
module mkPlainRegInt( Reg#(int) );
   Reg#(int) val <- mkPlainReg;

   // this bsv short cut works as an equality when
   // the methods are the same.. I.E. the _write
   // method of mkPlainReg is essentially the _write
   // method of Reg#(tx).  It is more correct and
   // understandable to write it out, but for these
   // examples, I am opting to save code space.
   method Action _write(int i) = val._write(i);
   method int    _read()       = val._read();
endmodule

//////////////////////////////////////////////////////////////////////
// Bitwise#(tx) specifies that the various bitwise
// operators have to be defined (i.e. ~ | & ^ )
// So this can't be used with a Bool, for instance..
module mkNotReg( Reg#(tx) ) provisos(Bits#(tx,szTX)
                                     ,Bitwise#(tx)  // comment this out to see error
                                     );
   Reg#(tx) val <- mkRegU;

   method Action _write(tx i) = val._write(i);

   method tx     _read();
      return ~val;  // Bitwise#(tx) added because of ~ here
   endmethod
endmodule

//////////////////////////////////////////////////////////////////////
// Arith#(tx) specifies that the various Arith
// operators have to be defined (i.e. + - * / % )
module mkDoubleReg( Reg#(tx) ) provisos(Bits#(tx,szTX),
                                        Arith#(tx));
   Reg#(tx) val <- mkRegU;

   method Action _write(tx i) = val._write(i);

   method tx     _read();
      return val * 2; // Arith#(tx) added here
   endmethod
endmodule

//////////////////////////////////////////////////////////////////////
// a complete list of provisos is available in the reference guide

//////////////////////////////////////////////////////////////////////
// As a general design guideline, we recommend that you get a
// non-polymorphic version of a module working before you attempt to
// parameterize a module.  Part of this is because error messages can be
// easier to understand when real data types and lines are being used.
//
// But once you start parameterizing your blocks, you need to
// understand the error message put out by the compiler.
// for instance.. Comment out the "Bitwise" provisos from the
// mkNotReg example and you will get this error message:

/*
Error: "Tb.bsv", line 61, column 8: (T0030)
  The provisos for this expression are too general.
  Given type:
    _m__#(Reg#(tx))
  With the following provisos:
    IsModule#(_m__, _c__)
    Bits#(tx, szTX)
  The following additional provisos are needed:
    Bitwise#(tx)
    Introduced at the following locations:
      "Tb.bsv", line 69, column 14
*/

// The general error you will often see in the beginning is T0030
//   "The provisos for this expression are too general."
// This basically means that there are not enough provisos
// for the compiler to be able to safely match the module
// to various data types when called.
// In this case, we see the compile has actually
// told us:
//         The following additional provisos are needed:
//             Bitwise#(tx)

//////////////////////////////////////////////////////////////////////
// a frequent issue is that blocks may have state of differing sizes
// I put the type on the interface for a module since the size of the
// inputs and outputs certainly affects the size of bus width going in
// and out of the blocks.  But Bluespec also allows more flexibility with
// parameters on interfaces as opposed to parameters on modules (which
// need to be Bit typeable).

interface T2#(type ta, type tb);
   method Action  drive(ta ina, tb inb);
   method ta      result();
endinterface

// ta => tx, tb => ty, you can use the same variable names
// but I use different ones here just to make the point
// that this variable "tx" is not the exact same variable as "ta"
// (even though you may use the same name for it)
module mkT2( T2#(tx,ty) ) provisos(Bits#(ty,sY),  // need to pack/unpack ty
                                   Arith#(tx),    // need to add with type "ta"
                                   Add#(SizeOf#(tx),0,SizeOf#(ty))  // must be the same size
                                   );
   Wire#(tx) val <- mkWire;

   method Action  drive(tx ina, ty inb);
      // since tb is a different type than ta
      // we need to "recast" it via pack/unpack
      // in this case it is the same width (thanks to the Add provisos above
      val <= ina + unpack(pack(inb));
   endmethod

   method tx      result();
      return val;
   endmethod
endmodule

//////////////////////////////////////////////////////////////////////
// but suppose I want to make them different sizes now
// like I want to sent in typeA but return a wider size
// (or more common, perhaps a log size ?)
interface T3#(type ta, type tb);
   method Action  drive(ta ina);
   method tb      result();
endinterface

module mkT3( T3#(ta,tb) ) provisos(Bits#(ta,sA),        // need to pack/unpack ta
                                   Bits#(tb,sB),
                                   Add#(ununsed,sB,sA), // needed by truncate
                                   Log#(sA,sB));        // just for fun, make output roof of log2
   Wire#(tb) val <- mkWire;

   method Action  drive(ta ina);
      // I got myself confused sorting this out, so I often
      // find it's easiest to break out the individual steps
      // of what I am doing, rather than try to do it all at once
      // Plus, this allows the compiler to give you more accurate error messages

      Bit#(sA) tmp  = pack(ina);
      Bit#(sB) tmp2 = truncate(tmp);
      val          <= unpack(tmp2);

      // I also presume you would do something logical useful here :)

      // val <= unpack(truncate(pack(ina)));
   endmethod

   method tb      result();
      return val;
   endmethod
endmodule

//////////////////////////////////////////////////////////////////////
// see the next section for more interesting examples

(* synthesize *)
module mkTb (Empty);
   // instantiate them all just to be sure we compile every
   Reg#(Bool) r1 <- mkPlainReg;
   Reg#(int)  r2 <- mkPlainRegInt;

   Reg#(int)  r3 <- mkNotReg;
   Reg#(int)  r4 <- mkDoubleReg;

   // I don't think you would really want to do this, but
   // it shoulds using more than one type
   T2#(UInt#(32),Int#(32)) modT2 <- mkT2;

   // funky, but we said typeb was log2(typea).. so...
   T3#(UInt#(32),UInt#(5)) modT3 <- mkT3;

endmodule
   
endpackage
