// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 12f
// build your own state machine
// a jtag controller testbench
// ================================================================

package Tb;

import StmtFSM::*;

// import the tap controller state machine
import tap::*;

(* synthesize *)
module mkTb (Empty);

   Tap        tap <- mkTap;

   // I will occasionally use DWire so that I have a default value
   // to drive to a pin that is (*always_enabled*) as the
   // pins into this tap controller are.  A rule at the end
   // fires every cycle and drives the wire to the input method
   //
   Wire#(bit) tms <- mkDWire(1);
   Wire#(bit) tdi <- mkDWire(0);
   Wire#(bit) tdo <- mkDWire(0);

   ///////////////////////////////////
   // just to show how we can do it, create your own
   //    "assertion" which is a simple state check
   //    if the condition is false, print message
   //    and stop the simulation

   function Action myAssert( Bool cond, String mess );
      return action
                if (!cond) begin
                   $display( "FAIL: ", mess );
                   $finish;
                end
             endaction;
   endfunction

   ///////////////////////////////////
   // spec says five 1s reset the controller ;)
   function Stmt resetTap();
      return seq
                repeat (5) action
                              tms <= 1;
                           endaction
                action
                   // for the bluesim simulator, myAssert this state is correct
                   myAssert( tap.getState == TEST_LOGIC_RESET, "ERROR: bad state: not reset" );
                   tms <= 1;
                endaction
             endseq;
   endfunction
   
   ///////////////////////////////////
   // spec says five 1s reset the controller ;)
   Stmt test =
   seq
      // start from reset mode and verify state
      resetTap();

      // manually I just picked a path through the state machine
      // and I'm putting that bit stream through the TMI
      // This is a bit laborious, but I want to check that
      // the states end up correct by default.

      // for testing purpose I manually set my states and verify that
      // each state goes the right direction for a 1 or a 0.  For this
      // we need to set the state directly
      action
         tms <= 0;
      endaction

      action
         // verify the state moved correctly
         myAssert( tap.getState == RUN_TEST_IDLE, "ERROR: bad state 1" );
         tms <= 0;
      endaction

      action
         myAssert( tap.getState == RUN_TEST_IDLE, "ERROR: bad state 2" );
         tms <= 1;
      endaction

      action
         myAssert( tap.getState == SELECT_DR, "ERROR: bad state 3" );
         tms <= 0;
      endaction

      action
         myAssert( tap.getState == CAPTURE_DR, "ERROR: bad state 4" );
         tms <= 0;
      endaction
      
      action
         myAssert( tap.getState == SHIFT_DR, "ERROR: bad state 5" );
         tms <= 1;
      endaction

      action
         myAssert( tap.getState == EXIT1_DR, "ERROR: bad state 6" );
         tms <= 0;
      endaction

      action
         myAssert( tap.getState == PAUSE_DR, "ERROR: bad state 7" );
         tms <= 1;
      endaction

      action
         myAssert( tap.getState == EXIT2_DR, "ERROR: bad state 7b" );
         tms <= 1;
      endaction

      action
         myAssert( tap.getState == UPDATE_DR, "ERROR: bad state 8" );
         tms <= 0;
      endaction

      action
         myAssert( tap.getState == RUN_TEST_IDLE, "ERROR: bad state 9" );
         tms <= 0;
      endaction
      
      // that's it for now - any myAsserts ?
      $display("Passed!");

   endseq;
   
   // start up the bit stream
   mkAutoFSM (test);

   // this just allows me to use default wires
   rule driveWire;
      tap.tms( tms );
      tap.tdi( tdi );
   endrule
   

endmodule
   
endpackage
