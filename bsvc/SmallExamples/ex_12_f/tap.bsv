// (setq bsv-indent-level 4)

// a common jtag tap controller
// this particular example was found at this url
//       http://bleyer.org/jjtag/jjtag.pdf
// though as the web works, it may not be there forever

// define state names with a enum
typedef enum {TEST_LOGIC_RESET=0,
              RUN_TEST_IDLE=1,
              SELECT_IR,CAPTURE_IR,SHIFT_IR,EXIT1_IR,PAUSE_IR,EXIT2_IR,UPDATE_IR,
              SELECT_DR,CAPTURE_DR,SHIFT_DR,EXIT1_DR,PAUSE_DR,EXIT2_DR,UPDATE_DR }
  TapState deriving(Bits,Eq);

// in this case, I wanted to be able to print out state names
// much prettier for debug printing, but not necessary for functionality
function String getStateString( TapState st );
    case (st)
        TEST_LOGIC_RESET: return "TEST_LOGIC_RESET";
        RUN_TEST_IDLE:    return "RUN_TEST_IDLE";
        SELECT_DR:        return "SELECT_DR";
        CAPTURE_DR:       return "CAPTURE_DR";
        SHIFT_DR:         return "SHIFT_DR";
        EXIT1_DR:         return "EXIT1_DR";
        PAUSE_DR:         return "PAUSE_DR";
        EXIT2_DR:         return "EXIT2_DR";
        UPDATE_DR:        return "UPDATE_DR";
        SELECT_IR:        return "SELECT_IR";
        CAPTURE_IR:       return "CAPTURE_IR";
        SHIFT_IR:         return "SHIFT_IR";
        EXIT1_IR:         return "EXIT1_IR";
        PAUSE_IR:         return "PAUSE_IR";
        EXIT2_IR:         return "EXIT2_IR";
        UPDATE_IR:        return "UPDATE_IR";
        default:          return "*unknown state*";
    endcase
endfunction

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
interface Tap;
   // these are defined by the standard
   // and they are a single pin, with new ready or enable as generally
   // used by BSV.  I could have put this in front of the module
   // but this seemed like a better place for it.

   (* always_ready, always_enabled *) method Action tdi( bit i );  // input
   (* always_ready, always_enabled *) method bit    tdo();         // output
   (* always_ready, always_enabled *) method Action tms( bit i );  // input
   
   // for debug so I know we are going to the correct states
   // I would presume that a real chip would not use this port, 
   // and hence allow a synthesizer to optimize it away completely
   method TapState getState();
endinterface

////////////////////////////////////////////////////////////
(* synthesize *)
module mkTap( Tap );
   Wire#(bit) wtdi <- mkDWire(0);
   Wire#(bit) wtdo <- mkDWire(0);
   Wire#(bit) wtms <- mkDWire(0);
   
   Reg#(TapState) state <- mkReg(TEST_LOGIC_RESET);
   
   ////////////////////////////////////////////////////////////
   // this makes a lot of sense for tap, since there are exactly
   // two state directions... 0 in, and 1 in
   // this function takes the 
   //    1.  current state
   //    2.  the state to go if tms is 0
   //    3.  the state to go if tms is 1

   function Rules genRule( TapState stHere, TapState st0, TapState st1 );
   // create a list of rules (even though we only have on rule here)
   return rules
             rule stateChange( state == stHere );
                // for this state, goto st0 when tms=0, and st1 when tms=1
                // wtms is global, so no need to pass it as an argument
                if (wtms == 0) state <= st0;
                else           state <= st1;
             endrule
          endrules; // again the semicolon for "return <foo> ;"
   endfunction
    
   ////////////////////////////////////////////////////////////
   // addRules ( foo ) is actually how rules are normally
   // added to a sytem.. But the compiler knows when
   // it sees "rule/end" at the top level scope of a module
   // that it is "Adding"...  So a rule really "looks" like this
   //   addRules( rule foo;
   //               <actions>
   //             endrule );
   // in cases where a function (genRules in thius case) returns rules, 
   // we need to explicitly use "addRules" to do this..
   //
   // each line is one state (the first argument)
   // the second arg is where we go when tms is 0
   // the third arg is where we go when tms is 1

   addRules( genRule( TEST_LOGIC_RESET, RUN_TEST_IDLE, TEST_LOGIC_RESET ) );

   addRules( genRule( RUN_TEST_IDLE, RUN_TEST_IDLE, SELECT_DR ) );

   addRules( genRule ( SELECT_DR,    CAPTURE_DR,    SELECT_IR ) );

   addRules( genRule ( CAPTURE_DR, SHIFT_DR,      EXIT1_DR ) ); 
   addRules( genRule ( SHIFT_DR,   SHIFT_DR,      EXIT1_DR ) ); 
   addRules( genRule ( EXIT1_DR,   PAUSE_DR,      UPDATE_DR ) ); 
   addRules( genRule ( PAUSE_DR,   PAUSE_DR,      EXIT2_DR ) ); 
   addRules( genRule ( EXIT2_DR,   SHIFT_DR,      UPDATE_DR ) );
   addRules( genRule ( UPDATE_DR,  RUN_TEST_IDLE, SELECT_DR )); 
   
   addRules( genRule ( SELECT_IR,  CAPTURE_IR,    TEST_LOGIC_RESET ) );
   addRules( genRule ( CAPTURE_IR, SHIFT_IR,      EXIT1_IR ) ); 
   addRules( genRule ( SHIFT_IR,   SHIFT_IR,      EXIT1_IR ) ); 
   addRules( genRule ( EXIT1_IR,   PAUSE_IR,      UPDATE_IR ) ); 
   addRules( genRule ( PAUSE_IR,   PAUSE_IR,      EXIT2_IR ) ); 
   addRules( genRule ( EXIT2_IR,   SHIFT_IR,      UPDATE_IR ) );
   addRules( genRule ( UPDATE_IR,  RUN_TEST_IDLE, SELECT_DR )); 
   
   // noisy debug
   rule showState;
      $display("Current state is ", getStateString( state ) );
   endrule
   
   // let's just connect tdo to tdi for now
   // we'll do a scan chain example later
   rule dummy;
      wtdo <= wtdi;
   endrule
   
   ////////////////////////////////////////////////////////
   method Action tdi( bit i );
      // write input directly to the tdi wire
      wtdi._write( i );
   endmethod

   method bit    tdo();
      // the output of the scan chain
      return wtdo;
   endmethod

   method Action tms( bit i );
      // write the state in
      wtms._write(i);
   endmethod

   method TapState getState() = state;

endmodule
