// Copyright 2008 Bluespec, Inc.  All rights reserved.

// section 9 d - Strings

package Tb;

(* synthesize *)
module mkTb (Empty);

   // Bluespec has support for Strings.  Remember that
   // by definition, the compiler is creating synthesizable
   // logic, so Strings are just ascii chars joined into
   // a list..  They must have a fixed size (this isn't C++)..
   // None the less, they come in useful
   
   function Action passString( String s );
      return action
             $display("passString => ", s);
             endaction;
   endfunction

   Reg#(int) step <- mkReg(0);

   //////////////////////////////////////////////////////////////////////
   rule init ( step == 0 );
      $display("=== step 0 ===");
      
      String s1 = "This is a test";
      $display("first string = ", s1);
      
      // we can use + to concatinate
      String s2 = s1 + " of concatination";
      $display("Second string = ", s2);
      
      // and pass them to functions, which is handy 
      // ultimately in testbenches, debugging, etc
      passString( "String passed to a function" );

      // the valid escape chars are listed in the reference manual

      step <= step + 1;
   endrule

   //////////////////////////////////////////////////////////////////////
   rule step1 ( step == 1 );
      $display ("All done");
      $finish (0);
   endrule

   ////////////////////////////////////////
   rule watcher (step != 0);
   endrule

endmodule: mkTb

endpackage: Tb

