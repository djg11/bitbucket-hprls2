// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ex_09_g
// Section 9 g - Struct of registers vs. register of struct

package Tb;

import FIFO::*;

// this is a struct of bit-able types...
// this means we can add deriving(Bits,Eq), which
// means this structure will have a default
// pack/unpack defined for it (so that you can load it
// into fifos, registers, etc, etc) and it will define
// "==", so that two structures of the same time can be
// compared (which ends up being a complete bit by bit compare)

typedef struct {
   int   sa;
   Bool  sb;
   } TEx1 deriving(Bits,Eq);


// This functions fairly differently from a "struct" of registers..
// this can't be "deriving(Bits,Eq)" because we can't pack/unpack
// an interface...

typedef struct {
   Reg#(int)  ra;
   Reg#(Bool) rb;
   } TEx2;

(* synthesize *)
module mkTb (Empty);

   Reg#(int) step <- mkReg(0);
   
   // this creates a Reg of struct.. meaning that
   // r1 is a register value and r1 is read/write like
   // any other register...  it's fields are accessed
   // via its interface, but the whole entity is a register.
   Reg#(TEx1)    r1 <- mkReg( unpack(0) );

   // It is just a type, so we can use Ex1 (the structure)
   // anywhere we can use a type that has a pack/unpack (i.e is bits)
   FIFO#(TEx1) fifo <- mkFIFO;

   // TEx2 now, has interfaces defined inside of it.
   // So we have to be careful to attach instances to the
   // interfaces (or assign them appropriately to other interfaces
   // which *are* connected to instances - see t3)..
   TEx2 r2;
   r2.ra <- mkReg(2);
   r2.rb <- mkReg(True);

   // create registers, and assign the interfaces to the same
   // instances as defined by this interface/instance
   Reg#(int)  reg3A <- mkReg(3);
   Reg#(Bool) reg3B <- mkReg(False);

   TEx2 r3;
   r3.ra = reg3A;
   r3.rb = reg3B;

   rule init ( step == 0 );
      $display("=== step 0 ===");
      $display("r1.sa = ", r1._read().sa);  // DJG temporary edit - please remove need for this.
      //$display("r1.sa = ", r1.sa);
      $display("r1.sb = ", r1._read().sb);  // DJG temporary edit - likewise.
      // $display("r1.sb = ", r1.sb);            
      
      // A reg whose type is struct, must be written to all at once. (DJG: we can extend semantics beyond this with syntactic sugar?)
      // It is "one register" even though it has many bits
      // in it.  This means you may have to a read/modify/write
      // to the structure to change more than one field.
      TEx1 tax = r1;
      tax.sa = 20;
      tax.sb = False;
      r1 <= tax; // DJG:yes we would automate this process.
      step <= step + 1;
   endrule

   rule step1 ( step == 1 );
      $display("=== step 1 ===");
      $display("r2.ra = ", r2.ra);
      $display("r2.rb = ", r2.rb);


      // A struct of regs can be written to individually because they each have different method and interface.
      // So you don't have to create a temp local variable, but now you can't unpack it, register it, etc..
      r2.ra <= 31;
      r2.rb <= False;

      // r2.ra <= rhs needs expanding to r2.ra._write(rhs).

      step <= step + 1;
   endrule

   rule step2 ( step == 2 );
      $display("=== step 2 ===");
      $display("r3.ra = ", r3.ra);
      $display("r3.rb = ", r3.rb);
      $display ("All done");
      $finish (0);
   endrule


endmodule: mkTb

endpackage: Tb

