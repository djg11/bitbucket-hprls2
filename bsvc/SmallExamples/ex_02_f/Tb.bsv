// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 2f
// "Rules, Registers, and FIFOs"
// Elastic "asynchronous" pipeline using FIFOs.
// (Please compare this to ex_02_c, the rigid synchronous pipeline with registers)
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package Tb;

import FIFO::*;

(* synthesize *)
module mkTb (Empty);

   Reg#(int) x    <- mkReg ('h10);
   Pipe_ifc  pipe <- mkPipe;

   rule fill;
      pipe.send (x);
      x <= x + 'h10;
   endrule

   rule drain;
      let y <- pipe.receive;
      $display ("    y = %0h", y);
      if (y > 'h80) $finish(0);
   endrule
endmodule

interface Pipe_ifc;
   method Action            send    (int x);
   method ActionValue#(int) receive();
endinterface

(* synthesize *)
module mkPipe (Pipe_ifc);

   FIFO#(int) f1    <- mkFIFO;
   FIFO#(int) f2    <- mkFIFO;
   FIFO#(int) f3    <- mkFIFO;
   FIFO#(int) f4    <- mkFIFO;

   rule r2;
      let v1 = f1.first; f1.deq;
      $display (" v1 = %0h", v1);
      f2.enq (v1+1);
   endrule

   rule r3;
      let v2 = f2.first; f2.deq;
      $display (" v2 = %0h", v2);
      f3.enq (v2+1);
   endrule

   rule r4;
      let v3 = f3.first; f3.deq;
      $display (" v3 = %0h", v3);
      f4.enq (v3+1);
   endrule

   method Action send (int a);
      f1.enq (a);
   endmethod

   method ActionValue#(int) receive ();
      let v4 = f4.first;
      $display (" v4 = %0h", v4);
      f4.deq;
      return v4;
   endmethod

endmodule: mkPipe

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

    import FIFO::*;

Import the FIFO package, which provides the FIFO interface
and mkFIFO module definitions.

    interface Pipe_ifc;
       method Action            send    (int x);
       method ActionValue#(int) receive;
    endinterface

The 'receive' method's type, ActionValue#(int) indicates that it both
returns and int value and performs an Action.  Because of the latter,
it can only be called in Action contexts, namely the bodies of rules
(or other Action or ActionValue methods).

       FIFO#(int) f1    <- mkFIFO;
       FIFO#(int) f2    <- mkFIFO;
       FIFO#(int) f3    <- mkFIFO;
       FIFO#(int) f4    <- mkFIFO;

Instantiate four FIFOs (all initially empty) and bind the interfaces to f1..f4.

       rule r2;
          let v1 = f1.first; f1.deq;
          $display (" v1 = %0h", v1);
          f2.enq (v1+1);
       endrule

Read the first int element v1 of fifo f1, dequeue fifo f1, and enqueue
v1+1 into fifo f2.
The rules r3 and r4 are similar.
However, rules r2, r3 and r4  may or may not fire together.
Each rule fires whenever its input fifo has data available (implicit
condition of 'first' and 'deq'), and whenever its output fifo has
space available (implicit condition of 'enq').

       method Action send (int a);
          f1.enq (a);
       endmethod

This method just enqueues value a into fifo f1.  This method is only
enabled when f1 has space available.  Thus, the implicit condition of
f1.enq becomes the implicit condition of this 'send' method, and
becomes part of the enabling condition of rule 'fill' in the
testbench, i.e., that rule cannot fire until all these conditions are
true.

       method ActionValue#(int) receive ();
          let v4 = f4.first;
          $display (" v4 = %0h", v4);
          f4.deq;
          return v4;
       endmethod

This method reads the first int element v4 of fifo f4, dequeues fifo
f4, and returns v4.  This method is only enabled when f4 has data
available (implicit condition of 'first' and 'deq').  Thus, the
implicit conditions of 'f4.first' and 'f4deq' become the implicit
condition of this 'receive' method, which in turn becomes part of the
enabling condition of rule 'drain' in the testbench, i.e., that rule
cannot fire until all these conditions are true.

When executed, you can observe the pipeline behavior.  However, the
outputs from the $displays in different rules that execute in the same
cycle may be unpredictable.  Hence, we recommend using Bluesim with
the -cc flag, which displays cycle boundaries, so you can see what
happens within each cycle (you can also use -r to see which rules fire
in each cycle).

* ================================================================
*/
