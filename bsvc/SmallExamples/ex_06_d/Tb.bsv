// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 6d
// "Variables, assignments and combinational circuits"
// if-then-else hardware (mux) vs. if-then-else static elaboration
// ================================================================

package Tb;

import FIFO::*;
import GetPut::*;
import ClientServer::*;

// ----------------------------------------------------------------
// This is a made-up example to various use of conditionals
// (hardware muxes and static elaboration),
// so the exact functionality does not matter.

Bool configVar = True;
int  x0        = 23;

(* synthesize *)
module mkTb (Empty);

   Reg#(int) cycle <- mkReg (0);

   int initval = (configVar ? (x0 + 10) : (x0 + 20));

   Reg#(int) r0 <- (configVar ? mkReg (initval) : mkRegU);

   Reg#(int) r1;
   if (configVar)
      r1 <- mkReg(initval);
   else
      r1 <- mkRegU;

   // ----------------
   // RULES

   rule r;
      $display ("%0d: rule r, r0 = %0d, r1 = %0d", cycle, r0, r1);

      r0 <= ((pack (cycle)[0]==1) ? (r0 + 1) : (r0 + 5));

      if (pack (cycle)[0]==1)
         r1 <= r1 + 1;
      else
         r1 <= r1 + 5;

      if (cycle > 7) $finish (0);
      cycle <= cycle + 1;
   endrule

endmodule: mkTb

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

This is a made-up example to various use of conditionals
(hardware muxes and static elaboration),
so the exact functionality does not matter.

The top-level statements:

    Bool configVar = True;
    int  x0        = 23;

just define some top-level constants.  Note, in general this is a more
modern and preferable way to define constants instead of macro
preprocessor 'define' statements used in Verilog.

The statement in module 'mkTb'

       int initval = (configVar ? (x0 + 10) : (x0 + 20));

uses a conditional expression on the right-hand side.  But the
right-hand side is completely 'reducible' by the compiler, since the
values of all the variables are known.  Thus, this conditional
expression is statically reduced by the compiler and the statement
effectively defines the constant

       int initval = 33;

Similarly, in the statement:

       Reg#(int) r0 <- (configVar ? mkReg (initval) : mkRegU);

the conditional is statically reduced, so that the statement is
equivalent to:

       Reg#(int) r0 <- mkReg (33);

Note: we have made a conditional choice of instantiating one of two
modules, mkReg(0) vs. mkRegU.  Thus, one can use statically elaborated
conditionals to choose amongst alternative module hierarchies.

The statements:

       Reg#(int) r1;
       if (configVar)
          r1 <- mkReg(initval);
       else
          r1 <- mkRegU;

just constitute an alternative notation for the same idea as the
previous statement that instantiated r0.  Here, we first declare the
variable r1, of type Reg#(int).  Then, we use a conditional statement,
instead of a conditional expression, to initialize it to one of two
modules, either mkReg(initval) or mkRegU.  Again, because 'configVar'
is a statically known constant, this reduces to, equivalently,

       Reg#(int) r1;
       r1 <- mkReg(initval);

In the rule 'r', the statement:

          r0 <= ((pack (cycle)[0]==1) ? (r0 + 1) : (r0 + 5));

updates register r0 with the value of the conditional expression on
the right-hand side.  Here the predicate of the conditional depends on
a dynamic value (contents of 'cycle' register), and hence this
conditional expression cannot be reduced by the compiler.  Instead, it
becomes actual hardware that makes the selection dynamically.
Specifically, it becomes a mux (a multiplexor) that selects one of the
two values r0+1 or r0+5.  In fact, if we look at the generated Verilog
file, mkTb.v, we see that this source statement has become the
following Verilog:

      // register r0
      assign r0$D_IN = cycle[0] ? r0 + 32'd1 : r0 + 32'd5 ;
      assign r0$EN = 1'd1 ;

The statements:

          if (pack (cycle)[0]==1)
             r1 <= r1 + 1;
          else
             r1 <= r1 + 5;

are an alternative way of saying the same thing (here with r1 instead
of r0).  Instead of using a conditional expression, we use a
conditional statement.  But, in fact, the generated Verilog is the
same (with r1 instead of r0):

      // register r1
      assign r1$D_IN = cycle[0] ? r1 + 32'd1 : r1 + 32'd5 ;
      assign r1$EN = 1'd1 ;

----------------
SUMMARY: Bluespec SystemVerilog does not distinguish notationally
between statically elaborated conditionals and dynamic conditionals
(muxes).  The compiler uniformly tries to reduce all expressions
statically as far as possible.  If the predicate reduces to one of the
constants True or False, the compiler can statically eliminate the
conditional; otherwise it remains as a mux that dynamically selects
the value.

Bluespec SystemVerilog also does not distinguish between conditional
expressions (e1?e2:e3), if-then-else expressions, if-then-else
statements, case expressions, case statements, etc.  I.e., internal to
the compiler they are all treated the same.

* ================================================================
*/
