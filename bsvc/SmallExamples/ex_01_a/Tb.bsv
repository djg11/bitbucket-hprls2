// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 1a
// Very basic 'Hello World' example just to go through the
// mechanics of actually building and running a small BSV program
// (and not intended to teach anything about hardware!).
// ================================================================

package Tb;

(* synthesize *)
module mkTb (Empty);

   rule greet;
      $display ("Hello World!");
      $finish (0);
   endrule

endmodule: mkTb

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

In the following, items like '[a.b]' are cross-references to sections
in the Bluespec SystemVerilog Reference Guide, which can be found at
    $(BLUESPEC_HOME)/doc/BSV/reference-guide.pdf
Items like '[UG a.b]' are cross-references to sections in the Bluespec
SystemVerilog User Guide, which can be found at
    $(BLUESPEC_HOME)/doc/BSV/user-guide.pdf


    // Small Example Suite: Example 1a

[2.1] Single-line comments start with two slashes and go to end of
line. Multi-line comments can be written with the slash-star opening
bracket and the star-slash closing bracket, like this entire
commentary section.

    package Tb;

[3.1] It's good to organize all your code into packages, which are
like 'namespaces'.  The file name should be <package name>.bsv.

    (* synthesize *)

[13.1.1] Items in '(*' and '*)' are SystemVerilog 'attributes'.  This
attribute specifies that the module that follows must be synthesized
separately into a Verilog module.  The top-level module of a design
must be such a synthesis boundary.  Interior modules can optionally
also represent synthesis boundaries.  This example design has only one
module.

[UG 3.1] You could also use the '-g' command-line flag to specify the
modules that should be synthesis boundaries but we recommend placing
such attributes in the source text.

    module mkTb (Empty);

[5.3] Declares module 'mkTb' with 'Empty' interface.

[5.3] 'Empty' is a pre-defined interface with no methods.

       rule greet;

[5.6] Defines a rule named 'greet', with no explicit rule condition.

          $display ("Hello World!");

[12.8.1] Invoke system task $display
[2.5]    with a literal String.

          $finish (0);

[12.8.8] Finish the simulation.  The exit value of the simulation
process is 0.

       endrule

[5.6] Could optionally be written as    'endrule: greet'

    endmodule: mkTb

[5.3] The ': mkTb' repetition of the module name is optional

    endpackage: Tb

[3] The ': Tb' repetition is of the package name optional

* ================================================================
*/
