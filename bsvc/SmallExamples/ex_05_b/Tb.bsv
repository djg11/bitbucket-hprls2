// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 5b
// Attributes on rules: simple prioritization using 'descending_urgency'.
// This is a variant of example 5a, showing how a user can control
// the urgency of rules with attributes
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package Tb;

// importing the FIFO library package    
import FIFO ::*;    

// ----------------------------------------------------------------
// A top-level module instantiating a fifo

(* synthesize *)
module mkTb (Empty);

   FIFO#(int) f <- mkFIFO;    // Instantiate a fifo

   // ----------------
   // Step 'state' from 1 to 10

   Reg#(int) state <- mkReg (0);

   rule step_state;
      if (state > 9) $finish (0);
      state <= state + 1;
   endrule

   // ----------------
   // Enqueue two values to the fifo from seperate rules in the testbench
   // 

   (* descending_urgency = "enq1, enq2"*)
   rule enq1 (state < 7);
      f.enq(state);
      $display("fifo enq1: %d", state);
   endrule

   rule enq2 (state > 4);
      f.enq(state+1);
      $display("fifo enq2: %d", state+1);
   endrule

   // ----------------
   // Dequeue from the fifo, and check the first value
   // 

   rule deq;
       f.deq();
       $display("fifo deq : %d", f.first() );
   endrule

endmodule: mkTb

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

As seen in example 5a, the mkTb1 module invokes the two "enq" methods 
from two seperate rules, and therefore the two rules conflict.


   rule enq1 (state < 7);
      f.enq(state);
      $display("fifo enq: %d", state);
   endrule

   rule enq2 (state > 4);
      f.enq(state+1);
      $display("fifo enq: %d", state+1);
   endrule

compilation would cause the following warning:

Warning: "Tb.bsv", line 48, column 8: (G0010)
  Rule "enq2" was treated as more urgent than "enq1". Conflicts:
    "enq2" cannot fire before "enq1": calls to f.enq vs. f.enq
    "enq1" cannot fire before "enq2": calls to f.enq vs. f.enq

However, adding the "descending_urgency" attribute will guide the compiler
which rule should it treat as more urgent (and give higher priority when
both rules can fire).
line 40 states:

   (* descending_urgency = "enq1, enq2"*)

which will have the compiler schedule rule "enq1" to fire when both rules can fire
(i.e - when state is 5 or 6).

* ================================================================
*/
