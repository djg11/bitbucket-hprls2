// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 6b
// "Variables, assignments and combinational circuits"
// Variable assignments describe combinational circuits,
// and not the updating of storage.
// ================================================================

package Tb;

import FIFO::*;
import GetPut::*;
import ClientServer::*;

// ----------------------------------------------------------------
// This is a made-up example to demonstrate variable assignment semantics,
// so the exact functionality does not matter.

(* synthesize *)
module mkTb (Empty);

   Reg#(int) cycle <- mkReg (0);

   rule count_cycles;
      cycle <= cycle + 1;
      if (cycle > 7) $finish(0);
   endrule

   int x = 10;

   rule r;
      int a = x;
      a = a * a;
      a = a - 5;

      if (pack(cycle)[0] == 0) a = a + 1;
      else                     a = a + 2;

      if (pack(cycle)[1:0] == 3) a = a + 3;

      for (int k = 20; k < 24; k = k + 1)
         a = a + k;

      $display ("%0d: rule r, a = %0d", cycle, a);
   endrule

endmodule: mkTb

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

This is a made-up example to demonstrate variable assignment semantics,
so the exact functionality does not matter.

In many software languages (such a C/C++/C#/Java etc.) a variable
represents a chunk of storage somewhere in a computer's memory, and a
variable assignment statement has the semantics of replacing the
contents of that storage with a new value, the value computed from the
right-hand side of the assignment statement.

In some software languages (such as Haskell and SML), a variable does
not represent any storage--it is just a name for the value of the
right-hand side.

Bluespec SystemVerilog uses the latter interpretation of variables.
In hardware terms, this is equivalent to saying that a sequence of
variable assignments is just an incremental way to specify a
combinational circuit.

----------------
Consider the following statement in rule 'r':

          a = a * a;

Suppose 'a' on the right-hand side represents the output of some
combinational circuit CC (in this case, the value of 'x', i.e., the
trivial combinational circuit whose output is the constant 10).  Then,
this statement simply says that in the sequel, i.e., in the text
following this statement within the current scope, 'a' represents the
output of a new combinational circuit, namely the output of a
multiplier both of whose inputs are fed by CC.  I.e., this statement
incrementally describes a new combinational circuit from existing
combinational circuits.

----------------
Consider the following statement in rule 'r':

          if (pack(cycle)[0] == 0) a = a + 1;
          else                     a = a + 2;

Let 'a' before this statement represent a combinational circuit CC1.
This statement simply says that in the sequel, 'a' will describe the
output of a multiplexor whose inputs are fed from two combinational
circuits CC1+1 and CC1+2, and whose selector is fed from a
combinational circuit that tests the zero'th bit of 'cycle' for
equality to zero.

----------------
Consider the following statement in rule 'r':

          if (pack(cycle)[1:0] == 3) a = a + 3;

Let 'a' before this statement represent a combinational circuit CC2.
This statement simply says that in the sequel, 'a' will describe the
output of a multiplexor whose inputs are fed from two combinational
circuits CC2+3 and CC2, and whose selector is fed from a combinational
circuit that tests the two lower-order bits of 'cycle' for equality to
3.

----------------
Consider the following statement in rule 'r':

          for (int k = 20; k < 24; k = k + 1)
             a = a + k;

This is simply 'unrolled' and is the same as the sequence obtained by
copying the loop body:

             a = a + 20;
             a = a + 21;
             a = a + 22;
             a = a + 23;

i.e., in the sequel, 'a' represents the output of a chain of four
adders that add 20, 21, 22 and 23, respectively, to the previous
combinational circuit.

----------------
If we compile and execute, we see the following output:

    0: rule r, a = 182
    1: rule r, a = 183
    2: rule r, a = 182
    3: rule r, a = 186
    4: rule r, a = 182
    5: rule r, a = 183
    6: rule r, a = 182
    7: rule r, a = 186
    8: rule r, a = 182

which is consistent with our interpretation of variables and
assignment.

----------------
SUMMARY: variables like 'x' and 'a' never represent storage, and
assignment statements with '=' never represent updating of storage.
Variables are simply names for the values (output of combinational
circuits) represented by the expressions on the right-hand sides of
their assignment statements.

----------------
EXERCISE:
- In rule 'r', comment out the 'int' declaration for 'a', like so:

          a = x;    // was: int a = x;

  When we compile, we get the following error message:

    Error: "Tb.bsv", line 33, column 17: (P0039)
      Assignment of 'a' without declaration, or non-local assignment in possibly
      wrong context (e.g. declared at evaluation time, maybe updated at runtime,
      perhaps you meant "<=" instead, etc.)

  The compiler is complaining that we are assigning to 'a' without any
  declaration. Every variable MUST first be declared, either with a
  type, or with 'let', before it can be used.

----------------
EXERCISE:
- In rule 'r', comment out the initialization of 'a', like so:

          int a; //  = x;

  When we compile, we get the following error message:

    Error: "Tb.bsv", line 34, column 11: (P0040)
      Use of 'a' without assignment

  where line 34 is the next line:

          a = a * a;

  The reason is that the 'a' on the right-hand side is now
  meaningless--since 'a' has not been initialized or assigned before,
  it does not represent any combinational circuit yet, and so the
  expression 'a * a' is meaningless.

* ================================================================
*/
