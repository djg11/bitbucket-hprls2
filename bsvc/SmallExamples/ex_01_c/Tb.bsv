// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 1c
// Same as Example 1b, i.e., a testbench module communicating with a DUT module via its interface
// except we separate the DUT and its interface into a separate package (and file).
// This file is the testbench only.
// ================================================================

package Tb;

import DeepThought::*;

(* synthesize *)
module mkTb (Empty);

   Ifc_type  ifc <- mkModuleDeepThought;

   rule theUltimateAnswer;
      $display ("Hello World! The answer is: %0d",  ifc.the_answer (10, 15, 17));
      $finish (0);
   endrule

endmodule: mkTb

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

    import DeepThought::*;

Imports declarations from the package.
The '*' imports all the declarations exported by the package.

* ================================================================
*/
