// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 1c
// Same as Example 1b, i.e., a testbench module communicating with a DUT module via its interface
// except we separate the DUT and its interface into a separate package (and file).
// This file is the DUT.
// ================================================================

package DeepThought;

interface Ifc_type;
   method int the_answer (int x, int y, int z);
endinterface: Ifc_type

(* synthesize *)
module mkModuleDeepThought (Ifc_type);

   method int the_answer (int x, int y, int z);
      return x + y + z;
   endmethod

endmodule: mkModuleDeepThought

endpackage: DeepThought

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

    package DeepThought;

The file name should be <package name>.bsv.

* ================================================================
*/
