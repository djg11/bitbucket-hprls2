// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 3g
// "Module hierarchy and interfaces"
// Nested interfaces; definition of methods and subinterfaces directly, and by assignment.
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package Tb;

import FIFO::*;

// ----------------------------------------------------------------
// A top-level module connecting the stimulus generator to the DUT

(* synthesize *)
module mkTb (Empty);

   Client_int  stimulus_gen <- mkStimulusGen;
   Server_int  dut          <- mkDut;

   // Connect flow of requests from stimulus generator to DUT
   rule connect_reqs;
      let req <- stimulus_gen.get_request.get();  dut.put_request.put (req);
   endrule

   // Connect flow of responses from DUT back to stimulus generator
   rule connect_resps;
      let resp <- dut.get_response.get();  stimulus_gen.put_response.put (resp);
   endrule
endmodule: mkTb

// ----------------------------------------------------------------
// Interface definitions

interface Put_int;
   method Action put (int x);
endinterface

interface Get_int;
   method ActionValue#(int) get ();
endinterface

interface Client_int;
   interface Get_int  get_request;
   interface Put_int  put_response;
endinterface: Client_int

interface Server_int;
   interface Put_int  put_request;
   interface Get_int  get_response;
endinterface: Server_int

// ----------------------------------------------------------------
// The DUT

(* synthesize *)
module mkDut (Server_int);

   FIFO#(int) f_in  <- mkFIFO;    // to buffer incoming requests
   FIFO#(int) f_out <- mkFIFO;    // to buffer outgoing responses

   rule compute;
      let x = f_in.first; f_in.deq;
      let y = x+1;                   // Some 'server' computation (here: +1)
      if (x == 20) y = y + 1;        // Modeling an 'occasional bug'
      f_out.enq (y);
   endrule

   interface Put_int put_request;
      method Action put (int x);
         f_in.enq (x);
      endmethod
   endinterface

   interface Get_int get_response;
      method ActionValue#(int) get ();
         f_out.deq; return f_out.first;
      endmethod
   endinterface

endmodule: mkDut

// ----------------------------------------------------------------
// The stimulus generator

(* synthesize *)
module mkStimulusGen (Client_int);

   Reg#(int)  x          <- mkReg (0);            // Seed for stimulus
   FIFO#(int) f_out      <- mkFIFO;               // To buffer outgoing requests
   FIFO#(int) f_in       <- mkFIFO;               // To buffer incoming responses
   FIFO#(int) f_expected <- mkFIFO;               // To buffer expected responses

   rule gen_stimulus;
      f_out.enq (x);
      x <= x + 10;
      f_expected.enq (x+1);    // mimic functionality of the Dut 'server'
   endrule

   rule check_results;
      let y     = f_in.first; f_in.deq;
      let y_exp = f_expected.first; f_expected.deq;
      $write ("(y, y_expected) = (%0d, %0d)", y, y_exp);
      if (y == y_exp) $display (": PASSED");
      else            $display (": FAILED");
      if (y_exp > 50) $finish(0);
   endrule

   interface get_request = interface Get_int;
                              method ActionValue#(int) get ();
                                 f_out.deq; return f_out.first;
                              endmethod
                           endinterface;

   interface Put_int put_response;
      method Action put (int y) = f_in.enq (y);
   endinterface

endmodule: mkStimulusGen

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

    module mkTb (Empty);
       ...
    endmodule

The top-level module just instantiates two submodules (the stimulus
generator and the DUT), and connects the flow of requests from the
stimulus generator to the DUT and the flow of responses in the
opposite direction using two rules.  We'll discuss the method calls in
the rules after discussing the interface definitions.

    interface Put_int;
       method Action put (int x);
    ...
    interface Get_int;
       method ActionValue#(int) get ();
    ...
    interface Client_int;
       interface Get_int  get_request;
       interface Put_int  put_response;
    ...
    interface Server_int;
       ...

These define the interface of the DUT (Server_int) and the stimulus
genenerator (Client_int).  Rather than defining methods in these
interfaces directly, we go about it hierchically, first defining the
interfaces Get_int and Put_int, because these interfaces capture a
common, reusable paradigm.

A Put_int interface just has a 'put' Action method, and a Get_int
interface just has a 'get' ActionValue method.  Once we have these,
then we can define the interface for the stimulus generator,
Client_int, in terms of these, i.e., it just has a subinterface to get
requests and another subinterface to put responses.  Conversely, the
interface for the DUT, Server_int just has a subinterface to put
requests and another subinterface to get responses.

Now we can go back to mkTb and look at one of its rules

       rule connect_reqs;
          let req <- stimulus_gen.get_request.get();  dut.put_request.put (req);
       endrule

to see how nested interfaces are used.  For example, in the
'dut.put_request.put()' phrase, 'dut' has type Server_int, which has
two subinterfaces. '.put_request' selects one of the subinterfaces,
the one with type Put_int.  This, in turn, has a method which is
selected by '.put'.

    module mkDut (Server_int);
       ...
       rule compute;
          ...

The first part of the module just instantiates two FIFOs to buffer
incoming requests and outgoing responses, and has a rule that models
"some" server computation, including an "occasional bug".  In general
the server computation could be arbitrarily complex, taking many
cycles, even a variable number of cycles, but these internal details
of the DUT are not important for this example.

       interface Put_int put_request;
          method Action put (int x);
             f_in.enq (x);
          endmethod
       endinterface

Here we define the put_request subinterface using the "direct" syntax.
Whenever the 'put' method inside this subinterface is called, it
enqueues the argument onto the fifo f_in.  The get_response
subinterface definition follows, also using direct definition syntax.

    module mkStimulusGen (Client_int);
       ...
       rule gen_stimulus;
          ...
       rule check_results;
          ...

The mkStimulusGen module first instantiates some local state. The rule
gen_stimulus outputs (into f_out) the current stimulus (x) and
prepares the next stimulus (here: +10).  The rule also buffers the
expected result by mimicing the DUT's functionality (here: +1).  The
rule check_results dequeues an incoming response, dequeues an expected
response, displays them, and prints PASSED or FAILED based on
comparing them.  The exact details of the stimulus and the dut
functionality are not important for this example.

       interface get_request = interface Get_int;
                                  method ActionValue#(int) get ();
                                     f_out.deq; return f_out.first;
                                  endmethod
                               endinterface;

Here we define the get_request subinterface using assignment.  The
entire 'interface ... endinterface' phrase after the '=' is an
"interface expression", i.e., it is an expression whose value is an
interface, and this value is assigned to the 'get_request' interface
variable on the left-hand side.  Why is this useful?  First, it allows
for flexible paramterized static elaboration.  For example, we could
write:

       interface _get_request = if (...static_parameter...)
                                   interface Get_int ... ... endinterface
                                else
                                   interface Get_int ... <different definition> ... endinterface

or even

       interface _get_request = ... function returning an interface ...

(which we will see in the next example).


       interface Put_int put_response;
          method Action put (int y) = f_in.enq (y);
       endinterface

Here, the interface definition uses the direct syntax, but the method
definition inside uses assignment.  After the '=' we see an expression
that represents the body of the method.

EXERCISES:

(1) Examine the Verilog generated for these modules, and understand
    the correspondence between the BSV interface and method
    definitions and the Verilog module port lists.

(2) In mkTb, we have two rules connect_reqs and connect_resps, one for
    each direction.  What happens if we combined them into a single
    rule?

(3) Execute the program and note the cycles in which rules fire.
    Then, for the fifo f_expected, replace the 'mkFIFO'
    constructor with 'mkSizedFIFO (10)', and observe again the cycles
    in which rules fire.  Why are they different? Hint: the mkFIFO
    constructor creates a default-sized FIFO (usually 2 elements),
    whereas the mkSizedFIFO(10) constructor creates a FIFO with 10 elements.
    Instead of 10, how small can the FIFO be without changing behavior?


(4) In rule check_results, what would happen if we forgot to write the
    statement 'f_expected.deq;'?

* ================================================================
*/
