// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 5e
// "Scheduling"
// Attributes on rules: conflict_free.
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package Tb;

(* synthesize *)
module mkTb(Empty);

   // a 3-bit reg with one bit always set
   Reg#(Bit#(3)) x <- mkReg (1);

   Reg#(int)     cycle <- mkReg (0);

   // A register used to induce a conflict
   Reg#(int) y <- mkReg (0);
  
   rule rx;
      cycle <= cycle + 1;
      x <= { x[1:0], x[2] };  // rotate the bits
      $display ("%0d: Rule rx", cycle);
   endrule

   (* conflict_free = "ry0, ry1, ry2" *)    // XXX

   rule ry0;
      if (x[0] == 1) y <= y + 1;
      $display ("%0d: Rule ry0", cycle);
   endrule

   rule ry1;
      if (x[1] == 1) y <= y + 2;
      $display ("%0d: Rule ry1", cycle);
   endrule

   rule ry2;
      if (x[2] == 1) y <= y + 3;
      $display ("%0d: Rule ry2", cycle);
   endrule

   rule done;
      if (cycle >= 10) $finish (0);
   endrule

endmodule: mkTb

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

The register x initially has value 1 (bit 0 is set).  In rule rx, we
rotate the bits.  Hence, it always has exactly one bit set, bit 0, 1
or 2.  Thus, the conditions insides rules ry0, ry1 and ry2 that guard
the updates to the register y are mutually exclusive.  But this proof
is too difficult for the compiler.

Here, the rules ry0, ry1 and ry2 are enabled on every cycle, but they
access the shared resource y on mutually exclusive conditions.  Hence,
there is no conflict in their accesses to y, but the compiler does not
know that.  Thus, it reports this during compilation and generates
generates priority logic that suppresses the other rules when one of
them is enabled.

With the 'conflict_free' attribute, however, the compiler assumes that
the conflicting resource will never be accessed simultaneously, and it
does not generate priority logic.  (It does generate simulation code
to ensure that the resource is never actually accessed
simultaneously.)

Compile this program twice,
- once with the line XXX above commented out
    [ Generates conflict warning message and priority logic.
      In fact, because of the conflicts, and the prioritization
      due the the conflict, one rule always gets priority.
      When executed, you will see only one of the ryJ rules firing. ]
- once with the line XXX above uncommented
    [ generates no conflict warning message and no priority logic.
      When executed, you will see all ryJ rules firing on every clock
      (although of course only one of them updates y on each clock. ]

In each case, examine the Verilog code to see the difference the logic.

* ================================================================
*/
