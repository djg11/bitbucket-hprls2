// Copyright 2008 Bluespec, Inc.  All rights reserved.

// section 9 d - Using abstract types (Bool, UInt#(), Int#()) instead of Bit#()

package Tb;

// verilog programmers will naturally want to only every use Bit#(n)
// as this gives them the complete bit selecting, operating freedom they
// are used to with verilog.  But verilog also allows a lot of dubious
// operations on data (such as automatic truncation, etc).  And a Bit#(n)
// doesn't really say what the data is all about.
//
// In BSV, we want to take advantage of strong typing (as vhdl coders are
// already used to).   This decreases bugs and find bugs earlier in the design
// cycle (at compile time, rather than debug or tapeout time)..  
// This is a good thing.


(* synthesize *)
module mkTb (Empty);

   Reg#(int) step <- mkReg(0);

   Reg#(Int#(16)) int16  <- mkReg('h800);
   Reg#(Int#(16)) uint16 <- mkReg('h800);

   rule step0 ( step == 0 );
      $display("== step 0 ==");
      // We already know about Bit#(16) so lets' look closer at the other types
      
      // UInt#(n) is an "unsigned int" of the defined bit length.  So a
      // UInt#(16) can be values 0 -> 'hFFFF unsigned.  It can never be negative
      // or less than zero (it is unsigned).  This is a common type to use
      // for index counters, etc, where you are counting things that will never
      // be negative.
      
      // as such, you shouldn't be able to directly load it with a negative number:
      //
      //    UInt#(16) foo = -1;  // uncomment this to see error
      //
      // will give you the error message:
      //    Error: "Tb.bsv", line 40, column 19: (T0051)
      //      Literal `-1' is not a valid UInt#(16).

      // 
      UInt#(16) foo = 'h1fff;
      $display("foo = %x", foo);

      // UInt has logical bitwise operators defined - that may change in the future...
      // the idea is that counters should allow bit fiddling since they are numbers, not bit
      // fields..  We know that, of course, this is what hardware guys do to optimize
      // structures a classic being to check if a number is odd or even by looking at bit 0.

      foo = foo & 5;
      $display("foo = %x", foo);

      // as such, [] is not legal for UInt (as it is for Bit)
      //     $display("foo[0] = %x", foo[0]); // uncomment this and see

      foo = 'hffff;
      $display("foo = %x", foo);

      // and it wraps just like you would expect it to
      foo = foo + 1;
      $display("foo = %x", foo);

      // beware the < 0 is never true (this is an unsigned number)
      // in fact, the compiler will generate this display as
      //     $display("foo = %x", 1'd0);
      $display("foo = %x", foo < 0) ;      

      // this is a quick way to get min and max
      UInt#(16) maxUInt16 = unpack('1);  // all 1's  --- DJG: should this be -1
      UInt#(16) minUInt16 = unpack(0);
      
      $display("maxUInt16 = %x", maxUInt16);
      $display("minUInt16 = %x", minUInt16);

      $display("%x < %x == %x (unsigned)", minUInt16, maxUInt16, minUInt16 < maxUInt16);

      step <= step + 1;
   endrule

   //////////////////////////////////////////////////////////////////////
   rule step1 ( step == 1 );
      $display("== step 1 ==");
      
      // Int#(n) has similar restrictions except that 

      // this is a quick way to get min and max
      Int#(16) maxInt16 = unpack({1'b0,'1});  // 'h011111
      Int#(16) minInt16 = unpack({1'b1,'0});  // 'h100000
      
      $display("maxInt16 = %x", maxInt16);
      $display("minInt16 = %x", minInt16);

      $display("%x < %x == %x (signed)", minInt16, maxInt16, minInt16 < maxInt16);

      // but again, you can't do bit manipulation directly...
      // uncomment these to see the errors they generate
      //
      // $display("error1 = %x", minInt16[4]);

      // but note/remember that in static cases, i.e.
      // where the values are fixed and know at compile time
      // the compiler evalutes... 
      $display("maxInt16/4 = %x", maxInt16 / 4);

      // But note the Bluespec passes / * % directly to verilog
      // if it is a runtime operation.. I.e. we try to divide a
      // register..  So we don't turn / into >> for instance
      // RTL synthesizers know how to deal with this :)

      // look at generated verilog to see the divider
      //     ie. ===>  x__h642 / 16'd4 ;
      int16 <= int16 / 4;

      // so becareful of * / % for the usual reasons
      //    (they may create long timing paths)

      step <= step + 1;
   endrule

   //////////////////////////////////////////////////////////////////////
   rule step2 ( step == 2 );
      $display("== step 2 ==");
      
      // "int" is a special case defined for Int#(32)
      // notice types always start with an Upper case

      // Int#(n)
      // UInt#(n)
      // Bit#(n)
      // Bool#(n)

      Int#(32) bar = 10;

      // except for two cases added for simplicity
      // "int" is predefined to be "Int#(32)"
      // notice I can assign directly from bar 
      int foo = bar;
      $display("foo is 32 bits = %b", foo);


      // this saves a few characters and a # so for
      // quick and dirty data types we can start with
      // int, and then size it properly later..  If we need to..
      
      // Another example is "bit"..  it is predefined to be Bit#(1)
      bit onebit = 1;
      Bit#(1) anotherbit = onebit;

      $display("this is 1 bit = %b", onebit);

      step <= step + 1;
   endrule

   //////////////////////////////////////////////////////////////////////
   rule step3 ( step == 3 );
      $display("== step 3 ==");
      
      // finally the type "Bool" is used for logical operations..
      // it has two values "True" and "False"
      Bool b1 = True;
      Bool b2 = True;
      Bool b3 = b1 && b2;
      
      // We use "Bool" as opposed to "bit" to define true/false
      // logical operation.  VHDL coders will find this pretty natural
      // But most verilog coders right now will be asking
      // "Why can't I just use bit and 0/1"..  Well logically
      // you can, but 0 and 1 are Bit types and like most
      // higher level and strongly typed languages, Bool != bit
      // it also has different operators...
      // Bool => && (and) || (or)  ! (not)
      //
      // NOTE: this means that if statements need to use Bool results
      if ( b1 )
         $display("b1 is True");
      
      // if you are testing a bit, you have to turn that bit into a "Bool"
      // with == (or whatever suitable equation you have)
      bit onebit = 1;
      if (onebit == 1)
         $display("onebit is a 1");         
      
      step <= step + 1;
   endrule


   rule step4 ( step == 4 );
      $display ("=== ===");
      $display ("All done");
      $finish (0);
   endrule

   ////////////////////////////////////////
   // rule watcher (step != 0);
   // endrule

endmodule: mkTb

endpackage: Tb

