// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 11c
// "More interesting parameterized examples
// ================================================================

package Tb;

/*
 
this first module is a "size shifter" it's basically a fifo that
drives data in size X and spits it out size Y in this case I will
simplify the example to say the input is 8*n bytes and the output is 1
byte.  A general purpose N -> M byte size shifter is a great example, 
which requires so thoughtful logic design, and hence is more complex
than necessary here, where I just want to show parameterization.
 
*/

import Vector::*;

interface Sizer#(type tin,type tout);
   method Action             push( tin i );
   method ActionValue#(tout) pop();
endinterface

//////////////////////////////////////////////////////////////////////
// This is essentially a one entry FIFO that sends data in on size
// and out another.  This is simplified to so that the output is
// divisable evenly in to the input size (ie 32 bits in, 8 bits out).
// but you get the provisos idea here :)
module mkSizer( Sizer#(tin,tout) ) provisos (Bits#(tin,sI),
                                             Bits#(tout,sO),
                                             Mul#(sO,depth,sI), // sO = sI * depth
                                             Log#(depth,szDepth),
   
                                             // and the compile told me I needed this one
                                             Bits#(Vector::Vector#(depth, tout), sI)
                                             );  
   
   // save some bits and the counter only needs to be as wide as roof log2 depth
   // you probably want to use a Maybe#() type here, but I'm not just to focus
   // on the basic functionality and parameterization
   Reg#(Bool)          valid <- mkReg(False);
   Reg#(Bit#(szDepth))   ptr <- mkRegU;

   // save the data here
   Reg#(tin) data <- mkRegU;
   
   // write data in when it is empty
   // if this were smarter, we'd make this a real fifo
   // and add two registers (see next example)
   method Action             push( tin n ) if (!valid);
      valid <= True;
      ptr   <= 0;
      data  <= n;
   endmethod
   
   method ActionValue#(tout) pop() if (valid);
      // bump the counter up to size depth
      // this alternating between valid and invalid
      // causes a 1 cycle stall, but you fix that
      // by special casing simultaneous push/pop
      // or by making the a 2 entry fifo
      // "exercise left to the reader"

      // but notice that "depth" itself is a very primative numeric type
      // inorder to compare it to "ptr" we get the "valueof" it first
      // which returns an integer, then we convert from an integer to "whatever"
      if (ptr == fromInteger(valueof(depth)-1))
         valid <= False;   // flag empty when done
      else
         ptr   <= ptr + 1; // else bump counter

      // create a vector for easy accessing
      Vector#(depth,tout) dataVector = unpack(pack(data));

      // you could have played shifting games also
      // but here's another use for pack/unpack
      return dataVector[ptr];
   endmethod
   
endmodule

//////////////////////////////////////////////////////////////////////
// there is one more useful type.  Occasionally,it really is easier
// just to pass a number in to module as a parameter.  We label this
// type "numeric" so the compiler knows it is a normal number (and
// no something else).  This is how we would define a vector length,
// for instance, like in a FIFO.

interface MyFifo#(type td, numeric type depth);
   method Action enq(td i);
   method Action deq();
   method td     first();
   method Action clear();
endinterface

module mkMyFifo( MyFifo#(td, depth) ) provisos (Bits#(td,szTD),           
                                                // we want to be able to count to depth + 1
                                                // i.e. 0 is empty, for 8 entries, #7 is entry 8
                                                Add#(depth,1,depthPlus1), 
                                                Log#(depthPlus1,maxCntr));

   // in this case, I'm wanting to increment but loop at some
   // value (i.e. depth), so for 3, it goes 0,1,2,0,1,2,etc
   // put this in a function (looks like a good think to
   // put in your personal library doesn't it ?)
   function UInt#(n) incMax( UInt#(n) val, UInt#(n) m );
      if (val == (m - 1))
         return 0;
      else
         return val + 1;
   endfunction

   // the fifo is a vector of registers, one for each entry
   Vector#(depth,Reg#(td)) fifo  <- replicateM(mkRegU);
   
   // you could create a manually sized type also
   // what you would want to do with it is up to you :)
   // Bit#(depth) foo <- mkReg(0); 

   // manually track the read and write pointers
   Reg#(UInt#(maxCntr))     wptr <- mkReg(0);
   Reg#(UInt#(maxCntr))     rptr <- mkReg(0);

   // get the depth counter as a UInt#(( for incrementing
   UInt#(maxCntr) maxD = fromInteger(valueof(depth));
   
   // empty is easy enough
   Bool empty = wptr == rptr;

   // but full we just manually check to two pointer cases we know of
   // I just did this rather an keep an actual counter for a change
   Bool full  = ((wptr+1) == rptr) || (((wptr+1) == maxD) && (rptr == 0));

   // enque whatever data (implicit condition: not full)
   method Action enq(td i) if ( !full );
      wptr       <= incMax( wptr, maxD );
      fifo[wptr] <= i;
   endmethod
   
   // implicit condition is !empty
   method Action deq() if ( !empty );
      rptr       <= incMax( rptr, maxD );
   endmethod

   method td     first() if ( !empty );
      return fifo[rptr];
   endmethod

   method Action clear();
      wptr <= 0;
      rptr <= 0;
   endmethod
      
endmodule
   
//////////////////////////////////////////////////////////////////////
import StmtFSM::*;

(* synthesize *)
module mkTb (Empty);

   // instantiate all you want with the sizes that match the privisos
   Sizer#(UInt#(32),UInt#(4)) tx  <- mkSizer;
   Sizer#(UInt#(16),UInt#(8)) tx2 <- mkSizer;

   // but this one causes a compile error because it's not sized properly
   // Sizer#(UInt#(16),UInt#(7)) tx3 <- mkSizer;
   
   // lets try the fifo also
   MyFifo#(int,3) tfifo <- mkMyFifo;

   // see section 12 for more detail on StmtFSM sequences
   Stmt test =
   seq
      // test Sizer first
      tx.push( 'h12345678 );
      tx.push( 'h9abcdef0 );
      repeat(10) noAction;
      
      tx2.push( 'h1234 );
      tx2.push( 'h5678 );
      repeat(10) noAction;

      // TODO: add more cases
      
      // try the fifo out
      par
         seq
            tfifo.enq( 'h10 );
            tfifo.enq( 'h11 );
            tfifo.enq( 'h12 );
            tfifo.enq( 'h13 );
            tfifo.enq( 'h14 );
         endseq
         
         seq
            repeat(5) noAction;
            action $display("fifo out is %x", tfifo.first()); tfifo.deq(); endaction
            action $display("fifo out is %x", tfifo.first()); tfifo.deq(); endaction
            noAction;
            noAction;
            action $display("fifo out is %x", tfifo.first()); tfifo.deq(); endaction
            action $display("fifo out is %x", tfifo.first()); tfifo.deq(); endaction
            action $display("fifo out is %x", tfifo.first()); tfifo.deq(); endaction
         endseq
      endpar
      
   endseq;
   
   mkAutoFSM( test );

   rule watchout1;
      let res <- tx.pop();
      $display("sizer output = %02x", res);
   endrule

   rule watchout2;
      let res <- tx2.pop();
      $display("sizer2 output = %02x", res);
   endrule

endmodule
   
endpackage
