// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 4d
// "RWires"
// Same example (up-down counter)
// showing use of DWires instead of RWires or Wires.
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package Tb;

// ----------------------------------------------------------------
// A top-level module connecting the stimulus generator to the DUT

(* synthesize *)
module mkTb (Empty);

   Counter c1 <- mkCounter_v1;    // Instantiate a version 1 counter
   Counter c2 <- mkCounter_v2;    // Instantiate a version 2 counter (same interface type)

   // ----------------
   // Step 'state' from 1 to 10 and show both counter values in each state

   Reg#(int) state <- mkReg (0);

   rule step_state;
      if (state > 9) $finish (0);
      state <= state + 1;
   endrule

   rule show;
      $display ("In state %0d, counter values are %0d, %0d", state, c1.read (), c2.read ());
   endrule

   // ----------------
   // For counter c1, increment by 5 in states 0..6, decrement by 2 in states 4..10
   // The interesting states are 4..6

   rule incr1 (state < 7);
      c1.increment (5);
   endrule

   rule decr1 (state > 3);
      c1.decrement (2);
   endrule

   // ----------------
   // Same for counter c2 (increment by 5 in states 0..6, decrement by 2 in states 4..10)
   // The interesting states are 4..6

   rule incr2 (state < 7);
      c2.increment (5);
   endrule

   rule decr2 (state > 3);
      c2.decrement (2);
   endrule

endmodule: mkTb

// ----------------------------------------------------------------
// Interface to an up-down counter

interface Counter;
   method int read();                     // Read the counter's value
   method Action increment (int di);      // Step the counter up by di
   method Action decrement (int dd);      // Step the counter down by dd
endinterface: Counter

// ----------------------------------------------------------------
// Version 1 of the counter

(* synthesize *)
module mkCounter_v1 (Counter);
   Reg#(int) value1 <- mkReg(0);  // holding the counter's value

   method int read();
      return value1;
   endmethod

   method Action increment (int di);
      value1 <= value1 + di;
   endmethod

   method Action decrement (int dd);
      value1 <= value1 - dd;
   endmethod

endmodule: mkCounter_v1

// ----------------------------------------------------------------
// Version 2 of the counter

(* synthesize *)
module mkCounter_v2 (Counter);
   Reg#(int) value2 <- mkReg(0);  // holding the counter's value

   Wire#(int) w_incr <- mkDWire (0);  // Signal that increment method is being invoked
   Wire#(int) w_decr <- mkDWire (0);  // Signal that decrement method is being invoked

   // ---------------- RULES

   // This rule does all the work.
   // If the increment method is being invoked, w_incr carries the increment value, else it carries 0
   // If the decrement method is being invoked, w_decr carries the decrement value, else it carries 0

   (* fire_when_enabled, no_implicit_conditions *)
   rule r_incr_and_decr;
      value2 <= value2 + w_incr - w_decr;
   endrule

   // ---------------- METHODS

   method int read();
      return value2;
   endmethod

   method Action increment (int di);
      w_incr <= di;
   endmethod

   method Action decrement (int dd);
      w_decr <= dd;
   endmethod

endmodule: mkCounter_v2

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

Most of this example is identical to Examples 4d, except for the style
in which we have written module mkCounter_v2.  Here, we have used the
the 'mkDWire' constructor for Wires, instead of 'mkWire'

The 'mkDWire' constructor also creates a module with a Wire#()
interface (which is a synonym for the Reg#() interface).  However,
it's '_read()' method is always enabled (whereas the Wire produced by
'mkWire' has an implicit condition on the _read() method).  Here, if
the Wire is being assigned earlier in the current clock cycle, the
_read() method returns the value assigned.  However, if the Wire is
not being assigned, the _read() method returns a default value, which
is provided as an parameter to the 'mkDWire()' module constructor.

We declare our Wires now as follows:

       Wire#(int) w_incr <- mkDWire (0);  // Signal that increment method is being invoked
       Wire#(int) w_decr <- mkDWire (0);  // Signal that decrement method is being invoked

Notice that the default value on both wires is 0.

We now need only one rule to capture the desired behavior:

       (* fire_when_enabled, no_implicit_conditions *)
       rule r_incr_and_decr;
          value2 <= value2 + w_incr - w_decr;
       endrule

This rule will fire on every clock (the 'fire_when_enabled' and
'no_implicit_conditions' guarantees that).  Whenever the 'increment()'
is invoked, 'w_incr' will carry the increment value, else it'll carry
0.  Whenever the 'decrement()' is invoked, 'w_decr' will carry the
decrement value, else it'll carry 0.  Thus, on every clock, we add and
subtract the appropriate value from 'value2'.

When we execute the code, the displayed outputs indeed show the same
behavior as the examples 4a, 4b and 4d.

* ================================================================
*/
