// Copyright 2008 Bluespec, Inc.  All rights reserved.

// section 8 e - array of registers

package Tb;

import Vector::*;

(* synthesize *)
module mkTb (Empty);

   // as you have seen before, you can create an array of any module
   // including interfaces, etc.. Registers in bluespec, as you know,
   // are just another module with two methods (_write and _read).

   // I like the one line vector way of defining an array of registers
   // NOTE: if you accidentally use "replicate" rather than "replicateM"
   //  (notice the "M" - which I think of as "replicate module" in this case)
   //  you will get the following error message.
   //
   // Error: "Tb.bsv", line 20, column 35: (T0107)
   //   The type `Vector::Vector#(12)' is not a module. Perhaps you have used the
   //   wrong function in a module instantiation, such as `map' vs `mapM'.
   //   Consider the modules at the following locations:
   //     "Tb.bsv", line 20, column 46
   //
   // we will see map and mapM in later examples.. For now just remember
   //    ....... <- replicateM( )   // notice <-
   //    ....... =  replicate( )    // notice =
   Vector#(12, Reg#(int)) arr1 <- replicateM( mkReg(0) );
   
   // but you can certainly use the array format
   Reg#(int) arr2[12];
   for (Integer i=0; i<12; i=i+1)
      arr2[i] <- mkReg(0);

   // create a counter for a few simple steps
   Reg#(int)      step <- mkReg(0);
   Reg#(Int#(5))  inx <- mkReg(0);

   rule test0 ( step == 0 );
      // you can assign all or some of the registers
      arr1[1] <= 10;
      arr1[3] <= 20;
      arr1[7] <= 30;

      // same thing for arr2
      step <= step + 1;
   endrule
   
   rule test1( step == 1 );
      // you might like to print out the array of values ?
      // or you might just look at waveforms
      $display("== step 1==");
      for (Integer i=0; i<12; i=i+1)
         $display("arr1[%d] = %x", i, arr1[i]);
      for (Integer i=0; i<12; i=i+1)
         $display("arr2[%d] = %x", i, arr2[i]);

      // let's set them all to some new values
      for (Integer i=0; i<12; i=i+1)
         arr1[i] <= fromInteger(i);

      for (Integer i=0; i<12; i=i+1)
         arr2[i] <= fromInteger(2*i);

      step <= step + 1;
   endrule

   rule test2( step == 2 );
      $display("== step 2==");
      for (Integer i=0; i<12; i=i+1)
         $display("arr1[%d] = %x", i, arr1[i]);
      for (Integer i=0; i<12; i=i+1)
         $display("arr2[%d] = %x", i, arr2[i]._read());
      
      // notice that this is "static" accessing to the arrays
      // since we are indexing with Integer.
      // But we can certainly use a register variable,
      // and this will create all the muxing needed to
      // write one or read one register from the array at a time
      inx <= 0;
      $display("== step 3==");
      step <= step + 1;
   endrule

   rule test3( step == 3 );
      $display("arr1[%d] => %x at time ", inx, arr1[inx], $time);

      // note that we can use Reg#() write method instead of <=
      // this is what the compiler is desugaring to anyway
      arr1[inx]._write( extend(inx+3) );

      inx <= inx + 1;

      if (inx == 11)
         step <= step + 1;
   endrule

   rule test4( step == 4 );
      $display("== step 4==");
      for (Integer i=0; i<12; i=i+1)
         $display("arr1[%d] = %x", i, arr1[i]);
      $display ("All done");
      $finish (0);
   endrule

endmodule: mkTb

endpackage: Tb

