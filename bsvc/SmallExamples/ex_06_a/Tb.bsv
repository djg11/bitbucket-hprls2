// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 6a
// "Variables, assignments and combinational circuits"
// Variable declaration and initialization, including:
//  - declaring a variable with its type, and initializing it
//  - declaring and initializing a variable using 'let' (inferring its type)
//  - variable declaration and initialization with '='
//  - interface variable declaration and initialization with '<-' (module instantiation)
//  - variable declaration and initialization with '<-' (ActionValue invocation)
// ================================================================

package Tb;

import FIFO::*;
import GetPut::*;
import ClientServer::*;

// ----------------------------------------------------------------
// This is a made-up example to demonstrate variable declaration and
// initialization syntax, so the exact functionality does not matter.

// ----------------------------------------------------------------
// A struct representing a 2-dimensional 'coordinate'

typedef struct { int x; int y; } Coord
    deriving (Bits);

// ----------------------------------------------------------------
// An interface, and a module with that interface

module mkTransformer (Server#(Coord, Coord));
   FIFO#(Coord) fi <- mkFIFO;
   FIFO#(Coord) fo <- mkFIFO;

   Coord delta1 = Coord { x: 10, y: 20 };
   let   delta2 = Coord { x: 5,  y: 8 };

   rule transform;
      Coord c = fi.first();  fi.deq();
      c.x = c.x + delta1.x + delta2.x;
      c.y = c.y + delta1.y + delta2.y;
      fo.enq (c);
   endrule

   interface request = fifoToPut (fi);
   interface response = fifoToGet (fo);

endmodule: mkTransformer

// ----------------------------------------------------------------

(* synthesize *)
module mkTb (Empty);

   Reg#(int) cycle <- mkReg (0);

   Reg#(int) rx <- mkReg (0);
   Reg#(int) ry <- mkReg (0);

   Server#(Coord, Coord) s <- mkTransformer;

   rule count_cycles;
      cycle <= cycle + 1;
      if (cycle > 7) $finish(0);
   endrule

   rule source;
      let c = Coord { x: rx,  y: ry };
      s.request.put (c);
      rx <= rx + 1;
      ry <= ry + 1;
      $display ("%0d: rule source, sending Coord { x: %0d, y: %0d }", cycle, rx, ry);
   endrule

   rule sink;
      let c <- s.response.get ();
      $display ("%0d: rule sink, returned value is Coord { x: %0d, y: %0d }", cycle, c.x, c.y);
   endrule

endmodule: mkTb

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

This is a made-up example to demonstrate variable declaration and
initialization syntax, so the exact functionality does not matter.  A
testbench (mkTb) generates some 2-dimensional (x,y) coordinate values
and sends it into a DUT (mkTransformer) which performs some
transformation on the coordinates and returns the transformed
coordinates to the testbench, which prints them.  The emphasis in this
example is on the various ways in which variables are declared and
initialized.

There are two kinds of initializations:

- "pure" or "value" initializations using '='
    Here, the value of the right-hand side is bound to the variable on
    the left-hand side.

- "side-effecting" initializations using '<-'

    Here, the right-hand side is an expression that has a side effect
    and also returns a value.  The returned value is bound to the
    variable on the left-hand side.

    There are two kinds of side effects:
       - The side-effect of instantiating a module and returning its interface
       - The side-effect of invoking an ActionValue and returning its value

All these are demonstrated in this example.

----------------
Consider the following statement:

       FIFO#(Coord) fi <- mkFIFO;

On the left-hand side we specify a type 'FIFO#(Coord)' and a variable
'fi' of that type.  On the right-hand side we invoke a module
instantiation 'mkFIFO', and the returned FIFO interface is bound to
the variable 'fi'.  Note that the expression 'mkFIFO' here by itself
has type:

    Module#(FIFO#(Coord))

When used on the right-hand side of an '<-' statement, it instantiates
the module and returns a value of the interface type, namely
'FIFO#(Coord)'.

----------------
Consider the following statement:

       Coord delta1 = Coord { x: 10, y: 20 };

On the left-hand side we specify a type 'Coord' and a variable
'delta1' of that type.  The right-hand side is a so-called 'struct
expression' that evaluates to a struct value of type Coord, and this
value is assigned to the variable 'delta1'.

Consider the next statement

       let delta2 = Coord { x: 5, y: 8 };

Here, we've used the 'let' shorthand, leaving it up to the compiler to
infer that 'delta2' has type 'Coord', based on the expression on the
right-hand side. This shorthand is especially useful when the type is
more complicated, but should be used with care, since it reduces
readability.  In this example line it does not matter--the right-hand
side being a 'Coord' struct expression, the type is obvious to any
reader.

----------------
Consider the following statement:

          Coord c = fi.first();

On the left-hand side we specify a type 'Coord' and a variable 'c' of
that type.  The right-hand side invokes the value method 'fi.first()'
whose result is of type 'Coord' and is assigned to 'c'.  Note that
'fi.first()' is a value method, not and ActionValue method, i.e., it
has no side-effect, hence it is used in a '=' statement, not a '<-'
statement.

This line could have been written as follows as well:

          let c = fi.first();

----------------
Consider the following statement:

       interface request = fifoToPut (fi);

This is special syntax for initializing sub-interface components.
'request' is the name of a sub-interface of the overall Server#() type
for the module.  This sub-interface has type Put#().  The right-hand
side of the '=' is indeed an expression of this type.

----------------
Consider the following statement:

       Reg#(int) cycle <- mkReg (0);

As before, we specify a type 'Reg#(int)' and a variable 'cycle' of
that type, and we initialize it by invoking the side-effect of
instantiating the module using 'mkReg(0)', and binding the resulting
interface to the variable 'cycle'.

----------------
Consider the following statement:

       Server#(Coord, Coord) s <- mkTransformer;

As before, we specify a type 'Server#(Coord, Coord)' and a variable
's' of that type, and we initialize it by invoking the side-effect of
instantiating the module using 'mkTransformer', and binding the
resulting interface to the variable 's'.

----------------
Consider the following statement

          let c = Coord { x: rx,  y: ry };

Here, again, we've used the 'let' shorthand, leaving it up to the
compiler to infer that 'c' has type 'Coord', based on the expression
on the right-hand side.

----------------
Consider the following statement

          let c <- s.response.get ();

Here we've used 'let' in a '<-' statement.  The right-hand side
expression 's.response.get ()' has type 'ActionValue#(Coord)'.  By
using it in a '<-' statemement we invoke the ActionValue's side
effect, and the returned value, of type 'Coord', is bound to the
variable 'c'.  By using 'let', we allow the compiler to infer the type
of 'c', which is 'Coord'.

* ================================================================
*/
