// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 12g
// "FSMs"
// Building your own FSM generator: FSMfromList
// Generates an FSM from a list of (current-state, action, next-state) triples
// which describe the desired state transitions.  This is an alternative
// way of specifying FSMs to the built-in StmtFSM sub-language, and
// can describe more "unstructured" FSMSs.
// ================================================================

package Tb;

import List::*;
import StmtFSM::*;

import FSMfromList::*;

// ----------------------------------------------------------------
// EXAMPLE
// State transitions:
//    S0 -> S1
//    S1 -> either S2 or S4                  (i.e., a conditional branch)
//    S2 -> S2 -> S2 -> S2 -> S2 -> S3       (i.e., a loop)
//    S3 -> S4
//    S4                                     (final state)

typedef enum { S0, S1, S2, S3, S4 } ExampleState
        deriving (Bits, Eq);

(* synthesize *)
module mkTb (Empty);

   Reg#(int) cycle <- mkReg(0);

   Reg#(int) loopcount <- mkRegU;

   // This is the FSM specification-- list of 3-tuples describing the state transitions
   List#(Tuple3#(ExampleState, Action, ExampleState))
       xyzs =    cons (tuple3 (S0, $display ("%d: In S0", cycle), S1),
                 cons (tuple3 (S1,
                               action
                                  $display ("%d: In S1", cycle); loopcount <= 4;
                               endaction,
                               ((cycle < 4) ? S2 : S4)),
                 cons (tuple3 (S2,
                               action
                                  $display ("%d: In S2", cycle); loopcount <= loopcount - 1;
                               endaction,
                               ((loopcount == 0) ? S3 : S2)),
                 cons (tuple3 (S3, $display ("%d: In S3", cycle), S4),
                 cons (tuple3 (S4,
                               action
                                  $display ("%d: In S4", cycle); $finish(0);
                               endaction,
                               ?),
                 tagged Nil)))));

   // Build the FSM module
   FSM fsm <- mkFSMfromList (xyzs);

   // This rule starts the FSM (repeatedly, but the note that the FSM quits in state S4)
   rule r1;
      $write ("%d: rule r1", cycle);
      fsm.start;
   endrule

   rule cycle_count;
      cycle <= cycle + 1;
   endrule

endmodule: mkTb

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

This example shows an FSM specificied according to the list expected
by mkFSMfromList.  It illustrates unconditional jumps, a conditional
jump, and a loop.

The following definition:

    typedef enum { S0, S1, S2, S3, S4 } ExampleState ...

defines the state space for the FSM.

The following list definition:

       List#(Tuple3#(ExampleState, Action, ExampleState))
           xyzs =    cons (tuple3 (S0, $display ("%d: In S0", cycle), S1),
                     ...

specifies the list of 3-tuples that specify the FSM.  Note that in a
line such as this:

                                   action
                                      $display ("%d: In S1", cycle); loopcount <= 4;
                                   endaction,

we compose multiple actions to be performed in a particular state.  In
a line such as this:

                                   ((cycle < 4) ? S2 : S4)),

we specify the next state conditionally, i.e., we either go to state
S2 or to S4 depending on the condition.  This, in turn, allows us to
define loops with loop-termination conditions, as shown in state S2.

In state S4, the next state is specified as '?', i.e., a don't care,
because it is the final state.

This line:

       FSM fsm <- mkFSMfromList (xyzs);

invokes our module constructor mkFSMfromList to create the FSM.  The
subsequent tule starts the FSM by calling its 'start' method.

When compiled and executed, we get output like this, as expected:

          0: rule r1          0: In S0
          1: In S1
          2: In S2
          3: In S2
          4: In S2
          5: In S2
          6: In S2
          7: In S3
          8: In S4

* ================================================================
*/
