// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 12g
// "FSMs"
// Building your own FSM generator: FSMfrom_list
// Generates an FSM from a list of (current-state, action, next-state) triples
// which describe the desired state transitions.  This is an alternative
// way of specifying FSMs to the built-in StmtFSM sub-language, and
// can describe more "unstructured" FSMSs.
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package FSMfromList;

import List::*;
import StmtFSM::*;

// ----------------------------------------------------------------
// The parameter 'transitions' is a list of 3-tuples
// Each 3-tuple specifies a state, an action to be performed in the state, and the next state
// The first 3-tuple represents the initial state
// The last  3-tuple represents the final state
// It provides an FSM interface (see Reference Guide for FSM chapter and StmtFSM library)

module mkFSMfromList #(List#(Tuple3#(state_t,Action,state_t))  transitions)
                     (FSM)
                     provisos (Bits#(state_t, ns), Eq#(state_t));

   state_t  s0  = tpl_1 (head (transitions));    // The initial state
   Action   a0  = tpl_2 (head (transitions));    // The initial action

   state_t  s1  = tpl_3 (head (transitions));    // The second state

   state_t  sN  = tpl_1 (last (transitions));    // The final state

   Reg#(state_t) state <- mkReg (s0);
   Reg#(Bool)    busy  <- mkReg (False);

   // ----------------
   // RULES

   // Generate all the rules for the FSM
   for (List#(Tuple3#(state_t,Action,state_t)) ts = transitions;
        ! (isNull (ts));
        ts = tail (ts)) begin

      let { curr_s, action_s, next_s } = head (ts);    // pattern-match the current 3-tuple
      rule foo (busy && (state == curr_s));
         action_s;
         state <= next_s;
         busy <= (curr_s != sN);
      endrule
   end

   // ----------------
   // METHODS

   method Action start () if (!busy);
      a0;              // Do the first state action
      state <= s1;     // Go to the second state
      busy  <= True;
   endmethod

   method Bool done ();
      return (! busy);
   endmethod

   method Action waitTillDone () if (! busy);
   endmethod

endmodule: mkFSMfromList

endpackage: FSMfromList

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

The mkFSMfromList module is paramaterized by a specification of an
FSM.  The module implements that FSM, and provides an FSM interface to
that FSM.

The FSM specification is a list of 3-tuples, where each 3-tuple
specifies a state, an action to be performed in that state, and the
next state to transition to.

The module is polymorphic in that it does not care about the type
'state_t' in which states are represented, other than it should be
storable in a register (hence the 'Bits' proviso) and that one can
test states for equality (hence the 'Eq' proviso).

Note that the 'next state' can be specified with any expression of
type state_t.  This includes conditional expressions.  Thus, one can
express if-then-else structures, loops, and indeed arbitrary
conditional and unconditional jumps.

The first set of bindings, such as:

       state_t  s0  = tpl_1 (head (transitions));    // The initial state

just extract certain elements of the FSM specification.  Here, it
takes the head of the transitions list (a 3-tuple), and then the first
element of that 3-tuple, i.e., it is is the initial state.

In this example, we instantiate two up-down counters. They have
exactly the same interface type, and they are called from the
testbench, mkTb, in identical ways from apparently identical rules.
However, their implementations result in different scheduling
behaviors, illustrating the use and scheduling properties of RWires.

The next two declarations:

   Reg#(state_t) state <- mkReg (s0);
   Reg#(Bool)    busy  <- mkReg (False);

just instantiate the register that keeps track of current state, and a
'busy' flag.

The for-loop:

       for (List#(Tuple3#(state_t,Action,state_t)) ts = transitions;

iterates over the FSM spec and generates a rule for each state.
Specifically, in the for-loop, we have:

          rule foo (busy && (state == curr_s));
             action_s;
             state <= next_s;
             busy <= (curr_s != sN);
          endrule

which fires when the current state is curr_s, performs action
action_s, transitions the state to next_s, and updates the busy flag
according to whether we're in the final state or not.

After this, we define the standard methods for the FSM interface.  The
'start' method can be invoked as long as the FSM is not busy.  It sets
the busy flag, does the first action and jumps to the second state.
This is just a design choice.  We could have just set the busy flag
and jumped to the initial state, in which case the FSM would start
operating in the next cycle.

The remaining two methods are straightforward.

* ================================================================
*/
