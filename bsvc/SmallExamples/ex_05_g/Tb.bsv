// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 5g
// "Scheduling"
// Attributes on methods: always_ready, always_enabled
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package Tb;

// ================================================================

import PipelinedSyncROM_model::*;

// A synthesizable instance of the polymorphic pipelined sync ROM

typedef UInt#(16) Addr;
typedef UInt#(24) Data;

// TODO djg commented out this separate comp flag: (* synthesize *)

(* always_ready = "request, response" *)
(* always_enabled = "request" *)

module mkPipelinedSyncROM_model_specific (PipelinedSyncROM#(Addr,Data));
   let m <- mkPipelinedSyncROM_model ("mem_init.data", 0, 31);
   return m;
   //error("stop here src file pragma");
endmodule

// ----------------------------------------------------------------
// A top-level module that just reads some data from the pipelined ROM

(* synthesize *)
module mkTb (Empty);

   PipelinedSyncROM#(Addr, Data) toprom <- mkPipelinedSyncROM_model_specific ();

   Reg#(Addr) ra <- mkReg (0);
   Reg#(int) cycle <- mkReg (0);

   // ----------------
   // Step 'address' and feed to the ROM

   rule step_addr;
      toprom.request (ra);
      if (ra > 15) $finish (0);
      ra <= ra + 1;
      cycle <= cycle + 1;
      $display ("%0d: Sending address %0h", cycle, ra);
   endrule

   rule show_data;
      $display ("%0d: Data returned is %0h", cycle, toprom.response());
   endrule

endmodule: mkTb

// ================================================================

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

The module mkPipelinedSyncROM_model is parameterized, and so cannot be
synthesized into a separate Verilog module.  The first few lines of
this file:

    (* synthesize *)
    (* always_ready = "request, response" *)
    (* always_enabled = "request" *)
    module mkPipelinedSyncROM_model_specific (PipelinedSyncROM#(Addr,Data));
       ...

make a specific instance of that module, fixing the parameters, and
this specific instance can therefore be synthesized into a separate
Verilog module.  The attributes:

    (* always_ready = "request, response" *)
    (* always_enabled = "request" *)

specify that both 'request' and 'response' methods are always ready,
and that the 'request' Action method is always enabled.  The compiler
will check that these assertions are indeed true, and then, in the
generated verilog, will omit the usual READY and ENABLE signals.

The details of the testbench mkTb are not important; it just sends in
some addresses and prints out data responses.

The model can be compiled and run both with Bluesim and Verilog sim.
When generating Verilog, observe the file

    mkPipelinedSyncROM_model_specific.v

You will see that the module header is:

    module mkPipelinedSyncROM_model_specific(CLK,
                                             RST_N,
                                             request_a,
                                             response);

i.e., there are no READY and ENABLE signals in the port list.

================================================================
Exercise:

- Remove the 'always_ready' and 'always_enabled' attributes, recompile
  to Verilog, and observe that in the Verilog ports we now have READY
  and ENABLE signals for the methods

* ================================================================
*/
