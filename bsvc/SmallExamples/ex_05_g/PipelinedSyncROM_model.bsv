// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 5g
// "Scheduling"
// Attributes on methods: always_ready, always_enabled
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package PipelinedSyncROM_model;

import RegFile::*;    // We'll use a register file for the ROM model

// ----------------------------------------------------------------
// This module is a model of a pipelined, synchronous ROM with a 3-cycle latency.
// It accepts an address and delivers data on every cycle.
// If an address arrives on cycle J, the data is delivered on cycle J+3
// Note: there is no handshaking, no flow control: addr and data are transferred
// on every clock (hence the 'sync' in the name).

// Note: this is just a model; in real life, the module would
// eventually be substituted by an actual ROM implementation,
// typically created by a 3rd-party "memory generator"

// ----------------
// The Pipelined synchronous ROM interface

interface PipelinedSyncROM #(type addr3p, type data3p);
   method Action request  (addr3p a);
   method data3p   response ();
endinterface

module mkPipelinedSyncROM_model#(String file_init_contents, addr_tt a1, addr_tt a2) (PipelinedSyncROM #(addr_tt, data4p))
    provisos (Bits#(addr_tt), Bits#(data4p));

   RegFileRO#(addr_tt, data4p) rf <- mkRegFileLoad1 (file_init_contents, a1, a2);

   // Address shift register, to model the 3-cycle latency
   Reg#(addr_tt) ra1 <- mkReg(a1);
   Reg#(addr_tt) ra2 <- mkReg(a1);
   Reg#(addr_tt) ra3 <- mkReg(a1);

   method Action request (addr_tt a);
      // Shift addresses
      ra1 <= a;
      ra2 <= ra1;
      ra3 <= ra2;
   endmethod

   method data4p response ();
      return rf.sub (ra3);
   endmethod

endmodule: mkPipelinedSyncROM_model

endpackage: PipelinedSyncROM_model

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

The interface to the ROM:

    interface PipelinedSyncROM #(type addr, type data);

is parameterized by address and data types.

The module parameters:

       #(String file_init_contents, addr a1, addr a2)

specify a file with hexadecimal data to initialize the memory
contents, and the start and end address for the memory.

The provisos:

        provisos (Bits#(addr, na), Bits#(data, nd));

are necessary because we will use the RegFile library (register files)
to model the memory, and RegFile requires that the address and data
types are reprsentable in Bits, i.e., part of the Bits typeclass.

This line:

       RegFile#(addr,data) rf <- mkRegFileLoad (file_init_contents, a1, a2);

implements the memory model and initializes the contents from the
file.

The next three lines:

       // Address shift register, to model the 3-cycle latency
       Reg#(addr) ra1 <- mkReg(a1);
       ...

implement a "delay line" shift register to model the desired 3-cycle
latency (since the RegFile itself has no latency).

The method 'request' just accepts an address and shifts it into the
address shift-register.

The method 'response' just returns the data according to the address
in ra3.

================================================================
Exercise:

- Parameterize the model by ROM latency, i.e., the ROM's latency
  should be programmable

* ================================================================
*/
