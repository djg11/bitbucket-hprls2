// Copyright 2008 Bluespec, Inc.  All rights reserved.

// section 8 h - Vector of sub interfaces

package Tb;

import Vector::*;
import FIFO::*;

// Vector works on all sorts of types..  It happens not only can you
// make a vector modules (reg, fifo, or *any* module), you can make a
// also make a vector of sub interfaces (interfaces under an interfce).

// make a vector of interfaces using Reg (as you know, it's an interface :)
interface Block;
   interface Vector#(2,Reg#(int)) regs;

   // you can still have methods next to sub interfaces
   method    Action clear();
endinterface

//////////////////////////////////////////////////////////////////////
module mkBlock1( Block );

   Vector#(2,Reg#(int)) internalRegs <- replicateM( mkRegU );

   // For this simple case, the type of the interface is the same as
   // the internal type so, we can directly assign (really this is equate).
   // This essentially gives the parent direct access to the modules interface 
   // methods below.   Notice also the slightly different syntax.
   // We say "interface" name, and then set it to a vector of interfaces
   // that defines the methods for that interface
   interface regs  = internalRegs;

   // just allow compile to continue, fill this in later
   method    Action clear() = ?;
      
endmodule

//////////////////////////////////////////////////////////////////////
module mkBlock2( Block );

   Vector#(2,Reg#(int)) internalRegs <- replicateM( mkRegU );
   Vector#(2,Reg#(int)) ifc = newVector;

   // rules in the parent may be accesing these registers
   // AND we may want to access internally also..
   
   // parent has higher urgency over child firing
   // We won't discuss this here, but just beware.
   // Two rules accessing the same resource have to
   // determine urgency and execution order (as all rules do)
   // I added this line mainly to show that this is just
   // a register inside this block.
   //
   // and notice the warning that comes out:
   //
   // Warning: "Tb.bsv", line 99, column 8: (G0010)
   //   Rule "step1" was treated as more urgent than "b2_setreg". Conflicts:
   //     "step1" cannot fire before "b2_setreg":
   //       calls to b2_internalRegs.write vs. b2_internalRegs.read
   //     "b2_setreg" cannot fire before "step1":
   //       calls to b2_internalRegs.write vs. b2_internalRegs.read

   rule setreg;
      internalRegs[0] <= internalRegs[0] + 1;
   endrule

   //////////////////////////////////////////////////////////////////////
   // explicitly define each method for each of the indecies for the interface
   ifc[0] = interface Reg;
               // when the interfaces are the same, = can be handy
               method Action _write(int x) = internalRegs[0]._write(x);
               method int    _read()       = internalRegs[0]._read();
            endinterface;

   ifc[1] = interface Reg;
               // but more proper and regular is the standard form
               method Action _write(int x);
                  internalRegs[1]._write(x);
               endmethod
               method int    _read();
                  return internalRegs[1]._read();
               endmethod
            endinterface;

   interface regs = ifc;

   method    Action clear() = ?;

endmodule
//////////////////////////////////////////////////////////////////////
// and you can of course have sub interfaces inside subinterfaces
// (as deep as you can handle).  We use the '.' notation to navigate
// down the sub inte
interface SubBlock;
   interface Vector#(3, Block) sb;
endinterface

module mkSubBlock( SubBlock );
   Vector#(3,Block) blocks <- replicateM( mkBlock1 );
   interface sb = blocks;
endmodule
      

//////////////////////////////////////////////////////////////////////
(* synthesize *)
module mkTb (Empty);

   Reg#(int) step <- mkReg(0);

   Block b1 <- mkBlock1;
   Block b2 <- mkBlock2;
   SubBlock b3 <- mkSubBlock;

   Reg#(int) val <- mkReg(10);

   ////////////////////////////////////////
   rule init ( step == 0 );
      // Block1 is Vector#(2, Reg#(int)) (as the inteface Block1 says)
      // You access this sub interface via it's variable name "regs" in this case
      // and "regs" is a vector, so you index that

      // parens added for clarity
      (b1.regs[0])._write( val );
      
      // but registers have the <= syntax, so you can use that also
      b1.regs[1] <= val * 2;

      b2.regs[0] <= val * 3;
      b2.regs[1] <= val * 4;

      // wow.. getting deep.. so
      // b3 = SubBlock => Vector#(3,Block) sb;
      // sb = Block    => Vector#(2,Reg#(int)) regs;
      // regs => methods _write and _read
      
      b3.sb[0].regs[0] <= val * 5;
      b3.sb[0].regs[1] <= val * 6;
      b3.sb[1].regs[0] <= val * 7;
      b3.sb[1].regs[1] <= val * 8;
      b3.sb[2].regs[0] <= val * 9;
      b3.sb[2].regs[1] <= val * 10;

      step <= step + 1;
   endrule

   ////////////////////////////////////////   
   rule step1 ( step == 1 );
      // and I can read them back (Again, _read added for emphasis)
      b2.regs[0] <= b2.regs[1];
      b2.regs[1] <= b2.regs[0];

      // might as well use those for loops!
      for (Integer i=0; i<3; i=i+1)
         for (Integer j=0; j<2; j=j+1)
            b3.sb[i].regs[j] <= b3.sb[i].regs[j] * 2;

      step <= step + 1;
   endrule

   ////////////////////////////////////////
   rule step2 ( step == 2 );
      // parent not loaded b2.regs[0] so rule "setreg" can fire

      for (Integer i=0; i<3; i=i+1)
         for (Integer j=0; j<2; j=j+1)
            b3.sb[i].regs[j] <= b3.sb[i].regs[j] * 3;

      step <= step + 1;
   endrule

   ////////////////////////////////////////
   rule step3 ( step == 3 );
      $display ("All done");
      $finish (0);
   endrule

   ////////////////////////////////////////
   rule watcher (step != 0);
      $display("=== step %d ===", step);
      $display("b1.regs[0] = ", b1.regs[0]._read() );
      $display("b1.regs[1] = ", b1.regs[1]._read() );
      $display("b2.regs[0] = ", b2.regs[0]._read() );
      $display("b2.regs[1] = ", b2.regs[1]._read() );

      // might as well use those for loops
      for (Integer i=0; i<3; i=i+1)
         for (Integer j=0; j<2; j=j+1)
            $display("b3.sb[%d].regs[%d] = ", i,j,b3.sb[i].regs[j]);
   endrule

endmodule: mkTb

endpackage: Tb

