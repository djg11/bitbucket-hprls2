// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 5a
// Scheduling error due to bad parallel composition (methods in same rule/method) 
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package Tb;
    
// importing the FIFO library package    
import FIFO ::*;    

// ----------------------------------------------------------------
// A top-level module instantiating a fifo

(* synthesize *)
module mkTb1 (Empty);

   FIFO#(int) f <- mkFIFO;    // Instantiate a fifo

   // ----------------
   // Step 'state' from 1 to 10

   Reg#(int) state <- mkReg (0);

   rule step_state;
      if (state > 9) $finish (0);
      state <= state + 1;
   endrule

   // ----------------
   // Enqueue two values to the fifo from the same rule
   // 

   rule enq (state < 7);
      f.enq(state);
      f.enq(state+1);
      $display("fifo enq: %d, %d", state, state+1);
   endrule

endmodule: mkTb1

// ----------------------------------------------------------------
// A top-level module instantiating a fifo

(* synthesize *)
module mkTb (Empty);

   FIFO#(int) f <- mkFIFO;    // Instantiate a fifo

   // ----------------
   // Step 'state' from 1 to 10

   Reg#(int) state <- mkReg (0);

   rule step_state;
      if (state > 9) $finish (0);
      state <= state + 1;
   endrule

   // ----------------
   // Enqueue two values to the fifo from seperate rules in the testbench
   // 

   rule enq1 (state < 7);
      f.enq(state);
      $display("fifo enq1: %d", state);
   endrule

   rule enq2 (state > 4);
      f.enq(state+1);
      $display("fifo enq2: %d", state+1);
   endrule

   // ----------------
   // Dequeue from the fifo, and check the first value
   // 

   rule deq;
       f.deq();
       $display("fifo deq: %d", f.first() );
   endrule

endmodule: mkTb

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

This example consistes of two modules: mkTb1, and mkTb.
In both modules, a fifo is instantiated, however - mkTb1 tries to invoke
the enq method twice from the same rule (results with an error), while mkTb
invokes two "enq" from two seperate rules.

In mkTb, the enq rule tries to enq to the same fifo twice, from within the
same rule (atomicly). Since the enq method of a fifo can only be invoked once per clock, the
two "enq" methods conflict, hence they are not parallel composable.

   rule enq (state < 7);
      f.enq(state);
      f.enq(state+1);
      $display("fifo enq: %d, %d", state, state+1);
   endrule

The compilation will fail with the error:

Error: "Tb.bsv", line 36, column 9: (G0004)
  Rule `RL_enq' uses methods that conflict in parallel:
    f.enq(...)
  and
    f.enq(...)
  For the complete expressions use the flag `-show-range-conflict'.
make: *** [tmpb/mkTb] Error 1


----------------
Alternatively, if you comment out mkTb1 module (lines 17-42), you will note the mkTb
is a similar module, but it invokes the two "enq" methods from two seperate rules:


   rule enq1 (state < 7);
      f.enq(state);
      $display("fifo enq: %d", state);
   endrule

   rule enq2 (state > 4);
      f.enq(state+1);
      $display("fifo enq: %d", state+1);
   endrule

These two rules conflict, since they both invoke "f.enq", so the compiler cannot
schedule them to fire in parallel. However, this will NOT cause a compilation error; 
the compilation will result with the warning:

Warning: "Tb.bsv", line 48, column 8: (G0010)
  Rule "enq2" was treated as more urgent than "enq1". Conflicts:
    "enq2" cannot fire before "enq1": calls to f.enq vs. f.enq
    "enq1" cannot fire before "enq2": calls to f.enq vs. f.enq

when executing, note that on cycles 0-6 "enq2" fired, and only once it was disabled
(cycle 7 and on) - "enq1" fired.
    

* ================================================================
*/
