// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 5f
// "Scheduling"
// Attributes on rules: 'preempts'.
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package Tb;

import FIFO::*;
import GetPut::*;
import ClientServer::*;

// ================================================================
// Exact details of this testbench are not important


(* synthesize *)
module mkTb (Empty);
   Reg#(int) cycle <- mkReg(0);

   Server#(int, int) g <- mkGadget;

   // Send data to gadget every 5 out of 8 cycles
   rule source;
      cycle <= cycle + 1;
      if (pack (cycle)[2:0] < 5) g.request.put (cycle);
   endrule

   rule drain;
      let x <- g.response.get ();
      $display ("%0d: draining %d", cycle, x);
      if (cycle > 20) $finish(0);
   endrule
endmodule: mkTb

// ================================================================
// This module transfers an item from the infifo to the outfifo whenever
// possible.  The other rule counts the idle cycles.

(* synthesize *)
module mkGadget (Server#(int,int));
   FIFO#(int) infifo <- mkFIFO;
   FIFO#(int) outfifo <- mkFIFO;

   Reg#(int) idle_cycles <- mkReg(0);

   (* preempts="enqueue_item, count_idle_cycles" *)
   rule enqueue_item;
      outfifo.enq(infifo.first);
      infifo.deq;
   endrule

   rule count_idle_cycles;
      idle_cycles <= idle_cycles + 1;
      $display ("Idle cycle %0d", idle_cycles + 1);
   endrule

   interface request  = fifoToPut (infifo);
   interface response = fifoToGet (outfifo);

endmodule: mkGadget

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

The exact details of the testbench are not important.  The focus is on
mkGadget and the schedule of its rules, demonstrating the 'preempts'
attribute.

Note that the two rules 'enqueue_item' and 'count_idle_cycles' do not
conflict in any way.  Thus, left to itself, 'count_idle_cycles' would
count every cycle.  However, the line:

       (* preempts="enqueue_item, count_idle_cycles" *)

ensures that the latter rule only fires when the first one doesn't,
and thus it only counts cycles where no item is transferred between
the queues.

Indeed, if we compile with the -show-schedule flag, we produce a file
mkGadget.sched which contains the following information about the
cound_idle_cycles rule:

    Rule: count_idle_cycles
    Predicate: 1'd1 && (! WILL_FIRE_RL_enqueue_item)
    Blocking rules: enqueue_item

* ================================================================
*/
