// Copyright 2008 Bluespec, Inc.  All rights reserved.

// section 9 k - Size types - Size provisos - Using size types in value expressions (valueOf)

package Tb;

(* synthesize *)
module mkTb (Empty);

   Reg#(int) step <- mkReg(0);


   rule init ( step == 0 );
      error("Not written yet");
      step <= step + 1;
   endrule

   rule step1 ( step == 1 );
      step <= step + 1;
   endrule

   rule step2 ( step == 2 );
      $display ("All done");
      $finish (0);
   endrule

   ////////////////////////////////////////
   rule watcher (step != 0);
   endrule

endmodule: mkTb

endpackage: Tb

