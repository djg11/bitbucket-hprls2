// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 9a
// Simple Processor model, illustrating the use of enums, tagged unions,
// structs, arrays of registers, and pattern-matching.
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package Tb;

import SimpleProcessor::*;

// ----------------------------------------------------------------
// The following is a program for the Simple Processor
// that computes the GCD (Greatest Common Divisor) of two numbers x and y
// that are initially loaded into registers R1 and R2.
// The program encodes Euclid's algorithm:
//     while (y != 0) if (x <= y) y = y - x else swap (x <=> y)
// In this example, x = 15 and y = 27, so the output should be 3

InstructionAddress label_loop     = 3;
InstructionAddress label_subtract = 10;
InstructionAddress label_done     = 12;
InstructionAddress label_last     = 13;

Instruction code [14] =
   {
     tagged MovI { rd: R0, v: 0 },                // 0: The constant 0
     tagged MovI { rd: R1, v: 15 },               // 1: x = 21
     tagged MovI { rd: R2, v: 27 },               // 2: y = 27
     // label_loop
     tagged Brz { rs: R2, dest:label_done },      // 3: if (y == 0) goto done
     tagged Gt  { rd: R3, rs1: R1, rs2: R2 },     // 4: tmp = (x > y)
     tagged Brz { rs: R3, dest: label_subtract }, // 5: if (x <= y) goto subtract
     // swap
     tagged Minus { rd: R3, rs1: R1, rs2: R0 },   // 6: tmp = x;
     tagged Minus { rd: R1, rs1: R2, rs2: R0 },   // 7: x = y;
     tagged Minus { rd: R2, rs1: R3, rs2: R0 },   // 8: y = tmp;
     tagged Br  label_loop,                       // 9: goto loop
     // label_subtract
     tagged Minus { rd: R2, rs1: R2, rs2: R1 },   // 10: y = y - x
     tagged Br  label_loop,                       // 11: goto loop
     // label_done
     tagged Output R1,                            // 12: output x
     // label_last
     tagged Halt                                  // 13: halt
   };

// ----------------------------------------------------------------
// The testbench

(* synthesize *)
module mkTb (Empty);

   Reg#(InstructionAddress) ia <- mkReg (0);
   SimpleProcessor sp <- mkSimpleProcessor ();

   // Iterate through the instructions array, loading the program into
   // into the processor's instruction memory
   rule loadInstrs (ia <= label_last);
      sp.loadInstruction (ia, code [ia]);
      ia <= ia + 1;
   endrule

   // Start the processor executing its program
   rule go (ia == label_last + 1);
      sp.start();
      ia <= ia + 1;
   endrule

   // Wait till the processor halts, and quit
   rule windup ((ia > label_last + 1) && (sp.halted));
      $display ("Fyi: size of an Instruction is %0d bits", valueof (SizeOf#(Instruction)));
      $finish (0);
   endrule

endmodule: mkTb

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

This is a simple testbench program that:
- Instantiates a Small Processor model
- Loads the processor's program memory with a small test program
- Starts the processor executing its program
- Waits for the processor to terminate, and then quits

The small test program is one that computes the GCD (Greatest Common
Divisor) of two numbers using Euclid's algorithm.  In this example,
the inputs are 15 and 27, and so the output is 3.

Indeed, when we compile and execute it, we see:

    48: output regs[1] = 3
    49: Halt at pc13

i.e., the output is 3, in processor cycle 48, and it halts in cycle 49.

----------------
The lines:

    Instruction code [14] =
       {
         tagged MovI { rd: R0, v: 0 },                // 0: The constant 0
         tagged MovI { rd: R1, v: 15 },               // 1: x = 21
         tagged MovI { rd: R2, v: 27 },               // 2: y = 27
         // label_loop
         tagged Brz { rs: R2, dest:label_done },      // 3: if (y == 0) goto done
         ...
         // label_last
         tagged Halt                                  // 13: halt
       };

create a compile-time constant array of size 14, representing the GCD
program.  Each element of the array is of type Instruction.  The
[0]'th element of the array, for example, is:

         tagged MovI { rd: R0, v: 0 },                // 0: The constant 0

The type declaration for 'Instruction' (in file SimpleProcessor.bsv)
shows that it is a 'tagged union', one of whose kinds is 'MovI'.
Hence the

        'tagged MovI ...'

expression.  When the instruction if of 'MovI' kind, it contains a
struct with two fields 'rd' and 'v', hence the '{ rd: R0, v: 0 }'
subexpression.

Notice that by using enum labels, tagged unions, and structs, the
program practically reads like "assembly language".  We are not
groveling here the bit-representations of instructions, which would be
very tedious, error-prone and unmaintainable.

----------------
A final comment: when we execute the program, another part of the
output says:

    Fyi: size of an Instruction is 37 bits

We will address more efficient encodings in the next version of this
example, example 9b.  For now, notice that the code is remarkably free
of all the bit operations (masking, extraction, shifting, etc.) that
normally makes this kind of code very messy.

* ================================================================
*/
