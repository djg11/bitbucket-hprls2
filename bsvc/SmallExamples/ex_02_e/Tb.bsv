// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 2e
// "Rules, Registers, and FIFOs"
// Multiple registers in a rigid synchronous pipeline,
// now using Valid bits to deal with pipeline bubbles.
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package Tb;

(* synthesize *)
module mkTb (Empty);

   Reg#(int) x    <- mkReg ('h10);
   Reg#(int) pipe <- mkPipe;

   rule fill;
      pipe <= x;
      x <= x + 'h10;
   endrule

   rule drain;
      let y = pipe;
      $display ("    y = %0h", y);
      if (y > 'h80) $finish(0);
   endrule
endmodule

(* synthesize *)
module mkPipe (Reg#(int));

   Reg#(Bool) valid1 <- mkReg(False);   Reg#(int) x1    <- mkRegU;
   Reg#(Bool) valid2 <- mkReg(False);   Reg#(int) x2    <- mkRegU;
   Reg#(Bool) valid3 <- mkReg(False);   Reg#(int) x3    <- mkRegU;
   Reg#(Bool) valid4 <- mkReg(False);   Reg#(int) x4    <- mkRegU;

   rule r1;
      valid2 <= valid1;    x2 <= x1 + 1;
      valid3 <= valid2;    x3 <= x2 + 1;
      valid4 <= valid3;    x4 <= x3 + 1;
   endrule

   rule show;
      $write ("    x1, x2, x3, x4 =");
      display_Valid_Value (valid1, x1);
      display_Valid_Value (valid2, x2);
      display_Valid_Value (valid3, x3);
      display_Valid_Value (valid4, x4);
      $display ("");
   endrule

   method Action _write (int a);
      valid1 <= True; x1 <= a;
   endmethod

   method int _read () if (valid4);
      return x4;
   endmethod

endmodule: mkPipe

function Action display_Valid_Value (Bool valid, int value);
   if (valid) $write (" %0h", value);
   else       $write (" Invalid");
endfunction

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

    Reg#(Bool) valid1 <- mkReg(False);   Reg#(int) x1    <- mkRegU;

'valid1' is a register holding a boolean value indicating whether x1 is valid.

          valid2 <= valid1;    x2 <= x1 + 1;

Shift the valid bit along with its associated value.

          display_Valid_Value (valid1, x1);

Call the function, defined below, to display a valid/invalid value.

      method int _read () if (valid4);

This method can only be called when valid4 is true.
The expression following the 'if' is called the 'implicit condition' of the method.

    function Action display_Valid_Value (Bool valid, int value);
       if (valid) $write (" %0h", value);
       else       $write (" Invalid");
    endfunction

This function either displays 'Invalid' or the valid value.
The function type is Action; hence it can be invoked in the body of a rule.

When executed, you can observe the pipeline behavior.
Further, note that until the 4-deep pipeline gets filled and produces a
valid value from the final register, no 'y' value gets displayed by mkTb.
This is because the _read() method is not enabled until that time, so
that the 'drain' rule cannot not fire until that time.
If you make a Bluesim executable and run it with the -r flag, you
can see the rule firings explicitly.

* ================================================================
*/
