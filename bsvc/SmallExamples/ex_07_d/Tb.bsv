// Copyright 2008 Bluespec, Inc.  All rights reserved.

package Tb;

// section 7 d -  Simple function as an abstraction of an expression (combo circuit)

(* synthesize *)
module mkTb (Empty);

   // function are very useful in BSV.. Any type can be passed to a function
   // (more on functions later) but a simple use to create logical equations
   
   function int square( int a );
      return a * a;
   endfunction

   function int increment( int a );
      // count up to maxInt then stop
      int maxInt = unpack({ 1'b1, 0 });
      if (a != maxInt)
         return a + 1;
      else
         return a;
   endfunction

   //////////////////////////////////////////////////////////////////////
   Reg#(int) step <- mkReg(0);

   rule init ( step == 0 );
      $display("=== step 0 ===");
      $display("square    10 = ", square(10));
      $display("increment 20 = ", increment(20));

      // and they can be nested since they don't use any global state
      $display("2*10+1       = ", increment(square(10)));
      
      step <= step + 1;
   endrule

   rule step1 ( step == 1 );
      $display("=== step 1 ===");
      $display ("All done");
      $finish (0);
   endrule

endmodule: mkTb

endpackage: Tb

