// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 4c
// "RWires"
// Same example (up-down counter)
// showing use of Wires instead of RWires.
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package Tb;

// ----------------------------------------------------------------
// A top-level module connecting the stimulus generator to the DUT

(* synthesize *)
module mkTb (Empty);

   Counter c1 <- mkCounter_v1;    // Instantiate a version 1 counter
   Counter c2 <- mkCounter_v2;    // Instantiate a version 2 counter (same interface type)

   // ----------------
   // Step 'state' from 1 to 10 and show both counter values in each state

   Reg#(int) state <- mkReg (0);

   rule step_state;
      if (state > 9) $finish (0);
      state <= state + 1;
   endrule

   rule show;
      $display ("In state %0d, counter values are %0d, %0d", state, c1.read (), c2.read ());
   endrule

   // ----------------
   // For counter c1, increment by 5 in states 0..6, decrement by 2 in states 4..10
   // The interesting states are 4..6

   rule incr1 (state < 7);
      c1.increment (5);
   endrule

   rule decr1 (state > 3);
      c1.decrement (2);
   endrule

   // ----------------
   // Same for counter c2 (increment by 5 in states 0..6, decrement by 2 in states 4..10)
   // The interesting states are 4..6

   rule incr2 (state < 7);
      c2.increment (5);
   endrule

   rule decr2 (state > 3);
      c2.decrement (2);
   endrule

endmodule: mkTb

// ----------------------------------------------------------------
// Interface to an up-down counter

interface Counter;
   method int read();                     // Read the counter's value
   method Action increment (int di);      // Step the counter up by di
   method Action decrement (int dd);      // Step the counter down by dd
endinterface: Counter

// ----------------------------------------------------------------
// Version 1 of the counter

(* synthesize *)
module mkCounter_v1 (Counter);
   Reg#(int) value1 <- mkReg(0);  // holding the counter's value

   method int read();
      return value1;
   endmethod

   method Action increment (int di);
      value1 <= value1 + di;
   endmethod

   method Action decrement (int dd);
      value1 <= value1 - dd;
   endmethod

endmodule: mkCounter_v1

// ----------------------------------------------------------------
// Version 2 of the counter

(* synthesize *)
module mkCounter_v2 (Counter);
   Reg#(int) value2 <- mkReg(0);  // holding the counter's value

   Wire#(int) w_incr <- mkWire();  // Signal that increment method is being invoked
   Wire#(int) w_decr <- mkWire();  // Signal that decrement method is being invoked

   // ---------------- RULES

   (* descending_urgency = "r_incr_and_decr, r_incr_only, r_decr_only" *)

   rule r_incr_and_decr;
      value2 <= value2 + w_incr - w_decr;
   endrule

   rule r_incr_only;
      value2 <= value2 + w_incr;
   endrule

   rule r_decr_only;
      value2 <= value2 - w_decr;
   endrule

   // ---------------- METHODS

   method int read();
      return value2;
   endmethod

   method Action increment (int di);
      w_incr <= di;
   endmethod

   method Action decrement (int dd);
      w_decr <= dd;
   endmethod

endmodule: mkCounter_v2

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

Most of this example is identical to Example 4b, except for the style
in which we have written module mkCounter_v2.  Here, we have used the
Wire#() datatype instead of RWire#().

The Wire#() datatype is just a different interface to the same
functionality as an RWire#(t).  In fact, Wire#(t) is just a synonym
for Reg#(t).  So, instead of writing a value using 'wset()', we use
the same notation as register writes, namely the non-blocking
assignment operator '<='.  Instead of reading a Maybe#() value, we use
the same notation as register reads, i.e., either we just mention the
name of the Wire to read it implicitly or we can use the method
'_read()' explicitly.

The key point is that the information that is carried on the Valid bit
of a Maybe#() type in an RWire is instead carried here as an implicit
condition of the _read() method.  That is, the _read() method is only
enabled if the _write() (assignment) is happening earlier in the same
clock.

We declare our Wires as follows:

       Wire#(int) w_incr <- mkWire();  // Signal that increment method is being invoked
       Wire#(int) w_decr <- mkWire();  // Signal that decrement method is being invoked

The 'increment()' and 'decrement()' methods are changed, accordingly,
to use register-write notation:

       method Action increment (int di);
          w_incr <= di;
       endmethod

       method Action decrement (int dd);
          w_decr <= dd;
       endmethod

The following rules captures the desired behavior:

       (* descending_urgency = "r_incr_and_decr, r_incr_only, r_decr_only" *)

       rule r_incr_and_decr;
          value2 <= value2 + w_incr - w_decr;
       endrule

       rule r_incr_only;
          value2 <= value2 + w_incr;
       endrule

       rule r_decr_only;
          value2 <= value2 - w_decr;
       endrule

Because of the implicit conditions on w_incr and w_decr, the first
rule can only fire if both Wires are being assigned.  Similarly, the
second rule can only fire if 'w_incr' is being assigned, and the third
rule can only fire if 'w_decr' is being assigned.

Note that all three rules are enabled if both 'w_incr' and 'w_decr'
are being assigned.  The 'descending_urgency' attribute ensures that
in this case the first rule is given priority over the other two.

When we execute the code, the displayed outputs indeed show the same
behavior as the examples 4a and 4b.

* ================================================================
*/
