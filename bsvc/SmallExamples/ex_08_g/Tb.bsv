// Copyright 2008 Bluespec, Inc.  All rights reserved.

// section 8 g - Static elaboration array vs. run-time value array
package Tb;

import Vector::*;

(* synthesize *)
module mkTb (Empty);

   // we've touch upon static vs run time array values in previous
   // examples, but let's be more pragmatic about it now.
   
   // static elaboration means that the compiler can determine,
   // at compile time, the values needed to index into an array.
   // Commonly this is done with Integer, but it could be done
   // with data types also... For instance.. Let's create an
   // array of registers and look at various ways we can read
   // the values out of that array..
   
   Vector#(8,Reg#(int)) arr1 <- replicateM( mkRegU );
   
   Reg#(int) step <- mkReg(0);

   rule init ( step == 0 );
      
      // notice here that we are indexing over a fixed
      // predetermined range.  0...7  There is no variable length
      // here and the compiler and easily see what to do at compile time.
      // This is "Static Elaboration" and you might think of it like
      // generate in verilog 2000, but this is much more powerful.

      for (Integer i=0; i<8; i=i+1)
         arr1[i] <= fromInteger(i);

      // This exands to 
      //     arr1[0] <= fromInteger(0);
      //     arr1[1] <= fromInteger(1);
      //     arr1[2] <= fromInteger(2);
      //          ::         ::
      //     arr1[7] <= fromInteger(7);

      step <= step + 1;
   endrule
   
   rule step1 ( step == 1 );
      // But becareful.. It's not because we used an Integer that
      // this is static, it's because we can determine at compile
      // time what the result is.
      // For instance, we can use another index / data type
      for (int i=0; i<8; i=i+1)
         arr1[i] <= i;

      // the variable i here is actually a int (short hand for Int#(32)
      // which is exactly the data type of arr1.  So we don't need
      // fromInteger, we can assign i directly to arr1[i]...
      // Also, the compiler here is smart enough to know that even though
      // the index in this case is a 32 bit integer, there are only 8
      // values, so there's not "32 bit index" created.. Just the 0..7

      // Also notice, that there is no dependence between steps of
      // this loop.  So, timing and hardware wise, it's pretty simple.
      // each register is loaded with it's static value when this rule fires

      step <= step + 1;
   endrule

   rule step2 ( step == 2 );
      // Let's make it a bit more interesting.  We can certainly
      // access registers as part of this loop, etc...  Just
      // loop careful to see what that loop then means...
      // For instance, suppose we now what to shift the elements
      // of this array manual
      for (int i=0; i<8; i=i+1)
         if (i != 7)
            arr1[i+1] <= arr1[i];
         else
            arr1[0]   <= arr1[7];
      
      // The compiler is still able to statically elaborate this
      // loop into the desired rotate.  variable i is still statically
      // incrementing from 0 .. 7
      // Also observe now that I have a if-else (and I can use any
      // case or structure that elaborates properly - you will get
      // a better understanding of the possibilities with experience)
      
      // so this loop simply elaborates out to
      //  arr[1] <= arr[0]
      //  arr[2] <= arr[1]
      //  arr[3] <= arr[2]
      //  arr[4] <= arr[3]
      //  arr[5] <= arr[4]
      //  arr[6] <= arr[5]
      //  arr[7] <= arr[6]
      //  arr[0] <= arr[7]

      step <= step + 1;
   endrule

   rule step3 ( step == 3 );
      // want to scramble the values?  use a case statement ?
      // sure..  it's still static
      for (int i=0; i<8; i=i+1)
         case (i)
            0: arr1[0] <= arr1[3];
            1: arr1[1] <= arr1[1];
            2: arr1[2] <= arr1[2];
            3: arr1[3] <= arr1[0];
            4: arr1[4] <= arr1[5];
            5: arr1[5] <= arr1[7];
            6: arr1[6] <= arr1[4];
            7: arr1[7] <= arr1[6];
         endcase

      // BUT since we are loading the same "arr" array in different
      // rules with different things. we are still creating wires
      // and logic.  Such is the life of higher level design.

      step <= step + 1;
   endrule

   //////////////////////////////////////////////////////////////////////
   // now let's get run time...
   Reg#(UInt#(3)) inx <- mkReg(0);
   
   rule step4 ( step == 4 );
      // now consider this..  The array is indexed by "inx"
      // which is a register of (UInt#(3))
      arr1[inx] <= 0;

      // as you know, this will create a runtime index.  Meaning
      // when this rule fires, whatever value is in register inx
      // will be used into index to the proper array value.
      // And of course, you don't get confused by mkReg(0) because
      // that's the value it gets reset to.. The compiler sees this
      // is a module which may change at run time (when clocks are applied)
      // there for it must create the appropriate logic to be able to
      // write every arr1 depending on the value of inx.
      
      // now as an optimization, if you *know* somehow that you
      // only ever write to inx==0 and inx==1 (for instance)
      // then give the compiler a clue by coding something like this
      //
      //   if ( inx == 0 )
      //      arr1[0] <= 0;
      //   else if ( inx == 1 )
      //      arr1[1] <= 1;
      //
      // or index down to a single bit or some trick so that
      // the compiler can see exactly which indecies are possible.
      //
      step <= step + 1;
   endrule


   rule step5 ( step == 5 );      
      // so lets look at something more complex

      for (Integer i=0; i<8; i=i+1) begin
         // do some recasting because arr1 is "int" and
         // inx is UInt#(3), so create a int with the
         // same value as inx
         int indexVal = unpack(zeroExtend(pack(inx)));
         
         // now load arr1[0] <=  0 + inx
         //          arr1[1] <=  2 + inx
         //          arr1[2] <=  4 + inx
         //               ::       ::
         //          arr1[7] <= 14 + inx

         arr1[i] <= fromInteger(2*i) + indexVal;
         
         // this is still mostly statically elaborated
         // but you can now see a hardware problem developing
         // (at least I hope you can)..  This elaboration
         // is going to take 8 adders (well 7 really) to
         // generate inx + <someval> and assign it to the 
         // appropriate register in the array
         
         // so we have generated specific hardware at compile
         // time.  We have generate rule enables, muxes to load
         // each register in the array and we've created
         // 7 adders, which we hope synthesis can perhaps
         // optimize well...

      end

      step <= step + 1;
   endrule


   rule step6 ( step == 6 );
      // one last example

      // the point of all this is to consider what this
      // for loop really means in terms of generated RTL.
      // Sequential programmers (i.e. C++ only folks)
      // don't think twice about writing loops that
      // create logic dependent on that last loops data.
      
      // but in hardware terms this can create a lot of logic
      // register assign occur at the end of the cycle so this
      // loop is still a bunch of adders, by the critical path
      // is still just the adder and register load (plus the
      // enable logic to determine if this rule is firing)

      for (Integer i=1; i<8; i=i+1)         
         arr1[i-1] <= arr1[i] + fromInteger(i * 3);

      // to unroll this loop manually, we see the following sequence
      //
      //   arr1[0] <= arr1[1] + 0;
      //   arr1[1] <= arr1[2] + 3;
      //   arr1[2] <= arr1[3] + 6;
      //     ::         ::
      //   arr1[6] <= arr1[7] + inx;
      

      step <= step + 1;
   endrule
   
   rule step7 ( step == 7 );

      // but, let's say we want to do something more sequential
      int acc = 0;
      
      for (Integer i=0; i<8; i=i+1) begin
         // calculate acc for this loop
         // but notice that acc is read acc, which is as
         // one might expect, the acc from the last loop
         // since acc is not a register (it is a local variable)
         acc = arr1[i] + acc;

         // assign the register
         arr1[i] <= acc;
      end
      
      // this expands like so
      //    acc      = arr1[0] + 0;
      //    arr1[0] <= acc
      //
      //    acc      = arr1[1] + acc;
      //    arr1[1] <= acc
      //     ::           ::
      //    acc      = arr1[7] + acc;
      //    arr1[7] <= acc

      // now look at this carefully.  acc is loaded in one
      // iteration of the loop, then reloaded in the next.
      // In sw terms this just means "use that value that I
      // stored somewhere in memory for this iteration"..
      // In hardware terms, this is creating a serial chain
      // of logic.  To look at it backwards, arr1[7] <= acc,
      // but acc can't be calculated until we know
      // arr1[7] + acc, which needs to be calculated from
      // index 6 of the loop, etc, all the way back to 0.
      // This will create 7 adders again (presuming the start of +0
      // doesn't really need an adder :), but this time
      // this is a potentially large critical path,
      // as one adder delay follows another adder delay (7 times).
      // This is very different than the parallel code
         
      // as a final note: none of this is to say you shouldn't
      // use static elaboration or run time indexing to generate
      // the code that your architecture requires.  For testbenches
      // and architectural exploration you may not care at all,
      // but for hardware going to an FPGA or an ASIC, like all
      // good RTL coders, you need to be aware of what structures
      // generate what logic :)

      step <= step + 1;
   endrule
   
   rule step8 ( step == 8 );
      $display ("All done");
      $finish (0);
   endrule

   
   rule watcher (step > 0);
      $display("=== step %d ===", step);
      for (Integer i=0; i<8; i=i+1)
         $display (" arr1[%d] = ", i, arr1[i]);
   endrule


endmodule: mkTb

endpackage: Tb

