// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 5l
// "Scheduling"
// Effect on scheduling when using mkConfigReg instead of mkReg.
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package Tb;

import ConfigReg::*;

// ================================================================
// This is a made-up example, so the exact functionality is not important.

interface Pipe_ifc;
   method ActionValue#(int) pump (int x);
endinterface

(* synthesize *)
module mkPipe (Pipe_ifc);

   Reg#(int) r0 <- mkReg (0);

//   Reg#(int) r1     <- mkReg (10);
   Reg#(int) r1 <- mkConfigReg (10);

   Reg#(int) r2 <- mkReg (25);

   // ----------------
   // RULES (representing some pipelined computation)

   rule stage1;
      r1 <= r0 + 10;
   endrule

   rule stage2;
      r2 <= r1 + 15;
   endrule

   // ----------------
   // METHODS

   method ActionValue#(int) pump (int x);
      r0 <= x;
      return r2;
   endmethod

endmodule

// ----------------------------------------------------------------

(* synthesize *)
module mkTb (Empty);

   Pipe_ifc foo <- mkPipe;

   Reg#(int) cycle <- mkReg (0);

   rule count_cycles;
      cycle <= cycle + 1;
      if (cycle > 9) $finish (0);
   endrule

   // ----------------
   // RULES

   rule rA;
      let y <- foo.pump (cycle+1);
      $display ("%0d: rule rA, y = %0d", cycle, y);
   endrule

endmodule: mkTb

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

This is a made-up example and so the exact functionality is not
important.

Past improvements in the BSV scheduler have obviated many earlier
needs for ConfigReg, so this example remains here just for
completeness.

The module mkPipe represents some pipelined computation (here, 3
stages represented by registers r0, r1 and r2).  The 'pump'
ActionValue method is supposed to feed a new value x into the pipe
(reg r0) and return the output of the pipe (reg r2).

The top-level testbench module mkTb just feeds the pipe with the cycle
count and prints the output from the pipe.

In module mkPipe, notice that reg r1 is either instantiated using
mkReg, or with mkConfigReg (one of the lines is commented out).

----------------
r1 instantiated with mkReg

Let us first try compiling with r1 instantiated using mkReg.  The
compiler reports the following scheduling error:

    Warning: "Tb.bsv", line 24, column 8: (G0009)
      The scheduling phase created a conflict between the following rules:
          `pump' and `RL_stage2'
      to break the following cycle:
          `pump' -> `RL_stage2' -> `RL_stage1' -> `pump'
    Error: "Tb.bsv", line 24, column 8: (G0095)
      A cycle was detected between the scheduling of method `pump' and its
      execution. This is not allowed, as the method may be called conditionally
      inside a rule in the parent module. The cycle is as follows:
        `pump' -> `RL_stage2' -> `RL_stage1' -> `pump'
      The relationships were introduced for the following reasons:
        (pump, RL_stage2)
        urgency order because of urgency order chosen by the compiler
        (RL_stage2, RL_stage1)
        execution order because of calls to r1.write vs. r1.read
        (RL_stage1, pump) execution order because of calls to r0.write vs. r0.read
      To resolve this cycle, consider removing an urgency dependency from the
      cycle or removing an execution order requirement (for example, by causing
      the two rules to conflict or be mutually exclusive). Removing the synthesis
      boundary for this module may also help.

The error can be explained as follows:
  Method pump writes r0, and so must follow rule stage1 which reads r0.
  Rule stage1 writes r1, and so must follow rule stage2 which reads r1.
  Rule stage2 writes r2, and so must follow method pump which reads r2.
Hence there is a cyclic scheduling dependency from 'pump' to 'pump'.

----------------
r1 instantiated with mkConfigReg

This time, compilation is successful and we can successfully build and
run the simulation.

The difference is because the ConfigReg has a more relaxed schedule on
reads and writes and this breaks the scheduling cycle.  In particular,
for reg r1, the write does not have to follow the read, per the
scheduling properties of the methods of ConfigRegs.  As long as we are
not expecting to read the new value of r1 in the same cycle as the
write, this is ok, and this is indeed the case in this example.

----------------
Note: This is a somewhat contrived example, since in principle both
    the pipeline rules could have been eliminated and their actions
    folded into the 'pump' ActionValue method.  In other words, all
    the work could have been done in the ActionValue method.
    Nevertheless, in a more complex example the rules may have had
    rule conditions, may have needed to access other local state, etc.

    In the distant past, the compiler always scheduled a module's
    methods to be 'earlier' than its rules.  In such a situation, an
    always-ready method that wrote into a register used for
    configuration would always preempt any rule that tried to read the
    register.  So, here was a need to allow the write to be earlier
    than the read, with the understanding that the newly written value
    would not be visible until the next cycle, and this is exactly
    what mkConfigReg does.

    However, now that the compiler allows methods to follow rules in
    execution order, this need has gone away.  Nevertheless,
    ConfigRegs remain in the library for legacy reasons and for
    expediency.

* ================================================================
*/
