// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 12d
// AutoFSM
// ================================================================

/*
   Often for tests and testbenches, we just want to start up
   a sequence more automatically and just let it run..  If we
   don't need to start and restart it, then we can use mkAutoFSM.
   Basically it's like any other FSM except it start running
   a few cycles after reset and it calls $finish when it is done.
   So bewarned!  If you use mkAutoFSM make sure all other state
   is finished before you let the fsm finish (and call $finish -
   or you may wonder what happend to your last few cycles)
*/
 
package Tb;

import StmtFSM::*;

(* synthesize *)
module mkTb (Empty);

   ////////////////////////////////////////
   Stmt test =
   seq
      $display("I am now running at ", $time);

      $display("I am now running one more step at ", $time);

      $display("And now I will finish at", $time);
   endseq;

   mkAutoFSM ( test );

   // same as 
   // FSM testFSM <- mkFSM( test );
   // but it autostarts a few cycles after reset
   
   ////////////////////////////////////////
   rule alwaysrun;
      $display("    and a rule fires at", $time);
   endrule

endmodule
   
endpackage
