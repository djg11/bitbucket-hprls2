// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 5k
// "Scheduling"
// Effect of scheduling (possible atomicity inequivalence) of separating
// an ActionValue method into Action and Value methods
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package Tb;

import FIFO::*;

// ================================================================
// This is a made-up example, so the exact functionality is not important.

interface Foo_ifc;
   method ActionValue#(int) avx ();
   method int vy ();
   method Action ay ();

   method int get_cycle_count ();
endinterface

(* synthesize *)
module mkFoo (Foo_ifc);

   Reg#(int) x <- mkReg (0);
   Reg#(int) y <- mkReg (0);
   Reg#(int) cycle <- mkReg (0);

   // ----------------
   // RULES

   rule count_cycles;
      cycle <= cycle + 1;
      if (cycle > 3) $finish (0);
   endrule

   rule incx;
      x <= x + 1;
      $display ("%0d: rule incx; new x = %0d", cycle, x+1);
   endrule

   rule incy;
      y <= y + 1;
      $display ("%0d: rule incy; new y = %0d", cycle, y+1);
   endrule

   // ----------------
   // METHODS

   method ActionValue#(int) avx ();
      $display ("%0d: method avx; setting x to 42; returning old x = %0d", cycle, x);
      x <= 42;
      return x;
   endmethod

   method int vy ();
      return y;
   endmethod

   method Action ay ();
      $display ("%0d: method ay; setting y to 42", cycle);
      y <= 42;
   endmethod

   method int get_cycle_count ();
      return cycle;
   endmethod
endmodule

// ----------------------------------------------------------------

(* synthesize *)
module mkTb (Empty);

   Foo_ifc foo <- mkFoo;

   let cycle = foo.get_cycle_count ();

   // ----------------
   // RULES

   rule rAVX;
      let x <- foo.avx ();
      $display ("%0d: rule rAVX, x = %0d", cycle, x);
   endrule

   rule rAY;
      $display ("%0d: rule rAY", cycle);
      foo.ay ();
   endrule

   rule rVY;
      let y = foo.vy ();
      $display ("%0d: rule rVY, y = %0d", cycle, y);
   endrule

endmodule: mkTb

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

This is a made-up example and so the exact functionality is not
important.

The interface definition:

    interface Foo_ifc;
       method ActionValue#(int) avx ();
       method int vy ();
       method Action ay ();
       ...

is intended to show a difference between combining an Action and
returning a value in a single ActionValue method, avx, vs. separating
the Action into an Action method ay and returning the value in a value
method vy.

In module mkFoo, rules incx and incy are identical, on registers x and
y respectively.

The method avx returns the old value of x and sets x to 42.
The method vy just returns the old value of y.
The method ay just sets y to 42.
I.e., methods vy and ay can be seen together as doing the same work as method avx.

In the top-level module mkTb, rule rAVX calls the ActionValue method avx.
Rule rAY just calls the Action method ay.
Rule rVY just calls the value method vy.

When we compile and run, we see output like this:

    0: rule rVY, y = 0
    0: rule incy; new y = 1
    0: rule rAY
    0: method ay; setting y to 42
    0: method avx; setting x to 42; returning old x = 0
    0: rule rAVX, x = 0
    1: rule rVY, y = 42
    1: rule incy; new y = 43
    1: rule rAY
    1: method ay; setting y to 42
    1: method avx; setting x to 42; returning old x = 42
    1: rule rAVX, x = 42

Note that in each cycle, with respect to the split method, rules rVY,
incy, rule rAY execute, in that order.  On the other hand, with
respect to the combined method, rule rAVX fires, but not rule incx.

The explanation has to do with atomicity.  The combined method avx
both reads and writes x, as does rule incx.  Thus, the combined method
conflicts with the rule and these two can never execute simultaneously
in the same cycle.

In the split method, on the other hand, method vy only reads y; rule
incy both reads and writes y, and method ay only writes y.  Thus, it
is possible to schedule these in that order: vy before incy before ay,
and this is what we see in the output trace.

In fact, we can also see this when compiling with the '-show-schedule'
flag.

    Rule: incy
    Predicate: 1'd1
    Blocking rules: (none)

    Rule: incx
    Predicate: 1'd1 && (! WILL_FIRE_avx)
    Blocking rules: avx

We see that incy is not blocked by anything, whereas incx is blocked
by method avx (because it conflicts with avx).  In the logical
execution order:

    Logical execution order: avx,
                             vy,
                             ...
                             incy,
                             ay,
                             incx,
                             ...

we see that incy can execute BETWEEN vy and ay.

----------------
SUMMARY

- Splitting an ActionValue method into an Action method and a Value
  method can change the atomicity properties.  In particular, whereas
  the ActionValue method is necessarily atomic, the separate Action
  and Value methods can now be called from separate rules, and those
  rules can now have other intervening rules.

----------------
EXERCISE

- In module mkTb, combine the rules rAY and rVY into a single rule:

        rule rAVY;
          foo.ay ();
          let y = foo.vy ();
          $display ("%0d: rule rAVY, y = %0d", cycle, y);
        endrule

   Observe and explain the behavior and the schedule.

* ================================================================
*/
