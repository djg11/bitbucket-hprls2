// Copyright 2008 Bluespec, Inc.  All rights reserved.
// section 8 f - array of module
package Tb;

typedef Bit#(14) TD;

import FIFO::*;
import Vector::*;
import StmtFSM::*;

(* synthesize *)
module mkTb (Empty);

   // as we saw in the last section, an array of registers is
   // possible because we can make an array of any module.
   // to emphasis this point, let's make an small 2x2 switch
   // out of 2 arrays of fifos

   // two fifos in
   Vector#(2,FIFO#(TD)) ififo <- replicateM( mkFIFO );

   // four fifos out
   Vector#(4,FIFO#(TD)) ofifo <- replicateM( mkFIFO );

   // lets be simple and just create a rule for each input
   // fifo to drive data to the output fifo

   // if move0 fires, then move1 does not
   // this just gets rid of the warning you see otherwise
   // this is good enough for this example of index modules
   (* preempts = "move0, move1" *)  
   rule move0;

      // notice that we created a vector of interfaces
      // ififo[0] is a single interface of FIFO
      // FIFO has first,deq,enq,clear
      // so we can access those methods directly
      let data = ififo[0].first();
      ififo[0].deq();

      case ( data[13:12] )
         0: ofifo[0].enq( data );  // call (ofifo[0]) method enq( data )
         1: ofifo[1].enq( data );
         2: ofifo[2].enq( data );
         3: ofifo[3].enq( data );
      endcase
   endrule

   rule move1;
      let data = ififo[1].first();
      ififo[1].deq();
      case ( data[13:12] )
         0: ofifo[0].enq( data );
         1: ofifo[1].enq( data );
         2: ofifo[2].enq( data );
         3: ofifo[3].enq( data );
      endcase
   endrule

   // see more about test sequences in section 12
   Stmt test =
   seq
      // bits 13:12 select the output fifo
      ififo[0].enq( 'h0000 );
      ififo[0].enq( 'h1001 );
      ififo[0].enq( 'h2002 );
      ififo[0].enq( 'h3003 );
      ififo[1].enq( 'h0000 );
      ififo[1].enq( 'h1010 );
      ififo[1].enq( 'h2020 );
      ififo[1].enq( 'h3030 );
      noAction;
   endseq;
   mkAutoFSM ( test );

   // and now create 4 rules to watch each output using
   // static elaboration
   for (Integer i=0; i<4; i=i+1)
      rule watch;
         let data = ofifo[i].first();
         ofifo[i].deq();
         $display("ofifo[%d] => %x at ", i, data, $time);
      endrule

endmodule: mkTb

endpackage: Tb

