// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 5j
// "Scheduling"
// Conditionals in rules, and effect on scheduling: '-split-if'
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package Tb;

import FIFO::*;

// ================================================================
// This is a made-up example, so the exact functionality is not important.

(* synthesize *)
module mkTb (Empty);

   Reg#(int) cycle <- mkReg (0);

   FIFO#(int) f <- mkFIFO ();

   Reg#(int) y <- mkReg (0);

   // ----------------
   // RULES

   rule count_cycles;
      cycle <= cycle + 1;
      if (cycle > 15) $finish(0);
   endrule

   rule rA;
      // (* split *)
      if (pack (cycle)[0] == 1) begin
         f.enq (y);
         $display ("%0d: rule rA (True side) Enqueuing %d into fifo f", cycle, y);
      end
      else begin
         f.enq (cycle);
         $display ("%0d: rule rA (False side) Enqueuing %d into fifo f", cycle, cycle);
         y <= 42;
      end
   endrule

   rule rB;
      f.deq ();
   endrule

   rule rC;
      y <= y + 1;
      $display ("%0d: rule rC incrementing y", cycle);
   endrule

endmodule: mkTb

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

This is a made-up example to show demonstrate the effect on rule
scheduling of the compiler flag '-split-if' and so the exact
functionality is not important.

First, we instantiate a FIFOs f, and a register y.

Rule rA has no explicit rule condition, i.e., the explicit rule
condition is 'True'.  On odd cycles, it reads 'y' and tries to enqueue
it into FIFO f.  On even cycles, it tries to enqueue the cycle count
into FIFO f, and also writes the constant value 42 into register y.

Rule rB simply drains FIFO f.

Rule rC reads and writes y.

Note that the conditional in rule rA has an attribute 'split', which
is initially commented out.  As described in the Bluespec
SystemVerilog Reference Guide and User Guide, you can either provide
the '-split-if' command line flag to the compiler, or you can preceded
a conditional with the 'split' attribute as shown here.  The
command-line flag version will apply to all conditionals in the
compilation unit, whereas the attribute only applies to the
conditional to which it is attached.

----------------
Without '-split-if' (attribute 'split' is commented out)

Since rules rA and rC both read and write register y, one may conclude
that they conflict, and therefore cannot fire together.

Indeed when we compile, the compiler gives the warning:

    Warning: "Tb.bsv", line 20, column 8: (G0010)
      Rule "rC" was treated as more urgent than "rA". Conflicts:
        "rC" cannot fire before "rA": calls to y.write vs. y.read
        "rA" cannot fire before "rC": calls to y.write vs. y.read

and when we execute, since rC was treated as more urgent, it's the
only rule that fires:

    0: rule rC incrementing y
    1: rule rC incrementing y
    2: rule rC incrementing y
    ...
    16: rule rC incrementing y

If we compile with the '-show-schedule' flag, it reports the following
rule execution order:

    Logical execution order: rC, rB, rA, count_cycles

Here, rC is scheduled before rA, somewhat arbitrarily (the order does
not matter since they conflict anyway and will therefore never be
allowed to fire together in the same clock.

----------------
Again without '-split-if' (attribute 'split' is commented out)
but with '-agressive-conditions'

The Makefile contains an alternative definition of BSC_FLAGS that
switches on '-agressive-conditions'.

Even with this flag, we see no change in the schedule.  The reason is
that it has no effect on the question of rule rA conflicting with rule
rC.  Both rules still read and write y, and therefore conflict.

----------------
With '-split-if' (attribute 'split' is uncommented)

Let us enable '(* split *)' attribute by remove the comment markers,
and recompile (with or without '-agressive-conditions', it does not
matter).

In the schedule report, we now see that there are two rules instead of
rule rA:

    Rule: rA_T
    Predicate: (cycle.read [32'd0]) && f.i_notFull
    Blocking rules: (none)

    Rule: rA_F
    Predicate: (! (cycle.read [32'd0])) && f.i_notFull
    Blocking rules: (none)

What the compiler has done is to split rule rA into the following two
rules:

       rule rA_T (pack (cycle)[0] == 0);
          f.enq (y);
          $display ("%0d: rule rA (True side) Enqueuing %d into fifo f", cycle, y);
       endrule: rA_T

       rule rA_F (! (pack (cycle)[0] == 0));
          f.enq (cycle);
          $display ("%0d: rule rA (False side) Enqueuing %d into fifo f", cycle, cycle);
          y <= 42;
       endrule: rA_F

Notice that neither of these rules conflicts with rule rC.  Rule rA_T
only reads y, and so it can be safely scheduled before rC, which reads
and writes y.  Rule rA_F only writes y, and so it can be safely
scheduled after rC.  Indeed, in the schedule report we see:

    Logical execution order: rB, rA_T, rC, rA_F, count_cycles

i.e., ra_T is before rC, which is before ra_F.

When we execute the program, we see:

    0: rule rC incrementing y
    0: rule rA (False side) Enqueuing           0 into fifo f
    1: rule rA (True side) Enqueuing          42 into fifo f
    1: rule rC incrementing y
    2: rule rC incrementing y
    2: rule rA (False side) Enqueuing           2 into fifo f
    3: rule rA (True side) Enqueuing          42 into fifo f
    3: rule rC incrementing y
    4: rule rC incrementing y
    4: rule rA (False side) Enqueuing           4 into fifo f
    5: rule rA (True side) Enqueuing          42 into fifo f
    5: rule rC incrementing y
    ...
    16: rule rC incrementing y
    16: rule rA (False side) Enqueuing          16 into fifo f

Also note that on even cycles, rC fires before rA (actually rA_F), and
on odd cycles, rA (actually ra_T) fires before rC, as shown in the
schedule.

----------------
SUMMARY

- '-aggressive-conditions' is just about a rule on its own.  It just
  calculates a more fine-grained version of a rule enabling condition,
  so that the rule may actually be enabled even though some resource
  (method) mentioned in the rule is unavailable, because the
  conditionals in the rule indicate that it won't currently need that
  resource.

- '-split-if' (or, equivalently, attribute '(* split *)') actually splits
  a rule around an internal condition, adding that conditional (and
  its negation) to the rule condition predicate.  Because of this
  physical splitting, each residual part may no longer conflict with
  other rules.  Further, the residual parts may be scheduled at
  different points in the execution order, with possibly other rules
  in between them.

- Note that when you have nested conditionals, '-split-if' acts
  recursively, i.e., after the top-level split, one or both of the
  residual rules may be further split again, if they still contain
  conditionals.  Thus, the '-split-if' flag can, in the worst case,
  produce 2^N rules out of one rule, where N is the depth of nested
  conditionals in the rule.  This of course increases the work for the
  compiler's scheduler, and is the reason why '-split-if' is kept off
  by default.  Thus, if you need to split a rule, you'll have to
  explicitly use the flag.  We recommend using the '*( split *)'
  attribute in the source code instead of using the commmand-line
  flag.

* ================================================================
*/
