// Copyright 2008 Bluespec, Inc.  All rights reserved.
// Arrays and square-bracket notation
package Tb;

// Arrays are certainly useful and BSV supports them for all first class
// objects.  A common desire is to have an array of registers, but since
// a register is just another module to BSV, we could also have arrays of
// any module (fifos, cpus, wires, etc).  What gets created depends, as
// always, on how they are used..
//

(* synthesize *)
module mkTb (Empty);

   ////////////////////////////////////////////////////////////
   // we can define any user defined or builtin type as an array simply by
   // using [].  But, this must be statically defined (it can be
   // parameterized) but by the time we generate actually hardware, the size
   // of this array has to be fixed.  The simplest example to use a coded
   // number here.  In this case we are creating 10 instances of arr
   // which can be accessed as arr[0], arr[1], arr[2] ... arr[9]
   
   UInt#(16) arr[10];

   // Now we can initialize it statically at the global level
   // and we have an array of "interesting" values.. I.e.
   //   arr[0] = 0
   //   arr[1] = 3
   //   arr[2] = 6
   //   etc
   // but this is a global variable, it's not wires or
   // registers, etc, so it's basically an initialize-only variable.

   // and a for loop here statically elaborates the array
   // like generate or a scripting language, this is identical
   // to typing the 10 lines (in this case) out separately
 
   for (Integer i=0; i<10; i=i+1)
      arr[i] = fromInteger(i * 3);

   ////////////////////////////////////////////////////////////
   // You can create 2  (or 3 or 4, etc) dimensions.. The
   // only real limitation is that you have to remember you
   // are creating logic/hardware here!  So you can create a 20
   // dimension array if you want, but it will be freakishly
   // big in terms of gates :O
   UInt#(16) arr2[3][4];

   for (Integer i=0; i<3; i=i+1)
      for (Integer j=0; j<4; j=j+1)
         arr2[i][j] = fromInteger((i * 4) + j);
   
   ////////////////////////////////////////////////////////////
   // You can also create arrays of registers (or any module
   // for that matter) in a similar way:
   // note that this relates an array of interfaces,
   // and each interface still needs to be assigned to an instance
   // (or another module interface of the same type - see next example)
   
   Reg#(int) arr3[4];
   for (Integer i=0; i<4; i=i+1)
      arr3[i] <- mkRegU;
   
   //////////////////////////////////////////////////////////////////////
   // finally, I have this function in my library of personal function
   // (it could move to the prelude soon)..  But this allows you to
   // loop over a vector, without "knowing" it's length ahead of time..
   function Integer vectorLen( Vector#(nogv, nogv_t) ivec);
      return valueof(nogv);
   endfunction

   // using vectorLen is handy since often we define state at the top
   // of a file and may do something else with it much later
   // So we don't necessarily have to remember and hardcode it's len

   Reg#(int) arr4[4];
   for (Integer i=0; i<vectorLen(arr4); i=i+1)
      arr4[i] <- mkRegU;

   // also, this are essentially an interface so we create
   // an alias, but again looping and assigning the new
   // interface to the old interface.  This way, both arr4
   // and arr5 read and write the same physical registers.

   Reg#(int) arr5[4];
   for (Integer i=0; i<vectorLen(arr4); i=i+1)
      arr5[i] = arr4[i];


   //////////////////////////////////////////////////////////////////////
   // this pragma determine the order these rules fire within
   // the same cycle (if they all fire in the same cycle).
   // More on this in rule exection, but for now just
   (* execution_order = "load_arr3, load_arr3_a, load_arr3_b" *)


   // load all or any single entry within the same rule
   rule load_arr3;
      arr3[0] <= 'h10;
      arr3[1] <= 4;
      arr3[2] <= 1;
      arr3[3] <= 0;
   endrule

   rule load_arr3_a;
      arr3[2] <= 'h30;
   endrule

   rule load_arr3_b;
      arr3[3] <= 'h20;
   endrule

   // after all 3 fire, look at the values
   rule display_arr3;
      for (Integer i=0; i<4; i=i+1)
         $display("arr3[i] = %x", arr3[i]);
      $finish;
   endrule
   

endmodule: mkTb

endpackage: Tb

