// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 12b
// one hot "FSM" by rule
// ================================================================

/*

The main difference now is to implement a one hot FSM using a register
of bits wide enough for each state.  You can of course create your own
implementations several other ways, but here is one version.
 
A quick explaination of what "one hot" fsm is:
 
A standard state machine would create a state register with bits 
wide enough to hold the values for each state..  I.E.  if there are
100 states, then log2 bits are needed, so a 7 bit register is all
that is needed to code 100 unique state.  This has the advantage
that it requires the minimal number of state registers, and you 
can't get confused about what state you are in.
 
A different version of a state machine is called "one hot".  The
basic idea is that you create one register bit for each unique state
(so in the case above, rather the 7 bits of state, there would be
100 bits of state).  But then only one state bit is set to 1 at any 
particular time.   So each state doeshn't need to decode a state 
value, it only needs to see if it's one state bit is set.

This typically is only done when the state machine is
large, and timing is tight enough such that the decode of the
state value is slower than desired).  This tends to make the 
front end timing (from register -> logic) slightly faster while
making the next state decode slower (logic -> register), and uses
more register bits, but often the state change logic is simpler than
the actual actions in the block.  

We index the state bits with predefined "Integer" types (which are
static integers determined at compile time).  We create our state
value with a function (you could also just manually create them, but
the point is to minimize errors).
 
As a general rule, if you are not generating tightly timed hardware,
then one hot makes very little sense.  I.E. if you are generating a
testbench or an architectural model, use a standard state register and
enums, not a one hot fsm.  Perhaps a better way to say this is: If you
don't already understand the cases where a one hot FSM is needed over
a normal mealy/moore state machine, then you don't need to use a one
hot.  So don't ;)

More comments embedded below

*/
 
package Tb;

// ----------------------------------------------------------------
// A top-level module connecting the stimulus generator to the DUT

(* synthesize *)
module mkTb (Empty);

   // four states
   // using explicit values is a bit laborious, but at least
   // you know which state for which bit when you are debugging :)
   Integer idle  = 0;
   Integer step1 = 1; // or "idle + 1", 
   Integer step2 = 2;
   Integer stop  = 3;

   // create the single bit in the proper place.
   // zero all others..
   function Bit#(4) toState( Integer st );
      return 1 << st;
   endfunction

   // set the default state to IDLE
   // out of reset, this means FSM is IDLE
   Reg#(Bit#(4)) state <- mkReg( toState(idle) );
   
   // counter just to generate some conditions to move state along
   Reg#(int)     counter <- mkReg(0);
   Reg#(Bool)    restart <- mkReg(False);

   rule runCounter;
      if (counter == 200) begin
         $display("Done");
         $finish;
      end 
     counter <= counter + 1;
   endrule

   ////////////////////////////////////////
   // default state is IDLE

   // the point of a one hot is for each state to
   // look at one and only one bit to see if we are in the
   // proper state.  But this means that the compiler
   // can't know for sure that these state are mutually
   // exclusive.   Which means, by default, the compiler
   // will generate conflict resolution logic which is
   // not needed.   But since the whole point of this
   // is to be fast and efficient, the mutually_exclusive
   // pragma tells the compiler to not generate this
   // conflict resolution logic (see reference guide).
   // Bluesim will also assert that these states never
   // fire in the same cycle.
   (* mutually_exclusive = "stateIdle, stateStep1, stateStep2, stateStop" *)
   rule stateIdle ( state[idle] == 1 );
      $display("Counter = %03d, State: IDLE", counter);

      if (counter % 4 == 0) 
         // we want to generate the proper bit pattern
         // wrapping this in a function keeps it simple :)
         state <= toState( step1 );
   endrule
   
   ////////////////////////////////////////
   // state is update to STEP1

   // we are picking out the one bit we need to check
   // rather than comparing against all 4 bits
   // i.e. state == 4'b0010
   rule stateStep1 ( state[step1] == 1 );
      $display("Counter = %03d, State: STEP1", counter);

      // TODO: add some action here for this state as required

      if (restart)
         state <= toState( idle );
      else if (counter % 4 == 0)
         state <= toState( step2 );

      // else 
      //   stay in this state
   endrule
   
   ////////////////////////////////////////
   // state is update to STEP1
   rule stateStep2 ( state[step2] == 1 );
      $display("Counter = %03d, State: STEP2", counter);

      // TODO: add some action here for this state as required

      // just advance to the next state
      // don't restart just yet
      state <= toState( stop );
   endrule
   
   
   ////////////////////////////////////////
   // state is update to STEP1
   rule stateStop ( state[stop] == 1 );
      $display("Counter = %03d, State: STOP", counter);
      
      // TODO: add some action here for this state as required

      // return to the beginning
      state <= toState( idle );
   endrule
   
endmodule
   
endpackage
