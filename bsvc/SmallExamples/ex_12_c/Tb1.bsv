// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 12c
// Stmt FSM examples
// ================================================================

// This file is a just a template showing the basic components
// of declaring, defining, and running and FSM.

package Tb;

// you must import the StmtFSM library to use it
import StmtFSM::*;

(* synthesize *)
module mkTb (Empty);

   // Stmt is a type which is essential a list of actions
   // but with StmtFSMs types built into it
   Stmt test =
   seq
      < sequences and actions here :) >
   endseq;

   // this creates the FSM (see reference manual)
   FSM testFSM <- mkFSM( test );

   // the basic FSM must be started by calling it's "start" method
   // this has an implicit condition that the FSM can not already
   // be running (i.e. it is "done").
   rule startit;
      testFSM.start();  
   endrule

   // we can test to see if the FSM is running or not with "done"
   rule finish (testFSM.done());
      $finish;
   endrule

endmodule
   
endpackage
