// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 12c
// Stmt FSM examples
// ================================================================

/*

The StmtFSM library is an extremely powerful way of specifying
sequences in BSV.  At the heart of this language built into BSV are
rules.. A sequence is a first class object (they can be created in
functions, etc).  A sequence is a series of actions, and each action
has the same requirements of a rule (they stall on implicit
conditions, etc).  Each action is, in fact, generated as a rule in
verilog.
 
These are extremely useful for testbenches, as they allow you to
quickly generate C style loops, sequences, parallel blocks, etc..
 
But, there is no "goto"..  The library tries very hard to generate
efficient logic, but ultimately rules are the best way to generate
the most efficient logic for cases where every last gate counts..
 
*/
 
package Tb;

// you must import the StmtFSM library to use this
import StmtFSM::*;

// I also want to be able to generate some implcit conditions
import FIFOF::*;

(* synthesize *)
module mkTb (Empty);

   FIFOF#(int) fifo <- mkFIFOF;
   Reg#(Bool)  cond <- mkReg(True);
   Reg#(int)     ii <- mkRegU;
   Reg#(int)     jj <- mkRegU;
   Reg#(int)     kk <- mkRegU;

   function Action functionOfAction( int x );
      return action
                $display("this is an action inside of a function, writing %d", x);
                ii <= x;
             endaction;
             // note the semi colon here, This is confusing at first, but just
             // remember that return <item> needs a semi colon after it 
             // like "return foo ;"
   endfunction

   function Stmt functionOfSeq( int x );
      // this will make more sense later skip this for now
      return seq
                action
                   $display("sequence in side function %d", x);
                   ii <= x;
                endaction
                action
                   $display("sequence in side function %d", x+1);
                   ii <= x+1;
                endaction
             endseq; 
   endfunction

   //////////////////////////////////////////////////////////////////////
   // type "Stmt" is a sequence of action

   Stmt test =
   seq
      // something of type "action" can be put on one line by itself
      $display("This is action 1"); // cycle 1

      $display("This is action 2"); // cycle 2

      $display("This is action 3"); // cycle 3

      // an action with several actions still only take one cycle
      action
         $display("This is action 4, part a");
         $display("This is action 4, part b");
      endaction

      ////////////////////////////////////////
      // use functions to create actions that you are repeating a lot
      functionOfAction( 10 );

      functionOfAction( 20 );

      functionOfAction( 30 );

      ////////////////////////////////////////
      // I can certainly also create a function with my sequence and
      // call it as needed
      functionOfSeq( 50 );

      ////////////////////////////////////////
      // this whole action is just one cycle when it fires
      action
         $display("This is action 5");
         if (cond)
            $display("And it may have several actions inside of it");
      endaction
      
      ////////////////////////////////////////
      // this is a valid "nop".. It's an action - it's noAction!
      noAction;

      ////////////////////////////////////////
      // you can also quickly specify some number of actions
      // this repeats 4 cycles of "noAction"
      repeat (4) noAction;

      // this prints out 4 cycles worth of "Repeating action"
      repeat (4) action
                     $display("Repeating action!");
                     $display("Check it out");
                 endaction;

      ////////////////////////////////////////
      // this doesn't mean much here, but sequences can be
      // with in sequences
      seq
         noAction;
         noAction;
      endseq
      
      ////////////////////////////////////////
      // in fact, and if condition is exactly this
      if ( cond ) seq
         // if cond is true, then execute this sequence
         $display("If seq 1");  // cycle 1
         $display("If seq 2");  // cycle 2
         $display("If seq 3");  // cycle 3
      endseq
      else seq
         // if cond is false, then execute this sequence
         action
            $display("Else seq 1");
         endaction
         action
            $display("Else seq 2");
         endaction
      endseq
      
      ////////////////////////////////////////
      // noice this is different that if/else inside a action
      // this takes one cycle when it executes
      action
         if (cond)
            $display("if action 1");
         else
            $display("else action 1");
      endaction

      ////////////////////////////////////////
      // notice now that implicit conditions still matter
      // these enq will cause the FSM to stall on the action
      // with false implicit condition
      // I am forcing this by having a rule pull
      // data out of the fifo, but only every 4 cycles
      // this means it will fill up, then only
      // add the second two when the rule pulls out the data
      action
         $display("Enq 10 at time ", $time);
         fifo.enq( 10 );
      endaction
      action
         $display("Enq 20 at time ", $time);
         fifo.enq( 20 );
      endaction
      action
         $display("Enq 30 at time ", $time);
         fifo.enq( 30 );
      endaction
      action
         $display("Enq 40 at time ", $time);
         fifo.enq( 40 );
      endaction
      
      ////////////////////////////////////////////////////////////
      // await is used to generate an implicit condition
      // await is an action which can be merged into another action
      // in this case I wanted to print a message when we were done
      // while the fifo is notEmpty, the state machine
      // sits here and no actions fire
      action
         $display("Fifo is now empty, continue...");
         await( fifo.notEmpty() == False );
      endaction
      
      ////////////////////////////////////////
      // in an if statement with implicit conds,
      // the if/else evaluates every cycle
      // and only when a path can be executed does it execute
      // specifically, remember that in the example below
      // if cond is true and A is stalled due to false implicit conditions
      // then this if/else stalls
      // careful though: if the (cond) changes and becomes false many cycles later
      // and actionB is ready to fire, then it fires...
      if ( cond )
         noAction; // A
      else
         noAction; // B    

      ////////////////////////////////////////////////////////////
      // for loops are handy
      // but notice one small quirk, the loop body
      // takes two cycles, one for the compare/first action
      // a second action to increment.  This is done
      // so the increment doesn't interfer with the
      // last step (which may be complicated in a big machine)
      for (ii <= 0; ii < 10; ii <= ii + 1) seq
         $display("For loop step %d at time ", ii, $time);
      endseq
      
      ////////////////////////////////////////////////////////////
      // a while loop has better control
      // the init is one cycle
      ii <= 0;

      // the while check and action is one cycle also
      // (notice the time difference - it is every cycle now)
      while (ii < 10) seq
         action
            $display("While loop step %d at time ", ii, $time);
            ii <= ii + 1;
         endaction
      endseq

      ////////////////////////////////////////////////////////////
      // we can nest all the control thingies as deep as it
      // function makes sense...
      for (ii <=0; ii<=4; ii<=ii+1)
         for (jj <=0; jj<=5; jj<=jj+1)
            for (kk <=0; kk<=3; kk<=kk+1) seq
               action
                  if (kk == 0)
                     $display("kk = 000, and ii=%1d, jj=%1d", ii, jj );
                  else
                     $display("kk = --%1d, and ii=%1d, jj=%1d", kk, ii, jj );
               endaction
             endseq

      ////////////////////////////////////////////////////////////
      // and it get really tricky we can use "par/endpar"
      // in a par (parallel) block, each line is a
      // sequence or action that is execute in parallel.
      // the whole par block completes when all
      // sub sequences finish up
      par
         $display("Par block statement 1 at ", $time);

         $display("Par block statement 2 at ", $time);
         
         seq
            $display("Par block statement 3a at ", $time);
            $display("Par block statement 3b at ", $time);
         endseq

      endpar
      $display("Par block done!");

      // note that these actions/sequences again may stall
      // if there are implicit conditions... since these
      // statements run in paralell, we expect each sequence
      // to run to completion as it can (and there may be
      // conflicts with other statements in the sequences)
      // so it can be tricky to see exactly what is going to happen
      // for example
      par
         seq
            ii <= 2;
            ii <= 3;
         endseq

         seq
            ii <= 1;
            ii <= 0;
         endseq
         
         ii <= 10;

      endpar
      // but then you wouldn't do this without being more careful
      // (would you ?)

      $display("par block conflict test, ii = ", ii);

      // note the warnings.. Here is one:

      // Warning: "Tb.bsv", line 36, column 8: (G0036)
      //   Rule "testFSM_mod_list_1_vs_vs_v_action_l256c16" will appear to fire before
      //   "testFSM_mod_list_1_vs_v_action_l252c13" when both fire in the same clock
      //     cycle, affecting:
      //   calls to ii.write vs. ii.write

      // one VERY helpful thing to note is the generated name
      // has at the end of it a useful piece of information
      //    testFSM_mod_list_1_vs_vs_v_action_l256c16
      // the last character are often _l<line>_c<char>
      // so in this case, I can see that the warning
      // is talking about line 256, char 16

      ////////////////////////////////////////////////////////////
      // finally we are done
   endseq;

   // this is how we create a generic FSM
   // interface type FSM with instance name testFSM
   // mkFSM takes the sequence name as a parameter
   //
   // The FSM interface has a few methods (see reference manual)
   // method Action start() -> start running state machine, if it's not running already
   // method Bool done()    -> return true if FSM is not running 
   //
   FSM testFSM <- mkFSM( test );

   //////////////////////////////////////////////////////////////////////
   // rule empty fifo sporatically via this FSM
   Stmt rcvr =
   seq
      // loop forever, watching for data out of the fifo
      while(True) seq

         action 
            $display("fifo popped data", fifo.first()); 
            fifo.deq(); 
         endaction

         repeat (5) noAction;
      endseq
   endseq;
   
   FSM rcvrFSM <- mkFSM( rcvr );
   
   //////////////////////////////////////////////////////////////////////
   // start main loop
   rule startit;
      // start fsm ( testFSM.done()==True is an implicit condition)
      //   this means that this rule can't fire again until the
      //   fsm is done
      testFSM.start();  

      // start them both
      rcvrFSM.start();
   endrule

   // stop the test when teh first FSM is finished
   // but the second is left running/looping of course, 
   // I could have just put $finish at the end of the FSM, 
   // but this shows you how to use the done method 
   rule finish (testFSM.done() && !rcvrFSM.done());
      $finish;
   endrule

endmodule
   
endpackage
