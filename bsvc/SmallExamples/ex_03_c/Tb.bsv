// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 3c
// "Module hierarchy and interfaces"
// ActionValue methods.
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package Tb;

(* synthesize *)
module mkTb (Empty);
   Dut_ifc dut <- mkDut;

   rule r1;
      int w <- dut.avmeth(10);
      $display ("w = %0d", w);
      if (w > 50) $finish (0);
   endrule

endmodule

// ----------------------------------------------------------------

interface Dut_ifc;
   method ActionValue#(int) avmeth (int v);
endinterface

(* synthesize *)
module mkDut (Dut_ifc);

   Reg#(int) x <- mkReg (0);

   method ActionValue#(int) avmeth (int v);
      x <= x + v;
      return x;
   endmethod

endmodule

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

          int w <- dut.avmeth(10);

This statement invokes the 'avmeth' method with argument 10, which
causes an action inside the module. The method returns a value which
is assigned to the local variable w of type 'int'.

The statement could also have been written with a 'let':
          let w <- dut.avmeth(10);

NOTE PARTICULARLY the use of '<-' instead of '=' for the assignment.
Technically, the type of the expression 'dut.avmeth(10)' is ActionValue#(int), not int.
If we had said:
          int w = dut.avmeth(10);
using '=' instead of '<-', then this would provoke a type error, since
it is an attempt to assign a value of type ActionValue#(int) to a
variable of type int (the next example deliberately does this so you
can see what happens).

[Side comment: A statement like this:
          ActionValue#(int) w = dut.avmeth(10);
 would typecheck correctly, because the types on the left- and
 right-hand sides match.  This is actually a meaningful statement,
 since all types are 'first class' in BSV.  This usage will be
 demonstrated in a later example. ]

When using '<-', the right-hand side must be an expression of type
ActionValue#(t) for some type t.  The statement causes the Action to
be performed, and assigns the returned value to the variable on the
left-hand side, which must be of type t.

       method ActionValue#(int) avmeth (int v);

Declares 'avmeth' to be an ActionValue method with an argument of type
int, and that returns a value of type int.

       method ActionValue#(int) avmeth (int v);
          x <= x + v;
          return x;
       endmethod

As the body of the method indicates, it both performs an action
(incrementing register x by v) and returns a value (the old value of
register x).

If executed with Bluesim with the -cc and -ss flags, we can see this
behavior, i.e., on each clock the current value of the register is
printed, and it is updated by 10.

* ================================================================
*/
