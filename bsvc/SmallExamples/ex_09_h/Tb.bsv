// Copyright 2008 Bluespec, Inc.  All rights reserved.

// section 9 h - Tuples

package Tb;




(* synthesize *)
module mkTb (Empty);

   // "tuples" are essentially a collection of data with
   // types defined in each field.  Tuples in Bluespec are
   // size 2,3,4,5,6,7,8,9 meaning they can have 2 to 9 places.
   
   // as a rule of thumb, structs are a better choice. It has a name,
   // and each field is named... But on some occasions, you just
   // want to collect some fields and pass them around without a type
   // name (useful in some parameterization cases)..

   // But unlike a normal concatination, like one might do in verilog
   // i.e. presume a is 32 bits, b is 1 bit
   //      then we can assign a 33 bit reg = { a,b }
   //                      OR   33 bit reg = { b,a }
   //                   and there is no warning, etc,,

   // But in Bsv, the fields are typed in the order they are defined
   // So we can't necessarily just mix up the fields..  This helps
   // keep things in order


   Reg#(int) step <- mkReg(0);


   rule init ( step == 0 );
      $display("=== step 0 ===");
      
      // let's start with a simple Tuple2
      // Tuple2( a, b ) is the type definition
      // and tuple2 allows us to create a value on the fly

      Tuple2#( Bool, int ) foo = tuple2( True, 25 );

      // we can read the value of each field using "tpl_"
      
      Bool field1 = tpl_1( foo ); // this is value 1 in the list
      int  field2 = tpl_2( foo ); // this is value 2 in the list

      // but, unforunately, we can't write only one field
      // We need to rebuild the tuple.. Of course you can bury this
      // in a function for you own use..
      foo = tuple2( !field1, field2 );

      $display("tpl_1(foo) = ", tpl_1( foo ));
      $display("tpl_2(foo) = ", tpl_2( foo ));

      // NOTE:  also that the compiler will error if you try to do this:
      // even though foo2 is 33 bits wide, it is a Bool, int
      // it as to be assigned with a typle( Bool, int ); 
      // Tuple2#( Bool, int ) foo2 = tuple2( 25, True );      

      step <= step + 1;
   endrule

   rule step1 ( step == 1 );
      $display("=== step 1 ===");
      // similary, typles can be 3,4,5,6
      // Here is a 4, just to show tpl_3, 4, etc( for 9, you go upto tpl_9)
      Tuple4#( Bool, UInt#(4), Int#(8), Bit#(12) ) bar = tuple4( True, 0, -2, 5 );

      $display("tpl_1(bar) = ", tpl_1(bar));
      $display("tpl_2(bar) = ", tpl_2(bar));
      $display("tpl_3(bar) = ", tpl_3(bar));
      $display("tpl_4(bar) = ", tpl_4(bar));

      step <= step + 1;
   endrule

   rule step2 ( step == 2 );
      $display("=== step 2 ===");
      
      // and this is a type, which can be passed via interface, method, function, etc
      // just like any other type
      
      // But, generally, I prefer to use a structure since the fields
      // are named, I can access each field individually if I need to
      // and there's no confusion the type name (since it has one)

      $display ("All done");
      $finish (0);
   endrule

endmodule: mkTb

endpackage: Tb

