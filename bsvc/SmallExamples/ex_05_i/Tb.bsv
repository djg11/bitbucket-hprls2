// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 5i
// "Scheduling"
// Conditionals in rules, and effect on scheduling: aggressive-conditions
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package Tb;

import FIFO::*;

// ================================================================
// This is a made-up example, so the exact functionality is not important.

(* synthesize *)
module mkTb (Empty);

   Reg#(int) cycle <- mkReg (0);

   FIFO#(int) f0 <- mkSizedFIFO (3);
   FIFO#(int) f1 <- mkSizedFIFO (3);

   // ----------------
   // RULES

   rule count_cycles;
      cycle <= cycle + 1;
      if (cycle > 15) $finish(0);
   endrule

   rule rA;
      if (pack (cycle)[0] == 0) begin
         f0.enq (cycle);
         $display ("%0d: Enqueuing %d into fifo f0", cycle, cycle);
      end
      else begin
         f1.enq (cycle);
         $display ("%0d: Enqueuing %d into fifo f1", cycle, cycle);
      end
   endrule

   rule rB;
      f0.deq ();
   endrule

endmodule: mkTb

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

This is a made-up example to show demonstrate the effect on rule
scheduling of the compiler flag 'aggressive-conditions' and so the
exact functionality is not important.

First, we instantiate two FIFOs f0 and f1 of depth 3 (using
mkSizedFIFO).

Rule rA tries to enqueue the cycle count into FIFO f0 on even cycles,
and into FIFO f1 on odd cycles.  Rule rB simply drains FIFO f0.

Since FIFO f1 is not drained, it will become full after 3 enqueues,
after which the f1.enq() method will be disabled.  So, clearly rule rA
cannot do anything on odd cycles.  The question is, can rule rA fire
on even cycles, since in those cases it is not trying to enqueue on
f1?  The answer is no if we do not use the compiler's
'-aggressive-conditions' flag, and yes if we do.

The Makefile contains two lines for BSC_FLAGS, one with
'-aggressive-conditions' and one without.  Let's try compiling both
ways.

----------------
Without '-agressive-conditions'

If we compile with the 'show-schedule' flag and without the
'-aggressive-conditions' flag, the compile reports the following
scheduling information about rule rA:

    Rule: rA
    Predicate: f1.i_notFull && f0.i_notFull
    Blocking rules: (none)

Thus, the rule will not fire whenever f1 is full, regardless of
whether it wants to enqueue on f1 or not.  Indeed, when we run the
program, we see that the rule stops firing

    0: Enqueuing           0 into fifo f0
    1: Enqueuing           1 into fifo f1
    2: Enqueuing           2 into fifo f0
    3: Enqueuing           3 into fifo f1
    4: Enqueuing           4 into fifo f0
    5: Enqueuing           5 into fifo f1

after three enqueues on f1.

----------------
With '-agressive-conditions'

If we compile with the 'show-schedule' flag and with the
'-aggressive-conditions' flag, the compile reports the following
scheduling information about rule rA:

    Rule: rA
    Predicate: (cycle.read [32'd0]) ? f1.i_notFull : f0.i_notFull
    Blocking rules: (none)

Now you can see that the rule predicate is more 'agressive', more
fine-grained in checking whether the rule can fire or not.  It only
checks whether f1 is full if it needs to enqueue on f1, and only
checks whether f0 is full if it needs to enqueue on f0.

Thus, the rule can continue to fire on even cycles, when it is not
attempting to enqueue on f1.  Indeed, when we run the program, we see
that the rule continues firing:

    0: Enqueuing           0 into fifo f0
    1: Enqueuing           1 into fifo f1
    2: Enqueuing           2 into fifo f0
    3: Enqueuing           3 into fifo f1
    4: Enqueuing           4 into fifo f0
    5: Enqueuing           5 into fifo f1
    6: Enqueuing           6 into fifo f0
    8: Enqueuing           8 into fifo f0
    10: Enqueuing          10 into fifo f0
    12: Enqueuing          12 into fifo f0
    14: Enqueuing          14 into fifo f0
    16: Enqueuing          16 into fifo f0

Until cycle 5 it enqueue on every cycle, alternating three items on f0
and f1, respectively.  After that, it only enqueues on even cycles, on
f0.

* ================================================================
*/
