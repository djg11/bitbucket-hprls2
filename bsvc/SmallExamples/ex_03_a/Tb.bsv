// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 3a
// "Module hierarchy and interfaces"
// A simple module hierarchy with multiple levels and instances.
// (Similar to example in Lecture 4 of training slides)
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package Tb;

(* synthesize *)
module mkTb (Empty);
   Empty m3 <- mkM3;
endmodule

// ----------------------------------------------------------------

(* synthesize *)
module mkM3 (Empty);

   Reg#(int) x   <- mkReg (10);
   M1_ifc    m1b <- mkM1 (20);
   M2_ifc    m2  <- mkM2 (30);

   rule justonce;
      $display ("x = %0d, m1b/x = %0d, m2/x = %0d, m2/m1a/x = %0d",
                 x, m1b.read_local_x, m2.read_local_x, m2.read_sub_x);
      $finish (0);
   endrule

endmodule

// ----------------------------------------------------------------

interface M2_ifc;
   method int read_local_x ();
   method int read_sub_x ();
endinterface

// DJG I have deleted the synthesis directive here since there is a top-level free parameter.
// (* synthesize *)
module mkM2 #(parameter int init_val) (M2_ifc);

   M1_ifc  m1a <- mkM1 (init_val + 10);
   Reg#(int) x <- mkReg (init_val);

   method int read_local_x ();
      return x;
   endmethod

   method int read_sub_x ();
      return m1a.read_local_x;
   endmethod

endmodule: mkM2

// ----------------------------------------------------------------

interface M1_ifc;
   method int read_local_x ();
endinterface

// DJG I have deleted the synthesis directive here since there is a top-level free parameter.
// (* synthesize *)
module mkM1 #(parameter int init_val) (M1_ifc);

   Reg#(int) x <- mkReg (init_val);

   method int read_local_x ();
      return x;
   endmethod

endmodule: mkM1

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

When instantiated, this produces the following module hierarchy:
   top (instance of mkTop)
      m3 (instance of mkM3)
         m1a (instance of mkM1)
         m2 (instance of mkM2)
            m1b (another instance of mkM1)

    module mkM3 (Empty);
       ...
       M1_ifc    m1b <- mkM1 (20);

Instantiate module mkM1 inside the module M3 with static elaboration
parameter 20; the interface provided by the instantiation is bound to
the variable m1b of type M1_ifc.

          $display ("x = %0d, m1b.x = %0d, m2.x = %0d, m2.m1a.x = %0d",
                     x, m1b.read_local_x, m2.read_local_x, m2.read_sub_x);

Display the 'x's in each module instance, using method invocations to
access 'x's in sub-modules.

    module mkM2 #(parameter int init_val) (M2_ifc);

The '#(...)' section specifies the parameters for module M2.  The
'parameter' keyword specifies that this is a static parameter that
becomes a Verilog parameter and not a Verilog port (input wires).
Wherever mkM2 is instantiated, the actual parameter must be a static
(compile-time) value.

       M1_ifc  m1a <- mkM1 (init_val + 10);

Instantiates the module mkM1 inside the module M2, passing it a
parameter that is mkM2's parameter plus 10.

       method int read_sub_x ();
          return m1a.read_local_x;
       endmethod

Note that this method, in turn, just invokes the 'read_local' method
of m1a.  I.e., one can have 'chains' of method calls across the module
hierarchy.

Note that there are 4 registers called 'x', one at each level of the
module hierarchy.  There are only 3 textual instances of 'mkReg', but
because mkM1 is instantiated twice, we get 4 registers after static
elaboration.

Note that there are 2 textual definitions of methods called
'read_local', but there are 3 invocations of methods called
'read_local':

  m3 invoking m1b.read_local
  m3 invoking m2.read_local
    m2 invoking m1a.read_local

BSV uses standard programming language "scope" rules to resolve
identifiers and so there is no ambiguity about the different uses of
'x' or 'read_local'.

We recommend executing with Bluesim using the -ss flag, which displays
module and state hierarchy.

We also recommend executing with your favorite Verilog simulator to
see the traditional Verilog hierarchy and state displays.

* ================================================================
*/
