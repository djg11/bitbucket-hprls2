// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 12a
// "FSM" by rule
// ================================================================

/* 
   === commentary ===
 
Notice below that since rules are operation centric, we define a rule
foreach state, and update the state with the various arc information
with in each state..  Standard RTL requires that you mux all the
possible values for "state" to one mux and generate the select signals
yourself.  With BSV, the compiler generates the muxing for you!  This
means each rule is self contained, and we think far more
understandable.  Need to add another state ?  Just add another rule!
Need to add another exit arc from a state ?  Just add it to the
if/else conditions that set "state" for that rule.

All the same concepts of rules apply otherwise.. Implicit (or ready)
conditions will cause a rule not to fire.  Additionally, the explicit
(or rule) condition can be more compilicated as you desire.. The end
effect is that when the rule fires, you specify the state update and
actions for that rule.
 
Also keep in mind, with the future examples in this section, that 
there are other ways to implement state machines, which may be 
simpler to use in some cases, but rules are the default, absolutely
most powerful way to implement a state machine.

*/

package Tb;

// this is a nice way to have names that mean something
// and it is typed so that we can't misuse it directly
typedef enum { IDLE, STEP1, STEP2, STOP } State deriving(Bits,Eq);

// ----------------------------------------------------------------
// A top-level module connecting the stimulus generator to the DUT

(* synthesize *)
module mkTb (Empty);

   // set the default state to IDLE
   // out of reset, this means FSM is IDLE
   Reg#(State)     state <- mkReg(IDLE);

   
   // counter just to generate some conditions to move state along
   Reg#(int)     counter <- mkReg(0);
   Reg#(Bool)    restart <- mkReg(False);

   rule runCounter;
      if (counter == 200) begin
         $display("Done");
         $finish;
      end
      counter <= counter + 1;
   endrule

   // This isn't really a specific state machine with expected behavior.  I fundumentally
   // just want to so each rule firing for a cycle or cycles, then how one branches out
   // of that cycle.  Basically, the idea was to have each state wait for the counter to
   // hit some value before going onto the next state.  The counter is incrementing externally
   // so basically we sit, and some number of cycles later, we go to another state...

   ////////////////////////////////////////
   // default state is IDLE
   rule stateIdle ( state == IDLE );
      $display("Counter = %03d, State: IDLE", counter);

      // arc out of this state when counter = 4,8,12,etc
      if (counter % 4 == 0)
         state <= STEP1;
      // else
      //    stay in this state for one more cycle
      //    we didn't update the state variable so
      //    this rule fires again next cycle (thanks to
      //    the explcit condition of this rule)
   endrule
   
   ////////////////////////////////////////
   // state is update to STEP1
   rule stateStep1 ( state == STEP1 );
      $display("Counter = %03d, State: STEP1", counter);

      // TODO: add some action here for this state as required

      // if "restart" is set by something (in this case, it's a
      // dummy reg, reset to false), then restart state machine
      if (restart)
         state <= IDLE;

      // again, if counter == 8,16,24,32 etc
      // move onto the next state
      else if (counter % 8 == 0)
         state <= STEP2;

      // else 
      //   stay in this state for one more cycle
   endrule
   
   ////////////////////////////////////////
   // state is update to STEP1
   rule stateStep2 ( state == STEP2 );
      $display("Counter = %03d, State: STEP2", counter);

      // TODO: add some action here for this state as required

      // just advance to the next state
      // ignore restart signal here
      state <= STOP;
   endrule
   
   
   ////////////////////////////////////////
   // state is update to STEP1
   rule stateSTOP ( state == STOP );
      $display("Counter = %03d, State: STOP", counter);
      
      // TODO: add some action here for this state as required

      // return to the beginning, and in this case, start again
      // since IDLE watches to counter to see when it can advance
      state <= IDLE;
   endrule
   
endmodule
   
endpackage
