// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 2a
// "Rules, Registers, and FIFOs"
// One register of type Reg#(int), a rule that updates it.
// Contains: local variable definition, non-blocking assignment, old and new values
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package Tb;

(* synthesize *)
module mkTb (Empty);

   Reg#(int) x <- mkReg (23);

   rule countup (x < 30);
      int y = x + 1;
      x <= x + 1;
      $display ("x = %0d, y = %0d", x, y);
   endrule

   rule done (x >= 30);
      $finish (0);
   endrule

endmodule: mkTb

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

       Reg#(int) x <- mkReg (23);

Uses module 'mkReg' to instantiate a register (a sub-module).
The parameter 23 specifies the initial (reset) value of the register.
The instantiation 'returns', or 'provides', or 'yields' an interface
which we bind to the variable 'x', of type 'Reg#(int)'.
I.e., x is bound to an interface containing _read() and _write() methods
which constitute a register interface.
Further only values of type 'int' can be read from or written to x.

       rule countup (x < 30);
          int y = x + 1;
          x <= x + 1;
          $display ("x = %0d, y = %0d", x, y);
       endrule

This rule can 'fire', or 'execute', only when x < 30.
Not that the $display shows different values for x and y.
'y' acts like a variable in a traditional programming language,
so it gets bound to the value of 'x' plus 1, and that's what is displayed.
However, the assignment 'x <= x + 1' is a shorthand for the Action 'x._write(x+1)'
i.e., calling the _write() method of register interface x with the value (x+1)
The effect of an 'Action' only take effect after the rule.
Thus, the value of x read by the $display statement is still the old value of x.
More generally: the textual order of Actions in a rule is irrelevant--all their
effects happen simultaneously ('atomically') and are visible only
after this rule's execution.

       rule done (x >= 30);
          $finish (0);
       endrule

This rule can 'fire', or 'execute', only when x >= 30 and it
terminates execution.

* ================================================================
*/
