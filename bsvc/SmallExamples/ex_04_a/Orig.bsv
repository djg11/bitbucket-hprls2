// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 4a
// "RWires"
// Up-down counter with simultaneous up/down commands. Simple RWire example; Maybe types.
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package Tb;

// ----------------------------------------------------------------
// A top-level module connecting the stimulus generator to the DUT

(* synthesize *)
module mkTb (Empty);

   Counter c1 <- mkCounter_v1;    // Instantiate a version 1 counter
   Counter c2 <- mkCounter_v2;    // Instantiate a version 2 counter (same interface type)

   // ----------------
   // Step 'state' from 1 to 10 and show both counter values in each state

   Reg#(int) state <- mkReg (0);

   rule step_state;
      if (state > 9) $finish (0);
      state <= state + 1;
   endrule

   rule show;
      $display ("In state %0d, counter values are %0d, %0d", state, c1.read (), c2.read ());
   endrule

   // ----------------
   // For counter c1, increment by 5 in states 0..6, decrement by 2 in states 4..10
   // The interesting states are 4..6

   rule incr1 (state < 7);
      c1.increment (5);
   endrule

   rule decr1 (state > 3);
      c1.decrement (2);
   endrule

   // ----------------
   // Same for counter c2 (increment by 5 in states 0..6, decrement by 2 in states 4..10)
   // The interesting states are 4..6

   rule incr2 (state < 7);
      c2.increment (5);
   endrule

   rule decr2 (state > 3);
      c2.decrement (2);
   endrule

endmodule: mkTb

// ----------------------------------------------------------------
// Interface to an up-down counter

interface Counter;
   method int read();                     // Read the counter's value
   method Action increment (int di);      // Step the counter up by di
   method Action decrement (int dd);      // Step the counter down by dd
endinterface: Counter

// ----------------------------------------------------------------
// Version 1 of the counter

(* synthesize *)
module mkCounter_v1 (Counter);
   Reg#(int) value1 <- mkReg(0);  // holding the counter's value

   method int read();
      return value1;
   endmethod

   method Action increment (int di);
      value1 <= value1 + di;
   endmethod

   method Action decrement (int dd);
      value1 <= value1 - dd;
   endmethod

endmodule: mkCounter_v1

// ----------------------------------------------------------------
// Version 2 of the counter

(* synthesize *)
module mkCounter_v2 (Counter);
   Reg#(int) value2 <- mkReg(0);  // holding the counter's value

   RWire#(int) rw_incr <- mkRWire();  // Signal that increment method is being invoked
   RWire#(int) rw_decr <- mkRWire();  // Signal that decrement method is being invoked

   // This rule does all the work, depending on whether the increment or the decrement
   // methods, both, or neither, is being invoked
   (* fire_when_enabled, no_implicit_conditions *)
   rule doit;
      Maybe#(int) mbi = rw_incr.wget();
      Maybe#(int) mbd = rw_decr.wget();
      int   di        = fromMaybe (?, mbi);
      int   dd        = fromMaybe (?, mbd);
      if      ((! isValid (mbi)) && (! isValid (mbd)))
       noAction;
      else if (   isValid (mbi)  && (! isValid (mbd)))
         value2 <= value2 + di;
      else if ((! isValid (mbi)) &&    isValid (mbd))
         value2 <= value2 - dd;
      else // (   isValid (mbi)  &&    isValid (mbd))
         value2 <= value2 + di - dd;
   endrule

   method int read();
      return value2;
   endmethod

   method Action increment (int di);
      rw_incr.wset (di);
   endmethod

   method Action decrement (int dd);
      rw_decr.wset (dd);
   endmethod

endmodule: mkCounter_v2

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

In this example, we instantiate two up-down counters. They have
exactly the same interface type, and they are called from the
testbench, mkTb, in identical ways from apparently identical rules.
However, their implementations result in different scheduling
behaviors, illustrating the use and scheduling properties of RWires.

    module mkTb (Empty);
       Counter c1 <- mkCounter_v1;    // Instantiate a version 1 counter
       Counter c2 <- mkCounter_v2;    // Instantiate a version 2 counter (same interface type)

Instantiates the two versions of the counters, each having the same
interface type.

       // ----------------
       // Step 'state' from 1 to 10 and show both counter values in each state
       ...

This section just instantiates a 'state' register, has a rule to step
it from 0 to 10, and a rule to read and display both counters.

       rule incr1 (state < 7);
          ...
       rule decr1 (state > 3);
          ...
       rule incr2 (state < 7);
          ...
       rule decr2 (state > 3);
          ...

The 'incr' rules try to invoke the 'increment' methods of the two counters in states 0..6.
The 'decr' rules try to invoke the 'decrement' methods of the two counters in states 4..10.
Thus, they try to invoke both methods on each counter in states 4..6.

    interface Counter;
       ...

The Counter interface has methods to read, increment and decrement the
counter.

    module mkCounter_v1 (Counter);
       ...

Version 1 of the counter is straightforward.  It has no independently
running rules.  The 'increment' and 'decrement' methods just do their
required actions.  But note that, because they both read and write
'value1', they conflict, i.e., they cannot both execute in the same
clock.  Specifically, the rules incr1 and decr1 in mkTb, which invoke
these methods, cannot both fire in the same clock.

Version 2 of the counter uses RWires.

    module mkCounter_v2 (Counter);
       ...
       RWire#(int) rw_incr <- mkRWire();  // Signal that increment method is being invoked
       RWire#(int) rw_decr <- mkRWire();  // Signal that decrement method is being invoked

These two RWires are used to communicate from the 'increment' and
'decrement' methods to the 'doit' rule.  Both of them, if driven,
carry an 'int' value.  We will discuss the 'doit' rule after
discussing the interface methods.

       method Action increment (int di);
          rw_incr.wset (di);
       endmethod

       method Action decrement (int dd);
          rw_decr.wset (dd);
       endmethod

Whenever the 'increment' method is invoked, it drives the RWire
'rw_incr', carrying the value di on it.  Specifically, whenever the
rule that invokes 'increment' fires, the RWire is driven.  Similarly,
whenever the 'decrement' method is invoked, it drives the RWire
'rw_decr'.  In this example those rules are 'incr2' and 'decr2' in
mkTb.

These two methods do not conflict (because they are driving different
RWires).  Hence, these two methods can be invoked in the same clock,
i.e., the rules 'incr2' and 'decr2' in mkTb can fire in the same
clock.

   (* fire_when_enabled, no_implicit_conditions *)
   rule doit;
      Maybe#(int) mbi = rw_incr.wget();
      Maybe#(int) mbd = rw_decr.wget();
      int   di        = fromMaybe (?, mbi);
      int   dd        = fromMaybe (?, mbd);
      if      ((! isValid (mbi)) && (! isValid (mbd)))
         noAction;
      else if (   isValid (mbi)  && (! isValid (mbd)))
         value2 <= value2 + di;
      else if ((! isValid (mbi)) &&    isValid (mbd))
         value2 <= value2 - dd;
      else // (   isValid (mbi)  &&    isValid (mbd))
         value2 <= value2 + di - dd;
   endrule

This rule does all the work.

We will discuss the '(* fire_when_enabled, no_implicit_conditions *)'
attribute after discussing the body of the rule.

Each '.wget()' method reads an RWire.  For an RWire of type int, the
returned value is of type Maybe#(int), which is
    either 'Invalid'
    or     'Valid' with a value v
Think of it as a 33 bit value, one bit of which is a "Valid" bit.  If
valid, the remaining 32 bits represent a legitimate int.  If invalid,
the remaining 32 bits are unspecified (junk, garbage).

In the current clock, if the RWire is not being driven, then .wget()
returns Invalid.  If the RWire is being driven with value v, the
.wget() returns Valid along with v.

A Maybe value can be queried, as to whether it is valid or not, using
the function 'isValid'.

The value carried on a Maybe value can be extracted using the function
'fromMaybe()'.  The Maybe value is the second argument to this
function.  If it is valid, the function returns the associated value
carried by the Maybe value; if invalid, it returns the "default" value
which is supplied as the first argument.  Here, we suppy '?' as the
default value, which represents a "don't care" value.

The if-then-else structure, therefore, just tests all four
combinations of whether the two RWire-reads are Invalid or Valid, that
is, whether the methods 'increment' and 'decrement' are being invoked
in the current clock cycle (neither, one of them, or both), and
performs the appropriate action.

'noAction' is just a special null Action (skip, do nothing).

Thus, the rule 'doit' performs the appropriate updates to the counter
depending on whether the increment and decrement methods are being
invoked (or neither, or both).

Returning to the attributes '(* fire_when_enabled, no_implicit_conditions *)',
these are intended to further guarantee safety.  That is, even though
the program will work without these attributes, as written, we are
relying on certain scheduling properties of the rule, and it is better
to make this explicit so that (a) the compiler will ensure it, and (b)
it is obvious to anyone reading the program.

What happens if the 'increment' method is invoked in a particular
cycle, and somehow the rule 'doit' does not fire?  Answer: the
increment command and value will be missed (the value is "dropped on
the floor").  To guarantee that this does not happen, we need to
ensure that the rule always fires.

The 'fire_when_enabled' attribute asserts that whenever the rule's
conditions are true, the rule fires. A rule's condition is normally
necessary, but not sufficient, for the rule to fire, because conflicts
with other rules may prevent it from firing even when its condition is
true.  This assertion strengthens it say that the rule's conditions
are indeed sufficient.  This assertion is proved by the compiler
(raising a compile-time error if false).

The 'no_implicit_conditions' attribute asserts that none of methods
invoked in the rule has a condition that could be false.  Since this
rule also has no explicit condition, this means that there are no
firing conditions for the rule.  This assertion is also proved by the
compiler (raising a compile-time error if false).

Together, these assertions ensure that this rule fires on every clock
(the rule has no conditions, explicit or implicit, and is not
prevented from firing due to conflicts with any other rule).

----------------

When compiled, we see the following warning from the compiler:

    Warning: "Tb.bsv", line 18, column 8: (G0010)
      Rule "decr1" was treated as more urgent than "incr1". Conflicts:
        "decr1" cannot fire before "incr1": calls to c1.decrement vs. c1.increment
        "incr1" cannot fire before "decr1": calls to c1.increment vs. c1.decrement

The compiler is stating that, since c1.increment and c1.decrement
conflict (cannot be invoked in the same cycle), it cannot allow rules
incr1 and decr1 to fire in the same cycle, and in absence of any other
guidance, it is picking decr1 over incr1.  Thus, in cycles 0..3, when
only incr1 can fire, it will fire; in cycles 4..6, when both rules are
enabled, it will only allow decr1 to fire; in cycles 7..10, when only
decr1 can fire, it will fire.

On the other hand, there is no such warning about counter c2 and rules
incr2 and decr2 because, as we have seen, they can fire together.  So,
for counter c2, in cycles 4..6, both rules incr2 and decr2 will fire.

When we execute the code, the displayed outputs indeed show this
behavior.

----------------------------------------------------------------
EXERCISE:

(1) Merge the two mkTb rules 'step_state' and 'show' into one rule;
    observe what happens, and explain it. [Hint: compile-time
    scheduling error.]

* ================================================================
*/
