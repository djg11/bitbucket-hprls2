// Copyright 2008 Bluespec, Inc.  All rights reserved.

// section 9 f - Enum, with explicit value

package Tb;

// Enumeration types are useful for all sorts of things.
// The values of the enum start with an upper case letter
// and, by default the first value is assigned 0, the next 1,
// the next 2, etc..  This creates a bit variable as wide
// as needed to hold the largest value..  In this case,
// there are 4 entries, so the values are 0,1,2,3
// so effectively a Bit#(2) is created (BUT this is it's own type
// this can't be assigned directly to a Bit#(2)
typedef enum { IDLE, DOX, DOY, FINISH } Tenum1 deriving(Bits, Eq);

// you can define specific values to the fields if you want
// if you don't assign a field though, it gets assigned
// the value of the previous field + 1, so S1 in this case is 44
// again, the width of the variable needed (for a register, for instance)
// is as wide as is needed to hold the largest value (in this case 254)
// so it is 8 bit wide.
typedef enum { S0=43, S1, S2=20, S3=254 } Tenum2 deriving(Bits, Eq);

(* synthesize *)
module mkTb (Empty);

   Reg#(int) step <- mkReg(0);

   // note I initialize with one of the enum value
   Reg#(Tenum1) r1 <- mkReg( IDLE );

   // Note, I initialize with one of the enum values.
   Reg#(Tenum2) r2 <- mkReg( S3 );


   rule init ( step == 0 );
      $display("=== step 0 ===");
      $display("enum1 IDLE   => ", IDLE);
      $display("enum1 DOX    => ", DOX);
      $display("enum1 DOY    => ", DOY);
      $display("enum1 FINISH => ", FINISH);

      $display("enum2 S0 => ", S0);
      $display("enum2 S1 => ", S1);
      $display("enum2 S2 => ", S2);
      $display("enum2 S3 => ", S3);
      
      step <= step + 1;
   endrule

   rule step1 ( step == 1 );
      $display("=== step 1 ===");
      Tenum1 foo = IDLE;

      // NOTE: you can do normal math on enums // djg? Should be can't ?
      //       they are values, not bits.
      //       uncomment this to see the error
      // foo = foo + 1;

      step <= step + 1;
   endrule

   rule step2 ( step == 2 );
      $display ("All done");
      $finish (0);
   endrule

   ////////////////////////////////////////
   rule watcher (step != 0);
   endrule

endmodule: mkTb

endpackage: Tb

