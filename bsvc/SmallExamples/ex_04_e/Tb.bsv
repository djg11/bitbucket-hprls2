// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 4e
// "RWires"
// Variant of last example (up-down counter) where the decrement
// is by a constant (1), showing use of a PulseWire instead of an
// RWire.
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package Tb;

// ----------------------------------------------------------------
// A top-level module connecting the stimulus generator to the DUT

(* synthesize *)
module mkTb (Empty);

   Counter c1 <- mkCounter_v1;    // Instantiate a version 1 counter
   Counter c2 <- mkCounter_v2;    // Instantiate a version 2 counter (same interface type)

   // ----------------
   // Step 'state' from 1 to 10 and show both counter values in each state

   Reg#(int) state <- mkReg (0);

   rule step_state;
      if (state > 9) $finish (0);
      state <= state + 1;
   endrule

   rule show;
      $display ("In state %0d, counter values are %0d, %0d", state, c1.read (), c2.read ());
   endrule

   // ----------------
   // For counter c1, increment by 5 in states 0..6, decrement by 1 in states 4..10
   // The interesting states are 4..6

   rule incr1 (state < 7);
      c1.increment (5);
   endrule

   rule decr1 (state > 3);
      c1.decrement ();    // Decrement always by 1, so method has no argument
   endrule

   // ----------------
   // Same for counter c2 (increment by 5 in states 0..6, decrement by 1 in states 4..10)
   // The interesting states are 4..6

   rule incr2 (state < 7);
      c2.increment (5);
   endrule

   rule decr2 (state > 3);
      c2.decrement ();    // Decrement always by 1, so method has no argument
   endrule

endmodule: mkTb

// ----------------------------------------------------------------
// Interface to an up-down counter

interface Counter;
   method int read();                     // Read the counter's value
   method Action increment (int di);      // Step the counter up by di
   method Action decrement ();            // Step the counter down by 1
endinterface: Counter

// ----------------------------------------------------------------
// Version 1 of the counter

(* synthesize *)
module mkCounter_v1 (Counter);
   Reg#(int) value1 <- mkReg(0);  // holding the counter's value

   method int read();
      return value1;
   endmethod

   method Action increment (int di);
      value1 <= value1 + di;
   endmethod

   method Action decrement ();
      value1 <= value1 - 1;
   endmethod

endmodule: mkCounter_v1

// ----------------------------------------------------------------
// Version 2 of the counter

(* synthesize *)
module mkCounter_v2 (Counter);
   Reg#(int) value2 <- mkReg(0);  // holding the counter's value

   RWire#(int) rw_incr <- mkRWire();      // Signal that increment method is being invoked
   PulseWire   pw_decr <- mkPulseWire();  // Signal that decrement method is being invoked

   // This rule does all the work, depending on whether the increment or the decrement
   // methods, both, or neither, is being invoked
   (* fire_when_enabled, no_implicit_conditions *)
   rule doit;
      case (tuple2 (rw_incr.wget(), pw_decr)) matches
         { tagged Invalid,   False } : noAction;
         { tagged Valid .di, False } : value2 <= value2 + di;
         { tagged Invalid,   True  } : value2 <= value2 - 1;
         { tagged Valid .di, True  } : value2 <= value2 + di - 1;
      endcase
   endrule

   method int read();
      return value2;
   endmethod

   method Action increment (int di);
      rw_incr.wset (di);
   endmethod

   method Action decrement ();
      pw_decr.send ();
   endmethod

endmodule: mkCounter_v2

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

This is a variant of the previous example, where the decrements are
always by 1 instead of an arbitrary integer.  Therefore, the
'decrement' method now has no argument.  For example:

       rule decr1 (state > 3);
          c1.decrement ();    // Decrement always by 1, so method has no argument
       endrule

    interface Counter;
       ...
       method Action decrement ();            // Step the counter down by 1
    endinterface: Counter

In module mkCounter2, in the previous example we used an RWire rw_decr
to carry a value from the 'decrement' method to the 'doit' rule.  We
could continue to do this, redefining the 'decrement method like this:

       method Action decrement ();    // The method no longer has an argument
          rw_decr.wset (1);           // Pass the int (32b wide) constant 1
       endmethod

----------------
Alternatively, we could modify the RWire declaration so that it
carries a value that is "zero bits" wide:

    module mkCounter_v2 (Counter);
       ...
       RWire#(Int#(0)) rw_decr <- mkRWire();  // Signal that decrement method is being invoked

In the 'decrement' method, we use a 'don't care' value for this 0-bit
integer argument of wset:

       method Action decrement ();
          rw_decr.wset (?);    // 'Don't care' value for the wset argument
       endmethod

And, in the pattern-match in rule 'doit', we use a 'wildcard' symbol
'*' in the patterns to match this value [10]:

       rule doit;
          case (tuple2 (rw_incr.wget(), rw_decr.wget())) matches
             { tagged Invalid,   tagged Invalid   } : noAction;
             { tagged Valid .di, tagged Invalid   } : value2 <= value2 + di;
             { tagged Invalid,   tagged Valid .*  } : value2 <= value2 - 1;
             { tagged Valid .di, tagged Valid .*  } : value2 <= value2 + di - 1;
          endcase
       endrule

----------------
But all these solutions with RWire seem like overkill.  PulseWires
directly express the idea that there is no value carried along with
the Valid/Invalid bit, i.e., the only thing of importance here is the
Valid/Invalid bit.   First, we declare the pulsewire:

    module mkCounter_v2 (Counter);
       ...
       PulseWire   pw_decr <- mkPulseWire();  // Signal that decrement method is being invoked

The PulseWire interface, defined in the library ([B.4.6]), looks like this:

    interface PulseWire;
       method Action send();
       method Bool _read();
    endinterface

Here, the 'send()' method is essentially RWire's 'wset()', except it
has no argument.  The Valid/Invalid status here is returned as the
Bool value of the _read() method.  Because the method is called
'_read()', just like in the Reg#(t) interface, we automatically reuse
BSV's syntactic shorthand where the '_read()' method call will be
implicitly inserted by the compiler, i.e., we don't have to say
_read() explicitly.

Thus, in the 'decrement' method, we just "send" the signal:

       method Action decrement ();
          pw_decr.send ();
       endmethod

And in the 'doit' rule, we implicitly '_read()' the PulseWire and
match it against the constants True and False.

       rule doit;
          case (tuple2 (rw_incr.wget(), pw_decr)) matches
             { tagged Invalid,   False } : noAction;
             { tagged Valid .di, False } : value2 <= value2 + di;
             { tagged Invalid,   True  } : value2 <= value2 - 1;
             { tagged Valid .di, True  } : value2 <= value2 + di - 1;
          endcase
       endrule

* ================================================================
*/
