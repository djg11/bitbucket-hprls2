// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite:
//     Example 4b ("RWires)
//     Example 19a ("Tagged Unions and pattern-matching")
// Same example as 4a (up-down counter) except
// using pattern-matching to query the Maybe types.
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package Tb;

// ----------------------------------------------------------------
// A top-level module connecting the stimulus generator to the DUT

(* synthesize *)
module mkTb (Empty);

   Counter c1 <- mkCounter_v1;    // Instantiate a version 1 counter
   Counter c2 <- mkCounter_v2;    // Instantiate a version 2 counter (same interface type)

   // ----------------
   // Step 'state' from 1 to 10 and show both counter values in each state

   Reg#(int) state <- mkReg (0);

   rule step_state;
      if (state > 9) $finish (0);
      state <= state + 1;
   endrule

   rule show;
      $display ("In state %0d, counter values are %0d, %0d", state, c1.read (), c2.read ());
   endrule

   // ----------------
   // For counter c1, increment by 5 in states 0..6, decrement by 2 in states 4..10
   // The interesting states are 4..6

   rule incr1 (state < 7);
      c1.increment (5);
   endrule

   rule decr1 (state > 3);
      c1.decrement (2);
   endrule

   // ----------------
   // Same for counter c2 (increment by 5 in states 0..6, decrement by 2 in states 4..10)
   // The interesting states are 4..6

   rule incr2 (state < 7);
      c2.increment (5);
   endrule

   rule decr2 (state > 3);
      c2.decrement (2);
   endrule

endmodule: mkTb

// ----------------------------------------------------------------
// Interface to an up-down counter

interface Counter;
   method int read();                     // Read the counter's value
   method Action increment (int di);      // Step the counter up by di
   method Action decrement (int dd);      // Step the counter down by dd
endinterface: Counter

// ----------------------------------------------------------------
// Version 1 of the counter

(* synthesize *)
module mkCounter_v1 (Counter);
   Reg#(int) value1 <- mkReg(0);  // holding the counter's value

   method int read();
      return value1;
   endmethod

   method Action increment (int di);
      value1 <= value1 + di;
   endmethod

   method Action decrement (int dd);
      value1 <= value1 - dd;
   endmethod

endmodule: mkCounter_v1

// ----------------------------------------------------------------
// Version 2 of the counter

(* synthesize *)
module mkCounter_v2 (Counter);
   Reg#(int) value2 <- mkReg(0);  // holding the counter's value

   RWire#(int) rw_incr <- mkRWire();  // Signal that increment method is being invoked
   RWire#(int) rw_decr <- mkRWire();  // Signal that decrement method is being invoked

   // This rule does all the work, depending on whether the increment or the decrement
   // methods, both, or neither, is being invoked
   (* fire_when_enabled, no_implicit_conditions *)
   rule doit;
      case (tuple2 (rw_incr.wget(), rw_decr.wget())) matches
         { tagged Invalid,   tagged Invalid   } : noAction;
         { tagged Valid .di, tagged Invalid   } : value2 <= value2 + di;
         { tagged Invalid,   tagged Valid .dd } : value2 <= value2 - dd;
         { tagged Valid .di, tagged Valid .dd } : value2 <= value2 + di - dd;
      endcase
   endrule

   method int read();
      return value2;
   endmethod

   method Action increment (int di);
      rw_incr.wset (di);
   endmethod

   method Action decrement (int dd);
      rw_decr.wset (dd);
   endmethod

endmodule: mkCounter_v2

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

Most of this example is identical to the previous one, except for the
style in which we have written the body of the rule 'doit'.  Instead
of this:

       (* fire_when_enabled, no_implicit_conditions *)
       rule doit;
          Maybe#(int) mbi = rw_incr.wget();
          Maybe#(int) mbd = rw_decr.wget();
          int   di        = fromMaybe (?, mbi);
          int   dd        = fromMaybe (?, mbd);
          if      ((! isValid (mbi)) && (! isValid (mbd)))
             noAction;
          else if (   isValid (mbi)  && (! isValid (mbd)))
             value2 <= value2 + di;
          else if ((! isValid (mbi)) &&    isValid (mbd))
             value2 <= value2 - dd;
          else // (   isValid (mbi)  &&    isValid (mbd))
             value2 <= value2 + di - dd;
       endrule

we now write this:

       (* fire_when_enabled, no_implicit_conditions *)
       rule doit;
          case (tuple2 (rw_incr.wget(), rw_decr.wget())) matches
             { tagged Invalid,   tagged Invalid   } : noAction;
             { tagged Valid .di, tagged Invalid   } : value <= value + di;
             { tagged Invalid,   tagged Valid .dd } : value <= value - dd;
             { tagged Valid .di, tagged Valid .dd } : value <= value + di - dd;
          endcase
       endrule

The expression 'tuple2(..., ...)' evaluates to a "two-tuple", or a
"pair".  Think of it as a 'struct' with two fields that are filled in
with the values of the two expressions.

The 'case (e) matches ... endcase' construct is called a
"pattern-matching" case (because of the presence of the keyword
'matches'), i.e., it selects one of the four lines that follow based
on the first one that "matches the pattern" of the value of expression
e.  The four lines, in this example, enumerate all four combinations
of whether the two RWire reads are Invalid or Valid, that is, whether
the methods 'increment' and 'decrement' are being invoked in the
current clock cycle (neither, one of them, or both).  2-tuples are
matched with patterns like '{pat1, pat2}', where pat1 and pat2 are
themselves patterns matching the first and second components of the
2-tuple, respectively.  A pattern like 'tagged Valid .di' will only
match a valid RWire value and, further, allows us to use the variable
name 'di' in the arm of the case statement.  Thus, for example, the
second line of the case statement can be read as follows:

 "If the value of rw_incr.wget() is valid and carries a value di
  (which must be an int), and the value of rw_decr.wget is invalid,
  then just increment value by di."

Note that with this pattern-matching notation, when the value is
Invalid, it is impossible to accidentally read the remaining 32 bits
(which are garbage).  Thus, pattern-matching notation not only
enhances readability (and hence maintainability), but also safety
(correctness).

When we execute the code, the displayed outputs indeed show the same
behavior as the previous example.

* ================================================================
*/
