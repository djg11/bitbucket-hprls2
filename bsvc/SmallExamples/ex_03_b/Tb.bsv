// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 3b
// "Module hierarchy and interfaces"
// Implicit conditions of methods.
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package Tb;

(* synthesize *)
module mkTb (Empty);
   Dut_ifc dut <- mkDut;

   rule r1;
      let x1 = dut.f1;
      $display ("x1 = %0d", x1);
      if (x1 > 10) $finish (0);
   endrule

   rule r2;
      let x2 = dut.f2;
      $display ("    x2 = %0d", x2);
      if (x2 > 10) $finish (0);
   endrule

endmodule

// ----------------------------------------------------------------

interface Dut_ifc;
   method int f1 ();
   method int f2 ();
endinterface

(* synthesize *)
module mkDut (Dut_ifc);

   Reg#(int) x   <- mkReg (0);

   rule count;
      x <= x + 1;
   endrule

   method int f1 ();
      return x;
   endmethod

   method int f2 () if (is_even(x));
      return x;
   endmethod

endmodule

function Bool is_even (int v);
   return ((pack (v))[0] == 0);
endfunction

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

       rule r1;
          let x1 = dut.f1;
          ...
       rule r2;
          let x2 = dut.f2;
          ...

These two rules look very similar, almost identical.

    interface Dut_ifc;
       method int f1 ();
       method int f2 ();
    endinterface

The interface contains two methods with the same type signature.

       method int f1 ();
          return x;
       endmethod

       method int f2 () if (is_even(x));
          return x;
       endmethod

The two methods are identical except that f2 has an 'implicit
condition', it can be invoked only when x is even.

    function Bool is_even (int v);
       return ((pack (v))[0] == 0);
    endfunction

This function tests whether an int is even.
'pack(v)' converts the 'int' to bits;
[0] selects the lsb (least significant bit);
and it tests if this is 0.

When executed, one can observe that rule r1 fires on every clock, and
rule r2 fires on every other clock.
(With Bluesim, use the -cc and -r flags to see which rules fire on
each clock.)

* ================================================================
*/
