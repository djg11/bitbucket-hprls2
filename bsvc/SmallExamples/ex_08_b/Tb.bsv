// Copyright 2008 Bluespec, Inc.  All rights reserved.

// section 8 b

package Tb;

// we are going to be discussing the basics of Vectors
// this is a class that needs to be imported like so
import Vector::*;

// I use a FIFO in one of myexamples also
import FIFO::*;

(* synthesize *)
module mkTb (Empty);

   // arrays vs vectors.
   
   // many verilog and vhdl designers will feel that arrays are pretty natural.
   // However, arrays are basically a subset of the "Vector" class.   Vectors
   // are used to group items together (modules, items, etc) just like arrays
   // do, but have extra features and different syntax..
   
   // to create a simple local Array we use this syntax
   // and fill it all with zeroes;
   Int#(16) arr1[4];
   for (Integer i=0; i<4; i=i+1)
      arr1[i] = 0;
   
   // to do the same as vector, we use the syntax below.
   // The interface is Vector#( numberOfItems, type )
   // We create an empty vector by saying "= newVector";
   Vector#(4, Int#(16)) vec1 = newVector;
  
   //////////////////////////////////////////////////////////////////////
   // but we can init the array with a slew of list processing (see reference guide)
   // functions and option, in one line, which some folks prefer.
   // this creates vec2 and assigns to all 0.
   // "replicate" creates as many items as needed of the given type (in this case).

   Vector#(4, Int#(16)) vec2 = replicate( 0 );
   
   // this is the same functionaly as
   Int#(16) vec2a[4];
   for (Integer i=0; i<4; i=i+1)
      vec2a[i] = 0;

   // read and write vec2a and vec2 the exact same way, but vec2's definition
   // only takes one line

   //////////////////////////////////////////////////////////////////////
   // we can use "map" now to run a function across a list to initialize.
   // In list case, rather than a handcoded list, it's "genVector"
   // which creates a list of Integer starting at 0,1,2,3,4, etc
   // this breaks down into
   //    vec3[0] = fromInteger(0)
   //    vec3[1] = fromInteger(1)
   //    vec3[2] = fromInteger(2)
   //    vec3[3] = fromInteger(3)

   Vector#(4, Int#(16)) vec3 = map( fromInteger, genVector );
   
   // i.e the same as

   Int#(16) vec3a[4];
   for (Integer i=0; i<4; i=i+1)
      vec3a[i] = fromInteger(i);

   //////////////////////////////////////////////////////////////////////
   // but we can even write out own function to load whatever
   // we want in each case..   For instance, create yourself
   // a small "times 13" table vector
   function Int#(16) vec3init( Integer i );
      return fromInteger(i * 13);
   endfunction

   Vector#(4, Int#(16)) vec4 = map( vec3init,  genVector );
   
   // i.e the same as

   Int#(16) vec4a[4];
   for (Integer i=0; i<4; i=i+1)
      vec4a[i] = fromInteger(i * 13);
   
   // these list functions are much more "composable", meaning
   // they can be nested as deep as the compiler has access to
   // store memory needed for the operation

   // right now, you may still be wondering "why use functions, maps, etc"
   // future examples will hopefully make this more clear

endmodule: mkTb

endpackage: Tb

