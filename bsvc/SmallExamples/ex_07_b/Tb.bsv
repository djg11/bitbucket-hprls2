// Copyright 2008 Bluespec, Inc.  All rights reserved.

package Tb;

// section 7 b - if else expressions

(* synthesize *)
module mkTb (Empty);
   
   // The bluespec also allows for great freedom of expression ;)
   // if-else statements can be written in several formats
   //
   
   Reg#(int) step <- mkReg(0);

   Reg#(int) a <- mkReg(0);

   rule init ( step == 0 );
      $display("=== step 0 ===");

      // verbose (and it requires the begin/end) and it doesn't
      // appear to be officially supported, but this works right now
      // You'd probably be better off using the supported conditional
      //      int val = (a==0) ? 27 : 45 ;
      int val = begin
                    if (a == 0)  
                       27;  // set val to 27
                    else
                       45;  // set val to 45
                 end;
      
      $display("val = %b", val);

      // or better yet, just use a standard if else
      int val2;
      if (a == 0)  
         val2 = 27;
      else
         val2 = 45;
      
      $display("val2 = %b", val2);

      step <= step + 1;
   endrule

   rule step1 ( step == 1 );
      $display("=== step 1 ===");
      step <= step + 1;
      $display ("All done");
      $finish (0);
   endrule

endmodule: mkTb

endpackage: Tb

