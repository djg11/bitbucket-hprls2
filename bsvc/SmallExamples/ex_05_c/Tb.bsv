// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 5c
// "Scheduling"
// Attributes on rules: showing the difference between 'descending_urgency'
// and 'execution_order'. 
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package Tb;

import FIFO::*;
import GetPut::*;
import ClientServer::*;

// ================================================================
// Exact details of this testbench are not important


(* synthesize *)
module mkTb (Empty);
   Reg#(int) cycle <- mkReg(0);

   Server#(int, int) g <- mkGadget;

   // Send data to gadget every 5 out of 8 cycles
   rule source;
      cycle <= cycle + 1;
      if (pack (cycle)[2:0] < 5) g.request.put (cycle);
   endrule

   rule drain;
      let x <- g.response.get ();
      $display ("%0d: draining %d", cycle, x);
      if (cycle > 20) $finish(0);
   endrule
endmodule: mkTb

// ================================================================
// This module enqueues something on the outfifo whenever it isn't full,
// either a value from infifo (if there is one), or a bubble (otherwise).  It
// also keeps a count of the maximum interval between enqueueing an item and
// enqueuing a bubble (with no intervening item).

// Let us assume that enqueuing an actual available item is more urgent than
// enqueueing a bubble (and so is explicitly specified).

(* synthesize *)
module mkGadget (Server#(int,int));
   int bubble_value = 42;

   FIFO#(int) infifo <- mkFIFO;
   FIFO#(int) outfifo <- mkFIFO;

   Reg#(int) bubble_cycles <- mkReg(0);
   Reg#(int) max_bubble_cycles <- mkReg(0);

   (* descending_urgency="enqueue_item, enqueue_bubble" *)
   rule enqueue_item;
      outfifo.enq(infifo.first);
      infifo.deq;
      bubble_cycles <= 0;
   endrule

   rule inc_bubble_cycles;
      bubble_cycles <= bubble_cycles + 1;
   endrule

   rule enqueue_bubble;
      outfifo.enq(bubble_value);
      max_bubble_cycles <= max(max_bubble_cycles, bubble_cycles);
   endrule

   interface request  = fifoToPut (infifo);
   interface response = fifoToGet (outfifo);

endmodule: mkGadget

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

The exact details of the testbench are not important.  The focus is on
mkGadget and the schedule of its rules, demonstrating that 'earliness'
is not the same as 'urgency'.

Note: we often use the following terms synonymously:
    execution order
    earliness
    TRS order ("term rewriting systems order")
All these refer to the logical order in which rules execute within a
clock cycle such that all methods are called in an order consistent
with their scheduling constraints.

In this line:

       (* descending_urgency="enqueue_item, enqueue_bubble" *)

the user has specified that rule enqueue_item is more urgent than
enqueue_bubble, i.e., if it is possible to enqueue an item, it is more
urgent to do that.

However, note that:
    rule enqueue_item         writes            register bubble_cycles
    rule inc_bubble_cycles    reads & writes    register bubble_cycles
    rule enqueue_bubble       reads             register bubble_cycles

By normal ordering of register read and write methods, this implies
that the 'earliness' ordering (TRS ordering) of these rules must be:

    enqueue_bubble, inc_bubble_cycles, enqueue_item

Indeed, if we compile with the -show-schedule flag, we produce a file
mkGadget.sched whose last few lines show:

    Logical execution order: ...
                             ...
                             enqueue_bubble,
                             inc_bubble_cycles,
                             enqueue_item

Specifically, note that the earliness ordering of enqueue_bubble and
enqueue_item is opposed to their urgency ordering.

In summary:

- 'Urgency' is a user-specified priority indicating in what order of
  importance rules must be considered for firing

- 'Earliness' is the logical sequencing of rules within a clock that
  is induced by the scheduling constraints on the methods they use
  (such as: a read from a register must be earlier than a write to
  that register).

* ================================================================
*/
