// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 5h
// "Scheduling"
// Attributes on rules: fire_when_enabled, no_implicit_conditions
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package Tb;

import GetPut::*;

// ================================================================
// A top-level module that calls a method in the DUT.
// This is a made-up example, so the exact functionality is not important.

(* synthesize *)
module mkTb (Empty);

   Reg#(int) cycle <- mkReg (0);
   Reg#(int) x     <- mkReg (0);

   Put#(int) dut <- mkDut ();

   // ----------------

   rule count_cycles;
      cycle <= cycle + 1;
      if (cycle > 15) $finish(0);
   endrule

   (* descending_urgency = "r2, r1" *)

   (* fire_when_enabled, no_implicit_conditions *)
   rule r1 (pack (cycle)[2:0] != 7);
      dut.put (x);
      x <= x + 1;
   endrule

   rule r2;
      x <= x + 2;
   endrule

endmodule: mkTb

// ================================================================
// A DUT.
// This is a made-up example, so the exact functionality is not important.

(* synthesize *)
module mkDut (Put#(int));
   Reg#(int) y <- mkReg (0);

   method Action put (int z) if (y > 0);
      y <= z;
   endmethod
endmodule

// ================================================================

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

This is a made-up example to show demonstrate the rule scheduling
attributes 'fire_when_enabled' and 'no_implicit_conditions', and so
the exact functionality is not important.

Notice that rule r1 has the attribute 'no_implicit_conditions'.  This
asserts that the rule cannot be prevented from firing because some
method called in the rule has an implicit condition. However, the rule
calls dut.put(), and we can see that in mkDut, the put() method has an
implicit condition.  Thus the assertion is false.  When we compile, we
indeed get the following scheduling error message:

    Error: "Tb.bsv", line 38, column 9: (G0005)
      The assertion `no_implicit_conditions' failed for rule `r1'

Now let us get beyond this by commenting out that implicit condition:

       method Action put (int z); // if (y > 0);

and let us recompile.  We now get the following error message:

    Error: "Tb.bsv", line 38, column 9: (G0005)
      The assertion `fire_when_enabled' failed for rule `RL_r1'
      because it is blocked by rule
        RL_r2
      in the scheduler
        esposito: [RL_r2 -> [], RL_r1 -> [RL_r2], RL_count_cycles -> []]

The reason is that, even if rule r1's explicit condition ('pack (cycle)[2:0] != 7')
were true, the rule is still prevented from firing because:
- It conflicts with ruled r2 (both r1 and r2 read and write register x)
- The 'descending_urgency' attribute gives priority to r2 over r1
Thus the 'fire_when_enabled' assertion is false, and this is the error message.

If we change the rule priorities as follows:

       (* descending_urgency = "r1, r2" *)

it will now compile successfully, because r1 can indeed execute
whenever its conditions are true.

----------------
In summary:

- 'no_implicit_conditions': A rule may call many methods, but unless
  we examine the implementations of those methods, we could not be
  sure whether those methods can be disabled or not by implicit
  conditions.  Further, at different times during the design, the
  modules behind those method calls may be substituted, i.e., the
  method implementations can change.  The 'no_implicit_conditions'
  attribute is a useful compile-checked assertion that the
  method-calls in a rule are never disabled by implicit conditions

- 'fire_when_enabled': A rule may be inhibited from firing even if all
  its implicit and explicit conditions are true, because it conflicts
  with some other rule which has higher priority.  Further, during
  program development, as we add rules incrementally, this situation
  may change.  The 'fire_when_enabled' attribute is a useful
  compiler-checked assertion that ensures that a rule is not inhibited
  by other rules.  It essentially asserts that the 'CAN FIRE' signal
  of a rule is the same as its 'WILL FIRE', i.e., that if it can fire,
  it won't be inhibited by conflicts with any other rule.

* ================================================================
*/
