// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 4f
// "RWires"
// Same as example 4d (up/down counter)
// showing use of a mkBypassWire constructor instead of mkDWire.
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package Tb;

// ----------------------------------------------------------------
// A top-level module connecting the stimulus generator to the DUT

(* synthesize *)
module mkTb (Empty);

   Counter c1 <- mkCounter_v1;    // Instantiate a version 1 counter
   Counter c2 <- mkCounter_v2;    // Instantiate a version 2 counter (same interface type)

   // ----------------
   // Step 'state' from 1 to 10 and show both counter values in each state

   Reg#(int) state <- mkReg (0);

   rule step_state;
      if (state > 9) $finish (0);
      state <= state + 1;
   endrule

   rule show;
      $display ("In state %0d, counter values are %0d, %0d", state, c1.read (), c2.read ());
   endrule

   // ----------------
   // For counter c1, increment by 5 always, decrement by 2 in states 4..10

   rule incr1;
      c1.increment (5);
   endrule

   rule decr1 (state > 3);
      c1.decrement (2);
   endrule

   // ----------------
   // Same for counter c2 (increment by 5 always, decrement by 2 in states 4..10)

   rule incr2;
      c2.increment (5);
   endrule

   rule decr2 (state > 3);
      c2.decrement (2);
   endrule

endmodule: mkTb

// ----------------------------------------------------------------
// Interface to an up-down counter

interface Counter;
   method int read();                     // Read the counter's value
   method Action increment (int di);      // Step the counter up by di
   method Action decrement (int dd);      // Step the counter down by dd
endinterface: Counter

// ----------------------------------------------------------------
// Version 1 of the counter

(* synthesize *)
module mkCounter_v1 (Counter);
   Reg#(int) value1 <- mkReg(0);  // holding the counter's value

   method int read();
      return value1;
   endmethod

   method Action increment (int di);
      value1 <= value1 + di;
   endmethod

   method Action decrement (int dd);
      value1 <= value1 - dd;
   endmethod

endmodule: mkCounter_v1

// ----------------------------------------------------------------
// Version 2 of the counter

(* synthesize,
   always_enabled = "increment" *)
module mkCounter_v2 (Counter);
   Reg#(int) value2 <- mkReg(0);  // holding the counter's value

   Wire#(int) w_incr <- mkBypassWire();  // Signal that increment method is being invoked
   Wire#(int) w_decr <- mkDWire (0);     // Signal that decrement method is being invoked

   // ---------------- RULES

   (* fire_when_enabled, no_implicit_conditions *)
   rule r_incr_and_decr;
      value2 <= value2 + w_incr - w_decr;
   endrule

   // ---------------- METHODS

   method int read();
      return value2;
   endmethod

   method Action increment (int di);
      w_incr <= di;
   endmethod

   method Action decrement (int dd);
      w_decr <= dd;
   endmethod

endmodule: mkCounter_v2

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

Most of this example is identical to Example 4d, except in module
mkCounter_v2 we have used the 'mkBypassWire' constructor instead of
the 'mkDWire' constructor for the increment signal. We have also
modified the testbench mkTb to invoke the increment method on every
clock, instead of just on clocks 0..6.

The 'mkBypassWire' module constructor also produces a module with a
Wire#() interface.  However, it assumes (and checks) that the _write()
method is always enabled.  Thus the _read() method does not carry any
implicit condition, and can therefore be used in rules with a
'no_implicit_conditions' attribute, or in an 'always_ready' method.

In mkCounter_v2, we use this to create the w_incr wire module:

       Wire#(int) w_incr <- mkBypassWire();  // Signal that increment method is being invoked

We have a single rule:

   (* fire_when_enabled, no_implicit_conditions *)
   rule r_incr_and_decr;
      value2 <= value2 + w_incr - w_decr;
   endrule

Here, this rule can always fire because neither w_incr (BypassWire)
nore w_decr (DWire) have implicit conditions on their _read() methods.

The 'increment' method:

       method Action increment (int di);
          w_incr <= di;
       endmethod

must now be called on every clock, since it is assigning to a
BypassWire, which assumes the assignment is always enabled.  The
attribute:

       always_enabled = "increment"

just above the 'module mkCounter_v2' header asserts this property, and
this is verified by the compiler.

Accordingly, in the testbench,

       rule incr1;
          c1.increment (5);
       endrule

the incr1 rule (and similarly the incr2 rule) now invoke the
increment() methods in all states (not just in states 0..6).

----------------------------------------------------------------
EXERCISE:

(1) Remove the attribute

        always_enabled = "increment"

    from the mkCounter_v2 module declaration, then compile with -verilog.
    Observe and explain the compiler warning message.

* ================================================================
*/
