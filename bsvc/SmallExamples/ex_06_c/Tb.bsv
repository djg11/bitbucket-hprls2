// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 6c
// "Variables, assignments and combinational circuits"
// For-loops/While-loops (static elaboration) -
// showing what goes wrong if we use a dynamic value in loop termination
// ================================================================

package Tb;

import FIFO::*;
import GetPut::*;
import ClientServer::*;

// ----------------------------------------------------------------
// This is a made-up example to demonstrate static elaboration semantics,
// so the exact functionality does not matter.

(* synthesize *)
module mkTb (Empty);

   Reg#(int) klimit <- mkReg (24);

   rule r;
      int a = 10;

      for (int k = 20; k < klimit; k = k + 1)
         a = a + k;

      $display ("rule r, a = %0d", a);
   endrule

endmodule: mkTb

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

This is a made-up example to demonstrate static elaboration semantics,
so the exact functionality does not matter.

----------------
Consider the following statement in rule 'r':

          for (int k = 20; k < klimit; k = k + 1)
             a = a + k;

In our discussion in example 6b, we said that loops like these are
'statically unrolled', essentially making separate copies of the loop
body.  The problem is that here, the loop termination depends on a
dynamic value, the value in register 'klimit'.  And although in this
trivial example there is no other assignment to register 'klimit' and
so we may infer that its value is constant at 24, in general a
register's value (or other dynamic value) is not constant and the
compiler cannot rely on that.

Conceptually, the compiler unrolls the loop one iteration at a time:

          k = 20;
          if (k < klimit) begin
             a = a + k;
             k = k + 1;
             if (k < klimit) begin
                a = a + k;
                k = k + 1;
                if (k < klimit) begin
                   a = a + k;
                   k = k + 1;

                   ... and so on ...

                end
             end
          end

At every stage, it tries to 'reduce' expressions whose values are
known.  For example, instead of 'klimit', if we had the constant 24,
the compiler would be able to reduce the first predicate

    (k < 24)
to
    (20 < 24)
and then to
    True

and then use this to eliminate the outermost 'if-then-else', and so
on.  In particular, by repeated application of such reduction, the
compiler can unroll the loop into the appropriate number of loop
bodies.

However, when the predicate depends on a dynamic value (i.e., a value
that is not known until simulation time, because, for example, it is
read out of a register), the compiler cannot 'reduce' these
expressions statically.  It thus goes on forever, trying to unwind the
loop until termination, which of course it never reaches.

When we compile, we see a warning message like this:

    Warning: "Prelude.bs", line 2469, column 0: (G0024)
      The function unfolding steps interval has been exceeded when unfolding
      `Prelude.primFix'. The current number of steps is 100000. Next warning at
      200000 steps. Elaboration terminates at 1000000 steps.

This is a red flag that indicates that the compiler is trying to
unwind something that may not terminate.  Whenever we see a message
like this, we should check if the compiler is trying to statically
elaborate something that actually depends on a dynamic value.

----------------
When viewing such a boiled-down trivial example, one may naturally ask
the following questions:

Q: Why can't the compiler figure out that the 'klimit' register is
   initialized to 24 and never updated, and so why can't it do the
   static reductions to unroll the loop successfully?

   A: The compiler works on general principles, and in general it is
      not so obvious that a register value is actually constant.
      There is a slippery slope of more and more sophisticated program
      analysis that is necessary to detect dynamic values that are
      actually constant, and in general it is infeasible for the
      compiler to pursue this.

Q: Why can't the compiler realise that it is trying to unroll a loop
   whose condition is dynamic, and report this as an error, instead of
   getting stuck trying to unroll it?

   A: The compiler works on general principles, and in general it is
      not so obvious that the unrolling of a loop will never terminate.
      There is a slippery slope of more and more sophisticated program
      analysis that is necessary to detect this, and in general it is
      infeasible for the compiler to pursue this.  The 'unfolding
      steps' warning is a partial solution to this problem, alerting
      the programmer that something may be wrong with their program.

----------------
SUMMARY: Loops in Bluespec SystemVerilog are statically elaborated by
the compiler (except see NOTE below on FSMs).  As such, the loop
termination conditions must be statically computable by the compiler,
and in particular must not depend on any dynamic values.


NOTE: Bluespec SystemVerilog also has a 'Finite State Machine' (FSM)
sub-language (see Appendix C5 in the Reference Guide, and Section 12
in this Small Examples Suite).  In the FSM sub-language, for-loops and
while-loops are used in a different way, to describe temporal
(time-based) sequencing.  There, because those loops describe dynamic
behavior and not static structure, one can write loops with dynamic
termination conditions etc.

----------------
EXERCISE:

- Replace the 'for' loop with a 'while' loop, like so:

      int k = 20;
      while (k < klimit) begin
         a = a + k;
         k = k + 1;
      end

  and you should see exactly the same behavior.

* ================================================================
*/
