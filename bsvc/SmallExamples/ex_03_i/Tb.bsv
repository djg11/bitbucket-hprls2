// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example ex_03_i
// "Module hierarchy and interfaces"
// Nested interfaces; definition of methods and subinterfaces directly, and by assignment.
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package Tb;

import FIFO::*;

import FIFO::*;
import Connectable::*;

instance Connectable#( FIFO#(int), FIFO#(int) );
   module mkConnection#( FIFO#(int) from, FIFO#(int) to )(Empty);
      rule move_data;
         to.enq( from.first() );
         from.deq();
      endrule
   endmodule
endinstance

/*
function Rules mkConectionFIFO ( FIFO#(int) from, FIFO#(int) to );
   return rules
             rule moveit;
                to.enq( from.first() );
                from.deq();
             endrule
          endrules;
endfunction
*/

(* synthesize *)
module mkTb( Empty );
   
   FIFO#(int) fifo1 <- mkFIFO;
   FIFO#(int) fifo2 <- mkFIFO;

   // this is here just to count data going into the pipeline
   Reg#(int) i <- mkReg(0);

   // data geos in 
   rule pushIn (i <= 10);
      fifo1.enq( i );
      i <= i + 1;
   endrule

   // mkConnection connects the coded interface methods of
   // fifo1 to fifo2
   mkConnection( fifo1, fifo2 );

   // get data out of fifo2
   rule popOut;
      let got = fifo2.first();
      fifo2.deq();
      $display("Got %x", got);
      if (got == 10) begin
         $display("Passed");
         $finish;
      end
   endrule

endmodule

endpackage



