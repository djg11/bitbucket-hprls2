// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 3d
// "Module hierarchy and interfaces"
// ActionValue methods.
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.
// The only difference from Example 3c is the DELIBERATE ERROR below

package Tb;

(* synthesize *)
module mkTb (Empty);
   Dut_ifc dut <- mkDut;

   rule r1;
      int w = dut.avmeth(10);        // DELIBERATE ERROR
      $display ("w = %0d", w);
      if (w > 50) $finish (0);
   endrule

endmodule

// ----------------------------------------------------------------

interface Dut_ifc;
   method ActionValue#(int) avmeth (int v);
endinterface

(* synthesize *)
module mkDut (Dut_ifc);

   Reg#(int) x <- mkReg (0);

   method ActionValue#(int) avmeth (int v);
      x <= x + v;
      return x;
   endmethod

endmodule

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

          int w = dut.avmeth(10);

The right-hand side expression, dut.avmeth(10) has type
ActionValue#(int).  This statement attempts to bind this to a variable
of type int, provoking this type error:

    Error: "Tb.bsv", line 20, column 15: (T0080)
      Type error at the use of the following function:
        dut.avmeth

      The expected return type of the function:
        Int#(32)

      The return type according to the use:
        ActionValue#(Int#(32))

* ================================================================
*/
