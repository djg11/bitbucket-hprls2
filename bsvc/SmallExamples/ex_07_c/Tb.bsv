// Copyright 2008 Bluespec, Inc.  All rights reserved.

package Tb;

// section 7 c - case expression

(* synthesize *)
module mkTb (Empty);
   
   // The bluespec also allows for great freedom of expression ;)
   // case statement can also be used as expressions, if you so desire..
   //
   
   Reg#(int) step <- mkReg(0);

   Reg#(int) a <- mkReg(0);

   rule init ( step == 0 );
      $display("=== step 0 ===");

      // the "case () .... endcase" is now an expression
      // this means the we need a semicolon at the end
      // which is consistent with the definition
      // of an expression in the case ... i.e.
      //  int val = expr ;

      int val = case( a )
                    0: return 25;
                    1: return 23;
                    2: return 17;
                    default: return 64;
                endcase;
      
      $display("val = %b", val);

      step <= step + 1;
   endrule

   // another useful case is to be able to select
   // instances based on a parameter or constant
   // (we presume in a more interesting way than this,
   // but you get the idea).
   Bool initReg = True;

   Reg#(int) rr <- case ( initReg )
                      False: mkRegU;
                      True:  mkReg(0);
                   endcase;

   rule step1 ( step == 1 );
      $display("=== step 1 ===");
      $display("reg rr is initialized to ", rr );
      step <= step + 1;
   endrule

   rule step2 ( step == 2 );
      $display("=== step 2 ===");
      $display ("All done");
      $finish (0);
   endrule

endmodule: mkTb

endpackage: Tb

