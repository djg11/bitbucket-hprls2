// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 9b
// Simple Processor model, illustrating the use of enums, tagged unions,
// structs, arrays of registers, and pattern-matching.
// Variant of 9a demonstrating custom bit-encoding of processor instructions.
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package SimpleProcessor;

// ----------------------------------------------------------------
// A small instruction set

// ---- Names of the four registers in the register file
typedef enum {R0, R1, R2, R3} RegNum
        deriving (Bits);

Integer regFileSize = exp (2, valueof (SizeOf#(RegNum)));

// ---- Instruction addresses for a 32-location instruction memory
typedef 5 InstructionAddressWidth;
typedef UInt#(InstructionAddressWidth) InstructionAddress;
Integer imemSize = exp (2, valueof(InstructionAddressWidth));

// ---- Values stored in registers
typedef Bit#(32)  Value;

// ---- Instructions
typedef union tagged {
   struct { RegNum rd; Value v; }                  MovI;    // Move Immediate
   InstructionAddress                              Br;      // Branch Unconditionally
   struct { RegNum rs; InstructionAddress dest; }  Brz;     // Branch if zero
   struct { RegNum rd; RegNum rs1; RegNum rs2; }   Gt;      // rd <= (rs1 > rs2)
   struct { RegNum rd; RegNum rs1; RegNum rs2; }   Minus;   // rd <= (rs1 - rs2)
   RegNum                                          Output;
   void                                            Halt;
} Instruction;

// ----------------
// Custom encoding of the 'Instruction' type into 32 bits

instance Bits#(Instruction, 32);
   function Bit#(32)     pack    (Instruction instr);
      Bit#(32) b32 = '1;
      case (instr) matches
         tagged MovI  { rd: .rd, v: .v }             : b32 =                           { pack(rd), v[28:0], 1'b0};
         tagged Br    .d                             : b32 =                        extend({ pack(d), 3'h1, 1'b1});
         tagged Brz   { rs: .rs, dest: .d }          : b32 =              extend({ pack(rs), pack(d), 3'h2, 1'b1});
         tagged Gt    { rd:.rd, rs1:.rs1, rs2:.rs2 } : b32 = extend({ pack(rd), pack(rs1), pack(rs2), 3'h3, 1'b1});
         tagged Minus { rd:.rd, rs1:.rs1, rs2:.rs2 } : b32 = extend({ pack(rd), pack(rs1), pack(rs2), 3'h4, 1'b1});
         tagged Output .rs                           : b32 =                       extend({ pack(rs), 3'h5, 1'b1});
         tagged Halt                                 : b32 =                                 extend({ 3'h6, 1'b1});
      endcase
      return b32;
   endfunction

   function Instruction  unpack  (Bit#(32) b32);
      Instruction instr = ?;
      if (b32[0] == 1'b0)
         instr = tagged MovI { rd: unpack(b32[31:30]), v: extend (b32[29:1]) };
      else
         case (b32[3:1])
            3'h1 : instr = tagged Br (unpack(b32[8:4]));
            3'h2 : instr = tagged Brz { rs:unpack(b32[10:9]), dest:unpack(b32[8:4])};
            3'h3 : instr = tagged Gt { rd:unpack(b32[9:8]), rs1:unpack(b32[7:6]), rs2:unpack(b32[5:4]) };
            3'h4 : instr = tagged Minus { rd:unpack(b32[9:8]), rs1:unpack(b32[7:6]), rs2:unpack(b32[5:4]) };
            3'h5 : instr = tagged Output (unpack(b32[5:4]));
            3'h6 : instr = tagged Halt;
         endcase
      return instr;
   endfunction
endinstance

// ----------------------------------------------------------------
// The processor model

interface SimpleProcessor;
   method Action loadInstruction (InstructionAddress ia, Instruction instr);
   method Action start ();    // Begin instruction execution at pc 0
   method Bool   halted ();
endinterface

(* synthesize *)
module mkSimpleProcessor (SimpleProcessor);

   // ---- Instruction memory (modeled here using an array of registers)
   Reg#(Instruction) imem[imemSize];
   for (Integer j = 0; j < imemSize; j = j + 1)
      imem [j] <- mkRegU;

   Reg#(InstructionAddress) pc <- mkReg (0);    // The program counter

   // ---- The register file (modeled here using an array of registers)
   Reg#(Value) regs[regFileSize];         // The register file
   for (Integer j = 0; j < regFileSize; j = j + 1)
      regs [j] <- mkRegU;

   // ---- Status
   Reg#(Bool) running <- mkReg (False);
   Reg#(UInt#(32)) cycle <- mkReg (0);

   // ----------------
   // RULES

   rule fetchAndExecute (running);
      let instr = imem [pc];
      case (instr) matches
         tagged MovI  { rd: .rd, v: .v }             : begin
                                                          regs[pack(rd)] <= v;
                                                          pc <= pc + 1;
                                                       end
         tagged Br    .d                             : pc <= d;
         tagged Brz   { rs: .rs, dest: .d }          : if (regs[pack(rs)] == 0)
                                                          pc <= d;
                                                       else
                                                          pc <= pc + 1;
         tagged Gt    { rd:.rd, rs1:.rs1, rs2:.rs2 } : begin
                                                          Bool b = (regs[pack(rs1)] > regs[pack(rs2)]);
                                                          Value bv = extend (pack (b));
                                                          regs[pack(rd)] <= bv;
                                                          pc <= pc + 1;
                                                       end
         tagged Minus { rd:.rd, rs1:.rs1, rs2:.rs2 } : begin
                                                          regs[pack(rd)] <= regs[pack(rs1)] - regs[pack(rs2)];
                                                          pc <= pc + 1;
                                                       end
         tagged Output .rs                           : begin
                                                          $display ("%0d: output regs[%d] = %0d",
                                                                    cycle, rs, regs[pack(rs)]);
                                                          pc <= pc + 1;
                                                       end
         tagged Halt                                 : begin
                                                          $display ("%0d: Halt at pc", cycle, pc);
                                                          running <= False;
                                                       end
         default: begin
                     $display ("%0d: Illegal instruction at pc %0d: %0h", cycle, pc, instr);
                     running <= False;
                  end
      endcase
      cycle <= cycle + 1;
   endrule

   // ----------------
   // METHODS

   method Action loadInstruction (InstructionAddress ia, Instruction instr) if (! running);
      imem [ia] <= instr;
   endmethod

   method Action start ();
      cycle <= 0;
      pc <= 0;
      running <= True;
   endmethod

   method Bool halted ();
      return (! running);
   endmethod

endmodule: mkSimpleProcessor

endpackage: SimpleProcessor

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

In the previous version of SimpleProcessor.bsv in example 9a, the
'Instruction' type was represented in 37 bits.  In this version, it is
represented in 32 bits.

This is accomplished with a very local change to the code.  The sole
difference between this file and the is in definition of the type
Instruction (beginning line 32):

- We have removed the 'deriving (Bits)' phrase that was attached to
  the type definition of 'Instruction'

- We have added the section 

    // ----------------
    // Custom encoding of the 'Instruction' type into 32 bits
    instance Bits#(Instruction, 32);
       ...
    endinstance

Please do a 'diff' to verify that these are the only changes!

----------------
Why did the encoding of 'Instruction' take 37 bits in example 9a?  The
answer is that we used 'deriving (Bits)' to ask the compiler to pick a
default encoding.  Default encodings are 'orthogonal' encodings, i.e.,
the encoding of an aggregate item is derived by separately encoding
its parts and then gluing them together.  In general, default
encodings are not the most optimal. By cleverly exploiting knowledge
about relationships between different parts, one can sometimes design
tighter encodings.  For example, in information theory, Huffman
encodings exploit knowledge about the statistics of the information to
produce tighter encodings.

In the 'Instruction' type, there are seven kinds (MovI, Br, Brz, Gt,
Minus, Output and Halt).  The 'kind' of the instruction thus requires
3 bits of encoding.  In the MovI kind of instruction, the 'rd' member
is of type RegNum, requiring 2 bits, and the 'v' member is of type
Value, requiring 32 bits.  Thus, the MovI kind of instruction requires
3+2+32=37 bits.  We can similarly analyze the other kinds of
instructions, but we will find that they all require fewer bits.
Thus, an 'Instruction', since it can in principle be of any kind, must
use the maximum size of all the kinds, which is 37 bits.  Another way
of looking at is that each slot in instruction memory must be capable
of holding any kind of instruction, and therefore must be sized for
the largest kind, in this case 37 bits.

----------------
Let us now impose a restriction that the MovI instruction should fit
in 32 bits.  We need a minimum of 1 bit to tell us that it is a MovI
instruction.  We need a minimum of 2 bits to specify the destination
register.  That leaves us with (32-1-2=) 29 bits for the immediate
value.  Let us choose to live with this restriction, namely that in a
MovI instruction, the immediate value will never be wider than 29
bits.  This is a common compromise in processor instruction sets,
namely that immediate values are usually shorter than the full value
space (typically 32 or 64 bits).

But how can we use just 1 bit to tell us if an instruction is a MovI
instruction?  There are 7 kinds of instructions, so don't we need 3
bits?  The answer is that we choose a representation where:
  - 1 bit tells us if it's a MovI or not
  - If it's not a MovI, we use 3 more bits to tell us which of the
        remaining 6 kinds it is

Overall, we're using 4 bits for the instruction kind, which is LESS
efficient than our previous 3 bits, but we're using the extra bits
only in those situations where we can afford the extra bits!

----------------
This idea of a custom, clever encoding, is expressed in the section:

    // ----------------
    // Custom encoding of the 'Instruction' type into 32 bits
    instance Bits#(Instruction, 32);
       ...
    endinstance

Normally, 'deriving (Bits)' tells the compiler to use default
mechanisms to define two functions:

       function Bit#(32)     pack    (Instruction instr);
       function Instruction  unpack  (Bit#(32) b32);

These functions encapsulate the bit encoding (pack()) and decoding
(unpack()) of the 'Instruction' type.

Now, we use the 'instance' declaration to define these functions
explicitly instead, using an encoding of our choice.

The details of the functions are fairly straighforward.  For example,
in the 'pack()' function, the first arm of the case statement
describes what happens when we encounter a MovI instruction--we encode
it using:

             tagged MovI { rd: .rd, v: .v }: b32 = { pack(rd), v[28:0], 1'b0};

i.e., we produce a 32-bit value containing the packed version of 'rd'
(2 bits), 29 bits of 'v', and the 1-bit value '0'.  This lower-order
bit (== 0) identifies this as a MovI instruction.

Consider the 'Brz' instruction:

             tagged Brz { rs: .rs, dest: .d } : b32 = extend({ pack(rs), pack(d), 3'h2, 1'b1});

Moving from lower-order to higher-order bits, the lower-order bit is
'1b1, identifying this as 'not a MovI instruction'.  Then we have a
3-bit code 3'h2 identifying this as a Brz instruction.  Then we have
the bits representing the destination 'd' and then the bits
representing the source register name 'rs'.  Finally, the whole thing
is 'extend'ed to 32 bits.

Similarly, the 'unpack()' function is given an encoded instruction (32
bits) and must return an Instruction.  The if-then-else first examines
the lower-order bit 'b32[0]' to determine if it's a MovI or not.  If
not, then the 'case' statement examines the next 3 bits 'b32[3:1]' to
figure out what kind of instruction it is.  The rest of the function
body is straightforward, just picking out the appropriate bits for
each of the members and packaging it up into an appropriate version of
the tagged union 'Instruction'.

----------------
Granted, the functions 'pack()' and 'unpack()' in the 'instance
Bits#(Instruction,32)' must be written very carefully.  A small
mistake in bit indexes etc. can introduce a subtle bug that will take
some hunting down.

But the beauty of this system is that the complexities of instruction
coding are completely localized herein!  We have made a pretty
dramatic change in instruction encoding, and not a bit of the rest of
the source code had to be touched--it retains all its readability and
maintainability!  Compare this with other systems where the details of
instruction encoding would likely be smeared across the rest of the
design, across pages and pages of code, a nightmare to maintain!

----------------
SUMMARY

- We can often use 'deriving (Bits)' initially to get our functional
  models working, exploiting the readability and type-checking
  robustness that comes with use of enums, tagged unions, structs and
  arrays, and later, as a separate, contained activity, fix up the bit
  encodings of our types for efficient representations.

* ================================================================
*/
