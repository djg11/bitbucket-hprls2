// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 3e
// "Module hierarchy and interfaces"
// ActionValue method: deliberate error (in type-checking) on attempt
// to use an ActionValue method in a rule condition.
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package Tb;

(* synthesize *)
module mkTb (Empty);
   Dut_ifc dut <- mkDut;

   rule r1 (dut.avmeth (10) > 0);     // DELIBERATE ERROR
      int w = dut.avmeth(10);
      $display ("w = %0d", w);
      if (w > 50) $finish (0);
   endrule

endmodule

// ----------------------------------------------------------------

interface Dut_ifc;
   method ActionValue#(int) avmeth (int v);
endinterface

(* synthesize *)
module mkDut (Dut_ifc);

   Reg#(int) x <- mkReg (0);

   method ActionValue#(int) avmeth (int v);
      x <= x + v;
      return x;
   endmethod

endmodule

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)


   rule r1 (dut.avmeth (10) > 0);     // DELIBERATE ERROR

The designer may have intended that the method 'dut.avmeth(10)' is to
be called and the returned value is to be compared with zero.

An attempt to compile this will provoke the following type-checking error:

    Error: "Tb.bsv", line 19, column 29: (T0031)
      The provisos for this expression could not be resolved because there are no
      instances of the form:
        Ord#(ActionValue#(Int#(32)))

which may seem a little cryptic at first, but is actually quite
logical.  Specifically, the left-hand side expression has type
ActionValue#(int), which is synonymous with ActionValue#(Int#(32)),
and here we are trying to use the operator '<' on this type.  In
general, '<' is defined only on types in the 'Ord' typeclass.  Here,
it is complaining that ActionValue(int) is not in the 'Ord' typeclass,
and hence can't apply the '<' operator to such an expression.

[ Other examples in this suite illustrate typeclasses and provisos. ]

Once again, this type error emphasizes the difference between two
types of expressions:
    An ActionValue that, when invoked, will return a value of type int, and
    an int.

In general, any Action or ActionValue expression represents a 'side
effect', i.e., a potential change of state.  Expressions that do not
have side effects are sometimes called 'pure'.  In the BSV model of
computation, boolean conditions (in particular rule conditions and
method implicit conditions) must always be pure, of type Bool; they
can never contain an Action or ActionValue method.  This is enforced
by the type-checking system, as seen in this example.  By keeping rule
and method conditions pure, the semantics cleanly separates the
activities of scheduling of rules from execution of rules.

* ================================================================
*/
