// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 5d
// "Scheduling"
// Attributes on rules: mutually_exclusive.
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package Tb;

(* synthesize *)
module mkTb(Empty);

   // a 3-bit reg with one bit always set
   Reg#(Bit#(3)) x <- mkReg (1);

   Reg#(int)     cycle <- mkReg (0);

   // A register used to induce a conflict
   Reg#(int) y <- mkReg (0);
  
   rule rx;
      cycle <= cycle + 1;
      x <= { x[1:0], x[2] };  // rotate the bits
      $display ("%0d: Rule rx", cycle);
   endrule

//   (* mutually_exclusive = "ry0, ry1, ry2" *)    // XXX

   rule ry0 (x[0] == 1);
      y <= y + 1;
      $display ("%0d: Rule ry0", cycle);
   endrule

   rule ry1 (x[1] == 1);
      y <= y + 2;
      $display ("%0d: Rule ry1", cycle);
   endrule

   rule ry2 (x[2] == 1);
      y <= y + 3;
      $display ("%0d: Rule ry2", cycle);
   endrule

   rule done (cycle >= 10);
      $finish (0);
   endrule

endmodule: mkTb

endpackage: Tb

/* ================================================================
 * ================================================================
 * ================================================================

Commentary (rest of this file)

The register x initially has value 1 (bit 0 is set).  In rule rx, we
rotate the bits.  Hence, it always has exactly one bit set, bit 0, 1
or 2.  Thus, the rules ry0, ry1 and ry2 have mutually exclusive
conditions.  But this proof is too difficult for the compiler.

Because the compiler does not know that the rules ry0, ry1 and ry2 are
mutually exclusive, and each rule reads and writes register y, the
compiler assumes a conflict between each pair of rules.  Thus, it
reports this during compilation and generates generates priority logic
that suppresses the other rules when one of them is enabled.

With the 'mutually_exclusive' attribute, however, the compiler assumes
mutual exclusion of rule conditions, and does not generate priority
logic.

Compile this program twice,
- once with the line XXX above commented out
    [ generates conflict warning message and priority logic ]
- once with the line XXX above uncommented
    [ generates no conflict warning message and no priority logic ]

In each case, examine the Verilog code to see the difference the logic.

* ================================================================
*/
