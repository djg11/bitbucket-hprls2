// Copyright 2008 Bluespec, Inc.  All rights reserved.

package Tb;

// section 7 a - Don't cares, x, z

(* synthesize *)
module mkTb (Empty);

   // There are several values a bit can "be" other than 0/1 in many systems.
   // In general we consider that wires really only have 1 of 2 values
   // This is digital logic, so we are general ignoring analog issues and values
   //
   //    0 -> gnd, low, etc
   //    1 -> vcc, high, etc
   //
   // in really really dangerous situations, it is possible to drive a wire
   // to neither value 0, 1, but to the value inbetween..  not a stable hardware -
   // this can actually even burn physical logic in your chip...
   //
   //    X -> bad state - means wire driven both to 0 and to 1
   //
   // This is different that a signal that is not driven at all, i.e. a "tri-state"
   // signal
   //
   //    Z -> not driven at all, floating, tristate
   //
   // now for evaluation and optimation reasons, we tend also to want to think of a
   // value as being "don't care", meaning I don't care if it's a 0 or a 1, it doesn't
   // affect the logic I care about now, so put whatever value makes the most sense there.
   // Clearly, one could always just drive to 0, but the compiler may be able to carefully
   // pick a value that produces the same logic when we do care, but with better timing
   // and smaller gates...   Many folks consider this an x value, but bsv separates
   // this carefully from "don't cares" because X's often cause simulation differences
   // and problems in many systems.. Experienced hardware people will use this to thier
   // advantage in testing reset/power up for instance..
   // But in bluespec, we have "?" to mean don't care...  We don't allow the user to directly
   // specify X as in verilog X...
   // So logic bit values can be 0,1,?
   
   // here's a quick example of how using ? can help you get better logic...
   // Let's look at this logic table.
   //
   //   a  b  o   
   //  ---------  
   //   0  0  1   
   //   0  1  ?
   //   1  0  ?
   //   1  1  0
   //
   // if we presume ? == 0, as we might simply do (or have done as a matter
   // of simplified coding), then we get the following logic
   //
   //    o = ~(a & b)   one NAND gate
   //
   // But, if we are more careful, this could optimize to
   //
   //    o = ~a         one INV gate
   //
   // for a single gate we probably don't care, but do this many times
   // and the gate and timing savings can be larger
   
   Reg#(int) step <- mkReg(0);

   rule init ( step == 0 );
      $display("=== step 0 ===");

      // for instance.. let's do the exact example above...
      bit o = 0;
      for (Bit#(2) i=0; i<2; i=i+1)
         for (Bit#(2) j=0; j<2; j=j+1) begin
            bit a = i[0];
            bit b = j[0];

            if ((a==0) && (b==0))
               o = 1;
            else
               o = 0;

            $display("%b%b = %b", a,b,o);
         end
      

      // this generates the NAND gate we expect and may be more natural
      step <= step + 1;
   endrule

   rule step1 ( step == 1 );
      $display("=== step 1 ===");

      bit o = 0;
      for (Bit#(2) i=0; i<2; i=i+1)
         for (Bit#(2) j=0; j<2; j=j+1) begin
            bit a = i[0];
            bit b = j[0];


            // but let's enumerate out the case we really care about 
            if ( (a==0) && (b==0) )
               o = 1;
            else if ( (a==1) && (b==1) )
               o = 0;
            else
               o = ?;

            $display("%b%b = %b", a,b,o);

            // this generates the correct value for 00,11 but don't care for 01,10
            //
            // And specifically, bsc may optimize this itself, or it may pass
            // the optimization onto verilog...  There are two compiler switches which
            // affect how this is handled
            //
            //   -opt-undetermined-vals - tells the compiler to try to optimize
            //                            don't cares, etc
            //
            //   -unspecified-to n      - tells compiler to write unspecified
            //                            values to n (where n is 0,1,x)
            //
            //   NOTE: bluesim only allows 0/1 (since it is a two state simulator)
            //
            // NOTE2: in verilog sim, the ? cases come out as X
            // which we expect/hope the synthesizer will see
            // and optimize to the proper value...
            // But simulation wise, it is now X, so things
            // things will break if the value is used as a X
         end

      step <= step + 1;
   endrule

   rule step2 ( step == 2 );
      $display("=== step 2 ===");

      // more natural, perhaps, to many old school hardware guys
      // will be something more like expresso format - a case statement
      bit o = 0;
      for (Bit#(2) i=0; i<2; i=i+1)
         for (Bit#(2) j=0; j<2; j=j+1) begin
            bit a = i[0];
            bit b = j[0];

            case ( { a, b } )
               2'b11: o = 1;
               2'b01: o = ?;
               2'b10: o = ?;
               2'b00: o = 0;
            endcase

            $display("%b%b = %b", a,b,o);
         end
      
      step <= step + 1;
   endrule

   rule step3 ( step == 3 );
      $display("=== step 3 ===");

      // even better, now is we can use "don't cares" in the pattern matching
      // of the inputs, but with a slightly different case syntax
      bit o = 0;
      for (Bit#(2) i=0; i<2; i=i+1)
         for (Bit#(2) j=0; j<2; j=j+1) begin
            bit a = i[0];
            bit b = j[0];

            case ( { a, b } ) matches
               2'b?1: o = ?;
               2'b10: o = 1;
               2'b00: o = 0;

            endcase

            $display("%b%b = %b", a,b,o);
         end
      
      step <= step + 1;
   endrule

   rule step4 ( step == 4 );
      $display("=== step 4 ===");
      
      $display ("All done");
      $finish (0);
   endrule

endmodule: mkTb

endpackage: Tb

