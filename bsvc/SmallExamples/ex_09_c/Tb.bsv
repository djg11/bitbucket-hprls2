// Copyright 2008 Bluespec, Inc.  All rights reserved.

// section 9 c - Integer type: unbounded, non-synthesizable

package Tb;

(* synthesize *)
module mkTb (Empty);

   // Integer is defined in the bluespec prelude library
   // It is an unbounded number (it is only bound by
   // the amount of memory and patience you have)..

   // but this is a static value that can't be stored in
   // a register (not directly anyway), or used in hardware

   // we can create a variable and do static compile time things
   // with it (and these are great for loops and indexing arrays, etc).

   // for instance, create an array of 4 elements
   // we can use the integer to index into that array statically

   Integer inx = 0;

   Bool arr1[4];
   arr1[inx  ] = True;

   inx = inx + 1;
   arr1[inx] = False;

   inx = inx + 1;
   arr1[inx] = True;

   inx = inx + 1;
   arr1[inx] = True;

   // even better we can even create a loop with this integer (see loops);
   // In order to use this as a sized variable, we need to use "fromInteger"
   // this converts the Integer to whatever type
   int arr2[16];
   for (Integer i=0; i<16; i=i+1)
      arr2[i] = fromInteger(i);

   // we can do the usual math things with Integers
   Integer foo = 10;
   foo = foo + 1;
   foo = foo * 5;
   Bit#(16) var1 = fromInteger( foo );

   //////////////////////////////////////////////////////////////////////
   Reg#(int) step <- mkReg(0);

   rule init ( step == 0 );
      $display("=== step 0 ===");
      // numbers really are unbounded...
      // the compiler will try to completely elaborate the result
      // meaning if it takes a million places, it will try...
      // to see this, either uncomment the display below
      // or for fun, add a few more 0s to the 100000 below
      // and the compile time will slow down as it tries to create
      // this huge number (which it will eventually if you have
      // enough memory, cpu power and time to spare :)
      Integer x = 10 ** 10000;
      Bool isLarge = (x > 0);
      $display("isLarge = ", isLarge);

      // uncomment this and see the big number create, causing a error :)
      //$display("Large number = ", x);

      step <= step + 1;
   endrule

   rule step1 ( step == 1 );
      $display("=== step 1 ===");
      // Static loops are expanded at compile time..
      // no types at all involved here
      for (Integer i=1; i<16; i=i*2)
         $display("i is ", i);

      step <= step + 1;
   endrule

   rule step2 ( step == 2 );
      $display ("All done");
      $finish (0);
   endrule

endmodule: mkTb

endpackage: Tb

