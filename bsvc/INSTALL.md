0. Install mono

1. Install FSHARP and set set env variable to FSHARP distribution.

I am using this release:
  wget http://koo.corpus.cam.ac.uk/privote1/FSharp-2.0.0.0.zip

 /usr/local/FSharp-2.0.0.0:

total used in directory 4384 available 22613672

drwxrwxr-x.  9 djg11 djg11    4096 Jan 26  2014 .

drwxr-xr-x. 20 root  root     4096 Oct 14 18:00 ..

drwxrwxr-x.  3 djg11 djg11    4096 Jan 26  2014 bin

drwxrwxr-x.  4 djg11 djg11    4096 Jan 26  2014 CompactFramework

drwxrwxr-x.  2 djg11 djg11    4096 Jan 26  2014 doc

drwxr-xr-x.  4 djg11 djg11    4096 Jan 26  2014 FSharpPowerPack-2.0.0.0

-rw-rw-r--.  1 djg11 djg11 4418620 Jan 26  2014 FSharpPowerPack.zip

-rwxrwxr-x.  1 djg11 djg11    1678 Jan 26  2014 install-mono.sh

drwxrwxr-x.  4 djg11 djg11    4096 Jan 26  2014 lib

-rw-rw-r--.  1 djg11 djg11    8878 Jan 26  2014 LICENSE-fsharp.txt

-rw-r--r--.  1 djg11 djg11     596 Jan 26  2014 mono.snk

-rw-rw-r--.  1 djg11 djg11    8428 Jan 26  2014 README-fsharp.html

drwxrwxr-x.  3 djg11 djg11    4096 Jan 26  2014 Silverlight

drwxrwxr-x.  7 djg11 djg11    4096 Jan 26  2014 source

2. Get from bit bucket in a fresh folder.  Set your HPRLS env variable to this folder.
 $ git clone  https://bitbucket.org/djg11/bitbucket-hprls2.git

3. Compile the HPR library that is used for a variety of EDA-related projects.  Two of these (Kiwi and BSVC) are in
the hprls2 repo.

4. Set your HPRLS env var to the folder containing hpr and bsvc.

 $ cd hpr ; make

5. Then compile toy BSV to create a private distribution. 
 $ cd bsvc ; make  priv_distro

You may need to do
export MONO_PATH=$HPRLS/bsvc/priv_distro/lib:$MONO_PATH

6. For a simple, get started test:

 $ cd smalltests
 $ $HPRLS/bsvc/priv_distro/bin/bsvc -incdir=$HPRLS/bsvc/priv_distro/libs/camlib Counter.bsv -vnl=DUT.v -conerefine=disable
 $ iverilog DUT.v vsys.v 
 $ ./a.out

You will often need to disable conerefine because, with no output, the whole design may well get deleted.


END





