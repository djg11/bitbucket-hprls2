// CBG SQUANDERER : Rudimentary/Toy Bluespec Verilog Compiler: Coded in F#.
// (C) 2012-15 DJ Greaves, University of Cambridge. All rights reserved.

(* All rights reserved, except as licensed by LGPL below.
 * DJ Greaves, University of Cambridge, Computer Laboratory.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *)



// $Id: toplevel_bsvc.fs,v 1.1 2013-08-28 11:39:34 djg11 Exp $

module toplevel_bsvc

open System.Collections.Generic
open System.Numerics

open moscow
open meox
open yout
open opath_hdr
open hprls_hdr
open abstract_hdr
open abstracte
open linepoint_hdr
(*
open bsv_grammar
open tydefs_bsvc
open dotreport
open lexbsv
open tableprinter
*)

open lex_bsvc
open tydefs_bsvc
open bsvc
open parser_types


let g_top_ee = ref None

let mine_ionets_3 arg cc =
    let qm = "mine_ionets_3"
    match arg with
        | Tfa(_, sigma) ->
            let miner ii cc = 
                let (rdy, en, rv, args) = (find_rdy ii, find_en_o ii, find_rvo ii, find_args qm ii)
                let v = function
                    | None -> []
                    | Some x -> [x]
                let qx n = if int_constantp n then [] else [n]
                (qx rdy) @ (if en=None then [] else qx (valOf en)) @ (v rv) @ (map f2o3 args) @ cc
            List.foldBack miner sigma cc


let mine_ionets_2b arg cc =
    let qm = "mine_ionets_2b"
    match arg with
        | sigma ->
            let miner ii cc = 
                let (rdy, en, rv, args) = (find_rdy ii, find_en_o ii, find_rvo ii, find_args qm ii)
                let v = function
                    | None -> []
                    | Some x -> [x]
                let qx n = if int_constantp n then [] else [n]
                (qx rdy) @ (if en=None then [] else qx (valOf en)) @ (v rv) @ (map f2o3 args) @ cc
            miner sigma cc


// Tie up some top-level currency wiring.
let capoff1 ww bobj vd (ee:ee_t) sh_emits exported_ios key1 arg =

    let hexpt_is_input = function
        | (X_bnet ff) as arg -> (lookup_net2 ff.n).xnet_io = INPUT && memberp arg exported_ios // Input to an imported interface is not an input to ourselves, so refine I/O test.
        | other ->
            //(sprintf "hexpt_is_input other form %A" (xToStr other))
            false

    let handshake_done_by_restructure = bobj.handshake_done_by_restructure

    let connect_method_arg method_id monica_o (fn_, argbus, xa) =
        let rec gen_arg_mux = function
            | [] -> X_undef
            | [(monica_, grd, vale)] -> vale
            | (monica_, grd, vale)::tt -> ix_query grd vale (gen_arg_mux tt)
        let enco = gen_arg_mux xa.encol
        if vd>=5 then vprintln 5 (sprintf "Consider connect_method_arg: fn=%A  n=%s  rhs=%s " fn_ (netToStr argbus) (xToStr enco))
        
        if hexpt_is_input argbus then
            //dev_println (sprintf "Not capping off input %s with %s" (netToStr argbus) (xToStr xa.enco))
            () // Do not capoff the inputs to this compilation (NB hexpt_is_input may hold owing to being an input to a lower, separately synthesised component but this is not an inputs in the current terms).

//        elif // Also, do not capoff an output from a lower component  ... do we need that ...
        else
            //reportx vd "I/Os are" xToStr ios
            if vd>=5 then
                vprintln 5 (sprintf "connect_method_arg: %s %s   fn=%s %s net=%s" (hptos(kToStr key1)) method_id fn_ (xkey argbus) (netToStr argbus) + " := " + xToStr enco)
            if handshake_done_by_restructure then // experimental new approach (May 2019)
                let monica_dirinfo = { ee.attribute_settings.dirinfo with monica_o=monica_o } // TODO again we need to observe more setting overrides.
                mutadd sh_emits.synch_logic ((monica_dirinfo, [gen_dflop(X_true, xi_X -1 argbus, argbus)])) // Make combinational logic tie to a thread/domain with this artifice.
            else mutadd sh_emits.comb_logic (gen_buf(argbus, enco))


            // misnamed - this is not so much a capoff perhaps as a wiring render

    let check_one_thread_only method_id snargs firelst =
        let get_thread cc (fn_, net, xa) = lst_union cc (map f1o3 xa.encol)

        let threads = list_once(List.fold get_thread (map fst firelst) snargs)
        if length threads > 1 then
            muddy (sprintf "more than one thread for %s in res2-handshake mode. threads=%s" method_id (sfold hptos (map snd threads)))
        elif length threads < 1 then
            vprintln 3 (sprintf "less than one thread for %s in res2-handshake mode. threads=%s" method_id (sfold hptos (map snd threads)))
            None
        else Some(hd threads)
            
    let capoff_currency_drive = function  // How does this compare with the method capoff in the scheduler?  This does args and that one does handshake.
        | (Sigma((_, _, method_id), _, (BsvAction(rdy, firelst, en), m_ats, retval), snargs)) ->

            let monica = if handshake_done_by_restructure then check_one_thread_only method_id snargs firelst else None
            let rex_fire g0 (monica, grd) = ix_or grd g0
            let fire = List.fold rex_fire X_false firelst
            if hexpt_is_input en then
                vprintln 3 (sprintf "No capoff applied for method %s with guard %s  en=%s" method_id (xbToStr fire) (netToStr en))
                () // Must not capoff externally invokable methods.
            else
                //dev_println (sprintf "Capoff method en=%A" (en))
                //vprintln 3 (sprintf "Capoff method %s with guard %s  en=%s" method_id (xbToStr fire) (netToStr en))
                if not_nonep monica then // experimental new
                     let monica_dirinfo = { ee.attribute_settings.dirinfo with monica_o=monica } // TODO again we need to observe more setting overrides.
                     mutadd sh_emits.synch_logic ((monica_dirinfo, [gen_dflop(X_true, xi_X -1 en, xi_blift fire)])) // Make combinational logic tie to a thread/domain with this artifice.
                else mutadd sh_emits.comb_logic (gen_buf(en, xi_blift fire))
            app (connect_method_arg method_id monica) snargs 


        | (Sigma((_, _, method_id), _, (_, retval, mats_), snargs)) ->
            let monica = if handshake_done_by_restructure then check_one_thread_only method_id snargs [] else None
            app (connect_method_arg method_id monica) snargs // Non-action?
        | (Sigma_e _) -> ()

    capoff_currency_drive arg





//
//
//
let rec emit_miner msg (n, r, c, s, flicts) ee =
    let rec emin stack (n, r, c, s, flicts, dones) e =
        //dev_println (msg + sprintf ": emit mine %s    %s"  e.uid (sfold id stack))
        if memberp e.uid stack || memberp e.uid dones then
            hpr_yikes(msg + sprintf ": Ignored looped emissions. Please do not make them.  %s is in " e.uid + sfold id stack)
            (n, r, c, s, flicts, dones)                  
        else
            //dev_println (msg + sprintf ": nets in %s are %s %i %i" e.uid (sfold xToStr !e.nets) (length !e.nets) (length (list_once !e.nets)))
            let nn = // Preserve net hierarchy to some extent - makes report file look nicer.
                if nullp !e.nets then []
                else
                    let wrap net = DB_leaf(Some net, None) // In a definition, we populate the first of the pair, which is the formal.
                    let db1 = { g_null_db_metainfo with pi_name=e.uid; not_to_be_trimmed=true; form=DB_form_external } 
                    gec_DB_group(db1, map wrap !e.nets)
            List.fold (emin (e.uid::stack)) (nn @ n, !e.rulectl @ r, !e.comb_logic @ c, !e.synch_logic @ s, !e.conflict_log @ flicts, e.uid::dones) !e.children 
    let (n, r, c, s, flicts, _) = emin [] (n, r, c, s, flicts, []) ee
    (n, r, c, s, flicts)



//  Rules and methods are in batches where a batch has an integer index or batch no.
//  Execution order is important for bluewires and other constructs that are not pure RTL.
//  Here we collect execution order annotations from component definitions and user pragmas and then make a topological sort that obeys all ordering pairs.
//  We conservatively preserve the input (lexicographical file/lineno) ordering for batches that have no specific ordering constraints.
let bsvc_execution_order_sort ww bobj execution_order_markup statements99 =
    let vd = bobj.sched_loglevel
    let msg = "execution_order_markup sort"
    let ww = WF 2 msg ww "starting"
    let rule_batch_map =
        let mapgen2 (sm:Map<string list, int>) (idx, stmt_lst, exordering, exaux) =
            let dig_idl cc = function
                | Bsv_rule(rulerec, explicit_condition, rulebody, keyname, lp) ->
                    [rulerec.name] :: cc
                | Bsv_methodDef(mood, mname, proto, potentially_ephemeralf_, rt, formals, explicit_condition, actions, lp) ->
                    mname :: cc
                | other -> cc
            let undig_idl (sm:Map<string list, int>) idl =
                vprintln 2 (sprintf "batch_no %i contains rule/method %s" idx (hptos idl))
                sm.Add(idl, idx)
            List.fold undig_idl sm (List.fold dig_idl [] stmt_lst)
        List.fold mapgen2 Map.empty statements99

    // order_control_edges = new ListStore<string list, string list>("order_control_edges")
    let verify_exists arg =
        match rule_batch_map.TryFind arg with
            | Some v -> v
            | None ->
                hpr_yikes(sprintf "execution order pragma: Rule or method %s named for %s was not found" (hptos arg) msg)
                0
    let order_control_edge_list =
        let scannate cc = function
            | (Pragma_shed_control("execution_order" as keyk, sublst), codepoint) ->
                let rec to_xition cc = function
                    | []  -> cc
                    | [b] ->
                        let _ = verify_exists b
                        cc
                    | a::b::tt ->
                        let a_n = verify_exists a
                        let b_n = verify_exists b
                        if vd>=4 then vprintln 4 (sprintf "rule ordering pragma: keyk=%s:  %s/%i edge %s/%i" keyk (hptos a) a_n (hptos b) b_n)
                        //order_control_edges.add a b
                        to_xition ((a_n, b_n)::cc) (b::tt)
                to_xition cc sublst
            | other -> sf (sprintf "Bad or unexpected rule sorting directive. other=%A" other)
        List.fold scannate []  execution_order_markup

    let m_ordering_constaint_map = ref Map.empty
    let additem (mm:Map<int, int list>) p q =
        mm.Add(p, singly_add q (valOf_or_nil (mm.TryFind p)))

    let _ =
        let adder cc (a, b) =
            additem cc b a
        m_ordering_constaint_map := List.fold adder !m_ordering_constaint_map order_control_edge_list

        
    // Toplogical sort of rule and external method execution order now follows.
    let ww = WF 2 "bsvc toplevel" ww (sprintf "prepare for execution order sort. %i items" (length statements99))
    let stmt_map =
        let mapgen (sm:Map<int, bsv_stmt_t list * exaux_t>) (idx, stmt_lst, exordering, exaux) = sm.Add(idx, (stmt_lst, exaux))
        List.fold mapgen Map.empty statements99
    let edgex = new ListStore<string list, int * execution_order_constraint_t>("edgex") // Collate ordering constraints

    let _ =
        let foo (idx, stmt_lst, exos, exaux) =
            if idx >= 0 then app (fun exo -> edgex.add exo.ekey (idx, exo)) exos
            if vd>=4 then vprintln 4 (sprintf "exordering report %i. %i stmts   %A" idx (length stmt_lst) exos)
            ()
        app foo (rev statements99)

    let _ = 
        let exo_recolate kk vv =
            let reads = List.filter (fun (no, exo)->not exo.writef) vv
            let writes = List.filter (fun (no, exo)->exo.writef) vv
            let save (mm:Map<int, int list>) ((read_no, read_exo), (write_no, write_exo)) =
                if read_exo.orderer <> write_exo.orderer then hpr_yikes(sprintf "Disagreeing ordering conventions for single item %s" (hptos write_exo.ekey))
                let (doit, before, after) =
                    match read_exo.orderer with
                        | RO_dontcare -> (false, 0, 0)
                        | RO_write_before_read ->
                            if vd>=4 then vprintln 4 (sprintf " ordering edge: %A  write %i -> read %i    %s <- %s" read_exo.orderer write_no read_no (hptos read_exo.ekey) (hptos write_exo.ekey))
                            (true, write_no, read_no)
                        | RO_read_before_write ->
                            if vd>=4 then vprintln 4 (sprintf " ordering edge %A: read %i -> write %i    %s -> %s" read_exo.orderer read_no write_no (hptos read_exo.ekey) (hptos write_exo.ekey))
                            (true, read_no, write_no)
                            
                if before = after then
                    if read_exo.unsafef then () // We ignore reflexive edges. We complain about them when unsafe flag does not holds.
                    else hpr_yikes(sprintf "Read and write of same wire in a common rule batch when not an unsafe wire. wire=%s kind=%s rule/method=%i" (hptos read_exo.ekey) (hptos read_exo.kind) (before))
                    mm
                elif doit then additem mm before after
                else mm
            m_ordering_constaint_map := List.fold save !m_ordering_constaint_map (cartesian_pairs reads writes)
            ()
        for zz in edgex do exo_recolate zz.Key zz.Value done
        ()

    let edges = !m_ordering_constaint_map
    let sorted = topological_sorter<int> "bsvc: execution-order topological sort" (map f1o4 statements99) edges
    let ww = WF 2 "bsvc toplevel" ww (sprintf "execution order: partial order topological sort complete")    
    if vd>=4 then vprintln 4 (sprintf "Sorted order is %s" (sfold_unlimited i2s sorted))

    let scc_rule_groups = [] // For now. TODO. This information has just been collated essentially.
    let reread nn =
        match stmt_map.TryFind nn with
            | None -> sf(sprintf "bsvc: toplevel: lost stmt no %i" nn)
            | Some (statement, exaux) -> (nn, statement, exaux)


    (map reread sorted, scc_rule_groups)
    
// Retrieve all method names on an interface
let all_method_names ty =
    match ty with
        | Bsv_ty_nom(_, F_ifc_1 protos, dx) ->
            let rec tog_method cc = function
                | Bsv_ty_nom(ats, F_fun _, _) when length ats.nomid >= 1 -> hd ats.nomid :: cc
                // TODO: We need to recurse on nested sub-interfaces here please.
                | other -> sf (sprintf "tog_method other form %A" other)
            List.fold tog_method [] protos.iparts
        | ty -> sf (sprintf "tother exported_ios top method other form %A" ty)
        

//
// Toplevel of non-recursive main compiler body: Synthesise a module and its children.
//
let compile1 ww0 bobj ee (iname, lp) (tl_netso, nominal_prams) (kind, mod_mk) =
    let vd = bobj.misc_vd
    let m = "compile 1 (main compile) " + htosp iname
    let ww = WF 1 m ww0 "start create top-level concrete interface type"
    let hardstate = false
    let rec deabs mod_mk =
        match mod_mk with
        | B_maker_l(idl, ats, (pparams, iused, iftype, hardstate), stmts, ifc_provisos, backdoors, lp) -> (idl, mod_mk)

        | B_maker_abs(idl, ats, (rootmarked, formal_prams, formal_args, formal_iftype, aliases_), provisos, body, ee_inner, backdoors, lp) ->

            if true then (idl, mod_mk)
            else
                let ifc_ty = Bsv_ty_dontcare "L56TL"
                muddy "deabs(deabs4_exp ww bobj ee iname m mod_mk None) // old bootstrap route"
              
        | other -> sf (sprintf "Abstract interface used at top level?  Or other %A" other) // The suggested problem is now the default behaviour but it still must have no free typevars.

    let (top_ifc_idl, mod_mk) = deabs mod_mk // nop

    let (iftype, pparams_in, iused) = 
        let ww = WN "topbootstrap" ww
        match mod_mk with

// Use this with new bootstrap
        | B_maker_abs(idl, ats, (rootmarked, formal_prams, formal_args, formal_iftype, aliases_), provisos, body, ee_inner, backdoors, lp) ->
            let xx (so, ty, y) = (so, ty, y) // If root modules had bindable type vars or prams this might need to be more than a nop?
            let mf() = lpToStr0 lp + ": B_maker_abs"
            let pparams = map xx formal_prams
            let iused = map xx formal_args
            let iftype = top_boot ww bobj ee mf  (SO_none "plugh", Some formal_iftype, "topplugh")  // 'plugh' is the hidden/dummy formal name for the one exported interface of BSV module.
            (f2o3 iftype, pparams, iused)


        | B_maker_l(idl, ats, (pparams, iused, iftype, hardstate), stmts, ifc_provisos, backdoors, lp) -> (iftype, pparams, iused)

    let (top_level_ifc_ty, ikind) =
        match iftype with
        | Bsv_ty_nom(ikind, F_ifc_1 protos, dx) -> (iftype, ikind)
        | other -> sf (m + sprintf "booog other %A" other)

    let mm = sprintf "Starting synthesise (main compile) iname=%s iftype=%s #pparms=%i" (htosp iname) (htosp ikind.nomid) (length pparams_in)
    let ww = WF 3 "compile1" ww0 mm

    let top_exported_ifc = B_ifc(PI_ifc_flat iname, top_level_ifc_ty)

    let qmap00 = { ifcmap=Map.empty; methmap=Map.empty; statemap=Map.empty; pendingmap=Map.empty; fwdpaths=Map.empty; bluewires=Map.empty; exordering=[]  }
    let top_emits = gec_blank_emission "top_emits"
    let atts = ref []

    let pparam_prepare arg (cc, cd) =
        match arg with
            | (SO_parameter, ty, name) ->
                let pram = gen_pram_net ww LOCAL ty name // really local?
                let nb = EE_e(ty, B_hexp(ty, pram))
                (nb::cc, arg::cd)

            | (so_, ty, name) ->
                let (hide, io) = // The director's clock and reset are considered as bluespec parameters, but become I/O nets in the RTL.  The parameters need hiding.  The render recipe stages will re-insert the io's from the directorate, so they are also not returned in the netlist from the front end.
                    match ty with
                        | Bsv_ty_nom(ats, _, []) when ats.nomid = g_canned_Clock_name || ats.nomid = g_canned_Reset_name ->
                            vprintln 3 (sprintf "Top-level parameter '%s' is part of directorate." name)
                            (true, INPUT)
                        | _ -> (false, LOCAL)
                // vprintln 0 (m + sprintf ": parameter form '%s' type %s" name (bsv_tnToStr ty))
                let pram = gen_pram_net ww io ty name            
                let nb = EE_e(ty, B_hexp(ty, pram))
                (nb::cc, (if hide then cd else arg::cd))
            
    let (pparams_env, pparams_on_show) = List.foldBack pparam_prepare pparams_in ([], [])

    let rec pair_nominal_params = function
        | (_, [])
        | ([], _) -> [] // Zip but stop early if run out of values
        | (h::tt, pv::pvs) ->
            let fid =
                match h with
                    | (SO_parameter, ty, name) -> name
                    | (_, ty, name) -> cleanexit(m + sprintf ": unsupported parameter form '%s' type %s" name (bsv_tnToStr ty))
            let nominal =
               let pv' = bsv_eval_pram bobj (WN ("pram override " +  fid) ww) ee.genenv pv
               match pv' with
                   | EE_e(_, B_hexp(_, x)) -> x
                   | other -> cleanexit(m + sprintf ": for '%s': unsupported parameter nominal value form %A" fid other)
            vprintln 3 (sprintf " Eval'd pram expression %s to %s" (bsv_expToStr pv) (xToStr nominal))
            (fid, nominal)::pair_nominal_params(tt, pvs)
            
    // The following attribute makes this revert to a Verilog parameter if this component is conveted to RTL later on in the recipe.
    mutadd atts (Napnode("parameters", map (fun (_, _, fid) -> Nap(fid, "")) pparams_on_show)) // Encode parameter names this way
    let pram_pairs = pair_nominal_params(pparams_on_show, nominal_prams) // but the value are here.


    let gen_top_imported_ifc arg ((qmap, ee), ifs_sofar, currency_sofar) =
        match arg with
        | (_, ((Bsv_ty_nom(iidl, F_ifc_1 protos, dx)) as ty), name) ->
            let sty = ty
            let tlu = (Some None, true)
            let borrowed = []
            let ifc_name = [ name ]
            let mod_ats = []
            //vprintln 0 (sprintf "Adding top bind '%s'  %A\n\n\n" name ty)
            let ww = WN ("top_imported " + name) ww
            let iexported = B_ifc(PI_ifc_flat ifc_name, ty)
            let hardstate = false
            let pass = { pass_no=0; pass_name="top-level-one-off"; m_statements= ref [] }
            let siggy = mk_ifc_currency ww bobj pass (fun()->"top_imported") (Some top_emits) tlu borrowed hardstate ee (iexported, mod_ats, ifc_name, ty)
            let inode = IIS (funique(hptos ifc_name)) // CAMRET not using the routine.
            ((sigma_add_methods "TL-418" ifc_name inode qmap siggy, ee), EE_e(ty, iexported)::ifs_sofar, (map snd siggy)@currency_sofar)

        | (_, ty, name) -> cleanexit(m + sprintf ": (this does not appear to be an interface type) unsupported imported interface form '%s' type=%s" name (bsv_tnToStr ty))
    let ((sig01, ee01), imported_ifs, imported_currency) = List.foldBack gen_top_imported_ifc iused ((qmap00, ee), [], [])
    let imported_ifc_ios = List.foldBack mine_ionets_3 imported_currency []
      
    let top_actuals = pparams_env @ imported_ifs
    let env01 = (sig01, { ee01 with genenv=ee01.genenv.Add(top_ifc_idl, GE_binding(top_ifc_idl, {g_blank_bindinfo with whom="top_level_ifc_insert"}, EE_e(top_level_ifc_ty, top_exported_ifc))) })
    let top_abs_cp =  "TOP_ZZ00" :: iname
    let uid = funique "UID_TOPLEVEL"
    let dirinfo = ee.attribute_settings.dirinfo


    // First pass is prescan for forwarding paths and flattening. It should cleanly collect all_stmts for prescan to be applied to. The execution order constraints are also collected. The definition statements and executed and the resulting environments are captured. FSM sublanguate is expanded. Executable statements are collected in numbered groups. Implementation logic from primtives is collected.
    let (top_inode, sigma9, envitems, statements99) = 
        let ww = WF 2 "bsvc toplevel" ww "start first pass"
        let emits_o = Some top_emits
        let m_statements= ref[]
        let pass =
            {
                pass_no=      1
                pass_name=    "first"
                m_statements= m_statements 
            }
        let ((top_inode, sigma9), envitems) = bsv_instantiate_module ww false bobj pass uid (Some tl_netso) (*an option option*) lp emits_o env01 (dirinfo, top_exported_ifc, iname, (iname, CPS(PS_prefix "LT155", map (fun x->(x, 0, None)) top_abs_cp)), top_actuals) mod_mk

        let sigma9 =
            carenv_write bobj pass "toplevel L451" sigma9 iname top_inode // Save the interface inode in the interface map.    -- Surely this is now done in instantiate module since we madly rely on this for ephemeral method def control.  Seemingly not. This is needed.
        (top_inode, sigma9, envitems, rev !m_statements)

    let ww = WF 2 "bsvc toplevel" ww "finished first pass"

    
    let sigma9 =
        if bobj.forwarding_paths_support then 
            let ww = WF 2 ("forwarding path finder") ww "start"
            let (statemap, fwdpaths) = forwarding_paths_prescan ww bobj (None) sigma9 (sigma9.statemap, sigma9.fwdpaths) (list_flatten (map f2o4 statements99))
            let ww = WF 2 ("forwarding path finder") ww "finished"            
            { sigma9 with statemap=statemap; fwdpaths=fwdpaths }
        else sigma9


    let (shed3, execution_order_markup) = degroom_schedulling_atts ww !bobj.m_shed_control_list

    let (statements99, scc_rule_groups) = bsvc_execution_order_sort ww bobj execution_order_markup statements99

    // The new second pass is now the real compile site - replaces pass 1.
    // This writes rule ctl to emits.rulectl
    let sigma9 =
        let ww = WF 2 ("bsvc toplevel") ww (sprintf "Start second pass: new site main compile")
        let pass_no = 2

        let emits_o = Some top_emits
        let pass =
            {
                pass_no=      pass_no
                pass_name=    i2s pass_no
                m_statements= ref []
            }
        let g0 = g_empty_resource_record
        let sigma9 = 
        //New site for the rule replicator, observing rulerec.copycount
        //Any pair of rules that are interconnected by Bluewires are in an equivalence class, called an scc_rule_group, that must be replicated en masse. 
        //let all_stmts = insert_superscalar_rule_repetitions // Copy in the code here from the other tree please.
            let new_emits_o =
                    let emitbin = gec_blank_emission "mainmash"
                    //dev_println (sprintf "Rez emitter %s" emitbin.uid)
                    Some emitbin 

            let sigma9 =
                    let compile_numbered_block sigma9 (no, stmts, (ifc_ty, ifc_name, toplevel, ee1)) =
                        let ww = WF 2 ("toplevel") ww (sprintf "Start new main compile batch %i on %i statements" no (length stmts))
                        if no < 0 then sigma9
                        else
                            //dev_println(sprintf "new site main pass: compile %i numbering: %s" no (sfold bsv_stmtToStr stmts))
                            let (sigma9, ee1) = List.fold (bsv_compile_Stmt ww bobj pass (ifc_ty, top_inode, ifc_name) g0 toplevel new_emits_o no) (sigma9, ee1) (zipWithIndex stmts)
                            let (eocs_, sigma9) = (sigma9.exordering, { sigma9 with exordering=[] })
                            sigma9
                    List.fold compile_numbered_block sigma9 (statements99)

            if not_nonep emits_o then
                //dev_println(sprintf "attach emits %s to %s" (valOf new_emits_o).uid (valOf emits_o).uid)
                mutadd (valOf emits_o).children (valOf_or_fail "L7881" new_emits_o)
            sigma9
        let ww = WF 2 ("bsvc top level") ww (sprintf "Finished main compile (second pass).")
        sigma9


    let sigma9 =
        let mm = "sigma_c statemap commit to hard currency"
        let ww = WF 2 ("bsvc top level") ww mm
        let mf() = mm
        let ww = WN mm ww
        let lower x = bsv_lower ww bobj x

        let commit_to_hard_currency ww msg methmap key update = // Committed updates are still in sigma_c and need to here be wired into the net-level Sigma (aka hard currency).
            match update with
                | MS_denot(monica, whom, live_alpha, (argty, argvale), subsc_o, cmd_method_name) ->
                    if not_nonep subsc_o then muddy "array entry finish"
                    let (key1, concrete_item_o, eph_item_o, error_o) = find_meth mf sigma9 (key) cmd_method_name // best pass sigma9 around this inner loop i think todo
                    let _:interface_inode_name_t * method_name_t = key1
                    if not_nonep error_o then
                        //dev_println (mm + sprintf ": fname=%s  key=%s" cmd_method_name (fst key))
                        cleanexit(valOf error_o)
                    let argvale = lower argvale // TODO generalise to more than one argvale
                    // We do not need to log a conflict for super-scalar updates since none that we so-far support can conflict. Also ownership/monica information is obscured.
                    //log_conflict_record emits_o mm (rulename, resname, fname, [(argty, B_exp(argty, argvale))], resats.idempotentf)  // Log lowered value to save lowering it again

                    match concrete_item_o with
                        | Some (Sigma(id3, resats, (proto, ty_rv, m_ats), [solo_arg])) ->
                            //vprintln 3 (mm + sprintf " for %s" (f2o3 key))
                            let go = live_alpha
                            let sendarg (ty, net, x) =
                                //let nv = if nullp x.encol then argvale else ix_query go argvale x.enco
                                (ty, net, {encol=(monica, go, argvale)::x.encol})
                            let proto = alpha_fire (monica) live_alpha proto
                            let sigcur = Sigma(id3, resats, (proto, ty_rv, m_ats), [sendarg solo_arg])
                            (methmap:methmap_t).Add(key1, sigcur)
                        | other -> sf (mm + sprintf ": Expected hard currency for %s. Not %A" (hptos (snd monica)) other)
        let methmap = Map.fold (commit_to_hard_currency ww "toplevel") sigma9.methmap sigma9.statemap
        let ww = WF 2 ("bsvc toplevel") ww (sprintf "finished new site main compile")
        { sigma9 with methmap=methmap; statemap=Map.empty }

        // Multicycle schedules: (future work)
        //   If we wish to have restructure operate on each rule or external method independently, we need each rule or external method to have its own director and for restructure to operate in TLM sythesis mode and render the handshake nets in a Bluespec-compatible mode.
        //   In TLM synthesis mode, resources shared over methods will need run-time arbitration, as per usual when shared over threads, inside restructure.
        //   Central resources such as arbiters will not be generated in the Bluespec front end owing to restructure doing it.  Passing the Bluespec priority mark up and other attributes down will require some work.
        
    let sigma9 =
        let ww = WF 2 ("bsvc top level") ww (sprintf "render_forwarding_logic start")
        let lower x = bsv_lower ww bobj x
        render_forwarding_activations ww lower top_emits ("toplevel") sigma9.fwdpaths
        { sigma9 with fwdpaths=Map.empty }


    establish_log false ("scheduling:" + htosp iname)
    let ww1 = WF 1 "bsvc toplevel" ww ("Scheduler start/Commence for " + hptos iname)
    let sched =
        match bobj.scheduler_name with
            | "none" -> sched0_bsvc.no_schedule
            | _      -> sched1_bsvc.default_schedule

    let (_, rulectl, _, _, conflict_log) = emit_miner "L324" ([], [], [], [], []) top_emits 



    let sh_emits = gec_blank_emission "sh_emits"
    mutadd top_emits.children sh_emits

    let _ = sched ww1 bobj (hptos iname) sh_emits ee.attribute_settings.dirinfo shed3 conflict_log rulectl // rather than pass the clkinfo in it should be noted in the individual emits - TODO? Or done?

    let _ = WF 1 "bsvc scheduler" ww "Finished"    
    // Now produce input and output terminals for the top-level interface. If this is a separately-synthesised yet instantiated
    // subcomponent we need to issue the same formal names in the parent as in the instance statment. Also we supply continuous assigns to those
    // formal names in the parent just as though it were not separately synthesised.
    let exported_ios =
        let mf () = "exported_ios gex"
        match hd envitems with // We use head here for access to the solo top-level module.
        | (idl_, GE_binding(_, _, EE_e(ty, B_ifc(PI_ifc_flat iidl, prams)))) ->
            match find_ifc mf iidl sigma9 with
                | IIS inode ->
                    let gex method_name cc =
                        match sigma9.methmap.TryFind (IIS inode, method_name) with
                            | None -> sf "missing method L595"
                            | Some currency ->
                                mine_ionets_2b currency cc
                    List.foldBack gex (all_method_names ty) []
                     



        
    let ww = WF 2 "compile1" ww0 "Starting final tie offs"

    for kv in sigma9.methmap do (capoff1 ww bobj vd ee sh_emits exported_ios) kv.Key kv.Value done

    let (nets, _, comb_logic, synch_logic, conflicts) = emit_miner "L354" ([], [], [], [], []) top_emits 

    let synch_logic_domains =
        let domains = generic_collate fst synch_logic
        vprintln 2 (sprintf "Number of distinct directors (clock/reset/cen domains) found is %i" (length domains))
        domains

    let ii = { id="from_bsvc" }:rtl_ctrl_t        

    let execs =
        let gec_block (dirinfo, synch_logic_revd) =
            let reverse_logic cc (dom_, logic) = (rev logic) @ cc
            // Generate the HPR L/S director from the local dirinfo. See also gbuild.gbuild_synth_director in future?
            let director = { g_null_directorate with duid=if nonep dirinfo.monica_o then next_duid() else hptos(snd(valOf dirinfo.monica_o)) }
            let director =
                let gedge arg =
                    let arg = g_input (Some sh_emits) ([arg], 1, [])
                    if dirinfo.clkpos then E_pos arg else E_neg arg
                if dirinfo.dclk = "" || dirinfo.dclk = "no_clock" then cleanexit(sprintf "A clock name is needed.  Cannot use '%s'" dirinfo.dclk)
                else  { director with clocks= [ gedge dirinfo.dclk ] }
            let director =
                let cens =
                    if dirinfo.dcen = "" || dirinfo.dcen = "no_gate" then []
                    else [ g_input (Some sh_emits) ([dirinfo.dcen], 1, []) ]
                { director with clk_enables=cens }
            let director =
                match dirinfo.monica_o with
                    | None -> director
                    | Some(true, monica) -> // External method, denoted with true in first position.
                        let handshakes = 
                            match sigma9.ifcmap.TryFind monica with
#if WIP
                             // Now need to lookup methods on that thread - there will be only one.
                                | Some(Sigma(method_id, _, (BsvAction(Some rdy_net, firelst, enable_net), m_ats, retval), snargs)) ->
                                    dev_println (sprintf "external method currency handshakes RDY=%s EN=%s" (netToStr rdy_net) (netToStr enable_net))
                                    [ rdy_net; enable_net ]
                                    //muddy ("need handshakes " + hptos monica)
#endif
                                | other  -> sf(sprintf "External method handshake locate: other sigma lookup form %A" other)


                        let bsv_hfast_prams = // The HFAST protocol degenerated to Bluespec mode: no ack or ackrdy nets.
                            {
                                max_outstanding=    1

                                posted_writes_only= true
                                req_present=        true   // The Bluespec EN signal
                                ack_present=        false
                                reqrdy_present=     true   // The Bluespec RDY signal
                                ackrdy_present=     false
                            }

                        { director with handshakes=handshakes; hang_mode=DHM_pipelined_stream_handshake("puddle", bsv_hfast_prams) } // External methods need handshake. Rules do not.
                    | Some(false, monica) ->
                        { director with hang_mode=DHM_auto_restart } // Rule.
            let director =
                let gnet arg = g_input (Some sh_emits) ([arg], 1, []) //  perhapse use bnetgen g_ip_xact_is_Reset_tag 
                // let default_bluespec_reset () =  (false, false, gnet "RST_N") // Active low, synchronous.
                if dirinfo.dreset  = "no_reset" || dirinfo.dreset = "" then { director with resets=[] }
                else { director with resets= [ (dirinfo.rstpos, dirinfo.assynchreset, gnet dirinfo.dreset) ] }
            H2BLK(director, SP_rtl(ii, List.fold reverse_logic [] synch_logic_revd))

        map gec_block synch_logic_domains

    let execs =
        if nullp comb_logic then execs // Site 1 for comb logic.
        else
            let comb_director = { g_null_directorate with  duid=next_duid() }
            [H2BLK(comb_director, SP_rtl(ii, comb_logic))] @ execs

    if bobj.make_a_listing then
        let concisef = true
        let queries =
            { g_null_queries with
                yd=               YOVD 3
                concise_listing=  concisef
                full_listing=not  concisef
                logic_costs=      None
            }
        anal_block ww None queries (hd execs)

#if SCCANAL
    let tripgen1 c = function
        | H2BLK(clko_, SP_rtl(_, xrtl)) ->
            let r = List.fold (tripgen 4 true) [] xrtl
            if vd>=4 then vprint (4) (sfoldcr tripToStr r + " was an xrtl trip\n") 
            r @ c

    let trips = List.fold tripgen1 [] execs
    reportx 3 "Trips" tripToStr trips
    let ll = length trips
    let _ =
        if ll > 0
        then // Find strongly-connected components ... why?
            let tpairs = zip2([ 0 .. ll-1], trips)
            let ta = Array.create ll (hd trips)
            app (fun (i, trip) -> Array.set ta i trip) tpairs
            let d_in_s ((d, c, s, _), (d', c', s', _)) = sd_intersection_pred d s'
            let edgefind c (i, trip) =
                let scan c (i', trip') = if d_in_s(trip, trip') then (i, i') :: c else c
                List.fold scan c tpairs 
            let edges = List.fold edgefind [] tpairs // TODO avoid quadratic search with inverted index
            let nodes = map fst tpairs
            let pof = (fun i -> i2s i + tripToStr(ta.[i]))
            let sccs = tarjan<int> (Some pof) (nodes, edges)
            vprintln 0 (i2s (length sccs) + " sccs")
            app (fun q -> vprintln 0 ("\n\n----------------------len=" + i2s(length q) + " ----\n" + sfoldcr pof q + "---------\n")) sccs
            ()
#endif

    let ww = WF 1 "compile1" ww0 "Finished"
    reportx 3 "Exported I/Os on primary interface (without clock(s))" netToStr exported_ios
    reportx 3 "Imported interface I/Os (without clock(s))" netToStr imported_ifc_ios
    let ios = imported_ifc_ios @ exported_ios 
    let nets = db_subtract ww nets ios
    //reportx 3 "Final nets (without clock(s))" netToStr nets
    (!atts, pram_pairs, (ios, nets), execs, sigma9)


let lookup_mks bobj name =
    let (found, mm) = bobj.m_modgens.TryGetValue name
    if found then Some mm else None


let compile1_target ww bobj ee (name, lp) (tl_netso, nominal_prams) =
    let ww0 = WF 1 "bsvc compile1_target" ww (htosp name)
// Actually we have logged the synthesis roots by their full name so lookup in ee bindings does not work - and hence don't now need double entry kludge for mks.
    match lookup_mks bobj name with
        | None->
            let explain idl _ = vprintln 0 (sprintf "  Possible compilation target name " + hptos idl)
            for k in bobj.m_modgens do explain k.Key k.Value done
            cleanexit(sprintf "Synthesis root module '%s' was not found." (htosp name))
        | Some mod_mk ->
            //vprintln 0 (sprintf "Normed AST for '%s' was %A\nEnd of AST\n\n" (htosp name) mod_mk)
            //vprintln 0 (sprintf "mod_mk is %A" mod_mk)            
            let folder = System.IO.Directory.GetCurrentDirectory()
            let ww = WF 1 "CBG-BSV" ww ("Commence compile of module " + htosp name + " in " + folder)
            compile1 ww bobj ee (name, lp) (tl_netso, nominal_prams) (name, mod_mk)



//
// For each instantiated sub component ...               
//
let subcomponent_instance ww bobj sigma9 m_nets atts (kind_idl, ifc_ty, iname, dirinfo, iactuals) cc =
    let mf() = sprintf "subcomponent/blackbox instance kind=%s iname=%s" (hptos kind_idl) (htosp iname)
    let ww = WF 1 "subcomponent_instance" ww ( "start " + mf())
    //vprintln 0 (smsg + sprintf " iactuals=%A" iactuals)
    let add_kind_to_formals = g_add_kind_to_formals // This probably needs setting on a per-block basis as read from IP-XACT generated by child compilations.

    let currency = // Retrieve all interface currency for this component.
        let cvx = find_ifc mf iname sigma9
        match cvx with
            | IIS inode ->
                let trag2 method_name cc =
                    match sigma9.methmap.TryFind (IIS inode, method_name) with
                        | None -> sf "missing method L758"
                        | Some currency -> currency :: cc 
                List.foldBack trag2 (all_method_names ifc_ty) []

    let contacts =
        if nullp currency then []
        else
            let mine_ionets_2a sigma cc =
                let (method_name, formal_names) = 
                    match sigma with
                    | Sigma((_, _, fname), _, _, t3lst) -> (fname, map f1o3 t3lst)
                    //_ -> sf(sprintf "L579a ionets_1a: idl=%s other=%A" (hptos kind_idl) other)
                let add_formal_prefix qxflag fid =
                    let a0 =
                        if g_bsc_preferred_netnaming then
                            if fid = "RV" then method_name  // There is no RV suffix for the result bus in the Bluespec preferred way.
                            elif qxflag then fid + "." + method_name
                            else method_name + "." + fid
                        else method_name + "." + fid // Use dot as separator here, as per hptos, which makes diosim happy, but verilog_gen will convert these dots to underscores for RTL output.
                    // pri (sprintf "yield for qxflag=%A  render %s" qxflag a0)
                    if add_kind_to_formals then hptos kind_idl + "." + a0 else a0
            
                let (rdy, en, rv, args) = (find_rdy sigma, find_en_o sigma, find_rvo sigma, find_args "mine_ionets_2a" sigma)
                let vret = function
                    | None -> []
                    | Some x -> [(add_formal_prefix true "RV", x)]
                let qx fid n = if int_constantp n then [] else [(add_formal_prefix true fid, n)]
                let nets = (qx "RDY" rdy) @ (if en=None then [] else qx "EN" (valOf en)) @ (vret rv) @ (List.zip (map (add_formal_prefix false) formal_names) (map f2o3 args))
                if nullp nets then cc else (method_name, nets)::cc
            List.foldBack mine_ionets_2a currency []


    let prams =
        let get_pram arg (cd) =
            match arg with
                | (top_id_, LT_curr currency) -> cd
                | (_, LT_pram x) -> x :: cd
        List.foldBack get_pram iactuals []

    let son_instance =
#if OLD
       // if false then // Don't synthesise it just because it is used anymore.
                    let ww = WF 1 smsg ww "mkmain"
                    let contacts = map (fun (a, b) -> (a, map snd b)) contacts
                    let son = "handle_synth_target ww (kind_idl, lp) (Some contacts, prams)"
                    let ww = WF 1 smsg ww "done"
                    sf "son"
#endif
            // Having the actual net used as a contact used to be sufficient to declare it but HPR L/S now seems to like a separate declaration
            // The declaration is coming from the currency now.
        let declare_contact_actual net = mutadd m_nets net
        let contacts =
            if false then []
            else
                let gec_contact_group(pi_name, nets) =
                    let wrap (fid, actual_net) =
                        declare_contact_actual actual_net
                        DB_leaf(Some (gec_X_net fid), Some actual_net)
                    let db1 = { g_null_db_metainfo with not_to_be_trimmed=true; form=DB_form_external } 
                    gec_DB_group(db1, map wrap nets)
                list_flatten(map gec_contact_group contacts)

        let spare_emits = gec_blank_emission "spare_emits" // Could be None really.
        let ctrl_contacts =  // Directorate/control contact wiring added here. HPRLS can better automate this ?
            let clknet = g_input (Some spare_emits) ([dirinfo.dclk], 1, [])
            //let cen = ...
            let resetnet = g_input (Some spare_emits) ([dirinfo.dreset], 1, [])
            let s = [ DB_leaf(Some clknet, Some clknet); DB_leaf(Some resetnet, Some resetnet) ]
            let db1 = { g_null_db_metainfo with not_to_be_trimmed=true; form=DB_form_external }
            gec_DB_group(db1, s)

        let decls = contacts @ ctrl_contacts
        let vlnv = { vendor= !g_m_xact_op_vendor; version= !g_m_xact_op_version; library= !g_m_xact_op_library; kind= ["-noname-"] } // This VLNV should not be filled in here but instead from the external IP library lookup.
        let iinfo = { g_null_vm2_iinfo with generated_by="bsvc"; preserve_instance=true; definitionf=false; kind_vlnv=vlnv; iname=hptos iname }
        //dev_println(sprintf "Create VM2 instance: iname %s" (hptos kind_idl))
        (iinfo, Some(HPR_VM2({ g_null_minfo with name=vlnv; atts=atts }, decls, [], [], [])))
    let ww = WF 1 "subcomponent_instance" ww "finished"
    son_instance :: cc

    
// Break up a dotted name into a reversed list of components.
let import_flat_name ss =  (anti_hptos ss, LP("command-line", 0)) 



let g_default_incdir = "." // This should default to the $(PRIV_DISTRO)/libs/camlib macro setting ... perhaps use a getenv to set the default?
//
//
//   
let create_some_vms ww0 (c1:control_t) names =
    let sn = "bsvc"
    let msg = "create_some_vms:" + sfold (fun x->x) names
    let ww = WF 3 "create_some_vms" ww0 msg
    let incdir_path =
        let path = hd(arg_assoc_or msg ("incdir", c1, [g_default_incdir]))  
        if path=g_default_incdir then hpr_yikes (sprintf "No -incdir has been set.  You may need -incdir=$(PRIV_DISTRO)/libs/camlib to find the standard prelude and other library files.")
        path
    let split_default = control_get_s sn c1 "bsv-split-if" None = "enable"
    let lextrace      = control_get_s sn c1 "bsv-lextrace" None = "enable"
    let printtree     = control_get_s sn c1 "bsv-print-parsetree" None <> "disable"
    let alltrees      = control_get_s sn c1 "bsv-print-parsetree" None = "all"
    let prelude_file  = control_get_s sn c1 "Prelude" (Some "Prelude")


    // Here is a first stab at allowing command line control over some of the basic director defaults. Convergence with the Bluespec Inc command line flags is preferable, so this is likely to change/evolve.
    // Some of these that I have suffixed '_prefix' should perhaps not have that suffix!
    let default_clock_prefix   = control_get_s sn c1 "bsv-default-clock-prefix" None
    let default_reset_prefix   = control_get_s sn c1 "bsv-default-reset-prefix" None
    let default_clk_polarity   = control_get_s sn c1 "bsv-default-clk-polarity" None
    let default_reset_polarity = control_get_s sn c1 "bsv-default-clk-polarity" None
    let default_reset_oscity   = control_get_s sn c1 "bsv-default-reset-oscity" None
    let default_gate_prefix    = control_get_s sn c1 "bsv-default-gate-prefix" None

    let default_director_info =
      {
        dclk=           default_clock_prefix
        dreset=         default_reset_prefix
        clkpos=         default_clk_polarity = "posedge"
        rstpos=         default_reset_polarity = "positive"
        assynchreset=   default_reset_oscity = "asynchronous"
        dcen=           default_gate_prefix
        monica_o=       None
      }

    let compilation_stoplist =
        let v = control_get_s sn c1 "compilation-stoplist" (Some "")    // Same as pragma compilation_root
        if v="" then [] else map anti_hptos (split_string_at [ ',' ] v)




    let prelude_asts =
        if prelude_file = "" || prelude_file = "NONE"
        then []
        else
            let ww = WF 1 "bsvc" ww0 "Reading prelude file."
            let prelude_files = split_string_at [ ';' ; ',' ] prelude_file
            let rd_prelude prelude_file = 
                let (filename, _) = pathopen incdir_path 6 [';'; ':'] prelude_file "bsv"
                if nonep filename || (not(existsFile(valOf filename)))
                        then cleanexit("BSV search incdir_path=" + incdir_path + "\nCannot open for import prelude filename=" + prelude_file)

                lexbsv_parse ww false alltrees [] (valOf filename)
            map rd_prelude prelude_files

    let prelude_constags =
        let gpc cc = function
            | Some(ast, constags) -> lst_union constags cc
            | None -> cc
        List.fold gpc [] prelude_asts

    let raw_asts =
        let ww = WF 1 "bsvc" ww0 "Reading user source file(s)"
        let qname fname =
            let ast = lexbsv_parse ww lextrace printtree prelude_constags fname
            if nonep ast then cleanexit ("Failed to parse " + fname)
            else (fname, valOf ast)
        map qname names

    let ww = WF 1 "bsvc" ww0 "setup settings"
    let vd = max !g_global_vd (control_get_i c1 "bsv-basic-loglevel" 3) // All Orangepath tools must default their loglevel to 3. More detailed logging than level 3 accordingly is not generated by default.
    let sched_vd = max !g_global_vd (control_get_i c1 "bsv-sched-loglevel" 3)

    let typecheck_vd = max !g_global_vd (control_get_i c1 "bsv-typecheck-loglevel" 3)
    vprintln 3 (sprintf "Set verbal diorosity loglevel to %i" vd)
    let bram_subscription_support =
        match control_get_s sn c1 "bsv-BRAM-native-support" None with
            | "single-ported" -> 1
            | "dual-ported"   -> 2
            | "disable"
            | _               -> 0

    let bobj = { // Global settings and mutable vars for one compilation.
                 stagename=                sn
                 current_synth_target=     [ "*Not-compiling-anything-at-the-moment*" ]


                 // Debug, loging and reporting control:
                 misc_vd=                  vd // It would be sensible to allow individual control over these loglevels.
                 normalise_loglevel=       vd
                 afreshen_loglevel=        typecheck_vd
                 dropping_loglevel=        typecheck_vd 
                 sum1_loglevel=            typecheck_vd
                 wverbosef=                control_get_s sn c1 "wverbose" None = "enable"

                 build_loglevel=           vd
                 sched_loglevel=           sched_vd
                 write_normed_logs=        true //TODO make optional
                 make_a_listing=           control_get_s sn c1 "bsv-make-listing-of-elaborated-code" None <> "disable" 
                 write_tables=             Some "tables.txt" // TODO from cmdline/recipe
                 
                 
                 incdir_path=              incdir_path
                 print_alltrees=           alltrees


                 // cmdline/recipe settings:
                 scheduler_name=           control_get_s sn c1 "bsv-scheduler" None
                 subevalcost=              control_get_i64 c1 "bsv-subexpression-cost" 10L
                 add_simple_arbiters=      control_get_s sn c1 "bsv-auto-arbiters" None <> "disable" 
                 round_robin_mode=         control_get_s sn c1 "bsv-auto-arbiters" None = "round-robin"                

                 idempotent_actions=       control_get_s sn c1 "bsv-idempotent-actions" None = "enable"
                 multiple_scalar_write_per_cycle= control_get_s sn c1 "bsv-enable-multiple-scalar-writes" None = "enable"
                 multiple_array_write_per_cycle= control_get_s sn c1 "bsv-enable-multiple-array-writes" None = "enable"                 
                 respect_rule_repeat_counts=   control_get_s sn c1 "bsv-respect-rule-repeat-counts" None = "enable"                 
                 report_rule_fire_rates=   control_get_s sn c1 "bsv-report-rule-fire-rates" None = "enable"                 
                 reentrant_methods=        control_get_s sn c1 "bsv-reentrant-methods" None = "enable"                 
                 elab_limit=               control_get_i c1 "bsv-elab-limit" 10000    
                 whilefor_limit=           control_get_i c1 "bsv-whilefor-limit" 1000    
                 keep_fires=               control_get_s sn c1 "keep-fires" None = "enable"
                 aggressive_conditions=    control_get_s sn c1 "aggressive-conditions" None = "enable"
                 textual_reverse=          control_get_s sn c1 "bsv-first-listed-rule" None = "lowest"
                 //
                 norecurse_flag=           false
                 unif_iterations=          6
                 forwarding_paths_support=   bram_subscription_support > 0
                 bram_subscription_support=  bram_subscription_support
                 logic_cost_walker=     { g_null_queries with logic_costs=Some(logic_cost_walk_set_gen ww vd "bsvc" false) }
                 

                 handshake_done_by_restructure= false      // Experimental advanced multi-cycle extensions for the future

                 // Mutables/databases.
                 utypes=                   new utypes_t("unificationOfTypes")
                 prags=                    new prags_t("prags") // Pragmas and provisos - they use the same syntax and management.
                 e_roots=                  new e_roots_t("e_roots")
                 lowered_expressions1=     new lowered_expressions1_t() 
                 lowered_expressions2=     new lowered_expressions2_t()
                 utype_gen=                ref 1
                 callgraph_recursions=     new ListStore<string, string list * int>("callgraph_recursions");
                 //m_targets=                ref []
                 m_bboxes_encountered=     ref []
                 m_synths=                 ref []
                 m_shed_control_list=      ref []
                 m_tables=                 ref []
                 m_modgens=                new Dictionary<string list, bsv_exp_t>()
                 m_compilation_stoplist=   ref compilation_stoplist // Mutable since may be extended by parsed attributes.
              }

    g_show_type_formalpram_namehints := control_get_s sn c1 "show-type-formalpram-namehints" None = "enable"; // Show formal names for types in debugging output - these are occasionally useful for debugging.

    setup_extra_recursion bobj

    let attribute_settings_t =
        {
            split_default=    split_default
            dirinfo=          default_director_info
            fire_rate=        None
            superscalar_rate= 1
        }
    let (ee:ee_t) =
             { 
               hof_float=                  false
               attribute_settings=         attribute_settings_t
               elab_steps=                 ref bobj.elab_limit
               // These next ones are fresh for each module
               vectors=                    new OptionStore<string, vector_t>("vectors")
               muts=                       new muts_t("muts")
               genenv=                     valOf !g_top_ee
               typeclasses=                new (typeclasses_t)(Map.empty)
               ctor_tags=                  Map.empty
               skip_synthesise_directives= false
               imported=                   []
               grounded=                   false
               forcef=                     Some false
               // Then per rule/method
               prefix=                     []
               static_path=                []
               module_path=                []
               //rulename_issue=           None
               callpath=                   CPS(PS_prefix "LT341", [(funique "TOP", 0, None)]) // The static chain or callstring.
               fbody_context=              false
               dynamic_chain=              []
             }

    let ee = { ee with grounded= true } // Packages are not polymorphic so we can set grounded during norm0 of package items.
        
    let (ee, _) =
        let norm_prelude arg (ee, constags) =
            match arg with
                | None -> (ee, constags)
                | Some(ast, constags) -> norm0 (WN ("norm0 prelude") ww0) bobj (ee, constags) ast
        List.foldBack norm_prelude prelude_asts (ee, [])

    let ee =
        let q_ast ee (name, (raw_asts, constags)) =  // 
            let (ee, _) = norm0 (WN ("norm0: " + name) ww0) bobj (ee, constags @ prelude_constags) raw_asts
            let ww = WF 1 ("create_some_vms") ww0  ("AST for '" + name + "' internalised")
            ee
        List.fold q_ast ee raw_asts

    let opname = control_get_s sn c1 "o" (Some "out")

    // It is meaningless for the user to use both -g and -gg.
    let rec handle_synth_target ww0 (idl, lp) (tl_netso, nominal_prams) =
        let m_encountered = ref []
        vprintln 3 (htosp idl + sprintf ": synth target prams=%s" (sfold bsv_expToStr nominal_prams))
        let (bobj:bsvc_free_vars_t) = { bobj with m_bboxes_encountered= m_encountered; current_synth_target=idl }
        let (atts, pram_pairs, (formals, locals), execs, sigma9) = compile1_target (WN "create_some_vms" ww0) bobj ee (idl, lp) (tl_netso, nominal_prams)
        let atts = Nap("preserveinstance-assoc", "true") :: Nap("preserveinstance", "") :: atts // This attribute stops flattening in later parts 
        let m_nets = ref []
        let m_comb_logic = ref []
        let xlats = ref []
        let walk_aux =
            let walkctl = bsvc_walker_gen ww vd bobj (bobj.lowered_expressions2)
            let m = "msg"
            let tallies = None
            let pli_log_ = "" 
            (m, walkctl, tallies, pli_log_)
        for z in bobj.lowered_expressions2 do ignore(mya_walk_it walk_aux g_null_directorate (z.Key)) done 
        let make_sharer = function
            | (l, (r:memo_t)) when !r.net <> None ->
                let unmake = length !r.parents < 2 && bobj.e_roots.lookup (l) = None // Unmake if referred to in only one place and not a root.
                if unmake then vprintln 0 ("Want to unmake subs " + xToStr l + " net=" + xToStr (valOf !r.net) + " p=" + sfold i2s !r.parents)
                else vprintln 0 ("Want to keep subs " + xToStr l + " net=" + xToStr (valOf !r.net) + " p=" + sfold i2s !r.parents)
                if not unmake then
                    mutadd m_comb_logic (gen_buf(valOf !r.net, l))
                    mutadd xlats (l, valOf !r.net)
                    mutadd m_nets (valOf !r.net)
                else mutadd xlats (valOf !r.net, l)

            | (_, _) -> ()
        for z in bobj.lowered_expressions2 do make_sharer (z.Key, z.Value) done // TODO use .AsList ?
        let mapping = makemap(!xlats)
        let comb_execs =
            if nullp !m_comb_logic then [] // Site 2 for comb_execs
            else
                let ii = { id="from_bsvc:comb" } : rtl_ctrl_t
                [ H2BLK({ g_null_directorate with duid=next_duid() }, SP_rtl(ii, !m_comb_logic)) ]
        let execs_final = comb_execs @ map (rewrite_exec ww0 vd X_true mapping) (execs)

        let sons = List.foldBack (subcomponent_instance ww bobj sigma9 m_nets atts) !m_encountered []
        let final_nets = 
             let wrap net = DB_leaf(Some net, None) // In a definition, we populate the first of the pair, which is the formal.
             let db1 = { g_null_db_metainfo with pi_name="final"; (* not_to_be_trimmed=true *) form=DB_form_local } 
             gec_DB_group(db1, map wrap !m_nets)

        let locals99 = db_duplicates_trim ww (final_nets @ locals) // TODO remove this list_once
        //reportx 3 "Final locals99 (without clock(s))" netToStr locals99
        let ww = WF 1 msg ww0 "son components done"
        let formals99 = (map snd pram_pairs) @ formals
        let decls =
            let wrap net = DB_leaf(Some net, None) // In a definition, we populate the first of the pair, which is the formal.
            let db1 = { g_null_db_metainfo with not_to_be_trimmed=true; form=DB_form_external } 
            //let db2 = { g_null_db_metainfo with not_to_be_trimmed=true; form=DB_form_local }
            gec_DB_group(db1, map wrap formals99) @ locals99

        let vlnv = { vendor="HPRLS"; library="user"; kind=idl; version="1.0" }
        let iinfo = { g_null_vm2_iinfo with generated_by="bsvc"; definitionf=true; kind_vlnv=vlnv }
        vprintln 3 (sprintf "Create output VM2 definition. name=%s" (hptos idl))
        (iinfo, Some(HPR_VM2({ g_null_minfo with name=vlnv; atts=atts }, decls, sons, execs_final, []))) // end of handle_synth_target


    let manual_root_name = control_get_s sn c1 "g" (Some "")    // The -g command line synthesis root specifier to augment synthesis pragmas in the source file.
    let alternate_root_name = control_get_s sn c1 "gg" (Some "")    // The -gg command line synthesis root specifier.     

    let synth_targets =
        if alternate_root_name <> "" then // -gg overrides everything else.
            let nn = length !bobj.m_synths
            if nn > 0 then vprintln 0 (sprintf "Using command line '-gg %s' root specification and ignoring %i synthesize directives in source file(s)." manual_root_name nn)
            [ import_flat_name alternate_root_name ]
        else
            let c0 = if manual_root_name <> "" then [ import_flat_name manual_root_name ] else []
            let okg (idl, (lp, skipf)) cc = if skipf then cc else (idl, lp)::cc
            List.foldBack okg !bobj.m_synths c0


    let m_summary_report_filename = ref "no-name"
    reportx 1 "Top-level Synthesis Targets" (fun (x,lp)-> lpToStr0 lp + "  :  " + htosp x) synth_targets

    let top_overrides = [] // TODO perhaps get top parameters from command line ... or render a parametric module (Jamey asked for this).

    let rec wrk_iterate (idl, lp) (done_names, cc) =
        if memberp idl done_names then
            vprintln 1 (sprintf "Skipping synthesis target %s that has been compiled already." (hptos idl))
            (done_names, cc)
        else
            if !m_summary_report_filename = "no-name" then m_summary_report_filename := htosp idl
            establish_log false ("next_target:" + htosp idl)
            let ww = WF 1 "synthesis unit" ww (sprintf "Start target " + htosp idl)
            //dev_println (sprintf "Start target " + htosp idl)                
            let ni = handle_synth_target ww (idl, lp) (None, top_overrides)
            (idl::done_names, ni::cc)

    let (compiled_unit_names, compiled_units) = List.foldBack wrk_iterate synth_targets ([], [])

    set_opath_report_banner_and_name( 
                [  "Cmd line args: " + !g_argstring
                   timestamp(true) 
                   cbg_toy_bsv_banner()
                   "Toy Bluespec Compiler Compilation Report"
                ], !m_summary_report_filename)
       

    let ww = WF 1 msg ww0 "Writing tables start"
    let _ =
        if bobj.write_tables<>None then
            let y = yout_open_out(valOf bobj.write_tables)
            youtln y (cbg_toy_bsv_banner())
            youtln y (sprintf "Top names %s" (sfold (fun x->x) names))
            youtln y (sprintf "Compiled unit names %s" (sfold hptos compiled_unit_names))
            youtln y ( "Args= " + !g_argstring)
            youtln y ( sprintf "Compiled on %s" (timestamp true))
            youtln y ""    
            let wx (title, bl) = (youtln y title; app (youtln y) bl)
            app wx !bobj.m_tables
            youtln y (sprintf "End")        
            yout_close y
            ()

    let ww = WF 1 msg ww0 "all compiled"

    if nullp compiled_units then hpr_yikes("No modules arising from any (* synthesize *) or -g annotation.")
    compiled_units:vm_t list


let opath_bsvc_vm ww invoker sons_in =
    let ww = WF 1 "opath_bsvc_vm" ww (cbg_toy_bsv_banner()) 
    let m = invoker.banner_msg
    let argpattern = invoker.argpattern
    let c1:control_t = invoker.c3
    let names = rev(control_get_strings c1 "srcfile") // Reverse since returned in wrong order 
    let ans = 
        if length names=0 then cleanexit "No input names"
        else create_some_vms ww (c1:control_t) (names)
    ans @ sons_in

let insert_predefs e0 =
    let augment (g0 : genenv_map_t) (idl, (sty, ats, numeric_arity)) =
        vprintln 3 ("Insert builtin type " + htosp idl)
        g0.Add(idl, GE_binding(idl, {g_blank_bindinfo with whom="insert_predefs"}, EE_typef(G_none, sty))) // , numeric_arity, g_builtin_lp))
    List.fold augment e0 predef_lst
        
let install_native_forms ww () =
    let e0 = new genenv_map_t(Map.empty);
    let map0 = insert_predefs e0
    //vprintln 3 ("Installing native forms.")
    let kernels =
        [
          cambridgetech.mk_kernel_Reg ww "mkReg"  "Prelude" ()
          cambridgetech.mk_kernel_Reg ww "mkRegU" "Prelude" ()          
          cambridgetech.mk_kernel_Reg ww "mkRegA" "Prelude" ()

          cambridgetech.mk_kernel_Reg ww "mkConfigReg"  "ConfigReg" ()
          cambridgetech.mk_kernel_Reg ww "mkConfigRegU" "ConfigReg" ()         
          cambridgetech.mk_kernel_Reg ww "mkConfigRegA" "ConfigReg" ()

          cambridgetech.mk_kernel_Reg ww "mkDReg"  "DReg" ()
          cambridgetech.mk_kernel_Reg ww "mkDRegU" "DReg" ()          
          cambridgetech.mk_kernel_Reg ww "mkDRegA" "DReg" ()

          //cambridgetech.mk_kernel_Wire ww "mkPulseWireOR" "Prelude" ()
          cambridgetech.mk_kernel_Wire ww "mkPulseWire"   "Prelude" ()
          cambridgetech.mk_kernel_Wire ww "mkUnsafeRWire" "Prelude" ()
          cambridgetech.mk_kernel_Wire ww "mkRWire"       "Prelude" ()
          cambridgetech.mk_kernel_Wire ww "mkBypassWire"  "Prelude" ()
          cambridgetech.mk_kernel_Wire ww "mkWire"        "Prelude" ()
          cambridgetech.mk_kernel_Wire ww "mkUnsafeWire"  "Prelude" ()
          cambridgetech.mk_kernel_Wire ww "mkDWire"       "Prelude" ()
          cambridgetech.mk_kernel_Wire ww "mkUnsafeDWire" "Prelude" ()
          fsm_bsvc.mk_kernel_FSM ww "mkFSM"  "StmtFSM" ()           

          cambridgetech.mk_kernel_Reg ww "mkNonBlockingInputReg"  "DJGNetLevelIO" ()
          cambridgetech.mk_kernel_Reg ww "mkNonBlockingOutputReg"  "DJGNetLevelIO" ()

          cambridgetech.mk_kernel_Ram ww "mkSSRAM_Cambridge_SP0D" "BRAM" ()
          cambridgetech.mk_kernel_Ram ww "mkSSRAM_Cambridge_SP1D" "BRAM" ()
          cambridgetech.mk_kernel_Ram ww "mkSSRAM_Cambridge_DP0D" "BRAM" ()
          cambridgetech.mk_kernel_Ram ww "mkSSRAM_Cambridge_DP1D" "BRAM" ()
        ]
    app (fun (n, f, arities) -> g_kernels_builtin.add n (f, arities)) kernels
    g_top_ee := Some map0
    ()



let bsvc_used () =
    let argpattern =
        [
          Arg_int_defaulting("bsv-subexpression-cost", 10, "Steps to unwind before considering unwindable")
          Arg_int_defaulting("bsv-whilefor-limit", 1000, "Steps to unwind in global compile before considering unwindable")
          Arg_int_defaulting("bsv-elab-limit", 10000, "Steps to unwind within a module elaboration before considering unwindable")

          Arg_enum_defaulting("unspecified-to", ["x"; "0"; "1"; ], "x", "Substitute a known value rather than let don't care optimisation take place.")
// -g root modules
          Arg_enum_defaulting("bsv-scheduler", ["none"; "default"; "advanced" ], "default", "Which rule scheduler to use.");
          Arg_enum_defaulting("bsv-lextrace", ["enable"; "disable" ], "disable", "Print parser lexical tokens")
          Arg_enum_defaulting("bsv-reentrant-methods", ["enable"; "disable" ], "enable", "Allow internal method to be elaborated multiple times within a single compilation.")
          Arg_enum_defaulting("bsv-report-rule-fire-rates", ["disable"; "enable" ], "enable", "Report on relative rule firing rates (naive version).")
          Arg_enum_defaulting("bsv-respect-rule-repeat-counts", ["disable"; "enable" ], "enable", "Allow rule to fire superscalar up to repeat count annotation.")
          Arg_enum_defaulting("bsv-make-listing-of-elaborated-code", ["disable"; "enable" ], "disable", "Write post-elaboration and flattened code to a listing report file(s).")
          Arg_enum_defaulting("bsv-auto-arbiters", ["disable"; "simple"; "round-robin" ], "disable", "Instantiate stateful arbiters to avoid rule starvation.")
          Arg_enum_defaulting("bsv-idempotent-actions", ["enable"; "disable" ], "enable", "Consider action methods to be idempotent unless specifically flagger otherwise.")
          Arg_enum_defaulting("bsv-enable-multiple-scalar-writes", ["enable"; "disable" ], "disable", "Allow a register and vectors of registers (and some other state elements) to be written by more than one rule per clock cycle, with final value reflecting the composed update.")
          Arg_enum_defaulting("bsv-enable-multiple-array-writes", ["enable"; "disable" ], "disable", "Allows BRAMs to be written by more than one rule per clock cycle, with final value reflecting the composed update.")          
          Arg_enum_defaulting("bsv-print-parsetree", ["enable"; "all"; "disable"; ], "disable", "Print input parse tree.")
          Arg_enum_defaulting("bsv-split-if", ["enable"; "disable" ], "disable", "Split IF statements inside a rule into sepearate rules unless pragma'd otherwise.")
          Arg_int_defaulting("bsv-basic-loglevel", 3, "Verbosity level for log files (higher is more verbose)")
          Arg_int_defaulting("bsv-typecheck-loglevel", 3, "Verbosity level for typechecking operations (higher is more verbose)")         
          Arg_int_defaulting("bsv-sched-loglevel", 3, "Verbosity level schedulling stage in log files (higher is more verbose)")         


          Arg_defaulting("bsv-default-clock-prefix", "CLK", "Default clock name (set this to 'no_clock' to disable defaulting.")
          Arg_defaulting("bsv-default-reset-prefix", "RST_N", "Default clock name (set this to 'no_reset' to disable default resets.")

          Arg_enum_defaulting("bsv-default-clk-polarity", ["posedge"; "negedge" ], "posedge", "Which edge of the default clock to use by default.")
          Arg_enum_defaulting("bsv-default-reset-polarity", ["positive"; "negative" ], "negative", "What polarity of reset to use by default.")
          Arg_enum_defaulting("bsv-default-reset-oscity", ["synchronous"; "asynchronous" ], "asynchronous", "Whether synchronous or asynchronous resets are used by default.")
          Arg_defaulting("bsv-default-gate-prefix", "no_gate", "Clock enable to generate by default or 'no_gate' if none is wanted.")
          Arg_enum_defaulting("bsv-BRAM-native-support", ["disable"; "single-ported"; "dual-ported"], "single-ported", "Allow direct subscripted access to synchronous BRAMs.")

          Arg_enum_defaulting("bsv-first-listed-rule", ["highest"; "lowest"], "lowest", "In the absence of other ordering information, schedulling order for the first-listed rule.")
          Arg_defaulting("aggressive-conditions", "disable", "Be non-strict with implict guards on conditional expressions.")           // Required in recipe but not on the command line.

          Arg_enum_defaulting("show-type-formalpram-namehints", [ "enable"; "disable"], "disable", "Show formal names for types in debugging output - these are occasionally useful for debugging.");
          
          Arg_required("keep-fires", 0, "Retain rule fire nets for debugging.", "");           // Required in recipe but not on the command line.
          Arg_defaulting("wverbose", "disable", "Detailed/debugging expression walk logging.");   
          Arg_defaulting("incdir", g_default_incdir, "Path for file searches");
          Arg_defaulting("Prelude", "Prelude", "Name(s) of the package(s) to auto include before user compilation (semicolon-separated if multiple).");
          Arg_required("srcfile", -1, "Input file name", "");
          Arg_defaulting("gg", "", "Root Bluespec module name to be compiled as roots instead of (*synthesize*) attribute mark up.");
          Arg_defaulting("g", "", "Root Bluespec module name to be compiled as roots as well as (*synthesize*) attribute mark up.");
          Arg_defaulting("compilation-stoplist", "", "Comma-separated list of module names that are roots of separate compilations but not necessarily to be compiled in the current run (see -g).");

          Arg_defaulting("o", "do", "Name of SystemC or Verilog output file");
        ]
    install_native_forms (WW_end) ()
#if NOLONGERUSED
    let insert arg = // Insert canned tree items into module symbol table.
        match arg with
           | B_maker_l(idl, ats, _, _, _, backdoors, lp) -> bobj.m_modgens.Add(idl, arg)
           //| _ -> () 
    let canned_prelude =  [ ]
    app insert canned_prelude
#endif
    install_operator ("bsvc-fe",   "Toy Bluespec Compiler", opath_bsvc_vm, [], [], argpattern)
    ()

// Interface definitions have two forms  : interface/endinterface or "interface [ty] ID = exp;"
// An interface definition contains method prototypes and subinterface declarations. It defines a type.

// An interface is declared in a module body either by defining its methods or with an inteface/endinterface construct or with an assigment like    "interface out = toGet(outQ);" where the body of toGet typically consists of "return (interface .... endinterface);"
//
// eof
