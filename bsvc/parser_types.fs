// CBG SQUANDERER : Rudimentary/Toy Bluespec Verilog Compiler: Coded in F#.
// (C) 2012 DJ Greaves, University of Cambridge. All rights reserved.

(* All rights reserved, except as licensed by LGPL below.
 * DJ Greaves, University of Cambridge, Computer Laboratory.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *)

//
// Front-end types - for the AST (parse tree) of read in Bluespec source files.
//

module parser_types

open yout
open moscow
open hprls_hdr
open linepoint_hdr


type chkpt_t = 
   {
       lp:                linepoint_t
       ty_stack_point:    string list
   }

type tk_t = linepoint_t * chkpt_t



type typeso_t =
    | SO_valueof
    | SO_type
    | SO_formal
    | SO_none of string
    | SO_numeric
    | SO_parameter
    | SO_directorate
    
type textual_uid_t = string
type abs_textual_uid_t = textual_uid_t list

type x_lifted_op_t =
    | KB_ab of x_abdiop_t
    | KB_bop of x_bdiop_t
    | KB_op of x_diop_t
    | KB_dk of string
    | KB_ast of bsv_ast_t
    | KB_unop of string

and bsv_ast_t = 
     | KB_dontcare of linepoint_t
     | KB_nestlist of bsv_ast_t list
     | KB_builtin of string
     | KB_constantAlias of string * (linepoint_t * textual_uid_t)
     | KB_returnStmt of bsv_ast_t * (linepoint_t * textual_uid_t)
     | KB_easc of bsv_ast_t * linepoint_t
     | KB_eventControl of string * bsv_ast_t * linepoint_t
     | KB_functionCall of x_lifted_op_t * bsv_ast_t list * (linepoint_t * textual_uid_t)
     | KB_intLiteral of string option * (bool * int * string) * linepoint_t
     | KB_stringLiteral of string
     | KB_bitConcat of bsv_ast_t list * linepoint_t
     | KB_actionValueBlock of string * string option * bsv_ast_t list * string option * (linepoint_t * textual_uid_t)
     | KB_caseExpr of bsv_ast_t * bsv_ast_t * linepoint_t
     | KB_case     of bsv_ast_t * (bsv_ast_t list * bsv_ast_t) list * linepoint_t     
     | KB_actionValue
     | KB_ds_time of bool
     | KB_typeNat of string
     | KB_structPattern of (string * bsv_ast_t) list * (linepoint_t * textual_uid_t)

     | KB_interfaceDecl of bsv_ast_t * bsv_ast_t list * string option * linepoint_t
     | KB_subinterfaceInst of bsv_ast_t list * bsv_ast_t * string * linepoint_t
     | KB_subinterfaceDef00 of string option * string * string * bsv_ast_t list * string option * linepoint_t
     | KB_subinterfaceDef1 of bsv_ast_t option * string * bsv_ast_t * linepoint_t


     | KB_tuplePattern of bsv_ast_t list * (linepoint_t * textual_uid_t)
     | KB_structured_id of bsv_ast_t * bsv_ast_t
     | KB_id of string * (linepoint_t * textual_uid_t)
     | KB_patternVar of string
     | KB_beginEndExpr of string option * bsv_ast_t list * bsv_ast_t * string option
     //| KB_typeIde of string * bsv_ast_t list  // now deprecated
     | KB_typeBit_lc of (int * int) option
     | KB_type_dub of bool * string * bsv_ast_typeformal_t list * linepoint_t
     | KB_type_fun of string * bsv_ast_typeformal_t list * linepoint_t
     | KB_systemFunction of string * bsv_ast_t list * (linepoint_t * textual_uid_t)
     | KB_typedefConst of string * bsv_ast_t * linepoint_t
     | KB_method_formal of bsv_ast_t * bsv_ast_t 
     | KB_tag of bsv_ast_t * string * (linepoint_t * textual_uid_t)
     | KB_letBinding of string * bsv_ast_t * string * (linepoint_t * textual_uid_t)
     | KB_dot of string
     | KB_varDeclDo of bsv_ast_t option * bsv_ast_t * bsv_ast_t  * linepoint_t
     | KB_while of bsv_ast_t * bsv_ast_t * linepoint_t
     | KB_for of bsv_ast_t * bsv_ast_t * bsv_ast_t * bsv_ast_t * linepoint_t
//   | KB_moduleApp of string * bsv_ast_t list * linepoint_t
     | KB_structSubTagged of bsv_ast_t list * string
     | KB_import of string * string option * linepoint_t
     | KB_export of string * bool  * linepoint_t
     | KB_scheduleOrder of string list * tk_t
     | KB_scheduleMatrix of string * tk_t     
     | KB_typedefSTUnion of string * bsv_ast_t list * bsv_ast_t * string list * linepoint_t
     | KB_typedefSynonym of bsv_ast_t * bsv_ast_t * linepoint_t
     | KB_proviso of string * bsv_ast_t list
     | KB_attribute of bsv_ast_t list * bsv_ast_t * linepoint_t 
     | KB_attribute_spec of bsv_ast_t * bsv_ast_t option
     | KB_typeAssertion of bsv_ast_t * bsv_ast_t * (linepoint_t * textual_uid_t)
     | KB_structExprAssociative of string * (string * bsv_ast_t) list * (linepoint_t * textual_uid_t)
     | KB_structExprPositional of string * bsv_ast_t list * (linepoint_t * textual_uid_t)
     | KB_structMember of bsv_ast_t * string
     | KB_unionMember of bsv_ast_t option * string
     | KB_unionSubStruct of bsv_ast_t list * string
     | KB_structMemberTagged of bsv_ast_t
     | KB_structMemberSubTagged of bsv_ast_t

     | KB_attr_spec of string * bsv_ast_t option
     | KB_taggedUnion     of string * (string * bsv_ast_t) list * bsv_ast_t option * (linepoint_t * textual_uid_t)
     | KB_taggedUnionExpr of string * (string * bsv_ast_t) list * bsv_ast_t option * (linepoint_t * textual_uid_t)

     | KB_interfaceExpr of bsv_ast_t * bsv_ast_t list * string option * linepoint_t
     | KB_query  of bsv_ast_t * bsv_ast_t  * bsv_ast_t * (linepoint_t * textual_uid_t)
     | KB_await of bsv_ast_t * linepoint_t
     | KB_sublang of string * bsv_ast_t list * linepoint_t
     | KB_subscripted of bool * bsv_ast_t * bsv_ast_t list * bool ref * (linepoint_t * textual_uid_t)
     | KB_block of string option * bsv_ast_t list * string option * linepoint_t
     | KB_regWrite_raw of bsv_ast_t * bsv_ast_t * (linepoint_t * textual_uid_t)
     | KB_regWrite of bsv_ast_t * bsv_ast_t * (linepoint_t * textual_uid_t)
     | KB_if of bsv_ast_t * bsv_ast_t * bsv_ast_t option * linepoint_t
     | KB_functionDef3 of bsv_ast_t * bool * bsv_ast_t list * string option * linepoint_t
     | KB_typedefEnum of (string * bsv_ast_t option) list * string * string list * linepoint_t
     | KB_rulesExpr of string option * bsv_ast_t * string option


     | KB_typeclassInstanceDef of string * bsv_ast_t list * bsv_ast_t list * bsv_ast_t list * string option * linepoint_t
     | KB_bitSelect of bsv_ast_t * bsv_ast_t * (linepoint_t * textual_uid_t) //Not generated by parser
     | KB_bitRange2 of bsv_ast_t * bsv_ast_t * bsv_ast_t * (linepoint_t * textual_uid_t)
     | KB_functionProto of bsv_ast_t * string * (bsv_ast_typeformal_t) list * bsv_ast_t list * linepoint_t
     //KB_varDeclAssign_raw of bsv_ast_t option * bsv_ast_t * bsv_ast_t option * linepoint_t
     | KB_varDeclAssign of bsv_ast_t option * bsv_ast_t * bsv_ast_t option * linepoint_t

     | KB_package of string * bsv_ast_t list * bsv_ast_t list * string option
     | KB_methodProto of bsv_ast_t list * bsv_ast_t * string * (bsv_ast_typeformal_t) list * bsv_ast_t list * linepoint_t
     | KB_typeclassDef of string * bsv_ast_typeformal_t list * bsv_ast_t list * bsv_ast_t list * string option * linepoint_t
     | KB_rule of bsv_ast_t list * string * bsv_ast_t option * bsv_ast_t list * string option * linepoint_t

     | KB_methodDef1 of bsv_ast_t option * string * (bsv_ast_t option * string) list * bsv_ast_t option * bsv_ast_t list * string option * linepoint_t
     | KB_methodDef2 of bsv_ast_t option * string * (bsv_ast_t option * string) list * bsv_ast_t option * bsv_ast_t * linepoint_t

     | KB_moduleSig of string list * string * bsv_ast_typeformal_t list option  * bsv_ast_t option * (bsv_ast_typeformal_t) list * bsv_ast_t list * linepoint_t

     | KB_moduleDef of bsv_ast_t * bsv_ast_t list * string option
     | KB_valueOf   of bsv_ast_t * (linepoint_t * textual_uid_t)

     | KB_wildcard

and bsv_ast_typeformal_t = typeso_t * bsv_ast_t option * string

let soToStr v = function
    | SO_numeric  -> "$numeric:"
    | SO_valueof  -> "$valueof:"
    | SO_formal   -> "$formal:"
    | SO_type     -> "$type:"
    | SO_parameter-> "$parameter:"
    | SO_none s when v -> "$" + s + ":"
    | SO_none s    -> ""

let rec astToStr = function
    | KB_id(id, _) -> id
    //| KB_typeFormal(so, id) -> soToStr false so + id
    | KB_type_dub(_, id, [], lp) -> id
    | KB_type_fun(id, args, lp)  -> id + "#(" + sfold asty3ToStr args + ")"
    | KB_type_dub(_, id, args, lp) -> id + "#(" + sfold asty3ToStr args + ")"
    | KB_intLiteral(None, (f, baser, s), lp) -> i2s baser + "'" + s
    | KB_intLiteral(Some fw, (f, baser, s), lp) ->  fw + i2s baser + "'" + s    
    | other -> sprintf "ast??%A" other

and asty3ToStr = function
    | (so, Some ast, fid) -> soToStr true so + "/" + astToStr ast + "/" + fid
    | (so, None, fid)     -> soToStr true so + "//" + fid    

let todub (ast_ty) = (SO_none "$todub", Some ast_ty, "")

let g_builtin_lp = LP ("*Compiler Builtin*", 0)

let g_int_ast = KB_type_dub(false, "Int", map todub [KB_typeNat "32"], g_builtin_lp)

let function_proto_name = function
    | KB_functionProto(return_type, name, formals, provisos, lp) -> name
    
let g_null_uid = "$missing_uid"

let g_schedule_operators = [ "CF"; "SB"; "SBR"; "C"; ]

let parse_schedule m lst =
    //let _ = vprintln 0 (m+ " Parsing schedule " + sfold (fun x->x) lst)
    let rec splitter sofar = function
        | [] -> cleanexit(m+ ": Malformed schedule statement  "  + sfold (fun x->x) lst)
        | "("::t
        | ")"::t -> splitter sofar t
        | h::t when memberp h g_schedule_operators -> (rev sofar, h, t)
        | h::t -> splitter (h::sofar) t
    let (l, oo, r) = splitter [] lst
    (oo, cartesian_pairs l r)

// eof
    
