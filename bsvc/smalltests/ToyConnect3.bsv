//
// DJ Greaves
//
// ToyConnect3 - another minor variation
//
package ToyConnect3;

  interface Source;
    method Bit#(12) sourcer();
  endinterface

  interface Sink;
    method Action sinker(Bit#(12) arg);
  endinterface

  module mkSource(Source);
    
    Reg#(Bit#(12)) drega <- mkReg(10);
    method Bit#(12) sourcer();
      drega <= drega + 5;
      return drega;
    endmethod
  endmodule

  module mkSink(Sink);
    method Action sinker(Bit#(12) arg);
      $display("Sink arg=%d", arg);
    endmethod
  endmodule

 
  module connectate3(Source src, Sink dest, Empty pligh_);
    rule connectate_pump;
      dest.sinker(src.sourcer());
    endrule
  endmodule

  (* synthesize *)
  module toyconnect_top();


    Source the_source  <- mkSource();
    Sink   the_sink    <- mkSink; // Leave off the unit app to show it is implied.

    // Differs from ToyConnect 1/2 in the form of the following line:
    let letb_x = connectate3(the_source, the_sink);
    Empty pooh <- letb_x;

  endmodule

endpackage
