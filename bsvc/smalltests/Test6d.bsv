//
// Small Bluespec test (C) DJ Greaves 2012. Subinterfaces promoted to the top level.
// $Id: Test6d.bsv,v 1.1 2013-08-28 15:41:37 djg11 Exp $


package Test6d;


   interface Lower;
      method int peek (int raddr);
      method Action poke (int waddr, int vale);
   endinterface

   interface Upper;
      method int status();
      interface Lower wiredown;
   endinterface

  module mkLowItem(Lower);

    Reg#(int) dizzy <- mkRegU;

      method int peek (int raddr);
        return dizzy+1;
      endmethod

      method Action poke (int waddr, int vale);
            $display("Wrote abstract reg %d %d", waddr, vale);
	    dizzy <= vale;
      endmethod 

  endmodule

  //-------------------

  (* synthesize *)
  module mkTest6d(Upper);


      Lower thelower <- mkLowItem;

      method int status();
         return 32;
      endmethod

      interface wiredown = thelower; // is this valid?

  endmodule

endpackage

// eof
