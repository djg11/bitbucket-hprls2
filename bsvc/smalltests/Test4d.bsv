// Recursive structures.

package Test4d;

typedef Bit#(8) D8_t;

typedef union tagged 
{
    TreeNode Son_R;
    void EOL;
} TreePointer;

typedef struct 
{
  D8_t   datain;
  TreePointer pointer;

} TreeNode;

interface Test4d_ifc_ifc;

  method Action push(Integer when_arg, TreeNode payload_arg);

endinterface

(* synthesize *)
module test4d(Test4d_ifc_ifc outputer, Empty plugh_);

      rule increment;
        outputer.push(111, TreeNode{ pointer:(EOL), datain: 222 }); // Associative struct expression
      endrule

endmodule


endpackage
// eof
