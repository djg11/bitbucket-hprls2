// Toy bsvc compiler: smalltest test Test1b.bsv
// (C) 2015 DJ Greaves, University of Cambridge, Computer Laboratory.
//
// Higher-order functions.
//
package Test8a;


  function int pogger100(int poo10);
    return poo10 + 100;
  endfunction

  function int pogger200(int poo20);
    return poo20 + 200;
  endfunction

  function int plipper(int poosal, function int formee(int barond));
    return formee(312) + 45 + poosal;
  endfunction

  module test8abot(function int formoo(int fromage), Empty nosh);
      let v = plipper(33, pogger200);
      Reg#(int) pp <- mkReg(1000+formoo(22));
      rule genesis (True);
       	  $display ("Hello World: Test8a %h", pp);
      endrule:genesis
  endmodule

  (* synthesize *)
  module test8a_top();
    Empty thebot <- test8abot(pogger100);
  endmodule


endpackage
