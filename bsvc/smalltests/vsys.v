// $Id: vsys.v,v 1.1 2013-08-20 13:33:03 djg11 Exp $
//
// vsys.v A test wrapper for simulating very simple tests with clock and reset.
//

module SIMSYS();
   reg clk, reset;
   initial begin reset = 1; clk = 0; # 400 reset = 0; end
   always #100 clk = !clk;
   DUT dut(.CLK(clk), .RST_N(!reset));

   wire [31:0] count, vol;
   wire finished;  
   reg start; 
//   DUT dut(reset, clk, count, finished, start, vol);
   initial # 35000 $finish;

   initial begin
   $dumpfile ("vsys.vcd"); // Change filename as appropriate.
   $dumpvars(1, SIMSYS);
   $dumpvars(1, SIMSYS.dut);      
   end
endmodule
// eof
