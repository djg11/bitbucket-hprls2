// Toy bsvc compiler: smalltest test Test1t.bsv
// (C) 2012 DJ Greaves, University of Cambridge, Computer Laboratory.
//
// Test10b.bsv
//
// A very simple BRAM test. Writes one value to one location over and over again.
//

import GetPut       :: *;
import ClientServer :: *;
import BRAM         :: *;

(* synthesize *)
module mkTest10b(Empty);

   BRAM_Configure cfg = BRAM_Configure { memorySize: 256, latency: 1, outFIFODepth: 3, loadFormat: None, allowWriteResponseBypass: False };

   BRAM1Port#(Bit#(8), Bit#(32)) server <- mkBRAM1Server(cfg);
   
   rule write;
      server.portA.request.put(BRAMRequest { write: True, responseOnWrite: False, address: 8'd23, datain: 32'd27 });
   endrule

endmodule

