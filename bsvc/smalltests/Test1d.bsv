//

package Test1d;

  (* synthesize *)
  module test1();
       Reg#(Bit#(15)) doner [2] ;
       Reg#(Bit#(10)) xx <- mkReg(30) ;
       Reg#(Bit#(10)) yy <- mkReg(30) ;

       doner[0] <- mkReg(10);
       doner[1] <- mkReg(20);

       rule test_ast1 (True);
          doner[yy] <= doner[xx] + 1;
       endrule
  endmodule

endpackage

// eof
