// Small Bluespec test (C) DJ Greaves 2012.
//
// Toy bsvc smalltest test Test3.bsv
//
package Test3;

typedef Bit#(15) Myint;

  interface Wig;
     method ActionValue#(Myint) setter(Int#(20) vformal);
  endinterface


  module toggle(Wig);
       Reg#(Bit#(8)) zx <- mkReg(30) ;

       Reg#(Myint) doner [2] ;
       doner[0] <- mkReg(10);
       doner[1] <- mkReg(20);

       method ActionValue#(Myint) setter(Int#(20) vsemi);
          zx <= zx + 1;
	  $display("itis %d %d", zx, doner[zx] < doner[1-zx]);
          return doner[zx];
       endmethod
  endmodule

  (* synthesize *)
  module test_three();
       Wig wig0 <- toggle;

       Reg#(Myint) xx <- mkReg(30) ;
       Reg#(Myint) yy <- mkReg(30) ;

       rule test_ast1 (True);
          xx <= xx + 1;
	  let y <- wig0.setter(unpack(extend(xx)));
	  yy <= y;
       endrule
  endmodule

endpackage

// eof
