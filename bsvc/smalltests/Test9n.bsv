// Toy bsvc compiler: smalltest test Test9n.bsv
// (C) DJ Greaves, University of Cambridge, Computer Laboratory.
// 
// Shorthand/infix syntax for BRAM access.
// A BRAM is found in FPGA and ASIC and is a synchronous static RAM.
//
//
import BRAM :: *;

// This to be added to its own package.
interface InfixBRAM; // Note the string InfixBRAM is hardcoded inside toy BSVC.

 method Action _array_write(int address, int value);
endinterface

module mkInfixBRAM #(int aa, int bb) ();
       // Implemented internally?
       //  error("Infix BRAM not implemented in this version of toy BSV compiler.");
endmodule


package Test1a;

  (* synthesize *)
  module mkTest9n();

 typedef Bit#(16) BRAMData_t;
 SSRAM_Cambridge_SP1D#(Bit#(10), BRAMData_t) pas <- mkSSRAM_Cambridge_SP1D(); // mkInfixBRAM(10, 10)

       Reg#(Bit#(1)) toggle <- mkReg(30) ;
       Reg#(Bit#(10)) xx <- mkReg(30) ;
       Reg#(Bit#(10)) yy <- mkReg(25) ;

       Reg#(BRAMData_t) rval <- mkReg(1) ;       

       rule toggler;
          toggle <= !toggle;
       endrule

       rule test9n_writer (toggle);
          xx <= xx + 1;
	  pas.[xx] <= xx + 10;
	  $display ("Writer rule %d Test1a", xx);
       endrule

      rule test9n_reader (!toggle);
          yy <= yy + 1;
//	  let vv = pas.[yy]; // This should not compile...
//	  $display ("Reader rule %d returns %d", yy, vv);
	  rval <= pas.[yy];
          $display ("Reader rule %d returned last time %d", yy, rval);	       
endrule
  endmodule

endpackage

// eof
