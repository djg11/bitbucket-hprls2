

// ================================================================
// Superscalar processor: DJ Greaves, University of Cambridge, UK.
// Compatible with small example suite: Example 9a
// Original file: Copyright 2008 Bluespec, Inc.  All rights reserved.
// ================================================================

package SuperScalar;

// Reservation station status:
typedef enum  { Idle, Busy, Ready } StationStatus_t deriving (Bits, Eq);

// ----------------------------------------------------------------
// A small instruction set


// ---- Names of the four registers in the register file
typedef enum {R0, R1, R2, R3} RegNum
        deriving (Bits);

Integer regFileSize = 2 ** valueof (SizeOf#(RegNum));

// ---- Instruction addresses for a 32-location instruction memory
typedef 6 InstructionAddressWidth;
typedef UInt#(InstructionAddressWidth) InstructionAddress;
Integer imemSize = 16; // exp (2, valueof(InstructionAddressWidth));


typedef UInt#(5) StepTag_t;

// ---- Values stored in registers
typedef Bit#(32)  Value;

// ---- Instructions
typedef union tagged {
   struct { RegNum rd; Value v; }                  MovI;    // Move Immediate
   InstructionAddress                              Br;      // Branch Unconditionally
   struct { RegNum rs; InstructionAddress dest; }  Brz;     // Branch if zero
   struct { RegNum rd; RegNum rs1; RegNum rs2; }   Gt;      // rd <= (rs1 > rs2)
   struct { RegNum rd; RegNum rs1; RegNum rs2; }   Minus;   // rd <= (rs1 - rs2)
   RegNum                                          Output;
   void                                            Halt;
} Instruction
  deriving (Bits);

// ----------------------------------------------------------------
// The processor model

typedef struct { RegNum rd; Value vale; } ResResult;

typedef enum { OpLoad, OpSubtract, OpGt } ResCmd deriving (Bits, Eq);

interface AluStation;
   method Action setup(StepTag_t tag, ResCmd cmd, RegNum rd, Value a, Value b);
   method Bool idle();
   method Bool ready();
   method ActionValue#(ResResult) unload();
   method Value answer();
endinterface


module mkAluStation (AluStation);
   Reg#(StationStatus_t) logged <- mkReg(Idle);
   Reg#(RegNum) dest <- mkRegU;
   Reg#(Value) op0 <- mkRegU;
   Reg#(ResCmd) cmd <- mkRegU;
   Reg#(Value) op1 <- mkRegU;
   Reg#(Value) result <- mkRegU;
   Reg#(StepTag_t) tag <- mkRegU;


   rule alu0 (logged == Busy);
      logged <= Ready;
      result <=     
            (cmd == OpSubtract) ? op0 - op1:
	    (cmd == OpGt) ? (op0 > op1 ? 1:0):
            op0;
   endrule

   method Bool ready();
    return logged==Ready;
   endmethod

   method Bool idle();
     return logged==Idle;
   endmethod

   method Value answer() if (logged == Ready);
      return result;
   endmethod

   method Action setup(StepTag_t t, ResCmd c, RegNum rd, Value a, Value b) if (logged==Idle);
     cmd <= c;
     tag <= t;
     dest <= rd;
     op0 <= a;
     op1 <= b;
     logged <= Busy;
     $display("Setup ALU c=%0d rd=%0d", c, rd);
   endmethod

   method ActionValue#(ResResult) unload();
      logged <= Idle;
      return ResResult { rd: dest, vale: result };
   endmethod
endmodule : mkAluStation

interface SimpleProcessor;
   method Action loadInstruction (InstructionAddress ia, Instruction instr);
   method Action start ();    // Begin instruction execution at pc 0
   method Bool   halted ();
endinterface


typedef enum { Arch, Alu0, Alu1 } Where_t deriving (Bits, Eq);

(* synthesize *)
module mkSuperScalar (SimpleProcessor);


   AluStation alu0 <- mkAluStation();
   AluStation alu1 <- mkAluStation();
   // ---- Instruction memory (modeled here using an array of registers)
   Reg#(Instruction) imem[imemSize];
   for (Integer j = 0; j < imemSize; j = j + 1)
      imem [j] <- mkRegU;


   Reg#(InstructionAddress) fpc <- mkReg (0);    // The fetching program counter

   // ---- The register file (modeled here using an array of registers)
   Reg#(Value) regs[regFileSize];         // The register file
   Reg#(Where_t) scoreBoard[regFileSize];         // The register file
   for (Integer j = 0; j < regFileSize; j = j + 1) begin
      scoreBoard[j] <- mkReg(Arch);
      regs [j] <- mkRegU;
      end

   // ---- Status
   Reg#(Bool) running <- mkReg (False);
   Reg#(UInt#(32)) cycle <- mkReg (0);

   Reg#(StepTag_t) steptag <- mkReg (0);
 

   // ----------------
   // Functions

   Reg#(InstructionAddress) bdest <- mkReg (0);
   Reg#(Bool) blogged <- mkReg (False);
   function Action jmp(InstructionAddress dest);
        action
         bdest <= dest;
         blogged <= True;
        endaction
   endfunction

   // ----------------
   // RULES


   function Value regsRead(RegNum r);
       // Want a split here - condexpression does not?
      return scoreBoard[pack(r)] == Alu0 ? alu0.answer() :
             scoreBoard[pack(r)] == Alu1 ? alu1.answer() : 
             regs[pack(r)];
   endfunction

   function Bool room();
      return (alu0.idle() || alu1.idle());
   endfunction

   function Action farm(StepTag_t t, ResCmd c, RegNum rd, Value a, Value b);
      action
//      (*split*)
      if (alu0.idle()) begin
           alu0.setup(t, c, rd, a, b);
           scoreBoard[pack(rd)] <= Alu0;
           end
      else begin
           alu1.setup(t, c, rd, a, b);
           scoreBoard[pack(rd)] <= Alu1;

           end
      endaction
   endfunction 

   function Action dispatch(Instruction instr);
    action
      $display("Cycle=%i Pc %h Instr=%h Steptag=%d", cycle, fpc, instr, steptag);
      case (instr) matches
         tagged MovI  { rd: .rd, v: .v }             : begin
							  farm(steptag, OpLoad, rd, v, 0);
                                                       end

         tagged Br    .d                             : jmp(d);

         tagged Brz   { rs: .rs, dest: .d }          : begin
	 	      	    	       	  	       Bool b = regsRead(rs) == 0;
						        $display("Brz r%d ==0 r%d  %d  ", rs, regs[pack(rs)], b);
                                                        if (b) jmp(d);
                                                       end

         tagged Gt    { rd:.rd, rs1:.rs1, rs2:.rs2 } : begin
							  //  $display("Gt ins r%0d > r%0d   %0d(%0d) %0d(%0d) res=%0d(%0d)", rs1, rs2, regs[rs1], regsRead(pack(rs1)), regs[rs2], regsRead(pack(rs2)), regs[rs1] > regs[rs2], b);
							  farm(steptag, OpGt, rd, regsRead(rs1), regsRead(rs2));
                                                       end

         tagged Minus { rd:.rd, rs1:.rs1, rs2:.rs2 } : begin
							  farm(steptag, OpSubtract, rd, regsRead(rs1), regsRead(rs2));
                                                       end


         tagged Output .rs                           : begin
                                                          $display ("%0d: output regs[%d] = %0d", cycle, rs, regsRead(rs));
                                                       end
         tagged Halt                                 : begin
                                                          $display ("%t: %0d: Halt at pc", $time, cycle, fpc);
                                                          running <= False;
                                                       end
         default: begin
                     $display ("%0d: Illegal instruction at pc %0d: %0h", cycle, fpc, instr);
                     running <= False;
                  end
      endcase
     endaction
   endfunction: dispatch


   rule inc_cycle;
      cycle <= cycle + 1;
   endrule


   rule fetchAndExecute (running);
      let instr = imem [fpc];

      if (blogged) begin
           fpc <= bdest;
           blogged <= False;
           end
      else fpc <= fpc + 1;

      steptag <= steptag + 1;
      dispatch(instr);

   endrule

   rule retirer0 (alu0.ready());
      match tagged ResResult { rd: .rd1, vale: .vale1 } <- alu0.unload();
      $display("Retired 0 r=%0d vale=%d", rd1, vale1);
      regs[pack(rd1)] <= vale1;
      scoreBoard[pack(rd1)] <= Arch;
  endrule

   rule retirer1 (alu1.ready());
      $display("Retire");
      match tagged ResResult { rd: .rd1, vale: .vale1 } <- alu1.unload();
      $display("Retired 1 r=%h vale=%d", rd1, vale1);
      regs[pack(rd1)] <= vale1;
      scoreBoard[pack(rd1)] <= Arch;

   endrule:retirer1

   // ----------------
   // METHODS

   method Action loadInstruction (InstructionAddress ia, Instruction instr) if (! running);
      imem [ia] <= instr;
   endmethod

   method Action start ();
      cycle <= 0;
      fpc<= 0;
      running <= True;
   endmethod

   method Bool halted ();
      return (! running);
   endmethod

endmodule: mkSuperScalar

endpackage: SuperScalar

// eof
