//
// Higher-order functions - polymorphic HOF.
//
package Test8c;

  function int hofmeister(hofmeisterarg_type22 poo, hofmeisterarg_type33 pong);

    //let strassen = warning("Beware the ides of March: ", 202);
    let mench    = fromInteger(valueOf(SizeOf#(hofmeisterarg_type22)));
      return mench; // + strassen;
  endfunction

  function int plipper(int poosal, function int formee(int a1, String a2), int poshal);
    return formee(poshal, "hello");
  endfunction

  module test8c_bot(int arg, Empty nosh);

      let vogle1 = plipper(133, hofmeister, 0);
      Reg#(int) pp1 <- mkReg(10001+vogle1);

      let vogle2 = plipper(233, hofmeister, 12345678);
      Reg#(int) pp2 <- mkReg(10001+vogle2);

      rule genesis (True);
       	  $display ("Hello World: Test8c pp1=%h pp2=%h", pp1, pp2);
      endrule:genesis

  endmodule



  (* synthesize *)
  module test8c_top();
    Empty thebot <- test8c_bot(100);

  endmodule


endpackage
