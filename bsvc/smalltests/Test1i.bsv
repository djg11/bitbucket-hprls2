//
//
// Small Bluespec test (C) DJ Greaves 2012.
//
// Can a method be invoked more than once per clock cycle?
// Two rules that invoke the same method in one clock cycle, thus demonstrating a method being invoked more than once per clock cycle.
//
// If the mkBarTender were in a different compilation unit from the driving rules, only one rule could fire at a time since only
// one set of nets are provided in the net-level implementation of the interface. But within a single compilation, this can be
// relaxed with as many elaborations of the methods being made as needed.
//
//  The difference between Test1g and Test1h is that 1h uses two compilation units.
//
//  Test1i is different again, since a run-time arbiter is needed to avoid total hogging by one rule or the other.
//  Compile with -bsv-round-robin=enable

package Test1i;


//
//
interface BarFace;
  method Action orderDrink(int which, int no);
endinterface


(* synthesize *)
module mkBarTender(BarFace);

       Reg#(Bit#(10)) beerdrink <- mkReg(20);
       Reg#(Bit#(10)) winedrink <- mkReg(10);       

       method Action orderDrink(int which, int no);
           if (which == 1) beerdrink <= beerdrink + no;
           if (which == 2) winedrink <= winedrink + no;	   
       endmethod

       rule shower if (True);
          $display("Beer is %1d and wine is %1d", beerdrink, winedrink);
       endrule

endmodule


(* synthesize *)
module mkTest1iBench();
       BarFace fbar <- mkBarTender();

       rule drinkBeer;
         fbar.orderDrink(1, 2);  // Beer should increase by two every clock cycle.
       endrule

       rule drinkWine if (True);
       	  fbar.orderDrink(2, 10); // This rule will be starved in the absence of a stateful arbiter  -bsv-round-robin=enable
       endrule

// This needs inserting for now other wise silly missing clock error from parent.
       Reg#(Bit#(2)) timer <- mkReg(0);       
       rule useClock if (True);
          timer <= timer + 1;
       endrule
endmodule

endpackage

// eof
