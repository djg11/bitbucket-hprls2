// Toy bsvc compiler: smalltest test Test1b.bsv
// (C) 2013 DJ Greaves, University of Cambridge, Computer Laboratory.
//
// $Id: Test1a.bsv,v 1.2 2013-08-23 13:13:02 djg11 Exp $
//
// A simple counter with printing.
//
package Test1a;

  (* synthesize *)
  module test1a();
       Reg#(Bit#(10)) xx <- mkReg(30) ;

       rule test_ast1 (True);
          xx <= xx + 1;
	  $display ("%d Test1a", xx);
       endrule
  endmodule

endpackage

// eof
