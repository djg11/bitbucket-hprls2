package Test4a;

// Local function calls:  4 with no tyvars
//                        4a with tyvars
//                        4b Two maybe ports of different parameter types.



(* synthesize *)
module test4a(Empty);

  Reg#(Bit#(16)) ros <- mkReg(0);
  Reg#(Bit#(16)) ris <- mkReg(0);

  function Action togit(Reg#(Bit#(16)) arg_uno, Bit#(16) incvale);
     action arg_uno <= arg_uno + incvale;   endaction
  endfunction : togit

  rule increment;
     $display("Regs are %h %h", ros, ris);

     togit(asReg(ros), 1);
     togit(asReg(ris), 2);

  endrule

endmodule


endpackage
// eof
