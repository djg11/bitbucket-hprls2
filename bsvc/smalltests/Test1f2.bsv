// Toy bsvc compiler: smalltest test Test1b.bsv
// (C) 2015 DJ Greaves, University of Cambridge, Computer Laboratory.
//
//  Multiple updates to a register.
//

(* synthesise  *)
module mkTest1f2();

       Reg#(Bit#(10)) vodka <- mkReg(30);
       Reg#(Bool) grd <- mkReg(0);

       rule test_1f_inc1 if (True);
          vodka <= vodka + 1;
	  if (vodka > 50) $finish;
       endrule
       
       rule test_1f_inc3 if (grd);
          vodka <= vodka + 3;
       endrule

       rule shower if (True);
          grd <= !grd;
          $display("Vodka is %1d", vodka);
       endrule
endmodule


