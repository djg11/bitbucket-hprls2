//
// Two maybe ports of different parameter types at top level.
//
package Test4b; // 

    typedef Maybe#(Bit#(16)) M16;
    typedef Maybe#(Bit#(8)) M8;


    interface Subsidy#(type leafty);
      method leafty readme();
    endinterface


    typedef struct {
	 spramty vale1;
    } Substruct#(type spramty);



    interface VWidth#(type prammer2);
      interface Subsidy#(Substruct#(prammer2)) portA;
    endinterface

    (* synthesize *)
    module test4b(
	   VWidth#(Bit#(16)) sixteener, 
	   VWidth#(Bit#(8)) eighter, 
	   Empty plugh_);

//      Reg#(M16) maybe16 <- mkReg(0);
  //    Reg#(Maybe#(Bit#(8))) maybe8 <- mkReg(0);

      rule increment;
       // There is no functionality in this test!
      endrule

    endmodule


endpackage
// eof
