//
//
package Test5b;


  interface Abstract_reg#(type thedata_t);
    method thedata_t _read();
    method Action _write(thedata_t nv);
  endinterface

  module mkMyareg (adata_t iv, Abstract_reg#(adata_t) mine__)  provisos (Bits#(adata_t, sa));

    Reg#(adata_t) the_areg <- mkReg(iv) ;

    method adata_t _read();
      return the_areg;
    endmethod 

    method Action _write(adata_t nv);
      $display("Wrote 5b %d", nv);
      the_areg <= nv;
    endmethod 

  endmodule

  (* synthesize *)
  module test5m();
       Abstract_reg#(UInt#(15)) xxx <- mkMyareg(30) ;

       rule testit (True);
          xxx <= xxx + 1;
       endrule
  endmodule

endpackage

// eof
