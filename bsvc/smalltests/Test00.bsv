// Toy bsvc compiler: smalltest test Test1b.bsv
// (C) 2015 DJ Greaves, University of Cambridge, Computer Laboratory.
//
// Hello World.
//
package Test00;

  module test0abot();

  rule genesis (True);
       	  $display ("Hello World: Test00");
  endrule:genesis

  endmodule

  (* synthesize *)
  module test0a_top();

    Empty thebot <- test0abot();

  endmodule


endpackage
