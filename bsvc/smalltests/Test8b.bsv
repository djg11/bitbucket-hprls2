//
// Higher-order functions - polymorphic HOF.
//
package Test8b;

  function int hofmeister(hofmeisterarg_type22 poo, hofmeisterarg_type33 pong);

    //let strassen = warning("Beware the ides of March: ", 202);
    let mench    = fromInteger(valueOf(SizeOf#(hofmeisterarg_type22)));
      return mench; // + strassen;
  endfunction

  function int plipper(int poosal, function int formee(int a1, String a2));
    return formee(100, "hello");
  endfunction

  module test8b_bot(int arg, Empty nosh);

      let vogle1 = plipper(133, hofmeister);
      Reg#(int) pp1 <- mkReg(10001+vogle1);

      rule genesis (True);
       	  $display ("Hello World: Test8b pp1=%h", pp1);
      endrule:genesis

  endmodule



  (* synthesize *)
  module test8b_top();
    Empty thebot <- test8b_bot(100);

  endmodule


endpackage
