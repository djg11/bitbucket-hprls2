//
// Small Bluespec test (C) DJ Greaves 2012. Interfaces with generics.
// $Id: Test7a.bsv,v 1.4 2012-12-30 13:22:43 djg11 Exp $
package Test7a;


import List::*;

   function List#(Int#(14)) makeme4();
      return  cons(11, cons(20, cons(30, cons(40, tagged Nil))));
   endfunction

   function List#(Int#(14)) makeme1();
      return  cons(10, tagged Nil);
   endfunction

  (* synthesize *)
  module test7am();
       List#(Int#(14)) the_list = makeme4();

       Reg#(Int#(1)) xxx <- mkReg(0) ;
       Reg#(Int#(14)) www <- mkReg(14) ;

       rule testit (True);
          //Int#(14) the_data = the_list.Cons.tail.Cons.head;
	  //Int#(14) the_data = the_list.Cons.head;
	  //Int#(14) the_data = select(the_list, 3); 

	  // is runtime subscription supposed to work?
	  
	  Int#(14) the_data = the_list[1+xxx];
          www <= the_data;
	  $display("xxx=%d the_data=%d", xxx, the_data);
          xxx <= xxx + 1;
       endrule
  endmodule

endpackage

// eof
