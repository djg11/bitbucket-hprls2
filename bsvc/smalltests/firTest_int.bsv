//
//
// FIR Filter  Bluespec
// (C) 2018 DJ Greaves, University of Cambridge, Computer Laboratory.



// To see this generate pipelined multipliers you need to add  -recipe recipes/bsvc_restructure00


import ConfigReg::*;

interface firFilter;

  method Action setCoef(int no, int vale);  
  method ActionValue#(int) pumpData(int din);
  
endinterface


(* synthesize *)
module mkFir4_int(firFilter);

  Reg#(Int#(32)) c0 <- mkConfigRegU;  // Configuration registers can be set without creating hazards.

  Reg#(Int#(32)) c1 <- mkConfigRegU;
  Reg#(Int#(32)) c2 <- mkConfigRegU;
  Reg#(Int#(32)) c3 <- mkConfigRegU;

  Reg#(Int#(32)) dd0 <- mkReg(0);  // Data register
  Reg#(Int#(32)) dd1 <- mkReg(1);
  Reg#(Int#(32)) dd2 <- mkReg(1);
  Reg#(Int#(32)) dd3 <- mkReg(1);



  method Action setCoef(int no, int vale);

     case (no) matches
	0: c0 <= vale;
	1: c1 <= vale;
	2: c2 <= vale;
	3: c3 <= vale;
     endcase

  endmethod
    

  method ActionValue#(int) pumpData(int din);
    dd3 <= dd2;
    dd2 <= dd1;
    dd1 <= dd0;
    dd0 <= din;	    
  
    int r = 0;
     r = r + dd0*c0;
     r = r + dd1*c1;
     r = r + dd2*c2;
     r = r + dd3*c3;
    return r;
  endmethod

endmodule


(* synthesize *)
module mkFir4_TestBench(firFilter);

   firFilter dut <- mkFir4_int;

   Reg#(int) args_loaded <- mkReg(0);
   Reg#(int) data_sent   <- mkReg(0);   

   rule load_args (args_loaded < 4);
     dut.setCoef(args_loaded, 100 + args_loaded * 32);
     args_loaded <= args_loaded + 1;
   endrule

   rule load_test (args_loaded == 4);
     int r = dut.pumpData((data_sent==5) ? 10: 0);
     data_sent <= data_sent + 1;
     $display("Step %1d,  data out %1d", data_sent, r);
   endrule


endmodule


// eof