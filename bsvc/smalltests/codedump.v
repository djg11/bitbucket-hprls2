

module SIMSYS;

  wire [36 : 0] sp$loadInstruction_instr;
  reg [4 : 0] iax;

  // remaining internal signals
  reg [33 : 0] CASE_iax_8589934619_0_0_1_4294967311__q1;
  reg [1 : 0] CASE_iax_1_6_0_7_0_8_0__q4,
	      CASE_iax_2_6_1_7_2_8_3__q3,
	      CASE_iax_2_6_3_7_1__q2;

initial begin
  iax = 0;
  while (iax < 31) begin
	#10
    $display("%h %h", iax, sp$loadInstruction_instr);
    iax = iax + 1;
    end
  end

 assign sp$loadInstruction_instr =
             (iax == 5'd0 || iax == 5'd1 || iax == 5'd2) ?
               { 3'd0, CASE_iax_8589934619_0_0_1_4294967311__q1 } :
               ((iax == 5'd9 || iax == 5'd11) ?
                  37'h0555555543 :
                  ((iax == 5'd3 || iax == 5'd5) ?
                     { 30'd313174698, (iax == 5'd3) ? 7'd76 : 7'd106 } :
                     ((iax == 5'd4) ?
                        37'h0EAAAAAAB6 :
                        ((iax == 5'd6 || iax == 5'd7 || iax == 5'd8 ||
                          iax == 5'd10) ?
                           { 31'd1252698794,
                             CASE_iax_2_6_3_7_1__q2,
                             CASE_iax_2_6_1_7_2_8_3__q3,
                             CASE_iax_1_6_0_7_0_8_0__q4 } :
                           { (iax == 5'd12) ? 3'd5 : 3'd6,
                             34'h2AAAAAAA9 })))) ;



  // remaining internal signals
  always@(iax)
  begin
    case (iax)
      5'd0: CASE_iax_8589934619_0_0_1_4294967311__q1 = 34'd0;
      5'd1: CASE_iax_8589934619_0_0_1_4294967311__q1 = 34'h10000000F;
      default: CASE_iax_8589934619_0_0_1_4294967311__q1 = 34'h20000001B;
    endcase
  end
  always@(iax)
  begin
    case (iax)
      5'd6: CASE_iax_2_6_3_7_1__q2 = 2'd3;
      5'd7: CASE_iax_2_6_3_7_1__q2 = 2'd1;
      default: CASE_iax_2_6_3_7_1__q2 = 2'd2;
    endcase
  end
  always@(iax)
  begin
    case (iax)
      5'd6: CASE_iax_2_6_1_7_2_8_3__q3 = 2'd1;
      5'd7: CASE_iax_2_6_1_7_2_8_3__q3 = 2'd2;
      5'd8: CASE_iax_2_6_1_7_2_8_3__q3 = 2'd3;
      default: CASE_iax_2_6_1_7_2_8_3__q3 = 2'd2;
    endcase
  end
  always@(iax)
  begin
    case (iax)
      5'd6, 5'd7, 5'd8: CASE_iax_1_6_0_7_0_8_0__q4 = 2'd0;
      default: CASE_iax_1_6_0_7_0_8_0__q4 = 2'd1;
    endcase
  end

endmodule
