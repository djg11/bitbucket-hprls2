//
// Small Bluespec test (C) DJ Greaves 2012. Subinterfaces: A simple subinterface example.
//


package Test3b;

typedef Bit#(32) Myint;

  interface Wig;
         method ActionValue#(Int#(4)) setter(Int#(20) vformal);
  endinterface

  module toggle(Wig);
       Reg#(Int#(4)) doner [3] ;
       doner[0] <- mkReg(10);
       doner[1] <- mkReg(20);
       doner[2] <- mkReg(30);

       method ActionValue#(Int#(4)) setter(Int#(20) vsemi);
          doner[vsemi] <= doner[vsemi] + 1;
	  $display("All 3 are %d %d %d", doner[0], doner[1], doner[2]);
          return doner[2];
       endmethod
  endmodule

  (* synthesize *)
  module test_three();
       Wig wig0 <- toggle;

       Reg#(Int#(20)) xx <- mkReg(30) ;
       Reg#(Int#(16)) yy <- mkReg(30) ;

       rule test_ast1 (True);
          xx <= xx + 1;
          let y <- wig0.setter(xx & 1);
	  yy <= extend(y);
       endrule
  endmodule

endpackage

// eof
