//
// Higher-order functions - polymorphic HOF - map test with intermediate application(s).
//

package Test8g;

import List::*;

  function low_t incrementer(low_t vv);
    return warning("polyincremeter run 8g", vv);
  endfunction

  // We deliberately give unique tyvar names so as to check the compiler works out all the unifications.
  function List#(xmid_t) midder_lo(function List#(xmid_mf_range_t) mapped_lowfunc(List#(xmid_t) barbar), List#(xmid_t) lowarg);
    let newlisty = map(mapped_lowfunc, lowarg);
    return newlisty;
  endfunction


  function Tuple2#(List#(xmid_hi_range_t), String) midder_hi(List#(xmid_hi_domain_t) higharg); // 
    let newlistz = midder_lo(incrementer, higharg);
    return tuple2(newlistz, "fred");
  endfunction

  (* synthesize *)
  module test8g_top();
   let testlist = 
      cons(190,
      cons(290,
      cons(390, tagged Nil)));

  let pans = midder_hi(testlist);
  let ye_answer = tpl_1(pans);

 
  Reg#(Int#(32)) bobreg1 <- mkReg(head(ye_answer));
  Reg#(Int#(32)) bobreg2 <- mkReg(head(tail(ye_answer)));

  endmodule


endpackage
