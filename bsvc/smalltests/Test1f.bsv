//
//
// Small Bluespec test (C) DJ Greaves 2012.
//
// Two rules that could be packed in one clock cycle operate on a common register.

package Test1f;


//
(* synthesize *) module mkTest1f();
       Reg#(Bit#(10)) shandy <- mkReg(30);

       rule test_1f_inc1 if (True);
          shandy <= shandy + 1;
       endrule
       
       rule test_1f_inc3 if ((shandy % 5)==0);
          shandy <= shandy + 3;
       endrule

       rule shower if (True);
          $display("Shandy is %1d", shandy);
       endrule
endmodule


endpackage

// eof
