//
// This example was pasted from the Bluespec Reference Guide and is (C) BlueSpec.
//

// Example. Using descending_urgency to control the scheduling of conflicting rules:
//
// Rule resetCounter conflicts with rule updateCounter because both try to modify the counter register when it contains all its bits set to one.
//
// Without any descending_urgency attribute, the updateCounter rule may obtain more urgency, meaning that if the explicit guard of resetCounter is met, only the rule
// updateCounter will fire. By setting the descending_urgency attribute the designer can control the scheduling in the case of conflicting rules.


// package Test10;

// IfcCounter with read method
interface IfcCounter#(type t);
   method t readCounter();
endinterface

// Definition of CounterType
  typedef Bit#(16) CounterType;


// Module counter using IfcCounter interface. It never contains 0.
(* synthesize,
        reset_prefix = "reset_b",
        clock_prefix = "counter_clk",
        always_ready = "readCounter",
        always_enabled= "readCounter" *)
module mkTest10 (IfcCounter#(CounterType));

  // Reg counter gets reset to 1 asynchronously with the RST signal
  Reg#(CounterType) counter <- mkRegA(1);

  /* The descending_urgency attribute will indicate the scheduling order for the indicated rules. */
//  (* descending_urgency = "resetCounter, updateCounter" *)
  (* descending_urgency = "updateCounter, resetCounter" *)  


  // Next rule updates the counter.
  rule updateCounter;
    action counter <= counter + 1; endaction
    $display("updated to %1d", counter);
  endrule

// Next rule resets the counter to 1 when it reaches its limit.
  rule resetCounter (counter >= 3);
    action counter <= 1; endaction
  endrule

  
  // Method to output the counter's value
  method CounterType readCounter;
    return counter;
  endmethod

endmodule

// endpackage
// eof
