// Toy bsvc compiler: smalltest test Test1b.bsv
// (C) 2012 DJ Greaves, University of Cambridge, Computer Laboratory.
//
// Test for races/conflicts in register assigments.    These will be resolved by the scheduler.
//
//

package Test2;



  (* synthesize *)
  module test2();

       Reg#(Bit#(10)) xx <- mkReg(0) ;
       Reg#(Bit#(10)) yy <- mkReg(1) ;
       (* mutually_exclusive = "race44" *)
       rule race1 (xx<3);
	  xx <= xx + 1;
	  yy <= xx + yy;
       endrule

       rule race2 (True);
	  xx <= xx + 2;
       endrule

       rule monitor;
	  $display ("Test2 pong %d %d", xx, yy);
       endrule
  endmodule


endpackage
// eof
