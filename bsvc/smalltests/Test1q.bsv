// Toy bsvc compiler: smalltest test Test1q.bsv
// (C) 2015 DJ Greaves, University of Cambridge, Computer Laboratory.
//
// BitConcat, BitSelect, bit-extract etc.
//
package Test1q;

  (* synthesize *)

module mkBitConcatSelect ();
  Bit#(3) a = 3'b010;         //a = 010
  Bit#(7) b = 7'h5e;          //b = 1011110
  Bit#(10) abconcat = {a, b}; // = 0101011110
  Bit#(4) bselect = b[6:3];   // = 1011
  Bit#(1) dselect = b[6];     // = 1

  // We need to add a bit field insert test here please.


   Reg#(Bit#(10)) counter <- mkReg(0);
   Reg#(Bit#(10)) fields <- mkReg(abconcat);   

  rule mandy;
    counter <= counter + 1;
    if (counter > 5) $finish(0);
    $display("%1d: Currently %1h", counter, fields);
    if (counter > 2) fields[9:5] <= 5;
  endrule

endmodule


endpackage

// eof

        
