// Toy bsvc compiler: smalltest test Test12a.bsv
//
// Top-level Input and Output.
// Some/most contents of this file were copied from  the Bluespec reference manual and hence copyright resides with Bluespec Inc.
// THIS DOES NOT WORK WITH TOY COMPILER YET. TRY Test12b.bsv.
//



import CBus :: * ;

////////////////////////////////////////////////////////////////////////////////
/// basic defines
////////////////////////////////////////////////////////////////////////////////
// Width of the address bus, it's easiest to use only the width of the bits needed
// but you may have other reasons for passing more bits around (even if some address
// bits are always 0).
typedef 2 DCBusAddrWidth; // roof( log2( number_of_config_registers ) )

// The data bus width is probably defined in your spec
typedef 32 DCBusDataWidth; // how wide is the data bus

////////////////////////////////////////////////////////////////////////////////
// Define the CBus
////////////////////////////////////////////////////////////////////////////////
typedef CBus#(DCBusAddrWidth, DCBusDataWidth) DCBus;
typedef CRAddr#(DCBusAddrWidth, DCBusDataWidth) DCAddr;
typedef ModWithCBus#(DCBusAddrWidth, DCBusDataWidth, i) DModWithCBus#(type i);


// In order to access the CBus at this parent, we need to expose the bus.
// Only modules of type [Module] can be synthesized.


interface Block;
  // TODO: normally this block would have at least a few methods
  // Cbus interface is hidden, but it is there
endinterface

////////////////////////////////////////////////////////////////////////////////
/// Configuration Register Types
////////////////////////////////////////////////////////////////////////////////
// These are configuration register from your design. The basic
// idea is that you want to define types for each individual field
// and later on we specify which address and what offset bits these
// go to. This means that config register address fields can
// actually be split across modules if need be.
//
typedef bit TCfgReset;

typedef Bit#(4) TCfgInit;
typedef Bit#(6) TCfgTz;
typedef UInt#(8) TCfgCnt;

typedef bit TCfgOnes;
typedef bit TCfgError;



DCAddr cfg_reset_reset = DCAddr {a: cfgResetAddr, o: 0}; // bits 0:0


DCAddr cfg_setup_init = DCAddr {a: cfgStateAddr, o: 0}; // bits 0:0
DCAddr cfg_setup_tz = DCAddr {a: cfgStateAddr, o: 4}; // bits 9:4
DCAddr cfg_setup_cnt = DCAddr {a: cfgStateAddr, o: 16}; // bits 24:16

DCAddr cfg_status_ones = DCAddr {a: cfgStatusAddr, o: 0}; // bits 0:0
DCAddr cfg_status_error = DCAddr {a: cfgStatusAddr, o: 0}; // bits 1:1

// All registers are read/write from the local block point of view.
// Config register interface types can be:
//   mkCBRegR -> read only from config bus
//   mkCBRegRW -> read/write from config bus
//   mkCBRegW -> write only from config bus
//   mkCBRegRC -> read from config bus, write is clear mode
// i.e. for each bit a 1 means clear, 0 means don't clear
// reset bit is write only from config bus
// we presume that you use this bit to fire some local rules, etc

module /*[DModWithCBus]*/ mkBlockInternal( Block );
   Reg#(TCfgReset) reg_reset_reset <- mkCBRegW(cfg_reset_reset, 0 /* init val */);
   Reg#(TCfgInit) reg_setup_init <- mkCBRegRW(cfg_setup_init, 0 /* init val */);
   Reg#(TCfgTz) reg_setup_tz <- mkCBRegRW(cfg_setup_tz, 0 /* init val */);
   Reg#(TCfgCnt) reg_setup_cnt <- mkCBRegRW(cfg_setup_cnt, 1 /* init val */);

   Reg#(TCfgOnes) reg_status_ones <- mkCBRegRC(cfg_status_ones, 0 /* init val */);
   Reg#(TCfgError) reg_status_error <- mkCBRegRC(cfg_status_error, 0 /* init val */);

// USER: you now have registers, so do whatever it is you do with registers :)
// for instance
    rule bumpCounter ( reg_setup_cnt != unpack(20) );
       reg_setup_cnt <= reg_setup_cnt + 1;
    endrule

    rule watch4ones ( reg_setup_cnt == unpack(1) );
      reg_status_ones <= 1;
    endrule
endmodule



// official syntax is
//        module [Module] mkBlock(IWithCBus#(DCBus, Block));
// for now we leave out the Module bit

(* synthesise *)
module mkBlock(IWithCBus#(DCBus, Block));
  let ifc <- exposeCBusIFC( mkBlockInternal );
  return ifc;
endmodule

// Within this module the CBus looks like normal Registers.
// This module can't be synthesized directly.
// How these registers are combined into CBus registers is defined in the CfgDefines package.

// EOF
