package Philo;

import StmtFSM::*;

//
// Dining Philosophers Test - DJ Greaves.
//

interface Fork_if;
  method Action pickup();
  method Action putdown();
endinterface


module fork (Fork_if) ;

   Reg#(Bool) inuse <- mkReg(0);    

   method Action pickup() if (!inuse);
      action
	inuse <= True;
      endaction
   endmethod

   method Action putdown();
      action
	inuse <= False;
      endaction
   endmethod

endmodule


interface Diner_if;

endinterface

//

interface Random_if;
  method ActionValue#(UInt#(15)) gen();
endinterface    

module mkRandom_gen #(UInt#(15) seed) (Random_if);

  Reg#(UInt#(15)) prbs <- mkReg(seed);

  method ActionValue#(UInt#(15)) gen();
     prbs <= (prbs << 1) | (((prbs >> 14) ^ (prbs >> 13)) & 1);   
     return prbs;
  endmethod

endmodule 


module mkDiner #(String dname, UInt#(15) on, UInt#(15) seed) (Fork_if left, Fork_if right, Diner_if i);

  Reg#(Bool) eating <- mkReg(0);    
  Reg#(UInt#(15)) timer <- mkReg(0);

  Random_if gen <- mkRandom_gen(seed);

  rule foo (timer != 0);
     timer <= timer - 1;
  endrule


  Stmt seq_behaviour =
     (seq while (True) seq
	action 
	   timer <= gen.gen() & 31; 
	endaction 
	await(timer== 0);

	action left.pickup(); 
	       $display(dname + ": %m pickup left");
	endaction

	noAction;

	action timer<=gen.gen() & 31; endaction await(timer == 0);

	action right.pickup(); 
	       $display(dname + ": %m pickup right");
	endaction

	eating <= True;
	timer <= gen.gen() & 31; await(timer==0);
	eating <= False;

	noAction;

	action $display(dname + ": %m putdown the forks");
	       left.putdown(); endaction
	noAction;

	action right.putdown(); endaction
	noAction;
     endseq endseq);

  FSM fsm <- mkFSM (seq_behaviour);
 
  rule kickoff ;
    fsm.start();
  endrule

endmodule : mkDiner



(* synthesize *)
module philoBENCH (Empty) ;

       Fork_if fork0 <- fork; 
       Fork_if fork1 <- fork; 
       Fork_if fork2 <- fork; 
       Fork_if fork3 <- fork; 
       Fork_if fork4 <- fork; 

       Diner_if diner0 <- mkDiner ("Zeon",   3, 7, fork0, fork1);
       Diner_if diner1 <- mkDiner ("Ona",    4, 4, fork1, fork2);
       Diner_if diner2 <- mkDiner ("Trevor", 2, 9, fork3, fork2);
       Diner_if diner3 <- mkDiner ("Tina",   3, 6, fork3, fork4);
       Diner_if diner4 <- mkDiner ("Frank",  3, 6, fork4, fork0);


endmodule: philoBENCH

endpackage
// eof
