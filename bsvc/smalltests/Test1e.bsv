// Toy bsvc compiler: smalltest test Test1b.bsv
// (C) 2012 DJ Greaves, University of Cambridge, Computer Laboratory.
//
//
// Vectors/arrays of interfaces.
//

package Test1e;

import RegFile::*;


  (* synthesize *)
  module test1();
       RegFile#(Bit#(4), Bit#(32)) regFile <- mkRegFile(0, 15);


       Reg#(Bit#(15)) doner [2] ;
       Reg#(Bit#(10)) xx <- mkReg(30) ;
       Reg#(Bit#(10)) yy <- mkReg(30) ;

       doner[0] <- mkReg(10);
       doner[1] <- mkReg(20);

       rule test_ast1 if (True);
          doner[yy] <= doner[xx] + 1;
       endrule

  endmodule

endpackage

// eof
