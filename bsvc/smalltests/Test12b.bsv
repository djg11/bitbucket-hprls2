// Toy bsvc compiler: smalltest test Test12a.bsv
//
// Top-level Input and Output.
// Some/most contents of this file were copied from  the Bluespec reference manual and hence copyright resides with Bluespec Inc.
// THIS DOES NOT WORK WITH TOY COMPILER YET. TRY Test12b.bsv.
//



import DJGNetLevelIO :: * ;

////////////////////////////////////////////////////////////////////////////////

package Test12b;

  (* synthesize *)
  module test12b_top();  

    // Test both forms of do : neither, of course, denotes Empty, in this situation. It is instead further syntactic jiggery where the
    // last argument on the rhs is stripped off and used as the lhs of the do.
    Reg#(Bit#(14)) ureg_with_paren_fourteen <- mkRegU();

    Reg#(Bit#(12)) output_port <- mkNonBlockingOutputReg(10);
    DJGNonblockingInputReg#(Bit#(12)) input_amount <- mkNonBlockingInputReg;

    rule copyout;
      output_port <= ureg_with_paren_fourteen;
      ureg_with_paren_fourteen <= ureg_with_paren_fourteen + 2 + input_amount;
    endrule





  endmodule

endpackage

// EOF
