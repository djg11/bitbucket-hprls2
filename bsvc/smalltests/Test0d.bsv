// Toy bsvc compiler: smalltest test Test1b.bsv
// (C) 2019 DJ Greaves, University of Cambridge, Computer Laboratory.
//
// Hello World with pre-processor.
//
package Test0d;

`define testmsg "Now is the knowing!"

module test0abot();

  rule genesis (True);
`ifdef PMAC1
     $display ("Hello World: Test0d alpha");
`else
     $display ("Hello World: Test0d beta");
`endif
  endrule:genesis

  endmodule

  (* synthesize *)
  module test0a_top();

    Empty thebot <- test0abot();

  endmodule


endpackage
