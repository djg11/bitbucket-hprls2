//
// Toy bsvc compiler: smalltest test Test0.bsv
// 
package Test0;

  (* synthesize *)
  module test0(Empty);

  rule genesis_test0 (True);
       	  $display ("Hello World");
  endrule:genesis_test0

  endmodule

endpackage
