// Positional and associative parameter passing

package Test4c;

typedef Bit#(8) D8_t;

typedef struct 
{
   Bool   writeFlag;
   D8_t   datain;
} Payload;

interface Tester;

  method Action push(Integer when_arg, Payload payload_arg);

endinterface

(* synthesize *)
module test4c(Tester tester, Empty plugh_);


      rule increment;
        tester.push(111, Payload{ writeFlag:True, datain: 222 }); // Associative struct expression
      endrule

    endmodule


endpackage
// eof
