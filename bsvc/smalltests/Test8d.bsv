// Toy bsvc compiler: smalltest test Test1b.bsv
// (C) 2015 DJ Greaves, University of Cambridge, Computer Laboratory.
//
// Higher-order functions - polymorphic HOF - map test.
//

package Test8d;

import List::*;

  function int incrementer(int v);
    return v + 1;
  endfunction

  (* synthesize *)
  module test8d_top();


   let testlist = 
      cons(190,
      cons(290,
      cons(390, tagged Nil)));

  //  let answer = warning("The answer is ", map(incrementer, testlist));
  let answer = map(incrementer, testlist);

  Reg#(Int#(32)) bobreg <- mkReg(head(tail(answer)));
  endmodule


endpackage
