// Toy bsvc compiler: smalltest test Test1b.bsv
// (C) 2015 DJ Greaves, University of Cambridge, Computer Laboratory.
//
// Uses bsv Clocks and has two clock domains. 
//
package Test1b;

  import Clocks :: *;

  // djg do we also need an Import of Resets :: *; 

  // Test1b has two clock domains -- the "current" one corresponding to the clock domain in which test1b is instantiated and the testClock one.

  // This test should in future have a compile-time error (as with Bluespec's compiler) because the we assume that testClock is different from current clock and rule test_ast1 reads both registers.

  // Jamey: I think that given the usage it will assume that dongleReset is in the current clock domain, but I'm not sure. No clocks are implied by a reset declaration, but it does check which clocks were used to generate the reset.

  // You can change the prefix of the port names used when generating verilog with clock_prefix= gate_prefix= and reset_prefix= attributes in Bluespec's compiler. You can also tell it not to include a default clock, in which case every module instantiated has to have explicit clocked_by and reset_by.


  (* synthesize *)
  module test1b#(Clock testClock, Reset dongleReset)(Empty);
       Reg#(Bit#(15)) doner [2] ;
       Reg#(Bit#(10)) xx <- mkReg(30, clocked_by testClock);
       Reg#(Bit#(10)) yy <- mkReg(11, reset_by dongleReset);

       doner[0] <- mkReg(10);
       doner[1] <- mkReg(20);

       rule test_ast1 (True);
          doner[yy] <= doner[xx] + 1;
       endrule
  endmodule

endpackage

// eof
