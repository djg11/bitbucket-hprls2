//  Satnam's Simple Example
//  This test has type expressions in parenthesis and uses a type assertion.
//
import Vector::*;

import BuildVector::*;

import RegFile::*;

interface Module1;
  method ActionValue#(Bit#(4)) count_value ();
endinterface

module mkModule1 (Module1);
    Reg#(Bit#(4)) counterReg <- mkReg(4'h0);
    
    rule incrementAndOutput;
        let x_0 = (counterReg);
        counterReg <= (x_0) + ((Bit#(4))'(4'h1)); 
    endrule
    
    
    method ActionValue#(Bit#(4)) count_value ();
        let x_1 = (counterReg);
        return x_1;
    endmethod
    
endmodule

module mkTop (Empty);Module1 m1 <- mkModule1 ();
                     
endmodule


/*

We expect:

module mkModule1(CLK,
                 RST_N,
                 EN_count_value,
                 count_value,
                 RDY_count_value);
  input  CLK;
  input  RST_N;

  // actionvalue method count_value
  input  EN_count_value;
  output [3 : 0] count_value;
  output RDY_count_value;

as produced by bsc.
*/