
package Test11;

typedef enum {
   Tag1, Tag2, Tag3 
   } Enum1;

typedef enum {
   Tag1, Tag2, Tag3 
   } Enum2;

typedef union tagged {
   Bit#(1) Tag1;
   Bit#(1) Tag2;
   } Union1;

module mkTest11(Empty);
   //FIXME: Disambiguation of two or more tagged unions with tag 'Tag1' not currently supported
   Union1 enum1 = tagged Tag1 0;
   //FIXME: Muddy bog: a part of the compiler tool is missing: other ctor tag form ...
   Enum2 enum2 = Tag2;
endmodule

endpackage
