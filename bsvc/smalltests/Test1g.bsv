//
//
// Small Bluespec test (C) DJ Greaves 2018.
//
// Can a method be invoked more than once per clock cycle?
// Two rules that invoke the same method in one clock cycle, thus demonstrating an ephemeral method being invoked more than once per clock cycle.
//
// If the mkBarTender were in a different compilation unit from the driving rules, only one rule could fire at a time since only
// one set of nets are provided in the net-level implementation of the interface. But within a single compilation, this can be
// relaxed with as many elaborations of the methods being made as needed.
//
//  The difference between Test1g and Test1h is that 1h uses two compilation units.  Hence 1g uses ephemeral methods.
//

package Test1g;

//
//
interface BarFace;
  method Action orderDrink(int which, int no);
endinterface


module mkBarTender(BarFace);

       Reg#(Bit#(10)) beerdrink <- mkReg(20);
       Reg#(Bit#(10)) winedrink <- mkReg(10);       

       method Action orderDrink(int which, int no);
           if (which == 1) beerdrink <= beerdrink + no;
           if (which == 2) winedrink <= winedrink + no;	   
       endmethod

       rule shower if (True);
          $display("Test1g: Beer is %1d and wine is %1d", beerdrink, winedrink);
       endrule

endmodule


(* synthesize *)
module mkTest1gBench();

       Reg#(Bit#(2)) timer <- mkReg(0);       

       BarFace bar <- mkBarTender();

       rule drinkBeer;
          bar.orderDrink(1, 2);  // Beer should increase by two every clock cycle.
       endrule

       rule drinkWine if (True);
          timer <= timer + 1;
	  if (timer == 3) bar.orderDrink(2, 10); // Wine should increase by ten every fourth clock cycle.
       endrule

endmodule

endpackage

// eof
