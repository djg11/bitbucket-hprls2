//
// $Id: Test6c.bsv,v 1.4 2013-08-23 13:13:02 djg11 Exp $
// Small Bluespec test (C) DJ Greaves 2012.
// Tests subinterfaces, user-defined _write method and interface assignment (again?)
//

package Test6c;

   interface Left#(type leftdata_t);
      method leftdata_t leftread();
   endinterface

  interface Abstract_reg#(type absdata_t);
    interface Left#(absdata_t) gauche;
    method Action _write(absdata_t nv);   
  endinterface

  // my abstract register
  module mkMyareg#(adata_t iv) (Abstract_reg#(adata_t))  provisos (Bits#(adata_t, adata_sz));

    Reg#(adata_t) the_areg <- mkReg(iv) ;

    method Action _write(adata_t nv);
      $display("Test6c: Wrote abstract reg  %d", nv);
      the_areg <= nv;
    endmethod 

   interface Test6c::Left gauche;
      method adata_t leftread();
        return the_areg;
      endmethod 
   endinterface

  endmodule

//-------------------

  interface Meta_reg#(type metadata_t);
    interface Left#(metadata_t) trotski;
    method Action _write(metadata_t nv);
  endinterface

  module mkMetareg#(motadata_t iv) (Meta_reg#(motadata_t))  provisos (Bits#(motadata_t, motadata_sz));

    Abstract_reg#(motadata_t) the_myareg <- mkMyareg(iv) ;

    method Action _write(motadata_t nv);
      $display("Test6c: Wrote metareg %d", nv);
      the_myareg <= nv;
    endmethod 

   interface trotski = the_myareg.gauche;  // Assignment under test, pass it up.

  endmodule


  (* synthesize *)
  module test6m();
       Meta_reg#(UInt#(15)) xxx <- mkMetareg(30) ;

       rule testit (True);
          xxx <= xxx.trotski.leftread() + 1;
       endrule
  endmodule

endpackage

// eof
