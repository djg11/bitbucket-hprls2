// Toy bsvc compiler: smalltest test Test1b.bsv
// (C) 2015 DJ Greaves, University of Cambridge, Computer Laboratory.
//
//
// Higher-order functions.
//
package Test8x;

  function int pogged200(int poo20);
    return poo20 + 200;
  endfunction

  function int togged400(int too40);
    return too40 + 400;
  endfunction

  function int plipper(int poosal, function int formee(int barond));
    return formee(312) + 45 + poosal;
  endfunction

  function int midder(int ginny, function int midnamer(int posit));
    return plipper(ginny+2, midnamer) + 10192;
  endfunction

  function int test8xbot(int otherarg);
      let v1 = midder(33+otherarg, pogged200);
      let v2 = midder(33+otherarg, togged400);
      return v1 + v2 + 2;
  endfunction

  (* synthesize *)
  module test8x_top();
      let v = test8xbot(3231);

      Reg#(int) pp <- mkReg(1000+v);

      rule genesis (True);
       	  $display ("Hello World: Test8a %h", pp);
      endrule:genesis

  endmodule


endpackage
