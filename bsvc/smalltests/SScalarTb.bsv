// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 9a
// Simple Processor model, illustrating the use of enums, tagged unions,
// structs, arrays of registers, and pattern-matching.
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package SScalarTb;

//import SimpleProcessor::*;
import SuperScalar::*;

// ----------------------------------------------------------------
// The following is a program for the Simple Processor
// that computes the GCD (Greatest Common Divisor) of two numbers x and y
// that are initially loaded into registers R1 and R2.
// The program encodes Euclid's algorithm:
//     while (y != 0) if (x <= y) y = y - x else swap (x <=> y)
// In this example, x = 15 and y = 27, so the output should be 3

InstructionAddress label_loop     = 3;
InstructionAddress label_subtract = 10;
InstructionAddress label_done     = 12;
InstructionAddress label_last     = 13;

Instruction code [14] =
   {
     tagged MovI { rd: R0, v: 0 },                // 0: The constant 0
     tagged MovI { rd: R1, v: 15 },               // 1: x = 21
     tagged MovI { rd: R2, v: 27 },               // 2: y = 27
     // label_loop
     tagged Brz { rs: R2, dest:label_done },      // 3: if (y == 0) goto done
     tagged Gt  { rd: R3, rs1: R1, rs2: R2 },     // 4: tmp = (x > y)
     tagged Brz { rs: R3, dest: label_subtract }, // 5: if (x <= y) goto subtract
     // swap
     tagged Minus { rd: R3, rs1: R1, rs2: R0 },   // 6: tmp = x;
     tagged Minus { rd: R1, rs1: R2, rs2: R0 },   // 7: x = y;
     tagged Minus { rd: R2, rs1: R3, rs2: R0 },   // 8: y = tmp;
     tagged Br  label_loop,                       // 9: goto loop
     // label_subtract
     tagged Minus { rd: R2, rs1: R2, rs2: R1 },   // 10: y = y - x
     tagged Br  label_loop,                       // 11: goto loop
     // label_done
     tagged Output R1,                            // 12: output x
     // label_last
     tagged Halt                                  // 13: halt
   };

// ----------------------------------------------------------------
// The testbench

(* synthesize *)
module mkTb (Empty);

   Reg#(InstructionAddress) iax <- mkReg (0);
   SimpleProcessor sp <- mkSuperScalar ();

   // Iterate through the instructions array, loading the program into
   // into the processor's instruction memory
   rule loadInstrs (iax <= label_last);
      sp.loadInstruction (iax, code [iax]);
      iax <= iax + 1;
   endrule

   // Start the processor executing its program
   rule go (iax == label_last + 1);
      sp.start();
      iax <= iax + 1;
   endrule

   // Wait till the processor halts, and quit
   rule windup ((iax > label_last + 1) && (sp.halted));
      $display ("Fyi: size of an Instruction is %0d bits", valueof (SizeOf#(Instruction)));
      $finish (0);
   endrule

endmodule: mkTb

endpackage: SScalarTb

