// Toy bsvc compiler: smalltest test Test1b.bsv
// (C) 2012 DJ Greaves, University of Cambridge, Computer Laboratory.
//
// Higher-order functions - polymorphic HOF - map test with intermediate application(s).
//

package Test8e;

import List::*;

  function low_t incrementer(low_t vv);
    return warning("polyincremeter run", vv);
  endfunction

  function Tuple2#(List#(xmid_t), String) midder(List#(xmid_t) xmidarg); // Already explict that domain and range tyvars are common!
    let newlisty = map(incrementer, xmidarg);
    return tuple2(newlisty, "fred");
  endfunction

  (* synthesize *)
  module test8e_top();
   let testlist = 
      cons(190,
      cons(290,
      cons(390, tagged Nil)));

  let pans = midder(testlist);
  let ye_answer = tpl_1(pans);

 
  Reg#(Int#(32)) bobreg1 <- mkReg(head(ye_answer));
  Reg#(Int#(32)) bobreg2 <- mkReg(head(tail(ye_answer)));

  endmodule


endpackage
