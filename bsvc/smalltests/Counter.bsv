
package Counter;

(*synthesize *)
module counter (Empty);

  Reg#(Bit#(32)) current <- mkReg(0);

  rule inc;
     current <= current + 1;
     $display("The current is %h", current);
  endrule


endmodule


endpackage