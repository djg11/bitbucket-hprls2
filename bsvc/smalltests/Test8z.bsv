//
// Higher-order functions.  8z - simple
//
package Test8z;

  function int pogged200(int poo20);
    return poo20 + 200;
  endfunction

  function int plipper(int poosal, function int formee(int barond));
    return formee(312) + 45 + poosal;
  endfunction

  function int test8zbot(int otherarg);
      let v = plipper(33+otherarg, pogged200);
      return v + 2;
  endfunction

  (* synthesize *)
  module test8z_top();
      let v = test8zbot(3231);

      Reg#(int) pp <- mkReg(1000+v);

      rule genesis (True);
       	  $display ("Hello World: Test8z %h", pp);
      endrule:genesis

  endmodule


endpackage
