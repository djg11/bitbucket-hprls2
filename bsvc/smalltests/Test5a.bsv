//
// Small Bluespec test (C) DJ Greaves 2012. Recursive abstract type test: the List.
//
// $Id: Test5a.bsv,v 1.1 2013-06-21 15:57:47 djg11 Exp $
//
package Test5a;

import List::*;

typedef List#(int) Mylist;

  (* synthesize *)
  module test5a();


  endmodule

endpackage

// eof
