// The deepthought separate compilation example extended with reversed interfaces

// Copyright 2008 Bluespec, Inc.  All rights reserved.

// ================================================================
// Small Example Suite: Example 1b - extended
// A testbench module communicating with a DUT module via its interface
// ================================================================

// The entire program is given on the next few lines.
// The rest of the file is commentary on this program.

package Test0c;

(* synthesize *)
module mkTb (Empty);

   Ifc_typeb  the_bee <- mkBee();
   Ifc_typec  the_cee <- mkCee();
   Ifc_typea  ifc_dt <- mkModuleDeepThought(the_cee, the_bee);

   rule theUltimateAnswer;
      $display ("Hello World! The answer is: %0d",  ifc_dt.the_answer (32'h0A, 32'd15, 17));
      $finish (0);
   endrule

endmodule: mkTb

interface Ifc_typea;
   method int the_answer (int x, int y, int z);
endinterface: Ifc_typea

interface Ifc_typeb;
   method ActionValue#(int) the_bee(int b0, int b1);
endinterface: Ifc_typeb

interface Ifc_typec;
   method Action the_cee(int c0);
endinterface: Ifc_typec


(* synthesize *)
module mkModuleDeepThought# (Ifc_typec cee_imported, Ifc_typeb bee_imported)(Ifc_typea);

   method int the_answer (int x, int y, int z);
      return x + y + z;
   endmethod

endmodule: mkModuleDeepThought


(* synthesize *)
module mkCee(Ifc_typec);

   method Action the_cee(int c0);
      $display("the cee has arg %d", c0);
   endmethod

endmodule


(* synthesize *)
module mkBee(Ifc_typeb);

   method ActionValue#(int) the_bee(int b0, int b1);
      return b0 * b1;
   endmethod

endmodule


endpackage: Test0c
