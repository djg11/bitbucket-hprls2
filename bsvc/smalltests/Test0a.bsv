// Toy bsvc compiler: smalltest test Test0a.bsv
//
// Separate synthesis of top and bottom.  Two synthesize directives.  No communication between them!
//
package Test0a;

  (* synthesize *)
  module test0abot();

  rule genesis (True);
       	  $display ("Hello World: Test0a");
  endrule:genesis

  endmodule

  (* synthesize *)
  module test0a_top();

    Empty thebot <- test0abot();

  endmodule


endpackage
