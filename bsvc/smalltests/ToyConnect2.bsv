//
// No 2.
// Differs from ToyConnect1/3 in the form of the following line:
//    connectate2(the_source, the_sink);

package ToyConnect2;

  interface Source;
    method Bit#(12) sourcer();
  endinterface

  interface Sink;
    method Action sinker(Bit#(12) arg);
  endinterface

  module mkSource(Source);
    
    Reg#(Bit#(12)) drega <- mkReg(10);
    method Bit#(12) sourcer();
      drega <= drega + 4;
      return drega;
    endmethod
  endmodule

  module mkSink(Sink);
    method Action sinker(Bit#(12) arg);
      $display("Sink arg=%d", arg);
    endmethod
  endmodule

 
  module connectate2(Source src, Sink dest, Empty pligh_);
    rule connectate_pump;
      dest.sinker(src.sourcer());
    endrule
  endmodule

  (* synthesize *)
  module toyconnect_top();


    Source the_source  <- mkSource();
    Sink   the_sink    <- mkSink();

    // Differs from ToyConnect1/3 in the form of the following line:
    connectate2(the_source, the_sink);

  endmodule

endpackage
