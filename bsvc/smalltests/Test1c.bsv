//
// $Id: Test1c.bsv,v 1.1 2013-08-23 13:13:02 djg11 Exp $
// A simple counter with uninit register.
//
//

package Test1c;

  (* synthesize *)
  module test1();
       Reg#(Bit#(10)) tenbit <- mkReg(100) ;
       Reg#(Bit#(10)) threebitu <- mkRegU;


  rule count;
     tenbit <= threebitu + 1;
     threebitu <= tenbit + 10;
  endrule

  endmodule

endpackage

// eof
