//
// DJ Greaves
//
// ToyConnect4 - another minor variation@     let letb4_x <- connectate4(the_source, the_sink);
//
package ToyConnect4;

  interface Source;
    method Bit#(12) sourcer();
  endinterface

  interface Sink;
    method Action sinker(Bit#(12) arg);
  endinterface

  module mkSource(Source);
    
    Reg#(Bit#(12)) drega <- mkReg(10);
    method Bit#(12) sourcer();
      drega <= drega + 5;
      return drega;
    endmethod
  endmodule

  module mkSink(Sink);
    method Action sinker(Bit#(12) arg);
      $display("Sink arg=%d", arg);
    endmethod
  endmodule

 
  module connectate4(Source src, Sink dest, Empty pligh_);
    rule connectate_pump;
      dest.sinker(src.sourcer());
    endrule
  endmodule

  (* synthesize *)
  module toyconnect_top();


    Source the_source  <- mkSource();
    Sink   the_sink    <- mkSink; // Leave off the unit app to show it is implied.

    // Differs from ToyConnect 1/2/3 in the form of the following line:
    let letb4_x <- connectate4(the_source, the_sink);
    return letb4_x;

  endmodule

endpackage
