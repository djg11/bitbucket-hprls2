// Toy bsvc compiler: smalltest test Test1b.bsv
// (C) 2015 DJ Greaves, University of Cambridge, Computer Laboratory.
//
// Toy bsvc compiler: smalltest test Test1.bsv
//

package Test1;

  (* synthesize *)
  module test1();
       Reg#(Bit#(10)) doner [3] ;
       Reg#(Bit#(10)) xx <- mkReg(0) ;
       Reg#(Bit#(10)) yy <- mkReg(1) ;

       doner[2] <- mkReg(10);
       doner[1] <- mkReg(20);
       doner[0] <- mkReg(30);

       rule test_ast1 (True);
          doner[xx>>2] <= doner[1] + 1;
	  xx <= xx + 1;
	  $display ("%d %d Test1 pong %d %d %d", xx, yy, doner[0], doner[1], doner[2]);
       endrule
  endmodule

endpackage

// eof
