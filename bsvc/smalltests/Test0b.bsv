//
//
package Test0b;


  (* synthesize *)
  module test0b_top();  // Here we see () denoting Empty.  Empty really is empty so this is correct under structural typing.

    // Test both forms of do : neither, of course, denotes Empty, in this situation. It is instead further syntactic jiggery where the
    // last argument on the rhs is stripped off and used as the lhs of the do.

    Reg#(Bit#(12)) ureg_sans_paren_twelve <- mkRegU;

    Reg#(Bit#(14)) ureg_with_paren_fourteen <- mkRegU();

  endmodule


endpackage
