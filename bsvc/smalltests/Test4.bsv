package Test4;

// Local function calls:  4 with no tyvars
//                        4a with tyvars
//                        4b Two maybe ports of different parameter types.


(* synthesize *)
module test4(Empty);

  Reg#(Bit#(16)) ros <- mkReg(0);
  Reg#(Bit#(16)) ris <- mkReg(0);

  function Action toga();
     action   ros <= ros+2;   endaction
  endfunction : toga

  rule increment;
     $display("Reg is %h %h", ros, ris);

     ris <= ris + 5;
     toga();
  endrule

endmodule


endpackage
// eof
