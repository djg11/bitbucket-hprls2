//
// Higher-order functions - polymorphic HOF - map test with intermediate application(s) and greater vagueness on midder.
//

package Test8f; // 

import List::*;

  function low_t poly_incrementer(low_t vv);
    return warning("poly_incrementer run 8f", vv);  // The warning and message handlers return their second arg, so incrementer has type 'a -> 'a.
  endfunction

  function Tuple2#(List#(xmid_range_t), String) midder(List#(xmid_domain_t) xmidarg); // Not explict that domain and range tyvars are common.
    let newlisty = map(poly_incrementer, xmidarg);
    return tuple2(newlisty, "fred");
  endfunction

  (* synthesize *)
  module test8f_top();
   let testlist = 
      cons(190,
      cons(290,
      cons(390, tagged Nil)));

  let pans = midder(testlist);
  let ye_answer = tpl_1(pans);

 
  Reg#(Int#(32)) bobreg1 <- mkReg(head(ye_answer));
  Reg#(Int#(32)) bobreg2 <- mkReg(head(tail(ye_answer)));

  endmodule


endpackage
