

package ToyConnect;

  interface Source;
    method Bit#(12) sourcer();
  endinterface

  interface Sink;
    method Action sinker(Bit#(12) argi);
  endinterface

  module mkSource(Source);
    
    Reg#(Bit#(12)) drega <- mkReg(10);
    method Bit#(12) sourcer();
      drega <= drega + 3;
      return drega;
    endmethod
  endmodule

  module mkSink(Sink);
    method Action sinker(Bit#(12) argy);
      $display("Sink arg=%d", argy);
    endmethod
  endmodule

  (* synthesize *)
  module toyconnect_top();


    Source the_source  <- mkSource();
    Sink   the_sink    <- mkSink();

    rule explict_pump;
      the_sink.sinker(the_source.sourcer());
    endrule

  endmodule

endpackage
