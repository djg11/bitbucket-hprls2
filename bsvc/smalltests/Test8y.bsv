//
// Higher-order functions.
//
package Test8y;

  function int pogged200(int poo20);
    return poo20 + 200;
  endfunction

  function int togged400(int too40);
    return too40 + 400;
  endfunction

  function int plipper(int poosal, function int formee1(int barond1), function int formee2(int barond2));
    return formee1(312) + formee2(23) + 45 + poosal;
  endfunction

  module test8ybot(int otherarg, Empty nosh);

      let v1 = plipper(33+otherarg, pogged200, togged400);

      Reg#(int) pp <- mkReg(1000+v1);

      rule genesis (True);
       	  $display ("Hello World: Test8a %h", pp);
      endrule:genesis

  endmodule



  (* synthesize *)
  module test8y_top();

    Empty thebot <- test8ybot(32);

  endmodule


endpackage
