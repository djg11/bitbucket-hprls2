//
// Small Bluespec test (C) DJ Greaves 2012. Subinterfaces: A simple subinterface example.
//
// $Id: Test6.bsv,v 1.5 2013-08-23 13:13:02 djg11 Exp $
//
package Test6;

   interface Left#(type leftdata_t);
      method leftdata_t leftread();
   endinterface

  interface Abstract_reg#(type thedata_t);
    interface Left#(thedata_t) gauche;         // Gauche is a the iname of a sub interface to Abstract_reg.
    method Action _write(thedata_t nv);
  endinterface

  // This module has both a parameter and an exported ifc.
  module mkMyareg#(adata_t iv) (Abstract_reg#(adata_t))  provisos (Bits#(adata_t, adata_sz));

    Reg#(adata_t) the_areg <- mkReg(iv) ;

    method Action _write(adata_t nv);
      $display("Wrote 6 %d", nv);
      the_areg <= nv;
    endmethod 

   interface Test6::Left gauche;
      method adata_t leftread();
        return the_areg; // _read() is auto_applied.
      endmethod 
   endinterface

  endmodule

  (* synthesize *)
  module test6m();
       Abstract_reg#(UInt#(15)) xxx <- mkMyareg(30) ;

       rule testit (True);
          xxx <= xxx.gauche.leftread() + 1;
       endrule
  endmodule

endpackage

// eof
