//
//
//
package CompletionTest;

import List::*;
import FIFO::*;
import GetPut::*;
import CompletionBuffer::*;

typedef Bit#(16) Tin;
typedef Bit#(32) Tout;

// Multiplier interface
interface Mult_IFC;
  method Action start (Tin m1, Tin m2);
  method ActionValue#(Tout) result();
endinterface

typedef Tuple2#(Tin,Tin) Args;
typedef 8 BuffSize;
typedef CBToken#(BuffSize) Token;

// This is a farm of multipliers, mkM. The module
// definition for the multipliers mkM is not provided here.
// The interface definition, Mult_IFC, is provided.
module mkFarm#( module#(Mult_IFC) mkM ) ( Mult_IFC );

  // make the buffer twice the size of the farm
  Integer n = div(valueof(BuffSize),2);

  // Declare the array of servers and instantiate them:
  Mult_IFC mults[n];
  for (Integer i=0; i<n; i=i+1) begin
     Mult_IFC s <- mkM;
     mults[i] = s;
  end

  FIFO#(Args) infifo <- mkFIFO;
  // instantiate the Completion Buffer, cbuff, storing values of type Tout
  // buffer size is Buffsize, data type of values is Tout
  CompletionBuffer#(BuffSize,Tout) cbuff <- mkCompletionBuffer;

  // an array of flags telling which servers are available:
  Reg#(Bool) free[n];
  // an array of tokens for the jobs in progress on the servers:
  Reg#(Token) tokens[n];
  // this loop instantiates n free registers and n token registers
  // as well as the rules to move data into and out of the server farm
  for (Integer i=0; i<n; i=i+1) begin
     // Instantiate the elements of the two arrays:
     Reg#(Bool) freef <- mkReg(True);
     free[i] = freef;
     Reg#(Token) ticken <- mkRegU;
     tokens[i] = ticken;
     Mult_IFC s = mults[i];
     // The rules for sending tasks to this particular server, and for
     // dealing with returned results:

     rule start_server (freef); // start only if flag says it's free
	 // Get a token
	 CBToken#(BuffSize) new_t <- cbuff.reserve.get;

	 Args afst = infifo.first;
	 Tin a1 = tpl_1(afst);
	 Tin a2 = tpl_2(afst);
	 infifo.deq;

	 freef <= False;
	 ticken <= new_t;
	 s.start(a1,a2);
       endrule

     rule end_server (!freef);
	 Tout x <- s.result;
	 // Put the result x into the buffer, at the slot ticken
	 cbuff.complete.put(tuple2(ticken, x));
	 freef <= True;
	 endrule
   end

   method Action start (Tin m1, Tin m2);
     infifo.enq(tuple2(m1,m2));
   endmethod

  // Remove the element from the buffer, returning the result
  // The elements will be returned in the order that the tokens were obtained.
  method result = cbuff.drain.get;

endmodule



(* synthesize *)
module mkTopLevelTest (Empty);

  // .. some guts are needed

endmodule

endpackage
