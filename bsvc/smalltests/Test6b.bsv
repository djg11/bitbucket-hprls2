//
// Small Bluespec test (C) DJ Greaves 2012. Subinterfaces: using an interface assignment.
//
// $Id: Test6b.bsv,v 1.4 2013-08-23 13:13:02 djg11 Exp $

package Test6b;

   interface Left#(type leftdata_t);
      method leftdata_t leftread();
   endinterface

  interface Abstract_reg#(type thedata_t);
    interface Left#(thedata_t) links;
    method Action _write(thedata_t nv);
  endinterface

  module mkMyareg#(adata_t iv) (Abstract_reg#(adata_t))  provisos (Bits#(adata_t, adata_size));

    Reg#(adata_t) the_areg <- mkReg(iv) ;

    method Action _write(adata_t nv);
      $display("Wrote 6b %d", nv);
      the_areg <= nv;
    endmethod 

//Does not this 'interface' keyword declare a fresh interface and mask the one already in scope?
// there is some other Left interface that bsc was finding, so use fully qualified name
     interface links = (interface Test6b::Left;
                      method adata_t leftread();
                        return the_areg._read();
                      endmethod
	endinterface);

   // All of the following assignments should work
   //interface links = 
   //interface Left links = 

  endmodule

  (* synthesize *)
  module test6m();
       Abstract_reg#(UInt#(15)) xxx <- mkMyareg(30) ;

       rule testit (True);
          xxx <= xxx.links.leftread() + 1;
       endrule
  endmodule

endpackage

// eof
