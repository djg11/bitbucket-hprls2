//
//
//

package ToyConnect1;

  interface Source;
    method Bit#(12) sourcer();
  endinterface

  interface Sink;
    method Action sinker(Bit#(12) argint);
  endinterface

  module mkSource(Source);
    
    Reg#(Bit#(12)) drega <- mkReg(10);
    method Bit#(12) sourcer();
      drega <= drega + 4;
      return drega;
    endmethod
  endmodule

  module mkSink(Sink);
    method Action sinker(Bit#(12) argant);
      $display("Sink arg=%d", argant);
    endmethod
  endmodule

 
  module connectate(Source src, Sink dest, Empty pligh_);
    rule connectate_pump;
      dest.sinker(src.sourcer());
    endrule
  endmodule

  (* synthesize *)
  module toyconnect_top();


    Source the_source  <- mkSource();
    Sink   the_sink    <- mkSink();

    //Differs from ToyConnect2/3 in the following line:
    Empty dummy_for_connectate <- connectate(the_source, the_sink);

  endmodule

endpackage
