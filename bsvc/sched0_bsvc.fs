// CBG SQUANDERER : Rudimentary/Toy Bluespec Verilog Compiler: Coded in F#.
// (C) 2012 DJ Greaves, University of Cambridge. 
// $Id: sched0_bsvc.fs,v 1.1 2013-08-28 11:39:34 djg11 Exp $

(* All rights reserved, except as licensed by LGPL below.
 * DJ Greaves, University of Cambridge, Computer Laboratory.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *)


module sched0_bsvc

open System.Collections.Generic;
open System.Numerics

open moscow
open meox
open yout

(*
open dotreport
open tableprinter
*)


open tydefs_bsvc
open hprls_hdr

//
// sched0: This file contains a dummy scheduller that does not do anything at all.  We have simple textual ordering, or not even that if order has been corrupted.
// The add_textual_ordering function would resort back into a textual order. But  this empty scheduller is just a placeholder for future grand plans. Use sched1 normally please.

//
// If we run with no scheduler there is no priority of occasionally firing rules wrt to constantly ready rules that conflict on a resource.  
// The priority of conflicting WaW rules is the inverse order of processing, since writes overwrite previous writes.

// We also ignore the descending_urgency manual priority specifications etc..
//   

let no_schedule ww bobj iname sh_emits dirinfo shed3_ conflict_log_ rulectl =
    let ww = WF 1 "Using no scheduler " ww ("Start for " + iname)    
    let vd = bobj.sched_loglevel
    let m = "scheduler=no_schedule"
    let myfalse bexp = xbmonkey bexp = Some false
        //bconstantp g && xi_bmanifest "myfalse invalid condition check"  g = false
    let rec igconjl cc = function
        | IG_resource(idl, ids, iguard, resats_) -> ix_and cc iguard
        | IG_alpha x -> ix_and cc x
        | IG_mux(g, t, f) -> ix_and cc (xi_bquery(g, Map.fold igconjm X_true t, Map.fold igconjm X_true f))
    and igconjm cc key_ item = igconjl cc item

    let no_scheduler ww = function // Basic anarchist capping off without conflict checks or thoughts of fairness.
        | RC_methbuf(idl, ids, en, l, ig) ->
            let g1 = Map.fold igconjm X_true ig.igs 
            if myfalse g1 then
                hpr_warn(m + sprintf ": Method '%s' can never fire (intrinsic guard invalid) and is being discarded." (htosp idl))
            vprintln 3 (m+ sprintf ": RC_methbuf: %s := %s;" (xToStr l) (xbToStr g1))
//          mutadd sh_emits.comb_logic (gen_buf(l, en))
            mutadd sh_emits.comb_logic (gen_buf(l, xi_blift g1)) // A simple capoff that wires l to g1.
            ()
        | RC_rule ri ->
            let g1 = Map.fold igconjm X_true ri.resource_records.igs
            if myfalse g1 then
                dev_println (sprintf "sched0: igconjl key=%s da=%s" (hptos ri.rulename) (igMapToStr ri.resource_records.igs))
                hpr_warn(m + sprintf ": Rule '%s' can never fire (intrinsic guard invalid) and is being discarded." (htosp ri.rulename))
            mutadd sh_emits.comb_logic (gen_buf(ri.fireguard, xi_blift g1)) // A simple capoff that connects fire to composite guard.
            ()

    vprintln 0 ("bsvc: Using no scheduler at all!")
    app (no_scheduler ww) rulectl
    ()

// eof  
