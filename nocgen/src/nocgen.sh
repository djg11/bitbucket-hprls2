#!/bin/bash
set -x
FSHARPC_FIX="/nowarn:75 /consolecolors- /nologo  /lib:. --nowarn:25,64  --noframework /r:System.Numerics.dll /r:System /r:System.Xml.dll /r:System.Core.dll" 
PRIV_DISTRO=$HPRLS/nocgen/pvdist
# -loglevel=5 -verboselevel=5
mono $PRIV_DISTRO/lib/nocgen.exe -verbose  -conerefine=disable $*

# eof
