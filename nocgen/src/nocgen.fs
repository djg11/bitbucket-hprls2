// nocgen.fs
//
// (C) 2015 DJ Greaves, University of Cambridge, Computer Laboratory.

open Microsoft.FSharp.Collections
open System.Collections.Generic


open yout
open moscow
open verilog_hdr
open meox
open hprls_hdr
open abstract_hdr
open verilog_gen
open verilog_render
open opath_hdr
open cpp_render
open opath
open hprxml


open hprtl
open datasrcsink
open cc_blocks
open std_blocks

let g_nocgen_banner = "nocgen RTL tooling - V0.0"

let nocgen_temp_args =
   [

   ] 



let g_canned_topologies = ["all"; "ring"; "torus-2d-3"; "torus-2d-5"; "bus-and-ring"; "src-sink-test-cc"; "src-sink-test-ss"; ]

//
let gec_canned_test0 ww (canned_topology) = // ... generic ...

    let ww = WF 1 "gec_canned_test0" ww (sprintf "Start for topology=%s" canned_topology)
    let layout_domain = Some "GDOM_NOC"


    let width = 2
    let height = 4
    let genv = [ ("DATA_WIDTH", xi_num 132) ]
    let dims = [ 2; 3; 4; 5; 8; 12; ] // 15; 20; 25 ]
    
    let vm_ = 

        match canned_topology with
            | "all" ->
                let a = mk_canned_simple_cc_setup0 ww layout_domain
                let b =
                    let gen dim =
                        let x = mk_2d_torus_cc_noc_testbench ww false dim dim genv
                        let y = mk_2d_torus_cc_noc_testbench ww true dim dim genv                        
                        let z = mk_canned_ring_testbench0 ww genv (dim*dim)
                        [x; y; z]
                    list_flatten(map gen dims)
                a :: b
            | "src-sink-test-ss"
            | "src-sink-test-cc" ->
                [mk_canned_simple_cc_setup0 ww layout_domain]
                

            | "ring"       -> [ mk_canned_ring_testbench0 ww genv 8 ]

//            | "torus-2d-3"  -> mk_2d_torus_cc_noc_testbench ww 132 false width height genv
//            | "torus-2d-5"  -> mk_2d_torus_cc_noc_testbench ww 132 true width height genv            
            | other -> sf (sprintf "gec_canned_test: unsupported name=%s" other)                


    //let nbm = mk_canned_ring_test0_nbm ww layout_domain
    //let vm = nbm.finish_definition ww []

    // Optionally rez an instance of our top-level machine (the testbench) for simulation. ... please sort out
    let finish_structure_tree vm =
        let rez_opath1_ii stagename externally_instantiatedf i_name kind_vlnv =
                { g_null_vm2_iinfo with
                      kind_vlnv=            kind_vlnv // { g_canned_default_iplib  with kind=kkey }
                      iname=                i_name
                      definitionf=          true
                      externally_provided=  false
                      external_instance=    externally_instantiatedf 
                      preserve_instance=    true
                      generated_by=         stagename
                      in_same_file=         hprtl_in_same_file
                      layout_domain=        layout_domain
                  }

        let externally_instantiatedf = true
        let iname = "HPRTL-TOPLEVEL"
        let ii = rez_opath1_ii stagename externally_instantiatedf iname (hpr_minfo vm).name
        let fu_definition = (ii, Some vm)
        fu_definition 

    //let ans = (finish_structure_tree vm) :: !g_m_hprtl_defs 
    let ans = !g_m_hprtl_defs 
    ans


let nocgen_main ww op_args c1 () = 
    let canned_topology = cmdline_flagset_validate "canned-topology" g_canned_topologies 0 c1
    let vms_out = gec_canned_test0 ww (g_canned_topologies.[canned_topology]) 
    vms_out


let nocgen_argpattern =
   [
       //       Arg_enum_defaulting("nocgen-dump-straightaway", ["enable"; "disable"], "disable", "Enable Kernighan and Ritchie style (pre-2001) Verilog port syntax");

       Arg_enum_defaulting("canned-topology", g_canned_topologies, "src-sink-test-cc", "Network-topology");              
   ] 

 
// This is the top-level instance of this o/path plugin
let opath_nocgen_fe ww op_args vms_in =
    let c1:control_t = op_args.c3
    let stagename = op_args.stagename
    let disabled = (cmdline_flagset_validate stagename ["enable"; "disable" ] 0 c1 = 1)
    let ww = WF 1 "cv3" ww "Starting"


    if disabled
    then
        vprintln 1 "nocgen: Stage is disabled"
        vms_in
    else                
        //let zz = cmdline_flagset_validate "skip-propagate" ["false"; "true" ] 0 c1
    let dump_straightaway = Some "dropping2"
    let vms_out = nocgen_main ww op_args c1 ()
    let ww = WF 1 "cv3" ww "Finished"        
    vms_out @ vms_in
    


let install_nocgen() =
    let op_args__ =
            { stagename=         "nocgen-fe"
              banner_msg=        "nocgen-fe"
              argpattern=        nocgen_argpattern
              command=           None //"nocgen"
              c3=                []
            }
    install_operator ("nocgen-fe",  "Various maniplulations of RTL designs held in hbev form.", opath_nocgen_fe, [], [], nocgen_argpattern);        



let _ =
    if false then
        let op_args =
            { stagename=         "nocgen"
              banner_msg=        "nocgen"
              argpattern=        nocgen_temp_args
              command=           None //"nocgen"
              c3=                []
            }
        //let ww = WF 1 "nocgen" ww "Starting"
        //temp_ep WW_end op_args []
        ()

[<EntryPoint>]
let main (argv : string array) =
    g_version_string := g_nocgen_banner
    loadPlugins() // This is supposed to autoload most of the below!
    ignore(conerefine.install_coneref())

    stimer.install_stimer()
    install_verilog()
    install_nocgen()
    //nocgen.install_nocgen()        
    //restructure.install_restructure()
    diosim.install_diosim()
    install_c_output_modes()
    let rc = opath_main ("nocgen", argv) // Give default recipe name.
    rc




// eof
