
module cc_blocks

open System.Numerics

open yout
open meox
open moscow
open hprls_hdr
open abstract_hdr

open hprtl
open std_blocks

// ----------------------------------------
// A simple/mux credit counter.
// Mux has one simple credit counter at its output port that starts at zero. Decremented on each send and incremented on each credit return.
// The mux has other state elements in its input FIFOs and startup logic.
// This should flag error on excess return credit overflow!
let mk_simple_credit_counter ww max_credit =
    let ww = WF 2 "mk_simple_credit_counter" ww "Start"
    let width = bound_log2 (BigInteger (max_credit:int))
    let nbm = new nbm_t(ww, sprintf "SIMPLE_CREDIT_COUNTER%i" max_credit, false, [])

    // Formals
    let credit_debit     = hx_input ww nbm "credit_sending_debit" 1
    let credit_return    = hx_input ww nbm "credit_return_inc" 1
    let credit_available = hx_output ww nbm "credit_available" 1

    let meld_ = nbm.fu_end ww

    // Locals
    let ctr = vectornet_w(funique "credit_counter", width) // Better to be named based on iname or pram!
    nbm.add_freehand_locals ww [ctr]

    // Behaviour
    let have_credit = hx_dned ctr xi_zero
    nbm.add_comb(credit_available, have_credit) 

    let n_ctr = hx_plus (credit_return) (hx_minus ctr credit_debit)
    nbm.add_seq(ctr, n_ctr)

    let vmd = nbm.finish_definition ww []
    let ww = WF 2 "mk_simple_credit_counter" ww "Finish"
    vmd


// ----------------------------------------
// A demux credit counter.
// Demux has some number of tally inputs
// and is self decrementing.  It perhaps resets to some initial non-zero credit but this will often be zero. (mux situation is different: bit width comes from initial credit?).
// All should flag error on excess return credit overflow!
let gec_demux_credit_counter ww n_inputs starting_credit max_credit =
    let ww = WF 2 "gec_demux_credit_counter" ww "Start"
    let credit_width = bound_log2 (BigInteger (max_credit:int))
    let nbm = new nbm_t(ww, sprintf "DEMUX_CREDIT_COUNTER%i_%i" starting_credit max_credit, false, [])

    let director_started = xi_num 1 // for now

    // Formals
    let credit_out = ionet_w("credit_out", 1, OUTPUT, Unsigned, []) 
    let credit_inputs = map (fun n -> ionet_w("credit_in" + i2s n, 1, INPUT, Unsigned, [])) [ 0 .. n_inputs-1 ]
    nbm.add_ports ww "" [  HBT_freehand_formals(credit_out :: credit_inputs) ]
    let meld_ = nbm.fu_end ww

    cassert(starting_credit = 0, "starting credit = 0")

    // Locals
    let ctr = vectornet_w(funique "credit_counter", credit_width) // Better to be named based on iname or pram!
    nbm.add_freehand_locals ww [ctr]

    // Behaviour
    let have_credit = hx_and (director_started) (hx_dned ctr xi_zero)
    nbm.add_seq(credit_out, have_credit) // Return a credit on all cycles we have some.

    let n_ctr = hx_plus (hx_tally credit_inputs) (hx_minus ctr have_credit)
    nbm.add_seq(ctr, n_ctr)

    let vmd = nbm.finish_definition ww []
    let ww = WF 2 "mk_demux_credit_counter" ww "Finish"
    vmd



// ----------------------------------------
// A demultiplexor for credit flow controlled busses..

let mk_nway_cc_demux ww genv n_outputs mk_addressing_predicate  =
    let domain = None
    let ww = WF 2 "mk_nway_cc_demux" ww "Start"
    let kind = sprintf "NWAY_CC_DEMUX_D%i" n_outputs
    let nbm = new nbm_t(ww, kind, false, genv)
    let word_width = atoi32(nbm.rides_lookup "32" "DATA_WIDTH")
    //let director_started = xi_num 1 // for now
    //let rides_and_defaults = [ ("DATA_WIDTH", xi_num ring_dwidth); ("ADDRESS_WIDTH", xi_num 4); ("STATION_NO", xi_num 1) ]

    let portmeta = [] // (string * string) list !
    // Formals
    let (inss_rschema, inss_nets) = nbm.busrez2 ww portmeta (Some false) g_standard_cc_netport_schema ("output_port")

    let (outss_rschema, outss_nets) =
        let zipped = map (fun port_no -> nbm.busrez2 ww portmeta (Some true) g_standard_cc_netport_schema ("in_port" + i2s port_no)) [0 .. n_outputs-1]
        app (fun (rs, rn) -> nbm.add_ports ww "" [ HBT_structured_formal(rs, rn) ]) zipped
        (fst (hd zipped), map snd zipped) 


    let meld_ = nbm.fu_end ww


    let [rxport_din; rxport_valid; rxport_crv] = map snd inss_nets    

    let unary_strobes =
        let gec_unary n = (n, simplenet("US" + i2s n))
        map gec_unary [ 0 .. n_outputs - 1 ]
        
    // Instantiate the routing predicator (a combinational function of the inputbus)
    let predicator =
        let portmeta = []
        let gen_unary_strobe (n, net) = ("sel" + i2s n, net)
        let bindings = HBT_freehand_bindings(("din", rxport_din) :: (map gen_unary_strobe unary_strobes))
        nbm.add_child ww mk_addressing_predicate domain "predicator" portmeta [bindings]
        ()

    // Behaviour
    let wire_up_output (n, unary_strobe) = 
        let [dout; valid; crv] = map snd outss_nets.[n]
        nbm.add_comb(dout, rxport_din)     // Data bus just fans out -- all wired in parallel
        nbm.add_comb(valid, hx_and unary_strobe  rxport_valid)

    app wire_up_output unary_strobes
        

    let credit_tally =
        let item = gec_demux_credit_counter ww n_outputs 0 100 // Arbitrary max credit here!
        let wire_tally_in ([dout; valid; (_, crv)], n) = ("credit_in" + i2s n, crv)
        let bindings = [  HBT_freehand_bindings (("credit_output", rxport_crv) :: (map wire_tally_in (zipWithIndex outss_nets))) ]
        let portmeta = []
        nbm.add_child ww item domain "credit_tally" portmeta bindings
        
    let vmd = nbm.finish_definition ww []
    let ww = WF 2 "mk_nway_cc_demux" ww "Finish"
    vmd


// ----------------------------------------
// A round robin arbiter with a master enable.

let mk_arbiter_rrobin ww rides_and_defaults arity =

// let round_robin_arbiter_gen ww bobj clkinfo sh_emit cls arity = // One share per participant.
    let ww = WF 2 "mk_arbiter_rrobin" ww "Start"
    if arity < 2 then sf "no-bits-rr-arbiter"

    let bits = bound_log2(BigInteger(arity-1))
    let kind = sprintf "RROBIN_ARBITER_%i" arity
    let nbm = new nbm_t(ww, kind, false, [])
    let keystr = "RRA"

    let reqs_and_grants =
        let gec nn =
            let grant = ionet_w("grant" + i2s nn, 1,    OUTPUT, Unsigned, [])
            let req = ionet_w("req" + i2s nn, 1,       INPUT, Unsigned, [])
            (nn, req, grant)
        map gec [ 0 .. arity-1 ]
    let enable = ionet_w("enable", 1,       INPUT, Unsigned, [])    

    let myformals =
        [ HBT_freehand_formals(enable :: map f2o3 reqs_and_grants @ map f3o3 reqs_and_grants) ]
    nbm.add_ports ww "" myformals
    let _ = nbm.fu_end ww

    let arb_advance_net = simplenet (keystr + "_advance")
    let statereg = // Generate state net for a round-robin arbiter.
        let ats = [ Nap(g_resetval, "0") ]
        ionet_w(keystr + (sprintf "_arbiter_state_reg_%i" bits), bits, LOCAL, Unsigned, ats)
    nbm.add_freehand_locals ww [ statereg; arb_advance_net ] 

    //let ww = WF 2 "round_robin_arbiter_gen" ww (sprintf " arbiter arity=%i state " arity + netToStr statereg)    
    let moda x = if x < 0 then x + arity elif x >= arity then x - arity else x
    let (_, reqs, acks) = List.unzip3 reqs_and_grants
    let gen_ack_logic (i, req, ack) =
            let rec razzer j =
                let site = hx_deqd (statereg) (xi_num j)
                let rec reqa k =
                    if k = i then xi_one else 
                        //dev_println(sprintf "reqa   i=%i  j=%i  k=%i" i j k)
                        hx_and (hx_inv reqs.[k]) (reqa (moda(k+1)))
                let here = hx_and site (reqa (moda(j+1)))
                if j=i then here else hx_or here (razzer (moda(j-1)))
            let no_higher = razzer (moda(i-1))
            //dev_println (sprintf " round robin no-higher predicate for i=%i is %s" i (xbToStr no_higher))
            nbm.add_comb (ack, hx_and req no_higher)

    app gen_ack_logic reqs_and_grants

    let nsf_logic = // A simple unary to binary encoder is all that is needed.  We generate a priority encoder which is a little bit of overkill.
        let rec nsf_enc i =
            if i = 0 then xi_num 0
            else hx_query acks.[i] (xi_num i) (nsf_enc (i-1))
        nsf_enc (arity-1) // The state register contains the ordinal of the last-serviced client.


    let advance_condition = hx_and enable (hx_orl reqs) 
    nbm.add_comb(arb_advance_net, advance_condition)
    nbm.add_seq_g arb_advance_net (statereg, nsf_logic)
    
    let vmd = nbm.finish_definition ww []
    let ww = WF 2 "mk_arbiter_rrobin" ww "Finish"
    vmd



// ----------------------------------------
// A start-up credit generator for credit-controlled links.
let mk_starter_cc ww genv =
    let ww = WF 2 "mk_starter_cc" ww "Start"
    let kind = sprintf "STARTUP_CC" 
    let nbm = new nbm_t(ww, kind, false, genv)
    let starting_credit_amount = atoi32(nbm.rides_lookup "2" "STARTING_CREDIT")
    
    // Formals (for starter_cc)
    let starting_credit = hx_output ww nbm "starting_credit" 1
    let meld_ = nbm.fu_end ww

    // Locals
    let width = bound_log2 (BigInteger (starting_credit_amount:int))    
    let ctr = hx_register_with_reset ww nbm (i2s starting_credit_amount) "ctr" width

    let n_ctr = hx_minus ctr xi_one
    let havesome = hx_dned ctr xi_zero
    nbm.add_seq_g havesome (ctr, n_ctr) // Just decrement until all issued. Then do nothing ever again.

    nbm.add_seq (starting_credit, havesome)

    let vmd = nbm.finish_definition ww []
    let ww = WF 2 "mk_starter_cc" ww "Finished"
    vmd

// ----------------------------------------
// A credit-controlled FIFO.  Like a one-input multiplexor, a wrapper around the 2-place FIFO.

let mk_fifo_cc ww genv  =
    let domain = None
    let ww = WF 2 "mk_fifo_cc" ww "Start"
    let kind = sprintf "FIFO_CC" 
    let nbm = new nbm_t(ww, kind, false, genv)
    let word_width = atoi32(nbm.rides_lookup "32" "DATA_WIDTH")
    vprintln 3 (sprintf "for %s word_width=%i   all=%A" kind word_width genv)
    let director_started = xi_num 1 // for now

    // Formals (for FIFO)
    let portmeta = [] // (string * string) list !
    let (inss_rschema, inss_nets) = nbm.busrez2 ww portmeta (Some false) g_standard_cc_netport_schema ("in_port")
    let (outss_rschema, outss_nets) = nbm.busrez2 ww portmeta (Some true) g_standard_cc_netport_schema ("out_port")

    let meld_ = nbm.fu_end ww

    let [rxport_din; rxport_valid; rxport_crv] = map snd inss_nets    
    let [txport_dout; txport_valid; txport_crv] = map snd outss_nets    


    let starting_credit_amount = 2
    // Issue start-up credit.
    let starting_credit =
        let l_starting_credit =  hx_register ww nbm "l_starting_credit" 1
        let starter_bindings = HBT_freehand_bindings[("starting_credit", l_starting_credit)]
        let portmeta = []
        let starter = mk_starter_cc ww (("STARTING_CREDIT", xi_num starting_credit_amount)::genv)
        nbm.add_child ww starter domain "starter" portmeta [starter_bindings]
        l_starting_credit
    

    let logged   = hx_register ww nbm "l_logged" 1
    let dequeue  = hx_register ww nbm "l_deqeue" 1
    let credit_available = hx_register ww nbm "l_credit_available" 1    


    let fifo_bindings = HBT_freehand_bindings[ ("dequeue", dequeue); ("enqueue", rxport_valid); ("din", rxport_din); ("dout", txport_dout); ("not_empty", logged) ] 
    let fifo = mk_two_place_fifo ww genv 

    let core_fifo_ =
        let portmeta = []
        nbm.add_child ww fifo domain ("THEFIFO") portmeta [fifo_bindings]
        
        
    let output_credit_counter =
        let item = mk_simple_credit_counter ww 10  // Arbitrary max credit here!
        let op_credit_bindings = [  HBT_freehand_bindings [("credit_available", credit_available); ("credit_return_inc", txport_crv); ("credit_sending_debit", txport_valid)] ]
        let portmeta = []
        nbm.add_child ww item domain "op_credit_counter" portmeta op_credit_bindings

    // Instantiate the policer (if any)
    nbm.add_comb (dequeue, hx_andl [ director_started; logged; credit_available] )
    nbm.add_comb (txport_valid, dequeue)
    nbm.add_comb (rxport_crv, hx_or starting_credit dequeue)

    let vmd = nbm.finish_definition ww []
    let ww = WF 2 "mk_fifo_cc" ww "Finish"
    vmd

// ----------------------------------------
// A data source for credit-controlled links.

let mk_datasrc_cc ww genv  =
    let domain = None
    let _:(string * hexp_t) list = genv
    let ww = WF 2 "mk_datasrc_cc_mux" ww "Start"
    let kind = sprintf "DATASRC_CC" 
    let nbm = new nbm_t(ww, kind, false, genv)
    let word_width = atoi32(nbm.rides_lookup "32" "DATA_WIDTH")
    let director_started = xi_num 1 // for now

    //let rides_and_defaults = [ ("DATA_WIDTH", is2 ring_dwidth); ]

    // Formals (for data src)
    let portmeta = [] // (string * string) list !
    let (outss_rschema, outss_nets) = nbm.busrez2 ww portmeta (Some true) g_standard_cc_netport_schema ("out_port")

    let meld_ = nbm.fu_end ww 

    let [txport_dout; txport_valid; txport_crv] = map snd outss_nets    

    // Freehand locals
    let seed = 3
    let reset_val = seed
    let prbs_d_register  = hx_register_with_reset ww nbm  (i2s reset_val) "l_prbs_d" 15 
    let prbs_c_register  = hx_register_with_reset ww nbm  (i2s reset_val) "l_prbs_c" 15     
    let credit_available = hx_register ww nbm "l_credit_available" 1
    
    let getbit n arg = ix_bitand (xi_num 1) (ix_rshift Unsigned arg (xi_num n))

    // Behaviour
    let new_c = hx_xor (getbit 13 prbs_c_register) (getbit 14 prbs_c_register)
    nbm.add_seq    (prbs_c_register, ix_bitor (ix_lshift prbs_c_register (xi_num 1)) new_c)

    let logged = hx_dltd (prbs_c_register) (xi_num 4000) // TODO Get from density override
    let step = hx_andl [ director_started; logged; credit_available ] 
    
// TODO seed on reset
    let new_d = hx_xor (getbit 13 prbs_d_register) (getbit 14 prbs_d_register)
    nbm.add_seq_g step (prbs_d_register, ix_bitor (ix_lshift prbs_d_register xi_one) new_d)

    // Child
    let output_credit_counter =
        let item = mk_simple_credit_counter ww 10  // Arbitrary max credit here!
        let op_credit_bindings = [  HBT_freehand_bindings [("credit_available", credit_available); ("credit_return_inc", txport_crv); ("credit_sending_debit", txport_valid)] ]
        let portmeta = []
        nbm.add_child ww item domain "op_credit_counter" portmeta op_credit_bindings


    nbm.add_comb (txport_valid, step)
    nbm.add_comb (txport_dout, prbs_d_register)
    
    let vmd = nbm.finish_definition ww []
    let ww = WF 2 "mk_datasrc_cc_mux" ww "Start"
    vmd




// ----------------------------------------
// A data sink for credit-controlled links.

let mk_datasink_cc ww genv = // TODO: hprtl should assist so that these mk routines should run once for each squirrel.
    let domain = None
    let _:(string * hexp_t) list = genv
    let ww = WF 2 "mk_datasink_cc" ww "Start"
    let kind = sprintf "DATASINK_CC" 
    let nbm = new nbm_t(ww, kind, false, genv)
    let word_width = atoi32(nbm.rides_lookup "32" "DATA_WIDTH")
    let director_started = xi_num 1 // for now

    // Formals (for sink)
    let (inss_rschema, inss_nets) =
        let portmeta = []
        nbm.busrez2 ww portmeta (Some false) g_standard_cc_netport_schema ("in_port")

    let starting_credit_amount = 2
    // Issue start-up credit.
    let starting_credit =
        let l_starting_credit =  hx_register ww nbm "l_starting_credit" 1
        let starter_bindings = HBT_freehand_bindings[("starting_credit", l_starting_credit)]
        let portmeta = []
        let starter = mk_starter_cc ww (("STARTING_CREDIT", xi_num starting_credit_amount)::genv)
        nbm.add_child ww starter domain "starter" portmeta [starter_bindings]
        l_starting_credit
    
    let meld_ = nbm.fu_end ww

    let [ rxport_din; rxport_valid; rxport_crv] = map snd inss_nets    


    
    // Locals
    let (dd1, dd2) = (simplenet "dd1", simplenet "dd2")
    let counter = vectornet_w("counter", 32)
    let rx_register = vectornet_w("rx_register", int word_width)     
    nbm.add_freehand_locals ww [ counter; rx_register; dd1; dd2 ]

    // Further behaviour
    nbm.add_seq  (dd1, rxport_valid) // Shift register so dont reply immediately.  Even better to jitter it with a prbs!
    nbm.add_seq  (dd2, dd1)
    nbm.add_seq_g  rxport_valid (rx_register, rxport_din)
    nbm.add_seq_g rxport_valid (counter, ix_plus counter xi_one)

    nbm.add_comb (rxport_crv, hx_or starting_credit dd2)    
    let vmd = nbm.finish_definition ww []
    let ww = WF 2 "mk_datasink_cc" ww "Finished"
    vmd

// ----------------------------------------
// A multiplexor for credit flow controlled busses..

let mk_nway_cc_mux ww genv n_inputs =
    let domain = None
    let ww = WF 2 "mk_nway_cc_mux" ww "Start"
    let kind = sprintf "NWAY_CC_MUX_D%i" n_inputs
    let nbm = new nbm_t(ww, kind, false, genv)
    let word_width = atoi32(nbm.rides_lookup "32" "DATA_WIDTH")
    
    //let director_started = xi_num 1 // for now

    //let rides_and_defaults = [ ("DATA_WIDTH", xi_num ring_dwidth); ("ADDRESS_WIDTH", xi_num 4); ("STATION_NO", xi_num 1) ]




    // Formals (for mux)
    let (inss_rschema, inss_nets) =
        let portmeta = [] // (string * string) list
        let zipped = map (fun port_no -> nbm.busrez2 ww portmeta (Some false) g_standard_cc_netport_schema ("in_port" + i2s port_no)) [0 .. n_inputs-1]
        (fst (hd zipped), map snd zipped) 

    let (outss_rschema, outss_nets) =
        let portmeta = [] // (string * string) list
        nbm.busrez2 ww portmeta (Some true) g_standard_cc_netport_schema ("out_port")

    nbm.add_ports ww "" [ HBT_structured_formal(outss_rschema, outss_nets); ]

    let meld_ = nbm.fu_end ww

    let [txport_dout; txport_valid; txport_crv] = map snd outss_nets    

    let starting_credit_amount = 2
    // Issue start-up credit.
    let starting_credit =
        let l_starting_credit =  hx_register ww nbm "l_starting_credit" 1
        let starter_bindings = HBT_freehand_bindings[("starting_credit", l_starting_credit)]
        let portmeta = []
        let starter = mk_starter_cc ww (("STARTING_CREDIT", xi_num starting_credit_amount)::genv)
        nbm.add_child ww starter domain "starter" portmeta [starter_bindings]
        l_starting_credit
    
    let input_channels =
        let gec_input_channel nn =
            let [din; in_valid; crv] = map snd inss_nets.[nn]
            let chname = "ch" + i2s nn
            let logged = simplenet ("logged_ " + chname)
            let dequeue = simplenet ("dequeue_ " + chname)
            let rdbus = vectornet_w("rdbus_" + chname, word_width)
            nbm.add_freehand_locals ww [dequeue; rdbus; logged]
            let fifo_bindings = HBT_freehand_bindings[ ("dequeue", dequeue); ("enqueue", in_valid); ("din", din); ("dout", rdbus); ("not_empty", logged) ] 
            let fifo = mk_two_place_fifo ww genv
            let portmeta = []
            let input_fifo = nbm.add_child ww fifo domain ("FIFO_" + chname) portmeta [fifo_bindings]
            nbm.add_comb (crv, hx_or starting_credit dequeue)
            (nn, dequeue, rdbus, logged)
        map gec_input_channel [ 0 .. n_inputs - 1 ]
        
    let credit_available = simplenet("credit_available")
    nbm.add_freehand_locals ww [credit_available]

    let output_credit_counter =
        let item = mk_simple_credit_counter ww 10  // Arbitrary max credit here!

        let op_credit_bindings = [  HBT_freehand_bindings [("credit_available", credit_available); ("credit_return_inc", txport_crv); ("credit_sending_debit", txport_valid)] ]
        let portmeta = []
        nbm.add_child ww item domain "op_credit_counter" portmeta op_credit_bindings



    // Instantiate the arbiter ...
    let abiter =
        let gen_unary_strobe (n, net) = ("sel" + i2s n, net)
        let arbiter_bindings =
            let gen_arb_connection cc (nn, dequeue, rdbus, logged) = ("req" + i2s nn, logged) :: ("grant" + i2s nn, dequeue) :: cc 
            let op = [ ("enable", credit_available) ]
            HBT_freehand_bindings(List.fold gen_arb_connection op input_channels)
        let arbiter = mk_arbiter_rrobin ww genv n_inputs
        let portmeta = []
        nbm.add_child ww arbiter domain  "arbiter" portmeta [arbiter_bindings]
        ()


    // Behaviour
    let wire_up_output (nn, dequeue, rdbus, logged)  = 
        nbm.add_comb_g  logged (txport_dout, rdbus)
    app wire_up_output input_channels

    let dequeues = map f2o4 input_channels
    nbm.add_comb(txport_valid, hx_orl dequeues)

    let vmd = nbm.finish_definition ww []
    let ww = WF 2 "mk_nway_cc_mux" ww "Finished"
    vmd


//---------------------------------------------------
//
// CC switching element arity x arity. Made out of arity multiplexors and de-multiplexors.
//


let mk_cc_noc_element ww genv mk_addressing_predicate arity =
    let domain = None
    let portmeta = []
    let kind = sprintf "CC_NOC_ELEMENT_%i" arity
    let ww = WF 2 ("mk_cc_noc_element") ww ("Start " + kind)
    let nbm = new nbm_t(ww, kind, false, genv)

    let in_port_names = map (fun n -> "in_port_%i") [0..arity-1]
    let out_port_names = map (fun n -> "out_port_%i") [0..arity-1]

    // Formals (for CC switching_element)
    let input_ports =
        let portmeta = [] 
        let zipped = map (fun port_no -> nbm.busrez2 ww portmeta (Some false) g_standard_cc_netport_schema ("in_port_" + i2s port_no)) [0 .. arity-1]
        zipped

    let output_ports = 
        let portmeta = [] 
        let zipped = map (fun port_no -> nbm.busrez2 ww portmeta (Some true) g_standard_cc_netport_schema ("out_port_" + i2s port_no)) [0 .. arity-1]
        zipped

    let meld_ = nbm.fu_end ww
        
    let crosspoints = 
        let mk_xpoint (xx, yy) =
            let sex = None 
            let xpoint = nbm.busrez2 ww [] sex g_standard_cc_netport_schema (sprintf "xpoint_%i_%i" xx yy)
            xpoint
        map mk_xpoint (cartesian_pairs [0..arity-1] [0..arity-1])



    let create_and_wire_up_demux nn =
        let demux = mk_nway_cc_demux ww genv arity mk_addressing_predicate
        let in_binding = HBT_structured_bind (["in_port"], input_ports.[nn])
        let out_bindings =
            let gec_binding dd = HBT_structured_bind (["out_port"], crosspoints.[nn+dd*arity])
            map gec_binding [0..arity-1]
        let iname = sprintf ("mux_%i") nn
        nbm.add_child ww demux domain iname portmeta (in_binding::out_bindings)

    let create_and_wire_up_mux nn =
        let mux = mk_nway_cc_mux ww genv arity 
        let in_bindings =
            let gec_binding dd = HBT_structured_bind (["in_port"], crosspoints.[nn*arity+dd])
            map gec_binding [0..arity-1]
        let out_binding = HBT_structured_bind (["out_port"], output_ports.[nn])
        let iname = sprintf ("demux_%i") nn
        nbm.add_child ww mux domain iname portmeta (out_binding::in_bindings)

    app create_and_wire_up_demux [0..arity-1]
    app create_and_wire_up_mux [0..arity-1]

    let vmd = nbm.finish_definition ww []
    let ww = WF 2 ("mk_cc_noc_element") ww ("Finished " + kind)    
    vmd


//---------------------------------------------------

let mk_cc_client_station ww genv = 
    let kind = sprintf "CC_CLIENT_STATION"
    let ww = WF 2 ("mk_cc_client_station") ww ("Start " + kind)
    let nbm = new nbm_t(ww, kind, false, genv)
    let portmeta = [] 
    dev_println(sprintf "%s: genenv is %A" kind genv)
    let in_bus = nbm.busrez2 ww portmeta (Some false) g_standard_cc_netport_schema ("in_port")
    let out_bus = nbm.busrez2 ww portmeta (Some true) g_standard_cc_netport_schema ("out_port")

    let meld = nbm.fu_end ww // No contacts for a testbench.


    let domain = Some "TESTFIXTURE_DOMAIN0"
    let src_bindings = HBT_structured_bind (["out_port"], out_bus)
    let sink_bindings = HBT_structured_bind(["in_port"], in_bus)

    let src = mk_datasrc_cc ww genv
    //let fifo = mk_fifo_cc ww rides_and_defaults
    let sink = mk_datasink_cc ww genv

    nbm.add_child ww src domain  "the_src" portmeta [src_bindings]
    nbm.add_child ww sink domain  "the_sink" portmeta [sink_bindings]

    let vmd = nbm.finish_definition ww []
    let ww = WF 2 ("mk_cc_client_station") ww ("End " + kind)
    vmd
    
//---------------------------------------------------
//
// Square Torus NOCs - unidir (radix 3 element) and bi-directional (radix 5 elements)


let mk_noc_address_predicate ww genv arity  =
    let domain = None
    let ww = WF 2 "mk_noc_address_predicate" ww "Start"
    let kind = sprintf "NOC_ADDRESS_PREDICATE_%i" arity
    let nbm = new nbm_t(ww, kind, false, genv)
    let word_width = atoi32(nbm.rides_lookup "32" "DATA_WIDTH")
    let input = hx_input ww nbm ("din") word_width
    let outputs = 
        let gec_output nn =
            hx_output ww nbm ("sel" + i2s nn) 1
        map gec_output [0..arity-1]

    let _ = nbm.fu_end ww
    let fw = bound_log2 (BigInteger arity)
    let field = hx_logic ww nbm (sprintf "field_%i" fw) fw
    nbm.add_comb   (field, ix_mod input (xi_num arity))
    let gen_decode nn =
        nbm.add_comb   (outputs.[nn], hx_deqd (xi_num nn) field)
    app gen_decode [0..arity-1]
    let vmd = nbm.finish_definition ww [] 
    let ww = WF 2 "mk_noc_address_predicate" ww "Finish"
    vmd
    

let mk_2d_torus_cc_noc_testbench ww bidirf width height genv =
    let kind = sprintf "CC_NOC_2D_%s_%i_%i" (if bidirf then "BIDIR" else "UNIDIR") width height
    let ww = WF 2 ("mk_2d_torus_cc_noc_testbench") ww ("Start " + kind +  " at T/S " + timestamp true)
    let nbm = new nbm_t(ww, kind, false, genv)
    
    let portmeta = []
    let minfo_atts = [ (g_csv_report_control_attribute, "enable") ]
    let data_width = atoi32(nbm.rides_lookup "32" "DATA_WIDTH")
    // let genv = [ ("DATA_WIDTH", xi_num 132); ] //  ("ADDRESS_WIDTH", xi_num 4); ("STATION_NO", xi_num 1) 
    let report_data = [ ("TITLE", kind); ("DATA_WIDTH", i2s data_width ); ("N_STATIONS", i2s (width*height)); ("SEED", i2s 4); ("BIDIRF", boolToStr bidirf); ("Width", i2s width); ("Height", i2s height) ]



    //type csv_report_field_t = string * string

    let meld_ = nbm.fu_end ww // This is a bench?
    
    let wrap modlim v = if v<0 then v+modlim elif v >= modlim then v-modlim else v
    
    let coords = cartesian_pairs [0..width-1] [0..height-1]


    let mk_link direction (src_x, src_y) (dx, dy) = // Main mesh link between peer nodes.
        let (dest_x, dest_y) = (wrap width (src_x + dx), wrap height (src_y + dy))
        let sex = None 
        let link = nbm.busrez2 ww []  sex g_standard_cc_netport_schema (sprintf "link_%i_%i_%s_%i_%i"  src_x src_y direction dest_x dest_y)
        link
        
    let links_east = map (mk_link "east" (1, 0)) coords
    let links_south = map (mk_link "south" (0, 1)) coords
    let links_west = if bidirf then  map (mk_link "west" (-1, 0)) coords else []
    let links_north = if bidirf then  map (mk_link "north" (0, -1)) coords else []      

    let all_links = [ links_east; links_south; links_west; links_north ]

    let arity = if bidirf then 5 else 3

    let mk_node (xx, yy) = // A node has an element and a client. The client has a data src and a data sink.

        let (rx_bus, tx_bus) = 
            let sex = None // Station to local element connections

            let rx_bus = nbm.busrez2 ww portmeta sex g_standard_cc_netport_schema (sprintf "rx_bus_%i_%i" xx yy)
            let tx_bus = nbm.busrez2 ww portmeta sex g_standard_cc_netport_schema (sprintf "rx_bus_%i_%i" xx yy)        
            (rx_bus, tx_bus)


        let client_station =
            let portmeta = []
            let station_iname = sprintf "station_%i_%i" xx yy
            let station_bindings =
                [ HBT_structured_bind(["out_port"], tx_bus); HBT_structured_bind(["in_port"], rx_bus) ]
            let station = mk_cc_client_station ww genv
            nbm.add_child ww station None station_iname portmeta station_bindings
            ()
        
        let mk_addressing_predicate = mk_noc_address_predicate ww genv arity
        let element = mk_cc_noc_element ww genv mk_addressing_predicate arity
        ((xx, yy), element, rx_bus, tx_bus)
        
    let elements = map mk_node coords

    let wire_element ((xx, yy), element, rx_bus, tx_bus) =
        let element_iname = sprintf "element_%i_%i" xx yy
        let element_bindings1 =
            [ HBT_structured_bind(["in_port_0"], tx_bus)
              HBT_structured_bind(["out_port_0"], rx_bus) 
              HBT_structured_bind(["in_port_1"], links_east.[wrap (xx-1) width])
              HBT_structured_bind(["out_port_1"],links_east.[xx]) 
              HBT_structured_bind(["in_port_2"], links_south.[wrap (yy-1) height])
              HBT_structured_bind(["out_port_2"],links_south.[yy]) ]

        let element_bindings2 =
            if bidirf then
                [
                    HBT_structured_bind(["in_port_3"], links_west.[wrap (xx+1) width])
                    HBT_structured_bind(["out_port_3"],links_west.[xx]) 
                    HBT_structured_bind(["in_port_4"], links_north.[wrap (yy+1) height])
                    HBT_structured_bind(["out_port_4"],links_north.[yy]) ]
            else []
        nbm.add_child ww element None element_iname portmeta (element_bindings1 @ element_bindings2)
        ()

    app wire_element elements
    let attributes = hx_attributes minfo_atts @ hx_report_data report_data
    let vmd = nbm.finish_definition ww attributes
    let ww = WF 2 ("mk_2d_torus_cc_noc_testbench") ww ("Finished " + kind)    
    vmd


//---------------------------------------------------
//
//  Standard synchronous data source.
//
let mk_datasrc101_ss ww genv = //
    let ww = WF 2 "mk_datasrc101_ss" ww "Start"
    let kind = sprintf "DATASRC101_SS"
    let nbm = new nbm_t(ww, kind, false, genv)

    // Formals
    let sex = Some (*initiator=*)true // An initiator port instance for transmitting.
    let (outss_rschema, outss_nets) = nbm.busrez2 ww [] sex g_standard_synchronous_netport_schema "out_port"
       
    //nbm.add_ports ww ""  [ HBT_structured_formal(outss_rschema, outss_nets) ]
    
    let _ = nbm.fu_end ww


    let [outss_valid; outss_rdy; outss_data] = map snd outss_nets

    // Locals
    let prbs_d_register = vectornet_w("prbs_d", 15)
    let prbs_c_register = vectornet_w("prbs_c", 15)     
    nbm.add_freehand_locals ww [ prbs_d_register; prbs_c_register ] 

    let getbit n arg = ix_bitand (xi_num 1) (ix_rshift Unsigned arg (xi_num n))

    // Behaviour
    let new_c = hx_xor (getbit 13 prbs_c_register) (getbit 14 prbs_c_register)
    nbm.add_seq    (prbs_c_register, ix_bitor (ix_lshift prbs_c_register (xi_num 1)) new_c)
    nbm.add_comb   (outss_valid, hx_dltd (prbs_c_register) (xi_num 4000)) // TODO Get from density override

// TODO seed on reset
    let step = hx_and outss_valid outss_rdy
    let new_d = hx_xor (getbit 13 prbs_d_register) (getbit 14 prbs_d_register)
    nbm.add_seq_g step          (prbs_d_register, ix_bitor (ix_lshift outss_data (xi_num 1)) new_d)


    nbm.add_comb                (outss_data, prbs_d_register)

    let vmd = nbm.finish_definition ww []
    let ww = WF 2 "mk_datasrc101_ss" ww "Finish"
    vmd

//---------------------------------------------------
//
//  Standard synchronous data sink
//
let mk_datasink101_ss ww genv = // 
    let ww = WF 2 "mk_datasink101_ss" ww "Start"
    let kind = sprintf "DATASINK101_SS"
    let nbm = new nbm_t(ww, kind, false, genv)

    // Formals
    let sex = Some (*initiator=*)false // A target port instance for receiving.
    let (inss_rschema, inss_nets) = nbm.busrez2 ww [] sex g_standard_synchronous_netport_schema "in_port"
       
    let _ = nbm.fu_end ww


    let [outss_valid; outss_rdy; outss_data] = map snd inss_nets
#if CHECKING
    // Locals
    let prbs_d_register = vectornet_w("prbs_d", 15)
    let prbs_c_register = vectornet_w("prbs_c", 15)     
    nbm.add_freehand_locals ww [ prbs_d_register; prbs_c_register ] 

    let getbit n arg = ix_bitand (xi_num 1) (ix_rshift Unsigned arg (xi_num n))

    // gEnerator behaviour -- needs changing for receiver checker.
    let new_c = hx_xor (getbit 13 prbs_c_register) (getbit 14 prbs_c_register)
    nbm.add_seq    (prbs_c_register, ix_bitor (ix_lshift prbs_c_register (xi_num 1)) new_c)
    nbm.add_comb   (outss_valid, hx_dltd (prbs_c_register) (xi_num 4000)) // TODO Get from density override

    let step = hx_and outss_valid outss_rdy
    let new_d = hx_xor (getbit 13 prbs_d_register) (getbit 14 prbs_d_register)
    nbm.add_seq_g step          (prbs_d_register, ix_bitor (ix_lshift outss_data (xi_num 1)) new_d)


    nbm.add_comb                (outss_data, prbs_d_register)
#endif
    let vmd = nbm.finish_definition ww []
    let ww = WF 2 "mk_datasink101_ss" ww "Finish"
    vmd


// -------------------------------


let mk_ring_station ww genv =
    let kind = sprintf "RING_STATION"
    let ww = WF 2 "mk_ring_station" ww ("Start " + kind)
    let nbm = new nbm_t(ww, kind, false, genv)
    let _ = nbm.fu_end ww

    let ring_dwidth = atoi32(nbm.rides_lookup "32" "DATA_WIDTH")
    let control_field_width = atoi32(nbm.rides_lookup "8" "CONTROL_FIELD_WIDTH")    

    let hx_overridable pid = // Only works if parent provides a dummy value?
        let _ = (nbm.rides_lookup "1" pid)     // Lookup has side effect of adding it to parameter signature
        gec_X_net pid

    let station_no = hx_overridable "STATION_NO"
    // Formals
    let sex = Some false // A target port instance for transmitting.
    let (txport_rschema, txport_nets) = nbm.busrez2 ww [] sex g_standard_synchronous_netport_schema "dtx_port"
    let (rxport_rschema, rxport_nets) = nbm.busrez2 ww [] (Some true) g_standard_synchronous_netport_schema "drx_port"

    let [rxport_valid; rxport_rdy; rxport_data] = map snd rxport_nets
    let [txport_valid; txport_rdy; txport_data] = map snd txport_nets    

    let outlink = ionet_w("outlink", ring_dwidth,         OUTPUT, Unsigned, [])
    let inlink = ionet_w("inlink", ring_dwidth,           INPUT, Unsigned, [])
    

    let myformals = [ HBT_freehand_formals[ inlink; outlink ] ]
    nbm.add_ports ww "" myformals
    let _ = nbm.fu_end ww

    // Locals
    let rx_holding_register = vectornet_w("rx_holding_register", ring_dwidth) 
    nbm.add_freehand_locals ww [ rx_holding_register ]

    // Address field is bottom address_Field_Wdith bits with the full/empty flag in the lsb position. Hence station numbers are
    // multiplied by two here adn the bottom bit set.
    // Behaviour ... Note there is no receive FIFO, so data will have the potential to be overwritten if not serviced immediately. TODO
    let control_field = ix_mask Unsigned control_field_width inlink //
    let rx_action = hx_deqd (ix_plus (xi_num 1) (ix_times (xi_num 2) station_no)) control_field 
    let dp1 = hx_query rx_action (xi_num 0) inlink // Destination release.
    let free_pred = hx_deqd (dp1) (xi_num 0)


    let tx_action = hx_and free_pred txport_valid
    nbm.add_comb            (txport_rdy, tx_action)    
    let dp2 = hx_query tx_action txport_data dp1 // TODO add int dest address
    nbm.add_seq             (outlink, dp2) 
    nbm.add_seq_g rx_action (rx_holding_register, inlink)     // Want a 1-place ping-pong FIFO here.
    nbm.add_comb            (rxport_data, rx_holding_register)

    let vmd = nbm.finish_definition ww []
    let ww = WF 2 "mk_ring_station" ww "Finish"
    vmd

// -----------------

// CC src -> fifo -> sink
       
let mk_canned_simple_cc_setup0 ww layout_domain =
    let kind = sprintf "BENCHUNIT"
    let ww = WF 2 "mk_canned_simple_cc" ww "Start"
    let minfo_atts = [ (g_csv_report_control_attribute, "enable") ]
    let data_width = 132
    let bench_env = [ ("DATA_WIDTH", xi_num data_width); ] //  ("ADDRESS_WIDTH", xi_num 4); ("STATION_NO", xi_num 1) 
    let genv = bench_env
    let report_data = [ ("TITLE", kind); ("DATA_WIDTH", i2s data_width ); ] // ("UTILISATION_PERCENT", i2s 4); ("SEED", i2s 4); ("MISCA", "Yes") 
    
    let nbm = new nbm_t(ww, kind, false, genv)

    let meld = nbm.fu_end ww // No contacts for a testbench.

    let sex = None // Declare as locals ... This needs a local name and then we must do a binding between local name and formal name
    let d1_bus = nbm.busrez2 ww [] sex g_standard_cc_netport_schema ("d1_bus")
    let d2_bus = nbm.busrez2 ww [] sex g_standard_cc_netport_schema ("d2_bus")

    let b0 = [ HBT_structured_bind(["out_port"], d1_bus) ]
    let b1 = [ HBT_structured_bind(["in_port"], d1_bus); HBT_structured_bind(["out_port"], d2_bus) ]
    let b2 = [ HBT_structured_bind(["in_port"], d2_bus) ]


    let src = mk_datasrc_cc ww genv
    let fifo = mk_fifo_cc ww genv
    let sink = mk_datasink_cc ww genv
 
    let tb_domain = Some "TESTFIXTURE_DOMAIN0"
    let portmeta = []
    nbm.add_child ww src   tb_domain      "the_src"  portmeta b0
    nbm.add_child ww fifo  layout_domain  "the_fifo" portmeta b1
    nbm.add_child ww sink  tb_domain      "the_sink" portmeta b2

    let vmd = nbm.finish_definition ww (hx_attributes minfo_atts @ hx_report_data report_data)

    vmd


// Single-tier slotted ring testbench
let mk_canned_ring_testbench0 ww genv (ring_no_stations) =
    let minfo_atts = [ (g_csv_report_control_attribute, "enable") ]
    let kind = sprintf "SINGLE_TIER_SLOTTED_RING_%i" ring_no_stations
    let ww = WF 2 ("mk_canned_ring_testbench0") ww ("Start " + kind +  " at T/S " + timestamp true)

    let nbm = new nbm_t(ww, sprintf "%s" kind, false, genv)
    let test_domain = Some "TESTFIXTURE_DOMAIN0"
    let logic_domain = None
    let ring_dwidth = atoi32(nbm.rides_lookup "32" "DATA_WIDTH")
    let report_data = [ ("TITLE", kind); ("N_STATIONS", i2s ring_no_stations);  ]    
    let meld = nbm.fu_end ww

    // Locals
    let ccbus = vectornet_w(funique "ccbus", 16) // Better to be named based on iname or pram!
    nbm.add_freehand_locals ww [ ccbus ]

    // Child structures (component instances).

    // Create ring nets
    let ring_name = "main_ring"
    let net3x =
        let gec_ring_link no =
            let link = vectornet_w(ring_name + "_link_" + i2s no, ring_dwidth)
            nbm.add_freehand_locals ww [ link ]

            //Structured port nets .. declare a bundle at once, but support any wiring you like ... but default is as per implicit datasheet.
            let sex = None // Declare as locals ... This needs a local name and then we must do a binding between local name and formal name
            let dtx_bus = nbm.busrez2 ww [] sex g_standard_synchronous_netport_schema ("dtx_bus_" + i2s no)
            let drx_bus = nbm.busrez2 ww [] sex g_standard_synchronous_netport_schema ("drx_bus_" + i2s no)
            (link, dtx_bus, drx_bus)
        map gec_ring_link [0..ring_no_stations-1]

    // Create ring stations and data sources
    let ring_stations =
        let data_src  = mk_datasrc101_ss ww genv
        let data_sink = mk_datasink101_ss ww genv         

        
        let gec_ring_station_instance no  =
            vprintln 3 (sprintf "Generating ring station %i on %s" no ring_name)

            let (outlink, dtx_bus, drx_bus)  = net3x.[ no ]

            let iname = sprintf "data_src_%i" no
            let data_src_rides =
                [ ("SEED", xi_num(55*no)); ("DATA_WIDTH", xi_num ring_dwidth) ]
            let data_src_bindings = 
                [ HBT_structured_bind(["out_port"], dtx_bus) ]
            nbm.add_child ww data_src test_domain iname data_src_rides data_src_bindings

            let iname = sprintf "data_sink_%i" no
            let data_sink_rides =
                [ ("DATA_WIDTH", xi_num ring_dwidth) ]
            let data_sink_bindings = 
                [ HBT_structured_bind(["in_port"], drx_bus) ]
            nbm.add_child ww data_sink test_domain iname data_sink_rides data_sink_bindings

            // Now bind station contacts
            let inlink = f1o3(net3x.[ (no - 1 + ring_no_stations) % ring_no_stations ])
            
            let station_bindings =
                HBT_freehand_bindings[ ("inlink", inlink); ("outlink", outlink) ]::
                [ HBT_structured_bind(["dtx_port"], dtx_bus)
                  HBT_structured_bind(["drx_port"], drx_bus) ]

            let iname = sprintf "%s_%i" ring_name no
            let actuals_rides =
                [ ("STATION_NO", xi_num no); ]
            let ring_station = mk_ring_station ww (actuals_rides @ genv )
            nbm.add_child ww ring_station logic_domain iname actuals_rides station_bindings
            ()
        app gec_ring_station_instance [0..ring_no_stations-1]

    // Old spare
        //let c16 = mk_upcounter ww 16 
        //nbm.add_child ww c16 layout_domain "the_c16" [] [ HBT_freehand_bindings[("yy", ccbus)] ]
    let attributes = hx_attributes minfo_atts @ hx_report_data report_data
    let vm = nbm.finish_definition ww attributes
    vm

//
//       


// eof
