
HPR L/S logic synthesis library and several projects.

University of Cambridge 
Computer Laboratory
HPR L/S Orangepath project
--------

(C) 1999-2016 DJ Greaves.

All of the IP and software in this directory and its subdirectories
belongs to David Greaves of the University of Cambridge.
It was open-sourced under GNU LGPL in 2017.

----------------
PREREQUSIITS

You are likely to need mono, mcs, fsharpc, iverilog, verilator, gtkwave.

For KiwiC you will need Mono.Cecil - there is a dll in this distro but it might not be compatible with your mono installation.

For bsvc you will need fsyacc.


Note that the hpr/Makefile.inc contains installation hints and establishes
local site setups that you should edit or otherwise override using env variables.

INSTALL

1. Using setenv or .bashrc, set the HPRLS env variable to the folder that contains hpr (not the hpr folder itself).

2. Compile the HPR L/S library.  This should be a matter of running make.



$ cd hpr
$ make

3. For a particular compiler too, (KiwiC or bsvc or joining or whatever), cd into that projects src folder and type make.

$ cd ../kiwiproj/kiwic/src
$ make

4. Try the test suite
$ cd $(HPRLS)/kiwiproj/kiwic/regression
$ make

Each compiler tool, when made, creates a distro folder.  The distro folder contains a binary file with .exe suffix.  There is also a shellscript that invokes the binary.  For general use, ensure that shellscript is on your PATH.



----------------


Please see

http://www.cl.cam.ac.uk/research/srg/han/hprls/orangepath/kiwic.html

Caution: Everything here is work-in-progress at the moment.  A stable release might be made at some point!


bsystem         - A pre-SystemC implementation of something very like SystemC!

docroot		- Documentation
h2tool		- Front end for H2 experimental language
h4tool		- Front end for H3
cxccfe		- Front end for C-to-Gates C parser.
kiwiproj	- The Kiwi project compiler and runtime library for kiwi under mono/.net
system-integrator - Automatic assembler of IP blocks for multi-FPGA designs.
liblibcv2	- The Verilog front end parser.  Please instead use hpr/cv3xrtl_import that takes xml from the tentech csim4.
bsvc            - Toy Bluespec compiler
resiliant       - TMR/Parity/Hamming plugins for the compilers.
joining         - Experiments with glue logic, protocol adaptor and transactor synthesis.

hpr		- The core HPR Logic Synthesis library.


utils		- diogif etc (Generates a gif from plot output)
hpr		- memoising digital logic IR library


mono		- to be deleted (old kiwic)
vogtile		- to be deleted (A widget library used by yawv)
yawv		- to be deleted : use gtkwave instead (yet another waveform viewer).



----------------
