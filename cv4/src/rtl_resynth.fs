// rtl_resynth.fs
// (C) 2015 DJ Greaves, University of Cambridge, Computer Laboratory.
//

//   rtl_resynth -- Various maniplulations of RTL designs held in hbev form.

module rtl_resynth

open Microsoft.FSharp.Collections
open System.Collections.Generic


open yout
open moscow
open verilog_hdr
open meox
open hprls_hdr
open abstract_hdr
open verilog_gen
open verilog_render
open opath_hdr
open abstracte
open cxverhdr
open dotreport

//
// Simple variant: does not collate the powers etc..
// 
let simple_xbev_resynth ww eE (ast_ctrl:ast_ctrl_t) g00 bev =
    let m_cs = ref []
    let eflt n = sf (sprintf "xbev_resynth_: nox=%i other form in exec_flatten SP_l cannot simply convert to RTL (perhaps use alternative recipe structure):" n + hbevToStr bev)    
    let rec simple_bev_compile nox g cmd =
        match cmd with
            | [] -> nox
            | Xif(g1, ct, cf) :: tt ->
                     let n0 = simple_bev_compile nox (ix_and g1 g) [ct]
                     let n1 = simple_bev_compile nox (ix_and (xi_not g1) g) [cf]
                     let max2(a,b) = if a>b then a else b
                     simple_bev_compile (max2(n0, n1)) g tt 
            | Xcomment _ :: tt 
            | Xskip :: tt     -> simple_bev_compile nox g tt
            | Xblock(v) :: tt -> simple_bev_compile nox g (v @ tt)
                 // Use counter nox to ensure only one assignment in any control flow, otherwise blocking and non-blocking resolution will be needed.
                 // Xeasc clause missing ::  should be converted to PLI here

            // Assign to one fewer power of X owing to implied one in the Rarc RTL construct.
            | Xassign(X_x(lhs, 1, _), r)::tt ->
                if nox=0 || true then (mutadd m_cs (Rarc(g, lhs, r)); simple_bev_compile (nox+1) g tt)
                else
                    hpr_yikes(sprintf "Rtl_resynth: More than one assignment in SP_l control flow path.")
                    eflt nox
            | Xassign(lhs, r)::tt ->
                if nox=0 || true then (mutadd m_cs (Rarc(g, xi_X -1 lhs, r)); simple_bev_compile (nox+1) g tt)
                else
                    hpr_yikes(sprintf "Rtl_resynth: More than one assignment in SP_l control flow path.")
                    eflt nox
            | other::tt ->
                     vprintln 0 ("Rtl_resynth: cannot effect a simple conversion of behavioural command (need an HLS expansion please) " + hbevToStr other)
                     eflt(nox)

    let _ = simple_bev_compile 0 g00 [bev]
    if nullp !m_cs then
        hpr_yikes("rtl_resynth_ all missing")
        muddy "bev // Return original at least."
    else
        let ii = { id=ast_ctrl.id }:rtl_ctrl_t
        let n = SP_rtl(ii, [ XRTL(None, X_true, !m_cs) ])
        n

type net_range_t = int * int // High and low bit positions stored.
type net_power_list_t = ListStore<int, net_range_t * hbexp_t * hexp_t>            // Guards (cen) and expression stored.
type net_powers_db_t = OptionStore<xidx_t, net_power_list_t>        // Indexed by net memo nn.

  

//
// The X0 site refers to the current value of a variable and the X1 site holds an expression for its the next value.
//
let power_insert_factory ww powers_db =        
    let _:net_powers_db_t = powers_db


    let get_power_list xidx =
        match powers_db.lookup xidx with
            | Some pwl -> pwl
            | None ->
                let pwl = new net_power_list_t("powers-for-" + i2s xidx)
                powers_db.add xidx pwl
                pwl

    let insert_power net power gg exp =
        match net with
            | X_bnet ff ->
                let ranger = (encoding_width net - 1, 0)
                let power_list = get_power_list ff.n
                match power_list.lookup power with
                    | [] ->
                        power_list.add power (ranger, gg, exp)
                    | ovl ->
                        // Existed already ... no probs - disjoint guards we trust .... should check as best we can.
                        power_list.add power (ranger, gg, exp)                            
                        ()

            | W_node(prec, V_cast CS_maskcast, [W_node(_, V_rshift _, [X_bnet ff; amount], _)], _) -> // Occurs on lhs for bit-insert operations.
                let msg = "rtl_resynth: insert_power: reveng bit-insert 1/2" 
                let power_list = get_power_list ff.n
                let amount = xi_manifest64 msg amount 
                let (width, baser) = (valOf_or_fail "rtl_resynth: no width in bit-insert 1/2" prec.widtho, int amount)
                let ranger = (width+baser-1, baser)
                if prec.signed=Signed then hpr_yikes("ignoring signed nature of bit insert in " + xToStr net)
                power_list.add power (ranger, gg, exp)                            
                ()

            | W_node(prec, V_cast CS_maskcast, [X_bnet ff], _) -> // Occurs on lhs for bit-insert operations.
                let msg = "rtl_resynth: insert_power: reveng bit-insert 2/2" 
                let power_list = get_power_list ff.n
                let (width, baser) = (valOf_or_fail "rtl_resynth: no width in bit-insert 2/2" prec.widtho, 0)
                let ranger = (width+baser-1, baser)
                if prec.signed=Signed then hpr_yikes("ignoring signed nature of bit insert in " + xToStr net)
                power_list.add power (ranger, gg, exp)                            
                ()


            | other -> sf (sprintf "rtl_resynth: insert_power: other lhs form %s" (xToStr other))


    (insert_power, get_power_list)

// Insert bev input structures in the powers matrix database (powers_db).
// This may not be used really, since input is supposed to be RTL!
let resynth_xbev_insert ww powers_db eE (ast_ctrl:ast_ctrl_t) bev =
    let ww = WF 2 "resynth_xbev_insert" ww (sprintf "Start")
    // Generic power's array - 2-D: net and power, along with CE form.   We can then project through the next-state function, like hypermode.

    let (insert_power, get_power_list) = power_insert_factory ww powers_db
    
    let rec simple_bev_compile nox gg cmd =
        match cmd with
            | [] -> nox
            | Xif(g1, ct, cf) :: tt ->
                let n0 = simple_bev_compile nox (ix_and g1 gg) [ct]
                let n1 = simple_bev_compile nox (ix_and (xi_not g1) gg) [cf]
                let max2(a,b) = if a>b then a else b
                simple_bev_compile (max2(n0, n1)) gg tt 
            | Xcomment _ :: tt 
            | Xskip :: tt     -> simple_bev_compile nox gg tt
            | Xblock(v) :: tt -> simple_bev_compile nox gg (v @ tt)
                 // Use counter nox to ensure only one assignment in any control flow, otherwise blocking and non-blocking resolution will be needed.
                 // Xeasc clause missing ::  should be converted to PLI here


            | Xassign(X_x(lhs, 1, _), rhs)::tt ->
                insert_power lhs 1 gg rhs    // Sequential logic   
                simple_bev_compile (nox+1) gg tt

            | Xassign(lhs, rhs)::tt -> // Combinational logic
                insert_power lhs 0 gg rhs 
                simple_bev_compile (nox+1) gg tt

            | other::tt ->
                hpr_yikes("rtl_resynth: cannot effect a simple conversion of behavioural command (need an HLS expansion please) " + hbevToStr other)
                nox

    let g00 = X_true
    let _  = simple_bev_compile 0 g00 [bev]
    ()


// Insert RTL input structures in the powers matrix database (powers_db).
let resynth_rtl_insert ww vd powers_db eE rtl cc =
    let ww = WF 2 "resynth_rtl_insert" ww (sprintf "Start")

    let (insert_power, get_power_list) = power_insert_factory ww powers_db

    let insert1 gg arg cc =
        match arg with 
        | Rnop ss ->
            arg :: cc
        | Rpli(guard, (fgis, _), args) ->
            dev_println (sprintf "resynth_rtl_insert: Ignore PLI calls for now")
            cc

        | Rarc(guard, lhs, rhs) ->
            insert_power lhs 1 (ix_and gg guard) rhs // Sequential logic
            cc

        //| other -> sf (sprintf "Insert1: Other : %A" other)

    let insert arg cc =
        match arg with
            | XIRTL_buf(gg, lhs, rhs) ->  // Combinational logic
                insert_power lhs 0 gg rhs
                cc
                
            | XRTL(Some(ll, rr), g1, lst) ->
                List.foldBack (insert1 (ix_and g1 (ix_deqd ll rr))) lst cc
            | XRTL(None, g1, lst) ->
                List.foldBack (insert1 g1) lst cc

            | other -> sf (sprintf "Other : %A" other)

    let nv = List.foldBack insert rtl []

    if nullp nv then cc else XRTL(None, X_true, nv) :: cc


let scc_find ww keyname nodes edges =  // Find strongly-connected components 
    vprintln 2 (sprintf "Find SCCs in graph of %i nodes, %i edges" (length nodes) (length edges))
    let nodes = map f1o3 nodes
    let edges = map (fun ((dest, _), (src, _)) -> (src, dest)) edges
    let pof = fun x -> x
    let edges =
        let e = new ListStore<string, string>("edges")
        let insert (s, d) = e.add s d
        app insert edges
        e 
    let sccs = moscow.tarjan1<string> "tarjan msg" (Some pof) 2 (nodes, edges)
    vprintln 0 (i2s (length sccs) + sprintf " SCCs have been found.")
    app (fun q -> vprintln 0 ("\n\n------------------no-of-members=" + i2s(length q) + " ----\n" + sfoldcr pof q + "---------\n")) sccs
    ()


let dotrep ww keyname nodes edges = 
    let keyname = filename_sanitize ['_'; '.'] keyname
    let title = keyname
    let ww = WF 3 "render_dot_plot" ww (sprintf "Start %i plots" (length edges))
    let ww = WF 2 "rtl_resynth" ww (sprintf "Writing a dot report.  keyname=%s" keyname)

    let nodes =
        let ngen (name, legend, iof) =
            let (colour, shape) = if iof then ("red", "box") else ("black", "ellipse")
            DNODE_DEF(name, [("label", legend); ("color", colour); ("shape", shape)])
        map ngen (list_once nodes)

    let edges =
        let decorate ((dest, _), (src, _)) =
            DARC(DNODE(src, ""), DNODE(dest, ""), [  ("color", "blue");  ])
        map decorate (list_once edges)

    let filename = "structure_" + keyname + ".dot"
    let fd = yout_open_log filename
    yout fd (report_banner_toStr "// ")

    let data = nodes @ edges 
    dotout fd (DOT_DIGRAPH("rtl_resynth_" + keyname, data))
    yout_close fd
    ()



let matrix_iterator ww powers_db msg ffun aux = 
    let _:net_powers_db_t = powers_db
    let itpull1 xidx (vv:net_power_list_t) =
            let (ff, f2) = valOf_or_fail "no such net" (lookup_net_by_xidx_o xidx)
            let lhs = X_bnet ff
            let ranger00 = (encoding_width lhs - 1, 0)
            vprintln 2 (sprintf "%s: %i powers stored for net %s.  " msg vv.KeyCount ff.id)
            for k in vv do ffun aux ff f2 lhs ranger00 k.Key k.Value done
    for k in powers_db do itpull1 k.Key k.Value done


// TODO handle more than one clock domain ...

let is_io = function
    | X_bnet ff ->
        let (ff, f2) = valOf_or_fail "rtl_resynth pza" (lookup_net_by_xidx_o ff.n)
        varmode_is_io f2.xnet_io
    | other ->
        sf("Other form in is_io" + xToStr other)
                    
let matrix_anal ww powers_db eE keyname =
    let ww = WF 2 "matrix_anal" ww (sprintf "Start %s" keyname)
    let dP = new_dP()
    // Analysis ignores vectorised shift register serialisation ... ..
    let (nodes, edges) = // anal: construct a directecd graph: nodes are register/net groups that have a combinational coupling. Arcs arise from sequential assignments between groups
        let combgroups = new EquivClass<int>("cnode")
        let (m_nodes, m_edges) = (ref [], ref [])
        let dummyf _ _ _ = ()
        let pull_comb (pass) ff f2 lhs (ranger00:int *int) power lst =
            if power=0 then //
                let get_comb cc (ranger_, gg, rhs) = 
                    let support_list = xi_support dP [] rhs
                    let support_list = xi_bsupport dP support_list gg
                    let (inputs, locals) = groom2 (fst>>is_io) support_list
                    dev_println(sprintf "combinational_node_form  %s  -> %A" (xToStr (X_bnet ff)) (map (fst>>xToStr) locals))
                    if not_nullp inputs then // Assume no inouts and an output cannot be in support.
                        hpr_yikes(sprintf "Unreported combinational arc(s)x %s <--- %s" (xToStr (X_bnet ff)) (sfold (fst>>xToStr) inputs))
                    List.fold (fun cc (net, lanes_) -> singly_add (x2nn net) cc) cc locals
                let lst = List.fold get_comb [ff.n] lst
                let _ = combgroups.AddFreshClass dummyf lst
                ()
        matrix_iterator ww powers_db "pull_anal-0" pull_comb (0)

        
        let pull_anal (pass) ff f2 lhs (ranger00:int *int) power lst =
            if power=1 then //
                let dest_n = combgroups.AddMember ff.n
                let get_seq cc (ranger_, gg, rhs) = 
                    let support_list = xi_support dP [] rhs
                    let support_list = xi_bsupport dP support_list gg
                    dev_println(sprintf "Sequential support for %s/%A  is %A" (xToStr (X_bnet ff)) dest_n (map (fst>>xToStr) support_list))
                    let gsl cc (net, lanes_) =
                        let shig = combgroups.AddMember(x2nn net)
                        //dev_println (sprintf "   shig %s/%i   %s" (xToStr net) (x2nn net) shig)
                        singly_add (shig) cc 
                    List.fold gsl cc support_list
                let src_ns = List.fold get_seq [] lst
                dev_println(sprintf "Sequential dependency for %s/%A  <--: %A" (xToStr (X_bnet ff)) dest_n  (src_ns))
                app (fun src -> mutadd m_edges ((dest_n, ""), (src, ""))) src_ns
        matrix_iterator ww powers_db "pull_anal-1" pull_anal (1)        

        let pza key (_, items) =
                let pzb (cc, iof) xidx =
                    let (ff, f2) = valOf_or_fail "rtl_resynth pza" (lookup_net_by_xidx_o xidx)
                    let iof = varmode_is_io f2.xnet_io || iof
                    (ff.id + " " + cc, iof)
                let (wrapped, iof) = List.fold pzb ("", false) items
                mutadd m_nodes (key, string_autoinsert_newlines 55 wrapped, iof)
            
        for k in combgroups do pza k.Key k.Value done
        (!m_nodes, !m_edges)

    // Dot report from above anal
    let _ =
        dotrep ww keyname nodes edges

    // SCC find
    let _ = scc_find ww keyname nodes edges  // Find strongly-connected components 
    ()



// Simple blanket render out again.
let resynth_full_roundtrip_render ww powers_db eE () =
    let newid = funique "rtl-roundtripped"
    let ww = WF 2 "resynth_full_roundtrip_render" ww (sprintf "Start %s" newid)
       

    let ans =
        let m_cs = ref []
        let (n_seq, n_comb) = (ref 0, ref 0)
        let simple_puller (_) ff f2 lhs (ranger00:int *int) power lst =
            vprintln 2 (sprintf "  --- %i entries for power %i of %s [%i:%i]:  " (length lst) power (netToStr lhs) (fst ranger00) (snd ranger00))

            let apply_range ranger ranger00 lhs =
                if ranger = ranger00 then lhs
                else
                    //vprintln 0 (sprintf "Ranger %A not %A" ranger ranger00)                
                    ix_bitinsert (fst ranger) (snd ranger) lhs

            match power with
                | 0 -> // Combinational 
                    let diog (ranger, gg, rhs) =
                        let lhs = apply_range ranger ranger00 lhs
                        mutadd m_cs (Rarc(gg, xi_X -1 lhs, rhs))
                    mutinc n_comb 1
                    app diog lst
                | 1 -> // Registered once.
                    let diog (ranger, gg, rhs) =
                        let lhs = apply_range ranger ranger00 lhs
                        mutadd m_cs (Rarc(gg, lhs, rhs))

                    mutinc n_seq 1
                    app diog lst
                | nn -> sf "other power"


        matrix_iterator ww powers_db "simple_puller" simple_puller (0)
        vprintln 3 (sprintf "Extracted %i seq and %i comb assign pairs."  !n_seq  !n_comb)
        !m_cs

    if nullp ans then hpr_yikes(sprintf "%s: rtl_resynth_ all missing - but was there any to start with?" newid)
    let ii = { id=newid }:rtl_ctrl_t
    let ans = SP_rtl(ii, [ XRTL(None, X_true, ans) ])
    let newnets = []
    (ans, newnets)




let rec rebuild_bits ranger00 backstop = function
    | [] -> backstop
    | (ranger, gg, rhs)::tt  ->
            let backstop = rebuild_bits ranger00 backstop tt
            let rhs = 
                if ranger = ranger00 then rhs
                else
                    let mask arg width baser = ix_bitand arg (ix_lshift (gec_X_bnum({widtho=Some(width+baser); signed=Unsigned}, himask width)) (xi_num baser))
                    let ((conc_h, conc_l), (wanted_h, wanted_l)) = (ranger, ranger00)
                    vprintln 0 (sprintf "rebuild: Ranger %A not %A" ranger ranger00)                

                    let top = if (conc_h > wanted_h) then mask backstop (conc_h-wanted_h) (wanted_h+1) else xi_num 0
                    let bot = if (conc_l < wanted_l) then mask backstop (wanted_l-conc_l) (wanted_l-1) else xi_num 0                    
                    let rhs =
                        let width = wanted_h-wanted_l + 1
                        ix_bitand rhs (gec_X_bnum({widtho=Some width; signed=Unsigned}, himask width))
                    let mid = ix_lshift rhs (xi_num wanted_l)
                    ix_bitor (ix_bitor top mid) bot

            ix_query gg rhs backstop


// Simple cone-directed render
let resynth_coned_render ww powers_db eE targets0 =
    let newid = funique "rtl-coned"
    let ww = WF 2 "resynth_coned_render" ww (sprintf "Start %s" newid)
    let _:net_powers_db_t = powers_db        
    let dP = new_dP()
    let m_bs = ref []

    let rec buildcone dones = function
        | [] -> ()
        | hid::tt when memberp hid dones -> buildcone dones tt
        | hid::tt ->
            vprintln 3 (sprintf "cone-directed render: build for %s" hid)
            match lookup_net_by_string hid with 
                | None -> sf("rtl_resynth: meox: cannot find net " + hid)
                | Some(X_bnet ff) ->
                    let dones = hid :: dones
                    let backstop = X_bnet ff
                    let ranger00 = (encoding_width (X_bnet ff) - 1, 0)
                    match powers_db.lookup ff.n with
                        | None ->
                            // ok if an input
                            vprintln 0 ("rtl_resynth: cannot find any drivers for net " + netToStr(X_bnet ff)) 
                            buildcone dones tt

                        | Some powers ->
                            let scanner cc power vlst =
                                let xv = rebuild_bits ranger00 backstop vlst
                                let support = map fst (xi_support dP [] xv)
                                dev_println (sprintf "scanner: %s: power=%i  xv=%s" hid power (xToStr xv))
                                dev_println (sprintf "support is " + sfold xToStr support)
                                mutadd m_bs (power, ranger00, X_bnet ff, xv)
                                lst_union (map xToStr support) cc
                            let further = powers.fold scanner []
                            buildcone dones (lst_union further tt)

    let _ = buildcone [] targets0
    let ans =
        let m_cs = ref []
        let (n_seq, n_comb) = (ref 0, ref 0)
        let doz2 (power, ranger00, lhs, rhs) = 
        //vprintln 2 (sprintf "  --- %i entries for power %i of %s [%i:%i]:  " lst) power (netToStr lhs) (fst ranger00) (snd ranger00))


            match power with
                | 0 -> // Combinational 
                    mutadd m_cs (Rarc(X_true, xi_X -1 lhs, rhs))
                    mutinc n_comb 1
                | 1 -> // Registered once.
                    mutadd m_cs (Rarc(X_true, lhs, rhs))
                    mutinc n_seq 1
                | nn -> sf "doz2: other power"

        app doz2 !m_bs
        vprintln 3 (sprintf "Extracted %i seq and %i comb assign pairs."  !n_seq  !n_comb)
        !m_cs

    if nullp ans then hpr_yikes(sprintf "%s: rtl_resynth_ all missing - but was there any to start with?" newid)
    let ii = { id=newid }:rtl_ctrl_t
    let ans = SP_rtl(ii, [ XRTL(None, X_true, ans) ])
    let newnets = []
    (ans, newnets)

type retoned_t =
    | CST_comb of int * (int * int) * hexp_t * hexp_t
    | CST_seq of hexp_t * int * (int * int) * hexp_t * hexp_t     

// Re-timed rebuild
let resynth_retimed_cone ww powers_db eE targets0 =
    let newid = funique "rtl-coned"
    let ww = WF 2 "resynth_retimed_cone" ww (sprintf "Start %s" newid)
    let _:net_powers_db_t = powers_db        
    let dP = new_dP()
    let m_bs = ref []

    let m_newnets = ref []
    let gec_power_newnet tp target =
        if tp=0 then target
        else
            let ans = clonenet "" (sprintf "__p%i" tp) target // TODO save cloned-net for net list.
            mutadd m_newnets ans
            ans
            
    let back_cvt = function // Reverse of gec_power_newnet
        | X_bnet ff ->
            let ans = ff.id.IndexOf "__p"
            if ans < 0 then (ff.id, 0)
            else
            sf(sprintf "back cvt %s  %i" ff.id ans)
            
    let rec buildcone dones = function
        | [] -> ()
        | (hid, tp)::tt when memberp (hid, tp) dones -> buildcone dones tt
        | (hid, tp)::tt ->
            dev_println (sprintf "start build for %s tp=%i" hid tp)
            match lookup_net_by_string hid with 
                | None -> sf("rtl_resynth: meox: cannot find net " + hid)
                | Some(X_bnet ff) ->
                    let basenet = X_bnet ff
                    let ranger00 = (encoding_width basenet - 1, 0)
                    let dones = (hid, tp) :: dones
                    match powers_db.lookup ff.n with
                        | None ->
                            // ok if an input
                            vprintln 0 (sprintf "rtl_resynth: cannot find any drivers for tp=%i of net %s" tp (netToStr basenet))
                            buildcone dones tt

                        | Some powers ->
                            dev_println (sprintf "scanner: %s: wanted_power=%i  available=%s" hid tp (sfold i2s (powers.fold (fun cc k v -> k::cc) [])))


                            let (further) = 
                                match powers.lookup tp with
                                    | []   ->
                                        let avail = rev(List.sort (powers.fold (fun cc k v -> k::cc) []))
                                        if nullp avail then sf("no powers available afterall for net " + hid) // Should be trapped above in powers_db=None.
                                        // Sort, with highest existing power first.
                                        // It's possible there's some gaps and the one we want is in the gap.
                                        if tp=0 && hd avail = tp + 1 then // other +ve values also easy to do.
                                            // If an earlier version (higher power is available) we can just shift-register it.
                                            // OR shift register its support ... any pipeline migration scheme works.
                                            let tp2 = hd avail
                                            let newnet = gec_power_newnet tp basenet 
                                            powers.add tp (ranger00, X_true, newnet)  // Generate a new net and a broadside and add its input support to the further work.
                                            let vlst = powers.lookup (tp+1)
                                            if nullp vlst then sf(sprintf "missing vlst for %s at %i" (xToStr basenet) (tp+1))
                                            let xv = rebuild_bits ranger00 newnet vlst
                                            //let furthers xv = temp_abstract tp basenet vlst 
                                            // Can rely on sub-expression pin detection or manually do it here.
                                            mutadd m_bs (CST_seq(newnet, tp2, ranger00, basenet, xv))
                                            ([(hid, tp2)])  // Return as a furthers to ensure it gets built.
                                        else
                                         muddy(sprintf "missing synth tp=%i %s needed. Avail=%s" tp (hid)  (sfold i2s avail)) // Assumes all nets have a driver
                                        
                                    | vlst -> 
// Assumption of built vs cone-refined for render ... not on done if already copied to output
                                        let temp_abstract tp basenet vlst =
                                            let backstop = gec_power_newnet (tp-1) basenet // Backstop is from gen before.
                                            let xv = rebuild_bits ranger00 backstop vlst
                                            let support = map fst (xi_support dP [] xv)
                                            dev_println (sprintf "temp_abstract: %s: power=%i  xv=%s" hid tp (xToStr xv))
                                            dev_println (sprintf "support is " + sfold xToStr support)
                                            mutadd m_bs (CST_comb(tp, ranger00, basenet, xv))
// We;ll end up with int tp applied to string tp
                                            let further = map (fun support -> back_cvt support) support
                                            (further)


                                        let further = temp_abstract tp basenet vlst 
                                        (further)
                            buildcone dones (lst_union further tt)

    let addn hid = (hid, 0)
    let _ = buildcone [] (map addn targets0)

    let ans =
        let m_cs = ref []
        let (n_seq, n_comb) = (ref 0, ref 0)
        let doz2 = function
            | CST_comb(tp, ranger00, basenet, rhs) ->
                vprintln 2 (sprintf "CST_comb:   %i of %s [%i:%i]:  " tp (netToStr basenet) (fst ranger00) (snd ranger00))
                let lhs =
                    if tp=0 then basenet
                    else gec_power_newnet tp basenet
                mutadd m_cs (Rarc(X_true, xi_X -1 lhs, rhs))
                mutinc n_comb 1
                    
            | CST_seq(lhs, tp, ranger00, basenet, rhs) ->
                vprintln 2 (sprintf "CST_seq setup (tp=%i) %s[%i:%i]:  " tp (netToStr lhs) (fst ranger00) (snd ranger00))
                mutadd m_cs (Rarc(X_true, lhs, rhs))
                mutinc n_seq 1

        app doz2 !m_bs
        vprintln 3 (sprintf "Extracted %i seq and %i comb assign pairs."  !n_seq  !n_comb)
        !m_cs

    if nullp ans then hpr_yikes(sprintf "%s: rtl_resynth_ all missing - but was there any to start with?" newid)
    let ii = { id=newid }:rtl_ctrl_t
    let newnets = list_once !m_newnets
    let ans = SP_rtl(ii, [ XRTL(None, X_true, ans) ])
    (ans, newnets)


let rec resynth_h2sp_insert ww vd dir powers_db eE arg cc =
    match arg with
        | SP_l(ast_ctrl, bev) ->
            resynth_xbev_insert ww powers_db eE ast_ctrl bev
            cc

        | SP_par(pstyle, lst) ->
            let _ = map (resynth_h2sp_insert ww vd dir powers_db eE) lst
            //SP_par(pstyle, ans_)
            cc

        | SP_seq(lst) -> // Seq semantic is discarded!
            let ans _ = map (resynth_h2sp_insert ww vd dir powers_db eE) lst
            //SP_seq(ans_)
            cc

        | SP_rtl(ii, rtl)        ->
            let pass_ons = resynth_rtl_insert ww vd powers_db eE rtl []
            if nullp pass_ons then cc else SP_rtl(ii, pass_ons)::cc


        | SP_dic(dic, ctrl) -> 
            let dic' = Array.create dic.Length (Xskip)
            let ww' = WN ("SP_dic " + ctrl.ast_ctrl.id) ww 
            let xlat b =
                let ast_ctrl = muddy "dic_ctrl_to_ast_ctrl ctrl"
                let ans = resynth_xbev_insert ww' powers_db eE ast_ctrl b
                muddy "DIC structure is lost ... not worthwhile... ?"
            let _ =
                let jresynth p = Array.set dic' p (xlat dic.[p])
                app jresynth [0..dic.Length-1]
            //SP_dic(dic', ctrl)
            cc

        | SP_comment ss -> // Can nicely pass comments through ?
            arg::cc

        //| SP_asm _ -> arg
        | other  -> muddy("resynth_h2sp other form:" + hprSPSummaryToStr other)


let resynth_dir ww eE dir =
    //muddy (sprintf "Dir=%A" dir)
    dir


let resynth_insert_exec ww vd powers_db m_dir eE (H2BLK(dir, sp_in)) cc =
    let ww = WF 2 "rtl_resynth" ww (sprintf "resynth_insert_exec: Start exec block")

    if nullp dir.clocks then ()
    else
        if not_nonep !m_dir  && valOf !m_dir <> dir then muddy "Two clock domains"
        m_dir := Some dir // Crude --- multiple clock domains risk!
    let passons = resynth_h2sp_insert ww vd dir powers_db eE sp_in []
    let ww = WF 2 "rtl_resynth" ww (sprintf "rsynth_insert_exec: Finished exec block")    
    let gen sp = H2BLK(dir, sp)
    (map gen (passons @ [])) @ cc

    
        
let rtl_resynth_main ww op_args control vms =
    let ww = WF 1 "rtl_resynth" ww (sprintf "Start %i machines" (length vms))

    let resynth ww = function
        | (ii, Some(HPR_VM2(minfo, decls, sons, execs, assertions))) when ii.definitionf ->
            let minfo = minfo //
            let ii = { ii with generated_by=op_args.stagename  }    
            let eE = [] // for now ... object record.
            let vd = 3
            let powers_db = new net_powers_db_t("net-powers-db") // Can be stored inside eE perhaps.
            let m_dir = ref None
            let execs = List.foldBack (resynth_insert_exec ww vd powers_db m_dir eE) execs []

            matrix_anal ww powers_db eE "raw"

            let (sp_out, newnets) =
                if true then resynth_retimed_cone ww powers_db eE [ "serout"]
                elif true then resynth_coned_render ww powers_db eE [ "serout"]                 
                else resynth_full_roundtrip_render ww powers_db eE ()
            let dir = resynth_dir ww eE (valOf_or_fail "no clock domain" !m_dir)
            let gen_sp = H2BLK(dir, sp_out)

            let newdec = // Declare new nets
                let cpi = { g_null_db_metainfo with kind= "rtl-resynth-nets" }
                gec_DB_group(cpi, map db_netwrap_null newnets)

            let ans = (ii, Some(HPR_VM2(minfo, newdec @ decls, sons, gen_sp :: execs, assertions)))
            ans
        | other -> other             

    let vms = map (resynth ww) vms
    vms


    

let rtl_resynth_argpattern =
   [
       Arg_enum_defaulting("rtl_resynth-dump-straightaway", ["enable"; "disable"], "disable", "Enable Kernighan and Ritchie style (pre-2001) Verilog port syntax");       
   ] 
 
// This is the top-level instance of this plugin
let opath_rtl_resynth ww op_args vms_in =
    let c1:control_t = op_args.c3
    let stagename = op_args.stagename
    let disabled = (cmdline_flagset_validate stagename ["enable"; "disable" ] 0 c1 = 1)
    let ww = WF 1 "cv3" ww "Starting"


    //vprintln 2 (bevelab_banner)
    if disabled
    then
        vprintln 1 "rtl_resynth: Stage is disabled"
        vms_in
    else                
        //let zz = cmdline_flagset_validate "skip-propagate" ["false"; "true" ] 0 c1
    let dump_straightaway = Some "dropping2"
    let vms_out = rtl_resynth_main ww op_args c1 vms_in
    let ww = WF 1 "cv3" ww "Finished"        
    vms_out
    


let install_rtl_resynth() =
    let op_args__ =
            { stagename=         "rtl_resynth"
              banner_msg=        "rtl_resynth"
              argpattern=        rtl_resynth_argpattern
              command=           None //"rtl_resynth"
              c3=                []
            }
    install_operator ("rtl_resynth",  "Various maniplulations of RTL designs held in hbev form.", opath_rtl_resynth, [], [], rtl_resynth_argpattern);        

// eof

