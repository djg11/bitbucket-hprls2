// rtl_repipeline.fs
// (C) 2022 DJ Greaves, University of Cambridge, Computer Laboratory.
//

//   rtl_repipeline -- Further maniplulations of RTL designs held in hbev form.

module rtl_repipeline

open Microsoft.FSharp.Collections
open System.Collections.Generic


open yout
open moscow
open verilog_hdr
open meox
open hprls_hdr
open abstract_hdr
open verilog_gen
open verilog_render
open opath_hdr
open abstracte
open cxverhdr
open dotreport


let quadToStr (px, l, r, _) = xToStr l + sprintf "<-%i-" px + xToStr r

//
let g_local_clk_to_q_time = 0.25


//
// Perform worst path timing analysis for a pool of logic.  Targetless and targetfull variants are possible ?
// If sources are provided, they are guaranteed and outside of our analysis here
//           
type logict_t = OptionStore<hexp_t, int * hexp_t * hexp_t * float>
let gen_logict_db ww key msg logic_pool =
    let (m_combs, m_seqs) = (ref 0, ref 0)

    let logict = new logict_t("logic store") // Delays in this one are old and ignored
    let insert_logic q =
            let (px, lhs, rhs, _) = q
            if px=0 then mutinc m_combs 1
            elif px=1 then mutinc m_seqs 1
            logict.add lhs q
    app insert_logic logic_pool
    vprintln 3 (sprintf "%s: %s: %i seqs, %i combs." msg key  !m_seqs !m_combs)
    logict


// Delay of an expression beyond that of its latest input arrival time.
let delof r =
    if xi_constantp r then 0.0
    else
        match r with
            | X_bnet _ -> 0.0  // A comb assign of another net has zero logic delay. Ditto some shifts!  Better to use full xi_cost please ..
            | _        -> 1.0  // All other logic functions...

let ap_timing_anal ww eE key logic_pool sources targets_o =
    let vd = 3 // The default
    let ww = WN "ap_timing_anal" ww
    let dP = new_dP()
    let tres = new OptionStore<xidx_t, float>("ap_timing_anal")
    let (m_csv_lines, dname) = eE
    let targets =
        match targets_o with
            | Some targets ->
                let pred (px, lhs, rhs, _) = memberp lhs targets
                List.filter pred logic_pool
            | None -> logic_pool

    let msg = sprintf "ap_timing_anal: %s: targets %i" (if nonep targets_o then "all" else "selected") (length targets)
    let logict = gen_logict_db ww key msg logic_pool




    // When px=0, the fourth field is the time of both the lhs and the rhs, but is the time of just the rhs when px=1

    let is_seq_net net =
        match logict.lookup net with
            | None -> false
            | Some(px, lhs, rhs, _) -> (px=1)


            
    let rec ap_tget (px, lhs, rhs, ov_) =
        let rhs_tv = ap_tget_exp 0 rhs
        // Ignore any old tv in input data and replace with newly computed tv
        (px, lhs, rhs, rhs_tv)

    and ap_tget_exp depth (exp:hexp_t) = 
        if (depth > 500) then hpr_yikes("L75 suspected comb loop on " + netToStr exp)
        if (depth > 1000) then sf ("L76 comb loop on " + netToStr exp)
        let kk = (x2nn exp)
        match tres.lookup kk with
            | Some ans -> ans
            | None ->
                let ans = 
                    match exp with
                        | (X_bnet ff) as net ->
                            match logict.lookup net with                    
                                | Some((px, lhs, rhs, _) as q) ->
                                    //dev_println ("At " + quadToStr q)
                                    if px = 1 then g_local_clk_to_q_time else (delof rhs + ap_tget_exp (depth+1) rhs)
                                | None ->
                                    match op_assoc exp sources with // exp might one day be in the sources but not a net.
                                        | Some del -> (-12345678.0) // 
                                        | None ->
                                            if xi_is_input net then (0.35) // g_default_input_ready_time
                                            else
                                                hpr_yikes(sprintf "%s:%s: L94: not a source or free input and no logic for " dname key + netToStr net)
                                                0.55 // Arbitrary result!
                        | exp ->
                            match op_assoc exp sources with // exp might one day be in the sources but not a net.
                                | Some del -> (-12345678.0) // 
                                
                                | None ->
                                    let support = map fst (xi_support dP [] exp)
                                    let ch net1 =
                                        //dev_println ("At net1=" + xToStr net1)
                                        (ap_tget_exp (depth+1) net1)
                                    let times = map ch support
                                    //vprintln 0 (sprintf "Times for %s are " (xToStr rhs) + sfold (sprintf "%.1f") times)    
                                    let rhs_tv = List.fold (max) 0.0 times // TODO assumes zero or one level of logic in exp TODO use cost function
                                    if vd >= 5 then vprintln 5 (sprintf "res.add  %s is  %A" (xToStr exp) rhs_tv)
                                    rhs_tv
                tres.add kk ans
                ans

    let times4 = map (ap_tget) targets // Populate tres with most useful timings and fill in the initial timings in the logic quads.

    let timing_lookup msg arg =
        let tv = ap_tget_exp 0 arg
        match tv with
            | lhs_tv ->
                dev_println (sprintf "timing_lookup of %s is %A" (xToStr arg) lhs_tv)
                Some lhs_tv

    let nets_to_report =
        if nonep targets_o then
            let get_seq_d_time cc (px, lhs, rhs, tv) =
                if px = 0 then cc // Ignore combs
                else
                    vprintln 1 (sprintf "  Seq D time for %s is %.2f" (netToStr lhs) tv) // We don' include any set-up time.
                    (lhs, tv)::cc
            List.fold get_seq_d_time [] times4
        else
            let get_target_time net =
                let lv = ap_tget_exp 0 net
                (net, lv) 
            map get_target_time (valOf targets_o)

    let (max_time, max_net) =
        let gag (cc:float, whom) (lhs, tv) = if tv > cc then (tv, lhs) else (cc, whom)
        List.fold gag (0.0, X_undef) nets_to_report

    vprintln 1 (sprintf "%s: Max seq time, MaxTime is %.1f for %s net" key max_time (netToStr max_net))

    let (m_csv_lines, dname) = eE
    mutadd m_csv_lines (key + ":MaxTime", sprintf "%.1f" max_time) // This key ignores targets set


    (max_time, max_net, logic_pool, timing_lookup) // Annotated variant of pool wanted really?

//
//
//   
let balanced_migrate ww eE m_newnets migration_targets migration_sources pre_pool =
    let vd = 4 // 3 is the default
    let ww = WF 2 "balanced_migrate" ww "Starting"
    let (m_csv_lines, dname) = eE
    if vd>= 4 then
        reportx 4 "balanced_migrate: Migration Targets" netToStr migration_targets
        reportx 4 "balanced_migrate: Migration Sources" (fun (net, del) -> sprintf "%.1f %s" del (netToStr net))  migration_sources
        reportx 4 "pre_pool"  (fun x -> " PreP " + quadToStr x) pre_pool

    // Find where to split:
    let (max_time, max_net, logic_pool, timing_lookup) = ap_timing_anal ww eE "pre_pool" pre_pool migration_sources (Some migration_targets)

    let time_to_slice = max_time / 2.0
    vprintln 1 (sprintf "%s: Aim to balance %.2f into 2 x %.2f" dname max_time time_to_slice)

    let savenet net =
        mutaddonce m_newnets net
        net
    let dP = new_dP()

    let adv expr =
        let sup = map fst (xi_support dP [] expr)
        let mapping =
            let pairgen net = (net, savenet(clonenet "" (sprintf "_adv") net))
            
            map pairgen sup
        let expr' = xi_assoc_exp g_sk_null (makemap mapping) expr
        if vd>=4 then vprintln 4 (sprintf "adv:  %s -> %s"  (netToStr expr) (netToStr expr'))
        (expr', mapping)

    let marker = gec_X_net "MARK"

    let (skippedf, post_targets, post_pool) =

        if (time_to_slice < 2.0 || max_net = X_undef) then
            vprintln 1 (sprintf "%s: Skipping trival rebalance." dname)
            
            (true, migration_targets, pre_pool)
        else 
            let msg = "balanced_migrate"
            let m_limiter = ref 0
            let logict = gen_logict_db ww "gen4" msg logic_pool
            let m_recoded_targets = ref []
            let stage1 cc net =
                let rec stage1_serf del stack topf cc net = 
                    if vd>= 4 then vprintln 4 (sprintf "stage1 net %.2f %s" del (xToStr net))
                    match logict.lookup net with // Its a source or else its locally provided
                        | None ->
                            match op_assoc net migration_sources with
                                | None ->
                                    vprintln 0 (sprintf "%s: L207: no net %s" dname (xToStr net))
                                    if xi_is_input net then cc
                                    else sf ("Needed it. L207")
                                | Some _ ->
                                    if topf then mutadd m_recoded_targets (net, net)
                                    cc     // No logic needed if it is a source
                        | Some((px, l, r, _) as q) ->
                            if (px=1) then
                                let cc = singly_add q cc  // It's already pipelined.  Proceed directly to stage 3
                                if topf then mutadd m_recoded_targets (net, r)
                                let sup = map fst (xi_support dP [] r)
                                List.fold (stage3_serf (0.0)) cc sup  // Can optimise in a few places: if a quad was already present in cc then likely its support was surely already added too?

                            elif px <> 0 then sf "L216"
                            else
                                let default_comb_action() = 
                                    let cc = singly_add q cc
                                    let sup = map fst (xi_support dP [] r)
                                    if topf then mutadd m_recoded_targets (net, r)
                                    List.fold (stage1_serf (del + delof r) (l::stack) false) cc sup

                                if (del >= time_to_slice) then
                                    if vd>= 4 then vprintln 4 (sprintf "Decide to pipeline this comb: " + quadToStr q)
                                    let (r', sup_pairs) = adv r // recode r to use new nets and return its support. 
                                    let cc = singly_add (1, l, r', -1.0) cc
                                    let trial = List.fold (stage2_serf (0.0) (marker :: l ::stack)) (Some cc) sup_pairs
                                    if nonep trial then default_comb_action()
                                    else
                                        if topf then mutadd m_recoded_targets (net, r')
                                        valOf trial
                                else default_comb_action()

                and stage2_serf del stack cco (old_net, new_net) = // We must now convert a 1 to a zero in all support paths
                    if vd>= 4 then vprintln 4 (sprintf "stage2 net %.2f %s" del (xToStr net))
                    if nonep cco then None
                    else
                    match logict.lookup old_net with // Its a source or else its locally provided
                        | None -> 
                            match op_assoc old_net migration_sources with
                                | None ->
                                    vprintln 0 (sprintf " Trace " + sfold xToStr stack)
                                    vprintln 0 (sprintf "%s: L231: no migration source for net %s" dname (xToStr old_net))
                                    sf ("Needed it. L231")


                                | Some _ ->
                                    mutinc m_limiter 1
                                    if (!m_limiter < 10000) || ((!m_limiter % 1000)=999) then
                                        vprintln 3 (sprintf " Trace " + sfold xToStr stack)
                                        vprintln 3 ("stage2: Path for splitting is not possible. SCC? Cannot get an earlier version of " + netToStr new_net + " by looking up " + xToStr old_net)
                                    None // Have failed, so return None to stage1 and it will take default action. It likely will try again at a lower level and fail again which would be nice to shortcut.
                        | Some((px, l, r, _) as q) ->
                            if px=0 then
                                let (r', sup_pairs) = adv r
                                let cco = Some(singly_add (0, new_net, r, -1.0) (valOf cco))
                                List.fold (stage2_serf (del + delof r) (l::stack)) cco sup_pairs  

                            elif (px=1) then // Success, we can change this assignment and have finished the work, but will collect the old support to save having to do it afterwards.
                                let cc = singly_add (0, new_net, r, -1.0) (valOf cco)
                                let sup = map fst (xi_support dP [] r)
                                let ans = List.fold (stage3_serf (g_local_clk_to_q_time)) cc sup  // Can optimise in a few places: if a quad was already present in cc then likely its support was surely already added too?
                                Some ans
                            else sf "L256px"
                and stage3_serf del cc net = // We here collect unmodified further support
                    if vd>=4 then vprintln 4 (sprintf "stage3 net %.2f %s" del (xToStr net))
                    match logict.lookup net with // Its a source or else its locally provided
                        | None -> 
                            match op_assoc net migration_sources with
                                | None -> sf (sprintf "L245: stage 3: no net %s" (xToStr net))
                                | Some _ -> cc
                        | Some((px, l, r, _) as q) ->
                            let cc = singly_add q cc
                            if (px = 1) then muddy "cc double pipeline" // I guess this is correct? Will be encounted with double pipelines which we'll forget for now.
                            elif (px <> 0) then sf "L250"
                            else
                                let sup = map fst (xi_support dP [] r)
                                List.fold (stage3_serf (del+ delof r)) cc sup  
                vprintln 0 (sprintf "\nCommence migrate for root %s" (netToStr net))
                stage1_serf 0.0 [] true cc net

            let post_pool =
                //map (stage1) migration_targets
                let rec operate cc = function
                    | target::tt ->
                        let (cc) = stage1 cc target
                        operate cc tt
                    | [] -> cc
                operate [] migration_targets // Just a fold now actually.

            if vd>= 4 then reportx 4 "post_pool"  (fun x -> " PostP " + quadToStr x)  post_pool
            let post_logict = gen_logict_db ww "gen4_post" msg post_pool

            let post_targets = rev !m_recoded_targets // Built up backwards
            if length post_targets <> length migration_targets then
                sf "target lost"
                


            (false, map snd post_targets, post_pool)

            
    vprintln 3 (sprintf "Skipped=%A. Comparing quads in post_pool %i  with pre_pool %i" skippedf (length post_pool) (length pre_pool))

    if (skippedf) then
        mutadd m_csv_lines ("Rebal", "Skipped")
    else
        if vd>= 4 then
            //reportx 4 "balanced_migrate: Procssed Targets" (fun (net, net1) -> sprintf " A=%s B=%s" (xToStr net) (netToStr net1)) post_targets
            reportx 4 "balanced_migrate: Processed Targets" (fun (net) -> sprintf " A=%s " (xToStr net)) post_targets            
            reportx 4 "balanced_migrate: post_pool"  (fun x -> " PostP2 " + quadToStr x)  post_pool


        let _ = ap_timing_anal ww eE "post_pool" post_pool migration_sources (Some migration_targets)
        ()
    let ww = WF 2 "balanced_migrate" ww "Finished"
    (post_targets, post_pool)

// Classical D-type migration: migrate forward or migrate backwards:
//
// Migrate backward appropriate to be applied to a D-type on the output of a function.
//   (1, Y, A&B) -> (0, Y, Ar & Br);  (1, Ar, A); (1, Br, B)  # Ar and Br introduced if not already present
//
// Migrate forward is applied to D-types near the input to a function.
//    (0, Y, Ar & Br);  (1, Ar, A); (1, Br, B) -> (1, Y, A&B) # Ar and Br perhaps no longer needed and can be trimmed.

// A sequential loop is manifested by, eg
//  e.g   (0, C, A&Y); (1, A, Z^B); (1, B, ~C)  ->
// we migrate (1, C, X(A) & X(Y))  Let Ax = X(A) etc, then we now need (0, Ax, Z^B) and assume we can get Y, an earlier copy of the input Y.
    
// For the SPXform, the Q register is introduced and then needs to be forward migrated.

// ========================================================================================

type net_range_t = int * int // High and low bit positions stored.
type net_power_list_t = ListStore<int, net_range_t * hbexp_t * hexp_t>            // Guards (cen) and expression stored.
type net_powers_db_t = OptionStore<xidx_t, net_power_list_t>        // Indexed by net memo nn.

  

//
// The X0 site refers to the current value of a variable and the X1 site holds an expression for its the next value.
//
let power_insert_factory ww eE powers_db =
    let vd = 5 // Three (3) is the default
    let _:net_powers_db_t = powers_db
    let (m_csv_lines, dname) = eE

    let get_power_list xidx =
        match powers_db.lookup xidx with
            | Some pwl -> pwl
            | None ->
                let pwl = new net_power_list_t("powers-for-" + i2s xidx)
                powers_db.add xidx pwl
                pwl

    let insert_power net power gg exp =
        if vd>=4 then vprintln 4 (sprintf "%s: insert_power %i g=%s    lhs=%s  rhs=%s" dname power (xbToStr gg)  (netToStr net) (xToStr exp))
        match net with
            | X_bnet ff ->
                let ranger = (encoding_width net - 1, 0)
                let power_list = get_power_list ff.n
                match power_list.lookup power with
                    | [] ->
                        power_list.add power (ranger, gg, exp)
                    | ovl ->
                        // Existed already ... no probs - disjoint guards we trust .... should check as best we can.
                        power_list.add power (ranger, gg, exp)                            
                        ()

            | W_node(prec, V_cast CS_maskcast, [W_node(_, V_rshift _, [X_bnet ff; amount], _)], _) -> // Occurs on lhs for bit-insert operations.
                let msg = "rtl_repipeline: insert_power: reveng bit-insert 1/2" 
                let power_list = get_power_list ff.n
                let amount = xi_manifest64 msg amount 
                let (width, baser) = (valOf_or_fail "rtl_repipeline: no width in bit-insert 1/2" prec.widtho, int amount)
                let ranger = (width+baser-1, baser)
                if prec.signed=Signed then hpr_yikes("ignoring signed nature of bit insert in " + xToStr net)
                power_list.add power (ranger, gg, exp)                            
                ()

            | W_node(prec, V_cast CS_maskcast, [X_bnet ff], _) -> // Occurs on lhs for bit-insert operations.
                let msg = "rtl_repipeline: insert_power: reveng bit-insert 2/2" 
                let power_list = get_power_list ff.n
                let (width, baser) = (valOf_or_fail "rtl_repipeline: no width in bit-insert 2/2" prec.widtho, 0)
                let ranger = (width+baser-1, baser)
                if prec.signed=Signed then hpr_yikes("ignoring signed nature of bit insert in " + xToStr net)
                power_list.add power (ranger, gg, exp)                            
                ()


            | other -> sf (sprintf "rtl_repipeline: insert_power: other lhs form %s" (xToStr other))


    (insert_power, get_power_list)



// Insert bev input structures in the powers matrix database (powers_db).
// This may not be used really, since input is supposed to be RTL!
let repipeline_xbev_insert ww powers_db eE (ast_ctrl:ast_ctrl_t) bev =
    let (m_csv_lines, dname) = eE
    let ww = WF 2 "repipeline_xbev_insert" ww (sprintf "Start %s" dname)
    // Generic power's array - 2-D: net and power, along with CE form.   We can then project through the next-state function, like hypermode.

    let (insert_power, get_power_list) = power_insert_factory ww eE powers_db
    
    let rec simple_bev_compile nox gg cmd =
        match cmd with
            | [] -> nox
            | Xif(g1, ct, cf) :: tt ->
                let n0 = simple_bev_compile nox (ix_and g1 gg) [ct]
                let n1 = simple_bev_compile nox (ix_and (xi_not g1) gg) [cf]
                let max2(a,b) = if a>b then a else b
                simple_bev_compile (max2(n0, n1)) gg tt 
            | Xcomment _ :: tt 
            | Xskip :: tt     -> simple_bev_compile nox gg tt
            | Xblock(v) :: tt -> simple_bev_compile nox gg (v @ tt)
                 // Use counter nox to ensure only one assignment in any control flow, otherwise blocking and non-blocking resolution will be needed.
                 // Xeasc clause missing ::  should be converted to PLI here


            | Xassign(X_x(lhs, 1, _), rhs)::tt ->
                insert_power lhs 1 gg rhs    // Sequential logic   
                simple_bev_compile (nox+1) gg tt

            | Xassign(lhs, rhs)::tt -> // Combinational logic
                insert_power lhs 0 gg rhs 
                simple_bev_compile (nox+1) gg tt

            | other::tt ->
                hpr_yikes("rtl_repipeline: cannot effect a simple conversion of behavioural command (need an HLS expansion please) " + hbevToStr other)
                nox

    let g00 = X_true
    let _  = simple_bev_compile 0 g00 [bev]
    ()


// Insert RTL input structures in the powers matrix database (powers_db).
let repipeline_rtl_insert ww vd powers_db eE rtl cc =
    let ww = WF 2 "repipeline_rtl_insert" ww (sprintf "Start")

    let (insert_power, get_power_list) = power_insert_factory ww eE powers_db

    let insert1 gg arg cc =
        match arg with 
        | Rnop ss ->
            arg :: cc
        | Rpli(guard, (fgis, _), args) ->
            dev_println (sprintf "repipeline_rtl_insert: Ignore PLI calls for now")
            cc

        | Rarc(guard, X_x(_, lhs, _), rhs) ->  muddy "insert L183"
        | Rarc(guard, lhs, X_x(_, rhs, _)) ->  muddy "insert L184"            

        | Rarc(guard, lhs, rhs) ->
            insert_power lhs 1 (ix_and gg guard) rhs // Sequential logic (or comb if X^-1)
            cc

        //| other -> sf (sprintf "Insert1: Other : %A" other)

    let insert arg cc =
        match arg with
            | XIRTL_buf(gg, lhs, rhs) ->  // Combinational logic
                insert_power lhs 0 gg rhs
                cc
                
            | XRTL(Some(ll, rr), g1, lst) ->
                List.foldBack (insert1 (ix_and g1 (ix_deqd ll rr))) lst cc
            | XRTL(None, g1, lst) ->
                List.foldBack (insert1 g1) lst cc

            | other -> sf (sprintf "Other : %A" other)

    let nv = List.foldBack insert rtl []

    if nullp nv then cc else XRTL(None, X_true, nv) :: cc







let rec repipeline_h2sp_insert ww vd dir powers_db eE arg cc =
    match arg with
        | SP_l(ast_ctrl, bev) ->
            repipeline_xbev_insert ww powers_db eE ast_ctrl bev
            cc

        | SP_par(pstyle, lst) ->
            let _ = map (repipeline_h2sp_insert ww vd dir powers_db eE) lst
            //SP_par(pstyle, ans_)
            cc

        | SP_seq(lst) -> // Seq semantic is discarded!
            let ans _ = map (repipeline_h2sp_insert ww vd dir powers_db eE) lst
            //SP_seq(ans_)
            cc

        | SP_rtl(ii, rtl)        ->
            let pass_ons = repipeline_rtl_insert ww vd powers_db eE rtl []
            if nullp pass_ons then cc else SP_rtl(ii, pass_ons)::cc


        | SP_dic(dic, ctrl) -> 
            let dic' = Array.create dic.Length (Xskip)
            let ww' = WN ("SP_dic " + ctrl.ast_ctrl.id) ww 
            let xlat b =
                let ast_ctrl = muddy "dic_ctrl_to_ast_ctrl ctrl"
                let ans = repipeline_xbev_insert ww' powers_db eE ast_ctrl b
                muddy "DIC structure is lost ... not worthwhile... ?"
            let _ =
                let jrepipeline p = Array.set dic' p (xlat dic.[p])
                app jrepipeline [0..dic.Length-1]
            //SP_dic(dic', ctrl)
            cc

        | SP_comment ss -> // Can nicely pass comments through ?
            arg::cc

        //| SP_asm _ -> arg
        | other  -> muddy("repipeline_h2sp other form:" + hprSPSummaryToStr other)


let repipeline_dir ww eE dir =
    //muddy (sprintf "Dir=%A" dir)
    dir


let repipeline_insert_exec ww vd powers_db m_dir eE (H2BLK(dir, sp_in)) cc =
    let (m_csv_lines, (dname:string)) = eE
    let ww = WF 2 "rtl_repipeline" ww (sprintf "repipeline_insert_exec: Start exec block for " + dname)

    if nullp dir.clocks then ()
    else
        if not_nonep !m_dir  && valOf !m_dir <> dir then muddy "Two clock domains"
        m_dir := Some dir // Crude --- multiple clock domains risk!
    let passons = repipeline_h2sp_insert ww vd dir powers_db eE sp_in []
    let ww = WF 2 "rtl_repipeline" ww (sprintf "rsynth_insert_exec: Finished exec block")    
    let gen sp = H2BLK(dir, sp)
    (map gen (passons @ [])) @ cc


let to_rtl_quad_form ww eE newkey execs = 
    let vd = 3
    let (m_csv_lines, dname) = eE
    let ww = WF 2 "rtl_repipeline" ww ("Start convert to quad form.")
    let powers_db = new net_powers_db_t("powers_db")

    let m_dir = ref None
    let _ = List.foldBack (repipeline_insert_exec ww vd powers_db m_dir eE) execs []
    

    let ww = WF 2 "rtl_repipeline" ww ("Finished convert to quad form.")

    let matrix_iterator ww powers_db msg ffun aux = 
        let _:net_powers_db_t = powers_db
        let itpull1 xidx (vv:net_power_list_t) =
            let (ff, f2) = valOf_or_fail "no such net" (lookup_net_by_xidx_o xidx)
            let lhs = X_bnet ff
            let ranger00 = (encoding_width lhs - 1, 0)
            //vprintln 2 (sprintf "%s: %i powers stored for net %s.  " msg vv.KeyCount ff.id)
            for k in vv do ffun aux ff f2 lhs ranger00 k.Key k.Value done
        for k in powers_db do itpull1 k.Key k.Value done


    let (quads, all_support, one_step_support_) =
        let dP = new_dP()
        let (m_quads, n_seq, n_comb) = (ref [], ref 0, ref 0)

        let apply_range ranger ranger00 lhs =
            if ranger = ranger00 then lhs
            else
                //vprintln 0 (sprintf "Ranger %A not %A" ranger ranger00)                
                ix_bitinsert (fst ranger) (snd ranger) lhs
        let one_step_support_ = new ListStore<hexp_t, (int * sd_pair_t)>("support_sets")
        let m_all_support = ref []
        let one_step_supf (dummy) ff f2 lhs (ranger00:int *int) power lst =
            let onestep_serf (ranger_, gg, rhs) = 
                let support_list = xi_support dP [] rhs
                let support_list = xi_bsupport dP support_list gg
                mutadd m_all_support (map fst support_list)
                //one_step_support_.adda lhs (map (fun x -> (power, x)) support_list)
                //dev_println (sprintf "One step set power %i %s  <-- %s" power (xToStr lhs) (sfold (fst>>xToStr) support_list))
            //List.fold (fun cc (net, lanes_) -> singly_add (x2nn net) cc) cc locals
                match power with
                    | 0 -> // Combinational logic
                        let diog (ranger, gg, rhs) = // This is not conglomorating bit inserts?  Where is the code that does this please?
                            let lhs = apply_range ranger ranger00 lhs
                            mutadd m_quads (power, lhs, rhs, -1.0) // -1.0 denotes time not stored (never used anyway?)
                            //mutadd m_cs (Rarc(gg, xi_X -1 lhs, rhs)) ... seemingly not liked by diosim or stimer!
                            mutinc n_comb 1
                        app diog lst
                    | 1 -> // Registered once (ie sequential logic).
                        let diog (ranger, gg, rhs) =
                            let lhs = apply_range ranger ranger00 lhs
                            mutadd m_quads (power, lhs, rhs, -1.0) // -1.0 denotes time not stored (never used anyway?)
                            //mutadd m_seq (Rarc(gg, lhs, rhs))
                            mutinc n_seq 1
                        app diog lst
                    | nn -> sf "other power"
            app onestep_serf lst
        matrix_iterator ww powers_db "one_step_supf" one_step_supf "dummy"
        vprintln 2 (sprintf "Extracted %i seq and %i comb assign quads/pairs."  !n_seq  !n_comb)
        if nullp !m_quads then hpr_yikes(sprintf "%s: rtl_repipeline: all RTL is missing - but was there any to start with?" newkey)
        (!m_quads, list_once (list_flatten !m_all_support), one_step_support_)


    let inputs =
        let inputs = List.filter xi_is_input all_support
        reportx 3 ("Inputs for " + newkey) netToStr inputs
        mutadd m_csv_lines ("Inputs", i2s(length inputs))
        inputs

    let seq_dtor = repipeline_dir ww eE (valOf_or_fail "no clock domain" !m_dir)
        
    let resets = 
        vprintln 3 (sprintf "Reset names for fuzzy tie off are  " + sfold resetToStr seq_dtor.resets)
        seq_dtor.resets


    let seq_dtor = { seq_dtor with duid=next_duid(); resets=resets }

    let domains = [(seq_dtor, quads)] // Just one for now
    (domains, all_support, one_step_support_, inputs) // end of to_rtl_quad_form



//------------------------------------
//
// Find strongly-connected components in sequential dependencies.
let scc_find ww eE keyname nodes edges =  
    let (m_csv_lines, dname) = eE
    vprintln 2 (sprintf "Find SCCs in graph of %i nodes, %i edges" (length nodes) (length edges))
    let nodes = map f1o3 nodes
    let edges = map (fun ((dest, _), (src, _)) -> (src, dest)) edges
    let pof = fun x -> x
    let edges =
        let e = new ListStore<string, string>("edges")
        let insert (s, d) = e.add s d
        app insert edges
        e 
    let sccs = moscow.tarjan1<string> keyname (Some pof) 2 (nodes, edges)
    vprintln 1 (keyname + " " + i2s (length sccs) + sprintf " SCCs have been found.")
    mutadd m_csv_lines ("SCCs", i2s (length sccs))
    app (fun q -> vprintln 1 ("\n\n------------------no-of-members=" + i2s(length q) + " ----\n" + sfoldcr pof q + "---------\n")) sccs
    ()


let dotrep ww keyname nodes edges = 
    let keyname = filename_sanitize ['_'; '.'] keyname
    let title = keyname
    let ww = WF 2 "rtl_repipeline" ww (sprintf "Writing a dot report.  keyname=%s, %i nodes, %i edges." keyname (length nodes) (length edges))

    let nodes =
        let ngen (name, legend, iof) =
            let (colour, shape) = if iof then ("red", "box") else ("black", "ellipse")
            DNODE_DEF(name, [("label", legend); ("color", colour); ("shape", shape)])
        map ngen (list_once nodes)

    let edges =
        let decorate ((dest, _), (src, _)) =
            DARC(DNODE(src, ""), DNODE(dest, ""), [  ("color", "blue");  ])
        map decorate (list_once edges)

    let filename = "structure_" + keyname + ".dot"
    let fd = yout_open_log filename
    yout fd (report_banner_toStr "// ")

    let data = nodes @ edges 
    dotout fd (DOT_DIGRAPH("rtl_repipeline_" + keyname, data))
    yout_close fd
    ()





// TODO handle more than one clock domain ...

let is_io = function
    | X_bnet ff ->
        let (ff, f2) = valOf_or_fail "rtl_repipeline pza" (lookup_net_by_xidx_o ff.n)
        varmode_is_io f2.xnet_io
    | other ->
        sf("Other form in is_io" + xToStr other)

// There are various forms of analysis possible. 
// The grouping code in resynth.fs is perhaps not relevant at the moment so lets find sequential support.        


let create_fuzzy_balance_analyser ww concisef =
    let title = "fuzzy_balance"
    let norecurse = true
    let logger arg = ()
    let vm_defs = new vm_defs_t("Not needed") // No external instances to consider.
    let report_areaf = false
    let (fuzzy_walkctl, reportf) = fuzzy_logic_walk_set_gen ww 3 "fuzzy_balance" report_areaf
    (fuzzy_walkctl, reportf)


let rez_fussy_consider fuzzy_walkctl =
    let fuzzy_walkctl = reset_walkctl fuzzy_walkctl
    let fuzzy_consider_exp (arg:hexp_t) =
        //let walkctl = valOf(queries.logic_costs)
        let clkinfo_o = g_null_directorate // for now
        let tallies = ref []
        let wlk_plifun = "notused" // For now
        let aux = ("fuzzymsg", fuzzy_walkctl, Some tallies, wlk_plifun)
        let ans = mya_walk_it aux clkinfo_o arg
        ans:walker_nodedata_t

    (fuzzy_consider_exp)


// Find expected values on each net.
let fuzzy_analyse ww reset_nets rtl_quads =
    let ww = WF 2 "fuzz_determine" ww "start iterating"
            

    let leaf_net_vector_final =
        let (fuzzy_walkctl, reportf) = create_fuzzy_balance_analyser ww true

        let (leaf_net_vector, _ ) = reportf ww ()

        let _ = 
            let tie_off_reset (is_pos, is_asynch, net) =
                leaf_net_vector.add (x2nn net) (if is_pos then 0.0 else 1.0) // Tie reset net expectation to zero.
            app tie_off_reset reset_nets
        
        let rec run_iteration iteration =
            let ww = WF 2 "fuzz_determine" ww (sprintf "start iteration %i" iteration)

            let fuzzy_consider_exp = rez_fussy_consider fuzzy_walkctl // Creates an instance with a fresh cache.

            let fuzzy_iterate power lhs rhs =
                let nn = x2nn lhs
                match fuzzy_consider_exp rhs with
                    | A_FUZZY(fv) ->
                        //vprintln 1 (sprintf "Iteration %i:  Set nn=%i  power=%i to fv=%.2f" iteration nn power fv)
                        leaf_net_vector.add nn fv
                    | _ -> () // should not happen?

            let fuzzserf (power, lhs, rhs, _ ) = fuzzy_iterate power lhs rhs
            app fuzzserf rtl_quads
            if (iteration < 5) then run_iteration (iteration + 1)
    
        run_iteration 0
        leaf_net_vector
        
    let assoc_list =
        let m_rr = ref []
        let yy xidx value =
            mutadd m_rr (xidx, value)
        for k in leaf_net_vector_final do yy k.Key k.Value done
        !m_rr        
    let ww = WF 2 "fuzz_determine" ww "finished iterating"
    assoc_list


let generate_logic_timingfun ww = // This should be boilerplate and not in here?
    let norecurse = true
    let costfun = 
        let concisef = true
        let report_areaf = false
        let coster_walkctl = logic_cost_walk_set_gen ww 3 "logic_cost" report_areaf
        let clkinfo_o = g_null_directorate // for now
        let tallies = ref []
        let wlk_plifun = "notused" // For now
        let aux = ("rtl_repipeline_logic_cost", coster_walkctl, Some tallies, wlk_plifun)
        let costfun (arg:hexp_t) = mya_walk_it aux clkinfo_o arg
        costfun

    costfun


type seqvecdata_t = hexp_t * hexp_t list * hexp_t list * string * hexp_t * float * float    
//                 (seqnet, full_support,  seq_support,  seqss,   dnet,    delay,  fuzzyv)     


// Pre-compute various useful properties of the RTL for one clock domain.
let repipe_rtl_quad_anal ww eE keyname reset_nets rtl_quads =
    let ww = WF 2 "repipe_rtl_quad_anal" ww (sprintf "Start %s" keyname)
    let vd = 4 // Three (3) is the default
    let dP = new_dP()
    let (m_csv_lines, dname) = eE

    let key = "original-anal-" + dname
    let logict = gen_logict_db ww key "repipeline_rtl_quad_anal" rtl_quads
    
    let transitive_comb_support = // Just find the combinational cone (yet another coding!)
        let transitive_comb_support = new OptionStore<hexp_t, sd_pair_t list>("transitive_comb_support_sets")

        let rec trans_comb_maid depth cc net =
            //dev_println (sprintf "Combinational support explore net=%s at depth %i " (xToStr net) depth)
            if (depth > 500) then vprintln 0 ("Infinite loop of combinational support perhaps encountered? " + xToStr net)
            if (depth > 1000) then sf ("Infinite loop of combinational support encountered. " + xToStr net)            
            match transitive_comb_support.lookup net with
                | Some ans -> ans @ cc
                | None ->
                    let ans = 
                        match logict.lookup net with
                            | None -> cc // tacit
                            | Some(power, lhs, rhs, _) ->
                                if power = 0 then []
                                else
                                    let aug cc = function
                                        | (item) -> item :: (trans_comb_maid (depth+1)) cc (fst item)
                                    List.fold aug [] (xi_support dP [] rhs)
                    transitive_comb_support.add net ans
                    ans @ cc
        
        let rec trans_comb_serf quad =
            let (power, lhs, rhs, _) = quad
            if (power = 1) then (lhs, [])
            else
                let support = List.fold (trans_comb_maid 1) [] (map fst (xi_support dP [] rhs))
                if vd>=4 then vprintln 4 (sprintf "Trans comb support of %s is %s\n\n" (xToStr lhs)  (sfold (fun (x, _) -> xToStr x) support)) //use sdpairToStr if you can find it!
                (lhs, support)
        let _ = map trans_comb_serf rtl_quads // Discarded, but side-effecting.
        transitive_comb_support

    let get_trans_comb_support_cc cc net =
        match transitive_comb_support.lookup net with
            | None -> cc
            | Some ans -> lst_union (list_once (map fst ans)) cc 

#if FUTURE_TO_USE_PROPER_COST
    let timings =
        let timingfun = generate_logic_timingfun ww
        let gec_timing (net, inputs) =
            vprintln 0 (sprintf "timingfun %s  %i inputs" (netToStr net) (length inputs) + sfold (fun (x, lanes)->xToStr x) inputs)
            let ans  =
                match timingfun (hd (map fst inputs)) with
                    | other -> vprintln 0 (sprintf "timingfun %s  %s" (netToStr net) (qToStr other))
            ()
        map gec_timing seq_nsfs
#endif

    // new call site:
    let timing_lookup =
        let standalone_mode_srcs = []// No external input timings to hand - will default to 0.35
        
        let (max_time, max_net, logic_pool, timing_lookup) = ap_timing_anal ww eE  "final result" rtl_quads standalone_mode_srcs None
        timing_lookup
        
    let ww = WF 2 "repipe_rtl_quad_anal" ww "Finished timing analysis"

    let seq_nsfs = List.fold (fun cc (power, lhs, rhs, _) -> (if power=1 then (lhs, rhs)::cc else cc)) [] rtl_quads
    let seq_nets = map fst seq_nsfs
    mutadd m_csv_lines ("Flops", i2s(length seq_nets))

    reportx 3 ("Sequential nets for " + dname)  netToStr seq_nets

    let fuzzy_metrics = fuzzy_analyse ww reset_nets rtl_quads

    let compute_vector seqnet = // For each sequentially-assigned net, precompute various properties and store them as a vector.
        let d_input =
            match op_assoc seqnet seq_nsfs with
                | None -> sf (sprintf "No d-input for such seq net as " + xToStr seqnet)
                | Some d_input -> d_input

        let full_support = // 
            let minze exp =
                let sup = xi_support dP [] exp
                map (fun xx -> map fst (valOf_or_nil (transitive_comb_support.lookup xx))) (map fst sup)
            list_once(list_flatten(minze d_input))

        let seq_support = lst_intersection full_support seq_nets

        let seqss = (sfold (xToStr) seq_support)
        vprintln 0 (sprintf "Sequential support for %s is " (xToStr seqnet) + seqss)

        let delay = 
            match timing_lookup "L865-delay" d_input with
                | Some v -> v
                | None   -> sf ("L497: timing missing for " + netToStr d_input)

        let fuzzyv = 
            match op_assoc (x2nn seqnet) fuzzy_metrics with
                | Some v -> v
                | None   -> sf "L501"
        (seqnet, full_support, seq_support, seqss, d_input, delay, fuzzyv):seqvecdata_t

    let vector_store = new OptionStore<hexp_t, seqvecdata_t>("vector store")

    let get_vector seqnet =
        match vector_store.lookup seqnet with
            | Some ov -> ov
            | None ->
                let nv = compute_vector seqnet
                vector_store.add seqnet nv
                nv

    let get_dnet net = // or expression
        let (seqnet, full_support, seq_support, seqss, dnet, delay, fuzzyv) = get_vector net
        dnet

    let get_seq_support net = 
        let (seqnet, full_support, seq_support, seqss, dnet, delay, fuzzyv) = get_vector net
        seq_support

    let get_delay net = 
        let (seqnet, full_support, seq_support, seqss, dnet, delay, fuzzyv) = get_vector net
        delay

    let get_fuzzyv net = 
        let (seqnet, full_support, seq_support, seqss, dnet, delay, fuzzyv) = get_vector net
        fuzzyv

    let unpredity fuzzyv = if fuzzyv > 0.5 then 1.0 - fuzzyv else fuzzyv
        
    let get_unpredictibility net =
        let fuzzyv = get_fuzzyv net
        unpredity fuzzyv


    // Dot report seq support graph.
    let _ =
        let keyname = "seq-support"
        let nodes = map (fun x-> (xToStr x, xToStr x, false)) seq_nets
        let edges =
            let gec_edge cc net =
                List.fold (fun cc (s) -> ((xToStr s, ""), (xToStr net, ""))::cc) cc (get_seq_support net)
            List.fold gec_edge [] seq_nets
        dotrep ww keyname nodes edges
        // SCC find
        let _ = scc_find ww eE keyname nodes edges  // Find strongly-connected components in sequential dependencies.
        ()

    // Input design report
    let table =
        let hdr = ["Net"; "D"; "Expected"; "Unpredity"; "Timing"; "Flags"; "Sequential Support" ]
        let seq_report net = 
            let (seqnet, full_support, seq_support, seqss, dnet, delay, fuzzyv) = get_vector net
            let flags = if memberp net seq_support then "S" else ""
            
            [ xToStr net; xToStr dnet; sprintf "%A" fuzzyv;sprintf "%A" (unpredity fuzzyv); sprintf "%A" delay; flags; seqss ]
        let title = "Pre-partition statistics for "  + dname
        let table = tableprinter.create_table (title, hdr, map seq_report seq_nets)
        app (vprintln 0) table
        aux_report_log 2 ("Input Design Report:" + title) table

    let ww = WF 2 "repipe_rtl_quad_anal" ww "Finished loadup analysis"

    let get_comb_expr_for net =
        match logict.lookup (net) with
            | None ->
                //vprintln 0 (sprintf "Yikes: no entry in powers for " + xToStr net)
                //This is ok for sequential nets too.
                //if not(xi_is_input net) then sf ("L576 Undriven comb should be an input not " + netToStr net)
                (None, None)
            | Some (power, lhs, rhs, _) ->
                if power = 0 then
                    //This is ok for sequential nets too.                        
                    //if not(xi_is_input net) then sf ("L581 Undriven comb should be an input not " + netToStr net)
                    //vprintln 0 (sprintf "Yikes: no entry for power 0 (comb) for " + xToStr net)
                    (Some rhs, None)
                elif power = 1 then (None, Some rhs)
                else sf "L950"

    let get_comb_or_seq_expr_for net = // doubles up on part of get_d elsewhere ...? Stupid function.
        match logict.lookup (net) with
            | None ->
                if not(xi_is_input net) then sf ("L89 Undriven comb should be an input not " + netToStr net)
                sf ("L698 " + netToStr net)
            | Some (0, lhs, rhs, dx) -> (Some rhs, None)
            | Some (1, lhs, d_exp, dx) -> (None, Some d_exp)                   
            | _ -> sf ("L695 " + netToStr net)


    let rec get_comb_assigns_cc_serf depth cc outp = // Add to cc all the combinational assign pairs needed to support target 'outp'
        if (depth > 500) then hpr_yikes("L590 suspected comb loop on " + netToStr outp)
        if (depth > 1000) then sf ("L590 comb loop on " + netToStr outp)
        let rec present_already = function
            | [] -> false
            | (_, net, _, _)::tt when net=outp -> true
            | _ :: tt -> present_already tt
        if present_already cc then cc 
        else
            if (xi_is_input outp) then cc // Nothing drives inputs
            else
                let (further, cc) = 
                    match outp with                
                        | X_bnet ff ->
                            match get_comb_or_seq_expr_for outp with // Seq or comb or input
                            | (None, _)     -> ([], cc)
                            | (Some ans, _) ->
                                if vd>=5 then vprintln 5 (sprintf "get_comb_assigns_cc_serf: comb %s  -> %s" (xToStr ans) (xToStr outp))
                                if ans=outp then sf("L714: Assign-to-self returned by get_comb_expr on " + netToStr outp)
                                let cc = (0, outp, ans, -1.0)::cc              // 0 denotes combinational
                                (map fst (xi_support dP [] ans), cc)

                        | other ->
                            let further = map fst (xi_support dP [] other)
                            (further, cc)
                            
            //elif (outp = ans) then // This is returned by default for for undriven (eg an exp) but ideally return None instead
            //or when we are called on an expression (e.g an input) - best return option?
                vprintln 0 (sprintf "  -- For %s get further  %s" (xToStr outp) (sfold xToStr further))
                List.fold (get_comb_assigns_cc_serf (depth+1)) cc further


    let get_seq_assign_plus_combs_cc cc outp =
        match get_comb_or_seq_expr_for outp with
            | (_, Some dexp) ->
                let seq_ap = (1, outp, dexp, -1.0)
                vprintln 0 (sprintf "get_seq_assign_plus_combs: For seq %s get d-expression %s" (xToStr outp) (xToStr dexp))
                get_comb_assigns_cc_serf 0 (seq_ap :: cc) dexp // Add to cc all the combinational assign pairs needed to support target 'outp'
            | _-> cc
    let augmented_anal = (seq_nets, get_seq_support, get_delay, get_unpredictibility, get_dnet, get_fuzzyv, get_seq_assign_plus_combs_cc, reset_nets, get_trans_comb_support_cc)

    (augmented_anal) // end of repipe_rtl_quad_anal 


//-----------------------------------------

let partition_state_vec ww eE (rtl_quads, augmented_anal) =

    let ww = WF 2 "partition_state_vec" ww "Commence Preparations"
    let (m_csv_lines, dname) = eE
    
    let (seq_nets, get_seq_support, get_delay, get_unpredictibility, get_dnet, get_fuzzyv, get_seq_assign_plus_combs_cc, reset_net, get_trans_comb_support_cc) = augmented_anal   

    //let get_comb_assign_pairs outp =  get_seq_assign_plus_combs_cc [] outp 

    let ww = WF 2 "Best Partition Search" ww "Starting"
    
    let initial_ption = // We only need to consider the SCC of the seq support in fact. But this is naive.
        let n = length seq_nets
        let rec take_n n cc = function
            | [] -> (cc, [])
            | h::tt when n > 0 -> take_n (n-1) (h::cc) tt
            | h::tt -> (cc, h::tt)
        let (pl, ql) = take_n (n/2) [] seq_nets
        //vprintln 0 (sprintf Initial partition "%i  %i  %i pop n=%i" (length seq_nets) (length pl) (length ql) n)
        (pl, ql)

    let permute_partition randv number_of_changes (pl, ql) =
        let permute1 randv (pl, ql) =
            let lpl = length pl
            if lpl = 0 then ([hd ql], tl ql)       // Should not happen
            elif lpl = 1 then ((hd ql)::pl, tl ql) // Don't remove last one
            else
                let n = randv % lpl
                let rec delete n1 cc = function
                    | h::tt when n1=n -> (h, tt @ cc)
                    | [] -> sf "L588-delete"
                    | h::tt -> delete (n1+1) (h::cc) tt
                let (q, pl) = delete 0 [] pl
                (pl, q::ql)  // Moves a random entry from first to second half
        let permute2 randv (pl, ql) =
            let x1 = ((randv*553)%127)
            let fwd = ((randv * 111 / 15) % 2) > 0
            //vprintln 0 (sprintf "x1=%i  fwd=%A" x1 fwd)
            if fwd then
                let (ql, pl) = permute1 x1 (ql, pl)
                (pl, ql)
            else permute1 x1 (pl, ql)
        let rec permute3 nn dd = // Make a number of changes at higher temperatures.
            if (nn = 0) then dd
            else permute3 (nn-1) (permute2 (randv + nn*87) dd)
        permute3 number_of_changes (pl, ql)

    // Figure of merit: weighted balance of predictibility of feedback and expense in the forward direction.
    let eval_partition logf (pl, ql) =
        let find_support outs ins =
            let find_seq_sup cc target =
                let support = get_seq_support target
                let support = lst_intersection support ins
                lst_union cc support
            List.fold find_seq_sup [] outs
        let (plen, qlen) = (length pl, length ql)     
        let (p_support_in_r, r_support_in_p) = (find_support pl ql, find_support ql pl)
        let (fwd_size, rev_size) = (length p_support_in_r, length r_support_in_p)

        let fwd_timing =  // Want max of these - TODO get dnet ... timing via cut path, not max from p, although q should be larger.
            let worst = List.fold (max) 0.0 (map get_delay p_support_in_r)
            worst    

        let rev_unplity =  // Want max of these - TODO get dnet ... timing via cut path, not max from p, although q should be larger.
            List.fold (fun a b -> a+b) 0.0 (map get_unpredictibility  r_support_in_p)

        if (logf) then
            mutadd m_csv_lines ("Unplity", sprintf "%.2f" rev_unplity)
            mutadd m_csv_lines ("FwdTimng", sprintf "%.1f" fwd_timing)
        // We want low unpredictibility of the q_support_in p, which arises when  the sum of the unpredictibility or its components is low
        let goodness = fwd_timing / (rev_unplity + 0.0001)
        //vprintln 0 (sprintf "Partition %i+%i eval: fwd/rev=(%i, %i) vars.  fwd_timing=%A rev_unplity=%A  goodness=%A" plen qlen fwd_size rev_size fwd_timing rev_unplity goodness)
        goodness
        

    let rec iterate count temp goodness ption =
        //let ww = WF 4 "Best Partition Search" ww "Commence new iteration"
        let new_ption = permute_partition count (int(temp)/10 + 1) ption
        let new_goodness = eval_partition false new_ption
        let keep = (new_goodness >= goodness)
        let temp = if keep then temp * 0.99 else temp
        if (count % 200) = 99 then vprintln 0 (sprintf "Iteration %i  perf keep=%A     goodness=%A   best=%A" count keep new_goodness goodness)
        let (goodness, ption) = if (keep) then (new_goodness, new_ption) else (goodness, ption)
        if (count < 5000) then iterate (count + 1) temp goodness ption
        else ption
    let initial_goodness = eval_partition false initial_ption
    let initial_temp = 100.0
    let final_ption = iterate 0 initial_temp initial_goodness initial_ption
    let (pl, rl) = final_ption
    mutadd m_csv_lines ("PL", i2s (length pl))
    mutadd m_csv_lines ("RL", i2s (length rl))                    


    let final_goodness = eval_partition true final_ption
    vprintln 0 (sprintf "\n\nFinal goodness %.2f" final_goodness)
    let ww = WF 2 "Best Partition Search" ww "Finished"

    final_ption



//
//
//    
let prepare_and_do_transform ww eE augmented_anal (pl, rl) =
    let (m_csv_lines, (dname:string)) = eE
    let dP = new_dP()    
    let (seq_nets, get_seq_support, get_delay, get_unpredictibility, get_dnet, get_fuzzyv, get_seq_assign_plus_combs_cc, reset_net, get_trans_comb_support_cc) = augmented_anal

    vprintln 0 (sprintf "spxform pl=%i rl=%i" (length pl) (length rl))
    let m_newnets = ref []
    
    let sanity_check msg (px, l, r, _) =
        //vprintln 3 (sprintf "Sanity check %s net %s px=%i" msg (netToStr l) px)
        if (px < 0 || px > 1) then sf (msg + ": PX Out of range on " + netToStr l)
        if (l = r) then sf (msg + ": Assign to self on  " + netToStr l)
        if (xi_is_input l) then sf (msg + ": " + dname + ": Assign to input on  " + netToStr l)

    let perform_transform ww (pl, rl) =
        let ww = WF 2 "Pipeline Transform" ww "Starting"
        let find_support outs ins =
            let find_seq_sup cc target =
                let support = get_seq_support target
                let support = lst_intersection support ins
                lst_union cc support
            List.fold find_seq_sup [] outs
        let (plen, rlen) = (length pl, length rl)     
        let (p_support_in_r, r_support_in_p) = (find_support pl rl, find_support rl pl)
        let (fwd_size, rev_size) = (length p_support_in_r, length r_support_in_p)
        vprintln 0 (sprintf "Xform based on partition %i+%i eval: fwd/rev=(%i, %i) vars." plen rlen fwd_size rev_size)


        let mismatch = simplenet (funique ("mismatch_" + dname))

        let oracle_nets = r_support_in_p
        let oracle_values =
            let gec_oracle_value net =
                let fv = get_fuzzyv net
                let orv = xi_blift(if (fv > 0.5) then xi_true else xi_false) // TODO assume binary for now
                vprintln 0 (sprintf "Oracle net %s  fv=%A  using %s" (xToStr net) fv (xToStr orv))
                (net, fv, orv)
            map gec_oracle_value oracle_nets

        let detector = // mispredict detector
            let oracle_bin_values = map (fun (net, fv, orv) -> orv) oracle_values
            vprintln 2 (sprintf "Generating detector %i cf %i" (length oracle_bin_values) (length oracle_nets))
            let m = ix_orl (map (fun (a, b) -> ix_dned a b) (List.zip (oracle_bin_values) (oracle_nets)))
            vprintln 0 (sprintf "Mismatch detector on %i bit is %s" (length oracle_values) (xbToStr m))
            [(0, mismatch, xi_blift m, -1.0)]


        let d_regs_raw = // We need to delay (pipeline) the input nets used by RF.  
            let all_comb_support_RF = (List.fold (fun cc net -> get_trans_comb_support_cc cc (get_dnet net)) [] rl)
            let all_comb_support_RF_old_ =
                let gec_acs cc r_net = xi_support dP cc (get_dnet r_net)
                List.fold gec_acs [] rl  // Need transitive comb support
            //reportx 0 "all_comb_support_RF" (fun n -> netToStr n) (all_comb_support_RF)
            let delayed_inputs = List.filter (fun net->xi_is_input net) all_comb_support_RF
            vprintln 0 (sprintf "delayed_inputs: %i in all comb support to RF" (length all_comb_support_RF))
            vprintln 0 (sprintf "delayed_inputs: %i d_regs needed to support RF" (length delayed_inputs))
            let gec_dreg inet =
                let dreg = clonenet "" "_DD" inet
                (1, dreg, inet, g_local_clk_to_q_time) // Assignments to dregs - not currently clock enabled for stuttering handshake - TODO
            let d_regs_raw = map gec_dreg delayed_inputs
            d_regs_raw

        let q_regs_raw = // Form fwd transform extra register (the pipeline stage)
            let gen_regQ ps =
                let qnet = clonenet "" "_QQ" ps
                (1, qnet, ps, g_local_clk_to_q_time) // Assignments to Q regs, pre migrate
            vprintln 0 (sprintf "delayed_p_support: %i q_regs needed to support RF" (length p_support_in_r))
            map gen_regQ p_support_in_r

        let t_regs = 
            let gen_regR ps =
                let tnet = clonenet "" "_TT" ps
                let tnet_d = ix_query (xi_not(xi_orred mismatch)) ps tnet // Clock-enabled
                (1, tnet, tnet_d, g_local_clk_to_q_time)
            map gen_regR pl

        let nsf_p = // Find all combinational supporting logic for the PF function.
            let gen_nsfP cc pbit =
                //let dnet = get_dnet pbit
                get_seq_assign_plus_combs_cc cc pbit
            let pre_rewrite_RF = List.fold gen_nsfP [] pl
            app (sanity_check "pre_rewrite_RF") pre_rewrite_RF

            // Then rewrite it so P input is mux of P and T and F input is oracle or real
            let p_t_pairs = List.zip pl (map f2o4 t_regs)
            let pmux_rewrites = map (fun (p, t) -> (p, ix_query (xi_not(xi_orred mismatch)) p t)) p_t_pairs
            

            let oracle_rewrites = map (fun (net, fv, orv) -> (net, orv)) oracle_values
            let rewrite_map = makemap (oracle_rewrites @ pmux_rewrites)
            let post_rewrite = map (fun (px, l, r, tv) -> (px, l, xi_assoc_exp g_sk_null rewrite_map r, tv)) pre_rewrite_RF
            //let tnet_d = ix_query (xi_not mismatch) ps tnet // Clock-enabled
            post_rewrite 


        let nsf_r = // Find all combinational expressions that form the RF function and rewrite their inputs.
            let gen_nsfR cc rbit =
                //let dnet = get_dnet rbit
                get_seq_assign_plus_combs_cc cc rbit  // We split these two apart again below for migration (!).
            let pre_rewrite_RF = List.fold gen_nsfR [] rl 
            reportx 2 "pre_rewrite_RF"  quadToStr  pre_rewrite_RF

            let q_rewrites = map (fun (px, q_reg, q_in, _) -> (q_in, q_reg)) q_regs_raw // We shall be migrating both of these, unless disabled.
            let d_rewrites = map (fun (px, d_reg, d_in, _) -> (d_in, d_reg)) d_regs_raw 
            let rewrite_map = makemap (q_rewrites @ d_rewrites)
            //let
            let post_rewrite_RF = map (fun (px, l, r, _) -> (px, l, xi_assoc_exp g_sk_null rewrite_map r, -1.0)) pre_rewrite_RF // Ignore px and make seq

            reportx 2 "post_rewrite_RF"  quadToStr  post_rewrite_RF
            let migrate_disable = false
            let (pool, rassigns) =
                if migrate_disable then
                    (d_regs_raw @ q_regs_raw, post_rewrite_RF)
                else
                    // The inputs expressions for each bit of the R register file are the functions to which we want to apply migration.
                    let migration_targets = map (get_dnet) rl
                    // The inputs to the Q and D regs are the signals that a raw inputs for the migration process

                    //The sources are the outputs of the P and R regs and the external data inputs; the Q and D regs can be migrated.
                    let migration_sources = List.fold (fun cc (1, l, r, _) -> (r, 0.35)::cc) [] d_regs_raw // ie. the D's in use
                    
                    let migration_sources = List.fold (fun cc net -> (net, g_local_clk_to_q_time)::cc) migration_sources (pl @ rl)

                    // The pool of logic to migrate is the pipelined (ie post_rewrite) comb support and the starting set up for the D and Q regs
                    let pre_pool = (List.filter (fun (px, l, r, _) -> px = 0) post_rewrite_RF) @ d_regs_raw @ q_regs_raw
                    let (post_targets, post_pool) = balanced_migrate ww eE m_newnets migration_targets migration_sources pre_pool
                    //vprintln 0 (sprintf "zip check %i cf %i" (length pl) (length post_targets))
                    let r_assigns = map (fun (q, d) -> (1, q, d, -1.0)) (List.zip (rl) post_targets)
                    (post_pool, r_assigns)
            let clock_enabled_r_assigns = map (fun (px, l, r, _) -> (px, l, ix_query (xi_not(xi_orred mismatch)) r l, -1.0)) rassigns // Clock-enabled
            pool @ clock_enabled_r_assigns 


        let logic = detector @ nsf_r @ nsf_p @ t_regs  // This is the answer without migration enabled
            
        let _ =
            app (sanity_check "nsf_r ") nsf_r
            app (sanity_check "nsf_p ") nsf_p
            //app (sanity_check "d_regs ") d_regs                
            app (sanity_check "t_regs ") t_regs                
            //app (sanity_check "q_regs ") q_regs                
            app (sanity_check "logic all") logic                

            // TODO want to mark as preserve for conerefine the p and t regs.
        let newnets = mismatch :: (map f2o4 d_regs_raw) @ (map f2o4 q_regs_raw) @ (map f2o4 t_regs) @ !m_newnets // Not everything in the pool will be new! 

        let further_message  = sprintf " |Q|=%i  |f|=%i "  fwd_size rev_size
        mutadd m_csv_lines ("Fwd_Size", i2s fwd_size)
        mutadd m_csv_lines ("Rev_Size", i2s rev_size)
        (logic, newnets, further_message)

    let (logic, newnets, further_message) = perform_transform ww (pl, rl)
    let ww = WF 2 "Pipeline Transform" ww "Finished"    
    (logic, newnets, further_message)  //  end of prepare_and_do_transform


   
// bit-insert reconstruct
let rec rebuild_bits ranger00 backstop = function
    | [] -> backstop
    | (ranger, gg, rhs)::tt  ->
            let backstop = rebuild_bits ranger00 backstop tt
            let rhs = 
                if ranger = ranger00 then rhs
                else
                    let mask arg width baser = ix_bitand arg (ix_lshift (gec_X_bnum({widtho=Some(width+baser); signed=Unsigned}, himask width)) (xi_num baser))
                    let ((conc_h, conc_l), (wanted_h, wanted_l)) = (ranger, ranger00)
                    vprintln 0 (sprintf "rebuild: Ranger %A not %A" ranger ranger00)                

                    let top = if (conc_h > wanted_h) then mask backstop (conc_h-wanted_h) (wanted_h+1) else xi_num 0
                    let bot = if (conc_l < wanted_l) then mask backstop (wanted_l-conc_l) (wanted_l-1) else xi_num 0                    
                    let rhs =
                        let width = wanted_h-wanted_l + 1
                        ix_bitand rhs (gec_X_bnum({widtho=Some width; signed=Unsigned}, himask width))
                    let mid = ix_lshift rhs (xi_num wanted_l)
                    ix_bitor (ix_bitor top mid) bot

            ix_query gg rhs backstop


// Simple cone-directed render
let repipeline_coned_render ww powers_db eE targets0 =
    let newid = funique "rtl-coned"
    let ww = WF 2 "repipeline_coned_render" ww (sprintf "Start %s" newid)
    let _:net_powers_db_t = powers_db        
    let dP = new_dP()
    let m_bs = ref []

    let rec buildcone dones = function
        | [] -> ()
        | hid::tt when memberp hid dones -> buildcone dones tt
        | hid::tt ->
            vprintln 3 (sprintf "cone-directed render: build for %s" hid)
            match lookup_net_by_string hid with 
                | None -> sf("rtl_repipeline: meox: cannot find net " + hid)
                | Some(X_bnet ff) ->
                    let dones = hid :: dones
                    let backstop = X_bnet ff
                    let ranger00 = (encoding_width (X_bnet ff) - 1, 0)
                    match powers_db.lookup ff.n with
                        | None ->
                            // ok if an input
                            vprintln 0 ("rtl_repipeline: cannot find any drivers for net " + netToStr(X_bnet ff)) 
                            buildcone dones tt

                        | Some powers ->
                            let scanner cc power vlst =
                                let xv = rebuild_bits ranger00 backstop vlst
                                let support = map fst (xi_support dP [] xv)
                                dev_println (sprintf "scanner: %s: power=%i  xv=%s" hid power (xToStr xv))
                                dev_println (sprintf "support is " + sfold xToStr support)
                                mutadd m_bs (power, ranger00, X_bnet ff, xv)
                                lst_union (map xToStr support) cc
                            let further = powers.fold scanner []
                            buildcone dones (lst_union further tt)

    let _ = buildcone [] targets0
    let ans =
        let m_cs = ref []
        let (n_seq, n_comb) = (ref 0, ref 0)
        let doz2 (power, ranger00, lhs, rhs) = 
        //vprintln 2 (sprintf "  --- %i entries for power %i of %s [%i:%i]:  " lst) power (netToStr lhs) (fst ranger00) (snd ranger00))


            match power with
                | 0 -> // Combinational 
                    mutadd m_cs (Rarc(X_true, xi_X -1 lhs, rhs))
                    mutinc n_comb 1
                | 1 -> // Registered once.
                    mutadd m_cs (Rarc(X_true, lhs, rhs))
                    mutinc n_seq 1
                | nn -> sf "doz2: other power"

        app doz2 !m_bs
        vprintln 3 (sprintf "Extracted %i seq and %i comb assign pairs."  !n_seq  !n_comb)
        !m_cs

    if nullp ans then hpr_yikes(sprintf "%s: rtl_repipeline_ all missing - but was there any to start with?" newid)
    let ii = { id=newid }:rtl_ctrl_t
    let ans = SP_rtl(ii, [ XRTL(None, X_true, ans) ])
    let newnets = []
    (ans, newnets)

type retoned_t =
    | CST_comb of int * (int * int) * hexp_t * hexp_t
    | CST_seq of hexp_t * int * (int * int) * hexp_t * hexp_t     


#if NOTDEF
// Re-timed rebuild
let repipeline_retimed_cone ww powers_db eE targets0 =
    let newid = funique "rtl-coned"
    let ww = WF 2 "repipeline_retimed_cone" ww (sprintf "Start %s" newid)
    let _:net_powers_db_t = powers_db        
    let dP = new_dP()
    let m_bs = ref []

    let m_newnets = ref []
    let gec_power_newnet tp target =
        if tp=0 then target
        else
            let ans = clonenet "" (sprintf "__p%i" tp) target // TODO save cloned-net for net list.
            mutadd m_newnets ans
            ans
            
    let back_cvt = function // Reverse of gec_power_newnet
        | X_bnet ff ->
            let ans = ff.id.IndexOf "__p"
            if ans < 0 then (ff.id, 0)
            else
            sf(sprintf "back cvt %s  %i" ff.id ans)
            
    let rec buildcone dones = function
        | [] -> ()
        | (hid, tp)::tt when memberp (hid, tp) dones -> buildcone dones tt
        | (hid, tp)::tt ->
            dev_println (sprintf "start build for %s tp=%i" hid tp)
            match lookup_net_by_string hid with 
                | None -> sf("rtl_repipeline: meox: cannot find net " + hid)
                | Some(X_bnet ff) ->
                    let basenet = X_bnet ff
                    let ranger00 = (encoding_width basenet - 1, 0)
                    let dones = (hid, tp) :: dones
                    match powers_db.lookup ff.n with
                        | None ->
                            // ok if an input
                            vprintln 0 (sprintf "rtl_repipeline: cannot find any drivers for tp=%i of net %s" tp (netToStr basenet))
                            buildcone dones tt

                        | Some powers ->
                            dev_println (sprintf "scanner: %s: wanted_power=%i  available=%s" hid tp (sfold i2s (powers.fold (fun cc k v -> k::cc) [])))


                            let (further) = 
                                match powers.lookup tp with
                                    | []   ->
                                        let avail = rev(List.sort (powers.fold (fun cc k v -> k::cc) []))
                                        if nullp avail then sf("no powers available afterall for net " + hid) // Should be trapped above in powers_db=None.
                                        // Sort, with highest existing power first.
                                        // It's possible there's some gaps and the one we want is in the gap.
                                        if tp=0 && hd avail = tp + 1 then // other +ve values also easy to do.
                                            // If an earlier version (higher power is available) we can just shift-register it.
                                            // OR shift register its support ... any pipeline migration scheme works.
                                            let tp2 = hd avail
                                            let newnet = gec_power_newnet tp basenet 
                                            powers.add tp (ranger00, X_true, newnet)  // Generate a new net and a broadside and add its input support to the further work.
                                            let vlst = powers.lookup (tp+1)
                                            if nullp vlst then sf(sprintf "missing vlst for %s at %i" (xToStr basenet) (tp+1))
                                            let xv = rebuild_bits ranger00 newnet vlst
                                            //let furthers xv = temp_abstract tp basenet vlst 
                                            // Can rely on sub-expression pin detection or manually do it here.
                                            mutadd m_bs (CST_seq(newnet, tp2, ranger00, basenet, xv))
                                            ([(hid, tp2)])  // Return as a furthers to ensure it gets built.
                                        else
                                         muddy(sprintf "missing synth tp=%i %s needed. Avail=%s" tp (hid)  (sfold i2s avail)) // Assumes all nets have a driver
                                        
                                    | vlst -> 
// Assumption of built vs cone-refined for render ... not on done if already copied to output
                                        let temp_abstract tp basenet vlst =
                                            let backstop = gec_power_newnet (tp-1) basenet // Backstop is from gen before.
                                            let xv = rebuild_bits ranger00 backstop vlst
                                            let support = map fst (xi_support dP [] xv)
                                            dev_println (sprintf "temp_abstract: %s: power=%i  xv=%s" hid tp (xToStr xv))
                                            dev_println (sprintf "support is " + sfold xToStr support)
                                            mutadd m_bs (CST_comb(tp, ranger00, basenet, xv))
// We;ll end up with int tp applied to string tp
                                            let further = map (fun support -> back_cvt support) support
                                            (further)


                                        let further = temp_abstract tp basenet vlst 
                                        (further)
                            buildcone dones (lst_union further tt)

    let addn hid = (hid, 0)
    let _ = buildcone [] (map addn targets0)

    let rtl =
        let (m_seq, m_buf) = (ref [], ref [])
        let (n_seq, n_comb) = (ref 0, ref 0)
        let doz2 = function
            | CST_comb(tp, ranger00, basenet, rhs) ->
                vprintln 2 (sprintf "CST_comb:   %i of %s [%i:%i]:  " tp (netToStr basenet) (fst ranger00) (snd ranger00))
                let lhs =
                    if tp=0 then basenet
                    else gec_power_newnet tp basenet
                //mutadd m_cs (Rarc(X_true, xi_X -1 lhs, rhs))
                mutadd m_buf (XIRTL_buf(X_true, lhs, rhs))
                mutinc n_comb 1
                    
            | CST_seq(lhs, tp, ranger00, basenet, rhs) ->
                vprintln 2 (sprintf "CST_seq setup (tp=%i) %s[%i:%i]:  " tp (netToStr lhs) (fst ranger00) (snd ranger00))
                mutadd m_seq (Rarc(X_true, lhs, rhs))
                mutinc n_seq 1

        app doz2 !m_bs
        vprintln 2 (sprintf "Extracted %i seq and %i comb assign pairs."  !n_seq  !n_comb)
        if nullp !m_seq then hpr_yikes(sprintf "%s: rtl_repipeline_ all missing - but was there any to start with?" newid)
        XRTL(None, X_true, !m_seq) :: !m_buf


    let ii = { id=newid }:rtl_ctrl_t
    let newnets = list_once !m_newnets
    let ans = SP_rtl(ii, rtl)
    (ans, newnets) // end of repipeline_retimed_cone ... not being used and perhaps suffering from wild/spurious global edits
#endif


        
let rtl_repipeline_main ww op_args control vms =
    let ww = WF 1 "rtl_repipeline" ww (sprintf "Start %i machines" (length vms))


    let repipeline ww = function
        | (ii, Some(HPR_VM2(minfo, decls, sons, execs, assertions))) when ii.definitionf ->

            let m_csv_lines = ref []  // One of these per VM compilation since we are using one VM per benchmark.
            let dname = hptos minfo.name.kind
            mutadd m_csv_lines ("Design", dname)
            let ww = WF 1 "rtl_repipeline" ww (sprintf "Design=%s starting" dname)
            let ii = { ii with generated_by=op_args.stagename  }    

            let vd = 3

            let eE = (m_csv_lines, dname)// object record.

            let (domains, all_support, one_step_support, inputs) = to_rtl_quad_form ww eE ("original-" + dname) execs

            let (seq_dtor, rtl_quads) =
                match domains with // All combs are in with the first domain for now
                | [dtor, quads] -> (dtor, quads)
                | _ -> muddy(dname +  ": Not one clock domain")


            let reset_nets = seq_dtor.resets

            let augmented_anal = repipe_rtl_quad_anal ww eE "raw" reset_nets rtl_quads

            let detrip (seq, comb) (px, lhs, rhs, _) =
                if px = 1 then (Rarc(X_true, lhs, rhs)::seq, comb)
                elif px = 0 then (seq, XIRTL_buf(X_true, lhs, rhs)::comb)
                else sf "L1204 - not seq or comb"


            let (sp_out, newnets) =
                if false then
                    let comment = XRTL_nop "cloned"
                    let (seq, comb) = List.fold detrip ([], []) rtl_quads
                    let ii = { id="cloned" }:rtl_ctrl_t
                    let sp_seq = H2BLK(seq_dtor, SP_rtl(ii, [ comment; XRTL(None, X_true, seq)]))
                    let comb_dtor = { g_null_directorate with duid=next_duid() }
                    let sp_comb = H2BLK(comb_dtor, SP_rtl(ii, comb))
                    let newnets = []
                    ([sp_seq; sp_comb], newnets)


                else
                    let (seq_nets, get_seq_support, get_delay, get_unpredictibility, get_dnet, get_fuzzyv, get_seq_assign_plus_combs_cc, reset_net, get_trans_comb_support_cc) = augmented_anal   
                    let count = length seq_nets
                    
                    let (pl, rl) =
                        if (false) then (seq_nets, []) // One extreme
                        elif (false) then ([], seq_nets) // Other extreme                        
                        else
                            partition_state_vec ww eE (rtl_quads, augmented_anal)

                    let message = sprintf "XPFORM using partition of %i into %i + %i." count (length pl) (length rl)
                    vprintln 0 message

                    reportx 3 ("PL seq nets for " + dname) netToStr pl
                    reportx 3 ("RL seq nets for " + dname) netToStr rl 
                    
                    let (logic, newnets, further_message) = prepare_and_do_transform ww eE augmented_anal (pl, rl)

                    let message = message + further_message
                    let standalone_mode_srcs = []// No external input timings to hand - will default to 0.35
                  
                    let timing = ap_timing_anal ww eE  "final result" logic standalone_mode_srcs None
                    
                    let (seq, comb) = List.fold detrip ([], []) logic
                    let newid = "spxformd"
                    let ii = { id=newid }:rtl_ctrl_t
                    let comment = XRTL_nop message

                    //let seq_dtor = repipeline_dir ww eE (valOf_or_fail "no clock domain" !m_dir)
                    let sp_seq = H2BLK(seq_dtor, SP_rtl(ii, [ comment; XRTL(None, X_true, seq)]))
                    let comb_dtor = { g_null_directorate with duid=next_duid() }
                    let sp_comb = H2BLK(comb_dtor, SP_rtl(ii, comb))
                    ([sp_seq; sp_comb], newnets)


            let newdec = // Declare new nets
                let cpi = { g_null_db_metainfo with kind= "rtl-repipeline-nets" }
                gec_DB_group(cpi, map db_netwrap_null newnets)

            // Old decls not deleted!
            let ans = (ii, Some(HPR_VM2(minfo, newdec @ decls, sons, sp_out, assertions)))

            let csv_report =
                {
                    csv_filename=       "benchmarks_report.csv"
                    csv_table_title=    "rtl_repipeline_csv_report.csv"
                    csv_lines=          [rev !m_csv_lines]
                }
            mutadd g_csv_reports csv_report // Other apps can follow this general pattern.
            let ww = WF 1 "rtl_repipeline" ww (sprintf "Design=%s finished" dname)
            
            ans
        | other -> other             

    let vms = map (repipeline ww) vms
    vms


    

let rtl_repipeline_argpattern stagename =
    [
        Arg_enum_defaulting(stagename, ["enable"; "disable"], "enable", "Enable/bypass control for this recipe stage.")
        
        Arg_enum_defaulting("rtl_repipeline-dump-straightaway", ["enable"; "disable"], "disable", "Enable Kernighan and Ritchie style (pre-2001) Verilog port syntax")       
   ] 
 
// This is the top-level instance of this plugin
let opath_rtl_repipeline ww op_args vms_in =
    let c1:control_t = op_args.c3
    let stagename = op_args.stagename
    let disabled = (cmdline_flagset_validate stagename ["enable"; "disable" ] 0 c1 = 1)
    let ww = WF 1 "cv3" ww "Starting"


    //vprintln 2 (bevelab_banner)
    if disabled
    then
        vprintln 1 "rtl_repipeline: Stage is disabled"
        vms_in
    else                
        //let zz = cmdline_flagset_validate "skip-propagate" ["false"; "true" ] 0 c1
    let dump_straightaway = Some "dropping2"
    let vms_out = rtl_repipeline_main ww op_args c1 vms_in
    let ww = WF 1 "rtl_repipeline" ww "Finished"        
    vms_out
    


let install_rtl_repipeline() =
    let op_args__ =
            { stagename=         "rtl_repipeline"
              banner_msg=        "rtl_repipeline"
              argpattern=        rtl_repipeline_argpattern "rtl_repipeline"
              command=           None //"rtl_repipeline"
              c3=                []
            }
    install_operator ("rtl_repipeline",  "Further maniplulations of RTL designs held in hbev form.", opath_rtl_repipeline, [], [], rtl_repipeline_argpattern "rtl_repipeline")        

// eof

