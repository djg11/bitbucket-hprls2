// cv3import.fs
// (C) 2015 DJ Greaves, University of Cambridge, Computer Laboratory.
//

//   cv3import -- Import RTL parse tree from old Tentech tools.  Please use cv3xrtl_import instead now. Delete this one.

module cv3import

open Microsoft.FSharp.Collections
open System.Collections.Generic


open yout
open moscow
open verilog_hdr
open meox
open hprls_hdr
open abstract_hdr
open verilog_gen
open verilog_render
open opath_hdr
open abstracte 
open hprxml


// Local
open cxverhdr

let denet = function
    | X_bnet ff -> ff
    | other -> sf(sprintf  "denet other %A" other)


let lid_import = function
    | CX_lid_wire   -> (LOCAL, VNT_WIRE)
    | CX_lid_reg    -> (LOCAL, VNT_REG)
    | CX_lid_input  -> (INPUT, V_IN)
    | CX_lid_regout -> (OUTPUT, V_OUT)
    | CX_lid_output -> (OUTPUT, V_OUT)
    | CX_lid_inout  -> (INOUT, V_INOUT)
    | other ->
        hpr_yikes(sprintf "cv3import: Treat other lid form as REG %A" other)
        (LOCAL, VNT_REG)


// Kludge: Where a net has been defined with the same name but different properties, we must autorename it, sadly. Memoising heaps no good at name space currently!  Please fix.
let autorename_net w io id =
    let (found, ff) = g_netbase_ss.TryGetValue id
    if not found then id
    else
        if ff.is_array || ff.is_fifo then sf "L86-array or FIFO"
        if ff.width <> w then sf "L86-width mismatch"
        let (found, ov) = g_netbase_nn.TryGetValue ff.n
        if (found) then
            match (snd ov).xnet_io with
                | a when a=io  -> id
                | INPUT -> "i_" + id
                | OUTPUT -> "o_" + id
                | _  -> "l_" + id                                
        else id


let cv3_import_net ww net =
    let san = string_fold_and_sanitize_no_leading_digit 65 [ '_' ]  // Remove the dots 
    match net with
        | CX_nsid(id, lid_form) -> // Scalar net
            let (io, _) = lid_import lid_form 
            let id = autorename_net 1 io id
            let net = ionet_w(san id, 1, io, Unsigned, [])
            
            let ff = denet net
            (V_NET(ff, ff.id, -1), lid_form)

        | CX_nvid(id, hh, 0, lid_form, is_signed) -> // Register (vector) net
            let (io, _) = lid_import lid_form 
            let ss = if is_signed then Signed else Unsigned
            let id = autorename_net (hh+1) io id
            let ff = denet(vectornet_ws(san id, hh+1, ss))
            (V_NET(ff, ff.id, -1), lid_form)
            
        | other -> sf(sprintf "Other net form %A" other)


let rec cv3_import_evc ww nets = function
    | CX_K_or(ll, rr) ->
        let a = cv3_import_evc ww nets ll 
        let b = cv3_import_evc ww nets rr
        V_EVC_OR(b, a) // Reverse as a hack to 'forget' asynch reset on rtl_resynth of blif for now.
    | CX_hash(number) -> V_EVC_HASH(cv3_import_exp ww nets number)
    | CX_K_posedge(net) -> V_EVC_POS(cv3_import_exp ww nets net)
    | CX_K_negedge(net) -> V_EVC_NEG(cv3_import_exp ww nets net)
    | CX_Anyedge(net)   -> V_EVC_ANY(cv3_import_exp ww nets net)            
    | other -> sf(sprintf "cv3_import_evc: Other CX form %A" other)            

and cv3_import_exp ww nets arg =

    let gprec = function
        | CX_attr(width, is_signed, is_downto, 0) ->
            let signed = if is_signed then Signed else Unsigned
            { g_default_prec with widtho=Some width; signed=signed }
        | other -> sf(sprintf "cb3_import_exp: other prec %A" other)


    let diadic_p oo lhs rhs prec =
        let (lhs, rhs) =  (cv3_import_exp ww nets lhs, cv3_import_exp ww nets rhs)
        match oo with // The following forms are for i/o modes only and not supported or at least  deprecated in internal forms.
            | V_DGTD -> V_DIADIC(prec, V_DLTD, rhs, lhs) // Swap args.
            | V_DGED -> V_DIADIC(prec, V_DLED, rhs, lhs) // Again swap args.
            | V_DNED -> V_LOGNOT(V_DIADIC(prec, V_DEQD, lhs, rhs))
            | oo     -> V_DIADIC(prec, oo, lhs, rhs)

    let diadic oo lhs rhs ats =
        let prec = gprec ats
        diadic_p oo lhs rhs prec

    match arg with
        | CX_nsid _
        | CX_nvid _          -> fst(cv3_import_net ww arg)
        | CX_sizednumber (nn, CX_attr(width, is_signed, is_downto, 0)) -> V_NUM(width, is_signed, "d", xi_num nn)
        | CX_text ss         -> V_STRING(xi_string ss, ss)
        | CX_lognot(arg)     -> V_LOGNOT(cv3_import_exp ww nets arg)
        | CX_simplecat(items, n_) ->
            let qf (arg, count)=  (count, cv3_import_exp ww nets arg)
            V_CAT(map qf items)
        | CX_extract(arg, baser, width) ->
            let arg = cv3_import_exp ww nets arg
            let rr = baser
            let ll = baser + width - 1
            V_BITSEL(arg, ll, rr)
        | CX_query(gg, lhs, rhs, _) ->
            let (lhs, rhs) =  (cv3_import_exp ww nets lhs, cv3_import_exp ww nets rhs)
            let gg         =  cv3_import_exp ww nets gg
            V_QUERY(gg, lhs, rhs)
            
// V_LSHIFT | V_ArithRSHIFT | V_LogicalRSHIFT  ... missing

        | CX_plus(lhs, rhs, ats)     -> diadic V_PLUS lhs rhs ats
        | CX_minus(lhs, rhs, ats)    -> diadic V_MINUS lhs rhs ats
        | CX_rem(lhs, rhs, ats)      -> diadic V_DMOD lhs rhs ats
        | CX_multiply(lhs, rhs, ats) -> diadic V_TIMES lhs rhs ats 
        | CX_divide(lhs, rhs, ats)   -> diadic V_DIVIDE lhs rhs ats
        // Bitwise operators
        | CX_binand(lhs, rhs, ats)   -> diadic V_BITAND lhs rhs ats
        | CX_binor(lhs, rhs, ats)    -> diadic V_BITOR lhs rhs ats
        | CX_binxor(lhs, rhs, ats)   -> diadic V_XOR lhs rhs ats                
        // Logic operators
        | CX_logand(lhs, rhs)        -> diadic_p V_LOGAND lhs rhs g_bool_prec
        | CX_logor(lhs, rhs)         -> diadic_p V_LOGOR lhs rhs g_bool_prec        
        // Arithmetic comparison operators:
        | CX_deqd(lhs, rhs)            -> diadic_p V_DEQD lhs rhs g_bool_prec
        | CX_dgtd(lhs, rhs, signed_)   -> diadic_p V_DGTD lhs rhs g_bool_prec // Signed has to be re-detected - lost in import.
        | CX_dged(lhs, rhs)            -> diadic_p V_DGED lhs rhs g_bool_prec

        | CX_systemfunctioncall(fname, args, rt) ->
            let args = map (cv3_import_exp ww nets) args
            let callers_flags = g_null_callers_flags
            let def = hpr_native (verilog_un_plimap fname)
            vprintln 0 (sprintf "Import CX_systemfunctioncall %s with %i args."  fname (length args))
            V_CALL(callers_flags, (def, None), args) // This has the Verilog name.
            // xi_apply (hpr_native fname, args)

            
        | other              -> sf(sprintf "cv3_import_exp: Other form %A" other)                   


// Behavioural RTL import
let rec cv3_import_seq ww nets cx cc =
    match cx with
        | CX_event_control(evc, bev) ->
            let evc = cv3_import_evc ww nets evc
            let bev = cv3_import_seq ww nets bev cc
            (V_EVC evc) :: bev
        | CX_linepoint _ -> cc  // Ignored for now
        | CX_block(id, decls, bevlst) ->
            if not_nullp decls then muddy "decs in block"
            let lst = List.foldBack (cv3_import_seq ww nets) bevlst []
            (V_BLOCK lst)::cc
        | CX_simf(pli_fname, args) ->
            let args = map (cv3_import_exp ww nets) args
            let native_fun_name = verilog_un_plimap pli_fname
            //let def = hpr_native (verilog_un_plimap fname)
            match builtin_fungis native_fun_name with
                | None ->
                    hpr_yikes(sprintf "Failed to import native pli call %s with %i args."  native_fun_name (length args))
                    cc
                | Some native_fun_def ->
                    vprintln 0 (sprintf "Import native pli call %s with %i args."  native_fun_name (length args))
                    V_EASC(V_CALL(g_null_callers_flags, ((native_fun_name, native_fun_def), None), args))::cc

        | CX_K_while(gg, body, ss_string_) ->
            let gg = cv3_import_exp ww nets gg
            let body = gen_V_BLOCK(cv3_import_seq ww nets body [])
            V_WHILE(gg, body, ss_string_)::cc

        | CX_K_if(gg, t_body, Some f_body) ->
            let gg = cv3_import_exp ww nets gg
            let t_body = gen_V_BLOCK(cv3_import_seq ww nets t_body [])
            let f_body = gen_V_BLOCK(cv3_import_seq ww nets f_body [])            
            V_IFE(gg, t_body, f_body)::cc

        | CX_K_if(gg, t_body, None) ->
            let gg = cv3_import_exp ww nets gg
            let t_body = gen_V_BLOCK(cv3_import_seq ww nets t_body [])
            V_IF(gg, t_body)::cc

        | CX_K_behev_assign(lhs, rhs, assop, None) ->
            let (lhs, rhs) = (cv3_import_exp ww nets lhs, cv3_import_exp ww nets rhs)
            let non_blocking = (assop = CX_ba_equals)
            (if non_blocking then V_NBA(lhs, rhs) else V_BA(lhs, rhs))::cc

        | other -> sf(sprintf "Other cx seq form %A" other)

let cv3_import_ap ww nets ap =
    match ap with
        | CX_AP(lhs, rhs, delay, pin_string) ->
            let (lhs, rhs) = (cv3_import_exp ww nets lhs, cv3_import_exp ww nets rhs) // no lmode convert needed.   

            let delay_o = Some delay
            let format = false // muddy (sprintf "biz_string=%s" biz_string)
            V_CONT((delay_o, format), lhs, rhs)
        | other -> sf(sprintf "Other ap lhs form %A" other)

        
let cv3_import_main ww op_args control dump_straightaway dname =
    let (kind, ast00) =
        match g_canned_verilog_asts.lookup dname with
            | Some ans -> ans
            | None ->
                vprintln 0 (sprintf "Available designs are:")
                let rep ss = vprintln 0 (sprintf "  Available design %s" ss)
                for k in g_canned_verilog_asts do rep k.Key done
                cleanexit(sprintf "Cannot open canned ast %s" dname)
            

    let kandr = control_get_s op_args.stagename control "cv3import-kandr" (Some "enable") = "enable"
    let ww = WF 1 "cv3import" ww (sprintf "Start import '%s' from cv3 ast." kind)
    let (contacts, bev00, ap_lst00, nets00, _) = ast00
    let contacts = contacts |> Array.toList 
    let nets00 = nets00 |> Array.toList
    //    vprintln 0 (sprintf "Printing whole ast: %A" ast00)
    let nets = map (cv3_import_net ww) (contacts @ nets00) // Ignore distinction and filter them later as needed.
    let seqs = List.foldBack (cv3_import_seq ww nets) (bev00 |> Array.toList) []
    let aps = map (cv3_import_ap ww nets) (ap_lst00 |> Array.toList)

    let ww = WF 1 "cv3import" ww (sprintf "Finished translate from cv3 ast.  %i nets. %i seqs. %i combs." (length nets) (length seqs) (length aps))


    let net_decls = 
        let to_netdecl (V_NET(ff, _, _), lid) =
            let (io_, vnt) = lid_import lid
            let f2 = lookup_net2 ff.n
            let vtype = f2.vtype
            let m = f2.xnet_io
            let alias = ""
            let init_o = None
            let delay_o = None // DELETED above: please restore.
            let array_max = asize f2.length - 1L // -1L for not an array
            V_NETDECL(X_bnet ff, delay_o, ff, encoding_width (X_bnet ff), alias, array_max, vnt, init_o)
        map to_netdecl nets

    let code_seq = map (fun x ->V_INITIAL x) seqs // Verilog's 'always' has been converted to 'initial forever' in most cases.
    let code_comb = aps //map (fun (l, r) ->V_CONT((None, false), l, r)) aps
    let vlnv = { vendor= !g_m_xact_op_vendor; version= !g_m_xact_op_version;  library="RTL-IMPORT"; kind=[kind] }    
    let lst =
        let pf x = (x, "silly") // why?
        map pf (net_decls @ code_seq @ code_comb)
    let hbev = rtl_presim ww (vlnv, lst) ([], [])  // This performs reset factorisation and so on.
    let ww = WF 1 "cv3import" ww (sprintf "Finished import '%s' to hbev from cv3 ast." kind)

    if not_nonep dump_straightaway then
        let ww = WF 1 "cv3import" ww (sprintf "Roundtrip render start for '%s' to hbev from cv3 ast." kind)
        let output_module_name = valOf dump_straightaway
        let (filename, file_suffix) = (output_module_name, ".v")

        let bag = { g_null_ddctrl with kandr=kandr }
        let pagewidth = 132 
        let fvv:(int * ddctrl_t) = (pagewidth, bag) // Silly place to put page width. Pass in separately please.

        let timescale = ""
        let aux_messages = []
        let add_aux_reports = true
        let items = lst
        let aliases = []
        let (gv_nets, lv_nets) =
            let pred(net, lid) =
                let (io, _) = lid_import lid // This is an unnecessary second import ... see net_decls
                isio io
            groom2 pred nets

        let gv_nets =
            let cpi = { g_null_db_metainfo with kind= "cv3import"; form= DB_form_external }
            let db_netwrap_null (net, lid) = VDB_formal(net)
            gec_VDB_group(cpi, map db_netwrap_null gv_nets)

        let lv_nets =
            let cpi = { g_null_db_metainfo with kind= "cv3import"; form= DB_form_local }
            let db_netwrap_null (net, lid) = VDB_actual(None, net)
            gec_VDB_group(cpi, map db_netwrap_null lv_nets)

        let newpramdefs = [] 
        let hh_level = 0
        //rtl_output2 ww' fv1.ddctrl filename fv1.timescale auxlist fv1.add_aux_reports
        let netinfo_dir = new Dictionary<string, netinfo_t>()

        //TODO nothing in the dict
        //app v2_make_directory (formals @ locals @ newpramdefs @ pramdefs)
        
        let vmods = [(netinfo_dir, "asrf_", output_module_name, fvv, hh_level, (aliases, newpramdefs, gv_nets, lv_nets), items)]
        rtl_output2 ww bag (filename, file_suffix) timescale aux_messages add_aux_reports vmods
        let ww = WF 1 "cv3import" ww (sprintf "Roundtrip render finish for '%s' to hbev from cv3 ast." kind)
        ()


    let ww = WF 1 "cv3import" ww (sprintf "Finished render '%s' to hbev from cv3 ast." kind)        
    //vprintln 0 ("cv3import: Finished")    

    let (decls, execs) = hbev
    let kind_vlnv = { vendor= !g_m_xact_op_vendor; version= !g_m_xact_op_version; library= !g_m_xact_op_library; kind=[kind] }
    let minfo = { g_null_minfo with name=kind_vlnv; }
    let ii = { g_null_vm2_iinfo with iname=kind; generated_by=op_args.stagename;  definitionf=true }    
    let ans = (ii, Some(HPR_VM2(minfo, decls, [], execs, [])))
    ans


let cv3import_argpattern =
   [
       Arg_enum_defaulting("cv3import-kandr", ["enable"; "disable"], "enable", "Enable Kernighan and Ritchie style (pre-2001) Verilog port syntax");
       Arg_enum_defaulting("cv3import-dump-straightaway", ["enable"; "disable"], "disable", "Enable Kernighan and Ritchie style (pre-2001) Verilog port syntax");       
   ] 
 
// This is the top-level instance of this plugin
let opath_cv3import ww op_args vms_in =
    let c1:control_t = op_args.c3
    let stagename = op_args.stagename
    let disabled = (cmdline_flagset_validate stagename ["enable"; "disable" ] 0 c1 = 1)
    let ww = WF 1 "cv3" ww "Starting"


    //vprintln 2 (bevelab_banner)
    if disabled
    then
        vprintln 1 "cv3import: Stage is disabled"
        vms_in
    else                
        //let zz = cmdline_flagset_validate "skip-propagate" ["false"; "true" ] 0 c1
    let dump_straightaway = Some "dropping2"



    let all_benchmark_names =
        let m_all_names = ref []
        for k in g_canned_verilog_asts do (mutadd m_all_names k.Key) done // Please can we fold over these !
        !m_all_names

    let dname0 = "s1196_bench"
    let dname1 = "s1238_bench" // had a nice speedup result = no pipeline?

    let dnames =
        if false then ["s1238_bench"]
        elif true then rev(lst_subtract all_benchmark_names bad)
        else [dname0; dname1; hd all_benchmark_names]

    vprintln 0 (sprintf "Processing %i benchmarks" (length dnames))
    let answers = map (cv3_import_main ww op_args c1 dump_straightaway) dnames

    let ww = WF 1 "cv3" ww "Finished cv3 import."        
    answers @ vms_in
    



let install_cv3import() =
    let op_args__ =
            { stagename=         "cv3import"
              banner_msg=        "cv3import"
              argpattern=        cv3import_argpattern
              command=           None //"cv3import"
              c3=                []
            }
    install_operator ("cv3import",  "Import RTL parse tree from old Tentech tools", opath_cv3import, [], [], cv3import_argpattern);        

// eof

