// cv4.fs
//
// ?? A top-level for RTL import ?
//
// cv3import.fs?
// (C) 2015 DJ Greaves, University of Cambridge, Computer Laboratory.

open Microsoft.FSharp.Collections
open System.Collections.Generic


open yout
open moscow
open verilog_hdr
open meox
open hprls_hdr
open abstract_hdr
open verilog_gen
open verilog_render
open opath_hdr

open cxverhdr
// open vspool

let g_cv4_banner = "cv4 RTL tooling - V0.0"

let cv4_temp_args =
   [

   ] 



// Need to do auto plugin scanning
open verilog_gen
open cpp_render
open opath
open hprxml




[<EntryPoint>]
let main (argv : string array) =
    g_version_string := g_cv4_banner
    loadPlugins() // This is supposed to autoload most of the below!
    ignore(conerefine.install_coneref())
    //bevelab,install_bevelab()

    install_verilog()
    //cv3import.install_cv3import()
    cv3xrtl_import.install_cv3xrtl_import()
    rtl_resynth.install_rtl_resynth()
    rtl_repipeline.install_rtl_repipeline()
    rtl_jbdd_experiments.install_rtl_jbdd_experiments()                
    stimer.install_stimer()
    //restructure.install_restructure()
    diosim.install_diosim()
    pandexer.install_pandexer()
    install_c_output_modes()
    let rc = opath_main ("cv4", argv)
    rc






// eof
