#!/bin/bash
set -x

CV4PRIV_DISTRO=$HPRLS/cv4/pvdist

# remove.v suffix - bash syntax
BASE=${1%.v}
# echo base $(BASE)
# Use this now as the pre-processor to generate XML from verilog files:

# /home/djg11/d320/csimloved/csimloved/csnox -listxseq-name ${1}  -nosim  -root ${BASE}_bench  ${BASE}.v

/home/djg11/d320/csimloved/csimloved/csnox -listxseq-name ${BASE}_XML  -nosim  -root ${BASE}  ${BASE}.v


# Not this:
# FSHARPC_FIX="/nowarn:75 /consolecolors- /nologo  /lib:. --nowarn:25,64  --noframework /r:System.Numerics.dll /r:System /r:System.Xml.dll /r:System.Core.dll" 

# Old automation for pre-xml route from csim4 that compiled the RTL AST to a heap structure.
#    rm -f vspool.fs vspool.dll $PRIV_DISTRO/lib/vspool.dll
#    csnox -listseqs -nosim -root $1  $1.v
#    fsharpc $FSHARPC_FIX /target:library /r:$PRIV_DISTRO/lib/cxverhdr.dll vspool.fs
#    rm -f FSharp.Core.dll
#    mv vspool.dll $PRIV_DISTRO/lib

MPROF=--profile=log,heapshot,sample
#-. If the output option is specified as well, the report will be written to the output file instead of the cons
#MPROF=--profile=log
#  --aot
export MONO_GC_PRAMS=max-heap-size=32G,nursery-size=12G
time mono-sgen -O=all ${MPROF} $CV4PRIV_DISTRO/lib/cv4.exe -sim=500 -vnl=$1-faster -vnl-rootmodname=$BASE -conerefine=disable ${BASE}_XML.xml
# /tmp/benchmarks-compiled/s953.dll

# eof
