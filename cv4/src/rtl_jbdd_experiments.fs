// rtl_jbdd_experiments.fs
// (C) 2022 DJ Greaves, University of Cambridge, Computer Laboratory.
//
//   rtl_jbdd_experiments

module rtl_jbdd_experiments

open Microsoft.FSharp.Collections
open System.Collections.Generic


open yout
open moscow
open verilog_hdr
open meox
open hprls_hdr
open abstract_hdr
open verilog_gen
open verilog_render
open opath_hdr
open abstracte
open cxverhdr
open dotreport
open jbdd

let quadToStr (px, l, r, _) = xToStr l + sprintf "<-%i-" px + xToStr r

//
let g_local_clk_to_q_time = 0.25

type eE_t = {
    m_csv_lines:        (string * string) list ref // For spreadsheet output and general reporting.
    dname:              string
    jbx:                jbx_t
    control:            control_t
    }



let fifo_tester_shim_101 ww eE modelnets =
    let _:(string * hexp_t) list = modelnets
    let jbx = eE.jbx
    let width = 4 // Test 4 bits only?
    let log_depth = 2
    let depth = int32(two_to_the log_depth)
    let msg = sprintf "fifo_tester_shim_101  width=%i  depth=%i" width depth
    let ww = WF 1 msg ww (sprintf "start")
    

    
    let localnet (net_id, net_width) =
        let hexp_t_net = vectornet_w(net_id, net_width)
        let bdd_net = 0
        (hexp_t_net, bdd_net)

    let modelmine (net_id:string) = // For a connection to the DUT, we look it up in the DUT's formals, since this checks it really exists.
         match op_assoc net_id modelnets with
            | Some net ->
                let bdd_net = 0
                (net, bdd_net)
            | None ->
                vprintln 0 (sprintf "Available nets are %s" (sfold (fst) modelnets))
                sf (sprintf "modelmine: %s: cannot find DUT net %s" msg net_id)
                

    let left_dvalid = modelmine "left_dvalid"
    let left_drdy = modelmine "left_drdy"
    let left_ddata = modelmine "left_ddata"    
    let right_dvalid = modelmine "right_dvalid"
    let right_drdy = modelmine "right_drdy"
    let right_ddata = modelmine "right_ddata"    

    // Variables/regs in the "shim"
    let reg_sd = localnet("Rsd", width)
    let reg_level = localnet("Rlevel",  depth+1) // Add one one bit to be nice and safe
    let reg_sl = localnet("Rsl",  depth+1) // Add one one bit to be nice and safe    
    let reg_h = localnet("Rh", 1)

    //let unification_varlist = jb_And jbx (map snd [left_ddata; left_dvalid; right_drdy; reg_sd; reg_sl; reg_h])

    let fing (net, _) = xi_orred net
    let enq = ix_and (fing left_dvalid) (fing left_drdy)
    let deq = ix_and (fing right_dvalid) (fing right_drdy)

    let rl_eqn =
        let a1 = ix_query (enq) (ix_plus (xi_num 1) (fst reg_level)) (fst reg_level)
        let a2 = ix_query (deq) (ix_minus (xi_num 1) a1) a1                      
        ix_deqd (xi_X (-1) (fst reg_level)) a2

    let capture =
        ix_andl [enq; ix_deqd (fst left_ddata) (fst reg_sd); ix_deqd (fst reg_level) (fst reg_sl); xi_not (xi_orred (fst reg_h))]

    let replay =
        let antecedent = ix_andl [deq;  ix_deqd (fst reg_level) (fst reg_sl); xi_orred (fst reg_h)]
        let consequent = ix_deqd (fst right_ddata) (fst reg_sd);
        ix_implies antecedent consequent

    let assertion = ix_implies capture replay

// Does not prohibit counter overflow ! TODO
// h control not included TODO    

    let ww = WF 1 "fifo_tester_shim_101" ww (sprintf "convert shim to bdd")
    let shim_bdd = 
        let pins = pandex_binary.new_pandex_pins msg
        let pep = pandex_binary.new_pandex_dp eE.control (*ternary*)None 
        let pep = { pep with ripple_adder_radix=1; pinopt=Some pins }
        let ll_main = bpandex_local ww pep msg  (ix_andl [rl_eqn; assertion ])

        let ll_pin_assigns =
            let fpin (lhs, rhs) =
                vprintln 3 (sprintf "   adding an ll_pin_assign for %s" (xToStr lhs))
                ix_deqd lhs rhs // A further list of combinational constraints
            ix_andl (map fpin !pins.m_newassigns)

        let axx = jbdd_bstore jbx msg 0 (ix_and ll_main ll_pin_assigns)
        axx

    let ww = WF 1 "fifo_tester_shim_101" ww (sprintf "convert shim to bdd finished.")
    shim_bdd

//------------------------------------        


// Insert bev input structures in the powers matrix database (powers_db).
// This may not be used really, since input is supposed to be RTL!
let jbdd_experiments_xbev_insert ww (eE:eE_t) (ast_ctrl:ast_ctrl_t) (cc, cd) bev =
    muddy "NOT_LIKELY_TO_BE_NEEDED_FOR_RTL_INPUT" // Code copied from rtl_repipeline
    
#if NOT_LIKELY_TO_BE_NEEDED_FOR_RTL_INPUT
    let ww = WF 2 "jbdd_experiments_xbev_insert" ww (sprintf "Start %s" dname)
    // Generic power's array - 2-D: net and power, along with CE form.   We can then project through the next-state function, like hypermode.


    let rec simple_bev_compile nox gg cmd =
        match cmd with
            | [] -> nox
            | Xif(g1, ct, cf) :: tt ->
                let n0 = simple_bev_compile nox (ix_and g1 gg) [ct]
                let n1 = simple_bev_compile nox (ix_and (xi_not g1) gg) [cf]
                let max2(a,b) = if a>b then a else b
                simple_bev_compile (max2(n0, n1)) gg tt 
            | Xcomment _ :: tt 
            | Xskip :: tt     -> simple_bev_compile nox gg tt
            | Xblock(v) :: tt -> simple_bev_compile nox gg (v @ tt)
                 // Use counter nox to ensure only one assignment in any control flow, otherwise blocking and non-blocking resolution will be needed.
                 // Xeasc clause missing ::  should be converted to PLI here


            | Xassign(X_x(lhs, 1, _), rhs)::tt ->
                insert_power ww eX lhs 1 gg rhs    // Sequential logic   
                simple_bev_compile (nox+1) gg tt

            | Xassign(lhs, rhs)::tt -> // Combinational logic
                insert_power ww eX lhs 0 gg rhs 
                simple_bev_compile (nox+1) gg tt

            | other::tt ->
                hpr_yikes("rtl_jbdd_experiments: cannot effect a simple conversion of behavioural command (need an HLS expansion please) " + hbevToStr other)
                nox

    let g00 = X_true
    let _  = simple_bev_compile 0 g00 [bev]
    ()
#endif

type ex_t =
    { jbx: jbx_t
      name: string
    }

let insert_arc ww eX  (dP, support, combfuns, seqfuns, comb_nets, seq_nets, resets) power guard lhs rhs  = // Sequential logic
    vprintln 0 (sprintf "Inserting an ARC power=%i  guard=%s  lhs=%s" power  (xbToStr guard) (xToStr lhs))
    //let jbx = eX.jbx
    let support = xi_bsupport dP support guard
    let support = xi_support dP support lhs
    let support = xi_support dP support rhs  

    let arcToStr() =
        sprintf "Power=%i.\n   lhs=%s\n   \ng=%s\   rhs=%s" power (xToStr lhs) (xbToStr guard) (xToStr rhs)
    let (comb_nets, seq_nets, resets) =
        match power with
            | 0 -> ((xi_driven [] lhs) @ comb_nets, seq_nets, resets)
            | 1 ->
                let lnets = xi_driven [] lhs
                let resets =
                    match lnets with
                        | [((X_bnet ff) as bnet, lanes)] ->
                            let f2 = lookup_net2 ff.n
                            match at_assoc "init" f2.ats with // cv3 rtl import should have removed reset logic and saved reset properties in the "init" field.
                                | None -> resets
                                | Some ix ->
                                    // let tobn = BigInteger.Parse // TODO - constant inits larger than 64 bits
                                    let rv = atoi64 ix
                                    ((bnet, lanes), rv)::resets

                        | other ->
                            sf (sprintf "insert_arc: unexpected lhs for %s. other=%A" (xToStr lhs) other)
                            resets
                (comb_nets, lnets @ seq_nets, resets)
            | nn -> sf "Bad insert_arc power"
    //dev_println (sprintf "seq_nets now %A" seq_nets)
    let ids = xToStr lhs
    let l1 = jbdd_store eX.jbx ids power lhs // power is 1 for sequential assigns: the lhs needs priming
    let g1 = jbdd_bstore eX.jbx ids 0 guard     
    let r1 = jbdd_store eX.jbx ids 0 lhs
    let arc = jb_implies eX.jbx g1 (jb_deqd eX.jbx l1 r1)

    let (combfuns, seqfuns) =
        match power with
            //| _ -> (combfuns, seqfuns)  // FOR NOW
            | 0 -> (jb_and eX.jbx combfuns arc, seqfuns)
            | 1 -> (combfuns, jb_and eX.jbx seqfuns arc)            
            | nn -> sf "Bad insert_arc power"


    if jb_falsep combfuns then
        vprintln 0 (sprintf "Alert: combinational assignments become inconsistent after inclusion of %s" (arcToStr()))

    if jb_falsep seqfuns then
        vprintln 0 (sprintf "Alert: NSF becomes inconsistent after inclusion of %s" (arcToStr()))

    // TODO could also consider the conjuniton of the two and at what point that becomes inconsistent.

    //eX.jbx.jb_evictall()
    (dP, support, combfuns, seqfuns, comb_nets, seq_nets, resets)

    
// Insert RTL input structures in the powers matrix database (powers_db).
let jbdd_experiments_rtl_insert ww vd powers_db eE rtl (cc, cd) =
    let msg = "jbdd_experiments_rtl_insert " + eE.dname
    let ww = WF 2 msg ww (sprintf "Start")
    let eX = { jbx=eE.jbx; name=eE.dname }


    let insert1 gg arg (cc, cd) =
        match arg with 
        | Rnop ss ->
            (arg :: cc, cd)
        | Rpli(guard, (fgis, _), args) ->
            dev_println (sprintf "jbdd_experiments_rtl_insert: Ignore PLI calls for now")
            (cc, cd)

        | Rarc(guard, X_x(_, lhs, _), rhs) ->  muddy "insert L183"
        | Rarc(guard, lhs, X_x(_, rhs, _)) ->  muddy "insert L184"            

        | Rarc(guard, lhs, rhs) ->
            let cd = insert_arc ww eX cd 1 (ix_and gg guard) lhs rhs // Sequential logic (or comb if X^-1)
            (cc, cd)

        //| other -> sf (sprintf "Insert1: Other : %A" other)

    let insert arg (cc, cd) =
        match arg with
            | XIRTL_buf(gg, lhs, rhs) ->  // Combinational logic
                let cd = insert_arc ww eX cd 0 gg lhs rhs // Need to collate on lhs please ...
                (cc, cd)
                
            | XRTL(Some(ll, rr), g1, lst) ->
                List.foldBack (insert1 (ix_and g1 (ix_deqd ll rr))) lst (cc, cd)
            | XRTL(None, g1, lst) ->
                List.foldBack (insert1 g1) lst (cc, cd)

            | other -> sf (sprintf "Other : %A" other)

    //let rtl = rev rtl
    let (spare_rtl, cd) = List.foldBack insert rtl ([], cd)

    let _ = 
        let (_dp, support, combfuns, seqfuns, comb_nets, seq_nets, resets) = cd
        let sdpairToStr (net, lanes) = xToStr net + ":" + sfold xToStr lanes
        reportx 3 (msg + ": support") sdpairToStr support
        reportx 3 (msg + ": comb-assigned") sdpairToStr comb_nets
        reportx 3 (msg + ": seq-assigned") sdpairToStr seq_nets
        vprintln 0 (msg + sprintf ": %i single-bit nets" (length support))
        vprintln 0 (msg + sprintf ": %i single-bit combinationally assigned nets" (length comb_nets))
        vprintln 0 (msg + sprintf ": %i single-bit sequentially assigned nets" (length seq_nets))

        
    let cc = if nullp spare_rtl then cc else XRTL(None, X_true, spare_rtl) :: cc
    //let cc = spare_rtl @ cc
    (cc, cd)




let rec jbdd_experiments_h2sp_insert ww vd dir eE arg (cc, cd) =
    match arg with
        | SP_l(ast_ctrl, bev) ->
            muddy "jbdd_experiments_xbev_insert ww eE ast_ctrl (cc, cd) bev"
            //(cc, cd)

        | SP_par(pstyle, lst) ->
            let (cc, cd) = List.foldBack (jbdd_experiments_h2sp_insert ww vd dir eE) lst (cc, cd)
            //SP_par(pstyle, ans_)
            (cc, cd)

        | SP_seq(lst) -> // Seq semantic is discarded! Treated as par.
            let (cc, cd) = List.foldBack (jbdd_experiments_h2sp_insert ww vd dir eE) lst (cc, cd)
            //SP_seq(ans_)
            (cc, cd)

        | SP_rtl(ii, rtl)        ->
            let (cc1, cd) = jbdd_experiments_rtl_insert ww vd dir eE rtl ([], cd)
            let cc = if nullp cc1 then cc else SP_rtl(ii, cc1)::cc
            (cc, cd)

        | SP_dic(dic, ctrl) -> 
            muddy "DIC structure is lost ... not worthwhile... ?"
#if UNDEF
            let dic' = Array.create dic.Length (Xskip)
            let ww' = WN ("SP_dic " + ctrl.ast_ctrl.id) ww 
            let xlat b =
                let ast_ctrl = muddy "dic_ctrl_to_ast_ctrl ctrl"
                let ans = jbdd_experiments_xbev_insert ww' eE ast_ctrl b
            let _ =
                let jjbdd_experiments p = Array.set dic' p (xlat dic.[p])
                app jjbdd_experiments [0..dic.Length-1]
            //SP_dic(dic', ctrl)
#endif
            (cc, cd)

        | SP_comment ss -> // Can nicely pass comments through ?
            (arg::cc, cd)

        //| SP_asm _ -> arg
        | other  -> muddy("jbdd_experiments_h2sp other form:" + hprSPSummaryToStr other)


let jbdd_experiments_dir ww eE dir =
    //muddy (sprintf "Dir=%A" dir)
    dir


let jbdd_experiments_insert_exec ww vd m_dir eE (H2BLK(dir, sp_in)) (cc, cd) =
    let ww = WF 2 "rtl_jbdd_experiments" ww (sprintf "jbdd_experiments_insert_exec: Start exec block for " + eE.dname)

    if nullp dir.clocks then ()
    else
        if not_nonep !m_dir && valOf !m_dir <> dir then
            vprintln 0 (sprintf "Clock domain 0 %A" (dirToStr false (valOf !m_dir)))
            vprintln 0 (sprintf "Clock domain 1 %A" (dirToStr false dir))
            //vprintln 0 (sprintf "Clock domain 0 %A" (valOf !m_dir))
            //vprintln 0 (sprintf "Clock domain 1 %A" dir)                        
            muddy "Two different directors or two or more clock domains"
        m_dir := Some dir // Crude --- multiple clock domains risk!

    let (passons, cd) = jbdd_experiments_h2sp_insert ww vd dir eE sp_in ([], cd)

    let ww = WF 2 "rtl_jbdd_experiments" ww (sprintf "rsynth_insert_exec: Finished exec block")    
    let gen sp = H2BLK(dir, sp)
    ((map gen passons) @ cc, cd)



let to_jbdd_form ww eE newkey execs = 
    let vd = 3
    let ww = WF 2 "rtl_jbdd_experiments" ww ("Start convert to jbdd form.")
    let null_vec =
        let support = []
        let (combfuns, seqfuns) = (g_jb_true, g_jb_true)
        //let (combfuns, seqfuns) = (g_jb_false, g_jb_false) /// FOR NOW !        
        let (comb_nets, seq_nets, resets) = ([], [], [])             
        (new_dP(), support, combfuns, seqfuns, comb_nets, seq_nets, resets)


    let m_dir = ref None // List of directors (clock domains)
    let (passons, populated_vec) = List.foldBack (jbdd_experiments_insert_exec ww vd m_dir eE) execs ([], null_vec)

    let (_dp, support, combfuns, seqfuns, comb_nets, seq_nets, resets) = populated_vec
    
    let double_driven =
        let qx (xnet, ivals) = (int64(x2nn xnet), ivals) // sd_pair instead of sd_pair_t. Oh dear.  
        sd_intersection(map qx comb_nets, map qx seq_nets)    


    let msg = "Final count " + eE.dname
    vprintln 0 (msg + sprintf ": %i single-bit nets" (length support))
    vprintln 0 (msg + sprintf ": %i single-bit combinationally assigned nets" (length comb_nets))
    vprintln 0 (msg + sprintf ": %i single-bit sequentially assigned nets" (length seq_nets))
    vprintln 0 (msg + sprintf ": %i single-bit reset pairs" (length resets))     
    vprintln 0 (msg + sprintf ": %i single-bit both!! assigned nets" (length double_driven))

    let _ = 
        let sdpairToStr (net, lanes) = xToStr net + ":" + sfold xToStr lanes
        reportx 3 (msg + ": support") sdpairToStr support
        reportx 3 (msg + ": comb-assigned") sdpairToStr comb_nets
        reportx 3 (msg + ": seq-assigned") sdpairToStr seq_nets

    let modelnets = map fst support
    let modelnets = map (fun x -> (xToStr x, x)) modelnets
    let reset_net_o =  op_assoc "reset" modelnets // TODO get string name from the director

        
    if jb_falsep combfuns then
        vprintln 0 (sprintf "Alert: %s: combinational assignments are self-inconsistent at the global level." eE.dname)

    if jb_falsep seqfuns then
        vprintln 0 (sprintf "Alert: %s: sequential assignments are self-inconsistent at the global level." eE.dname)

    let dut_fsm_trans = jb_and eE.jbx seqfuns combfuns
    if jb_falsep dut_fsm_trans then
        vprintln 0 (sprintf "Alert: %s: dut_fsm_trans is self-inconsistent at the global level." eE.dname)


    let (support, dut_fsm_trans) =
        match reset_net_o with 
        | None ->
            vprintln 2 (sprintf "%s: No reset net found" msg) // TODO check with director
            (support, dut_fsm_trans)

        | Some reset_net->
            vprintln 2 (sprintf "%s: Removing reset net %s from general support." msg (netToStr reset_net)) 
            let sub (net, lanes) cc = (if net=reset_net then cc else (net, lanes)::cc)
            let m1 = "reset logicate"

            let reset_set_assumption   = ix_deqd (reset_net) (xi_num 1) // Assume +ve : check with director please
            let reset_asserted = (jbdd_bstore eE.jbx m1 0 reset_set_assumption)
            let m1 = "reset conditions"
            let fsm_reset_assigns = jb_simplify_assuming eE.jbx m1  (dut_fsm_trans) reset_asserted
            dev_println (sprintf "Reset condition is " + (jb_printss eE.jbx fsm_reset_assigns))
            let support = List.foldBack sub support []

            let dut_fsm_trans = jb_simplify_assuming eE.jbx m1  (dut_fsm_trans) (jb_not eE.jbx reset_asserted)

            if jb_falsep dut_fsm_trans then
                vprintln 0 (sprintf "Alert: %s: dut_fsm_trans is self-inconsistent beyond reset." eE.dname)
                         
            (support, dut_fsm_trans)

    let inputs = sd_subtract(support, sd_union(comb_nets, seq_nets))
    vprintln 0 (msg + sprintf ": %i single-bit floating input  nets" (length inputs))

    let ww = WF 2 "rtl_jbdd_experiments" ww (sprintf "compute initial state")    
    let dut_fsm_initial_state =
        let jbx = eE.jbx
        let create_reset_clause cc = function
            | (((X_bnet ff) as lhs, [X_num bitlane]), vale) when vale >= 0L || vale <= 1L ->
                let lhs_item = jb_leafnet_lev jbx "initial_state" ff bitlane 0
                let reset_vale = (if vale<>0L then g_jb_true else g_jb_false)
                jb_and jbx cc (jb_deqd jbx lhs_item reset_vale)
            | other -> sf (sprintf "create_reset_clause: other form %A" other)

        List.fold create_reset_clause g_jb_true resets

    if jb_falsep dut_fsm_initial_state then
        vprintln 0 (sprintf "Alert: %s: initial state is self-inconsistent (some net reset both high and low?)" eE.dname)

    let ww = WF 2 "rtl_jbdd_experiments" ww (sprintf "Interim...")    

    let net_to_varlist cc = function
        | ((X_bnet ff) as lhs, []) -> // Really we can just use jbdd_store since that has this functionality
            let width = encoding_width lhs
            let net_to_varlistbit cc bitlane = 
                let jbnet = jb_leafnet_lev eE.jbx "external_input" ff bitlane 0
                jb_and eE.jbx cc jbnet // This is the standard conjunct form for a varlist. Please abstract me.
            List.fold net_to_varlistbit cc [0..width-1]

        | ((X_bnet ff) as lhs, [X_num bitlane]) ->
            let jbnet = jb_leafnet_lev eE.jbx "external_input" ff bitlane 0
            jb_and eE.jbx cc jbnet
        | other -> sf (sprintf "net_to_varlist: other form %A" other)

    let dut_fsm_external_inputs =
        List.fold net_to_varlist g_jb_true inputs

    let dut_fsm_statevec  = 
        List.fold net_to_varlist g_jb_true seq_nets

    let the_dut = (dut_fsm_initial_state, dut_fsm_external_inputs, dut_fsm_statevec, dut_fsm_trans)
    
    let _ =
        let input_support = jb_support eE.jbx msg dut_fsm_external_inputs
        vprintln 0 (sprintf "%s: Input support is %s" msg (jb_printss eE.jbx input_support))
        let statevec_support = jb_support eE.jbx msg dut_fsm_statevec
        vprintln 0 (sprintf "%s: Statevec support is %s" msg (jb_printss eE.jbx statevec_support))


    if true then 
        let ww = WF 2 "rtl_jbdd_experiments" ww (sprintf "Find reachable state space.")    
        let concourse = jb_reachable eE.jbx msg dut_fsm_initial_state dut_fsm_external_inputs dut_fsm_statevec dut_fsm_trans
        if jb_truep concourse then vprintln 1 (sprintf "%s: All states are reachable" msg)
        let n_seq_bits = int(jb_count eE.jbx msg JBM_depth dut_fsm_statevec) - 1 // Discard end-of-list leaf
        let state_value_count = pown 2L (n_seq_bits) // should just be 2^|seq_nets in bit form|
        let reachable_value_count = jb_count eE.jbx msg JBM_settings concourse    // Wrong function.... need to restrict this
        vprintln 0 (sprintf "%s:  %A seq bits, %A possible states. %A reachable" msg n_seq_bits state_value_count reachable_value_count)

        let concourse_support = jb_support eE.jbx msg concourse
        vprintln 0 (sprintf "%s: Concourse support is %s" msg (jb_printss eE.jbx concourse_support))
        ()
        
    let ww = WF 2 "rtl_jbdd_experiments" ww ("Finished convert to quad form.")

    let shim_bdd = fifo_tester_shim_101 ww eE modelnets 

    let ww = WF 1 "fifo_tester_shim_101" ww (sprintf "combine shim with DUT")


    // TODO apply reset ...
    
    let full_model = jb_and eE.jbx shim_bdd  dut_fsm_trans
    
    let ww = WF 1 "fifo_tester_shim_101" ww (sprintf "Start quantify over all shim input conditions.")

    model check

    let ww = WF 1 "fifo_tester_shim_101" ww (sprintf "Printing result of quantify over all input conditions.")        
    vprintln 1 (sprintf "Missing ...")
    ()


    ()



//------------------------------------
let rtl_jbdd_experiments_main ww op_args control vms =
    let ww = WF 1 "rtl_jbdd_experiments" ww (sprintf "Start %i machines" (length vms))


    let jbdd_experiments ww arg cc =
        match arg with
        | (ii, Some(HPR_VM2(minfo, decls, sons, execs, assertions))) when ii.definitionf ->

            let m_csv_lines0 = ref []  // One of these per VM compilation since we are using one VM per benchmark.
            let dname = hptos minfo.name.kind
            mutadd m_csv_lines0 ("Design", dname)
            let ww = WF 1 "rtl_jbdd_experiments" ww (sprintf "Design=%s starting" dname)
            let ii = { ii with generated_by=op_args.stagename  }    

            let vd = 3
            let jbx = new jbx_t("name")
            let eE = { m_csv_lines=m_csv_lines0; control=control; dname=dname; jbx=jbx } // object record.

            let rx = to_jbdd_form ww eE ("original-" + dname) execs
            cc






#if SPARE
            let (domains, all_support, one_step_support, inputs) = rx
            let (seq_dtor, rtl_quads) =
                match domains with // All combs are in with the first domain for now
                | [dtor, quads] -> (dtor, quads)
                | _ -> muddy(dname +  ": Not one clock domain")


            let reset_nets = seq_dtor.resets

            let augmented_anal = repipe_rtl_quad_anal ww eE "raw" reset_nets rtl_quads

            let detrip (seq, comb) (px, lhs, rhs, _) =
                if px = 1 then (Rarc(X_true, lhs, rhs)::seq, comb)
                elif px = 0 then (seq, XIRTL_buf(X_true, lhs, rhs)::comb)
                else sf "L1204 - not seq or comb"


            let (sp_out, newnets) =
                if false then
                    let comment = XRTL_nop "cloned"
                    let (seq, comb) = List.fold detrip ([], []) rtl_quads
                    let ii = { id="cloned" }:rtl_ctrl_t
                    let sp_seq = H2BLK(seq_dtor, SP_rtl(ii, [ comment; XRTL(None, X_true, seq)]))
                    let comb_dtor = { g_null_directorate with duid=next_duid() }
                    let sp_comb = H2BLK(comb_dtor, SP_rtl(ii, comb))
                    let newnets = []
                    ([sp_seq; sp_comb], newnets)


                else
                    let (seq_nets, get_seq_support, get_delay, get_unpredictibility, get_dnet, get_fuzzyv, get_seq_assign_plus_combs_cc, reset_net, get_trans_comb_support_cc) = augmented_anal   
                    let count = length seq_nets
                    
                    let (pl, rl) =
                        if (false) then (seq_nets, []) // One extreme
                        elif (false) then ([], seq_nets) // Other extreme                        
                        else
                            partition_state_vec ww eE (rtl_quads, augmented_anal)

                    let message = sprintf "XPFORM using partition of %i into %i + %i." count (length pl) (length rl)
                    vprintln 0 message

                    reportx 3 ("PL seq nets for " + dname) netToStr pl
                    reportx 3 ("RL seq nets for " + dname) netToStr rl 
                    
                    let (logic, newnets, further_message) = prepare_and_do_transform ww eE augmented_anal (pl, rl)

                    let message = message + further_message
                    let standalone_mode_srcs = []// No external input timings to hand - will default to 0.35
                  
                    let timing = ap_timing_anal ww eE  "final result" logic standalone_mode_srcs None
                    
                    let (seq, comb) = List.fold detrip ([], []) logic
                    let newid = "spxformd"
                    let ii = { id=newid }:rtl_ctrl_t
                    let comment = XRTL_nop message

                    //let seq_dtor = jbdd_experiments_dir ww eE (valOf_or_fail "no clock domain" !m_dir)
                    let sp_seq = H2BLK(seq_dtor, SP_rtl(ii, [ comment; XRTL(None, X_true, seq)]))
                    let comb_dtor = { g_null_directorate with duid=next_duid() }
                    let sp_comb = H2BLK(comb_dtor, SP_rtl(ii, comb))
                    ([sp_seq; sp_comb], newnets)


            let newdec = // Declare new nets
                let cpi = { g_null_db_metainfo with kind= "rtl-jbdd_experiments-nets" }
                gec_DB_group(cpi, map db_netwrap_null newnets)

            // Old decls not deleted!
            let ans = (ii, Some(HPR_VM2(minfo, newdec @ decls, sons, sp_out, assertions)))

            let csv_report =
                {
                    csv_filename=       "benchmarks_report.csv"
                    csv_table_title=    "rtl_jbdd_experiments_csv_report.csv"
                    csv_lines=          [rev !m_csv_lines]
                }
            mutadd g_csv_reports csv_report // Other apps can follow this general pattern.
            let ww = WF 1 "rtl_jbdd_experiments" ww (sprintf "Design=%s finished" dname)
            ans
#endif            

        | other -> other::cc

    let vms = List.foldBack (jbdd_experiments ww) vms []
    //jb_run_unit_tests ww ()
    vms


    

let rtl_jbdd_experiments_argpattern stagename =
    [
        Arg_enum_defaulting(stagename, ["enable"; "disable"], "enable", "Enable/bypass control for this recipe stage.")
        
        Arg_enum_defaulting("rtl_jbdd_experiments-dump-straightaway", ["enable"; "disable"], "disable", "Enable Kernighan and Ritchie style (pre-2001) Verilog port syntax")       
   ] 
 
// This is the top-level instance of this plugin
let opath_rtl_jbdd_experiments ww op_args vms_in =
    let c1:control_t = op_args.c3
    let stagename = op_args.stagename
    let disabled = (cmdline_flagset_validate stagename ["enable"; "disable" ] 0 c1 = 1)
    let ww = WF 1 "cv3" ww "Starting"


    //vprintln 2 (bevelab_banner)
    if disabled
    then
        vprintln 1 "rtl_jbdd_experiments: Stage is disabled"
        vms_in
    else                
        //let zz = cmdline_flagset_validate "skip-propagate" ["false"; "true" ] 0 c1
    let dump_straightaway = Some "dropping2"
    let vms_out = rtl_jbdd_experiments_main ww op_args c1 vms_in
    let ww = WF 1 "rtl_jbdd_experiments" ww "Finished"        
    vms_out
    


let install_rtl_jbdd_experiments() =

    vprintln 0 (sprintf "Allowing hpr to use XOR gates everywhere")
    g_xor_heuristic_value := -1 // Free use of XORs (since BDD with complementaty edges supports them nicely!

    let op_args__ =
            { stagename=         "rtl_jbdd_experiments"
              banner_msg=        "rtl_jbdd_experiments"
              argpattern=        rtl_jbdd_experiments_argpattern "rtl_jbdd_experiments"
              command=           None //"rtl_jbdd_experiments"
              c3=                []
            }
    install_operator ("rtl_jbdd_experiments",  "Further maniplulations of RTL designs held in hbev form.", opath_rtl_jbdd_experiments, [], [], rtl_jbdd_experiments_argpattern "rtl_jbdd_experiments")        

// eof

