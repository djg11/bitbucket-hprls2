// TEST1

// Two-bit shift register

module TEST1(input clk, input reset, 
	     input  serin0,
	     input  serin1,
	     output serout);

   reg 		    dd1, dd2;

   
   always @(posedge clk) begin
      dd1 = serin0;
      dd2 <= dd1;
   end

   assign serout = dd2;
endmodule 

// eof
