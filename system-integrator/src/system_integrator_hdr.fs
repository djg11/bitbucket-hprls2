//
// HPR System Integrator  - (also known briefly as SoC Partitioner  and SoC Render)
// 

//
// HPR L/S Formal Codesign System.
//
//
// (C) 2017, DJ Greaves, University of Cambridge, Computer Laboratory.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer;
// redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution;
// neither the name of the copyright holders nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

module system_integrator_hdr

open moscow
open meox
open yout

// open cpp_render

open hprxml
open hprls_hdr
open abstract_hdr




type desmodule_purpose_t =
    | DF_primary
    | DF_subsidiary
    | DF_concentrator
    | DF_zonebridge
    | DF_static_port    
    | DF_adaptor

let purposeToStr x = sprintf "%A" x


let is_nailed = function // Cannot be moved between zones
    | DF_zonebridge
    | DF_static_port    -> true
    | _ -> false
    
type desport_purpose_t =
    | DP_var of string            // Available for unification
    | DP_named_group of string    // A grounded constant
    | DP_multi                    // Outside of the binding/unification process.
    | DP_filler    

let rec domToStr env = function
    | DP_var vv ->
        match op_assoc vv env with
            | None    -> vv
            | Some ss ->  sprintf "%s===%s" vv (domToStr env ss)
    | DP_named_group ss -> ss
    | other -> sprintf "%A" other

type unit_of_soc_measure_t =
  {
      name:  string
      units: string
      value: double
      sumrule: string
  }


let g_default_area_uom =       { name="Area"; value=0.0; units="NAND2-equivalents"; sumrule="zoned_sum" }
let g_default_latency_uom =    { name="Latency"; value=1.0; units="Clock Cycles"; sumrule="sum" }
let g_default_bandwidth_uom =  { name="Bandwidth"; value=1.0; units="bps"; sumrule="sum" }


type layout_zone_name_t = string

type busbabs_t =
    {
        onetoonef:      bool                 // False for a broadcast connection, true for a one-to-one connection.
        vlnv:           ip_xact_vlnv_t 
        portmeta:       (string*string) list
    }
    
type desmodule_port_t =
  {
      dp_nailed:           layout_zone_name_t option // Set when hardened IP
      dp_name:             string                    // Port name
      busbabs:             busbabs_t
      masterf:             bool                  // Holds if this port is a master (not slave).
      bandwidth:           unit_of_soc_measure_t
      domain:              desport_purpose_t     // Group or domain - what rules to apply to this port
      dp_needs_connection: bool
      qualquad:            bool * bool * bool * bool // (isClock, isReset, isAddress, isData) disjunction over individual parts - is sufficient digest for now, but would be needed per-wire for render.
  }


let gec_busbabs vlnv = { onetoonef=true; vlnv=vlnv; portmeta=[] }

let g_default_desmodule_port =
  {
      dp_nailed=           None
      dp_name=             "-anon-"
      busbabs=             gec_busbabs { vendor="hpr"; library="ip0"; kind=["-anon-"]; version="1.0" }
      masterf=             false  // Holds if this port is a master (not slave).
      bandwidth=           g_default_bandwidth_uom
      domain=     DP_filler
      dp_needs_connection= true    // Only set this false for additional ports on a concentrator/aggregator or static ports
      qualquad=            (false, false, false, false) // (isClock, isReset, isAddress, isData)
  }

type desmodule_t =
  {
    name:              ip_xact_vlnv_t 
    area:              unit_of_soc_measure_t
    latency:           unit_of_soc_measure_t    
    form:              desmodule_purpose_t
    ports:             desmodule_port_t list
  }



type desmodule_instance_t =
  {
    iname:             string
    kind:              desmodule_t
    nailed_:           layout_zone_name_t option // implies all ports are nailed to this zone too
  }



type soclayout_zone_t = // Normally this is one FPGA
  {
      name:              layout_zone_name_t
      area:              unit_of_soc_measure_t
      connected_peers:   string list
      static_contents:   desmodule_instance_t list
  }

type block_placement_t =
    {
        where:  soclayout_zone_t
        what:   desmodule_instance_t
    }

type soc_design_t =
    | DES_empty
    | DES_augment of string * block_placement_t * soc_design_t
    

type conquad_t = string * string * layout_zone_name_t option * desmodule_port_t // Fields are (iname, common iname for a bridge, zone option, port)



