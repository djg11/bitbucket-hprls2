
module system_integrator_canned_example

open moscow
open meox
open yout
open abstract_hdr

open system_integrator_hdr


// ------------------------------------------------------------


let simple_clock_reset_pair = 
 let (isClock, isReset, isAddress, isData) = (true, true, false, false)

 let busbabs = gec_busbabs { vendor="hpr"; library="ip0"; kind=["hpr_clkrst0"]; version="1.0" }
 let busbabs = { busbabs with
                     onetoonef=       false   // False for a broadcast connection, true for a one-to-one connection.
               }
 {
   g_default_desmodule_port with
    dp_name=         "clkreset"
    busbabs=         busbabs
    masterf=         false // Holds if this port is a master (not slave).
    bandwidth=       { g_default_bandwidth_uom with value=0.0;  }
    domain= DP_named_group "default-clkreset"
    qualquad=       (isClock, isReset, isAddress, isData)
    dp_needs_connection= true// True if input - please indicate - 

 }

   
let example_static_axi_m =
  {
    name=              { vendor="hpr"; library="ip0"; kind=["axi4-static-master101"]; version="1.0" }
    area=              { g_default_area_uom with value=4020.1; }
    latency=           { g_default_latency_uom with value=2.0; }    
    form=              DF_static_port 
    ports=             [ simple_clock_reset_pair;
                         { g_default_desmodule_port with
                             dp_name=         "axi4-static-master101p0"
                             busbabs=         gec_busbabs { vendor="hpr"; library="ip0"; kind=["axi4-lite-D32A32"]; version="1.0" }
                             masterf=         true  // Holds if this port is a master (not slave).
                             bandwidth=       { g_default_bandwidth_uom with value=30.1;  }
                             domain= DP_named_group "directing"
                             dp_needs_connection= false
                         }

                       ] 
  }

let example_static_clkgen =
  {
    name=              { vendor="hpr"; library="ip0"; kind=["clk_reset_gen"]; version="1.0" }
    area=              { g_default_area_uom with value=1.1; }
    latency=           { g_default_latency_uom with value=0.0; }    
    form=              DF_static_port 
    ports=             [
                         simple_clock_reset_pair;
                         { g_default_desmodule_port with
                             dp_name=         "axi4-static-master101p0"
                             busbabs=         gec_busbabs { vendor="hpr"; library="ip0"; kind=["clk_reset_port"]; version="1.0" }
                             masterf=         true  // Holds if this port is a master (not slave).
                             bandwidth=       { g_default_bandwidth_uom with value=0.0;  }
                             domain= DP_named_group "default-clock"
                             dp_needs_connection= false
                         }

                       ] 
  }

let example_static_axi_s =
  {
    name=              { vendor="hpr"; library="ip0"; kind=["axi4-static-slave202"]; version="1.0" }
    area=              { g_default_area_uom with value=4020.1; }
    latency=           { g_default_latency_uom with value=2.0; }    
    form=              DF_static_port 
    ports=             [
                         simple_clock_reset_pair;
                         { g_default_desmodule_port with 
                             dp_name=         "axi4-static-slave202p0"
                             busbabs=         gec_busbabs { vendor="hpr"; library="ip0"; kind=["axi4-lite-D32A32"]; version="1.0" }
                             masterf=         false  // Holds if this port is a master (not slave).
                             bandwidth=       { g_default_bandwidth_uom with value=30.1;  }
                             domain= DP_named_group "memoryservice" 
                             dp_needs_connection= false
                         }

                       ] 
  }


let example_axi_director_shim = // block for the demo library: mailbox paradigm but is classed as subsidiary (adaptors must have one master and one slave port).
  {
    name=              { vendor="hpr"; library="ip0"; kind=["axi_dir_shim"]; version="1.0" }
    area=              { g_default_area_uom with value=800.1; }
    latency=           { g_default_latency_uom with value=2.0; }    
    form=              DF_subsidiary // a mailbox style subsidiary block
    ports=             [
                         simple_clock_reset_pair;
                         { g_default_desmodule_port with 
                             dp_name=         "dirshim-axi4-slave-port"
                            
                             busbabs=         gec_busbabs { vendor="hpr"; library="ip0"; kind=["axi4-lite-D32A32"]; version="1.0" }
                             masterf=         false  // Holds if this port is a master (not slave).
                             bandwidth=       { g_default_bandwidth_uom with value=30.1;  }
                             domain= DP_named_group "directing"
                         };
                         { g_default_desmodule_port with 
                             dp_name=         "dirshim-directorate12-port"
                             busbabs=         gec_busbabs { vendor="hpr"; library="ip0"; kind=["directorate12"]; version="1.0" }
                             masterf=         false  // Holds if this port is a master (not slave).
                             bandwidth=       { g_default_bandwidth_uom with value=30.1;  }
                             domain= DP_named_group "directing"
                         }
                       ]

  }
  


// Hardcoded instances for testing
let axi_m1 =
    {
        iname= "axi_m1"
        kind= example_static_axi_m
        nailed_=  Some "chip1"
    }

let axi_m2 =
    {
        iname= "axi_m2"
        kind= example_static_axi_m
        nailed_=  Some "chip1"
    }

let axi_directorate_slave =
    {
        iname=   "axi_s1"
        kind=    example_static_axi_s
        nailed_=  Some "chip1"
    }

let axi_directorate_master =
    {
        iname=   "axi_m1"
        kind=    example_static_axi_m
        nailed_=  None
    }

let clkgen1 =
    {
        iname=     "clkgen1"
        kind=       example_static_clkgen
        nailed_=    Some "chip1"
    }

let clkgen2 =
    {
        iname=     "clkgen2"
        kind=       example_static_clkgen
        nailed_=    Some "chip2"
    }


// Concentrators applied to static ports do not need to be available in pairs, since the `other' side is not under our control.
let gec_example_concentrators _ =
                    
    let gec_canned_concentrator protocol arity side = // Side holds for the MUX, which has a master focus port.

        let gec_conc_port no =
          { g_default_desmodule_port with 
              dp_name=         "childport" + i2s no
              busbabs=         protocol
              masterf=         not side // Holds if this port is a master (not slave).
              bandwidth=       { g_default_bandwidth_uom with value=30.1;  }
              domain=          DP_multi
              dp_needs_connection= false
           }

        let focus =
          { g_default_desmodule_port with 
              dp_name=         "focus"
              busbabs=         protocol
              masterf=         side  // For a MUX the focus is a master. Focus is a slave on a DEMUX.
              bandwidth=       { g_default_bandwidth_uom with value=30.1;  }
              domain=          DP_multi
          }

        let ans = 
          {
            name=              { vendor="hpr"; library="canned-examples"; kind=[sprintf "concen_%i_%s_%s" arity (hptos protocol.vlnv.kind) (if side then "MUX" else "DEMUX")]; version="1.0" }
            area=              { g_default_area_uom with value=25.2 * (double)arity; }
            latency=           { g_default_latency_uom with value=2.0; }    
            form=              DF_concentrator
            ports=             simple_clock_reset_pair :: focus :: map gec_conc_port [0..arity-1]
          }
        ans

    // Muxes and demuxes must (currently) be available in the same matching arity sets.
    let gec_canned_concentrator_pair protocol arity = [ gec_canned_concentrator protocol arity true; gec_canned_concentrator protocol arity false; ] 
    let ans =
        [ gec_canned_concentrator_pair (gec_busbabs { vendor="hpr"; library="canned-protocols"; kind=["axi4-lite-D32A32"]; version="1.0" }) 2
          gec_canned_concentrator_pair (gec_busbabs { vendor="hpr"; library="canned-protocols"; kind=["axi4-lite-D32A32"]; version="1.0" }) 4
        ]

    let normalise conc = conc // We must ensure the focus port is first - clearly it is while we are canned!

    map normalise (list_flatten ans)

        
let gec_example_adaptors _ =
    let sorter ports =
        let masters = List.filter (fun port->port.masterf) ports
        masters @ list_subtract(ports, masters)

    let gen_canned_adaptor0 flip tag =
      let dom = DP_var(funique "AZ1")   // Use Prolog-style capitals for variables yet to be bound // Needs freshening on each retrieve
      {
        name=              { vendor="hpr"; library="canned-examples"; kind=["axi4-32_axi3-32" + tag]; version="1.0" }
        area=              { g_default_area_uom with value=10.3; }
        latency=           { g_default_latency_uom with value=2.0; }    
        form=              DF_adaptor
        ports= sorter      [
                             simple_clock_reset_pair
                             {g_default_desmodule_port with 
                                 dp_name=         "port-left"
                                 busbabs=         gec_busbabs { vendor="hpr"; library="canned-examples"; kind=["axi3-D32"]; version="1.0" }
                                 masterf=         flip // Holds if this port is a master (not slave).
                                 bandwidth=       { g_default_bandwidth_uom with value=12.1;  }
                                 domain=          dom // We could brand this as  "directing" here but thats implicit from the busbabs kind.
                             }
                             {  g_default_desmodule_port with 
                                 dp_name=         "port-right"
                                 busbabs=         gec_busbabs { vendor="hpr"; library="canned-examples"; kind=["axi4-lite-D32A32"]; version="1.0" }
                                 masterf=         not flip  // Holds if this port is a master (not slave).
                                 bandwidth=       { g_default_bandwidth_uom with value=21.1;  }
                                 domain=          dom
                             }
                           ] 
      }

    let gen_canned_adaptor1 flip tag = 
      let dom = DP_var(funique "Gsos")   // Use Prolog-style capitals for variables yet to be bound.
      {
        name=              { vendor="hpr"; library="canned-examples"; kind=["axi4_axi4_lite_adaptor_D32_" + tag ]; version="1.0" }
        area=              { g_default_area_uom with value=20.2; }
        latency=           { g_default_latency_uom with value=2.0; }    
        form=              DF_adaptor
        ports= sorter      [
                             simple_clock_reset_pair
                             {g_default_desmodule_port with 
                                 dp_name=         "port-left"
                                 busbabs=         gec_busbabs { vendor="hpr"; library="canned-examples"; kind=["axi4-lite-D32"]; version="1.0" }
                                 masterf=         flip // Holds if this port is a master (not slave).
                                 bandwidth=       { g_default_bandwidth_uom with value=30.1;  }
                                 domain=          dom // We could brand this as  "directing" here but thats implicit from the busbabs kind.
                             }
                             {  g_default_desmodule_port with 
                                 dp_name=         "port-right"
                                 busbabs=         gec_busbabs { vendor="hpr"; library="canned-examples"; kind=["axi4-lite-D32A32"]; version="1.0" }
                                 masterf=         not flip  // Holds if this port is a master (not slave).
                                 bandwidth=       { g_default_bandwidth_uom with value=30.1;  }
                                 domain=          dom
                             }
                           ] 
      }

// Post hoc optimise perhaps needed where a pair of inverse adaptors are back-to-backed
// There is a shim to use instead?
    let gen_canned_adaptor2 flip tag =
      let dom = DP_var(funique "Gcar")   // Use Prolog-style capitals for variables yet to be bound        
      {
        name=              { vendor="hpr"; library="canned-examples"; kind=["adaptor-loadstore10-axi_" + tag]; version="1.0" }
        area=              { g_default_area_uom with value=40.3; }
        latency=           { g_default_latency_uom with value=2.0; }    
        form=              DF_adaptor

        ports= sorter      [
                             simple_clock_reset_pair
                             { g_default_desmodule_port with 
                                 dp_name=         "port-left"
                                 busbabs=         gec_busbabs { vendor="hpr"; library="canned-examples"; kind=["axi4-lite-D32A32"]; version="1.0" }
                                 masterf=         flip // Holds if this port is a master (not slave).
                                 bandwidth=       { g_default_bandwidth_uom with value=30.1;  }
                                 domain=          dom
                             }
                             { g_default_desmodule_port with 
                                 dp_name=         "port-right"
                                 busbabs=         gec_busbabs { vendor="hpr"; library="canned-examples"; kind=["loadstore10"]; version="1.0" }
                                 masterf=         not flip  // Holds if this port is a master (not slave).
                                 bandwidth=       { g_default_bandwidth_uom with value=30.1;  }
                                 domain=          dom
                             }
                           ] 
      }

// Post hoc optimise perhaps needed where a pair of inverse adaptors are back-to-backed
// There is a shim to use instead?
    let gen_canned_adaptor3 flip tag =
      let dom = DP_var(funique "Gcar")   // Use Prolog-style capitals for variables yet to be bound        
      {
        name=              { vendor="hpr"; library="canned-examples"; kind=["adaptor-directorate12-axi_" + tag]; version="1.0" }
        area=              { g_default_area_uom with value=191.1; }
        latency=           { g_default_latency_uom with value=2.0; }    
        form=              DF_adaptor

        ports= sorter      [
                             simple_clock_reset_pair
                             { g_default_desmodule_port with 
                                 dp_name=         "port-axi4"
                                 busbabs=         gec_busbabs { vendor="hpr"; library="canned-examples"; kind=["axi4-lite-D32A32"]; version="1.0" }
                                 masterf=         flip // Holds if this port is a master (not slave).
                                 bandwidth=       { g_default_bandwidth_uom with value=30.1;  }
                                 domain=          dom
                             }
                             { g_default_desmodule_port with 
                                 dp_name=         "port-dir12"
                                 busbabs=         gec_busbabs { vendor="hpr"; library="canned-examples"; kind=["directorate12"]; version="1.0" }
                                 masterf=         not flip  // Holds if this port is a master (not slave).
                                 bandwidth=       { g_default_bandwidth_uom with value=30.1;  }
                                 domain=          dom
                             }
                           ] 
      }

    [gen_canned_adaptor0 false "zunter"; gen_canned_adaptor0 true "zuber" ] @
    [gen_canned_adaptor1 false "unter"; gen_canned_adaptor1 true "uber" ]  @
    [gen_canned_adaptor2 false "punter"; gen_canned_adaptor2 true "puber" ] @
    [gen_canned_adaptor3 false "funter"; gen_canned_adaptor3 true "fuber" ]

    
let make_example_bridge name (f_zone, t_zone) =
    let vv = funique ("Bdom") // Use Prolog-style capitals for variables yet to be bound

    // A bridge is formed of a pair of ramps, one in each zone.
    let gen_canned_ramp masterf zone = //
      {
        name=              { vendor="hpr"; library="canned-examples"; kind=[name + "-ramp" + (if masterf then "M" else "S")]; version="1.0" }
        area=              { g_default_area_uom with value=44.6;  } // Area in each zone needed for a bridge
        latency=           { g_default_latency_uom with value=2.0; }    
        form=              DF_zonebridge // halfbridge
        ports=             [
                             simple_clock_reset_pair
                             { g_default_desmodule_port with
                                 dp_name=         if masterf then "PortM" else "PortS"
                                 dp_nailed=       Some zone
                                 busbabs=         gec_busbabs { vendor="hpr"; library="canned-examples"; kind=["axi4-lite-D32A32"]; version="1.0" }
                                 masterf=         masterf  // Holds if this port is a master (not slave).
                                 bandwidth=       { g_default_bandwidth_uom with value=30.1;  }
                                 domain= DP_var vv
                                 dp_needs_connection= false  // Bridges in the manifest do not need to be used. If one half is used then the other half needs using but that is done constructively at bridge deployment.
                             }
                           ] 
      }
    // Simplex initiating bridge
    let ramp_gen iname masterf zone = 
        {
          iname=       iname                           // CRITICALLY we have the same instance name and group name at both ends
          kind=        gen_canned_ramp (masterf) zone // But different direction at each end
          nailed_=      None
        }
    ((f_zone, t_zone), name, (ramp_gen (name + "launch") false f_zone, ramp_gen (name + "landing") true  t_zone))


// HPR System_Integrator: Cannot find any registered subsidiary IP block to instantiate so as to serve Master:*top-primary-IP-block*/loadstore10m-port/loadstore10/1.0
// Adapting route finding ...
// The offchip service adaptor should connect to a static memory sevice domain named port.  We can then adapt to it.

// Adaptors in pairs?
// Dont need this one since an adaptor will serve - but currently subsidiary needs cannot be met with an adaptor .... please rationalise and write up
let loadstore10_axi_shim = // We term an adaptor for subsidiary use a 'shim'

      let dom = DP_var(funique "Gshi")   // Use Prolog-style capitals for variables yet to be bound
      {
        name=              { vendor="hpr"; library="canned-examples"; kind=["offchip-memory-service-shimr"]; version="1.0" }
        area=              { g_default_area_uom with value=140.3;  }
        latency=           { g_default_latency_uom with value=2.0; }    
        form=              DF_subsidiary
        ports=             [
                             simple_clock_reset_pair
                             { g_default_desmodule_port with
                                 dp_name=         "offchip-memory-service-port"
                                 busbabs=         gec_busbabs { vendor="hpr"; library="canned-examples"; kind=["loadstore10"]; version="1.0" }
                                 masterf=         false  // Holds if this port is a master (not slave).
                                 bandwidth=       { g_default_bandwidth_uom with value=30.1;  }
                                 domain=          dom
                             }
                             { g_default_desmodule_port with
                                 dp_name=         "axiout"
                                 busbabs=         gec_busbabs { vendor="hpr"; library="canned-examples"; kind=["axi4-lite-D32A32"]; version="1.0" }
                                 masterf=         true  // Holds if this port is a master (not slave).
                                 bandwidth=       { g_default_bandwidth_uom with value=30.1;  }
                                 domain=          dom
                             }
                           ] 
      }


// Discuss mailbox paradigm
    
let example_primary_block = //  desmodule_t 

    let pp1 = // desmodule_port_t
      { g_default_desmodule_port with
          dp_name=           "dir12port"
          busbabs=           gec_busbabs { vendor="hpr"; library="des0"; kind=["directorate12"]; version="1.0" }
          masterf=           true  // Holds if this port is a master (not slave).
          bandwidth=         { g_default_bandwidth_uom with value=33.1;  }
          domain=   DP_named_group "directing" //  desport_purpose_t
      }

    let pp2 = // desmodule_port_t
      { g_default_desmodule_port with
          dp_name=           "bram33port"
          busbabs=           gec_busbabs { vendor="hpr"; library="des0"; kind=["bram33"]; version="1.0" }
          masterf=           true  // Holds if this port is a master (not slave).
          bandwidth=         { g_default_bandwidth_uom with value=233.1;  }
          domain=   DP_named_group "memoryservice" //  desport_purpose_t
      }

    let pp3 = // desmodule_port_t
      { g_default_desmodule_port with
          dp_name=           "loadstore10m-port"
          busbabs=           gec_busbabs { vendor="hpr"; library="des0"; kind=["loadstore10"]; version="1.0" }
          masterf=           true  // Holds if this port is a master (not slave).
          bandwidth=         { g_default_bandwidth_uom with value=233.1;  }
          domain=   DP_named_group "memoryservice" //  desport_purpose_t
      }

    let pp4 = // desmodule_port_t
      { g_default_desmodule_port with
          dp_name=           "subsa0-master-port0"
          busbabs=           gec_busbabs { vendor="hpr"; library="des0"; kind=["subsa0"]; version="1.0" }
          masterf=           true  // Holds if this port is a master (not slave).
          bandwidth=         { g_default_bandwidth_uom with value=233.1;  }
          domain=   DP_named_group "memoryservice" //  desport_purpose_t
      }

    {
            name=     { vendor="hpr"; library="des0"; kind=["primex"]; version="1.0" }
            area=     { g_default_area_uom with value=844.1; }
            latency=           { g_default_latency_uom with value=2.0; }    
            form=     DF_primary
            ports=    [ pp1;  pp2; pp3; pp4; simple_clock_reset_pair ]

    }
    
let example_subs_block = //  desmodule_t 

    let pp1 = // desmodule_port_t
      { g_default_desmodule_port with
          dp_name=           "subsa0-slave-port"
          busbabs=           gec_busbabs { vendor="hpr"; library="des0"; kind=["subsa0"]; version="1.0" }
          masterf=           false  // Holds if this port is a master (not slave).
          bandwidth=         { g_default_bandwidth_uom with value=33.1;  }
          domain=   DP_var "SubsVar0"
      }

    let pp2 = // desmodule_port_t
      { g_default_desmodule_port with
          dp_name=           "loadstore10-m-port"
          busbabs=           gec_busbabs { vendor="hpr"; library="des0"; kind=["loadstore10"]; version="1.0" }
          masterf=           true  // Holds if this port is a master (not slave).
          bandwidth=         { g_default_bandwidth_uom with value=233.1;  }
          domain=   DP_named_group "memoryservice" //  desport_purpose_t
      }

    {
            name=     { vendor="hpr"; library="des0"; kind=["primsubs55"]; version="1.0" }
            area=     { g_default_area_uom with value=844.1; }
            latency=           { g_default_latency_uom with value=2.0; }    
            form=     DF_primary
            ports=    [ pp1;  pp2; simple_clock_reset_pair ]

    }

let example_ram44 = //  desmodule_t 

    let pp1 = // desmodule_port_t
      { g_default_desmodule_port with
          dp_name=           "BRAM-slave-port"
          busbabs=           gec_busbabs { vendor="hpr"; library="des0"; kind=["bram33"]; version="1.0" }
          masterf=           false  // Holds if this port is a master (not slave).
          bandwidth=         { g_default_bandwidth_uom with value=33.1; }
          domain=   DP_named_group "memoryservice" //  General RAM resources go in the group 'memoryservice' by default
      }


    {
            name=     { vendor="hpr"; library="des0"; kind=["example_BRAM44"]; version="1.0" }
            area=     { g_default_area_uom with value=2814.0; }
            latency=  { g_default_latency_uom with value=2.0; }    
            form=     DF_subsidiary
            ports=    [ pp1; simple_clock_reset_pair ]

    }


// eof
    
