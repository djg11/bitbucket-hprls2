
http://www.cl.cam.ac.uk/research/srg/han/hprls/orangepath/system-integrator/hpr-system-integrator.html

This folder contains:
  HPR System Integrator - A Multi-FPGA logic partitioner, instantiator and structural wiring generator.

//
// (C) 20017, DJ Greaves, University of Cambridge, Computer Laboratory.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met: redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer;
// redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution;
// neither the name of the copyright holders nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.



HPR System Integrator is an open-source EDA tool that uses the HPR L/S
library. It is primarily used to generate top-level circuit structures
around logic generate by Kiwi HLS. However, it can be used with IP
blocks from any source, provided they are annotated with a small
number of IP-XACT extensions regarding purpose, area, latency and
throughput.

HPR System Integrator automatically instantiates Verilog RTL blocks
created by KiwiC or sourced from third parties, provided they are
accompanied with an XML meta-data file that indicates their primary
features. Its output is a structural RTL netlist plus report
files. Alternatively, its output is an IP-XACT design document that
will instruct a third-party tool to form the structural netlist. Many
EDA systems will obey such a design document.

The tool is described in a dedicated section in the back of the Kiwi
user manual.

The tool is being released June/July 2017. 



