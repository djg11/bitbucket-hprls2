
/*

//  Extensions to the Verisity {\em e} language

@INPROCEEDINGS{911754,
author={Y. Hollander and M. Morley and A. Noy},
booktitle={Technology of Object-Oriented Languages and Systems, 2001. TOOLS 38. Proceedings},
title={The e language: a fresh separation of concerns},
year={2001},
pages={41-50},
keywords={constraint handling;formal verification;hardware description languages;inheritance;object-oriented languages;object-oriented programming;aspect oriented programming;compiled code;constraint oriented language;data formats;e programming language;formal specification;hardware testing;hardware verification;inheritance;interpreted code;microchip industry;modeling;object oriented language;Computer languages;Design engineering;Embedded software;Hardware;Life estimation;Object oriented modeling;Object oriented programming;Programming profession;Software testing;System testing},
doi={10.1109/TOOLS.2001.911754},
ISSN={1530-2067},
month={},}

The keyword synth, as the first word in a comment, is parsed to extract the input or output direction
for a net that it part of an interface. It is reflected to the opposite direction as needed when
separately generating the target or initiator sides of an interface.

// Where an {\em e} unit is deployed inside another unit, the /* synth target */ or /* synth initiator */
// mark up sets which variant to instantiate. 


The other main extension is that events, like units, are parameterised with symbolic formal parameters
that can refer to the data or command information conveyed by that event.  

*/

//
// axi.ee
//
// Taken from http://www.gstitt.ece.ufl.edu/courses/fall15/eel4720_5721/labs/refs/AXI4_specification.pdf
// ARM AMBA AXI and ACE Protocol Specification



type abus_t       : uint (bits: 32);
type dbus_t       : uint (bits: 32);
type tagid_t      : uint (bits: 4);
type burst_len_t  : uint (bits: 8);


type burst_size_t : [BS_1, BS_2, BS_4, BS_8, BS_16, BS_32, BS_64, BS_128];
type burst_type_t : [BT_FIXED, BT_INCR, BT_WRAP];

type resp_t       : [R_OKAY, R_EXOKAY, R_SLVERR, R_DECERR ];

struct AXI_GlobalSignals
{
  CLK   : bool  /* synth in always */; 
  RESET : bool  /* synth in always */;
};

// Syntheisable directions are given for a target/slave device - reversed on an initiator/ master.
// ID fields will tend to be wider on targets.

struct AXI_ReadAddressChannel // Read address
{
  ARID    : tagid_t           /* synth in target */; 
  ARLEN   : burst_len_t       /* synth in target */; 
  ARSIZE  : burst_size_t      /* synth in target */; 
  ARBURST : burst_type_t      /* synth in target */; 
  ARADDR  : abus_t            /* synth in target  */; 
  ARREADY : bool              /* synth out target */; 
  ARVALID : bool              /* synth in target  */; 
};


struct AXI_ReadDataChannel // Read data and response
{
  RDATA  : dbus_t            /* synth out target */; 
  RID    : tagid_t           /* synth out target */; 
  RRESP  : resp_t            /* synth out target */; 
  RLAST  : bool              /* synth out target */; 
  RREADY : bool              /* synth in target  */; 
  RVALID : bool              /* synth out target */; 
};

struct AXI_WriteAddressChannel // Write address
{
  AWID    : tagid_t           /* synth in target */; 
  AWLEN   : burst_len_t       /* synth in target */; 
  AWSIZE  : burst_size_t      /* synth in target */; 
  AWBURST : burst_type_t      /* synth in target */; 
  AWADDR  : abus_t            /* synth in target  */; 
  AWREADY : bool              /* synth out target */; 
  AWVALID : bool              /* synth in target  */; 
};


struct AXI_WriteDataChannel // Write data 
{
  WDATA  : dbus_t            /* synth in target */; 
  WID    : tagid_t           /* synth in target */; 
  WLAST  : bool              /* synth in target */; 
  WREADY : bool              /* synth in target  */; 
  WVALID : bool              /* synth out target */; 
};


struct AXI_WriteResponseChannel // Write response
{
  BID    : tagid_t           /* synth out target */; 
  BRESP  : resp_t            /* synth out target */; 
  BREADY : bool              /* synth in target  */; 
  BVALID : bool              /* synth out target */; 
};


unit read_port
{
   rac : AXI_ReadAddressChannel;
   rdc : AXI_ReadDataChannel;

   event e_rda(addr:abus_t) is true(rac.ARREADY && rac.ARVALID && rac.ARADDR==addr) @rise(glob.CLK);
   event e_rdd(data:dbus_t) is true(rac.RREADY  && rdc.RVALID  && rdc.RDATA==data) @rise(glob.CLK);
};


unit write_port
{
   wac : AXI_WriteAddressChannel;
   wdc : AXI_WriteDataChannel;
   wrc : AXI_WriteResponseChannel;

   // A couple of bi-implications that turn straight into logic for a target but what havoc do they cause at the initiator?
//     expect force_a_d_concurrent1 is true((wdc.WVALID && wac.AWVALID) == wdc.WREADY) @rise(glob.CLK);
//     expect force_a_d_concurrent2 is true((wdc.WVALID && wac.AWVALID) == wac.AWREADY) @rise(glob.CLK);

     event e_wda(addr:abus_t) is true(wac.AWREADY && wac.AWVALID && rac.AWADDR==addr) @rise(glob.CLK);
     event e_wdc(data:dbus_t) is true(wac.WREADY  && wdc.WVALID  && wdc.WDATA==data) @rise(glob.CLK);
     event e_wbr(rest:resp_t) is true(wrc.BREADY  && wrc.BVALID  && wrc.BRESP==resp) @rise(glob.CLK);
  };

unit axi_port
{
  glob : AXI_GlobalSignals;
  readside  : read_port  is instance;
  writeside : read_port  is instance;
};

// Initially we will ignore LEN and ID to generate an AXI4-Lite switch.

unit axi_relay
{
  port0    : axi_port is instance /* synth target */;
  port1    : axi_port is instance /* synth initiator */;

  event fwd_read is @port0.readside.e_rda(trans_addr) and @port0.readside.e_rdd(trans_data)  and
                    @port1.readside.e_rda(trans_addr) and @port1.readside.e_rdd(trans_data);    

  event fwd_read_goal is @fwd_read or cycle; // need implied fair or

  event axi_relay_goals is @fwd_read_goal;
};

extend sys
{
   relay: axi_relay;
   expect topgoals is @relay.axi_relay_goals;
};


// ERROR: we mean the write conjuction!
// Simply writing the following conjunction might look attractive, although it means the data passes through
// combinationally, and worse, it neglects the loading of the write address and write data in different clock cycles, togging that data until a conjunction arises.
// MOREOVER: the handshaked ordering constraints may be violated ...
//  event fwd_read is @port0.er_da(trans_addr) and @port0.e_rdd(trans_data)  and
//                    @port1.er_da(trans_addr) and @port1.e_rdd(trans_data);    


// eof
 