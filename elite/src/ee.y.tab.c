/* A Bison parser, made by GNU Bison 2.7.  */

/* Bison implementation for Yacc-like parsers in C
   
      Copyright (C) 1984, 1989-1990, 2000-2012 Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.7"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
/* Line 371 of yacc.c  */
#line 202 "tmp.yy"

#include "cbglish.h"

#define EE_LINEPOINT(X) add_linepoint("EE_linepoint", X)

int yyerror (const char *s);
extern builder *g_ast_parsed;

/* Line 371 of yacc.c  */
#line 77 "ee.y.tab.c"

# ifndef YY_NULL
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULL nullptr
#  else
#   define YY_NULL 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "ee.y.tab.h".  */
#ifndef YY_YY_EE_Y_TAB_H_INCLUDED
# define YY_YY_EE_Y_TAB_H_INCLUDED
/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     sd_string = 258,
     sd_id = 259,
     sd_number = 260,
     sd_char = 261,
     sd_synthembed_start = 262,
     sd_synthembed_end = 263,
     sd_dstring = 264,
     s_on = 265,
     s_event = 266,
     s_var = 267,
     s_assume = 268,
     s_dut_error = 269,
     s_expect = 270,
     s_check = 271,
     s_that = 272,
     s_wait = 273,
     s_until = 274,
     s_cover = 275,
     s_using = 276,
     s_count_only = 277,
     s_trace_only = 278,
     s_at_least = 279,
     s_transition = 280,
     s_item = 281,
     s_ranges = 282,
     s_range = 283,
     s_ignore = 284,
     s_illegal = 285,
     s_cross = 286,
     s_text = 287,
     s_print = 288,
     s_is = 289,
     s_also = 290,
     s_only = 291,
     s_list = 292,
     s_like = 293,
     s_of = 294,
     s_first = 295,
     s_all = 296,
     s_compute = 297,
     s_radix = 298,
     s_hex = 299,
     s_post_generate = 300,
     s_pre_generate = 301,
     s_setup_test = 302,
     s_finalize_test = 303,
     s_extract_test = 304,
     s_init = 305,
     s_as_a = 306,
     s_quit = 307,
     s_lock = 308,
     s_unlock = 309,
     s_release = 310,
     s_swap = 311,
     s_to_string = 312,
     s_value = 313,
     s_and_all = 314,
     s_apply = 315,
     s_average = 316,
     s_exists = 317,
     s_unit = 318,
     s_integer = 319,
     s_real = 320,
     s_int = 321,
     s_uint = 322,
     s_byte = 323,
     s_bytes = 324,
     s_bits = 325,
     s_bit = 326,
     s_time = 327,
     s_string = 328,
     s_locker = 329,
     s_change = 330,
     s_rise = 331,
     s_fall = 332,
     s_delay = 333,
     s_sync = 334,
     s_true = 335,
     s_detach = 336,
     s_eventually = 337,
     s_emit = 338,
     s_cycle = 339,
     s_bool = 340,
     s_if = 341,
     s_then = 342,
     s_else = 343,
     s_when = 344,
     s_case = 345,
     s_casex = 346,
     s_casez = 347,
     s_default = 348,
     s_and = 349,
     s_or = 350,
     s_not = 351,
     s_while = 352,
     s_for = 353,
     s_from = 354,
     s_fail = 355,
     s_to = 356,
     s_step = 357,
     s_each = 358,
     s_do = 359,
     s_gen = 360,
     s_keep = 361,
     s_keeping = 362,
     s_soft = 363,
     s_index = 364,
     s_in = 365,
     s_instance = 366,
     s_new = 367,
     s_return = 368,
     s_select = 369,
     s_define = 370,
     s_as = 371,
     s_computed = 372,
     s_type = 373,
     s_extend = 374,
     s_struct = 375,
     s_verilog = 376,
     s_variable = 377,
     s_import = 378,
     ss_quote = 379,
     ss_lsect = 380,
     ss_rsect = 381,
     ss_lbra = 382,
     ss_rbra = 383,
     ss_lpar = 384,
     ss_rpar = 385,
     ss_equals = 386,
     ss_comma = 387,
     ss_query = 388,
     ss_dot = 389,
     ss_dotdot = 390,
     ss_ampersand = 391,
     ss_stile = 392,
     ss_snail = 393,
     ss_percent = 394,
     ss_caret = 395,
     ss_star = 396,
     ss_slash = 397,
     ss_dltd = 398,
     ss_dgtd = 399,
     ss_plus = 400,
     ss_minus = 401,
     ss_hash = 402,
     ss_colon = 403,
     ss_tilda = 404,
     ss_pling = 405,
     ss_semicolon = 406,
     ss_dled = 407,
     ss_dged = 408,
     ss_deqd = 409,
     ss_colonequals = 410,
     ss_dned = 411,
     ss_rarrow1 = 412,
     ss_rarrow2 = 413,
     ss_plusequals = 414,
     ss_minusequals = 415,
     ss_lshift = 416,
     ss_rshift = 417,
     ss_logor = 418,
     ss_logand = 419
   };
#endif


#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{
/* Line 387 of yacc.c  */
#line 212 "tmp.yy"

	builder *auxval;
       

/* Line 387 of yacc.c  */
#line 289 "ee.y.tab.c"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif

extern YYSTYPE yylval;

#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */

#endif /* !YY_YY_EE_Y_TAB_H_INCLUDED  */

/* Copy the second part of user declarations.  */

/* Line 390 of yacc.c  */
#line 317 "ee.y.tab.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(N) (N)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int yyi)
#else
static int
YYID (yyi)
    int yyi;
#endif
{
  return yyi;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)				\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack_alloc, Stack, yysize);			\
	Stack = &yyptr->Stack_alloc;					\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (YYID (0))
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  58
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   497

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  165
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  43
/* YYNRULES -- Number of rules.  */
#define YYNRULES  172
/* YYNRULES -- Number of states.  */
#define YYNSTATES  404

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   419

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,   146,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,   162,   163,   164
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     5,     6,     9,    10,    16,    20,    21,
      24,    27,    31,    37,    38,    41,    42,    45,    47,    50,
      53,    54,    56,    57,    60,    68,    74,    81,    87,    92,
     100,   109,   113,   118,   125,   129,   134,   143,   151,   160,
     164,   170,   181,   182,   187,   188,   199,   200,   211,   212,
     215,   216,   222,   223,   226,   230,   236,   244,   246,   253,
     258,   263,   267,   269,   273,   277,   281,   287,   291,   299,
     307,   311,   318,   325,   330,   336,   342,   344,   347,   349,
     351,   353,   355,   357,   359,   361,   365,   367,   369,   371,
     373,   377,   378,   380,   384,   385,   387,   391,   395,   396,
     398,   402,   406,   410,   414,   418,   422,   425,   428,   431,
     434,   437,   440,   444,   446,   450,   456,   459,   462,   466,
     468,   472,   476,   480,   482,   488,   490,   494,   496,   498,
     502,   506,   508,   512,   516,   520,   524,   526,   530,   534,
     538,   542,   546,   550,   552,   556,   558,   562,   564,   568,
     572,   574,   578,   582,   584,   586,   593,   598,   601,   606,
     610,   615,   617,   619,   621,   627,   629,   633,   636,   638,
     640,   642,   645
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int16 yyrhs[] =
{
     166,     0,    -1,   167,    -1,    -1,   176,   167,    -1,    -1,
       4,   148,   185,   132,   168,    -1,     4,   148,   185,    -1,
      -1,   139,   169,    -1,   150,   169,    -1,   129,   168,   130,
      -1,   129,   168,   130,   148,   185,    -1,    -1,     7,   173,
      -1,    -1,    34,   111,    -1,     8,    -1,   110,   173,    -1,
       4,   173,    -1,    -1,   170,    -1,    -1,    38,     4,    -1,
     169,     4,   148,   185,   172,   151,   171,    -1,   119,     4,
     148,   185,   151,    -1,   119,     4,   125,   167,   126,   151,
      -1,   118,     4,   148,   185,   151,    -1,   115,     4,   193,
     151,    -1,   119,     4,     4,   125,   167,   126,   151,    -1,
     119,     4,     4,     4,   125,   167,   126,   151,    -1,   106,
     193,   151,    -1,   106,   108,   193,   151,    -1,    11,     4,
     174,    34,   192,   151,    -1,    11,     4,   151,    -1,    10,
       4,   183,   151,    -1,   120,     4,   175,   174,   125,   167,
     126,   151,    -1,    63,     4,   174,   125,   167,   126,   151,
      -1,    20,     4,   177,    34,   125,   179,   126,   151,    -1,
     186,     4,   151,    -1,   186,     4,    34,   192,   151,    -1,
     186,     4,    34,   192,   151,    88,    14,   129,   189,   130,
      -1,    -1,    21,   183,   151,   177,    -1,    -1,    28,   129,
     127,   193,   128,   132,   193,   130,   151,   178,    -1,    -1,
      26,     4,    21,    27,   131,   125,   178,   126,   151,   179,
      -1,    -1,   183,   180,    -1,    -1,   125,   180,   126,   151,
     181,    -1,    -1,   193,   182,    -1,   193,   151,   182,    -1,
      12,   188,   148,   185,   151,    -1,    12,   188,   148,   185,
     131,   193,   151,    -1,   193,    -1,    98,   103,   110,   193,
     183,   151,    -1,    97,   193,   183,   151,    -1,    86,   193,
     183,   151,    -1,    83,   205,   151,    -1,   151,    -1,   113,
     193,   151,    -1,    18,   192,   151,    -1,    79,   192,   151,
      -1,    86,   193,   183,    88,   183,    -1,   125,   180,   126,
      -1,   104,     4,   107,   125,   182,   126,   151,    -1,   105,
       4,   107,   125,   182,   126,   151,    -1,   105,     4,   151,
      -1,    41,    39,   125,   180,   126,   151,    -1,    40,    39,
     125,   181,   126,   151,    -1,   193,   155,   193,   151,    -1,
     129,    70,   148,   193,   130,    -1,   129,    69,   148,   193,
     130,    -1,   187,    -1,   185,   184,    -1,    71,    -1,    68,
      -1,    85,    -1,    73,    -1,    64,    -1,    72,    -1,    66,
      -1,    37,    39,   185,    -1,     4,    -1,    67,    -1,    15,
      -1,    13,    -1,   127,   189,   128,    -1,    -1,     4,    -1,
       4,   132,   188,    -1,    -1,   193,    -1,   193,   132,   189,
      -1,   129,   193,   130,    -1,    -1,   192,    -1,   192,   151,
     191,    -1,   192,    94,   192,    -1,   192,    95,   192,    -1,
     192,   158,   192,    -1,   129,   192,   130,    -1,   125,   191,
     126,    -1,    80,   190,    -1,   100,   192,    -1,    82,   192,
      -1,    76,   190,    -1,    77,   190,    -1,    75,   190,    -1,
     192,   141,   192,    -1,    84,    -1,   127,   193,   128,    -1,
     127,   193,   148,   193,   128,    -1,   138,   205,    -1,   138,
     192,    -1,   192,   138,   192,    -1,   194,    -1,   194,   131,
     194,    -1,   194,   159,   194,    -1,   194,   160,   194,    -1,
     195,    -1,   195,   133,   195,   148,   194,    -1,   196,    -1,
     196,   158,   196,    -1,   197,    -1,   198,    -1,   197,    95,
     198,    -1,   197,   163,   198,    -1,   199,    -1,   198,    94,
     199,    -1,   198,   164,   199,    -1,   198,   110,   199,    -1,
     198,   140,   199,    -1,   200,    -1,   200,   154,   200,    -1,
     200,   156,   200,    -1,   200,   143,   200,    -1,   200,   152,
     200,    -1,   200,   144,   200,    -1,   200,   153,   200,    -1,
     201,    -1,   201,   137,   200,    -1,   202,    -1,   201,   136,
     202,    -1,   203,    -1,   202,   145,   203,    -1,   202,   146,
     203,    -1,   204,    -1,   203,   141,   204,    -1,   203,   142,
     204,    -1,   205,    -1,   207,    -1,   205,   127,   193,   148,
     193,   128,    -1,   205,   127,   193,   128,    -1,   134,   206,
      -1,   139,   125,   189,   126,    -1,   205,   134,   206,    -1,
     205,   129,   189,   130,    -1,     4,    -1,    72,    -1,   105,
      -1,   127,   193,   135,   193,   128,    -1,   206,    -1,   129,
     193,   130,    -1,   133,   207,    -1,     5,    -1,     9,    -1,
       3,    -1,   149,   207,    -1,   150,   207,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   229,   229,   239,   241,   249,   252,   257,   264,   266,
     270,   277,   281,   288,   291,   299,   302,   308,   312,   314,
     320,   323,   331,   334,   345,   363,   367,   371,   375,   379,
     383,   390,   394,   400,   404,   408,   413,   417,   422,   426,
     431,   437,   448,   451,   461,   465,   487,   490,   499,   502,
     510,   513,   522,   525,   528,   536,   540,   544,   549,   553,
     557,   561,   565,   569,   573,   577,   581,   585,   589,   593,
     597,   602,   606,   611,   650,   654,   660,   662,   667,   671,
     675,   679,   683,   687,   692,   696,   700,   704,   711,   712,
     716,   737,   740,   742,   749,   752,   756,   763,   770,   773,
     777,   789,   793,   797,   801,   805,   809,   813,   817,   821,
     824,   828,   834,   838,   844,   848,   854,   858,   862,   884,
     885,   890,   896,   905,   906,   910,   912,   919,   925,   926,
     930,   937,   939,   943,   949,   953,   959,   961,   964,   967,
     970,   973,   976,   984,   985,   993,   994,  1001,  1002,  1007,
    1015,  1017,  1021,  1028,  1032,  1034,  1038,  1042,  1045,  1049,
    1053,  1061,  1065,  1069,  1076,  1082,  1084,  1086,  1091,  1096,
    1101,  1106,  1111
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "sd_string", "sd_id", "sd_number",
  "sd_char", "sd_synthembed_start", "sd_synthembed_end", "sd_dstring",
  "s_on", "s_event", "s_var", "s_assume", "s_dut_error", "s_expect",
  "s_check", "s_that", "s_wait", "s_until", "s_cover", "s_using",
  "s_count_only", "s_trace_only", "s_at_least", "s_transition", "s_item",
  "s_ranges", "s_range", "s_ignore", "s_illegal", "s_cross", "s_text",
  "s_print", "s_is", "s_also", "s_only", "s_list", "s_like", "s_of",
  "s_first", "s_all", "s_compute", "s_radix", "s_hex", "s_post_generate",
  "s_pre_generate", "s_setup_test", "s_finalize_test", "s_extract_test",
  "s_init", "s_as_a", "s_quit", "s_lock", "s_unlock", "s_release",
  "s_swap", "s_to_string", "s_value", "s_and_all", "s_apply", "s_average",
  "s_exists", "s_unit", "s_integer", "s_real", "s_int", "s_uint", "s_byte",
  "s_bytes", "s_bits", "s_bit", "s_time", "s_string", "s_locker",
  "s_change", "s_rise", "s_fall", "s_delay", "s_sync", "s_true",
  "s_detach", "s_eventually", "s_emit", "s_cycle", "s_bool", "s_if",
  "s_then", "s_else", "s_when", "s_case", "s_casex", "s_casez",
  "s_default", "s_and", "s_or", "s_not", "s_while", "s_for", "s_from",
  "s_fail", "s_to", "s_step", "s_each", "s_do", "s_gen", "s_keep",
  "s_keeping", "s_soft", "s_index", "s_in", "s_instance", "s_new",
  "s_return", "s_select", "s_define", "s_as", "s_computed", "s_type",
  "s_extend", "s_struct", "s_verilog", "s_variable", "s_import",
  "ss_quote", "ss_lsect", "ss_rsect", "ss_lbra", "ss_rbra", "ss_lpar",
  "ss_rpar", "ss_equals", "ss_comma", "ss_query", "ss_dot", "ss_dotdot",
  "ss_ampersand", "ss_stile", "ss_snail", "ss_percent", "ss_caret",
  "ss_star", "ss_slash", "ss_dltd", "ss_dgtd", "ss_plus", "ss_minus",
  "ss_hash", "ss_colon", "ss_tilda", "ss_pling", "ss_semicolon", "ss_dled",
  "ss_dged", "ss_deqd", "ss_colonequals", "ss_dned", "ss_rarrow1",
  "ss_rarrow2", "ss_plusequals", "ss_minusequals", "ss_lshift",
  "ss_rshift", "ss_logor", "ss_logand", "$accept", "prog", "ee_decl_list",
  "ee_formal_item_list", "ee_fieldflags", "ee_formals", "ee_synthctrl",
  "ee_instance_flag", "ee_synthdirectives", "ee_formals_option",
  "ee_like_option", "ee_decl", "ee_using_list", "ee_range_comma_list",
  "ee_cover_item_list", "ee_action_list", "ee_ofaction_list",
  "ee_keeping_list", "ee_action", "ee_type_qual", "ee_type", "ee_dir",
  "ee_enumeration_type", "sd_id_comma_list", "exp_comma_list", "ee_econd",
  "tex_list", "tex", "exp", "exp1", "exp2", "exp3", "exp4", "exp5", "exp6",
  "exp7", "exp8", "exp9", "exp10", "exp11", "exp12", "exp13a", "exp13", YY_NULL
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346,   347,   348,   349,   350,   351,   352,   353,   354,
     355,   356,   357,   358,   359,   360,   361,   362,   363,   364,
     365,   366,   367,   368,   369,   370,   371,   372,   373,   374,
     375,   376,   377,   378,   379,   380,   381,   382,   383,   384,
     385,   386,   387,   388,   389,   390,   391,   392,   393,   394,
     395,   396,   397,   398,   399,   400,   401,   402,   403,   404,
     405,   406,   407,   408,   409,   410,   411,   412,   413,   414,
     415,   416,   417,   418,   419
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,   165,   166,   167,   167,   168,   168,   168,   169,   169,
     169,   170,   170,   171,   171,   172,   172,   173,   173,   173,
     174,   174,   175,   175,   176,   176,   176,   176,   176,   176,
     176,   176,   176,   176,   176,   176,   176,   176,   176,   176,
     176,   176,   177,   177,   178,   178,   179,   179,   180,   180,
     181,   181,   182,   182,   182,   183,   183,   183,   183,   183,
     183,   183,   183,   183,   183,   183,   183,   183,   183,   183,
     183,   183,   183,   183,   184,   184,   185,   185,   185,   185,
     185,   185,   185,   185,   185,   185,   185,   185,   186,   186,
     187,   188,   188,   188,   189,   189,   189,   190,   191,   191,
     191,   192,   192,   192,   192,   192,   192,   192,   192,   192,
     192,   192,   192,   192,   192,   192,   192,   192,   192,   193,
     193,   193,   193,   194,   194,   195,   195,   196,   197,   197,
     197,   198,   198,   198,   198,   198,   199,   199,   199,   199,
     199,   199,   199,   200,   200,   201,   201,   202,   202,   202,
     203,   203,   203,   204,   205,   205,   205,   205,   205,   205,
     205,   206,   206,   206,   207,   207,   207,   207,   207,   207,
     207,   207,   207
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     0,     2,     0,     5,     3,     0,     2,
       2,     3,     5,     0,     2,     0,     2,     1,     2,     2,
       0,     1,     0,     2,     7,     5,     6,     5,     4,     7,
       8,     3,     4,     6,     3,     4,     8,     7,     8,     3,
       5,    10,     0,     4,     0,    10,     0,    10,     0,     2,
       0,     5,     0,     2,     3,     5,     7,     1,     6,     4,
       4,     3,     1,     3,     3,     3,     5,     3,     7,     7,
       3,     6,     6,     4,     5,     5,     1,     2,     1,     1,
       1,     1,     1,     1,     1,     3,     1,     1,     1,     1,
       3,     0,     1,     3,     0,     1,     3,     3,     0,     1,
       3,     3,     3,     3,     3,     3,     2,     2,     2,     2,
       2,     2,     3,     1,     3,     5,     2,     2,     3,     1,
       3,     3,     3,     1,     5,     1,     3,     1,     1,     3,
       3,     1,     3,     3,     3,     3,     1,     3,     3,     3,
       3,     3,     3,     1,     3,     1,     3,     1,     3,     3,
       1,     3,     3,     1,     1,     6,     4,     2,     4,     3,
       4,     1,     1,     1,     5,     1,     3,     2,     1,     1,
       1,     2,     2
};

/* YYDEFACT[STATE-NAME] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       3,     0,     0,    89,    88,     0,     0,     0,     0,     0,
       0,     0,     8,     8,     0,     2,     0,     3,     0,     0,
      20,    42,    20,   170,   161,   168,   169,   162,   163,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   119,   123,
     125,   127,   128,   131,   136,   143,   145,   147,   150,   153,
     165,   154,     0,     0,     0,    22,     9,    10,     1,     0,
       4,     0,    91,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   163,     0,    48,    62,     0,    57,     5,    34,
      21,     0,     0,     0,     0,     0,     0,     0,   167,   157,
      94,   171,   172,    31,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    94,     0,
       0,     0,     0,     3,     0,     0,    20,     0,     0,    39,
      92,     0,     0,     0,     0,     0,     0,   113,     0,    98,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    48,    35,     0,     0,     0,
       0,     0,     0,     3,    32,     0,   166,     0,    95,   120,
     121,   122,     0,   126,   129,   130,   132,   134,   135,   133,
     139,   141,   140,   142,   137,   138,   146,   144,   148,   149,
     151,   152,     0,     0,   159,    28,    86,     0,    82,    84,
      87,    79,    78,    83,    81,    80,    94,     0,    76,     0,
       3,     0,     0,    23,     0,    15,     0,    91,     0,     0,
     111,   109,   110,   106,   108,   107,     0,    99,     0,     0,
       0,     0,   117,   116,     0,     0,     0,     0,    64,     0,
      50,    48,    65,    61,     0,     0,     0,     0,     0,    70,
      63,    67,    49,     0,     0,    11,     0,    42,    46,     0,
       0,   158,    94,     0,   156,     0,   160,     0,     0,     0,
      27,    77,     3,     0,     0,    25,     3,     0,     0,    40,
      93,     0,     0,   105,    98,   114,     0,   104,     0,   101,
     102,   118,   112,   103,    48,     0,     0,     0,    60,    59,
       0,    52,    52,    73,     7,     0,    33,    43,     0,     0,
       0,   164,    96,   124,     0,    85,    90,     0,     0,     0,
       0,    26,     0,    16,    13,     0,     0,    55,    97,   100,
       0,     0,     0,     0,    66,     0,     0,    52,     0,     5,
      12,     0,     0,    37,   155,     0,     0,     0,    29,     0,
       0,    24,     0,     0,   115,     0,    72,    71,    58,     0,
      52,    53,     0,     6,     0,    38,     0,     0,    30,    36,
       0,    17,     0,    14,    94,    56,    50,    68,    54,    69,
       0,    75,    74,    19,    18,     0,    51,     0,    41,    44,
       0,     0,     0,     0,     0,    46,     0,    47,     0,     0,
       0,     0,    44,    45
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,    14,    15,   159,    16,    80,   351,   278,   373,    81,
     126,    17,    83,   391,   309,   154,   295,   336,   155,   271,
     207,    18,   208,   131,   167,   220,   226,   227,    77,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -316
static const yytype_int16 yypact[] =
{
       6,    36,    55,  -316,  -316,    83,   123,    87,   132,   137,
     142,   148,   -66,   -66,   117,  -316,   168,     6,   174,   229,
     -95,   159,    57,  -316,  -316,  -316,  -316,  -316,  -316,   139,
     139,   139,   272,    14,    68,   272,   272,    31,   -96,    80,
      64,   -65,   -61,  -316,    44,    54,   -74,    23,  -316,    28,
    -316,  -316,   139,    67,     5,   187,  -316,  -316,  -316,    81,
    -316,   -14,   231,   359,   201,   203,   359,   139,   139,   139,
     140,   241,   242,   139,   229,  -316,    97,    98,   248,  -316,
    -316,   223,   229,   225,   138,   111,   130,   144,  -316,  -316,
     139,  -316,  -316,  -316,   139,   139,   139,   139,   139,   139,
     139,   139,   139,   139,   139,   139,   139,   139,   139,   139,
     139,   139,   139,   139,   139,   139,   139,   139,   139,    14,
     116,   344,     3,     6,   344,   278,    57,   344,   359,  -316,
     152,   143,   156,   156,   156,   156,   359,  -316,   359,   359,
     139,   359,   313,   113,   161,   162,   155,   -81,   229,   229,
     180,   185,   -76,   146,   172,   229,  -316,   139,   151,   170,
     359,   153,   182,     6,  -316,   139,  -316,   179,   177,  -316,
    -316,  -316,   163,  -316,   -61,   -61,  -316,  -316,  -316,  -316,
    -316,  -316,  -316,  -316,  -316,  -316,   -74,  -316,    23,    23,
    -316,  -316,   -13,   184,  -316,  -316,  -316,   271,  -316,  -316,
    -316,  -316,  -316,  -316,  -316,  -316,   139,   -71,  -316,   195,
       6,   197,   -62,  -316,   196,    -9,   208,   231,   344,   139,
    -316,  -316,  -316,  -316,   -44,   -44,   198,   329,    12,   -53,
     139,   313,   -44,    28,   359,   359,   359,   359,  -316,   359,
     200,   229,  -316,  -316,   -52,   178,   139,   206,   207,  -316,
    -316,  -316,  -316,   186,   344,   188,   331,   159,   302,   209,
     212,  -316,   139,   139,  -316,   139,  -316,   344,   213,    53,
    -316,  -316,     6,   217,   194,  -316,     6,   236,   199,   263,
    -316,   -77,   222,  -316,   359,  -316,   139,  -316,   -19,   -44,
     -44,   -44,   -44,   -44,   229,   234,   235,   229,  -316,  -316,
     229,   139,   139,  -316,   -85,   344,  -316,  -316,   351,   238,
     214,  -316,  -316,  -316,   247,   228,  -316,   243,   246,   256,
     232,  -316,   258,  -316,   379,   382,   139,  -316,  -316,  -316,
     270,   274,   251,   252,  -316,   253,   280,    34,   281,   248,
     228,   388,   268,  -316,  -316,   139,   139,   269,  -316,   276,
      24,  -316,   285,   277,  -316,   279,  -316,  -316,  -316,   282,
     139,  -316,   286,  -316,   404,  -316,   314,   315,  -316,  -316,
      24,  -316,    24,  -316,   139,  -316,   200,  -316,  -316,  -316,
     301,  -316,  -316,  -316,  -316,   318,  -316,   324,  -316,   422,
     325,   327,   328,   305,   139,   302,   330,  -316,   332,   139,
     335,   306,   422,  -316
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -316,  -316,   -16,   121,   193,  -316,  -316,  -316,  -315,   -18,
    -316,  -316,   204,    66,    71,  -140,    99,  -242,   -17,  -316,
    -116,  -316,  -316,   257,  -113,    84,   189,   -60,    -7,   -82,
     380,   378,  -316,   110,   100,   264,  -316,   367,   114,   115,
     -38,    -6,   134
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -9
static const yytype_int16 yytable[] =
{
      37,    60,    76,   143,    84,   193,   146,   209,   212,   122,
      -8,   215,   169,   170,   171,   252,     1,     2,    24,     3,
     128,     4,    85,    86,    87,   277,     5,    89,   370,   147,
      99,   248,   371,   101,    78,    94,   297,    23,    24,    25,
      19,   234,   235,    26,   269,   120,   117,   339,   118,   102,
     234,   235,   269,   119,   326,   383,    79,   384,   269,    20,
     338,   148,   149,    95,    96,   161,   153,   269,   216,     6,
     243,   113,   114,    12,   327,   249,   224,   287,   225,   103,
     270,   229,   232,   168,    13,   236,    27,    21,   237,   275,
      23,    24,    25,   268,   236,   361,    26,   237,   100,   298,
     256,   296,   281,   104,   233,   239,    27,   211,   214,   285,
     192,   168,     7,   194,   239,   264,   165,    58,   378,    28,
     269,     8,   317,   318,     9,    10,    11,    22,   210,   286,
     123,   244,   245,   228,   372,   265,    52,   129,   304,    28,
     285,    53,    23,    24,    25,    12,    54,   259,    26,   312,
     253,   315,    55,   124,   331,   117,    13,   118,   260,    27,
     286,    30,   119,    31,   115,   116,    88,    32,    33,    91,
      92,   229,    59,    34,   289,   290,   291,   292,    61,   293,
      82,   313,    93,    35,    36,   360,    78,   105,   106,   340,
     111,   112,    28,    90,   273,    29,   107,   108,   109,   168,
     110,   176,   177,   178,   179,    56,    57,   234,   235,   174,
     175,    27,   282,    97,    30,   121,    31,   221,   222,   223,
      32,    33,    98,   288,    87,   125,    34,   188,   189,   127,
     190,   191,    23,    24,    25,   130,    35,    36,    26,   300,
     144,    62,   145,   150,    28,   151,   152,    63,   156,   234,
     235,   236,   158,   157,   237,   168,   319,   160,   314,   162,
     322,   385,   164,   163,   238,   165,    30,   195,    31,    64,
      65,   239,    32,    33,   166,    23,    24,    25,    34,   330,
     334,    26,   213,   335,   217,   219,   240,   241,    35,    36,
     246,   218,   247,   236,   337,   337,   237,   250,   251,   254,
     255,    27,   234,   235,   257,   261,   242,   258,    66,   262,
     267,   263,    67,   239,   266,    68,    23,    24,    25,   353,
     272,   276,    26,   274,   283,   294,    69,    70,   308,   299,
     337,   301,   302,    71,    72,   310,   305,   303,   366,   367,
     311,   316,    73,   320,    27,   321,   236,   323,   196,   237,
     324,   325,   328,   337,    74,   341,    30,   269,    31,   279,
     332,   333,    32,    33,   342,   343,   239,   168,    34,   180,
     181,   182,   183,   184,   185,   344,   187,    28,    35,    36,
      75,   197,   347,   348,   349,    27,   350,   396,   132,   133,
     134,   345,   400,   135,   346,   136,   352,   137,   354,    30,
     355,    31,   356,   357,   358,    32,   359,   362,   198,   364,
     199,   200,   201,   138,   374,   202,   203,   204,    28,   365,
     368,    35,    36,   234,   235,   234,   235,   369,   375,   205,
     376,   380,   387,   377,   132,   133,   134,   379,   139,   135,
     230,   136,   231,   137,   381,   382,    32,    33,   388,   389,
     390,   142,    34,   393,   392,   394,   395,   402,   398,   138,
     363,   307,    35,    36,   399,   401,   397,   236,   403,   236,
     237,   206,   237,   329,   280,   386,   173,   172,   186,     0,
     284,     0,   306,     0,   139,     0,   140,   239,   141,   239,
       0,     0,     0,     0,     0,     0,     0,   142
};

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-316)))

#define yytable_value_is_error(Yytable_value) \
  YYID (0)

static const yytype_int16 yycheck[] =
{
       7,    17,    19,    63,    22,   118,    66,     4,   124,     4,
       4,   127,    94,    95,    96,   155,    10,    11,     4,    13,
      34,    15,    29,    30,    31,    34,    20,    33,     4,    67,
      95,   107,     8,    94,   129,   131,    88,     3,     4,     5,
       4,    94,    95,     9,   129,    52,   127,   132,   129,   110,
      94,    95,   129,   134,   131,   370,   151,   372,   129,     4,
     302,    68,    69,   159,   160,    82,    73,   129,   128,    63,
     151,   145,   146,   139,   151,   151,   136,   130,   138,   140,
     151,   141,   142,    90,   150,   138,    72,     4,   141,   151,
       3,     4,     5,   206,   138,   337,     9,   141,   163,   151,
     160,   241,   218,   164,   142,   158,    72,   123,   126,   128,
     117,   118,   106,   119,   158,   128,   135,     0,   360,   105,
     129,   115,    69,    70,   118,   119,   120,     4,   125,   148,
     125,   148,   149,   140,   110,   148,     4,   151,   254,   105,
     128,     4,     3,     4,     5,   139,     4,   163,     9,   262,
     157,   267,     4,   148,   294,   127,   150,   129,   165,    72,
     148,   127,   134,   129,   141,   142,    32,   133,   134,    35,
      36,   231,     4,   139,   234,   235,   236,   237,     4,   239,
      21,   263,   151,   149,   150,   151,   129,   143,   144,   305,
     136,   137,   105,   125,   210,   108,   152,   153,   154,   206,
     156,   101,   102,   103,   104,    12,    13,    94,    95,    99,
     100,    72,   219,   133,   127,   148,   129,   133,   134,   135,
     133,   134,   158,   230,   231,    38,   139,   113,   114,   148,
     115,   116,     3,     4,     5,     4,   149,   150,     9,   246,
      39,    12,    39,   103,   105,     4,     4,    18,   151,    94,
      95,   138,     4,   155,   141,   262,   272,    34,   265,    34,
     276,   374,   151,   125,   151,   135,   127,   151,   129,    40,
      41,   158,   133,   134,   130,     3,     4,     5,   139,   286,
     297,     9,     4,   300,   132,   129,   125,   125,   149,   150,
     110,   148,   107,   138,   301,   302,   141,   151,   126,   148,
     130,    72,    94,    95,   151,   126,   151,   125,    79,   132,
      39,   148,    83,   158,   130,    86,     3,     4,     5,   326,
     125,   125,     9,   126,   126,   125,    97,    98,    26,   151,
     337,   125,   125,   104,   105,   126,   148,   151,   345,   346,
     128,   128,   113,   126,    72,   151,   138,   111,     4,   141,
     151,    88,   130,   360,   125,     4,   127,   129,   129,   151,
     126,   126,   133,   134,   126,   151,   158,   374,   139,   105,
     106,   107,   108,   109,   110,   128,   112,   105,   149,   150,
     151,    37,   126,   151,   126,    72,     7,   394,    75,    76,
      77,   148,   399,    80,   148,    82,    14,    84,   128,   127,
     126,   129,   151,   151,   151,   133,   126,   126,    64,    21,
      66,    67,    68,   100,   129,    71,    72,    73,   105,   151,
     151,   149,   150,    94,    95,    94,    95,   151,   151,    85,
     151,    27,   131,   151,    75,    76,    77,   151,   125,    80,
     127,    82,   129,    84,   130,   130,   133,   134,   130,   125,
      28,   138,   139,   126,   129,   127,   151,   151,   128,   100,
     339,   257,   149,   150,   132,   130,   395,   138,   402,   138,
     141,   127,   141,   284,   217,   376,    98,    97,   111,    -1,
     151,    -1,   151,    -1,   125,    -1,   127,   158,   129,   158,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   138
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    10,    11,    13,    15,    20,    63,   106,   115,   118,
     119,   120,   139,   150,   166,   167,   169,   176,   186,     4,
       4,     4,     4,     3,     4,     5,     9,    72,   105,   108,
     127,   129,   133,   134,   139,   149,   150,   193,   194,   195,
     196,   197,   198,   199,   200,   201,   202,   203,   204,   205,
     206,   207,     4,     4,     4,     4,   169,   169,     0,     4,
     167,     4,    12,    18,    40,    41,    79,    83,    86,    97,
      98,   104,   105,   113,   125,   151,   183,   193,   129,   151,
     170,   174,    21,   177,   174,   193,   193,   193,   207,   206,
     125,   207,   207,   151,   131,   159,   160,   133,   158,    95,
     163,    94,   110,   140,   164,   143,   144,   152,   153,   154,
     156,   136,   137,   145,   146,   141,   142,   127,   129,   134,
     193,   148,     4,   125,   148,    38,   175,   148,    34,   151,
       4,   188,    75,    76,    77,    80,    82,    84,   100,   125,
     127,   129,   138,   192,    39,    39,   192,   205,   193,   193,
     103,     4,     4,   193,   180,   183,   151,   155,     4,   168,
      34,   183,    34,   125,   151,   135,   130,   189,   193,   194,
     194,   194,   195,   196,   198,   198,   199,   199,   199,   199,
     200,   200,   200,   200,   200,   200,   202,   200,   203,   203,
     204,   204,   193,   189,   206,   151,     4,    37,    64,    66,
      67,    68,    71,    72,    73,    85,   127,   185,   187,     4,
     125,   167,   185,     4,   174,   185,   192,   132,   148,   129,
     190,   190,   190,   190,   192,   192,   191,   192,   193,   192,
     127,   129,   192,   205,    94,    95,   138,   141,   151,   158,
     125,   125,   151,   151,   183,   183,   110,   107,   107,   151,
     151,   126,   180,   193,   148,   130,   192,   151,   125,   167,
     193,   126,   132,   148,   128,   148,   130,    39,   189,   129,
     151,   184,   125,   167,   126,   151,   125,    34,   172,   151,
     188,   185,   193,   126,   151,   128,   148,   130,   193,   192,
     192,   192,   192,   192,   125,   181,   180,    88,   151,   151,
     193,   125,   125,   151,   185,   148,   151,   177,    26,   179,
     126,   128,   189,   194,   193,   185,   128,    69,    70,   167,
     126,   151,   167,   111,   151,    88,   131,   151,   130,   191,
     193,   180,   126,   126,   183,   183,   182,   193,   182,   132,
     185,     4,   126,   151,   128,   148,   148,   126,   151,   126,
       7,   171,    14,   193,   128,   126,   151,   151,   151,   126,
     151,   182,   126,   168,    21,   151,   193,   193,   151,   151,
       4,     8,   110,   173,   129,   151,   151,   151,   182,   151,
      27,   130,   130,   173,   173,   189,   181,   131,   130,   125,
      28,   178,   129,   126,   127,   151,   193,   179,   128,   132,
     193,   130,   151,   178
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  However,
   YYFAIL appears to be in use.  Nevertheless, it is formally deprecated
   in Bison 2.4.2's NEWS entry, where a plan to phase it out is
   discussed.  */

#define YYFAIL		goto yyerrlab
#if defined YYFAIL
  /* This is here to suppress warnings from the GCC cpp's
     -Wunused-macros.  Normally we don't worry about that warning, but
     some users do, and we want to make it easy for users to remove
     YYFAIL uses, which will produce warnings from Bison 2.5.  */
#endif

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))

/* Error token number */
#define YYTERROR	1
#define YYERRCODE	256


/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */
#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
        break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
#else
static void
yy_stack_print (yybottom, yytop)
    yytype_int16 *yybottom;
    yytype_int16 *yytop;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yyrule)
    YYSTYPE *yyvsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULL, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULL;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - Assume YYFAIL is not used.  It's too flawed to consider.  See
       <http://lists.gnu.org/archive/html/bison-patches/2009-12/msg00024.html>
       for details.  YYERROR is fine as it does not invoke this
       function.
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULL, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  YYUSE (yyvaluep);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
        break;
    }
}




/* The lookahead symbol.  */
int yychar;


#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval YY_INITIAL_VALUE(yyval_default);

/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       `yyss': related to states.
       `yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss_alloc, yyss);
	YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
/* Line 1792 of yacc.c  */
#line 230 "tmp.yy"
    {
	builder *r = (yyvsp[(1) - (1)].auxval);
	g_ast_parsed = LISTCONS(r, g_ast_parsed);
	g_ast_parsed = freverse(g_ast_parsed);
        (yyval.auxval) = (void *) 101;
	}
    break;

  case 3:
/* Line 1792 of yacc.c  */
#line 239 "tmp.yy"
    { (yyval.auxval) =	LISTEND(0);
	}
    break;

  case 4:
/* Line 1792 of yacc.c  */
#line 242 "tmp.yy"
    {

	   (yyval.auxval) = LISTCONS((yyvsp[(1) - (2)].auxval), (yyvsp[(2) - (2)].auxval));
	}
    break;

  case 5:
/* Line 1792 of yacc.c  */
#line 249 "tmp.yy"
    { (yyval.auxval) =
	LISTEND(0);
	}
    break;

  case 6:
/* Line 1792 of yacc.c  */
#line 253 "tmp.yy"
    { (yyval.auxval) =
	LISTCONS(TREE2("EE_fp", (yyvsp[(1) - (5)].auxval), (yyvsp[(3) - (5)].auxval)), (yyvsp[(5) - (5)].auxval));
	}
    break;

  case 7:
/* Line 1792 of yacc.c  */
#line 258 "tmp.yy"
    { (yyval.auxval) =
	LISTCONS(TREE2("EE_fp", (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)), LISTEND(0));
	}
    break;

  case 8:
/* Line 1792 of yacc.c  */
#line 264 "tmp.yy"
    {  (yyval.auxval) = LISTEND(0); }
    break;

  case 9:
/* Line 1792 of yacc.c  */
#line 266 "tmp.yy"
    { (yyval.auxval) =
	LISTCONS(TREE0("EE_fpercent"), (yyvsp[(2) - (2)].auxval));
	}
    break;

  case 10:
/* Line 1792 of yacc.c  */
#line 270 "tmp.yy"
    { (yyval.auxval) =
	LISTCONS(TREE0("EE_fpling"), (yyvsp[(2) - (2)].auxval));
	}
    break;

  case 11:
/* Line 1792 of yacc.c  */
#line 277 "tmp.yy"
    { (yyval.auxval) = 
	TREE1("E_formal_lst", (yyvsp[(2) - (3)].auxval));
	}
    break;

  case 12:
/* Line 1792 of yacc.c  */
#line 281 "tmp.yy"
    { (yyval.auxval) =
	TREE2("E_function", (yyvsp[(2) - (5)].auxval), (yyvsp[(5) - (5)].auxval));
	}
    break;

  case 13:
/* Line 1792 of yacc.c  */
#line 288 "tmp.yy"
    {
	(yyval.auxval) = 0;
	}
    break;

  case 14:
/* Line 1792 of yacc.c  */
#line 292 "tmp.yy"
    {
	(yyval.auxval) = (yyvsp[(2) - (2)].auxval);
	}
    break;

  case 15:
/* Line 1792 of yacc.c  */
#line 299 "tmp.yy"
    {
	(yyval.auxval) = 0;
	}
    break;

  case 16:
/* Line 1792 of yacc.c  */
#line 302 "tmp.yy"
    {
        (yyval.auxval) = 	LISTCONS(TREE0("EF_IS_INSTANCE"), 0); 
	}
    break;

  case 17:
/* Line 1792 of yacc.c  */
#line 309 "tmp.yy"
    {
	(yyval.auxval) = 0;
	}
    break;

  case 18:
/* Line 1792 of yacc.c  */
#line 313 "tmp.yy"
    { (yyval.auxval) = 	LISTCONS(TREE0("SD_in"), (yyvsp[(2) - (2)].auxval)); }
    break;

  case 19:
/* Line 1792 of yacc.c  */
#line 315 "tmp.yy"
    { (yyval.auxval) = 	LISTCONS(TREE1("SD_id", (yyvsp[(1) - (2)].auxval)), (yyvsp[(2) - (2)].auxval)); }
    break;

  case 20:
/* Line 1792 of yacc.c  */
#line 320 "tmp.yy"
    {
	(yyval.auxval) = TREE0("None"); 
	}
    break;

  case 21:
/* Line 1792 of yacc.c  */
#line 324 "tmp.yy"
    {
	(yyval.auxval) = TREE1("Some", (yyvsp[(1) - (1)].auxval));
	}
    break;

  case 22:
/* Line 1792 of yacc.c  */
#line 331 "tmp.yy"
    {
	(yyval.auxval) = TREE0("None"); 
	}
    break;

  case 23:
/* Line 1792 of yacc.c  */
#line 335 "tmp.yy"
    {
	(yyval.auxval) = TREE1("Some", (yyvsp[(2) - (2)].auxval));
	}
    break;

  case 24:
/* Line 1792 of yacc.c  */
#line 345 "tmp.yy"
    {
	(yyval.auxval) = TREE5("EE_g_decl", (yyvsp[(1) - (7)].auxval), (yyvsp[(2) - (7)].auxval), (yyvsp[(4) - (7)].auxval), (yyvsp[(5) - (7)].auxval), (yyvsp[(7) - (7)].auxval)); 
	}
    break;

  case 25:
/* Line 1792 of yacc.c  */
#line 363 "tmp.yy"
    { 
	(yyval.auxval) = TREE2("EE_type_extend", (yyvsp[(2) - (5)].auxval), (yyvsp[(4) - (5)].auxval)); 
	}
    break;

  case 26:
/* Line 1792 of yacc.c  */
#line 367 "tmp.yy"
    { 
	(yyval.auxval) = TREE2("EE_extend1", (yyvsp[(2) - (6)].auxval), (yyvsp[(4) - (6)].auxval)); 
	}
    break;

  case 27:
/* Line 1792 of yacc.c  */
#line 371 "tmp.yy"
    {
	(yyval.auxval) = TREE2("EE_typedef", (yyvsp[(2) - (5)].auxval), (yyvsp[(4) - (5)].auxval));
	}
    break;

  case 28:
/* Line 1792 of yacc.c  */
#line 375 "tmp.yy"
    {
	(yyval.auxval) = TREE2("EE_define", (yyvsp[(2) - (4)].auxval), (yyvsp[(3) - (4)].auxval));
	}
    break;

  case 29:
/* Line 1792 of yacc.c  */
#line 379 "tmp.yy"
    { 
	(yyval.auxval) = TREE3("EE_extend2", (yyvsp[(2) - (7)].auxval), (yyvsp[(3) - (7)].auxval), (yyvsp[(5) - (7)].auxval)); 
	}
    break;

  case 30:
/* Line 1792 of yacc.c  */
#line 383 "tmp.yy"
    { 
	(yyval.auxval) = TREE4("EE_extend", (yyvsp[(2) - (8)].auxval), (yyvsp[(3) - (8)].auxval), (yyvsp[(4) - (8)].auxval), (yyvsp[(6) - (8)].auxval)); 
	}
    break;

  case 31:
/* Line 1792 of yacc.c  */
#line 390 "tmp.yy"
    {
	(yyval.auxval) = TREE1("EE_keep", (yyvsp[(2) - (3)].auxval)); 
	}
    break;

  case 32:
/* Line 1792 of yacc.c  */
#line 394 "tmp.yy"
    {
	(yyval.auxval) = TREE1("EE_keepsoft", (yyvsp[(3) - (4)].auxval)); 
	}
    break;

  case 33:
/* Line 1792 of yacc.c  */
#line 400 "tmp.yy"
    {
	(yyval.auxval) = TREE3("EE_event_decl", (yyvsp[(2) - (6)].auxval), (yyvsp[(3) - (6)].auxval), TREE1("Some", (yyvsp[(5) - (6)].auxval))); 
	}
    break;

  case 34:
/* Line 1792 of yacc.c  */
#line 404 "tmp.yy"
    {
	(yyval.auxval) = TREE2("EE_event_decl", (yyvsp[(2) - (3)].auxval), TREE0("None")); 
	}
    break;

  case 35:
/* Line 1792 of yacc.c  */
#line 408 "tmp.yy"
    {
	(yyval.auxval) = TREE2("EE_event_sens", (yyvsp[(2) - (4)].auxval), (yyvsp[(3) - (4)].auxval)); 
	}
    break;

  case 36:
/* Line 1792 of yacc.c  */
#line 413 "tmp.yy"
    {
	(yyval.auxval) = TREE4("EE_struct_def", (yyvsp[(2) - (8)].auxval), (yyvsp[(3) - (8)].auxval), (yyvsp[(4) - (8)].auxval), (yyvsp[(6) - (8)].auxval)); 
	}
    break;

  case 37:
/* Line 1792 of yacc.c  */
#line 417 "tmp.yy"
    {
	(yyval.auxval) = TREE3("EE_unit_def", (yyvsp[(2) - (7)].auxval), (yyvsp[(3) - (7)].auxval), (yyvsp[(5) - (7)].auxval)); 
	}
    break;

  case 38:
/* Line 1792 of yacc.c  */
#line 422 "tmp.yy"
    {
	(yyval.auxval) = TREE3("EE_cover", (yyvsp[(2) - (8)].auxval), (yyvsp[(3) - (8)].auxval), (yyvsp[(6) - (8)].auxval)); 
	}
    break;

  case 39:
/* Line 1792 of yacc.c  */
#line 427 "tmp.yy"
    {
 	(yyval.auxval) = TREE3("EE_ea", (yyvsp[(1) - (3)].auxval), TREE0("None"), TREE0("None"));
	}
    break;

  case 40:
/* Line 1792 of yacc.c  */
#line 432 "tmp.yy"
    {
 	(yyval.auxval) = TREE3("EE_ea", (yyvsp[(1) - (5)].auxval), TREE1("Some", (yyvsp[(4) - (5)].auxval)), TREE0("None"));
	}
    break;

  case 41:
/* Line 1792 of yacc.c  */
#line 438 "tmp.yy"
    {
 	(yyval.auxval) = TREE3("EE_ea", (yyvsp[(1) - (10)].auxval), TREE1("Some", (yyvsp[(4) - (10)].auxval)), TREE1("SOME", (yyvsp[(9) - (10)].auxval)));
	}
    break;

  case 42:
/* Line 1792 of yacc.c  */
#line 448 "tmp.yy"
    { (yyval.auxval) =
	LISTEND(0);
	}
    break;

  case 43:
/* Line 1792 of yacc.c  */
#line 452 "tmp.yy"
    { (yyval.auxval) =
	LISTCONS((yyvsp[(2) - (4)].auxval), (yyvsp[(4) - (4)].auxval));
	}
    break;

  case 44:
/* Line 1792 of yacc.c  */
#line 461 "tmp.yy"
    { (yyval.auxval) =
	LISTEND(0);
	}
    break;

  case 45:
/* Line 1792 of yacc.c  */
#line 466 "tmp.yy"
    { (yyval.auxval) =
	LISTCONS(TREE2("EE_range_node", (yyvsp[(4) - (10)].auxval), (yyvsp[(7) - (10)].auxval)), (yyvsp[(10) - (10)].auxval));
	}
    break;

  case 46:
/* Line 1792 of yacc.c  */
#line 487 "tmp.yy"
    { (yyval.auxval) =
	LISTEND(0);
	}
    break;

  case 47:
/* Line 1792 of yacc.c  */
#line 491 "tmp.yy"
    { (yyval.auxval) =
	LISTCONS(TREE2("EE_cover_item_ranges", (yyvsp[(2) - (10)].auxval), (yyvsp[(7) - (10)].auxval)), (yyvsp[(10) - (10)].auxval));
	}
    break;

  case 48:
/* Line 1792 of yacc.c  */
#line 499 "tmp.yy"
    { (yyval.auxval) =
	LISTEND(0);
	}
    break;

  case 49:
/* Line 1792 of yacc.c  */
#line 503 "tmp.yy"
    { (yyval.auxval) =
	LISTCONS((yyvsp[(1) - (2)].auxval), (yyvsp[(2) - (2)].auxval));
	}
    break;

  case 50:
/* Line 1792 of yacc.c  */
#line 510 "tmp.yy"
    { (yyval.auxval) =
	LISTEND(0);
	}
    break;

  case 51:
/* Line 1792 of yacc.c  */
#line 514 "tmp.yy"
    { (yyval.auxval) =
	LISTCONS((yyvsp[(2) - (5)].auxval), (yyvsp[(5) - (5)].auxval));
	}
    break;

  case 52:
/* Line 1792 of yacc.c  */
#line 522 "tmp.yy"
    { (yyval.auxval) =
	LISTEND(0);
	}
    break;

  case 53:
/* Line 1792 of yacc.c  */
#line 525 "tmp.yy"
    { (yyval.auxval) =
	LISTCONS((yyvsp[(1) - (2)].auxval), (yyvsp[(2) - (2)].auxval));
	}
    break;

  case 54:
/* Line 1792 of yacc.c  */
#line 528 "tmp.yy"
    { (yyval.auxval) =
	LISTCONS((yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval));
	}
    break;

  case 55:
/* Line 1792 of yacc.c  */
#line 536 "tmp.yy"
    { (yyval.auxval) = 
	EE_LINEPOINT(TREE3("EE_lvardecl", (yyvsp[(2) - (5)].auxval), (yyvsp[(4) - (5)].auxval), TREE0("None")));
	}
    break;

  case 56:
/* Line 1792 of yacc.c  */
#line 540 "tmp.yy"
    { (yyval.auxval) = 
	EE_LINEPOINT(TREE3("EE_lvardecl", (yyvsp[(2) - (7)].auxval), (yyvsp[(4) - (7)].auxval), TREE1("Some", (yyvsp[(6) - (7)].auxval))));
	}
    break;

  case 57:
/* Line 1792 of yacc.c  */
#line 544 "tmp.yy"
    { (yyval.auxval) = 
	EE_LINEPOINT(TREE1("EE_e_as_c", (yyvsp[(1) - (1)].auxval)));
	}
    break;

  case 58:
/* Line 1792 of yacc.c  */
#line 549 "tmp.yy"
    { (yyval.auxval) = 
	EE_LINEPOINT(TREE2("EE_foreach", (yyvsp[(4) - (6)].auxval), (yyvsp[(5) - (6)].auxval)));
	}
    break;

  case 59:
/* Line 1792 of yacc.c  */
#line 553 "tmp.yy"
    { (yyval.auxval) = 
	EE_LINEPOINT(TREE2("EE_while", (yyvsp[(2) - (4)].auxval), (yyvsp[(3) - (4)].auxval)));
	}
    break;

  case 60:
/* Line 1792 of yacc.c  */
#line 557 "tmp.yy"
    { (yyval.auxval) = 
	EE_LINEPOINT(TREE3("EE_if", (yyvsp[(2) - (4)].auxval), (yyvsp[(3) - (4)].auxval), TREE0("None")));
	}
    break;

  case 61:
/* Line 1792 of yacc.c  */
#line 561 "tmp.yy"
    { (yyval.auxval) = 
	EE_LINEPOINT(TREE1("EE_emit", (yyvsp[(2) - (3)].auxval)));
	}
    break;

  case 62:
/* Line 1792 of yacc.c  */
#line 565 "tmp.yy"
    { (yyval.auxval) = 
	EE_LINEPOINT(TREE0("EE_skip"));
	}
    break;

  case 63:
/* Line 1792 of yacc.c  */
#line 569 "tmp.yy"
    { (yyval.auxval) = 
	EE_LINEPOINT(TREE1("EE_return", (yyvsp[(2) - (3)].auxval)));
	}
    break;

  case 64:
/* Line 1792 of yacc.c  */
#line 573 "tmp.yy"
    { (yyval.auxval) = 
	EE_LINEPOINT(TREE1("EE_wait", (yyvsp[(2) - (3)].auxval)));
	}
    break;

  case 65:
/* Line 1792 of yacc.c  */
#line 577 "tmp.yy"
    { (yyval.auxval) = 
	EE_LINEPOINT(TREE1("EE_sync", (yyvsp[(2) - (3)].auxval)));
	}
    break;

  case 66:
/* Line 1792 of yacc.c  */
#line 581 "tmp.yy"
    { (yyval.auxval) = 
	EE_LINEPOINT(TREE3("EE_if", (yyvsp[(2) - (5)].auxval), (yyvsp[(3) - (5)].auxval), TREE1("Some", (yyvsp[(5) - (5)].auxval))));
	}
    break;

  case 67:
/* Line 1792 of yacc.c  */
#line 585 "tmp.yy"
    { (yyval.auxval) = 
	EE_LINEPOINT(TREE1("EE_block", (yyvsp[(2) - (3)].auxval)));
	}
    break;

  case 68:
/* Line 1792 of yacc.c  */
#line 589 "tmp.yy"
    {
	(yyval.auxval) = TREE2("EE_do_keeping", (yyvsp[(2) - (7)].auxval), (yyvsp[(5) - (7)].auxval)); 
	}
    break;

  case 69:
/* Line 1792 of yacc.c  */
#line 593 "tmp.yy"
    {
	(yyval.auxval) = TREE2("EE_gen", (yyvsp[(2) - (7)].auxval), TREE1("Some", (yyvsp[(5) - (7)].auxval))); 
	}
    break;

  case 70:
/* Line 1792 of yacc.c  */
#line 597 "tmp.yy"
    { 
	(yyval.auxval) = TREE2("EE_gen", (yyvsp[(2) - (3)].auxval), TREE0("None")); 
	}
    break;

  case 71:
/* Line 1792 of yacc.c  */
#line 602 "tmp.yy"
    {
	(yyval.auxval) = TREE1("EE_allof", (yyvsp[(4) - (6)].auxval)); 
	}
    break;

  case 72:
/* Line 1792 of yacc.c  */
#line 606 "tmp.yy"
    {
	(yyval.auxval) = TREE1("EE_firstof", (yyvsp[(4) - (6)].auxval)); 
	}
    break;

  case 73:
/* Line 1792 of yacc.c  */
#line 611 "tmp.yy"
    { (yyval.auxval) = 
	EE_LINEPOINT(TREE2("EE_colonassign", (yyvsp[(1) - (4)].auxval), (yyvsp[(3) - (4)].auxval)));
	}
    break;

  case 74:
/* Line 1792 of yacc.c  */
#line 650 "tmp.yy"
    {
 	(yyval.auxval) = TREE1("EE_bits", (yyvsp[(4) - (5)].auxval));
	}
    break;

  case 75:
/* Line 1792 of yacc.c  */
#line 654 "tmp.yy"
    {
 	(yyval.auxval) = TREE1("EE_bytes", (yyvsp[(4) - (5)].auxval));
	}
    break;

  case 77:
/* Line 1792 of yacc.c  */
#line 662 "tmp.yy"
    {
 	(yyval.auxval) = TREE2("EE_typequal", (yyvsp[(1) - (2)].auxval), (yyvsp[(2) - (2)].auxval));
	}
    break;

  case 78:
/* Line 1792 of yacc.c  */
#line 667 "tmp.yy"
    {
 	(yyval.auxval) = TREE0("EE_bit");
	}
    break;

  case 79:
/* Line 1792 of yacc.c  */
#line 671 "tmp.yy"
    {
 	(yyval.auxval) = TREE0("EE_byte");
	}
    break;

  case 80:
/* Line 1792 of yacc.c  */
#line 675 "tmp.yy"
    {
 	(yyval.auxval) = TREE0("EE_bool");
	}
    break;

  case 81:
/* Line 1792 of yacc.c  */
#line 679 "tmp.yy"
    {
 	(yyval.auxval) = TREE0("EE_string");
	}
    break;

  case 82:
/* Line 1792 of yacc.c  */
#line 683 "tmp.yy"
    {
 	(yyval.auxval) = TREE0("EE_integer");
	}
    break;

  case 83:
/* Line 1792 of yacc.c  */
#line 687 "tmp.yy"
    {
 	(yyval.auxval) = TREE0("EE_time");
	}
    break;

  case 84:
/* Line 1792 of yacc.c  */
#line 692 "tmp.yy"
    {
 	(yyval.auxval) = TREE0("EE_integer");
	}
    break;

  case 85:
/* Line 1792 of yacc.c  */
#line 696 "tmp.yy"
    {
 	(yyval.auxval) = TREE1("EE_list", (yyvsp[(3) - (3)].auxval));
	}
    break;

  case 86:
/* Line 1792 of yacc.c  */
#line 700 "tmp.yy"
    {
 	(yyval.auxval) = TREE1("EE_usertype", (yyvsp[(1) - (1)].auxval));
	}
    break;

  case 87:
/* Line 1792 of yacc.c  */
#line 704 "tmp.yy"
    {
 	(yyval.auxval) = TREE0("EE_uint");
	}
    break;

  case 88:
/* Line 1792 of yacc.c  */
#line 711 "tmp.yy"
    { (yyval.auxval) = TREE0("ED_expect"); }
    break;

  case 89:
/* Line 1792 of yacc.c  */
#line 712 "tmp.yy"
    { (yyval.auxval) = TREE0("ED_assume"); }
    break;

  case 90:
/* Line 1792 of yacc.c  */
#line 717 "tmp.yy"
    { (yyval.auxval) =
	TREE1("EE_enum", (yyvsp[(2) - (3)].auxval));
    }
    break;

  case 91:
/* Line 1792 of yacc.c  */
#line 737 "tmp.yy"
    { (yyval.auxval) =
	LISTEND(0);
	}
    break;

  case 92:
/* Line 1792 of yacc.c  */
#line 740 "tmp.yy"
    { (yyval.auxval) = LISTCONS((yyvsp[(1) - (1)].auxval), LISTEND(0)); }
    break;

  case 93:
/* Line 1792 of yacc.c  */
#line 742 "tmp.yy"
    { (yyval.auxval) = LISTCONS((yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); }
    break;

  case 94:
/* Line 1792 of yacc.c  */
#line 749 "tmp.yy"
    { (yyval.auxval) =
	LISTEND(0);
	}
    break;

  case 95:
/* Line 1792 of yacc.c  */
#line 753 "tmp.yy"
    { (yyval.auxval) =
	LISTCONS((yyvsp[(1) - (1)].auxval), LISTEND(0));
	}
    break;

  case 96:
/* Line 1792 of yacc.c  */
#line 757 "tmp.yy"
    { (yyval.auxval) =
	LISTCONS((yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval));
	}
    break;

  case 97:
/* Line 1792 of yacc.c  */
#line 763 "tmp.yy"
    { (yyval.auxval) = 
	TREE1("EE_t_e", (yyvsp[(2) - (3)].auxval));
	}
    break;

  case 98:
/* Line 1792 of yacc.c  */
#line 770 "tmp.yy"
    { (yyval.auxval) =
	LISTEND(0);
	}
    break;

  case 99:
/* Line 1792 of yacc.c  */
#line 774 "tmp.yy"
    { (yyval.auxval) =
	LISTCONS((yyvsp[(1) - (1)].auxval), LISTEND(0));
	}
    break;

  case 100:
/* Line 1792 of yacc.c  */
#line 778 "tmp.yy"
    { (yyval.auxval) =
	LISTCONS((yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval));
	}
    break;

  case 101:
/* Line 1792 of yacc.c  */
#line 789 "tmp.yy"
    { (yyval.auxval) = 
	TREE2("EE_t_and", (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 102:
/* Line 1792 of yacc.c  */
#line 793 "tmp.yy"
    { (yyval.auxval) = 
	TREE2("EE_t_or", (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 103:
/* Line 1792 of yacc.c  */
#line 797 "tmp.yy"
    { (yyval.auxval) = 
	TREE2("EE_t_arrow", (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 104:
/* Line 1792 of yacc.c  */
#line 801 "tmp.yy"
    { (yyval.auxval) = (yyvsp[(2) - (3)].auxval);
	}
    break;

  case 105:
/* Line 1792 of yacc.c  */
#line 805 "tmp.yy"
    { (yyval.auxval) =
	TREE1("EE_t_seq", (yyvsp[(2) - (3)].auxval));
	}
    break;

  case 106:
/* Line 1792 of yacc.c  */
#line 809 "tmp.yy"
    { (yyval.auxval) = 
	TREE1("EE_t_true", (yyvsp[(2) - (2)].auxval)); 
	}
    break;

  case 107:
/* Line 1792 of yacc.c  */
#line 813 "tmp.yy"
    { (yyval.auxval) = 
	TREE1("EE_t_fail", (yyvsp[(2) - (2)].auxval)); 
	}
    break;

  case 108:
/* Line 1792 of yacc.c  */
#line 817 "tmp.yy"
    { (yyval.auxval) = 
	TREE1("EE_t_eventually", (yyvsp[(2) - (2)].auxval)); 
	}
    break;

  case 109:
/* Line 1792 of yacc.c  */
#line 821 "tmp.yy"
    { (yyval.auxval) = 
	TREE1("EE_t_rise", (yyvsp[(2) - (2)].auxval)); 
	}
    break;

  case 110:
/* Line 1792 of yacc.c  */
#line 824 "tmp.yy"
    { (yyval.auxval) = 
	TREE1("EE_t_fall", (yyvsp[(2) - (2)].auxval)); 
	}
    break;

  case 111:
/* Line 1792 of yacc.c  */
#line 828 "tmp.yy"
    { (yyval.auxval) = 
	TREE1("EE_t_change", (yyvsp[(2) - (2)].auxval)); 
	}
    break;

  case 112:
/* Line 1792 of yacc.c  */
#line 834 "tmp.yy"
    {
 	(yyval.auxval)= TREE2("EE_t_star", (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval));
	}
    break;

  case 113:
/* Line 1792 of yacc.c  */
#line 838 "tmp.yy"
    {
 	(yyval.auxval)= TREE0("EE_t_cycle");
	}
    break;

  case 114:
/* Line 1792 of yacc.c  */
#line 844 "tmp.yy"
    {
 	(yyval.auxval)= TREE1("EE_t_count", (yyvsp[(2) - (3)].auxval));
	}
    break;

  case 115:
/* Line 1792 of yacc.c  */
#line 848 "tmp.yy"
    {
 	(yyval.auxval)= TREE2("EE_t_count2", (yyvsp[(2) - (5)].auxval), (yyvsp[(4) - (5)].auxval));
	}
    break;

  case 116:
/* Line 1792 of yacc.c  */
#line 854 "tmp.yy"
    {
 	(yyval.auxval)= TREE1("EE_ev_ex", (yyvsp[(2) - (2)].auxval));
	}
    break;

  case 117:
/* Line 1792 of yacc.c  */
#line 858 "tmp.yy"
    {
 	(yyval.auxval)= TREE1("EE_ev_ev", (yyvsp[(2) - (2)].auxval));
	}
    break;

  case 118:
/* Line 1792 of yacc.c  */
#line 862 "tmp.yy"
    {
 	(yyval.auxval)= TREE2("EE_ev_sampled", (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval));
	}
    break;

  case 119:
/* Line 1792 of yacc.c  */
#line 884 "tmp.yy"
    { (yyval.auxval) = (yyvsp[(1) - (1)].auxval); }
    break;

  case 120:
/* Line 1792 of yacc.c  */
#line 886 "tmp.yy"
    { (yyval.auxval) =
	TREE2("EO_assign", (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval));
	}
    break;

  case 121:
/* Line 1792 of yacc.c  */
#line 891 "tmp.yy"
    { (yyval.auxval) =
	TREE2("EE_assign", (yyvsp[(1) - (3)].auxval), 	TREE3("EO_diadic", YYLEAF("EE_plus"), (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval))); 
	}
    break;

  case 122:
/* Line 1792 of yacc.c  */
#line 897 "tmp.yy"
    { (yyval.auxval) =
	TREE2("EE_assign", (yyvsp[(1) - (3)].auxval), 	TREE3("EO_diadic", YYLEAF("EE_minus"), (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval))); 
	}
    break;

  case 123:
/* Line 1792 of yacc.c  */
#line 905 "tmp.yy"
    { (yyval.auxval) = (yyvsp[(1) - (1)].auxval); }
    break;

  case 124:
/* Line 1792 of yacc.c  */
#line 906 "tmp.yy"
    { (yyval.auxval) = TREE3("EE_query", (yyvsp[(1) - (5)].auxval), (yyvsp[(3) - (5)].auxval), (yyvsp[(5) - (5)].auxval)); }
    break;

  case 125:
/* Line 1792 of yacc.c  */
#line 910 "tmp.yy"
    { (yyval.auxval) = (yyvsp[(1) - (1)].auxval); }
    break;

  case 126:
/* Line 1792 of yacc.c  */
#line 912 "tmp.yy"
    { (yyval.auxval) = 
	TREE3("EE_diadic", YYLEAF("EO_implies"), (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 127:
/* Line 1792 of yacc.c  */
#line 919 "tmp.yy"
    { (yyval.auxval) = (yyvsp[(1) - (1)].auxval); }
    break;

  case 128:
/* Line 1792 of yacc.c  */
#line 925 "tmp.yy"
    { (yyval.auxval) = (yyvsp[(1) - (1)].auxval); }
    break;

  case 129:
/* Line 1792 of yacc.c  */
#line 926 "tmp.yy"
    { (yyval.auxval) = 
	TREE3("EE_diadic", YYLEAF("EO_logor"), (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 130:
/* Line 1792 of yacc.c  */
#line 930 "tmp.yy"
    { (yyval.auxval) = 
	TREE3("EE_diadic", YYLEAF("EO_logor"), (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 131:
/* Line 1792 of yacc.c  */
#line 937 "tmp.yy"
    { (yyval.auxval) = (yyvsp[(1) - (1)].auxval); }
    break;

  case 132:
/* Line 1792 of yacc.c  */
#line 939 "tmp.yy"
    { (yyval.auxval) = 
	TREE3("EE_diadic", YYLEAF("EO_logand"), (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 133:
/* Line 1792 of yacc.c  */
#line 943 "tmp.yy"
    { (yyval.auxval) = 
	TREE3("EE_diadic", YYLEAF("EO_logand"), (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 134:
/* Line 1792 of yacc.c  */
#line 949 "tmp.yy"
    { (yyval.auxval) = 
	TREE3("EE_diadic", YYLEAF("EO_in"), (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 135:
/* Line 1792 of yacc.c  */
#line 953 "tmp.yy"
    { (yyval.auxval) = 
	TREE3("EE_diadic", YYLEAF("EO_xor"), (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 136:
/* Line 1792 of yacc.c  */
#line 959 "tmp.yy"
    { (yyval.auxval) = (yyvsp[(1) - (1)].auxval); }
    break;

  case 137:
/* Line 1792 of yacc.c  */
#line 961 "tmp.yy"
    { (yyval.auxval) = 
	TREE3("EE_diadic", YYLEAF("EO_deqd"), (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 138:
/* Line 1792 of yacc.c  */
#line 964 "tmp.yy"
    { (yyval.auxval) = 
	TREE3("EE_diadic", YYLEAF("EO_dned"), (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 139:
/* Line 1792 of yacc.c  */
#line 967 "tmp.yy"
    { (yyval.auxval) = 
	TREE3("EE_diadic", YYLEAF("EO_dltd"), (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 140:
/* Line 1792 of yacc.c  */
#line 970 "tmp.yy"
    { (yyval.auxval) = 
	TREE3("EE_diadic", YYLEAF("EO_dled"), (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 141:
/* Line 1792 of yacc.c  */
#line 973 "tmp.yy"
    { (yyval.auxval) = 
	TREE3("EE_diadic", YYLEAF("EO_dgtd"), (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 142:
/* Line 1792 of yacc.c  */
#line 976 "tmp.yy"
    { (yyval.auxval) = 
	TREE3("EE_diadic", YYLEAF("EO_dged"), (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 143:
/* Line 1792 of yacc.c  */
#line 984 "tmp.yy"
    { (yyval.auxval) = (yyvsp[(1) - (1)].auxval); }
    break;

  case 144:
/* Line 1792 of yacc.c  */
#line 985 "tmp.yy"
    { (yyval.auxval) = 
	TREE3("EE_diadic", YYLEAF("EO_binor"), (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 145:
/* Line 1792 of yacc.c  */
#line 993 "tmp.yy"
    { (yyval.auxval) = (yyvsp[(1) - (1)].auxval); }
    break;

  case 146:
/* Line 1792 of yacc.c  */
#line 994 "tmp.yy"
    { (yyval.auxval) = 
	TREE3("EE_diadic", YYLEAF("EO_bitand"), (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 147:
/* Line 1792 of yacc.c  */
#line 1001 "tmp.yy"
    { (yyval.auxval) = (yyvsp[(1) - (1)].auxval); }
    break;

  case 148:
/* Line 1792 of yacc.c  */
#line 1002 "tmp.yy"
    { (yyval.auxval) = 
	TREE3("EE_diadic", YYLEAF("EO_plus"), (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 149:
/* Line 1792 of yacc.c  */
#line 1007 "tmp.yy"
    { (yyval.auxval) = 
	TREE3("EE_diadic", YYLEAF("EO_minus"), (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 150:
/* Line 1792 of yacc.c  */
#line 1015 "tmp.yy"
    { (yyval.auxval) = (yyvsp[(1) - (1)].auxval); }
    break;

  case 151:
/* Line 1792 of yacc.c  */
#line 1017 "tmp.yy"
    { (yyval.auxval) = 
	TREE3("EE_diadic", YYLEAF("EO_times"), (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 152:
/* Line 1792 of yacc.c  */
#line 1021 "tmp.yy"
    { (yyval.auxval) = 
	TREE3("EE_diadic", YYLEAF("EO_divide"), (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 153:
/* Line 1792 of yacc.c  */
#line 1028 "tmp.yy"
    { (yyval.auxval) = (yyvsp[(1) - (1)].auxval); }
    break;

  case 154:
/* Line 1792 of yacc.c  */
#line 1032 "tmp.yy"
    { (yyval.auxval) = (yyvsp[(1) - (1)].auxval); }
    break;

  case 155:
/* Line 1792 of yacc.c  */
#line 1034 "tmp.yy"
    { (yyval.auxval) = 
	TREE3("EE_slice", (yyvsp[(1) - (6)].auxval), (yyvsp[(3) - (6)].auxval), (yyvsp[(5) - (6)].auxval));
	}
    break;

  case 156:
/* Line 1792 of yacc.c  */
#line 1038 "tmp.yy"
    { (yyval.auxval) = 
	TREE2("EE_subscript", (yyvsp[(1) - (4)].auxval), (yyvsp[(3) - (4)].auxval));
	}
    break;

  case 157:
/* Line 1792 of yacc.c  */
#line 1042 "tmp.yy"
    { (yyval.auxval) = 
	TREE1("EE_medot", (yyvsp[(2) - (2)].auxval));
	}
    break;

  case 158:
/* Line 1792 of yacc.c  */
#line 1045 "tmp.yy"
    { (yyval.auxval) =
	TREE1("EE_bitcat", (yyvsp[(3) - (4)].auxval));
	}
    break;

  case 159:
/* Line 1792 of yacc.c  */
#line 1049 "tmp.yy"
    { (yyval.auxval) = 
	TREE3("EE_diadic", YYLEAF("EO_dotaccess"), (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 160:
/* Line 1792 of yacc.c  */
#line 1053 "tmp.yy"
    { (yyval.auxval) =
	TREE2("EE_apply", (yyvsp[(1) - (4)].auxval), (yyvsp[(3) - (4)].auxval));
    }
    break;

  case 161:
/* Line 1792 of yacc.c  */
#line 1061 "tmp.yy"
    { (yyval.auxval) =
	TREE1("EE_id", (yyvsp[(1) - (1)].auxval));
	}
    break;

  case 162:
/* Line 1792 of yacc.c  */
#line 1065 "tmp.yy"
    { (yyval.auxval) =
	TREE0("EE_timeval");
	}
    break;

  case 163:
/* Line 1792 of yacc.c  */
#line 1069 "tmp.yy"
    { (yyval.auxval) =
	TREE0("EE_genval");
	}
    break;

  case 164:
/* Line 1792 of yacc.c  */
#line 1077 "tmp.yy"
    { (yyval.auxval) =
	TREE2("EE_range", (yyvsp[(2) - (5)].auxval), (yyvsp[(4) - (5)].auxval));
    }
    break;

  case 165:
/* Line 1792 of yacc.c  */
#line 1082 "tmp.yy"
    { (yyval.auxval) = (yyvsp[(1) - (1)].auxval); }
    break;

  case 166:
/* Line 1792 of yacc.c  */
#line 1084 "tmp.yy"
    { (yyval.auxval) = (yyvsp[(2) - (3)].auxval); }
    break;

  case 167:
/* Line 1792 of yacc.c  */
#line 1087 "tmp.yy"
    { (yyval.auxval) =
	TREE1("EE_channel_read", (yyvsp[(2) - (2)].auxval));
	}
    break;

  case 168:
/* Line 1792 of yacc.c  */
#line 1092 "tmp.yy"
    { (yyval.auxval) =
	TREE1("EE_num", (yyvsp[(1) - (1)].auxval));
	}
    break;

  case 169:
/* Line 1792 of yacc.c  */
#line 1096 "tmp.yy"
    { (yyval.auxval) =
	TREE1("EE_dstring", (yyvsp[(1) - (1)].auxval));
	}
    break;

  case 170:
/* Line 1792 of yacc.c  */
#line 1102 "tmp.yy"
    { (yyval.auxval) =
	TREE1("EE_stringval", (yyvsp[(1) - (1)].auxval));
	}
    break;

  case 171:
/* Line 1792 of yacc.c  */
#line 1107 "tmp.yy"
    { (yyval.auxval) =
	TREE1("EE_binnot", (yyvsp[(2) - (2)].auxval));
	}
    break;

  case 172:
/* Line 1792 of yacc.c  */
#line 1112 "tmp.yy"
    { (yyval.auxval) =
	TREE1("EE_lognot", (yyvsp[(2) - (2)].auxval));
	}
    break;


/* Line 1792 of yacc.c  */
#line 3233 "ee.y.tab.c"
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}


/* Line 2055 of yacc.c  */
#line 1117 "tmp.yy"


#include <stdio.h>
#include <ctype.h>
#include "lineno.h"

extern FILE *stderr;
int yyerror (const char *s) 
{
	extern void exit(int);	
	extern builder *lishlex();
	int line_number = filestack[filesp].line_number;
	const char *fn = filestack[filesp].fn;
	printf("+++ Syntax error: %s: %s:; %i\n", s, fn, line_number);
	printf("Next symbol: %s\n", atom_to_str(lishlex()));
	fflush(stdout);
	fflush(stderr);
	exit(1);
	return 1;

}

void yydebug_off()
{
#ifdef YYDEBUG
//	yydebug = 0;
#endif
}

char *smllib = "eegram", *langsuffix = ".e";

int g_need_ee_starter = 0; // Turn this off for now - the strange php style bracketing.


/*
 * Two characters which define the escape seq for comment to end of line 
 */
char commentchar1 = '/';
char commentchar2 = '/';


int yylex()
{
  int vd = lextracef;
  char s;  int v = 0;
  extern builder *lishlex();
  extern int lextracef;
  builder *keywordf, *a = lishlex();

  if (!a)
  {
    printf("cbg: yylex EOF\n");
    return 0;	
  }	  
  yylval.auxval = a;

//  printf("yylex called\n");

  if (fnumberp(a)) return sd_number;
  if (fstringp(a)) return sd_string;
  if (fdstringp(a)) return sd_dstring;
  keywordf = fgetprop(a, smacro);
  if (vd)  printf("  eegram Lex %i macrof=%p '%s'\n", v, keywordf, atom_to_str(a)); fflush(stdout);
  if (keywordf) 
  {
     int symno = atom_to_int(fcdr(keywordf));
     if (vd)printf("keyword symno=%i\n", symno);
     return symno;
  }
  s = atom_to_str(a)[0];
  if (isalpha(s) || s=='_') return sd_id;
  cbgerror(cbge_fatal, "Illegal input token %c", s);
  return 0;
}


/* eof */

