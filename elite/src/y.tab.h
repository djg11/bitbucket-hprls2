/* A Bison parser, made by GNU Bison 2.7.  */

/* Bison interface for Yacc-like parsers in C
   
      Copyright (C) 1984, 1989-1990, 2000-2012 Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_EE_Y_TAB_H_INCLUDED
# define YY_YY_EE_Y_TAB_H_INCLUDED
/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     sd_string = 258,
     sd_id = 259,
     sd_number = 260,
     sd_char = 261,
     sd_synthembed_start = 262,
     sd_synthembed_end = 263,
     sd_dstring = 264,
     s_on = 265,
     s_event = 266,
     s_var = 267,
     s_assume = 268,
     s_dut_error = 269,
     s_expect = 270,
     s_check = 271,
     s_that = 272,
     s_wait = 273,
     s_until = 274,
     s_cover = 275,
     s_using = 276,
     s_count_only = 277,
     s_trace_only = 278,
     s_at_least = 279,
     s_transition = 280,
     s_item = 281,
     s_ranges = 282,
     s_range = 283,
     s_ignore = 284,
     s_illegal = 285,
     s_cross = 286,
     s_text = 287,
     s_print = 288,
     s_is = 289,
     s_also = 290,
     s_only = 291,
     s_list = 292,
     s_like = 293,
     s_of = 294,
     s_first = 295,
     s_all = 296,
     s_compute = 297,
     s_radix = 298,
     s_hex = 299,
     s_post_generate = 300,
     s_pre_generate = 301,
     s_setup_test = 302,
     s_finalize_test = 303,
     s_extract_test = 304,
     s_init = 305,
     s_as_a = 306,
     s_quit = 307,
     s_lock = 308,
     s_unlock = 309,
     s_release = 310,
     s_swap = 311,
     s_to_string = 312,
     s_value = 313,
     s_and_all = 314,
     s_apply = 315,
     s_average = 316,
     s_exists = 317,
     s_unit = 318,
     s_integer = 319,
     s_real = 320,
     s_int = 321,
     s_uint = 322,
     s_byte = 323,
     s_bytes = 324,
     s_bits = 325,
     s_bit = 326,
     s_time = 327,
     s_string = 328,
     s_locker = 329,
     s_change = 330,
     s_rise = 331,
     s_fall = 332,
     s_delay = 333,
     s_sync = 334,
     s_true = 335,
     s_detach = 336,
     s_eventually = 337,
     s_emit = 338,
     s_cycle = 339,
     s_bool = 340,
     s_if = 341,
     s_then = 342,
     s_else = 343,
     s_when = 344,
     s_case = 345,
     s_casex = 346,
     s_casez = 347,
     s_default = 348,
     s_and = 349,
     s_or = 350,
     s_not = 351,
     s_while = 352,
     s_for = 353,
     s_from = 354,
     s_fail = 355,
     s_to = 356,
     s_step = 357,
     s_each = 358,
     s_do = 359,
     s_gen = 360,
     s_keep = 361,
     s_keeping = 362,
     s_soft = 363,
     s_index = 364,
     s_in = 365,
     s_instance = 366,
     s_new = 367,
     s_return = 368,
     s_select = 369,
     s_define = 370,
     s_as = 371,
     s_computed = 372,
     s_type = 373,
     s_extend = 374,
     s_struct = 375,
     s_verilog = 376,
     s_variable = 377,
     s_import = 378,
     ss_quote = 379,
     ss_lsect = 380,
     ss_rsect = 381,
     ss_lbra = 382,
     ss_rbra = 383,
     ss_lpar = 384,
     ss_rpar = 385,
     ss_equals = 386,
     ss_comma = 387,
     ss_query = 388,
     ss_dot = 389,
     ss_dotdot = 390,
     ss_ampersand = 391,
     ss_stile = 392,
     ss_snail = 393,
     ss_percent = 394,
     ss_caret = 395,
     ss_star = 396,
     ss_slash = 397,
     ss_dltd = 398,
     ss_dgtd = 399,
     ss_plus = 400,
     ss_minus = 401,
     ss_hash = 402,
     ss_colon = 403,
     ss_tilda = 404,
     ss_pling = 405,
     ss_semicolon = 406,
     ss_dled = 407,
     ss_dged = 408,
     ss_deqd = 409,
     ss_colonequals = 410,
     ss_dned = 411,
     ss_rarrow1 = 412,
     ss_rarrow2 = 413,
     ss_plusequals = 414,
     ss_minusequals = 415,
     ss_lshift = 416,
     ss_rshift = 417,
     ss_logor = 418,
     ss_logand = 419
   };
#endif


#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{
/* Line 2058 of yacc.c  */
#line 212 "tmp.yy"

	builder *auxval;
       

/* Line 2058 of yacc.c  */
#line 226 "ee.y.tab.h"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif

extern YYSTYPE yylval;

#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */

#endif /* !YY_YY_EE_Y_TAB_H_INCLUDED  */
