/*
 * cbg csim version 1.0
 *
 * (C) 1995 djg, mjcg
 *
 * University of Cambridge
 * Computer Laboratory
 * New Museums Site
 * Pembroke Street
 * Cambridge, UK.
 *
 * Copyright for parts of this code belongs to Olivetti.
 * With acknowledgements to ach, drm, jdp, orl, iap, mdh.
 *
 */


/* varient of cbglish.h */


#define  ss_pair    100
#define  ss_symbol  101
#define  ss_number  102
#define  ss_nmacro  103
#define  ss_string  104
#define  ss_dstring 105
#define  ss_argument 106

#define  s_native0  200
#define  s_native1  201
#define  s_native2  202
#define  s_native3  203
#define  s_native4  204
#define  s_native5  205
#define  s_native6  206


typedef int key_t;

#define c_filename 1
#define c_memfile  2

#define cbge_fatal 1
#define cbge_msg 0

struct bldcons
{
  key_t key;
  struct bldcons *car, *cdr;
};





struct atom 
{
  key_t key;
  /* The car is negative enumeration type for each class of atom. */
  union car 
  { 
    void *car;
    struct atom *acar; 
    int ncar; 
    char * ccar;
  } car;        

  /* numeric val, cdr, or printname */
  union cdr 
  { 
    struct atom *acdr; 
    int ncdr; 
    char *ccdr;
  } cdr;      
  struct bldcons *carval, *props;
  struct net_d *net;
  struct guard_d *guard;
};




#define builder struct bldcons
#define GCAST struct atom *


#define errorprintf printf

#define fcar_atom(X)  (struct atom *) fcar(X)
#define m_ncar(X) (((struct atom *) X)->car.ncar)
#define m_ccdr(X) (((struct atom *) X)->cdr.ccdr)
#define m_carval(X) (((struct atom *) X)->carval)
#define m_props(X) (((struct atom *) X)->props)
#define m_ncdr(X)  (((struct atom *) X)->cdr.ncdr)
#define m_fb_carval(X)  (((struct atom *) X)->carval)
#define m_key(X)   (((struct atom *) X)->key)


extern builder *cbgeval();
extern builder *fpairp(builder *p);
extern builder *fprintl(builder *p);
extern builder *fabso(builder *p);
extern int ignorenewlines;

struct anum { key_t key; int val; struct anum *next; };

#define  atom_to_str(X) ((X) ? (((struct atom *) X)->cdr.ccdr) : "()")
#define  atom_to_int(X) (((struct anum *) X)->val)

extern char * atomtostring();

extern builder *mquote();

extern builder *flist3();
extern builder *flist2();
extern builder *str_to_atom();
extern builder *fcons();
extern builder *cbglish();



extern builder *feval();



extern builder *sif;
extern builder *sand;
extern builder *sor;
extern builder *scond;
extern int lextracef;
extern builder *print();
extern builder *firead();
extern builder *findatom();
extern builder *mknumber();
extern char insys();
extern builder *fdisplay(builder *x);
extern builder *fevlist(builder *l,  builder *env);
extern builder *sunset, *sbuiltin, *slambda, *smacro;
extern builder *shasht;


extern builder *fimplode();

#define ffirst(X)   (fcar(X))
#define fsecond(X)  (fcadr(X))
#define fthird(X)   (fcaddr(X))
#define ffourth(X)  (fcadddr(X))
#define ffifth(X)  (fcaddddr(X))
#define fsixth(X)  (fcadddddr(X))

#define e_ok    0

#define nil (0)
extern builder *slist;
extern builder *amemberp(builder *x, builder *y);
extern builder *fappend(builder *x, builder *y);
extern builder *freverse(builder *x);
extern builder *fcdr(builder *x);
extern builder *fcar(builder *x);
extern builder *fcaar(builder *x);
extern builder *fcadr(builder *x);
extern builder *fcadddddr(builder *x);
extern builder *fcaddddr(builder *x);
extern builder *fcadddr(builder *x);
extern builder *fcaddr(builder *x);
extern builder *fcdar(builder *x);
extern builder *fcddr(builder *x);
extern void define_lisp_prims();
extern builder *fcaadr(builder *x);
extern builder *fcaaadr(builder *x);
extern builder *fatomp(builder *x);
extern void init_cbglish();
extern builder *fdeletemember();
extern builder *mkatom();
extern builder *list(builder *, ...);
extern builder *list1();
extern builder *list2();
extern builder *list3();
extern builder *list4();
extern builder *list5();
extern builder *list6();
extern builder *fmemberp();
extern builder *gatom();
extern int check_an_atom(builder *a, char *fn);
extern void reenter();
extern int length();
extern int check_not_an_atom(builder *a, char *fn);
extern void cbgerror(int mode, ...);
extern builder *econs();
extern builder *fputprop();
extern builder *fnot();
extern builder *fmap();

extern builder *cmapcar();
extern builder *cmapcar_tails();
extern builder *fgetprop();
extern builder *srule, *sstile, *swhite, *sany, *scnewline, *sequals, *ssentential;
extern builder *sproductions;
extern builder *fplus();
extern builder *ftimes();
extern builder *fminus();
extern builder *flength();
extern builder *fassoc();
extern builder *foptidx();
extern builder *funique();
extern builder *funique_indexed();
extern builder *funique_indexed_string();
extern builder *fsign();
extern builder *ffmax();
extern builder *ffmin();
extern builder *evalmanifest();
extern builder *freplaca(), *freplacd();
extern int yylex();
extern char *file_name;
extern int line_number;
extern builder *cbglish_keyword(int domain, char *name, int symbol, int x, int y);
extern void cbglish_symbol(int domain, char *name, int symbol, int x, int y);
extern   void yydebug_on();
extern builder *results;
extern builder *fnumberp(builder *);
extern builder *fstringp(builder *);
extern builder *fdstringp(builder *);
extern char *smllib, *langsuffix;
extern const char *incdirpath;
extern int g_need_ee_starter;

extern char commentchar1, commentchar2;

#define FLIST1(X)  fcons(X, NULL)
#define FLIST2(X, Y)  fcons(X, fcons(Y, NULL))
#define FLIST3(X, Y, Z)  fcons(X, fcons(Y, fcons(Z, NULL)))
#define FLIST4(W, X, Y, Z)  fcons(W, fcons(X, fcons(Y, fcons(Z, NULL))))
#define FLIST5(V, W, X, Y, Z)  fcons(V, fcons(W, fcons(X, fcons(Y, fcons(Z, NULL)))))

extern builder *genconst(int, char*, builder *);

#define GENCONST(A, N, D) genconst(A, N, D)

extern builder *add_linepoint(char *contag, builder *arg);

#define TREE0(N)        GENCONST(0, N, NULL)
#define YYLEAF(N)       GENCONST(0, N, NULL)
#define TREE1(N, X)     GENCONST(1, N, FLIST1(X))
#define TREE2(N, X, Y)  GENCONST(2, N, FLIST2(X, Y))
#define TREE3(N, X, Y, Z)  GENCONST(3, N, FLIST3(X, Y, Z))
#define TREE4(N, W, X, Y, Z)  GENCONST(4, N, FLIST4(W, X, Y, Z))
#define TREE5(N, V, W, X, Y, Z)  GENCONST(4, N, FLIST5(V, W, X, Y, Z))
#define LISTEND(X)      NULL
#define LISTLAST(X)     fcons(X, NULL)
#define LISTCONS(X, Y)  fcons(X, Y)

extern void cleanexit(int code);

/*  end of cbglish.h */

