// 
//  
// Bigtop-e. A hardware specification language frontend inspired by Specman-e from Verisity Inc.  
// (C) 2003-16 DJ Greaves, University of Cambridge Computer Laboratory. All rights reserved.
// No avoidance of liability for infringements, loss or failures is warranted in this code.
// Not for commercial use without license.
//

open Microsoft.FSharp.Collections
open System.Collections.Generic

open yout
open eegram
open hprls_hdr
open moscow
open meox

type ee_spare_t = int // for now

type synside_t =
   | SS_neither
   | SS_initiator
   | SS_target


type etype_t =
    | ETL_net of netst_t * int * int * ee_spare_t option
    | ETL_enum of string list
    | ETL_struct of string list * ee_formals_t option * (string * eunion_t) list
    | ETL_unit of string list * string list * ee_formals_t option * (eunion_t) list    
    | ETL_event
    | ETL_void

    
and eenv_item_t =
    {
        it:   (string * efabric_t list) list;
        iid:  string list
        eraw: (string * eunion_t) list
        ty:   etype_t
    }

and efabric_t =
    | EF_dotaccess of  efabric_t * string
    | EF_built of string list
    | EF_unbuilt of etype_t
    | EF_x of hexp_t
    | EF_it of eenv_item_t

and eunion_t =
    | E_raw    of ee_decl_t   // raw ast
    | E_cooked of eenv_item_t

let g_ee_canned_bool = ETL_net(Unsigned, 1, -1, None)

let g_null_eitem = { it=[]; iid=[]; ty=ETL_void; eraw=[];  }

let g_built_dir = new OptionStore<string list, etype_t>("built_dir")

type eenv_t =
    {
        pass:int
        env: (string * eunion_t) list
        iprefix: string list // instantiating prefix
        dprefix: string list // defining context
        synside: synside_t
    }

let gen_rawdir cc arg =
    match arg with
        | EE_typedef(name, ty) -> (name, E_raw arg)::cc
        | EE_struct_def(name, parent, formals_opt, items) -> (name, E_raw arg)::cc
        | EE_event_decl(name, formals_opt, Some event) ->                 (name, E_raw arg)::cc
        | EE_unit_def(name, formals_opt, items) -> (name, E_raw arg)::cc
        //| EE_extend1(ss, decls) -> ss = "sys"
        | _ -> cc
        
let rec norm1_ty_and_instance ww eenv = function
    | EE_usertype ss ->
        match op_assoc ss eenv.env with
            | None -> sf (sprintf "user type name '%s' not declared or in scope" ss)
            | Some(E_cooked v) -> (v.ty, None)
            | Some(E_raw srcast) ->
                let  ww = WF 3 "norm1" ww ("norm1 recurse raw " + ss)
                let (name_, nb) = norm1_def ww eenv srcast
                let ty =
                    match nb with
                        | E_cooked cooked ->  cooked.ty
                        | _ ->  sf (sprintf "norm raw v.ty %s cooked =%A" ss nb)            
                (ty, Some nb)

    | other -> (norm1_ty ww eenv other, None)
        
and norm1_ty ww eenv = function
    | EE_usertype ss ->
        let (ty, nbo_) = norm1_ty_and_instance ww eenv (EE_usertype ss)
        ty

    | EE_typequal(ty1, ty2) ->
        let ty1' = norm1_ty ww eenv ty1
        let ty2' = norm1_ty ww eenv ty2  
        ty2' // for now

    | EE_enum items ->
        let deid = function
            | EE_id ss -> ss
            | other ->sf (sprintf "Bigtop-e : unsupported enumeration entry %A" other)

        let items' = map deid items
        ETL_enum items'
        
    | EE_uint    -> ETL_net(Unsigned, 32, -1, None)

    | EE_bool    -> g_ee_canned_bool
    | EE_bits n  ->
        let n' = norm1_exp_manifest32 ww eenv n 
        ETL_net(Unsigned, n',  -1, None)
    | EE_bytes n  ->
        let n' = norm1_exp_manifest32 ww eenv n 
        ETL_net(Unsigned, 8 * n',  -1, None)

    | other -> sf (sprintf "Bigtop-e : unsupported input type %A" other)

and norm1_exp_manifest32 ww eenv arg =
    let arg'= norm1_exp (WN "manifest" ww) eenv arg
    match arg' with
        | EF_x x -> xi_manifest "Bigtop-e: norm1_exp_panifest32" x
        
        | other -> sf(sprintf "looking for manifest integer in unexpectedly odd item %A" other)


and norm1_exp ww eenv = function
    | EE_apply (f, args) ->
         let f' = norm1_exp ww eenv f
         let args' = map (norm1_exp ww eenv) args
         muddy "EE_apply  (EE_diadic (EO_dotaccess,EE_id port0,EE_id er_da),[EE_id trans_addr])"

    | EE_id ss ->
        match op_assoc ss eenv.env with
            | None -> sf (sprintf "identifier '%s' not declared or in scope" ss)
            | Some(E_raw raw)  -> muddy "raooa"
            | Some(E_cooked v) ->
                match v.ty with
                    | ETL_struct _ ->
                        EF_unbuilt(v.ty)

                    | ETL_unit _ -> EF_built v.iid
                    //EF_unbuilt(v.ty) // TODO units are built?

                    | other -> sf (sprintf "ss=%s lookup id finds cooked %A" ss v)


    | EE_diadic(EO_dotaccess, e1, EE_id tag) ->                
        match norm1_exp ww eenv e1 with
            | EF_unbuilt(ETL_struct(sname, forms, items)) ->
                match op_assoc tag items with
                    | None ->sf (sprintf "failed when looking for field %A in struct %s" tag (htos sname))
                    | Some _ -> EF_dotaccess(EF_unbuilt(ETL_struct(sname, forms, items)), tag)

            | EF_built idl ->

                match g_built_dir.lookup idl with
                    | None ->sf (sprintf "looking for field %A that is not built afterall " tag)

                    | Some (ETL_unit(kind, iname, _, pairs)) ->
                        let rec scan = function
                            | [] -> sf (sprintf "There is no field '%s' in unit type %s" tag (htos kind))
                            | (E_cooked nb)::tt ->
                                let _ = vprintln 0 (sprintf "skupped %s" (htos nb.iid))
                                if hd nb.iid = tag then EF_it nb
                                else scan tt
                            | h::tt->
                                scan tt
                                //muddy (sprintf "other form for field '%s' in built item %A" tag tt)
                                
                        scan pairs
                    | Some ob -> muddy (sprintf "looking for field '%s' in built item %A" tag ob)
            | EF_it nb ->
                match op_assoc tag nb.it with
                    | Some _ -> muddy ("foix ya 1" + tag)
                    | None _ ->
                        match op_assoc tag nb.eraw with
                            | None ->  sf (sprintf "Failed looking for field '%s' in %s" tag (htos nb.iid))
                            | Some(E_cooked nb) ->
                                EF_it nb//muddy ("foix ya 2" + tag)
                            | Some(E_raw ast) ->
                                let ww = WF 3 "deferred norm lazy" ww ("start on tag " + tag)
                                let (name_, nb) = norm1_def ww { eenv with pass=2; } ast
                                match nb with
                                    | E_cooked nb -> EF_it(nb)
                                    | other -> sf (sprintf "POOR FORM for field %A - still raw after norming %A" tag other)
            | other -> sf (sprintf "looking for field %A in unexpectedly odd item %A" tag other)
        
    | EE_diadic(diop, e1, e2) ->
        let e1' = norm1_exp ww eenv e1
        let e2' = norm1_exp ww eenv e2        
        //let ty2' = norm1_event ww eenv e2  
        e1' //for now
    
    | EE_num n -> EF_x(xi_num n)
    | other -> sf (sprintf "Bigtop-e : unsupported input expression construct %A" other)

and norm1_event ww eenv = function
    | EE_t_e fluent ->
        match (norm1_exp (WN "EE_t_e" ww) eenv fluent) with
            | EF_x x   -> xi_orred x
            | other    ->sf (sprintf "EV_t_e:looking for fluent in unexpectedly odd item %A" other)

    | EE_ev_ex fluent ->
        match (norm1_exp (WN "EE_ev_ex" ww) eenv fluent) with
            | EF_x x   -> xi_orred x
            | other    ->sf (sprintf "EV_ev_ex: looking for fluent in unexpectedly odd item %A" other)


    | EE_t_true(fluent) -> norm1_event ww eenv fluent    // no semantic? This is where orred should be!
        
    | EE_ev_sampled(e1, e2) ->
        let e1' = norm1_event ww eenv e1
        //let ty2' = norm1_event ww eenv e2  
        e1' //for now

    | EE_t_and(e1, e2) ->
        let e1' = norm1_event ww eenv e1
        let ty2' = norm1_event ww eenv e2  
        e1' //for now

    | other -> sf (sprintf "Bigtop-e : unsupported input event construct %A" other)

and norm1_def ww eenv deff =

    match deff with
    | EE_typedef(name, ty) ->
        let eraw = List.fold gen_rawdir [] [deff]
        let fullname = name::eenv.dprefix        
        let ww = WF 3 "norm1" ww ("norm1 type " + htos fullname)
        let ty' = norm1_ty ww eenv ty
        let nb = { g_null_eitem with iid=fullname; eraw=eraw; ty=ty'; }
        (name, E_cooked nb)


    | EE_struct_def(name, parent_, None, items) ->
        let fullname = name::eenv.dprefix        
        let ww = WF 3 "norm1" ww ("norm1 struct " + htos fullname)
        let eraw = List.fold gen_rawdir [] [deff]
        let dof = function
            | EE_fp(ss,ty) ->
                let ty' = norm1_ty ww eenv ty
                (ss, ty')
        let _ = if not_nonep parent_ then sf("extend not supported in def " + htos fullname)
        let eenv' =
            { eenv with dprefix= name::eenv.dprefix; } // dprefix extended by declared name
            
        let pairs = map (norm1_def ww eenv') items
        let ty' = ETL_struct(fullname, None, pairs)
        let itt =
            let destruct = function
                | (tag_, E_cooked ei) -> (ei.it)
                | other -> sf (sprintf "Bigtop-e other destruct %A" other)
            map destruct pairs
        let nb = { g_null_eitem with iid=fullname; eraw=eraw; ty=ty'; it= list_flatten itt; }
        (name, E_cooked nb)

        // Leave parameterised events until pass 2.
    | EE_event_decl(name, Some formals, Some event) when eenv.pass < 2 -> (name, E_raw deff)

    | EE_event_decl(name, formalso, Some event) ->
        let eraw = List.fold gen_rawdir [] [deff]
        let fullname = name::eenv.dprefix        
        let ww = WF 3 "norm1" ww ("norm1 event decl " + htos fullname)
        let dof = function
            | EE_fp(ss,ty) ->
                let ty' = norm1_ty ww eenv ty
                (ss, ty')

//        let eenv' =
//            match formals_opt with
//                | None -> eenv
//                | Some formals ->
//                    let fbind cc (bv, ty) =             //expand with formal bindings
//                        (bv, ty) :: cc
//                    {eenv with env=List.fold fbind eenv.env formals; }
            
        let it__  = norm1_event ww eenv event
        let ty' = ETL_event
        let nb = { g_null_eitem with iid=fullname; eraw=eraw; ty=ty'; }
        (name, E_cooked nb)

    | EE_unit_def(name, formals_opt, items) ->
        let kind = name::eenv.dprefix
        let iname = eenv.iprefix                
        let ww = WF 3 "norm1" ww (sprintf "norm1 unit kind=%s iname=%s" (htos kind) (htos iname))
        let dof = function
            | EE_fp(ss,ty) ->
                let ty' = norm1_ty ww eenv ty
                (ss, ty')
        let eraw = List.fold gen_rawdir [] items
        let eenv' =
            //expand with formal bindings
            // expand with new prefix is parent has not IS_INSTANCE added an extension
            { eenv with dprefix=kind; }
            
        let (pairs, _) =
            let uedef (pairs, ee) (item) =
                let (name, nb) = norm1_def ww ee item
                let ee = { ee with env=(name, nb)::ee.env; }
                (nb::pairs, ee)
            List.fold uedef ([], eenv') items
        let pairs = rev pairs
        let ty' = ETL_unit(kind, iname, formals_opt, pairs) // more than a type ! contains the created gubbins
        let itt =
            let destruct2 arg cc =
                match arg with
                | (E_cooked ei) -> (ei.it)::cc
                | other ->
                    let _ = vprintln 10 (sprintf "Bigtop-e other destruct2 %A" other)
                    cc
            List.foldBack destruct2 pairs []
        
        let nb = { g_null_eitem with iid=iname; eraw=eraw; ty=ty'; it=list_flatten itt; }
        (name, E_cooked nb)

    | EE_g_decl(field_flags, name, ty, inst_flags, synctrl) ->
        let iname = name::eenv.iprefix
        let isi =  memberp EF_IS_INSTANCE inst_flags
        let ww = WF 3 "norm1" ww (sprintf "norm1 isi=%A g_decl iname=%s " isi (htos iname))
        let rezzing = true // ... for now
        // For a unit/instance we are instantiating it and we do much the same for structs.
        // We pass in its iname 
        let (sctrl, synside) = norm1_synctrl ww eenv synctrl
        let eenv' = { eenv with iprefix=iname; synside=synside; } // pass in iname already bound
        let (ty', nbo) = norm1_ty_and_instance ww eenv' ty
        let ids = htos iname
        let nb = match nbo with
                    | Some(E_cooked nb) -> nb
                    | None ->
                        let eraw = List.fold gen_rawdir [] [deff]
                        { g_null_eitem with iid=iname; eraw=eraw; ty=ty'; }

        let nb =
            match ty' with // trawl scalar net decls from leaves
                | ETL_struct _ -> nb
                
                | ETL_unit(kind, iname, formals_opt, pairs) ->
                    if rezzing then
                        let _ = vprintln 3 (sprintf "Adding %s to built collection" ids)
                        let _ = g_built_dir.add iname ty'
                        // { nb with it=(name, ...::nb.it; } // need a residual fantom?
                        nb
                    else nb
                | ETL_enum items ->
                    let width = encoding_width (xi_num (length items))
                    let _ = vprintln 3 (sprintf "declared static variable/net %s dir=%A (enum) width=%i" ids sctrl width)
                    let net = ionet_w(ids, width, sctrl, [])
                    { nb with it=(name, [EF_x net])::nb.it; }
                    
                | ETL_net(signed, width, _, _) -> 
                    let _ = vprintln 3 (sprintf "declared static variable/net %s dir=%A width=%i" ids sctrl width)
                    let net = vectornet_w(ids, width) //,     Ndi sctrl, gen_Symbolic_bitvec (width))
                    { nb with it=(name, [EF_x net])::nb.it; }

                | other -> sf(sprintf "looking for net declaration '%s' in unexpectedly odd item %A" ids other)


        (name, E_cooked nb)

    | EE_ea(keyword, evento, expso) ->
        let ww = WF 3 "norm1" ww (sprintf "norm1 ea=%A " keyword)
        muddy (htos eenv.dprefix)
        
    | other -> sf (sprintf "Bigtop-e : unsupported input declaration construct %A" other)



and norm1_synctrl ww eenv items =
    let side = ref SS_neither
    let pol = ref LOCAL
    //| SS_initiator

    let hex = function
        | SD_id "always" -> side := SS_neither
        | SD_id "target" -> side := SS_target
        | SD_id "initiator" -> side := SS_initiator        
        | SD_in  -> pol := INPUT
        | SD_id "out"  -> pol := OUTPUT        
        | other -> sf (sprintf "other form in synctrl %A" other)
    let _ = app hex items
    let polf =
        if !side = SS_neither then !pol
        elif !side = eenv.synside then !pol // Keep declared port direction (in v out) unless flipped.
        elif !pol = INPUT then OUTPUT
        elif !pol = OUTPUT then INPUT
        else !pol
    // We return the direction for a net declaration and for a component instantiation.

    (polf, if !side <> SS_neither then !side else eenv.synside)



and norm1 ww eenv item = // norm and add to env
    let (name, nb) = norm1_def ww eenv item
    let _ =
        match nb with
            | E_cooked nb -> vprintln 3 (sprintf "EE Bigtop - defined %s: bound as %s" (htos nb.iid) name)
            | _ -> ()
    { eenv with env=(name, nb)::eenv.env; }

let eenv0 =
    {   pass=     1
        synside=  SS_neither
        iprefix=  [ "TOP" ]
        dprefix=  []
        env=      []
    }
    
let start() =
    let sfn = "spool"
    let ww = WW_end
    let _ = vprintln 0 ("eerun running!")
    let (src_fn, ast) = spool.eegram_ast
    let roots =
        let rootfn = function
            | EE_extend1(ss, decls) -> ss = "sys"
            | _ -> false
        List.filter rootfn ast

    let rawdir = List.fold gen_rawdir [] ast
    let eenv0 = { eenv0 with env=rawdir @ eenv0.env; }  
    let _ = vprintln 1 (sprintf "Bigtop-e: Found %i declarations  and %i roots in src file %s" (length rawdir) (length roots) src_fn)

    let roots' = map (fun (EE_extend1(ss, decls)) -> decls) roots
    let _ =
        let ff rootsection =
            norm1 ww eenv0 (EE_unit_def(funique "sys-extend", None,  rootsection)) // 
        map ff roots'

    //let _ = vprintln 0 (sprintf "spool.eegram_data=%A" ast)
    let _ = vprintln 1 ("Bigtop-e: eerun successful completion")    
    ()



let _ = start()

// eof


    
