/*
 *
 * Bigtop.  
 * (C) 2003-16 DJ Greaves. All rights reserved.
 *
 */


#include <stdio.h>
#include "cbglish.h"
#include <string.h>

void reenter()
{
  extern void exit(int);
  exit(0);
}

static int tokens = 0;
int lextracef=0;
const char *incdirpath = NULL;

// Render as ML/FSharp source code
void mlprint1(FILE *f, builder *v, int depth)
{
  int i;
  if (tokens++>12)
    {
      tokens = 0;
      fprintf(f, "\n");
      for (i=0;i<depth;i++) fprintf(f, "  ");
    }
  if (!v) fprintf(f, "[]");
  else if (fpairp(v) && fcar(v) == smacro) 
    {
      int firstf = 1;
      v = fcdr(v);
      fprintf(f, "%s", atom_to_str(fcar(v)));
      v = fcadr(v);
      if (v)
	{
	  fprintf(f, "(");
	  while(v)
	    {
	      if (firstf) firstf = 0; else fprintf(f, ", ");
	      mlprint1(f, fcar(v), depth+1);
	      v = fcdr(v);
	    }
	  fprintf(f, ")");
	}
    }
  else if (fpairp(v))
    {
      int firstf = 1;
      fprintf(f, "[");
      while(v)
	{
	  if (firstf) firstf = 0; else fprintf(f, "; "); // For FSharp, the list delimiter is a semicolon.
	  mlprint1(f, fcar(v), depth+1);
	  v = fcdr(v);
	}
      fprintf(f, "]");
    }
  else if (fnumberp(v)) 
    {
      fprintf(f, "%i", atom_to_int(v));
    }

  else if (fatomp(v)) 
    {
      builder *p = fgetprop(smacro, v);
      if (!p) fprintf(f, "\"%s\"", atom_to_str(v));
      else fprintf(f, "%s", atom_to_str(v));
    }
  else 
    {
      fprintf(f, "-?bad-");
      printf("Bad ml item\n"); fdisplay(v);
    }
}



void mlprint(const char*src_fn, FILE *f, builder *v)
{
  fprintf(f, "let %s_ast =(\"%s\", \n", smllib, src_fn);
  mlprint1(f, v, 0);
  fprintf(f, ");\n\n\n");

}

void mlprintlist(const char*src_fn, FILE *f, builder *v)
{
  while (v)
    {
      mlprint(src_fn, f, fcar(v));
      v = fcdr(v);
    }
}


static FILE *cos = NULL;
const char *opfn = "spool.fs";


int main(int argc, const char *argv[])
{
  const char *progname = argv[0];
  extern void define_keywords();
  init_cbglish();
  cos = stdout;
  define_keywords();

#if YYDEBUG
  extern int yydebug;
  yydebug = 0;
#endif


  while (argc >= 2 && ((argv[1][0]=='-') || (argv[1][0]=='+')))
    {
      if (!strcmp(argv[1], "-lextrace"))
      {
	lextracef = 1;
	argv +=1; argc-=1;
	continue;
      }

      if (!strcmp(argv[1], "-yydebug"))
      {
#if YYDEBUG
	extern int yydebug;
	yydebug = 1;
#endif
	argv +=1; argc-=1;
	continue;
      }

      if (!strcmp(argv[1], "-o"))
      {
	opfn = argv[2];
	argv +=2; argc-=2;
	continue;
      }
      
      if (!strncmp(argv[1], "+incdir+", 8))
	{
	  incdirpath = argv[1]+8;
	  argv +=1; argc-=1;
	  continue;
      }
      break;
    }

  if (argc < 2) 
    {
      printf("Useage:  %s [ -lextrace -yydebug ] [ +incdir+PATH ] srcfiles ...", progname);
      return -1;
    }

  cos = fopen(opfn, "w");
  fprintf(cos, "module spool\n");
  fprintf(cos, "#lite \"off\"\n");
  fprintf(cos, "open %s;\n\n", smllib); 

  while (argc >= 2) 
    {
      int vd = 1;
      const char *src_fn = argv[1];
      builder * r =  cbglish(c_filename, 0, src_fn, NULL);
      if (vd)  fdisplay(r);
      fprintf(cos, "(* Parse tree from %s *)\n\n", src_fn); 
      mlprintlist(src_fn, cos, r);
      fprintf(cos, "\n\n\n");
      argc --; argv ++;
    }

  fprintf(cos, "\n\n(* eof *)\n");
  fclose(cos);
  return 0;
}


builder *genconst(int A, char *N, builder *D)
{
  builder *r = FLIST3(smacro, str_to_atom(N), D);
  if (0)
    {
      mlprint1(stdout, r, 0);
      printf("\n");
      fflush(stdout);
    }
  return r;
}


extern void cleanexit(int code)
{
  extern void exit(int);
  if (cos && cos != stdout) fclose(cos);
  if (opfn && code) remove(opfn);
  exit(code);
}	
     /* eof */
