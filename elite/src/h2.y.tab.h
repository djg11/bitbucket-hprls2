/* A Bison parser, made by GNU Bison 2.7.  */

/* Bison interface for Yacc-like parsers in C
   
      Copyright (C) 1984, 1989-1990, 2000-2012 Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_H2_Y_TAB_H_INCLUDED
# define YY_YY_H2_Y_TAB_H_INCLUDED
/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     sd_string = 258,
     sd_id = 259,
     sd_number = 260,
     sd_char = 261,
     sd_synthembed = 262,
     sd_dstring = 263,
     s_statechart = 264,
     s_simplebev = 265,
     s_statemachine = 266,
     s_in = 267,
     s_wait = 268,
     s_while = 269,
     s_of = 270,
     s_goto = 271,
     s_when = 272,
     s_neutral = 273,
     s_forward = 274,
     s_reverse = 275,
     s_protocol = 276,
     s_interface = 277,
     s_structure = 278,
     s_section = 279,
     s_unit = 280,
     s_connect = 281,
     s_domain = 282,
     s_if = 283,
     s_node = 284,
     s_out = 285,
     s_inout = 286,
     s_statevar = 287,
     s_bool = 288,
     s_enum = 289,
     s_always = 290,
     s_initial = 291,
     s_never = 292,
     ss_quote = 293,
     ss_lsect = 294,
     ss_rsect = 295,
     ss_lbra = 296,
     ss_rbra = 297,
     ss_lpar = 298,
     ss_rpar = 299,
     ss_equals = 300,
     ss_comma = 301,
     ss_query = 302,
     ss_dot = 303,
     ss_ampersand = 304,
     ss_stile = 305,
     ss_snail = 306,
     ss_percent = 307,
     ss_caret = 308,
     ss_star = 309,
     ss_slash = 310,
     ss_dltd = 311,
     ss_dgtd = 312,
     ss_plus = 313,
     ss_minus = 314,
     ss_hash = 315,
     ss_colon = 316,
     ss_tilda = 317,
     ss_pling = 318,
     ss_semicolon = 319,
     ss_dled = 320,
     ss_dged = 321,
     ss_deqd = 322,
     ss_colonequals = 323,
     ss_dned = 324,
     ss_rarrow1 = 325,
     ss_rarrow2 = 326,
     ss_plusequals = 327,
     ss_minusequals = 328,
     ss_lshift = 329,
     ss_rshift = 330,
     ss_disj = 331,
     ss_conj = 332,
     ss_logor = 333,
     ss_logand = 334
   };
#endif


#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{
/* Line 2058 of yacc.c  */
#line 116 "tmp.yy"

	builder *auxval;
       

/* Line 2058 of yacc.c  */
#line 141 "h2.y.tab.h"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif

extern YYSTYPE yylval;

#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */

#endif /* !YY_YY_H2_Y_TAB_H_INCLUDED  */
