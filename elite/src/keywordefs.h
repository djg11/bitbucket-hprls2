# 37 "/tmp/aa.c"
  %token s_statechart;
  %token s_in;
  %token s_wait;
  %token s_while;
  %token s_of;
  %token s_goto;
  %token s_when;
  %token s_neutral;
  %token s_forward;
  %token s_reverse;
  %token s_protocol;
  %token s_domain;



  %token ss_lsect;
  %token ss_rsect;
  %token ss_lpar;
  %token ss_rpar;
  %token ss_equals;
  %token ss_comma;
  %token ss_dot;
  %token ss_dled;
  %token ss_dged;
  %token ss_deqd;
  %token ss_dltd;
  %token ss_dgtd;
  %token ss_dned;
  %token ss_arrow1;
  %token ss_percent;
  %token ss_plus;
  %token ss_minus;
  %token ss_plusequals;
  %token ss_minusequals;
  %token ss_lshift;
  %token ss_rshift;
  %token ss_star;
  %token ss_colon;
