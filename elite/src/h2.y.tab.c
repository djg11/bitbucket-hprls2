/* A Bison parser, made by GNU Bison 2.7.  */

/* Bison implementation for Yacc-like parsers in C
   
      Copyright (C) 1984, 1989-1990, 2000-2012 Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.7"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
/* Line 371 of yacc.c  */
#line 106 "tmp.yy"

#include "cbglish.h"

#define OUI_LINEPOINT(X) add_linepoint("Oui_linepoint", X)

int yyerror (const char *s);
 


/* Line 371 of yacc.c  */
#line 78 "h2.y.tab.c"

# ifndef YY_NULL
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULL nullptr
#  else
#   define YY_NULL 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "h2.y.tab.h".  */
#ifndef YY_YY_H2_Y_TAB_H_INCLUDED
# define YY_YY_H2_Y_TAB_H_INCLUDED
/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     sd_string = 258,
     sd_id = 259,
     sd_number = 260,
     sd_char = 261,
     sd_synthembed = 262,
     sd_dstring = 263,
     s_statechart = 264,
     s_simplebev = 265,
     s_statemachine = 266,
     s_in = 267,
     s_wait = 268,
     s_while = 269,
     s_of = 270,
     s_goto = 271,
     s_when = 272,
     s_neutral = 273,
     s_forward = 274,
     s_reverse = 275,
     s_protocol = 276,
     s_interface = 277,
     s_structure = 278,
     s_section = 279,
     s_unit = 280,
     s_connect = 281,
     s_domain = 282,
     s_if = 283,
     s_node = 284,
     s_out = 285,
     s_inout = 286,
     s_statevar = 287,
     s_bool = 288,
     s_enum = 289,
     s_always = 290,
     s_initial = 291,
     s_never = 292,
     ss_quote = 293,
     ss_lsect = 294,
     ss_rsect = 295,
     ss_lbra = 296,
     ss_rbra = 297,
     ss_lpar = 298,
     ss_rpar = 299,
     ss_equals = 300,
     ss_comma = 301,
     ss_query = 302,
     ss_dot = 303,
     ss_ampersand = 304,
     ss_stile = 305,
     ss_snail = 306,
     ss_percent = 307,
     ss_caret = 308,
     ss_star = 309,
     ss_slash = 310,
     ss_dltd = 311,
     ss_dgtd = 312,
     ss_plus = 313,
     ss_minus = 314,
     ss_hash = 315,
     ss_colon = 316,
     ss_tilda = 317,
     ss_pling = 318,
     ss_semicolon = 319,
     ss_dled = 320,
     ss_dged = 321,
     ss_deqd = 322,
     ss_colonequals = 323,
     ss_dned = 324,
     ss_rarrow1 = 325,
     ss_rarrow2 = 326,
     ss_plusequals = 327,
     ss_minusequals = 328,
     ss_lshift = 329,
     ss_rshift = 330,
     ss_disj = 331,
     ss_conj = 332,
     ss_logor = 333,
     ss_logand = 334
   };
#endif


#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{
/* Line 387 of yacc.c  */
#line 116 "tmp.yy"

	builder *auxval;
       

/* Line 387 of yacc.c  */
#line 205 "h2.y.tab.c"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif

extern YYSTYPE yylval;

#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */

#endif /* !YY_YY_H2_Y_TAB_H_INCLUDED  */

/* Copy the second part of user declarations.  */

/* Line 390 of yacc.c  */
#line 233 "h2.y.tab.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(N) (N)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int yyi)
#else
static int
YYID (yyi)
    int yyi;
#endif
{
  return yyi;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)				\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack_alloc, Stack, yysize);			\
	Stack = &yyptr->Stack_alloc;					\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (YYID (0))
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  13
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   200

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  80
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  35
/* YYNRULES -- Number of rules.  */
#define YYNRULES  103
/* YYNRULES -- Number of states.  */
#define YYNSTATES  203

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   334

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     5,     6,     9,    11,    13,    15,    18,
      20,    25,    31,    33,    35,    37,    39,    41,    43,    45,
      47,    49,    52,    54,    57,    59,    65,    73,    79,    85,
      89,    93,    97,   101,   105,   110,   118,   121,   124,   127,
     129,   136,   137,   140,   141,   143,   147,   148,   150,   154,
     155,   157,   161,   162,   165,   167,   171,   172,   175,   178,
     183,   186,   190,   196,   203,   205,   209,   211,   217,   219,
     223,   225,   227,   231,   235,   237,   241,   245,   249,   251,
     255,   259,   263,   267,   271,   275,   277,   281,   283,   287,
     289,   291,   293,   295,   297,   301,   306,   313,   315,   319,
     322,   324,   326,   329
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int8 yyrhs[] =
{
      81,     0,    -1,    82,    -1,    -1,    91,    82,    -1,    22,
      -1,    21,    -1,    24,    -1,    25,     4,    -1,     4,    -1,
       4,    43,    95,    44,    -1,    83,     4,    39,    92,    40,
      -1,    30,    -1,    12,    -1,    31,    -1,    63,    -1,     4,
      -1,    19,    -1,    20,    -1,    87,    -1,    89,    -1,    83,
       4,    -1,    33,    -1,    63,     4,    -1,     4,    -1,    41,
     100,    61,   100,    42,    -1,    11,    43,    93,    44,    39,
      98,    40,    -1,    29,    96,    61,    97,    64,    -1,    83,
       4,    61,    94,    64,    -1,    83,     4,    64,    -1,    26,
      95,    64,    -1,    35,   100,    64,    -1,    36,   100,    64,
      -1,    37,   100,    64,    -1,   100,    68,   100,    64,    -1,
      32,     4,    34,    39,    93,    40,    64,    -1,    18,    61,
      -1,    19,    61,    -1,    20,    61,    -1,    85,    -1,    10,
      43,    44,    39,    98,    40,    -1,    -1,    90,    92,    -1,
      -1,     4,    -1,     4,    46,    93,    -1,    -1,    84,    -1,
      84,    46,    94,    -1,    -1,   100,    -1,   100,    46,    95,
      -1,    -1,    86,    96,    -1,    88,    -1,    88,    46,    97,
      -1,    -1,    99,    98,    -1,     4,    61,    -1,   100,    63,
     100,    64,    -1,   100,    64,    -1,    16,     4,    64,    -1,
      13,    43,   100,    44,    64,    -1,    28,    43,   100,    44,
      99,    64,    -1,   101,    -1,   101,    45,   101,    -1,   102,
      -1,   102,    47,   102,    61,   101,    -1,   103,    -1,   103,
      71,   103,    -1,   104,    -1,   105,    -1,   104,    78,   105,
      -1,   104,    76,   105,    -1,   106,    -1,   105,    79,   106,
      -1,   105,    53,   106,    -1,   105,    77,   106,    -1,   107,
      -1,   107,    67,   107,    -1,   107,    69,   107,    -1,   107,
      56,   107,    -1,   107,    65,   107,    -1,   107,    57,   107,
      -1,   107,    66,   107,    -1,   108,    -1,   108,    50,   107,
      -1,   109,    -1,   108,    49,   109,    -1,   110,    -1,   111,
      -1,   112,    -1,   114,    -1,     4,    -1,     4,    48,   113,
      -1,   113,    43,    95,    44,    -1,   113,    41,   100,    61,
     100,    42,    -1,   113,    -1,    43,   100,    44,    -1,    47,
     114,    -1,     5,    -1,     3,    -1,    62,   114,    -1,    63,
     114,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   127,   127,   136,   139,   148,   149,   150,   151,   158,
     162,   171,   178,   182,   186,   190,   194,   198,   202,   206,
     210,   212,   219,   229,   233,   246,   253,   258,   262,   266,
     270,   274,   278,   283,   287,   291,   296,   300,   304,   315,
     319,   329,   332,   340,   343,   345,   351,   354,   356,   362,
     365,   369,   377,   380,   387,   391,   399,   402,   409,   415,
     420,   425,   430,   434,   442,   443,   452,   453,   457,   459,
     466,   471,   472,   475,   482,   483,   486,   489,   495,   497,
     500,   503,   506,   509,   512,   520,   521,   529,   530,   537,
     542,   546,   550,   555,   559,   566,   571,   577,   579,   581,
     586,   591,   596,   601
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "sd_string", "sd_id", "sd_number",
  "sd_char", "sd_synthembed", "sd_dstring", "s_statechart", "s_simplebev",
  "s_statemachine", "s_in", "s_wait", "s_while", "s_of", "s_goto",
  "s_when", "s_neutral", "s_forward", "s_reverse", "s_protocol",
  "s_interface", "s_structure", "s_section", "s_unit", "s_connect",
  "s_domain", "s_if", "s_node", "s_out", "s_inout", "s_statevar", "s_bool",
  "s_enum", "s_always", "s_initial", "s_never", "ss_quote", "ss_lsect",
  "ss_rsect", "ss_lbra", "ss_rbra", "ss_lpar", "ss_rpar", "ss_equals",
  "ss_comma", "ss_query", "ss_dot", "ss_ampersand", "ss_stile", "ss_snail",
  "ss_percent", "ss_caret", "ss_star", "ss_slash", "ss_dltd", "ss_dgtd",
  "ss_plus", "ss_minus", "ss_hash", "ss_colon", "ss_tilda", "ss_pling",
  "ss_semicolon", "ss_dled", "ss_dged", "ss_deqd", "ss_colonequals",
  "ss_dned", "ss_rarrow1", "ss_rarrow2", "ss_plusequals", "ss_minusequals",
  "ss_lshift", "ss_rshift", "ss_disj", "ss_conj", "ss_logor", "ss_logand",
  "$accept", "prog", "orange_decls", "orange_unit_mode",
  "orange_unit_instance", "orange_unit", "orange_node_qualifier",
  "built_in_type", "orange_instance_qualified", "onq_square_range",
  "orange_unit_item", "module", "orange_unit_item_list",
  "sd_id_comma_list", "orange_unit_instance_comma_list", "exp_comma_list",
  "orange_node_qualifier_list", "orange_instance_qualified_list",
  "bev_stat_list", "bev_stat", "exp", "exp1", "exp2", "exp3", "exp4",
  "exp5", "exp6", "exp7", "exp8", "exp9", "exp10", "exp11", "exp12",
  "exp13a", "exp13", YY_NULL
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    80,    81,    82,    82,    83,    83,    83,    83,    84,
      84,    85,    86,    86,    86,    86,    86,    86,    86,    86,
      86,    86,    87,    88,    88,    89,    90,    90,    90,    90,
      90,    90,    90,    90,    90,    90,    90,    90,    90,    91,
      91,    92,    92,    93,    93,    93,    94,    94,    94,    95,
      95,    95,    96,    96,    97,    97,    98,    98,    99,    99,
      99,    99,    99,    99,   100,   100,   101,   101,   102,   102,
     103,   104,   104,   104,   105,   105,   105,   105,   106,   106,
     106,   106,   106,   106,   106,   107,   107,   108,   108,   109,
     110,   111,   112,   113,   113,   114,   114,   114,   114,   114,
     114,   114,   114,   114
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     0,     2,     1,     1,     1,     2,     1,
       4,     5,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     2,     1,     2,     1,     5,     7,     5,     5,     3,
       3,     3,     3,     3,     4,     7,     2,     2,     2,     1,
       6,     0,     2,     0,     1,     3,     0,     1,     3,     0,
       1,     3,     0,     2,     1,     3,     0,     2,     2,     4,
       2,     3,     5,     6,     1,     3,     1,     5,     1,     3,
       1,     1,     3,     3,     1,     3,     3,     3,     1,     3,
       3,     3,     3,     3,     3,     1,     3,     1,     3,     1,
       1,     1,     1,     1,     3,     4,     6,     1,     3,     2,
       1,     1,     2,     2
};

/* YYDEFACT[STATE-NAME] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       3,     0,     6,     5,     7,     0,     0,     2,     0,    39,
       3,     0,     8,     1,     0,     4,     0,    41,    56,   101,
      93,   100,     0,     0,     0,     0,    49,    52,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    41,     0,     0,
      64,    66,    68,    70,    71,    74,    78,    85,    87,    89,
      90,    91,    97,    92,    93,     0,     0,     0,     0,    56,
       0,     0,    43,    36,    37,    38,     0,    50,    16,    13,
      17,    18,    12,    14,    22,     0,    15,     0,    52,    19,
      20,     0,     0,     0,     0,     0,     0,    99,   102,   103,
       0,    42,    11,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    49,    58,     0,     0,     0,    40,    57,     0,    60,
      94,    44,     0,    30,    49,     0,    21,    53,     0,     0,
      31,    32,    33,    98,    46,    29,     0,    65,     0,    69,
      73,    72,    76,    77,    75,    81,    83,    82,    84,    79,
      80,    88,    86,     0,     0,     0,    61,     0,     0,    43,
       0,    51,     0,    24,     0,    54,     0,    43,     9,    47,
       0,    34,     0,     0,    95,     0,     0,    59,    45,    56,
       0,    23,     0,    27,     0,    49,    46,    28,    67,     0,
      62,     0,     0,    25,    55,     0,     0,    48,    96,    63,
      26,    35,    10
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     6,     7,     8,   169,     9,    78,    79,   165,    80,
      37,    10,    38,   122,   170,    66,    81,   166,    58,    59,
      60,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -142
static const yytype_int16 yypact[] =
{
      25,   -32,  -142,  -142,  -142,    17,    42,  -142,    21,  -142,
      25,    13,  -142,  -142,    31,  -142,    39,    19,    56,  -142,
      16,  -142,    36,    22,    27,    41,    28,   108,    85,    28,
      28,    28,    28,    28,    28,    28,    88,    19,    60,    38,
      66,    69,    51,   -11,   -43,  -142,    48,   -21,  -142,  -142,
    -142,  -142,    54,  -142,   -42,    80,   120,    82,    86,    56,
      10,   127,   130,  -142,  -142,  -142,    71,    90,  -142,  -142,
    -142,  -142,  -142,  -142,  -142,    28,  -142,   133,   108,  -142,
    -142,    79,   109,    78,    83,    84,   100,  -142,  -142,  -142,
     -56,  -142,  -142,    28,    28,    28,    28,    28,    28,    28,
      28,    28,    28,    28,    28,    28,    28,    28,    28,    28,
      28,    28,  -142,    28,    87,    28,  -142,  -142,    28,  -142,
    -142,   104,   102,  -142,    28,    91,  -142,  -142,     0,   114,
    -142,  -142,  -142,  -142,   150,  -142,    99,  -142,    94,  -142,
     -43,   -43,  -142,  -142,  -142,  -142,  -142,  -142,  -142,  -142,
    -142,  -142,  -142,   105,   121,   123,  -142,   125,   106,   130,
     134,  -142,    28,  -142,   168,   128,   111,   130,   135,   131,
     112,  -142,    28,    28,  -142,   115,    56,  -142,  -142,    56,
     138,  -142,     0,  -142,   141,    28,   150,  -142,  -142,   140,
    -142,   119,   144,  -142,  -142,   122,   143,  -142,  -142,  -142,
    -142,  -142,  -142
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -142,  -142,   175,   -10,  -142,  -142,  -142,  -142,  -142,  -142,
    -142,  -142,   151,  -141,     3,  -108,   113,     8,   -58,    18,
     -17,   -92,    97,   101,  -142,   -45,   -14,    55,  -142,    92,
    -142,  -142,  -142,   132,    75
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -1
static const yytype_uint8 yytable[] =
{
      39,   117,   137,   154,   163,   134,    61,    36,   135,    67,
      99,    11,    83,    84,    85,    86,   161,    77,   178,   112,
      39,    12,    19,    20,    21,    14,   184,    36,   108,   109,
      22,    19,    20,    21,   100,     1,   101,    23,    24,    25,
       2,     3,    13,     4,     5,    26,     2,     3,    27,     4,
       5,    28,   140,   141,    29,    30,    31,    16,   125,    19,
      54,    21,    32,   164,    61,    97,    33,    98,    77,    55,
      17,    32,    56,   118,   119,    33,   136,   196,    18,    62,
     188,    34,    35,    63,    57,   142,   143,   144,    64,    82,
      34,    35,    90,   153,    67,   110,   155,   111,   157,    32,
      92,   158,    65,    33,   102,   103,    93,    67,    87,    88,
      89,    94,    68,   104,   105,   106,    95,   107,    34,    35,
      69,   192,    96,   113,   114,   115,   116,    70,    71,     2,
       3,    20,     4,     5,   121,   123,   124,   126,    72,    73,
     128,    74,   130,   129,   133,   180,   160,   131,   132,    75,
     159,   156,   162,   167,   168,   172,   189,   145,   146,   147,
     148,   149,   150,   171,   152,   174,   173,   175,    67,   176,
     177,    76,   181,   179,   182,   183,   187,   186,   185,   190,
     193,   195,   198,   199,   200,    15,   201,   202,    91,   197,
     194,   127,   138,   120,   191,     0,     0,   139,     0,     0,
     151
};

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-142)))

#define yytable_value_is_error(Yytable_value) \
  YYID (0)

static const yytype_int16 yycheck[] =
{
      17,    59,    94,   111,     4,    61,    48,    17,    64,    26,
      53,    43,    29,    30,    31,    32,   124,    27,   159,    61,
      37,     4,     3,     4,     5,     4,   167,    37,    49,    50,
      11,     3,     4,     5,    77,    10,    79,    18,    19,    20,
      21,    22,     0,    24,    25,    26,    21,    22,    29,    24,
      25,    32,    97,    98,    35,    36,    37,    44,    75,     3,
       4,     5,    43,    63,    48,    76,    47,    78,    78,    13,
      39,    43,    16,    63,    64,    47,    93,   185,    39,    43,
     172,    62,    63,    61,    28,    99,   100,   101,    61,     4,
      62,    63,     4,   110,   111,    41,   113,    43,   115,    43,
      40,   118,    61,    47,    56,    57,    68,   124,    33,    34,
      35,    45,     4,    65,    66,    67,    47,    69,    62,    63,
      12,   179,    71,    43,     4,    43,    40,    19,    20,    21,
      22,     4,    24,    25,     4,    64,    46,     4,    30,    31,
      61,    33,    64,    34,    44,   162,    44,    64,    64,    41,
      46,    64,    61,    39,     4,    61,   173,   102,   103,   104,
     105,   106,   107,    64,   109,    44,    61,    44,   185,    44,
      64,    63,     4,    39,    46,    64,    64,    46,    43,    64,
      42,    40,    42,    64,    40,    10,    64,    44,    37,   186,
     182,    78,    95,    61,   176,    -1,    -1,    96,    -1,    -1,
     108
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    10,    21,    22,    24,    25,    81,    82,    83,    85,
      91,    43,     4,     0,     4,    82,    44,    39,    39,     3,
       4,     5,    11,    18,    19,    20,    26,    29,    32,    35,
      36,    37,    43,    47,    62,    63,    83,    90,    92,   100,
     101,   102,   103,   104,   105,   106,   107,   108,   109,   110,
     111,   112,   113,   114,     4,    13,    16,    28,    98,    99,
     100,    48,    43,    61,    61,    61,    95,   100,     4,    12,
      19,    20,    30,    31,    33,    41,    63,    83,    86,    87,
      89,    96,     4,   100,   100,   100,   100,   114,   114,   114,
       4,    92,    40,    68,    45,    47,    71,    76,    78,    53,
      77,    79,    56,    57,    65,    66,    67,    69,    49,    50,
      41,    43,    61,    43,     4,    43,    40,    98,    63,    64,
     113,     4,    93,    64,    46,   100,     4,    96,    61,    34,
      64,    64,    64,    44,    61,    64,   100,   101,   102,   103,
     105,   105,   106,   106,   106,   107,   107,   107,   107,   107,
     107,   109,   107,   100,    95,   100,    64,   100,   100,    46,
      44,    95,    61,     4,    63,    88,    97,    39,     4,    84,
      94,    64,    61,    61,    44,    44,    44,    64,    93,    39,
     100,     4,    46,    64,    93,    43,    46,    64,   101,   100,
      64,    99,    98,    42,    97,    40,    95,    94,    42,    64,
      40,    64,    44
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  However,
   YYFAIL appears to be in use.  Nevertheless, it is formally deprecated
   in Bison 2.4.2's NEWS entry, where a plan to phase it out is
   discussed.  */

#define YYFAIL		goto yyerrlab
#if defined YYFAIL
  /* This is here to suppress warnings from the GCC cpp's
     -Wunused-macros.  Normally we don't worry about that warning, but
     some users do, and we want to make it easy for users to remove
     YYFAIL uses, which will produce warnings from Bison 2.5.  */
#endif

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))

/* Error token number */
#define YYTERROR	1
#define YYERRCODE	256


/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */
#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
        break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
#else
static void
yy_stack_print (yybottom, yytop)
    yytype_int16 *yybottom;
    yytype_int16 *yytop;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yyrule)
    YYSTYPE *yyvsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULL, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULL;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - Assume YYFAIL is not used.  It's too flawed to consider.  See
       <http://lists.gnu.org/archive/html/bison-patches/2009-12/msg00024.html>
       for details.  YYERROR is fine as it does not invoke this
       function.
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULL, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  YYUSE (yyvaluep);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
        break;
    }
}




/* The lookahead symbol.  */
int yychar;


#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval YY_INITIAL_VALUE(yyval_default);

/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       `yyss': related to states.
       `yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss_alloc, yyss);
	YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
/* Line 1792 of yacc.c  */
#line 128 "tmp.yy"
    {
	builder *r = (yyvsp[(1) - (1)].auxval);
	results = LISTCONS(r, results);
	(yyval.auxval) = freverse(results);
	}
    break;

  case 3:
/* Line 1792 of yacc.c  */
#line 136 "tmp.yy"
    { (yyval.auxval) =
	LISTEND(0);
	}
    break;

  case 4:
/* Line 1792 of yacc.c  */
#line 140 "tmp.yy"
    { (yyval.auxval) =
	LISTCONS((yyvsp[(1) - (2)].auxval), (yyvsp[(2) - (2)].auxval));
	}
    break;

  case 5:
/* Line 1792 of yacc.c  */
#line 148 "tmp.yy"
    { (yyval.auxval) = TREE0("Ointerface"); }
    break;

  case 6:
/* Line 1792 of yacc.c  */
#line 149 "tmp.yy"
    { (yyval.auxval) = TREE0("Oprotocol"); }
    break;

  case 7:
/* Line 1792 of yacc.c  */
#line 150 "tmp.yy"
    { (yyval.auxval) = TREE0("Osection"); }
    break;

  case 8:
/* Line 1792 of yacc.c  */
#line 151 "tmp.yy"
    { (yyval.auxval) = 
	TREE1("Ounitmode", (yyvsp[(2) - (2)].auxval)); 
	}
    break;

  case 9:
/* Line 1792 of yacc.c  */
#line 158 "tmp.yy"
    { (yyval.auxval) =
	TREE2("Oui", (yyvsp[(1) - (1)].auxval), NULL);
	}
    break;

  case 10:
/* Line 1792 of yacc.c  */
#line 162 "tmp.yy"
    { (yyval.auxval) =
	TREE2("Oui", (yyvsp[(1) - (4)].auxval), (yyvsp[(3) - (4)].auxval));
	}
    break;

  case 11:
/* Line 1792 of yacc.c  */
#line 172 "tmp.yy"
    { (yyval.auxval) =
	TREE3("Ounit_presence", (yyvsp[(1) - (5)].auxval), (yyvsp[(2) - (5)].auxval), (yyvsp[(4) - (5)].auxval));
	}
    break;

  case 12:
/* Line 1792 of yacc.c  */
#line 178 "tmp.yy"
    { (yyval.auxval) = 
	TREE0("Onq_out"); 
	}
    break;

  case 13:
/* Line 1792 of yacc.c  */
#line 182 "tmp.yy"
    {  (yyval.auxval) = 
	TREE0("Onq_in"); 
	}
    break;

  case 14:
/* Line 1792 of yacc.c  */
#line 186 "tmp.yy"
    { (yyval.auxval) =  
	TREE0("Onq_inout"); 
	}
    break;

  case 15:
/* Line 1792 of yacc.c  */
#line 190 "tmp.yy"
    { (yyval.auxval) = 
	TREE0("Onq_pling"); 
	}
    break;

  case 16:
/* Line 1792 of yacc.c  */
#line 194 "tmp.yy"
    { (yyval.auxval) = 
	TREE1("Onq_user", (yyvsp[(1) - (1)].auxval)); 
	}
    break;

  case 17:
/* Line 1792 of yacc.c  */
#line 198 "tmp.yy"
    { (yyval.auxval) = 
	TREE0("Onq_forward"); 
	}
    break;

  case 18:
/* Line 1792 of yacc.c  */
#line 202 "tmp.yy"
    { (yyval.auxval) = 
	TREE0("Onq_reverse"); 
	}
    break;

  case 19:
/* Line 1792 of yacc.c  */
#line 206 "tmp.yy"
    { (yyval.auxval) = 
	TREE1("Onq_bit", (yyvsp[(1) - (1)].auxval)); 
	}
    break;

  case 20:
/* Line 1792 of yacc.c  */
#line 210 "tmp.yy"
    { (yyval.auxval) = (yyvsp[(1) - (1)].auxval); }
    break;

  case 21:
/* Line 1792 of yacc.c  */
#line 212 "tmp.yy"
    { (yyval.auxval) = 
 	TREE2("Onq_inherit", (yyvsp[(1) - (2)].auxval), (yyvsp[(2) - (2)].auxval));
	}
    break;

  case 22:
/* Line 1792 of yacc.c  */
#line 219 "tmp.yy"
    {
     (yyval.auxval) = TREE0("O_bool");
     }
    break;

  case 23:
/* Line 1792 of yacc.c  */
#line 229 "tmp.yy"
    { (yyval.auxval) = 
	TREE1("Oiq_pling", (yyvsp[(2) - (2)].auxval)); 
	}
    break;

  case 24:
/* Line 1792 of yacc.c  */
#line 233 "tmp.yy"
    { (yyval.auxval) = 
	TREE1("Oiq_id", (yyvsp[(1) - (1)].auxval)); 
	}
    break;

  case 25:
/* Line 1792 of yacc.c  */
#line 247 "tmp.yy"
    { (yyval.auxval) = 
	TREE2("Onq_square_range", (yyvsp[(2) - (5)].auxval), (yyvsp[(4) - (5)].auxval));
	}
    break;

  case 26:
/* Line 1792 of yacc.c  */
#line 254 "tmp.yy"
    { (yyval.auxval) =
	OUI_LINEPOINT(TREE2("Oui_statemachine", (yyvsp[(3) - (7)].auxval), (yyvsp[(6) - (7)].auxval)));
	}
    break;

  case 27:
/* Line 1792 of yacc.c  */
#line 258 "tmp.yy"
    { (yyval.auxval) = 
	OUI_LINEPOINT(TREE2("Oui_node_decl", (yyvsp[(2) - (5)].auxval), (yyvsp[(4) - (5)].auxval)));
	}
    break;

  case 28:
/* Line 1792 of yacc.c  */
#line 262 "tmp.yy"
    { (yyval.auxval) = 
	OUI_LINEPOINT(TREE3("Oui_instance", (yyvsp[(1) - (5)].auxval), (yyvsp[(2) - (5)].auxval), (yyvsp[(4) - (5)].auxval)));
	}
    break;

  case 29:
/* Line 1792 of yacc.c  */
#line 266 "tmp.yy"
    { (yyval.auxval) = 
	OUI_LINEPOINT(TREE3("Oui_instance", (yyvsp[(1) - (3)].auxval), (yyvsp[(2) - (3)].auxval), NULL)); /* null means exactly one anonymous instance */
	}
    break;

  case 30:
/* Line 1792 of yacc.c  */
#line 270 "tmp.yy"
    { (yyval.auxval) = 
	OUI_LINEPOINT(TREE1("Oui_connect", (yyvsp[(2) - (3)].auxval)));
	}
    break;

  case 31:
/* Line 1792 of yacc.c  */
#line 274 "tmp.yy"
    { (yyval.auxval) = 
	OUI_LINEPOINT(TREE1("Oui_always", (yyvsp[(2) - (3)].auxval)));
	}
    break;

  case 32:
/* Line 1792 of yacc.c  */
#line 278 "tmp.yy"
    { (yyval.auxval) = 
	OUI_LINEPOINT(TREE1("Oui_initial", (yyvsp[(2) - (3)].auxval)));
	}
    break;

  case 33:
/* Line 1792 of yacc.c  */
#line 283 "tmp.yy"
    { (yyval.auxval) = 
	OUI_LINEPOINT(TREE1("Oui_never", (yyvsp[(2) - (3)].auxval)));
	}
    break;

  case 34:
/* Line 1792 of yacc.c  */
#line 287 "tmp.yy"
    { (yyval.auxval) = 
	OUI_LINEPOINT(TREE2("Oui_colonassign", (yyvsp[(1) - (4)].auxval), (yyvsp[(3) - (4)].auxval)));
	}
    break;

  case 35:
/* Line 1792 of yacc.c  */
#line 291 "tmp.yy"
    { (yyval.auxval) = 
	OUI_LINEPOINT(TREE2("Oui_enum_decl", (yyvsp[(2) - (7)].auxval), (yyvsp[(5) - (7)].auxval)));
	}
    break;

  case 36:
/* Line 1792 of yacc.c  */
#line 296 "tmp.yy"
    { (yyval.auxval) = 
	OUI_LINEPOINT(TREE0("Oui_neutral"));
	}
    break;

  case 37:
/* Line 1792 of yacc.c  */
#line 300 "tmp.yy"
    { (yyval.auxval) = 
	OUI_LINEPOINT(TREE0("Oui_forward"));
	}
    break;

  case 38:
/* Line 1792 of yacc.c  */
#line 304 "tmp.yy"
    { (yyval.auxval) = 
	OUI_LINEPOINT(TREE0("Oui_reverse"));
	}
    break;

  case 39:
/* Line 1792 of yacc.c  */
#line 315 "tmp.yy"
    { (yyval.auxval) = (yyvsp[(1) - (1)].auxval); }
    break;

  case 40:
/* Line 1792 of yacc.c  */
#line 320 "tmp.yy"
    { (yyval.auxval) =
	TREE2("Osimplebev", nil, (yyvsp[(5) - (6)].auxval));
	}
    break;

  case 41:
/* Line 1792 of yacc.c  */
#line 329 "tmp.yy"
    { (yyval.auxval) =
	LISTEND(0);
	}
    break;

  case 42:
/* Line 1792 of yacc.c  */
#line 333 "tmp.yy"
    { (yyval.auxval) =
	LISTCONS((yyvsp[(1) - (2)].auxval), (yyvsp[(2) - (2)].auxval));
	}
    break;

  case 43:
/* Line 1792 of yacc.c  */
#line 340 "tmp.yy"
    { (yyval.auxval) =
	LISTEND(0);
	}
    break;

  case 44:
/* Line 1792 of yacc.c  */
#line 343 "tmp.yy"
    { (yyval.auxval) = LISTCONS((yyvsp[(1) - (1)].auxval), LISTEND(0)); }
    break;

  case 45:
/* Line 1792 of yacc.c  */
#line 345 "tmp.yy"
    { (yyval.auxval) = LISTCONS((yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); }
    break;

  case 46:
/* Line 1792 of yacc.c  */
#line 351 "tmp.yy"
    { (yyval.auxval) =
	LISTEND(0);
	}
    break;

  case 47:
/* Line 1792 of yacc.c  */
#line 354 "tmp.yy"
    { (yyval.auxval) = LISTCONS((yyvsp[(1) - (1)].auxval), LISTEND(0)); }
    break;

  case 48:
/* Line 1792 of yacc.c  */
#line 356 "tmp.yy"
    { (yyval.auxval) = LISTCONS((yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); }
    break;

  case 49:
/* Line 1792 of yacc.c  */
#line 362 "tmp.yy"
    { (yyval.auxval) =
	LISTEND(0);
	}
    break;

  case 50:
/* Line 1792 of yacc.c  */
#line 366 "tmp.yy"
    { (yyval.auxval) =
	LISTCONS((yyvsp[(1) - (1)].auxval), LISTEND(0));
	}
    break;

  case 51:
/* Line 1792 of yacc.c  */
#line 370 "tmp.yy"
    { (yyval.auxval) =
	LISTCONS((yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval));
	}
    break;

  case 52:
/* Line 1792 of yacc.c  */
#line 377 "tmp.yy"
    { (yyval.auxval) =
	LISTEND(0);
	}
    break;

  case 53:
/* Line 1792 of yacc.c  */
#line 381 "tmp.yy"
    { (yyval.auxval) =
	LISTCONS((yyvsp[(1) - (2)].auxval), (yyvsp[(2) - (2)].auxval));
	}
    break;

  case 54:
/* Line 1792 of yacc.c  */
#line 388 "tmp.yy"
    { (yyval.auxval) =
	LISTCONS((yyvsp[(1) - (1)].auxval), LISTEND(0));
	}
    break;

  case 55:
/* Line 1792 of yacc.c  */
#line 392 "tmp.yy"
    { (yyval.auxval) =
	LISTCONS((yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval));
	}
    break;

  case 56:
/* Line 1792 of yacc.c  */
#line 399 "tmp.yy"
    { (yyval.auxval) =
	LISTEND(0);
	}
    break;

  case 57:
/* Line 1792 of yacc.c  */
#line 403 "tmp.yy"
    { (yyval.auxval) =
	LISTCONS((yyvsp[(1) - (2)].auxval), (yyvsp[(2) - (2)].auxval));
	}
    break;

  case 58:
/* Line 1792 of yacc.c  */
#line 410 "tmp.yy"
    { (yyval.auxval) =
	TREE1("Olabel", (yyvsp[(1) - (2)].auxval));

	}
    break;

  case 59:
/* Line 1792 of yacc.c  */
#line 416 "tmp.yy"
    { (yyval.auxval) =
	TREE2("Ochannel_write", (yyvsp[(1) - (4)].auxval), (yyvsp[(3) - (4)].auxval));
	}
    break;

  case 60:
/* Line 1792 of yacc.c  */
#line 421 "tmp.yy"
    { (yyval.auxval) =
	TREE1("Oe_as_c", (yyvsp[(1) - (2)].auxval));
	}
    break;

  case 61:
/* Line 1792 of yacc.c  */
#line 426 "tmp.yy"
    { (yyval.auxval) =
	TREE1("Ogoto", (yyvsp[(2) - (3)].auxval));
	}
    break;

  case 62:
/* Line 1792 of yacc.c  */
#line 431 "tmp.yy"
    { (yyval.auxval) =
	TREE1("Owait", (yyvsp[(3) - (5)].auxval));
	}
    break;

  case 63:
/* Line 1792 of yacc.c  */
#line 435 "tmp.yy"
    { (yyval.auxval) =
	TREE3("Oif", (yyvsp[(3) - (6)].auxval), (yyvsp[(5) - (6)].auxval), nil);
	}
    break;

  case 64:
/* Line 1792 of yacc.c  */
#line 442 "tmp.yy"
    { (yyval.auxval) = (yyvsp[(1) - (1)].auxval); }
    break;

  case 65:
/* Line 1792 of yacc.c  */
#line 444 "tmp.yy"
    { (yyval.auxval) =
	TREE2("Oassign", (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval));
	}
    break;

  case 66:
/* Line 1792 of yacc.c  */
#line 452 "tmp.yy"
    { (yyval.auxval) = (yyvsp[(1) - (1)].auxval); }
    break;

  case 67:
/* Line 1792 of yacc.c  */
#line 453 "tmp.yy"
    { (yyval.auxval) = TREE3("Oquery", (yyvsp[(1) - (5)].auxval), (yyvsp[(3) - (5)].auxval), (yyvsp[(5) - (5)].auxval)); }
    break;

  case 68:
/* Line 1792 of yacc.c  */
#line 457 "tmp.yy"
    { (yyval.auxval) = (yyvsp[(1) - (1)].auxval); }
    break;

  case 69:
/* Line 1792 of yacc.c  */
#line 459 "tmp.yy"
    { (yyval.auxval) = 
	TREE3("Odiadic", YYLEAF("Oimplies"), (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 70:
/* Line 1792 of yacc.c  */
#line 466 "tmp.yy"
    { (yyval.auxval) = (yyvsp[(1) - (1)].auxval); }
    break;

  case 71:
/* Line 1792 of yacc.c  */
#line 471 "tmp.yy"
    { (yyval.auxval) = (yyvsp[(1) - (1)].auxval); }
    break;

  case 72:
/* Line 1792 of yacc.c  */
#line 472 "tmp.yy"
    { (yyval.auxval) = 
	TREE3("Odiadic", YYLEAF("Ologor"), (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 73:
/* Line 1792 of yacc.c  */
#line 475 "tmp.yy"
    { (yyval.auxval) = 
	TREE3("Odiadic", YYLEAF("Ologor"), (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 74:
/* Line 1792 of yacc.c  */
#line 482 "tmp.yy"
    { (yyval.auxval) = (yyvsp[(1) - (1)].auxval); }
    break;

  case 75:
/* Line 1792 of yacc.c  */
#line 483 "tmp.yy"
    { (yyval.auxval) = 
	TREE3("Odiadic", YYLEAF("Ologand"), (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 76:
/* Line 1792 of yacc.c  */
#line 486 "tmp.yy"
    { (yyval.auxval) = 
	TREE3("Odiadic", YYLEAF("Oxor"), (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 77:
/* Line 1792 of yacc.c  */
#line 489 "tmp.yy"
    { (yyval.auxval) = 
	TREE3("Odiadic", YYLEAF("Ologand"), (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 78:
/* Line 1792 of yacc.c  */
#line 495 "tmp.yy"
    { (yyval.auxval) = (yyvsp[(1) - (1)].auxval); }
    break;

  case 79:
/* Line 1792 of yacc.c  */
#line 497 "tmp.yy"
    { (yyval.auxval) = 
	TREE3("Odiadic", YYLEAF("Odeqd"), (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 80:
/* Line 1792 of yacc.c  */
#line 500 "tmp.yy"
    { (yyval.auxval) = 
	TREE3("Odiadic", YYLEAF("Odned"), (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 81:
/* Line 1792 of yacc.c  */
#line 503 "tmp.yy"
    { (yyval.auxval) = 
	TREE3("Odiadic", YYLEAF("Odltd"), (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 82:
/* Line 1792 of yacc.c  */
#line 506 "tmp.yy"
    { (yyval.auxval) = 
	TREE3("Odiadic", YYLEAF("Odled"), (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 83:
/* Line 1792 of yacc.c  */
#line 509 "tmp.yy"
    { (yyval.auxval) = 
	TREE3("Odiadic", YYLEAF("Odgtd"), (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 84:
/* Line 1792 of yacc.c  */
#line 512 "tmp.yy"
    { (yyval.auxval) = 
	TREE3("Odiadic", YYLEAF("Odged"), (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 85:
/* Line 1792 of yacc.c  */
#line 520 "tmp.yy"
    { (yyval.auxval) = (yyvsp[(1) - (1)].auxval); }
    break;

  case 86:
/* Line 1792 of yacc.c  */
#line 521 "tmp.yy"
    { (yyval.auxval) = 
	TREE3("Odiadic", YYLEAF("Obinor"), (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 87:
/* Line 1792 of yacc.c  */
#line 529 "tmp.yy"
    { (yyval.auxval) = (yyvsp[(1) - (1)].auxval); }
    break;

  case 88:
/* Line 1792 of yacc.c  */
#line 530 "tmp.yy"
    { (yyval.auxval) = 
	TREE3("Odiadic", YYLEAF("Obitand"), (yyvsp[(1) - (3)].auxval), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 89:
/* Line 1792 of yacc.c  */
#line 537 "tmp.yy"
    { (yyval.auxval) = (yyvsp[(1) - (1)].auxval); }
    break;

  case 90:
/* Line 1792 of yacc.c  */
#line 542 "tmp.yy"
    { (yyval.auxval) = (yyvsp[(1) - (1)].auxval); }
    break;

  case 91:
/* Line 1792 of yacc.c  */
#line 546 "tmp.yy"
    { (yyval.auxval) = (yyvsp[(1) - (1)].auxval); }
    break;

  case 92:
/* Line 1792 of yacc.c  */
#line 550 "tmp.yy"
    { (yyval.auxval) = (yyvsp[(1) - (1)].auxval); }
    break;

  case 93:
/* Line 1792 of yacc.c  */
#line 555 "tmp.yy"
    { (yyval.auxval) =
	TREE1("Oid", (yyvsp[(1) - (1)].auxval));
	}
    break;

  case 94:
/* Line 1792 of yacc.c  */
#line 559 "tmp.yy"
    { (yyval.auxval) = 
	TREE3("Odiadic", YYLEAF("Ocatenate"), TREE1("Oid", (yyvsp[(1) - (3)].auxval)), (yyvsp[(3) - (3)].auxval)); 
	}
    break;

  case 95:
/* Line 1792 of yacc.c  */
#line 567 "tmp.yy"
    { (yyval.auxval) =
	TREE2("Oapply", (yyvsp[(1) - (4)].auxval), (yyvsp[(3) - (4)].auxval));
    }
    break;

  case 96:
/* Line 1792 of yacc.c  */
#line 572 "tmp.yy"
    { (yyval.auxval) =
	TREE3("Oexp_range", (yyvsp[(1) - (6)].auxval), (yyvsp[(3) - (6)].auxval), (yyvsp[(5) - (6)].auxval));
    }
    break;

  case 97:
/* Line 1792 of yacc.c  */
#line 577 "tmp.yy"
    { (yyval.auxval) = (yyvsp[(1) - (1)].auxval); }
    break;

  case 98:
/* Line 1792 of yacc.c  */
#line 579 "tmp.yy"
    { (yyval.auxval) = (yyvsp[(2) - (3)].auxval); }
    break;

  case 99:
/* Line 1792 of yacc.c  */
#line 582 "tmp.yy"
    { (yyval.auxval) =
	TREE1("Ochannel_read", (yyvsp[(2) - (2)].auxval));
	}
    break;

  case 100:
/* Line 1792 of yacc.c  */
#line 587 "tmp.yy"
    { (yyval.auxval) =
	TREE1("Onum", (yyvsp[(1) - (1)].auxval));
	}
    break;

  case 101:
/* Line 1792 of yacc.c  */
#line 592 "tmp.yy"
    { (yyval.auxval) =
	TREE1("Ostring", (yyvsp[(1) - (1)].auxval));
	}
    break;

  case 102:
/* Line 1792 of yacc.c  */
#line 597 "tmp.yy"
    { (yyval.auxval) =
	TREE1("Obinnot", (yyvsp[(2) - (2)].auxval));
	}
    break;

  case 103:
/* Line 1792 of yacc.c  */
#line 602 "tmp.yy"
    { (yyval.auxval) =
	TREE1("Olognot", (yyvsp[(2) - (2)].auxval));
	}
    break;


/* Line 1792 of yacc.c  */
#line 2392 "h2.y.tab.c"
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}


/* Line 2055 of yacc.c  */
#line 607 "tmp.yy"


#include <stdio.h>
#include <ctype.h>

#include "lineno.h"


extern FILE *stderr;

int yyerror (const char *s) 
{
	extern void cleanexit(int);	
  extern builder *lishlex();
	int line_number = filestack[filesp].line_number;
	char *fn = filestack[filesp].fn;
    fprintf(stderr, "Syntax error: %s: %s:; %i\n", s, fn, line_number);
		fprintf(stderr, "Next symbol: %s\n", atom_to_str(lishlex()));
	cleanexit(1);
		return 1;

}


void yydebug_on()
{
#ifdef YYDEBUG
	yydebug = 0;
#endif
}

char *smllib = "h2gram";
int g_need_ee_starter = 0;

/*
 * Two characters which define the escape seq for comment to end of line 
 */
char commentchar1 = '/';
char commentchar2 = '/';
char *langsuffix = ".h2";


int yylex()
{
  char s;  int v;
  extern builder *lishlex();
  extern int lextracef;
  builder *b, *a = lishlex();
  if (!a) return 0;
  yylval.auxval = a;

  if (fnumberp(a)) return sd_number;
  if (fstringp(a)) return sd_string;

  b = fgetprop(a, smacro);
  if (lextracef)  printf("Lex %i %p %s\n", v, b, atom_to_str(a)); fflush(stdout);
  if (b) return atom_to_int(fcdr(b));
  s = atom_to_str(a)[0];
  if (isalpha(s) || s=='_') return sd_id;
  cbgerror(cbge_fatal, "Illegal input token %c", s);
  return 0;
}



/* eof */

