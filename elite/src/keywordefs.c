# 34 "/tmp/aa.c"
  cbglish_keyword(100, "statechart", s_statechart, SPARE1, SPARE2);;
  cbglish_keyword(100, "in", s_in, SPARE1, SPARE2);;
  cbglish_keyword(100, "wait", s_wait, SPARE1, SPARE2);;
  cbglish_keyword(100, "while", s_while, SPARE1, SPARE2);;
  cbglish_keyword(100, "of", s_of, SPARE1, SPARE2);;
  cbglish_keyword(100, "goto", s_goto, SPARE1, SPARE2);;
  cbglish_keyword(100, "when", s_when, SPARE1, SPARE2);;
  cbglish_keyword(100, "neutral", s_neutral, SPARE1, SPARE2);;
  cbglish_keyword(100, "forward", s_forward, SPARE1, SPARE2);;
  cbglish_keyword(100, "reverse", s_reverse, SPARE1, SPARE2);;
  cbglish_keyword(100, "protocol", s_protocol, SPARE1, SPARE2);;
  cbglish_keyword(100, "domain", s_domain, SPARE1, SPARE2);;


  cbglish_symbol(100, "\"{\"", ss_lsect, SPARE1, SPARE2);
  cbglish_symbol(100, "\"}\"", ss_rsect, SPARE1, SPARE2);
  cbglish_symbol(100, "\"(\"", ss_lpar, SPARE1, SPARE2);
  cbglish_symbol(100, "\")\"", ss_rpar, SPARE1, SPARE2);
  cbglish_symbol(100, "\"=\"", ss_equals, SPARE1, SPARE2);
  cbglish_symbol(100, "\",\"", ss_comma, SPARE1, SPARE2);
  cbglish_symbol(100, "\".\"", ss_dot, SPARE1, SPARE2);
  cbglish_symbol(100, "\"<=\"", ss_dled, SPARE1, SPARE2);
  cbglish_symbol(100, "\">=\"", ss_dged, SPARE1, SPARE2);
  cbglish_symbol(100, "\"==\"", ss_deqd, SPARE1, SPARE2);
  cbglish_symbol(100, "\"<\"", ss_dltd, SPARE1, SPARE2);
  cbglish_symbol(100, "\">\"", ss_dgtd, SPARE1, SPARE2);
  cbglish_symbol(100, "\"!=\"", ss_dned, SPARE1, SPARE2);
  cbglish_symbol(100, "\"->\"", ss_arrow1, SPARE1, SPARE2);
  cbglish_symbol(100, "\"%\"", ss_percent, SPARE1, SPARE2);
  cbglish_symbol(100, "\"+\"", ss_plus, SPARE1, SPARE2);
  cbglish_symbol(100, "\"-\"", ss_minus, SPARE1, SPARE2);
  cbglish_symbol(100, "\"+=\"", ss_plusequals, SPARE1, SPARE2);
  cbglish_symbol(100, "\"-=\"", ss_minusequals, SPARE1, SPARE2);
  cbglish_symbol(100, "\"<<\"", ss_lshift, SPARE1, SPARE2);
  cbglish_symbol(100, "\">>\"", ss_rshift, SPARE1, SPARE2);
  cbglish_symbol(100, "\"*\"", ss_star, SPARE1, SPARE2);
