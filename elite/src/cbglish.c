/*
 *  $ID: $
 *
 * Bigtop.  
 * (C) 2003 DJ Greaves. All rights reserved.
 *
 */


/* cbg cbglish.c */

/* CBG solos 1993. V2 1996 - with preprocessor calls */



#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "cbglish.h"
#include "y.tab.h"
#include <fcntl.h>
#include <stdarg.h>
#include <unistd.h>
#include "lineno.h"

#define MAXARGS 100

#define NEW(X) ((struct X *) localmalloc(sizeof(struct X)))
#define TRC(X) 
#define TRC1(X) 

#define SKIP(X)
extern char preprocessor_buffer[];
extern char *grammer[], *nodes[];
static int usingtoks = 0;
static int sizednumf = 0;
static unsigned char lokch;

#define MAXATOML 100
#define INBUFFERL 4096
char atomheart[MAXATOML];
#define c_white 1
#define c_selfextending 2
#define c_continued 4
#define c_badchar 8
#define c_selfterminating 16
#define c_digit 32
#define c_dquote 64
#define c_delimiter 128

#define comment_parse_buffer_len 255
static char comment_parse_buffer[comment_parse_buffer_len];
builder *pushed_lex_tokens[100];
int n_pushed_lex_tokens = 0;

#define COLONSEPS
char *preprocessor_path = ".";
unsigned char (*lishch1)();  /* One calls lishch to read the next input character */

/* Local prototypes */
unsigned char lishch();
int lex_set_input_file_name(const char *fn);
void lex_set_input_file(int fd);
void lex_set_input_mem(unsigned char *start, unsigned char *end);
void toc_fprintl(FILE *f, unsigned char *s, unsigned char *e, unsigned char *pt);
void c_defidx();
void print1(builder *x);
void init_scheme();
builder * ffread_one();
builder * lishlex();
builder * lishlex_string();
builder * lishlex_number();
builder * fillin_atom();
builder * mknumber();
builder * lishlex_list();
builder *rr1();
char *progname = "bigtop";

/*
 * localmalloc code
 */
static char *localmem;
static int locallen = 0;
#define LOCALMALSIZE 0x2000
#define LOCALMINSIZE 0x20


FILESTACK filestack[100]
;

int filesp = 0;

char *localmalloc(int size)
{
  char *r;
  while (locallen < LOCALMINSIZE)
    {
      localmem = malloc(LOCALMALSIZE);
      if (localmem) locallen = LOCALMALSIZE;
      else 
	{ 
	  printf("%s: Out of (swap/VM) memory\n", progname);
	  exit(1);
	}
    }
  if (size > locallen)
    {
      return (char *) malloc(size);
    }
  else
    {
      r = localmem;
      localmem += size;
      locallen -= size;
      return r;
    }
}

  
unsigned char * lineiolinein(); // Prototype

/* 
 *A character, which if at the start of a line, makes it a comment 
 */
char newline_commentchar = '%';




int ignorenewlines = 1;

const char *filename = "";

#define UNIQUE_MAX 6
char unique_run[UNIQUE_MAX];

/* Static vars */
static int newlinef = 1;
static char nchar = 1;

int vogue;
static int colons_not_in_lists = 0;
builder *textualnil;
int eflag = 0;
int errors;
int tabsok = 0;
int verbose = 0;
int endnotneeded = 0;
builder *srule, *sstile, *swhite, *sany, *scline, *scnewline, *sequals, *ssentential;
builder *sproductions, *shasht, *slambda, *sbuiltin, *sunset, *smacro;
builder *slist, *simport;




#define maxline (132)
char zbuf[maxline];


void breakpoint()
{
  exit(1);
}

void cbgerror(int mode, ...)
{
  unsigned char *eom = filestack[filesp].inpoi;
  unsigned char *som = --filestack[filesp].inpoi;
  builder *a, *b, *c;
  va_list ap;
  va_start(ap, mode);
  mode = va_arg(ap, int);
  char *string = va_arg(ap, char *);
  a = va_arg(ap, builder *);
  b = va_arg(ap, builder *);
  c = va_arg(ap, builder *);
/*
  win_gain_dialogue();
*/
  printf(string, a, b, c);
  printf("\n");

  if (usingtoks)
    {
      while (som > filestack[filesp].toks_start && *som != '\n') som--;
      while (eom < filestack[filesp].toks_end && *eom != '\n') eom++;

      som++;
      toc_fprintl(stdout, som, eom, filestack[filesp].inpoi);
    }
  if (mode == -1)
  {
    printf("Arg :"); 
    fdisplay(a);
  }
  if (mode == -2)
  {
    printf("L-Arg :"); fdisplay(a);
    printf("R-Arg :"); fdisplay(b);
  }
/*
  backtrace();
*/
  va_end(ap);
  if (mode != cbge_msg) reenter();
  breakpoint();
  exit(1);
}    


#ifdef CBGLISHMAIN
int main (argc, argv) char **argv;
{
  int notstdin = 0;

  argc--;
  argv++;

  while (argc)
  {
    if (strcmp(*argv, "-E")==0)
    {
      eflag = 1;
      argc -= 1;
      argv += 1;
      continue;
    }
    if (strcmp(*argv, "-endnotneeded")==0)
    {
      endnotneeded = 1;
      argc -= 1;
      argv += 1;
      continue;
    }
    if (strcmp(*argv, "-verbose")==0)
    {
      verbose = 1;
      argc -= 1;
      argv += 1;
      continue;
    }
    if (argv[0][0] == '_')
    {
      eflag = atoi(argv[0]+1);
      argc --;
      argv ++;
      continue;
    }

    if (argv[0][0] == '-')  /* Any other option flag prints message */
    {
      printf("usenix is: cmx [ -endnotneeded -verbose -tabsok ] [ fn fn ... ] \n");
      exit(1);
    }
    break;
  }

  init_cbglish();

 /* End of option parsing */
  errors = 0;
  fdisplay(str_to_atom("working"));
  while(1) 
    {
      builder *i;
      do i= firead(); while (i == nil);
      fdisplay(i);
      fdisplay(feval(i));
    }
  return errors;
}
#endif


/*
 * Search a path for a string 
 */
#define MAXFN 1024
static char pathseq [MAXFN];

FILE *fpathopen_r(const char *name, const char *path)
{
  int vd = 0;
  FILE *r;
  char *mode = "r";
  /* explicit path name used */
  filename = name;
  if (path == NULL || *name == '/' || *name == '.')
    {
      FILE *r= fopen(name, mode);
      if (vd) printf(">1>%s  %p\n", name, r);
      return r;
    }
  while (*path)
  {
    int c = 0;
    char * d = pathseq;
    const char * n = name;
    while (c<MAXFN && *path && *path != ':')
    {
      *(d++) = *(path++);
      c++;
    }
    if (*path) path++; /* skip colon */
    if (c) *(d++) = '/';
    while (c<MAXFN && *n)
    {
      *(d++) = *(n++);
      c++;
    }
    *d = (char) 0;
    r = fopen(pathseq,  mode);
    if (vd) printf(">2>%s  fd=%p\n", name, r);
    if (r) return r;
  }
  //  printf(">>FAILED\n");
  return NULL;
}


builder *cbglish_import()
{
  int tab = 0;
  while (lokch == ' ') lokch = lishch();
  
  while (lokch != '\n' && lokch != ';')
    {
      atomheart[tab++] = lokch;
      lokch = lishch();
      if (tab >= MAXATOML) break;
    }
  atomheart[tab] = (char) 0;
  if (tab == MAXATOML) cbgerror(0, "Import filename too long, truncated\n>%s<", atomheart);
  lokch = lishch();
  printf("From %s: %i: import open of %s\n", filestack[filesp].fn, filestack[filesp].line_number, atomheart);
  filesp += 1;
  lex_set_input_file_name(strdup(atomheart));
  return lishlex();

}



builder *cbglish(int mode, int slists, void *arg, void *arg1)
{
  extern void preprocessor_clean_end_check();
  builder *r;
  colons_not_in_lists = slists;
  newlinef = 1;

  if (mode == c_filename)
  {
    int r;
    char *fn = (char *)arg;
    preprocessor_path = (char *) arg1;
    r = lex_set_input_file_name(fn);
    if (r) cbgerror(cbge_fatal, "Input file not found %s", fn);
    TRC(printf("cbglish file %s\n", (char *) arg));
  }
  if (mode == c_memfile)
  {
    lex_set_input_mem((unsigned char*) arg, (unsigned char*)arg1);
    TRC(printf("From %p to %p\n", (char *)arg, (char *)arg1));
  }
  r = lishlex_list(slists);

#ifdef PREPROCESSOR
  preprocessor_clean_end_check();
#endif
  return r;
}


void push_token(builder *tok) 
{
  pushed_lex_tokens[n_pushed_lex_tokens++] = tok;
}


builder *s_synthembed_start = 0;
builder *s_synthembed_end = 0;

void parse_comment(const char *cbuf)
{
#ifdef PREPROCESSOR
  if (!strncmp(cbuf, "line", 4)) handle_line_token(c);
#endif

  // TODO Set these up once.
  s_synthembed_start = cbglish_keyword(0, "$synthembed_start", sd_synthembed_start, 0, 0);
  s_synthembed_end = cbglish_keyword(0, "$synthembed_end", sd_synthembed_end, 0, 0);

  const char *p = strstr(cbuf, "synth");
  if (p && p-cbuf < 3) // Avoid latching on to the string 'synth' wherever it occurs in a comment.
    {
      //printf("Embedded synth directive found \"%s\"\n", p);
      // Ignored order of magic words for now
      push_token(s_synthembed_end);
      if (strstr(cbuf, " target")) push_token(str_to_atom("target"));
      if (strstr(cbuf, " initiator")) push_token(str_to_atom("initiator"));
      if (strstr(cbuf, " in")) push_token(str_to_atom("in"));
      if (strstr(cbuf, " out")) push_token(str_to_atom("out"));
      if (strstr(cbuf, " always")) push_token(str_to_atom("always"));
      push_token(s_synthembed_start);

    }
}



/* Routine to preprocess, character by character, the input text */
static int incomment = 0;

unsigned char lishch()
{
  while(1)
  {
    unsigned char c = nchar;
    nchar = lishch1();
    
    while (c && c != '\n' && filestack[filesp].need_ee_starter)
      {
	if (c == '<' && nchar == '\'')
	{
	  c = nchar; nchar = lishch1();
	  c = nchar; nchar = lishch1();
	  filestack[filesp].need_ee_starter = 0;
	  break;
	}
	else
	  {
	    c = nchar; nchar = lishch1();
	  }
      }

    if (c == '\'' && nchar == '>')
      {
	filestack[filesp].need_ee_starter = 1;
	c = nchar; nchar = lishch1();
	c = nchar; nchar = lishch1();
	continue;
      }


    if ((c == commentchar1 && nchar == commentchar2)) // eol comments
      {
	int ll = comment_parse_buffer_len;
	char *p = comment_parse_buffer;
	while (c != '\n' && c != (char) 0) 
	  { 
	    c = nchar; nchar = lishch1();
	    if (--ll > 0) *p++ = c;
	  }
	*p = (char) 0;
	if (0)	printf("My comment >%s< end %i\n", comment_parse_buffer+1, filestack[filesp].line_number);
	parse_comment(comment_parse_buffer+1); /* Skip second half of escape by adding 1 */

      }
    
    
    /* Handle block comments */
    while (incomment || (c == '/' && nchar == '*'))
      {
	incomment = 1;
	int ll = comment_parse_buffer_len;
	char *p = comment_parse_buffer;
	while (c != (char) 0)
	  {
	    if (--ll > 0) *p++ = c;
	    if (c == '\n') break;
	    if (c == '*' && nchar == '/') 
	      {
		incomment = 0;
		c = nchar; nchar = lishch1();
		c = nchar; nchar = lishch1();
		break;
	      }
	    c = nchar; nchar = lishch1();
	  }
	if (c == '\n') break;
	if (p > comment_parse_buffer) p--;
        *p = (char) 0;
	if (0) printf("My comment >%s< end %i\n", comment_parse_buffer+2, filestack[filesp].line_number);
	parse_comment(comment_parse_buffer+2); /* Skip opening escape by adding 2 */
      }

    if (c == '\n') 
      {
	filestack[filesp].line_number += 1;
	newlinef = 1; 
      }
    else newlinef = 0;

    /* printf("lishch %02X\n", c); */ 
    
    return c;
  }
}



/* non extending lexical symbols are those such as single and double
   character opertators (eg + <=) which need not be followed by white
   to delimit them.  
*/
struct char_extender 
{
  struct char_extender *h_left, *h_right, *v_left, *v_right;
  builder *atom;
  char key;
};

int char_exteristics[256];
struct char_extender **char_extensions;



unsigned char lishmemget()
{
  if (filestack[filesp].inpoi >= filestack[filesp].toks_end) return (char) 0;
  return *filestack[filesp].inpoi ++;
}

/* The symbol table is held using tree sort with one character per
   tree node.  Horizontal links in the tree are alternative characters
   at the same depth whereas vertical links reflect subsequent chars
   of the symbol.   

   This chain routine processes one char and moves *pp one level down
   the tree.
*/
struct char_extender **link_to_extension(struct char_extender **pp, char c, int make)
{
  struct char_extender *p = *pp;
  SKIP(printf("Extend top >%c< %x %x\n", c, p, pp));
  if (c < p->key) pp = &(p->v_left); else pp = &(p->v_right);
  p = *pp;
  while(1)
  {
    if (p == NULL)
    {
      if (make == 0) return NULL;
      p = (struct char_extender *) malloc(sizeof(struct char_extender));
      memset(p, 0, sizeof(struct char_extender));
      *pp = p;
      p->key = c;
    }
    if (p->key == c) return pp;
    if (c < p->key) pp = &(p->h_left); else pp = &(p->h_right);
    p = *pp;
  }
}




void for_each_atom_one(int (*f)(), struct char_extender *p)
{
  if (p == NULL) return;

  if (p->atom) f(p->atom);
  for_each_atom_one(f, p->h_left);
  for_each_atom_one(f, p->v_left); 
  for_each_atom_one(f, p->h_right);
  for_each_atom_one(f, p->v_right);
}

void for_each_atom(int (*f)())
{
  unsigned char c;
  for (c=0; c != 255; c++) for_each_atom_one(f, char_extensions[c]);
}

/* The main lexical input sub-routine  called by the parsers.
 * >lishlex<
 */
builder *lishlex_core()
{
  int vd = 0;
  int tab = 0;
  builder *a;
  if (vd) printf("lishlex start c=0x%x\n", lokch);
  struct char_extender **pp;
  if (lokch == (char) 0)
  {
    if (vd) printf("lishlex return EOF\n");
    return nil;
  }

  
  while(1)
  {
    int code = char_exteristics[lokch];
    int nextcode;
    TRC1(printf("   lex .. lokch=0x%x  code=0x%x\n", lokch, code));


    if (lokch == '\'')
      {
	int tab = 0;
	lokch = lishch();
	while (lokch != '\'')
	  {
	    atomheart[tab++] = lokch;
	    lokch = lishch();
	    if (tab >= MAXATOML) break;
	  }
	atomheart[tab] = (char) 0;
	if (tab == MAXATOML) cbgerror(0, "Q-String too long, truncated\n>%s<", atomheart);
	lokch = lishch();
	TRC(printf("Q-String in '%s' ", atomheart));
	if (lextracef)  printf("%i DS >%s<\n", filestack[filesp].line_number, atomheart);
	return fillin_atom(ss_dstring, atomheart, tab);
      }

    if (code & c_digit && tab == 0) return lishlex_number(1);
    if (code & c_dquote && tab == 0) return lishlex_string();
    if (code & c_delimiter) break;
    if (code & c_badchar) 
      {
	cbgerror(cbge_msg, "Bad input character 0x%x\n", lokch);
	lokch = lishch();
	continue;
      }
    if (code & c_white && tab == 0)
    {    
      lokch = lishch();
      continue;
    }

    if (lokch == '\n')
    {
      lokch = lishch();
      if (ignorenewlines) return lishlex();
      return scnewline;
    }

    if (sizednumf && isalnum(lokch))
    {
      return lishlex_number(1);  /* Handle n'Xnnnn of Verilog */
    }

    /* Record expectation of verilog field specifications and note them here */
    if (lokch == '\'')
    {
      sizednumf = 1;
    }

    if (tab == 0) pp = char_extensions+lokch;
    else pp = link_to_extension(pp, lokch, 1);
    
    //printf("atomheart char %i\n", tab);
    atomheart[tab] = lokch;
    lokch = lishch();
    tab++;
    if (tab >= MAXATOML) break;

    nextcode = char_exteristics[lokch];
    if (nextcode & (c_white | c_delimiter)) break;
    if ((nextcode | code) & c_selfterminating)
    {
      if (link_to_extension(pp, lokch, 0)==NULL) break;
    }
  }
  atomheart[tab] = (char) 0;
  if (tab >= MAXATOML) cbgerror(cbge_fatal, "Input atom too long %s", atomheart);


  if (lextracef)  printf("    lex %i >%s<\n", filestack[filesp].line_number, atomheart);

  if (tab == 0) return nil;
  if ((*pp)->atom)
    {
      a = (*pp)->atom;
      //printf("already an atom: '%s' p=%p\n", atomheart, a);
    }
  else
    {
      a = (builder *) fillin_atom(ss_symbol, atomheart, tab);
      (*pp)->atom = a;
    }

  if (a == textualnil)
    {
      if (lextracef)  printf("    lex %i textual nil\n", filestack[filesp].line_number);
      return nil;
    }
  if (a == simport) return cbglish_import();
  return a;
}


builder *lishlex()
{
  builder *a = 0;
  if (n_pushed_lex_tokens > 0)
    {
      a = pushed_lex_tokens[--n_pushed_lex_tokens];
    }
  else a = lishlex_core();
  //if (lextracef)  printf("   lex %i %p\n", filestack[filesp].line_number, a);
  return a;
}



/*
 * Another version of lishlex, but this one called for a single atom
 * whose string is passed.  This routine is used to define atoms
 * whose sequence of characters is outside the normal default parsing
 * rules, such as digraphs <= != etc, which would otherwise become two
 * atoms.
 */
builder *str_to_atom(char *name)
{
  int tab = 0;
  builder *a;
  struct char_extender **pp;
  char * s = name;

  while(*s)
  {
    if (tab == 0) pp = char_extensions+(*s);
    else pp = link_to_extension(pp, *s, 1);
    s++;
    tab++;
  }
  if (tab == 0) return nil;
  if ((*pp)->atom) a = (*pp)->atom;
  else
  {
    a = (builder *) fillin_atom(ss_symbol, name, tab);
    (*pp)->atom = a;
  }

  TRC(printf("Atom %s -> %p\n", name, a));
  return (builder *) a;
}



/*
 * Read rest of stream as a list.  If slists is set, then
 * parenthesis and dotted pairs of the slist notation are
 * parsed into a tree structure, otherwise they are not
 * treated as sepcial lexical symbols.
 */
builder *g_ast_parsed = 0;

builder *lishlex_list(int slists)
{
  builder *r = nil;
  builder *start = nil;
  extern int yyparse();

  TRC1(printf("start parse slists=%i\n", slists));
  if (slists)   while (1)
    {
      builder *p; 
      if (lokch == (char) 0) return start;
      if (slists) p = ffread_one(); else p = lishlex();
      if (r) 
	{
	  freplacd(r, fcons(p, nil));
	  r = fcdr(r);
	}
      else
	{
	  r = fcons(p, nil);
	  start = r;
	}
      return start;
    }
  else { 
    TRC1(printf("Calling yyparse()\n"));
    yyparse(); 
    TRC1(printf("Called yyparse()\n"));
    return g_ast_parsed; 
  }
}


/*
 * The following line was used in the much shorter, but stack hungry 
  * recursive version of this routine.
 * return fcons(p, lishlex_list(slists));
 */

  

/* A debug or utility routine which just reads all of the src and
   then prints it.
*/
void cbglish_lex_debug()
{
  builder *p = lishlex_list(0);
  fprintl(p);
}

/* Routine to establish the input stream from memory for lex analysis */
void lex_set_input_mem(unsigned char *start, unsigned char *end)
{
  filestack[filesp].inpoi = start;
  filestack[filesp].toks_start = filestack[filesp].inpoi;
  filestack[filesp].toks_end = end;
  filestack[filesp].need_ee_starter = g_need_ee_starter;
  newlinef = 1;
  lishch1 = lishmemget;
  nchar = lishch1();
  lokch = lishch();
  filestack[filesp].line_number = 1;
  TRC(printf("set_input_mem, first char %x\n", lokch));
}

#if 1
/* Old input routine for when not preprocessing */
unsigned char lishfget()
{
  if (filestack[filesp].inbytes == 0)
  {
    /*   size_t fread( void *ptr, size_t size, size_t nmemb, FILE *stream); */

    filestack[filesp].inbytes = fread(filestack[filesp].toks_start, 1, INBUFFERL, filestack[filesp].cis);
    filestack[filesp].inpoi = filestack[filesp].toks_start;
    filestack[filesp].toks_end = filestack[filesp].toks_start+filestack[filesp].inbytes;
    TRC(printf("Read %i src bytes\n", filestack[filesp].inbytes));
  }
  if (filestack[filesp].inbytes == 0) return (char) 0;
  filestack[filesp].inbytes--;
  return *filestack[filesp].inpoi++;
}
#else 

/* Input routine for when preprocessing. Preprocessor returns
 * null terminated lines but here we convert this to a null
 * terminated file
 */
unsigned char lishfget()
{
  char r;
  if (preprocessor_eoff()) return (char) 0;	
  r = *filestack[filesp].inpoi;
  if (r) 
    {
      filestack[filesp].inpoi ++;
      return r;
    }
  else
    {
      preprocessor_replenish();
      filestack[filesp].inpoi = preprocessor_buffer;
      return '\n';
    }     
}
#endif


int lex_set_input_file_name(const char *fn)
{
  const char *path = (incdirpath) ? incdirpath: getenv("ORANGEPATH");
  char line[1024];
  FILE *fd;
  strcpy(line, fn);
  fd = fpathopen_r(line, (!path) ? ".": path);
  if (!fd && strlen(line)> strlen(langsuffix) && strcmp(line+strlen(line)-strlen(langsuffix), langsuffix))
    {
      strcat(line, langsuffix);
      fd = fpathopen_r(line, (!path) ? ".": path);
    }
  if (!fd) 
    {
      perror("");

      printf("Input file not found %s", line);
      return 1;
    }

  //  printf("%s is the open file\n", fn);
  filestack[filesp].cis = fd;
  filestack[filesp].fn = fn;
  filestack[filesp].toks_start = malloc(INBUFFERL);
  filestack[filesp].toks_end = filestack[filesp].toks_start + INBUFFERL;
  filestack[filesp].need_ee_starter = g_need_ee_starter;
  newlinef = 1;
  filestack[filesp].line_number = 1;
  filestack[filesp].inbytes = 0;
  lishch1 = lishfget;
  nchar = lishch1();
  lokch = lishch();
  return 0;
}

static char scrbuf[20];
static builder *indxs[10];


builder *add_linepoint(char *contag, builder *arg)
{
  char line [132];
  const char *fn = filename;
  builder *lp;
  sprintf(line, "%i", filestack[filesp].line_number);
  lp = TREE2("LP", fillin_atom(ss_string, fn, strlen(fn)),  fillin_atom(ss_string, line, strlen(line)));
  return TREE2(contag, lp, arg);
}


/*
 * Return a unique atom with index string built in to its name. 
 */
builder *funique_indexed_string(char *string)
{
  int i = UNIQUE_MAX-2;
  strcpy(scrbuf, string);
  while(1)   /* Count in BCD */
  {
    if (unique_run[i] != '9')
    {
      unique_run[i] = 1 + unique_run[i];
      break;
    }
    unique_run[i] = '0';
    i -= 1;
  }
  strcat(scrbuf, unique_run);
  return str_to_atom(scrbuf);
}


builder *funique() /* Return a unique atom */
{
  return funique_indexed(nil) ;
}

builder *funique_indexed(builder *index)
{
  if (index)
    {
      return funique_indexed_string(atom_to_str(index));
    }
  else
    { 
      return funique_indexed_string("u");
    }
}

builder *cbglish_keyword(int domain, char *name, int symbol, int x, int y)
{
  TRC1(printf("Define keyword %s symno=%i\n", name, symbol));
  builder *v = str_to_atom(name);
  fputprop(v, smacro, mknumber(symbol));
  return v;
}

void cbglish_symbol(int domain, char *name, int symbol, int x, int y)
{
  builder *v = str_to_atom(name);
  TRC1(printf("Symbol define %s symno=%i\n", name, symbol));
  fputprop(v, smacro, mknumber(symbol));
}


void init_cbglish()
{
  int i;
  strcpy(unique_run, "10000");

  char_extensions = (struct char_extender **) malloc(256 * sizeof(struct char_extender));
  for (i = 0; i<256; i++)
  {
    struct char_extender *p = NEW(char_extender);
    memset(p, 0, sizeof(struct char_extender));
    char_extensions[i] = p;
    p->key = i;
  }

  for (i=0;   i<=255; i++)   char_exteristics[i] = c_badchar;
  for (i=33;  i<'A';  i++)  char_exteristics[i] = c_selfterminating;
  for (i='0'; i<='9'; i++)  char_exteristics[i] = c_selfextending+c_digit;
  for (i='A'; i<='Z'; i++)  char_exteristics[i] = c_selfextending;
  for (i='Z'+1;  i<'a';  i++)  char_exteristics[i] = c_selfterminating;
  for (i='a'; i<='z'; i++)  char_exteristics[i] = c_selfextending;
  for (i='z'+1; i<=126; i++)  char_exteristics[i] = c_selfterminating;
  char_exteristics['_'] = c_selfextending;
  char_exteristics['$'] = c_selfextending;

  if (ignorenewlines) char_exteristics['\n'] = c_white;
  else char_exteristics['\n'] = c_selfterminating;
  char_exteristics[' '] = c_white;
  char_exteristics[13] = c_white;
  char_exteristics['\t'] = c_white;
  char_exteristics['\"'] = c_dquote;
  char_exteristics[0] = c_delimiter;
  char_exteristics[255] = c_delimiter;
  init_scheme();

  ssentential  = str_to_atom("$S");
  srule  = str_to_atom("$R");
  slist = str_to_atom("$L");
  simport = str_to_atom("import");
  sstile = str_to_atom("|");
  swhite = str_to_atom("$WHITESPACE");
  sany   = str_to_atom("$ANY");
  scnewline = str_to_atom("$NEWLINE");
  scline = str_to_atom("$LINE");
  sunset = str_to_atom("$unset");
  shasht = str_to_atom("#t");
  sbuiltin = str_to_atom("$builtin");
  slambda = str_to_atom("lambda");
  smacro = str_to_atom("macro");
  sequals = str_to_atom("=");

  sproductions = str_to_atom("$PRODS");
  textualnil = str_to_atom("nil");
  c_defidx();
  //TRC1(printf("Init %i %s\n", __LINE__, __FILE__));
  define_lisp_prims();
  TRC1(printf("Init %i %s done\n", __LINE__, __FILE__));
}


void c_defidx()
{
  int i;
  for (i=0; i<10; i++)
  {
    scrbuf[0] = '$'; scrbuf[1] = i + '0'; scrbuf[2] = 0;
    indxs[i] = str_to_atom(scrbuf);
  }
}

int index_to_int(builder *p)
{
  int i;

  for (i=0; i<10; i++) 
    {
      if (p == indxs[i]) return i;
    }
  return -1;
}




void toc_fprintl(FILE *f, unsigned char *s, unsigned char *e, unsigned char *pt)
{
  fprintf(f, "Line %i:", filestack[filesp].line_number);
  while(s<= e)
  {
    if (s==pt) fprintf(f, "<-<<");
    putc(*s++, f);
  }
  fprintf(f, "\n");
}

/* LISP bits */



int  tab = 0;
int  term_width = 65;

builder * fdisplay(builder *x)
{
  /* printf("fdisplay %x\n", x); */
  fprintl(x);
  printf("\n");
  return x;
}

int pchars;
int pexitf;
#define PLIM 2000

builder * fprintl(builder *x)
{
  pchars = 0;
  pexitf = 0;
  tab = 0;
  print1(x);
  return x;
}

void print1(builder *x)
{
  if (pchars > PLIM || pexitf)
  {
    pexitf = 1;
    printf("...");
    return;
  }

  if (x == nil)
  {
    printf("()");
    tab = tab + 2;
    return;
  }

  if (0 && x < 0) 
  {
    printf("<%p>", x);
    return;
  }
  
  if (m_key(x) == ss_pair)
  {
    printf("(");
    tab ++;
    while (x && pexitf == 0)   /* S expression list element */
    {
      print1(fcar(x));
      if (m_ncdr(x) && pexitf == 0) 
      {
        printf(" "); tab++;
        if (m_ncar(fcdr(x)) < 0) /* if atom for cdr then a dotted pair */
        { 
          printf(". ");
          tab = tab + 2;
          print1(fcdr(x));
          break;
        }
      }
      x = fcdr(x);
    }

    printf(")");
    tab ++;
    return;
  }

  switch (m_key(x))
  {
  case ss_symbol:
  case ss_nmacro:
  case s_native0:
  case s_native1:
  case s_native2:
  case s_native3:
  case s_native4:
  case s_native5:
  case s_native6:
    tab = strlen(m_ccdr(x)) + tab + 1;
    printf("%s", m_ccdr(x));
    if (tab > term_width)
    { 
      pchars += tab;
      tab = 0;
      printf("\n");
    }
    return;
    
  case ss_string:
    tab = strlen(m_ccdr(x)) + tab + 1;
    printf("\"%s\" ", m_ccdr(x));
    return;

  case ss_dstring:
    tab = strlen(m_ccdr(x)) + tab + 1;
    printf("(\'%s\') ", m_ccdr(x));
    return;
    
  case ss_argument:
    printf("*%p ", m_ccdr(x));
    return;
    
  case ss_number:
    tab = tab + 5;
    printf("%d ", m_ncdr(x));
    return;
    
  default:
    printf("bad atom type %li", m_key(x));
    return;
  }
}


builder *falpha(builder *p)
{
  switch(m_key(p))
  {
  case ss_symbol:
  case ss_nmacro:
  case s_native0:
  case s_native1:
  case s_native2:
  case s_native3:
  case s_native4:
  case s_native5:
  case s_native6:
    if (isalpha((m_ccdr(p))[0]) || (m_ccdr(p)[0] == '_')) return shasht;
  }
  return nil;
}

builder *fcons(void *x, builder *y)
{
  builder *r = NEW(bldcons);
  r->key = ss_pair;
  r->car = x;
  r->cdr = y;
  SKIP(printf("%x->(%x,%x)\n", r, x, y));
  return r;
}


int length(builder *x)
{
  int r = 0;
  while (x) 
  {
    r++;
    x = fcdr(x);
  }
  return r;
}

builder *flength(builder *x)
{
  return mknumber(length(x));
}

builder *fnull(builder *x)   
{
  if (x == nil) return str_to_atom("#t");
  return nil;
}




#define MAXN 4096


struct anum **numbers = 0;

void init_scheme()
{
  int i;
  numbers = (struct anum **) malloc(sizeof(struct anum *) * MAXN);
  for(i=0;i<MAXN; i++) numbers[i] = NULL;
}

builder *mknumber(int s)
{
  int h = (s & (MAXN-1));
  struct anum *r = numbers[h];
  while(r) // nasty linear search over all numbers!
  {
    if (r->val == s) break;
    r = r->next;
  }
  if (r == nil)
  {
    r = (struct anum *) malloc(sizeof(struct anum));
    r->key = ss_number;
    r->val = s;
    r->next = numbers[h];
    numbers[h] = r;
  }
  return (builder *) r;
}


/* 
 * Return a list of 0, 1, X and Z forming a Verilog composite number 
 * ignores previous leading sign.
 * List is built up msb first, so is returned lsb first
 */
builder *composite_number(int base, int r, char c, int si)
{
  builder *l = nil;
  int m = 1;
  int i;
  int w = (base == 2) ? 1 : (base == 8) ? 3: 4;
  
  /* First convert partially read number into a list */
  for (i=1; i<si; i++) m = m << 1;
  for (i=0; i<si; i++)
    {
      builder *v = mknumber((r & m) ? 1 : 0);
      l = fcons(v, l);
      m = m >> 1;
    }
  /* Then continue reading input */

  m = (base == 2) ? 1 : (base == 8) ? 4: 8;
  while (1)
    {
      builder *v = nil;
      int n = -1;
      if (c == 'X') v = str_to_atom("x");
      else if (c == '_') 
	{
	  lokch = lishch();  
	  c = toupper(lokch);
	  continue;  /* Underscores are allowed in Verilog numbers */
	}      
      else if (c == 'Z' || c == '?') v = str_to_atom("z");
      else if (isdigit(c)) n = c - '0';
      else if (base == 16 && c >= 'A' && c <= 'F') n = c - ('A'-10);
      else break;
      
      if (v) for (i=0; i<w; i++) l = fcons(v, l);
      else for(i=0; i<w; i++)
	{
	  builder *d = mknumber((n & m) ? 1 : 0);
	  l = fcons(d, l);
	  m = m >> 1;
	}
      lokch = lishch();
      c = toupper(lokch);
    }
  return fcons(str_to_atom("composite"), l);
}


/*
 * Read a number from the source and convert to binary int
 */
builder *lishlex_number(int sign)
{
  int r = 0;     /* running total */
  int base = -1; /* 1, 8, 10 or 16 */
  int si = 0;    /* size of the digit in bits */
  sizednumf = 0; /* clear the lexical assist flag */

  while(1)
  {
    char c = toupper(lokch);
    if (c == '_') 
    {
      lokch = lishch();  
      continue;  /* Underscores are allowed in Verilog numbers */
    }      

    if (c == 'X' || c == 'H') if (base < 0)
    {
      base = 16;
      lokch = lishch();
      continue;
    }


    if (c == 'X' || c == 'Z' || c == '?') if (base > 0) return composite_number(base, r, c, si);

    if (base == 16 && c >= 'A' && c <= 'F')
    {
      r = r*base + c - ('A'-10);
      si += 4;
      lokch = lishch();
      continue;
    }

    if (c == 'O' && base < 0)
    {
      base = 8;
      lokch = lishch();
      continue;
    }

    if (c == 'B' && base < 0)
    {
      base = 2;
      lokch = lishch();
      continue;
    }
    if (c == 'D' && base < 0)
    {
      base = 10;
      lokch = lishch();
      continue;
    }

    if (isdigit(lokch))
    {
      int d = c - '0';
      if (d == 0 && base < 0) { lokch = lishch(); continue; }
      if (d > 0 && base < 0) base = 10;
      r = r*base + d;
      lokch = lishch();
      if (base == 2) si += 1;
      else if (base == 8) si += 3;
      else si += 4;
      if (d > base) printf("Bad digit %i in number base %i\n", d, base);   
    }
    else break;

  }
  if (sign < 0) r = -r;

  /* Check for verilog field specifications and note them here */
  if (lokch == '\'')
  {
    sizednumf = 1;
  }

  return mknumber(r);
}

builder *list(builder *r, ...)
{
  builder *b, *first;
  va_list ap;
  va_start(ap, r);
  
  b = first = fcons(r,nil);
  
  while((r = va_arg(ap, builder *)))
    {
      b->cdr = fcons(r,nil);
      b = b->cdr;
    }
  
  va_end(ap);
  return first;
}    



builder * lishlex_string()
{ 
  int tab = 0;
  lokch = lishch();
  while (lokch != '\"')
  {
    atomheart[tab++] = lokch;
    lokch = lishch();
    if (tab >= MAXATOML) break;
  }
  atomheart[tab] = (char) 0;
  if (tab == MAXATOML) cbgerror(0, "String too long, truncated\n>%s<", atomheart);
  lokch = lishch();
  TRC(printf("String in '%s' ", atomheart));
  return fillin_atom(ss_string, atomheart, tab);
}

builder *fillin_atom(key_t key, char * name, int len)
{
  builder *a = (builder *) NEW(atom);

  m_key(a) = key;
  m_ccdr(a) = malloc(len+1);
  strncpy(m_ccdr(a), name, len);
  m_ccdr(a)[len] = (char) 0;
  m_carval(a) = sunset;
  m_props(a) = nil;
  TRC1(printf("fillin_atom %x >%s<\n", a, name));
  return (builder *) a;
}


builder *fsymbolp(builder *a) // same as atomp but without nil?
{
  if (a == nil || (m_key(a) == ss_pair)) return nil;
  return shasht;
}

int check_an_atom(builder *a, char *fn)
{
  if (a == nil || m_key(a) == ss_pair)
  {
    printf("ca=%p ", a);
    fdisplay(a);
    cbgerror(cbge_msg, "%s called not on a symbolic atom", fn);
    reenter();
  }
  return 0;
}

int check_not_an_atom(builder *a, char *fn)
{
  if (a != nil && m_ncar(a) == ss_pair)
  {
    fdisplay(a);
    cbgerror(cbge_msg, "%s called on an atom", fn);
    reenter();
  }
  return 0;
}

builder *fputprop(builder *atom, builder *prop, builder *value)
{
  builder * a = (builder *) atom;
  builder *plist = m_props(a);
  check_an_atom(a, "putprop");
  m_props(a) = fcons(fcons(prop,value), plist);
  TRC(fdisplay(list4(str_to_atom("putprop"), atom, prop, value)));
  //TRC1(printf("putprop done\n"));

  return value;
}

builder *fgetprop(builder *atom, builder *prop)
{
  builder * a = (builder *) atom;
  builder *plist = m_props(a);
  check_an_atom(a, "getprop");
  while(plist)
  {
    if (fcar(fcar(plist)) == prop) return fcar(plist);
    plist = fcdr(plist);
  }
  return nil;
}

builder *list1(p) builder *p;
{
   return fcons(p, nil);
}

builder *list2(p, q) builder *p, *q;
{
  return fcons(p, fcons(q, nil));
}

builder *list3(o, p, q) builder *o, *p, *q;
{
  return fcons(o, fcons(p, fcons(q, nil)));
}

builder *list4(o, p, q, r) builder *o, *p, *q, *r;
{
  return fcons(o, fcons(p, fcons(q, fcons(r, nil))));
}

builder *list5(o, p, q, r, s) builder *o, *p, *q, *r, *s;
{
  return fcons(o, fcons(p, fcons(q, fcons(r, fcons(s, nil)))));
}

builder *list6(o, p, q, r, s, t) builder *o, *p, *q, *r, *s, *t;
{
  return fcons(o, fcons(p, fcons(q, fcons(r, fcons(s, fcons(t, nil))))));
}


builder *fassoc(builder *key, builder *list)
{
  while(list)
  {
    if (fcaar(list) == key) return fcar(list);
    list = fcdr(list);
  }
  return nil;
}

#define MAXLINE 1024
static unsigned char cmd[MAXLINE];   /* A buffer for interactive input strings */
static int firstline = 0;       /* A prompt modifying flag */

/* scheme/lisp input routine with prompt */
unsigned char interactive_lisch()
{
  unsigned char c;
  while (filestack[filesp].inpoi == NULL)
  {
    if (firstline) printf(" ->"); else printf("  =");
    filestack[filesp].inpoi = lineiolinein(stdin);
    if (filestack[filesp].inpoi == NULL) return nil;
  }
  firstline = 0;
  c = *filestack[filesp].inpoi ++;
  if (c == (char) 0) filestack[filesp].inpoi = NULL;
  return c;
}

/* This routine reads an input line consisting of one or more s-expressions
   in ascii and converts it into a lisp list.
*/
builder *ffread_one()
{
  builder * r = rr1();
  TRC(printf("rr1 gave"); fprintl(r); printf("\n"));
  while (lokch)
  {
    if (lokch)
    {
      return fcons(r, ffread_one());
    }
  }
  return fcons(r, nil);
}
 
/*
 * A useable lish routine for reading one s-expression from
 * interactive input.
 */
builder *firead()
{
  filestack[filesp].inpoi = NULL;
  firstline = 1;
  printf("Calling lishch\n");
  lishch1 = interactive_lisch;
  lokch = lishch();
  filestack[filesp].inpoi = NULL;
  return fcar(ffread_one());
  /* Should restore old input stream ? */
}

/* 
 * Read a list or sublist, terminating in a dotted pair or rparen 
 */
builder * read_slist()
{
  builder * ll;
  while (char_exteristics[lokch] & c_white) lokch = lishch();

  if (lokch == ':' && colons_not_in_lists)
  {
    printf("Last routine XX\n");
    cbgerror(0, "Colon encountered in a list");
  } 

  if (lokch == (char) 0)
  {
    cbgerror(0, "end of file in list");
  }
  if (lokch == ')') 
  {
    lokch = lishch();
    return nil; 
  }
  if (lokch == '.')
  { 
    lokch = lishch();
    ll = rr1();
    if (lokch != ')') cbgerror(e_ok, "bad dot syntax");
    lokch = lishch();
    return ll;
  }
  ll = rr1();
  return fcons(ll, read_slist());
}


builder *rr1()
{
  while (1) 
  {
    while (char_exteristics[lokch] & c_white) lokch = lishch();
    switch(lokch)
    {
    case (char) 0:
      return nil;   /* Return nil and do not advance lokch on eol */

    case '-':
      lokch = lishch();
      if (isdigit(lokch))
      {
        return lishlex_number(-1);
      }
      else return str_to_atom("-");



     case '(':
      lokch = lishch();
      return read_slist();

     case '\'':
      lokch = lishch();
      return list2(ss_quote, rr1());

     default:
       /* Read a single atom */

       return lishlex();

     case ')':  /* excess rpars ignored */
      lokch = lishch();
      continue;
    }
  }
}


unsigned char *lineiolinein(FILE *sysin)
{
  int tab = 0;
  while(1)
  {
    char c = getc(sysin);
    if (c == 4) return 0;  /* Return zero on eod symbol */
    if (c == '\n')
    {
      cmd[tab] = 0;
      putchar(c);
      return cmd;
    }
     if (c == 8 || c == 127)
     {
       if (tab == 0) continue;
       cmd[tab] = ' ';
       putchar(8); putchar(32); putchar(8); fflush(stdout);
       tab--;
       continue;
     }
     if (c >= ' ')
     {
       if (tab < MAXLINE) cmd[tab++] = c;
       putchar(c);
       fflush(stdout);
     }
  }
}

builder *fmemberp(builder *item, builder *p)
{
  while (p) 
  {
    if (fcar(p) == item) return item;
    p = fcdr(p);
  }
  return nil;
}

builder *fdeletemember(builder *item, builder *list)
{
  if (list == nil) return nil;
  if (item == fcar(list)) return fcdr(list);
  return fcons(fcar(list), fdeletemember(item, fcdr(list)));
}

builder *freplaca(builder **p, builder *q)
{
  *p = q;
  return q;
}

builder *freplacd(builder **p, builder *q)
{
  p[1] = q;
  return q;
}

builder *freplacboth(builder **old, builder *new)
{
  old[0] = new->car;
  old[1] = new->cdr;
  return new;
}



/*
 * Textually combine two atoms to give a new atom of longer name
 */
static char joinabuf[512];
builder *cjoinatoms(builder *x, builder *y, char *cs)
{
  if (fatomp(x)==nil || fatomp(y)==nil)
    cbgerror(-2, "joinatoms not on atoms", x, y);
  strcpy(joinabuf, m_ccdr(x));
  strcat(joinabuf, cs);
  strcat(joinabuf, m_ccdr(y));  
  return str_to_atom(joinabuf);
}

/* end of cbg cbglish.c */

