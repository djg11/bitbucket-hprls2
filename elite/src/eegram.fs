//
//
module eegram

type ee_dop_t =
    | EO_logand
    | EO_logor
    | EO_in
    | EO_dotaccess     
    | EO_deqd
    
type eegram_t =
//types
    | EE_usertype of string
    | EE_enum of eegram_t list
    | EE_uint
    | EE_bits of ee_exp_t
    | EE_bytes of ee_exp_t
    | EE_bool
    | EE_typequal of ee_type_t * ee_type_t


//expressions
    | EE_apply of ee_exp_t * ee_exp_t list
    | EE_id of string    
    | EE_num of int
    | EE_diadic of ee_dop_t * ee_exp_t * ee_exp_t

// synthcontrol
    | SD_id of string
    | SD_in
    
// declarations
    | EE_event_decl of string * ee_formals_t option * ee_event_t option
    | EE_unit_def of string * ee_formals_t option * ee_decl_t list
    | EE_typedef of string * ee_type_t
    | EE_struct_def of string * string option * ee_formals_t option * eegram_t list
    | EE_g_decl of ee_fieldflags_t list * string * ee_type_t * ee_flags list * ee_synthcontrol_t list
    | EE_ea of ee_ea_t * ee_event_t option * ee_exp_t list option
    | EE_extend1 of string * ee_decl_t list

and ee_flags =
    | EF_IS_INSTANCE
    
and ee_ea_t =
    | ED_expect
    | ED_assume
    
and ee_synthcontrol_t = eegram_t

and ee_type_t = eegram_t

and ee_decl_t = eegram_t

and ee_formals_t =
    | E_formal_lst of ee_formal_t list

and ee_formal_t = 
    | EE_fp of string * ee_type_t

and ee_event_t =
// events
    | EE_t_cycle
    | EE_t_and of ee_event_t * ee_event_t
    | EE_t_or  of ee_event_t * ee_event_t
    | EE_t_e of ee_exp_t    
    | EE_t_true of ee_event_t
    | EE_t_rise of ee_event_t
    | EE_t_fall of ee_event_t
    | EE_cycle
    | EE_ev_ex of ee_exp_t
    | EE_ev_ev of ee_event_t        
    | EE_ev_sampled of ee_event_t * ee_event_t    

and ee_exp_t = eegram_t

and ee_fieldflags_t = eegram_t


//eof
