/* lineno.h */




typedef struct filestack_s
{
  const char *fn;
  FILE *cis;
  int line_number;
  unsigned char *inpoi, *toks_start, *toks_end;
  int inbytes, need_ee_starter;
} FILESTACK;

extern int filesp;

extern FILESTACK filestack[];

/* eof */
