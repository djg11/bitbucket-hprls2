module spool
#lite "off"
open eegram;

(* Parse tree from ../examples/axi.ee *)

let eegram_ast =("../examples/axi.ee", 
[EE_typedef("abus_t", EE_typequal(EE_uint, EE_bits(EE_num(32)))); EE_typedef("dbus_t", EE_typequal(EE_uint, EE_bits(
        EE_num(32)))); EE_typedef("tagid_t", EE_typequal(EE_uint, EE_bits(EE_num(4)))); EE_typedef("burst_len_t", EE_typequal(EE_uint, EE_bits(
        EE_num(8)))); EE_typedef("burst_size_t", EE_enum([EE_id("BS_1"); EE_id("BS_2"); EE_id("BS_4"); EE_id("BS_8"); 
        EE_id("BS_16"); EE_id("BS_32"); EE_id("BS_64"); EE_id("BS_128")])); EE_typedef("burst_type_t", EE_enum([EE_id("BT_FIXED"); 
        EE_id("BT_INCR"); EE_id("BT_WRAP")])); EE_typedef("resp_t", EE_enum([EE_id("R_OKAY"); EE_id("R_EXOKAY"); EE_id("R_SLVERR"); 
        EE_id("R_DECERR")])); EE_struct_def("AXI_GlobalSignals", None, None, [EE_g_decl([], "CLK", EE_bool, [], [SD_id(
            "always"); SD_in]); EE_g_decl([], "RESET", EE_bool, [], [SD_id("always"); SD_in])]); EE_struct_def("AXI_ReadAddressChannel", None, 
    None, [EE_g_decl([], "ARID", EE_usertype("tagid_t"), [], [SD_in; SD_id("target")]); EE_g_decl([], 
        "ARLEN", EE_usertype("burst_len_t"), [], [SD_in; SD_id("target")]); EE_g_decl([], "ARSIZE", EE_usertype("burst_size_t"), [], 
        [SD_in; SD_id("target")]); EE_g_decl([], "ARBURST", EE_usertype("burst_type_t"), [], [SD_in; SD_id("target")]); 
      EE_g_decl([], "ARADDR", EE_usertype("abus_t"), [], [SD_in; SD_id("target")]); EE_g_decl([], "ARREADY", EE_bool, 
        [], [SD_id("out"); SD_id("target")]); EE_g_decl([], "ARVALID", EE_bool, [], [SD_in; SD_id(
            "target")])]); EE_struct_def("AXI_ReadDataChannel", None, None, [EE_g_decl([], "RDATA", EE_usertype("dbus_t"), [], [SD_id(
            "out"); SD_id("target")]); EE_g_decl([], "RID", EE_usertype("tagid_t"), [], [SD_id("out"); SD_id("target")]); 
      EE_g_decl([], "RRESP", EE_usertype("resp_t"), [], [SD_id("out"); SD_id("target")]); EE_g_decl([], "RLAST", 
        EE_bool, [], [SD_id("out"); SD_id("target")]); EE_g_decl([], "RREADY", EE_bool, [], [SD_in; 
          SD_id("target")]); EE_g_decl([], "RVALID", EE_bool, [], [SD_id("out"); SD_id("target")])]); EE_struct_def("AXI_WriteAddressChannel", 
    None, None, [EE_g_decl([], "AWID", EE_usertype("tagid_t"), [], [SD_in; SD_id("target")]); EE_g_decl(
        [], "AWLEN", EE_usertype("burst_len_t"), [], [SD_in; SD_id("target")]); EE_g_decl([], "AWSIZE", EE_usertype("burst_size_t"), 
        [], [SD_in; SD_id("target")]); EE_g_decl([], "AWBURST", EE_usertype("burst_type_t"), [], [SD_in; SD_id(
            "target")]); EE_g_decl([], "AWADDR", EE_usertype("abus_t"), [], [SD_in; SD_id("target")]); EE_g_decl([], "AWREADY", 
        EE_bool, [], [SD_id("out"); SD_id("target")]); EE_g_decl([], "AWVALID", EE_bool, [], [SD_in; 
          SD_id("target")])]); EE_struct_def("AXI_WriteDataChannel", None, None, [EE_g_decl([], "WDATA", EE_usertype("dbus_t"), [], [
          SD_in; SD_id("target")]); EE_g_decl([], "WID", EE_usertype("tagid_t"), [], [SD_in; SD_id("target")]); EE_g_decl(
        [], "WLAST", EE_bool, [], [SD_in; SD_id("target")]); EE_g_decl([], "WREADY", EE_bool, [], [
          SD_in; SD_id("target")]); EE_g_decl([], "WVALID", EE_bool, [], [SD_id("out"); SD_id("target")])]); EE_struct_def(
    "AXI_WriteResponseChannel", None, None, [EE_g_decl([], "BID", EE_usertype("tagid_t"), [], [SD_id("out"); SD_id(
            "target")]); EE_g_decl([], "BRESP", EE_usertype("resp_t"), [], [SD_id("out"); SD_id("target")]); EE_g_decl([], 
        "BREADY", EE_bool, [], [SD_in; SD_id("target")]); EE_g_decl([], "BVALID", EE_bool, [], [SD_id(
            "out"); SD_id("target")])]); EE_unit_def("read_port", None, [EE_g_decl([], "rac", EE_usertype("AXI_ReadAddressChannel"), [], []); 
      EE_g_decl([], "rdc", EE_usertype("AXI_ReadDataChannel"), [], []); EE_event_decl("e_rda", Some(E_formal_lst([EE_fp("addr", 
                EE_usertype("abus_t"))])), Some(EE_ev_sampled(EE_t_true(EE_t_e(EE_diadic(EO_logand, EE_diadic(EO_logand, EE_diadic(EO_dotaccess, EE_id("rac"), 
                      EE_id("ARREADY")), EE_diadic(EO_dotaccess, EE_id("rac"), EE_id("ARVALID"))), EE_diadic(EO_deqd, EE_diadic(EO_dotaccess, EE_id("rac"), 
                      EE_id("ARADDR")), EE_id("addr"))))), EE_t_rise(EE_t_e(EE_diadic(EO_dotaccess, EE_id("glob"), EE_id("CLK"))))))); EE_event_decl("e_rdd", 
        Some(E_formal_lst([EE_fp("data", EE_usertype("dbus_t"))])), Some(EE_ev_sampled(EE_t_true(EE_t_e(EE_diadic(EO_logand, EE_diadic(
                    EO_logand, EE_diadic(EO_dotaccess, EE_id("rac"), EE_id("RREADY")), EE_diadic(EO_dotaccess, EE_id("rdc"), EE_id("RVALID"))), EE_diadic(
                    EO_deqd, EE_diadic(EO_dotaccess, EE_id("rdc"), EE_id("RDATA")), EE_id("data"))))), EE_t_rise(EE_t_e(EE_diadic(EO_dotaccess, EE_id(
                    "glob"), EE_id("CLK")))))))]); EE_unit_def("write_port", None, [EE_g_decl([], "wac", EE_usertype("AXI_WriteAddressChannel"), [], []); 
      EE_g_decl([], "wdc", EE_usertype("AXI_WriteDataChannel"), [], []); EE_g_decl([], "wrc", EE_usertype("AXI_WriteResponseChannel"), [], []); 
      EE_event_decl("e_wda", Some(E_formal_lst([EE_fp("addr", EE_usertype("abus_t"))])), Some(EE_ev_sampled(EE_t_true(EE_t_e(EE_diadic(
                  EO_logand, EE_diadic(EO_logand, EE_diadic(EO_dotaccess, EE_id("wac"), EE_id("AWREADY")), EE_diadic(EO_dotaccess, EE_id("wac"), EE_id(
                        "AWVALID"))), EE_diadic(EO_deqd, EE_diadic(EO_dotaccess, EE_id("rac"), EE_id("AWADDR")), EE_id("addr"))))), EE_t_rise(EE_t_e(EE_diadic(
                  EO_dotaccess, EE_id("glob"), EE_id("CLK"))))))); EE_event_decl("e_wdc", Some(E_formal_lst([EE_fp("data", EE_usertype("dbus_t"))])), 
        Some(EE_ev_sampled(EE_t_true(EE_t_e(EE_diadic(EO_logand, EE_diadic(EO_logand, EE_diadic(EO_dotaccess, EE_id("wac"), EE_id("WREADY")), 
                    EE_diadic(EO_dotaccess, EE_id("wdc"), EE_id("WVALID"))), EE_diadic(EO_deqd, EE_diadic(EO_dotaccess, EE_id("wdc"), EE_id("WDATA")), 
                    EE_id("data"))))), EE_t_rise(EE_t_e(EE_diadic(EO_dotaccess, EE_id("glob"), EE_id("CLK"))))))); EE_event_decl("e_wbr", Some(E_formal_lst(
            [EE_fp("rest", EE_usertype("resp_t"))])), Some(EE_ev_sampled(EE_t_true(EE_t_e(EE_diadic(EO_logand, EE_diadic(EO_logand, EE_diadic(
                      EO_dotaccess, EE_id("wrc"), EE_id("BREADY")), EE_diadic(EO_dotaccess, EE_id("wrc"), EE_id("BVALID"))), EE_diadic(EO_deqd, EE_diadic(
                      EO_dotaccess, EE_id("wrc"), EE_id("BRESP")), EE_id("resp"))))), EE_t_rise(EE_t_e(EE_diadic(EO_dotaccess, EE_id("glob"), EE_id(
                    "CLK")))))))]); EE_unit_def("axi_port", None, [EE_g_decl([], "glob", EE_usertype("AXI_GlobalSignals"), [], []); EE_g_decl([], 
        "readside", EE_usertype("read_port"), [EF_IS_INSTANCE], []); EE_g_decl([], "writeside", EE_usertype("read_port"), [EF_IS_INSTANCE], [])]); 
  EE_unit_def("axi_relay", None, [EE_g_decl([], "port0", EE_usertype("axi_port"), [EF_IS_INSTANCE], [SD_id("target")]); 
      EE_g_decl([], "port1", EE_usertype("axi_port"), [EF_IS_INSTANCE], [SD_in; SD_id("initiator")]); EE_event_decl("fwd_read", None, 
        Some(EE_t_and(EE_ev_ex(EE_apply(EE_diadic(EO_dotaccess, EE_diadic(EO_dotaccess, EE_id("port0"), EE_id("readside")), EE_id("e_rda")), 
                [EE_id("trans_addr")])), EE_t_and(EE_ev_ex(EE_apply(EE_diadic(EO_dotaccess, EE_diadic(EO_dotaccess, EE_id("port0"), EE_id("readside")), 
                    EE_id("e_rdd")), [EE_id("trans_data")])), EE_t_and(EE_ev_ex(EE_apply(EE_diadic(EO_dotaccess, EE_diadic(EO_dotaccess, EE_id("port1"), 
                        EE_id("readside")), EE_id("e_rda")), [EE_id("trans_addr")])), EE_ev_ex(EE_apply(EE_diadic(EO_dotaccess, EE_diadic(EO_dotaccess, EE_id(
                          "port1"), EE_id("readside")), EE_id("e_rdd")), [EE_id("trans_data")]))))))); EE_event_decl("fwd_read_goal", None, Some(EE_t_or(EE_ev_ex(
              EE_id("fwd_read")), EE_t_cycle))); EE_event_decl("axi_relay_goals", None, Some(EE_ev_ex(EE_id("fwd_read_goal"))))]); EE_extend1("sys", [EE_g_decl(
        [], "relay", EE_usertype("axi_relay"), [], []); EE_ea(ED_expect, Some(EE_ev_ex(EE_diadic(EO_dotaccess, EE_id("relay"), 
              EE_id("axi_relay_goals")))), None)])]);







(* eof *)
