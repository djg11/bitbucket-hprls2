//
// Kiwi Scientific Acceleration: KiwiC compiler test/demo.
// test75 - Linq EnumerableRange

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KiwiSystem;


    /*
 This code produces the following output:

 1
 4
 9
 16
 25
 36
 49
 64
 81
 100
 */


class test75
{
    static int toplimit = 10;

    static void runtest()
    {
      // Generate a sequence of integers from 1 to 10
      // and then select their squares.

      IEnumerable<int> baserange = Enumerable.Range(1, toplimit);
      IEnumerable<int> squares = baserange.Select(x => x * x);

      foreach (int num in squares)
      {
          Console.WriteLine(num-1);
      }

    }

    [Kiwi.HardwareEntryPoint()]
    public static void Main()
    {
        int jojo;
 	Console.WriteLine("Test 1 Limit=" + toplimit);
        runtest();
     	Console.WriteLine(" Test 75 finished.");
	Kiwi.Pause();
    }
}

// eof



