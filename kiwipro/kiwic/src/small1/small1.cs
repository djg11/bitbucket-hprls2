// $Id: small1.cs,v 1.2 2013/02/08 10:11:07 djg11 Exp $
//

using System;
using KiwiSystem;

class small1
{


  static int counter = 0;
  static int dropper = 0;
  const int limit = 1024 * 1024;

  static int [] BigArray = new int [limit];

  //  [Kiwi.OutputWordPort(11, 0, "bitwiser")]   public static int[] bitwise = new int [12];

  [Kiwi.InputWordPort(11, 0, "wordwise")]   public static uint wordwise;
                    
  static void ThisShouldBeIgnored() // This method is not converted to hardware.
  {
     dropper = 400;
  
  }

  static void structsort(int [] data)
  {
    for (int i=0; i<limit; i++)
      for (int j=0; j<limit-1; j++)
	{
	  if (data[i] > data[i+1]) 
	    {
	      int t = data[i] / 33;
	      data[i+1] = data[i];
	      data[i] = t;
	    }
	}
  }

  static void TopLevel ()
  {
     int bitvec = 1234567;
     Console.WriteLine("Hello World. Wordwise={0}", wordwise);
     for (int j=0; j<20; j++)
     {
       bool f = (bitvec & 1) > 0;
       if (f) counter += 1;
       Console.WriteLine("{2}: Bitvec is {0} Counter is {1}", bitvec, counter, dropper);
       bitvec = bitvec / 2;
       dropper += 202;
       Kiwi.Pause();
     }
  }

  // This will be a root for hardware compilation using the Kiwi.Hardware() attribute.
  [Kiwi.HardwareEntryPoint()]
  public static void EntryPoint() 
  {
    Console.WriteLine("Hardware thread should start here.");
    TopLevel();
    Console.WriteLine("Doing the sort");
    structsort(BigArray);
    Console.WriteLine("Hardware thread should terminate here.");
  }

  public static int Main()
  {
    Console.WriteLine("Software execution under mono should start here.");
    // NB: this method is not included in the hardware compilation using KiwiC.
    TopLevel();
    return 0;
  }


}

// eof
