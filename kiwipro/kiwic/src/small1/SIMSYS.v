
// $Id: SIMSYS.v,v 1.3 2013/02/08 10:11:07 djg11 Exp $
//

//

module SIMSYS();
   reg clk, reset;
   initial begin reset = 1; clk = 0; # 400 reset = 0; end
   always #100 clk = !clk;
   small1 dut(.clk(clk), .reset(reset));



   initial # 1000000 $finish();
endmodule
// eof
