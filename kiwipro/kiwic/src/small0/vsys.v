
// $Id: vsys.v,v 1.1 2010/09/07 17:50:08 djg11 Exp $
//

//

module SIMSYS();
   reg clk, reset;
   initial begin reset = 1; clk = 0; # 400 reset = 0; end
   always #100 clk = !clk;
   DUT dut(.clk(clk), .reset(reset));

   initial # 1000000 $finish();
endmodule
// eof


