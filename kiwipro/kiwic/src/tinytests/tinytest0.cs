// Kiwi Scientific Acceleration


// Basic hello-world like test file.


using System;
using System.Text;
using KiwiSystem;

using System.Runtime.InteropServices;

class Pipper
{
  public static int fred = 3;
}

class BarClass
{
  
  static Pipper bazzer;

  public int getit(int bb)
  {
    Pipper.fred += bb;
    return Pipper.fred;
  }
}

class tinytest0
{
  [Kiwi.InputWordPort("testin")] static int testin;
  [Kiwi.OutputWordPort("testout")] static int testout;

  [Kiwi.HardwareEntryPoint()]
  [Kiwi.ClockDom("clockTwo", "rstTwo", "clockPolarity=neg")]  
  public static void HWMain() // Testbench - with args
  {
        Console.WriteLine("Test tinytest0 finished.");
// 	testout = testin + 1;
  }


  public static int Main()
  {
    Console.WriteLine("Test tinytest0 started.");
    Kiwi.Pause();
    BarClass bc = new BarClass();

    // Strength-reduction demonstration
    for (int pp = 0; pp<5; pp++)
    {
      Kiwi.NoUnroll(); // For demo purposes, stop this from just getting unrolled since it is so simple!

      // Strength reduction should convert this multiply to an add.
      Console.WriteLine(" runner {0}", bc.getit(pp*111));
    }

    Environment.Exit(32);
    Console.WriteLine("Test tinytest0 finished.");
    Kiwi.Pause();
    return 42;
  }  
}

// eof



