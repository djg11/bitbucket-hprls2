//
// Kiwi Scientific Acceleration  
// University of Cambridge, Computer Laboratory
//
// vsys.v - A test wrapper for simulating very simple tests with clock and reset.
//
`timescale 1ns/10ps

module SIMSYS();

   reg clk, reset;
   initial begin reset = 1; clk = 1; #33 reset = 0; end
   always #5 clk = !clk;

   initial begin # (100 * 1000) $display("Finish HDL simulation on timeout %t.", $time); $finish(); end

   initial begin $dumpfile("vcd.vcd"); $dumpvars(); end

   DUT the_dut(.clk(clk), .reset(reset));

endmodule
// eof


