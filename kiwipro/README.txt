Kiwi Scientific Acceleration of C# on FGPA
------------------------------------------

# All rights reserved. (C) 2007-17, DJ Greaves, University of Cambridge, Computer Laboratory.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met: redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer;
# redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution;
# neither the name of the copyright holders nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

-----------------------------------------------------------------------------------------

Kiwi Project:  DJ Greaves and S Singh.

Compilation of parallel programs written in C# to H/W for ASIC embedded accelerators or on FPGA.

(C) 2007-17 DJ Greaves, University of Cambridge. 
(C) 2007 Microsoft Research.

-----------------------------------------------------------------------------------------

KiwiC : Kiwi Project Compiler (C) 1999-2009 DJ Greaves.
The following message is rescinded as of January 2007's open-source release:

   Old message : All of the IP and software in this directory and its subdirectories
   belongs to David Greaves of the University of Cambridge.  It cannot be
   copied, distributed or used commercially without explicit permission.


Please see manual (PDF and HTML):
  http://www.cl.cam.ac.uk/research/srg/han/hprls/orangepath/kiwic.html

-----------------------------------------------------------------------------------------

RELEASE NOTES - these need a good tidy up Dec 2016 pre the first release


The following features/facilities are currently still not mature and are all being fixed December 2016:

  Pipelined Accelerator Mode:

  C# dynamic method dispatch

  kiwic-autodispose pseudo garbage collection
 
  repack-roms and rom mirroring

  null-pointer traps

  C# checked instruction traps
  
  AXI DRAM connection

  AXI-Streaming accelerator 

  Cleaner top-level arguments, Multi-FPGA Designs and Incremental Compilation

  C# Structs for packet parsing and in general (C# Classes are fine)

  Soft pause in critical region

  Function calls with varargs - do they exist in C#?

  Recursive function handling, where recursion depth determined at compile time  - has stopped working.

-----------------------------------------------------------
RELEASE NOTES: There is no stable release so far.

-----------------------------------------------------------
RELEASE NOTES: Version alpha 0.2.17b : 1st-December-2016"


-----------------------------------------------------------

-----------------------------------------------------------
October 2011:
  dual-porting attributes
  pass by reference top-level args

  banks test in fftbench ct_valuetype DT error

-----------------------------------------------------------
Features missing in this release:
  tail calls
  boxing of native leaf types, such as integer and bool
  clk domain association - no its added ?

  I/O terminals that are arrays in vhdl style need better scalarizing


-----------------------------------------------------------
Added in "HPR Orange IL/.net front end: Version alpha 14 : 15-May-08\n"	
  Two-dimensional arrays
  Setting width of nets and ports HwWidth attribute
  Remote procedure call stubs  - 4-phase only version: Remote attribute.
  LTL/CTL compiler - first fragments: 


Added in "HPR Orange IL/.net front end: Version alpha 8 : ?Feb-08\n"	
  call by reference ?
  enums (only partially: still some work to do)
  local inits
  Method overloading has been tested.
  Finite state machine output as a Verilog case statement



OTHER POINTS


END

--- ------------------------------------------
