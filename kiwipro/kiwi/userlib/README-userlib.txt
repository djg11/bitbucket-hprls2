
userlib - Library Files for KiwiC use that replace the standard Windows/Mono implementations.

The standard implementations are either incompatible, too large or inapproprate for Kiwi use.  They can be inappropriate
if they assume the O/S and filesystem and RTC services are all conventionally available rather than accessed via
a substrate gateway.

In this folder we have the convention of storing the .dll binaries as well as the source files.

This is in case Windows users have a problem generating some of the .dll files whereas mono mcs makes them ok.

We will add the IP-XACT and .v files for some libraries for incremental compilation use to kickoff-libraries.

4Q18: KiwiC is now able to dynamically lookup and load these libraries instead of users having to list them on the KiwiC command line.  Aliases are sometimes used in the process (Kiwi Library Substitutions).



For many standard dot net libraries, the normal (mono or MS) release
of them is not suitable for use with KiwiC.  So KiwiC needs to
substitute alternate versions.  There are two main forms of
alternative: they may be implemented in C\# (or CIL assembler)
themselves, or may be RTL modules from an IP library.  So, on a
case-by-case basis, the decision for either a high-level (C\#) swap or
a low-level (RTL) swap needs to be made.

There are three ways a stateless FU (IP-block) such as Math.Sqrt can be deployed:

1. Inlined. The C\# src code for the sqrt is inlined by KiwiC and the ALUs it uses are borrowed from or shared with other parts of the application.

2. Fenced. The square-root FU is compiled to an RTL module containing its own private resources, the FU is instantiated one or more times and these FU instances are shared as per other stateless FUs and and ALUs.

3. Swapped. Again an RTL module is instantiated and ringfenced, but a  back-end substitution is made where the body of the C# implementation is ignored and a foreign RTL implementation of the SQRT FU is deployed and sharable.



// EOF
