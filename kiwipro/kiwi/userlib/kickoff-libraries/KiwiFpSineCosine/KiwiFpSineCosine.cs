//
// Kiwi Scientific Acceleration.
// DJ Greaves. University of Cambridge, Computer Laboratory.
//

using float64 = System.UInt64;
using System;
using KiwiSystem;

// Should be in namespace KiwiSystem.Math or something, going forward, with an auto-redirect in g_hardcoded_library_substitutions or loaded recipe xml.

namespace HprlsMathsPrimsCrude
{
   class hutils
   {

  /* Adding the FastBitConvert attribute makes KiwiC
    ignore the bodies of functions such as these and replaces the body
    with its own fast-path identity code based only on the signatures of the functions. */

       [Kiwi.FastBitConvert()]
       public static double to_double(float64 farg)
       {
         byte [] asbytes = BitConverter.GetBytes(farg);
         double rr = BitConverter.ToDouble(asbytes, 0);
         return rr;
       }

       [Kiwi.FastBitConvert()]
       public static float64 from_double(double darg)
       {
         byte [] asbytes = BitConverter.GetBytes(darg);
         return BitConverter.ToUInt64(asbytes, 0);
       }

       [Kiwi.FastBitConvert()]
       public static float to_single(uint farg)
       {
         byte [] asbytes = BitConverter.GetBytes(farg);
         float rr = BitConverter.ToSingle(asbytes, 0);
         return rr;
       }

       [Kiwi.FastBitConvert()]
       public static uint from_single(float sarg)
       {
         byte [] asbytes = BitConverter.GetBytes(sarg);
         return BitConverter.ToUInt32(asbytes, 0);
       }

   }


   public class KiwiFpSineCosine
   {
       const bool rounding_enabled = false;


/*
 * ====================================================
 * Copyright (C) 1993 by Sun Microsystems, Inc. All rights reserved.
 *
 * Developed at SunPro, a Sun Microsystems, Inc. business.
 * Permission to use, copy, modify, and distribute this
 * software is freely granted, provided that this notice
 * is preserved.
 * ====================================================
 */

/* __kernel_sin( x, y, iy)
 * kernel sin function on [-pi/4, pi/4], pi/4 ~ 0.7854
 * Input x is assumed to be bounded by ~pi/4 in magnitude.
 * Input y is the tail of x.
 * Input iy indicates whether y is 0. (if iy=0, y assume to be 0).
 *
 * Algorithm
 *	1. Since sin(-x) = -sin(x), we need only to consider positive x.
 *	2. if x < 2^-27 (hx<0x3e400000 0), return x with inexact if x!=0.
 *	3. sin(x) is approximated by a polynomial of degree 13 on
 *	   [0,pi/4]
 *		  	         3            13
 *	   	sin(x) ~ x + S1*x + ... + S6*x
 *	   where
 *
 * 	|sin(x)         2     4     6     8     10     12  |     -58
 * 	|----- - (1+S1*x +S2*x +S3*x +S4*x +S5*x  +S6*x   )| <= 2
 * 	|  x 					           |
 *
 *	4. sin(x+y) = sin(x) + sin'(x')*y
 *		    ~ sin(x) + (1-x*x/2)*y
 *	   For better accuracy, let
 *		     3      2      2      2      2
 *		r = x *(S2+x *(S3+x *(S4+x *(S5+x *S6))))
 *	   then                   3    2
 *		sin(x) = x + (S1*x + (x *(r-y/2)+y))
 */

     


     const double half =  5.00000000000000000000e-01, /* 0x3FE00000, 0x00000000 */
       S1  = -1.66666666666666324348e-01, /* 0xBFC55555, 0x55555549 */
       S2  =  8.33333333332248946124e-03, /* 0x3F811111, 0x1110F8A6 */
       S3  = -1.98412698298579493134e-04, /* 0xBF2A01A0, 0x19C161D5 */
       S4  =  2.75573137070700676789e-06, /* 0x3EC71DE3, 0x57B1FE7D */
       S5  = -2.50507602534068634195e-08, /* 0xBE5AE5E6, 0x8A2B9CEB */
       S6  =  1.58969099521155010221e-10; /* 0x3DE5D93A, 0x5ACFD57C */

     public static double KiwiSineDouble(double x, double y, int iy)
     {
       double z, r, v;

       float64 ixx = hutils.from_double(x);
       uint ix = (uint)(ixx >> 32); 
       //GET_HIGH_WORD(ix,x); // high word of x
       ix &= 0x7fffffff;			
       if(ix<0x3e400000)			// |x| < 2**-27 
         { if ((int)x==0) return x; }		// generate inexact
       z	=  x*x;  // Squared
       v	=  z*x;  // Cubed
       r	=  S2+z*(S3+z*(S4+z*(S5+z*S6)));
       Console.WriteLine("Sine Double Quad={0}, v={1}, r={2}", iy, v, r);
       if(iy==0)
         {
           double sub = (S1+z*r);
           Console.WriteLine("  sub {0}", sub);

           double prod =v*sub;
           Console.WriteLine("  prod {0}", prod);
           double ans = x+prod;
           Console.WriteLine("  ans {0}", ans);
           return ans;
         }
       else return x-((z*(half*y-v*r)-y)-v*S1); // Two multiplies in parallel - one could be done as an exponent add.
     }
   
     
       public static double KiwiSineDouble_(double x_in)
       {
           float64 ix = hutils.from_double(x_in);

           // Double-Precision:  IEEE 1+11+52 = 64-bit format.
           bool in_sign = ((ix >> 63) & 1L) == 1L;
           int in_exponent = (int)((ix >> 52) & 0x7FFL);
           ulong in_mantissa = ix & ((1L<<52)-1L);
           bool in_zero = (in_exponent == 0) && (in_mantissa == 0);
           bool in_denormal = (in_exponent == 0) && (in_mantissa != 0);
           in_zero |= in_denormal; // To save gates, discard denormals.
           //Console.WriteLine("DP in s={0} exp={1} m={2}", in_sign, in_exponent, in_mantissa); 
           int out_exponent = 0;
           bool out_sign = false;
           bool out_NaN = false;
           bool out_Inf = false;
           bool out_zero = false;
           ulong res = 0uL;

           if (in_exponent == 0x7ff)
             {
                if (in_mantissa != 0 || in_sign) out_NaN = true;  // Non-zero mantissa is NaN. Zero mantissa is +/- inf.
                else
                    { out_Inf = true; out_sign = in_sign;  }
             }
           else if (in_zero)
            {
              out_zero = true;
            }
          else
            {
            }        
          float64 f64_out;
          // Double-Precision:  IEEE 1+11+52 = 64-bit format.
          if (out_NaN) f64_out = 0x7FFF0000FFFF0000;
          else if (out_zero) f64_out = 0x0;
          else if (out_Inf) f64_out = ((out_sign) ? (1uL << 63) : 0uL) | 0x7FF000000000000uL;
          else  f64_out = ((out_sign) ? (1uL << 63) : 0uL) | (((ulong)out_exponent) << 52) | res;
	  // Console.WriteLine("sqrt out_zero={0}  out_NaN={1}  out_Inf={2}", out_zero, out_NaN, out_Inf);
          // Console.WriteLine("sqrt. f64_out=0x{0:x}", f64_out);
          return hutils.to_double(f64_out);
       }


     public static float KiwiSineSingle(float x)
     {
       return 1.23456f;
     }


     public static float KiwiSineSingle_(float x)
     {
       uint ix = hutils.from_single(x);
           // Single-Precision:  IEEE 1+8+23 = 32-bit format.
           bool in_sign = ((ix >> 31) & 1) == 1;
           int in_exponent = (int)((ix >> 23) & 0xFF);
	   uint in_mantissa = ix & ((1u<<23)-1u);
           bool in_zero = (in_exponent == 0) && (in_mantissa == 0);
           bool in_denormal = (in_exponent == 0) && (in_mantissa != 0);
           in_zero |= in_denormal; // To save gates, discard denormals.
           //           Console.WriteLine("S/P in s={0} exp={1} m={2}", in_sign, in_exponent, in_mantissa); 
           int out_exponent = 0;
           bool out_sign = false;
           bool out_NaN = false;
           bool out_Inf = false;
           bool out_zero = false;
           uint res = 0u;

           if (in_exponent == 0xff)
             {
                if (in_mantissa != 0 || in_sign) out_NaN = true;  // Non-zero mantissa is NaN. Zero mantissa is +/- inf.
                else
                    { out_Inf = true; out_sign = in_sign;  }
             }
           else if (in_zero)
            {
              out_zero = true;
            }
          else
            {
              
            }
          uint f32_out;
          // Single-Precision:  IEEE 1+8+23 = 32-bit format.
          if (out_NaN) f32_out = 0x7FFF0000u;
          else if (out_zero) f32_out = 0u;
          else if (out_Inf) f32_out = ((out_sign) ? (1u << 31) : 0u) | 0x7F80000u;
          else  f32_out = ((out_sign) ? (1u << 31) : 0u) | (((uint)out_exponent) << 23) | res;
	  // Console.WriteLine("sqrt out_zero={0}  out_NaN={1}  out_Inf={2}", out_zero, out_NaN, out_Inf);
          // Console.WriteLine("sqrt. f32_out=0x{0:x}", f32_out);
          return hutils.to_single(f32_out);
       }


       /*
        * wrapper sine and cosine(x)
        */
       [Kiwi.Remote("protocol=HFAST1;reftran=true;mirrorable=true;overloaded=true;inhold=false")]
       public static double Sin(double xd)
       {
         return KiwiSineDouble(xd, 0.0, 0);
       }


       //[Kiwi.Remote("protocol=HFAST1;reftran=true;mirrorable=true;overloaded=true;inhold=false")]
       public static float Sin(float xs)
       {
         return KiwiSineSingle(xs);
       }

   }
}

// eof
