//
// Kiwi Scientific Acceleration.
// DJ Greaves. University of Cambridge, Computer Laboratory.
//

// KiwiFpSqRoot.cs
// Single and double precision square root. (C) DJ Greaves, University of Cambridge Computer Laboratory. January 2010.
// Based on old C code from Sun Microsystems.

// This file is pre-compiled as part of the Kiwi distribution library and is represented by a .v  and some .xml forms ready for incremental inclusion.
// Of course it can be recompiled with various levels of parallelism, at the expense of more area.  56 clocks for DP is slowest sensible.



/*
 * ====================================================
 * Copyright (C) 1993 by Sun Microsystems, Inc. All rights reserved.
 *
 * Developed at SunPro, a Sun Microsystems, Inc. business.
 * Permission to use, copy, modify, and distribute this
 * software is freely granted, provided that this notice
 * is preserved.
 * ====================================================
 */

/* __ieee754_sqrt(x)
 * Return correctly rounded sqrt.
 *           ------------------------------------------
 *	     |  Use the hardware sqrt if you have one |
 *           ------------------------------------------
 * Method:
 *   Bit by bit method using integer arithmetic. (Slow, but portable)
 *   1. Normalization
 *	Scale x to y in [1,4) with even powers of 2:
 *	find an integer k such that  1 <= (y=x*2^(2k)) < 4, then
 *		sqrt(x) = 2^k * sqrt(y)
 *   2. Bit by bit computation
 *	Let q  = sqrt(y) truncated to i bit after binary point (q = 1),
 *	     i							 0
 *                                     i+1         2
 *	    s  = 2*q , and	y  =  2   * ( y - q  ).		(1)
 *	     i      i            i                 i
 *
 *	To compute q    from q , one checks whether
 *		    i+1       i
 *
 *			      -(i+1) 2
 *			(q + 2      ) <= y.			(2)
 *     			  i
 *							      -(i+1)
 *	If (2) is false, then q   = q ; otherwise q   = q  + 2      .
 *		 	       i+1   i             i+1   i
 *
 *	With some algebric manipulation, it is not difficult to see
 *	that (2) is equivalent to
 *                             -(i+1)
 *			s  +  2       <= y			(3)
 *			 i                i
 *
 *	The advantage of (3) is that s  and y  can be computed by
 *				      i      i
 *	the following recurrence formula:
 *	    if (3) is false
 *
 *	    s     =  s  ,	y    = y   ;			(4)
 *	     i+1      i		 i+1    i
 *
 *	    otherwise,
 *                         -i                     -(i+1)
 *	    s	  =  s  + 2  ,  y    = y  -  s  - 2  		(5)
 *           i+1      i          i+1    i     i
 *
 *	One may easily use induction to prove (4) and (5).
 *	Note. Since the left hand side of (3) contain only i+2 bits,
 *	      it does not necessary to do a full (53-bit) comparison
 *	      in (3).
 *   3. Final rounding
 *	After generating the 53 bits result, we compute one more bit.
 *	Together with the remainder, we can decide whether the
 *	result is exact, bigger than 1/2ulp, or less than 1/2ulp
 *	(it will never equal to 1/2ulp).
 *	The rounding mode can be detected by checking whether
 *	huge + tiny is equal to huge, and whether huge - tiny is
 *	equal to huge for some floating point number "huge" and "tiny".
 *
 * Special cases:
 *	sqrt(+-0) = +-0 	... exact
 *	sqrt(inf) = inf
 *	sqrt(-ve) = NaN		... with invalid signal
 *	sqrt(NaN) = NaN		... with invalid signal for signaling NaN
 *
 * Other methods : see the appended file at the end of the program below.
 *---------------
 */

using float64 = System.UInt64;
using System;
using KiwiSystem;

// Should be in namespace KiwiSystem.Math or something, goint forward, with an auto-redirect in g_hardcoded_library_substitutions or loaded recipe xml.

namespace HprlsMathsPrimsCrude
{
   class hutils
   {

  /* Adding the FastBitConvert attribute makes KiwiC
    ignore the bodies of functions such as these and replaces the body
    with its own fast-path identity code based only on the signatures of the functions. */

       [Kiwi.FastBitConvert()]
       public static double to_double(float64 farg)
       {
         byte [] asbytes = BitConverter.GetBytes(farg);
         double rr = BitConverter.ToDouble(asbytes, 0);
         return rr;
       }

       [Kiwi.FastBitConvert()]
       public static float64 from_double(double darg)
       {
         byte [] asbytes = BitConverter.GetBytes(darg);
         return BitConverter.ToUInt64(asbytes, 0);
       }

       [Kiwi.FastBitConvert()]
       public static float to_single(uint farg)
       {
         byte [] asbytes = BitConverter.GetBytes(farg);
         float rr = BitConverter.ToSingle(asbytes, 0);
         return rr;
       }

       [Kiwi.FastBitConvert()]
       public static uint from_single(float sarg)
       {
         byte [] asbytes = BitConverter.GetBytes(sarg);
         return BitConverter.ToUInt32(asbytes, 0);
       }

   }



   public class KiwiFpSqRoot
   {
       const bool rounding_enabled = false;

       public static double KiwiSqRootDouble(double x_in)
       {
           float64 ix = hutils.from_double(x_in);

           // Double-Precision:  IEEE 1+11+52 = 64-bit format.
           bool in_sign = ((ix >> 63) & 1L) == 1L;
           int in_exponent = (int)((ix >> 52) & 0x7FFL);
           ulong in_mantissa = ix & ((1L<<52)-1L);
           bool in_zero = (in_exponent == 0) && (in_mantissa == 0);
           bool in_denormal = (in_exponent == 0) && (in_mantissa != 0);
           in_zero |= in_denormal; // To save gates, discard denormals.
           //Console.WriteLine("DP in s={0} exp={1} m={2}", in_sign, in_exponent, in_mantissa); 
           int out_exponent = 0;
           bool out_sign = false;
           bool out_NaN = false;
           bool out_Inf = false;
           bool out_zero = false;
           ulong res = 0uL;

           if (in_exponent == 0x7ff)
             {
                if (in_mantissa != 0 || in_sign) out_NaN = true;  // Non-zero mantissa is NaN. Zero mantissa is +/- inf.
                else
                    { out_Inf = true; out_sign = in_sign;  }
             }
           else if (in_zero)
            {
              out_zero = true;
            }
          else
            {
               ulong num = in_mantissa |(0x00100000uL<<32); // NB: re-insert hidden bit.
               int mm = in_exponent;     // Use m to denote exponent (fools!)
               mm -= 1023;               // Unbias exponent
               //Console.WriteLine("  db D/P sqrt exponent={0} mantissa={1:X}", mm, num);
               if ((mm&1) != 0)
                 {                	 // Odd exponent, double num to make it even.
                     num <<= 1;
                 }
               mm >>= 1;                 // Answer exponent is half target exponent.
               // Bit-recursive sqrt function on mantissa (note this is different from integer square root where r>>=2 per iteration)
               num <<= 1;
               ulong s0 = 0uL;
               Kiwi.Pause();
               for (ulong r = 1uL<<53; r != 0uL; r >>= 1)  // r = moving bit from right to left.
                 { 
                   ulong t = s0+r;
                   //Console.WriteLine(" D/P iterate num={0}  t={1}  r={2:x}", num, t, r);
                   if (num >= t) 
                   {
                     s0   = t+r;
                     num  -= t;
                     res |=  r; //   Console.WriteLine("accum {0:x} {1:x}", r, res);
                   }
                   num <<= 1;
                   Kiwi.Pause();
                 }
               Kiwi.Pause();
               /*

         Note that root two is, S/P 0x03fb504f3  = 1068827891      =      1.414214e+00 SINGLE

             if (rounding_enabled)
              {
                 // Use a full floating addder to find out rounding direction !
                 const double one = 1.0, tiny = 1.0e-300;
                 if (in_mantissa!=0) { // Code not checked post djg edits!
                  double z = one-tiny; // trigger inexact flag 
                  if (z>=one) {
                         z = one+tiny;
                         if ((uint)q1==0xffffffffu) { q1=0; q += 1;}
                         else if (z>one) {
                             if (q1==(uint)0xfffffffeU) q+=1;
                             q1+=2;
                         } else
                             q1 += (q1&1);
                     }
                 }
*/

               res >>= 1;
               res &= ((1uL << 52)-1uL);   // Mask off the hidden bit
               out_exponent = mm + 1023;   // Restore bias to mantissa.
             }
               
          float64 f64_out;
          // Double-Precision:  IEEE 1+11+52 = 64-bit format.
          if (out_NaN) f64_out = 0x7FFF0000FFFF0000;
          else if (out_zero) f64_out = 0x0;
          else if (out_Inf) f64_out = ((out_sign) ? (1uL << 63) : 0uL) | 0x7FF000000000000uL;
          else  f64_out = ((out_sign) ? (1uL << 63) : 0uL) | (((ulong)out_exponent) << 52) | res;
	  // Console.WriteLine("sqrt out_zero={0}  out_NaN={1}  out_Inf={2}", out_zero, out_NaN, out_Inf);
          // Console.WriteLine("sqrt. f64_out=0x{0:x}", f64_out);
          return hutils.to_double(f64_out);
       }


       public static float KiwiSqRootSingle(float x)
       {
         uint ix = hutils.from_single(x);
           // Single-Precision:  IEEE 1+8+23 = 32-bit format.
           bool in_sign = ((ix >> 31) & 1) == 1;
           int in_exponent = (int)((ix >> 23) & 0xFF);
	   uint in_mantissa = ix & ((1u<<23)-1u);
           bool in_zero = (in_exponent == 0) && (in_mantissa == 0);
           bool in_denormal = (in_exponent == 0) && (in_mantissa != 0);
           in_zero |= in_denormal; // To save gates, discard denormals.
           //           Console.WriteLine("S/P in s={0} exp={1} m={2}", in_sign, in_exponent, in_mantissa); 
           int out_exponent = 0;
           bool out_sign = false;
           bool out_NaN = false;
           bool out_Inf = false;
           bool out_zero = false;
           uint res = 0u;

           if (in_exponent == 0xff)
             {
                if (in_mantissa != 0 || in_sign) out_NaN = true;  // Non-zero mantissa is NaN. Zero mantissa is +/- inf.
                else
                    { out_Inf = true; out_sign = in_sign;  }
             }
           else if (in_zero)
            {
              out_zero = true;
            }
          else
            {
               uint num = in_mantissa |(1<<23); // NB: re-insert hidden bit.
               int mm = in_exponent;     // Use m to denote exponent (fools!)
               mm -= 127;                // Unbias exponent
               //Console.WriteLine("  sqrt exponent={0} mantissa={1:X}", mm, num);
               if ((mm&1) != 0)
                 {                	 // Odd exponent, double num to make it even.
                     num <<= 1;
                 }
               mm >>= 1;                 // Answer exponent is half target exponent.
               // Bit-recursive sqrt function on mantissa (note this is different from integer square root where r>>=2 per iteration)
               num <<= 1;
               uint s0 = 0;
               Kiwi.Pause();
               for (uint r = 1<<24; r != 0; r >>= 1)  // r = moving bit from right to left.
                 { 
                   uint t = s0+r; // Console.Write("S/P s0={0:x} num={1:x} t={2:x}    ", s0, num, t);
                   if (num >= t) 
                   {
                     // Console.WriteLine("{0:x} was >= {1:x}", num, t);
                     s0   = t+r;
                     num  -= t;
                     res |=  r;   
                   }
                   // Console.WriteLine("S/P accum {0:x} {1:x}", r, res);
                   num <<= 1;
                   Kiwi.Pause();
                 }
               Kiwi.Pause();
               res >>= 1;
               res &= ((1u << 23)-1u);    // Mask off the hidden bit
               out_exponent = mm + 127;   // Restore bias to mantissa.
             }
               
          uint f32_out;
          // Single-Precision:  IEEE 1+8+23 = 32-bit format.
          if (out_NaN) f32_out = 0x7FFF0000u;
          else if (out_zero) f32_out = 0u;
          else if (out_Inf) f32_out = ((out_sign) ? (1u << 31) : 0u) | 0x7F80000u;
          else  f32_out = ((out_sign) ? (1u << 31) : 0u) | (((uint)out_exponent) << 23) | res;
	  // Console.WriteLine("sqrt out_zero={0}  out_NaN={1}  out_Inf={2}", out_zero, out_NaN, out_Inf);
          // Console.WriteLine("sqrt. f32_out=0x{0:x}", f32_out);
          return hutils.to_single(f32_out);
       }


       /*
        * wrapper sqrt(x)
        */
       [Kiwi.Remote("protocol=HFAST1;reftran=true;mirrorable=true;overloaded=true;inhold=false")]
       public static double Sqrt(double xd)
       {
         return KiwiSqRootDouble(xd);
       }


       [Kiwi.Remote("protocol=HFAST1;reftran=true;mirrorable=true;overloaded=true;inhold=false")]
       public static float Sqrt(float xs)
       {
         return KiwiSqRootSingle(xs);
       }

   }
}

/*
Other methods  (use floating-point arithmetic)
-------------
(This is a copy of a drafted paper by Prof W. Kahan
and K.C. Ng, written in May, 1986)

	Two algorithms are given here to implement sqrt(x)
	(IEEE double precision arithmetic) in software.
	Both supply sqrt(x) correctly rounded. The first algorithm (in
	Section A) uses newton iterations and involves four divisions.
	The second one uses reciproot iterations to avoid division, but
	requires more multiplications. Both algorithms need the ability
	to chop results of arithmetic operations instead of round them,
	and the INEXACT flag to indicate when an arithmetic operation
	is executed exactly with no roundoff error, all part of the
	standard (IEEE 754-1985). The ability to perform shift, add,
	subtract and logical AND operations upon 32-bit words is needed
	too, though not part of the standard.

A.  sqrt(x) by Newton Iteration

   (1)	Initial approximation

	Let x0 and x1 be the leading and the trailing 32-bit words of
	a floating point number x (in IEEE double format) respectively

	    1    11		     52				  ...widths
	   ------------------------------------------------------
	x: |s|	  e     |	      f				|
	   ------------------------------------------------------
	      msb    lsb  msb				      lsb ...order


	     ------------------------  	     ------------------------
	x0:  |s|   e    |    f1     |	 x1: |          f2           |
	     ------------------------  	     ------------------------

	By performing shifts and subtracts on x0 and x1 (both regarded
	as integers), we obtain an 8-bit approximation of sqrt(x) as
	follows.

		k  := (x0>>1) + 0x1ff80000;
		y0 := k - T1[31&(k>>15)].	... y ~ sqrt(x) to 8 bits
	Here k is a 32-bit integer and T1[] is an integer array containing
	correction terms. Now magically the floating value of y (y's
	leading 32-bit word is y0, the value of its trailing word is 0)
	approximates sqrt(x) to almost 8-bit.

	Value of T1:
	static int T1[32]= {
	0,	1024,	3062,	5746,	9193,	13348,	18162,	23592,
	29598,	36145,	43202,	50740,	58733,	67158,	75992,	85215,
	83599,	71378,	60428,	50647,	41945,	34246,	27478,	21581,
	16499,	12183,	8588,	5674,	3403,	1742,	661,	130,};

    (2)	Iterative refinement

	Apply Heron's rule three times to y, we have y approximates
	sqrt(x) to within 1 ulp (Unit in the Last Place):

		y := (y+x/y)/2		... almost 17 sig. bits
		y := (y+x/y)/2		... almost 35 sig. bits
		y := y-(y-x/y)/2	... within 1 ulp


	Remark 1.
	    Another way to improve y to within 1 ulp is:

		y := (y+x/y)		... almost 17 sig. bits to 2*sqrt(x)
		y := y - 0x00100006	... almost 18 sig. bits to sqrt(x)

				2
			    (x-y )*y
		y := y + 2* ----------	...within 1 ulp
			       2
			     3y  + x


	This formula has one division fewer than the one above; however,
	it requires more multiplications and additions. Also x must be
	scaled in advance to avoid spurious overflow in evaluating the
	expression 3y*y+x. Hence it is not recommended uless division
	is slow. If division is very slow, then one should use the
	reciproot algorithm given in section B.

    (3) Final adjustment

	By twiddling y's last bit it is possible to force y to be
	correctly rounded according to the prevailing rounding mode
	as follows. Let r and i be copies of the rounding mode and
	inexact flag before entering the square root program. Also we
	use the expression y+-ulp for the next representable floating
	numbers (up and down) of y. Note that y+-ulp = either fixed
	point y+-1, or multiply y by nextafter(1,+-inf) in chopped
	mode.

		I := FALSE;	... reset INEXACT flag I
		R := RZ;	... set rounding mode to round-toward-zero
		z := x/y;	... chopped quotient, possibly inexact
		If(not I) then {	... if the quotient is exact
		    if(z=y) {
		        I := i;	 ... restore inexact flag
		        R := r;  ... restore rounded mode
		        return sqrt(x):=y.
		    } else {
			z := z - ulp;	... special rounding
		    }
		}
		i := TRUE;		... sqrt(x) is inexact
		If (r=RN) then z=z+ulp	... rounded-to-nearest
		If (r=RP) then {	... round-toward-+inf
		    y = y+ulp; z=z+ulp;
		}
		y := y+z;		... chopped sum
		y0:=y0-0x00100000;	... y := y/2 is correctly rounded.
	        I := i;	 		... restore inexact flag
	        R := r;  		... restore rounded mode
	        return sqrt(x):=y.

    (4)	Special cases

	Square root of +inf, +-0, or NaN is itself;
	Square root of a negative number is NaN with invalid signal.


B.  sqrt(x) by Reciproot Iteration

   (1)	Initial approximation

	Let x0 and x1 be the leading and the trailing 32-bit words of
	a floating point number x (in IEEE double format) respectively
	(see section A). By performing shifs and subtracts on x0 and y0,
	we obtain a 7.8-bit approximation of 1/sqrt(x) as follows.

	    k := 0x5fe80000 - (x0>>1);
	    y0:= k - T2[63&(k>>14)].	... y ~ 1/sqrt(x) to 7.8 bits

	Here k is a 32-bit integer and T2[] is an integer array
	containing correction terms. Now magically the floating
	value of y (y's leading 32-bit word is y0, the value of
	its trailing word y1 is set to zero) approximates 1/sqrt(x)
	to almost 7.8-bit.

	Value of T2:
	static int T2[64]= {
	0x1500,	0x2ef8,	0x4d67,	0x6b02,	0x87be,	0xa395,	0xbe7a,	0xd866,
	0xf14a,	0x1091b,0x11fcd,0x13552,0x14999,0x15c98,0x16e34,0x17e5f,
	0x18d03,0x19a01,0x1a545,0x1ae8a,0x1b5c4,0x1bb01,0x1bfde,0x1c28d,
	0x1c2de,0x1c0db,0x1ba73,0x1b11c,0x1a4b5,0x1953d,0x18266,0x16be0,
	0x1683e,0x179d8,0x18a4d,0x19992,0x1a789,0x1b445,0x1bf61,0x1c989,
	0x1d16d,0x1d77b,0x1dddf,0x1e2ad,0x1e5bf,0x1e6e8,0x1e654,0x1e3cd,
	0x1df2a,0x1d635,0x1cb16,0x1be2c,0x1ae4e,0x19bde,0x1868e,0x16e2e,
	0x1527f,0x1334a,0x11051,0xe951,	0xbe01,	0x8e0d,	0x5924,	0x1edd,};

    (2)	Iterative refinement

	Apply Reciproot iteration three times to y and multiply the
	result by x to get an approximation z that matches sqrt(x)
	to about 1 ulp. To be exact, we will have
		-1ulp < sqrt(x)-z<1.0625ulp.

	... set rounding mode to Round-to-nearest
	   y := y*(1.5-0.5*x*y*y)	... almost 15 sig. bits to 1/sqrt(x)
	   y := y*((1.5-2^-30)+0.5*x*y*y)... about 29 sig. bits to 1/sqrt(x)
	... special arrangement for better accuracy
	   z := x*y			... 29 bits to sqrt(x), with z*y<1
	   z := z + 0.5*z*(1-z*y)	... about 1 ulp to sqrt(x)

	Remark 2. The constant 1.5-2^-30 is chosen to bias the error so that
	(a) the term z*y in the final iteration is always less than 1;
	(b) the error in the final result is biased upward so that
		-1 ulp < sqrt(x) - z < 1.0625 ulp
	    instead of |sqrt(x)-z|<1.03125ulp.

    (3)	Final adjustment

	By twiddling y's last bit it is possible to force y to be
	correctly rounded according to the prevailing rounding mode
	as follows. Let r and i be copies of the rounding mode and
	inexact flag before entering the square root program. Also we
	use the expression y+-ulp for the next representable floating
	numbers (up and down) of y. Note that y+-ulp = either fixed
	point y+-1, or multiply y by nextafter(1,+-inf) in chopped
	mode.

	R := RZ;		... set rounding mode to round-toward-zero
	switch(r) {
	    case RN:		... round-to-nearest
	       if(x<= z*(z-ulp)...chopped) z = z - ulp; else
	       if(x<= z*(z+ulp)...chopped) z = z; else z = z+ulp;
	       break;
	    case RZ:case RM:	... round-to-zero or round-to--inf
	       R:=RP;		... reset rounding mod to round-to-+inf
	       if(x<z*z ... rounded up) z = z - ulp; else
	       if(x>=(z+ulp)*(z+ulp) ...rounded up) z = z+ulp;
	       break;
	    case RP:		... round-to-+inf
	       if(x>(z+ulp)*(z+ulp)...chopped) z = z+2*ulp; else
	       if(x>z*z ...chopped) z = z+ulp;
	       break;
	}

	Remark 3. The above comparisons can be done in fixed point. For
	example, to compare x and w=z*z chopped, it suffices to compare
	x1 and w1 (the trailing parts of x and w), regarding them as
	two's complement integers.

	...Is z an exact square root?
	To determine whether z is an exact square root of x, let z1 be the
	trailing part of z, and also let x0 and x1 be the leading and
	trailing parts of x.

	If ((z1&0x03ffffff)!=0)	... not exact if trailing 26 bits of z!=0
	    I := 1;		... Raise Inexact flag: z is not exact
	else {
	    j := 1 - [(x0>>20)&1]	... j = logb(x) mod 2
	    k := z1 >> 26;		... get z's 25-th and 26-th
					    fraction bits
	    I := i or (k&j) or ((k&(j+j+1))!=(x1&3));
	}
	R:= r		... restore rounded mode
	return sqrt(x):=z.

	If multiplication is cheaper then the foregoing red tape, the
	Inexact flag can be evaluated by

	    I := i;
	    I := (z*z!=x) or I.

	Note that z*z can overwrite I; this value must be sensed if it is
	True.

	Remark 4. If z*z = x exactly, then bit 25 to bit 0 of z1 must be
	zero.

		    --------------------
		z1: |        f2        |
		    --------------------
		bit 31		   bit 0

	Further more, bit 27 and 26 of z1, bit 0 and 1 of x1, and the odd
	or even of logb(x) have the following relations:

	-------------------------------------------------
	bit 27,26 of z1		bit 1,0 of x1	logb(x)
	-------------------------------------------------
	00			00		odd and even
	01			01		even
	10			10		odd
	10			00		even
	11			01		even
	-------------------------------------------------

    (4)	Special cases (see (4) of Section A).

 */

// eof
