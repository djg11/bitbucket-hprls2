//
// Kiwi project fileserver access: KiwiFilesystemStubs.cs
//
// (C) 2013 DJ Greaves.
//
// Contains various generic stubs for use in simulation and h/w synthesis for fifesystem and console input and output.
//
// cf FILE SYSTEM ACCESS FROM RECONFIGURABLE FPGA HARDWARE PROCESSES IN BORPH - Hayden Kwok-Hay So
// cf MIT Leap FPGA O/S
//
//
using System;
using KiwiSystem;


namespace System.IO
{

  public enum FileMode
  {
    Append, //	Opens the file if it exists and seeks to the end of the file, or creates a new file.
    Create,  //  Specifies that the operating system should create a new file.
    CreateNew,  // If the file already exists, an IOException exception is thrown.
    Open,  //     Specifies that the operating system should open an existing file.
    OpenOrCreate, // Specifies that the operating system should open a file if it exists; otherwise, a new file should be    Truncate
  }


  public enum FileAccess
  {
     Read,      // access to the file. Data can be read from the file. Combine with Write for read/write access.
     ReadWrite, // 
     Write      //	   
  }



  public class Path
  {

    public static string Combine(string s1, string s2)
    {
      return s1 + "/" + s2; // Unix file path separator. Currently tough on Windows users!
      // TODO could push it down into KiwiC and get the native one but that's not correct for a cross compiler anyway.
    }
  }

  public class TextReader : IDisposable
  {
    // Flag: Has Dispose already been called? 
    bool disposed = false;
    int file_handle;
    UInt64 buffer;
    uint no_in_buffer;

    public TextReader(int fd) // Constructor.
    {
      no_in_buffer = 0;
      file_handle = fd;
    }

    public void Dispose()
    {
      Dispose(true);
      // FOR now  GC.SuppressFinalize(this);           
    }

   // Protected implementation of Dispose pattern. 
   protected virtual void Dispose(bool disposing)
   {
      if (disposed)return; 
      if (disposing)
      {
         /*Int64 r = */KiwiFiles.KiwiRemoteStreamServices.perform_op(KiwiFiles.fs_protocol_cmd_t.Kiwi_fs_close, (UInt64)file_handle << 56);
         // Free any other managed objects here. 
         //
      }

      // Free any unmanaged objects here. 
      //
      disposed = true;
   }


    // TextReader: Read an 8-bit character, returning -1 if past end of file.
    public int Read()
    {
    //      Console.WriteLine("Pre Replen {0} buffer={1:x}", no_in_buffer, buffer);
      if (no_in_buffer == 0)
      {
        const int rd_len_max = 7;
        buffer = KiwiFiles.KiwiRemoteStreamServices.perform_op(KiwiFiles.fs_protocol_cmd_t.Kiwi_fs_read_word, ((UInt64)file_handle << 56) + (UInt64)rd_len_max);
        no_in_buffer = (uint)(buffer >> 56);
      }
Kiwi.Pause();
      if (no_in_buffer == 0) return -1;
      else 
        {
          char c = (char)(buffer & 0xFF);
Kiwi.Pause();
//	Console.WriteLine("Serve pre  {0} buffer={1:x} {2}", no_in_buffer, buffer, c);
	  buffer = buffer >> 8;
Kiwi.Pause();
	  no_in_buffer = no_in_buffer-1;
//	Console.WriteLine("Serve post {0} buffer={1:x}", no_in_buffer, buffer);
          return (int) c;
        }
    }

    public int Read(char [] arg) // A garbage free reader.
    {
      int p = 0;
      while(true)
      {
         int c = Read();
	 arg[p++] = (char)c;
         if (c == -1 || (char)c == '\n') break;
      }
      return p;
    }

    public string ReadLine__() // The ReadLine api is allowed to create garbage under Kiwi provided the outerloop frees or garbages the returned string on every iteration.  It must not, for example, store a handle on the returned string in an array.
    {
      return "dummy-file-data-for-now";
    }

  }


  public partial class Stream
  {
    protected int file_handle; 
    protected ulong position;
    public int Read(byte [] buffer, int seek_pt, int rd_len_max)
    {
 /// if (seek_pt != position) // TODO need a seek!

        UInt64 rx = KiwiFiles.KiwiRemoteStreamServices.perform_op(KiwiFiles.fs_protocol_cmd_t.Kiwi_fs_read_word, ((UInt64)file_handle << 56) + (UInt64)rd_len_max);
	int no_read = (int)(rx >> 56);
	//Console.WriteLine("Stream read {0} at {1}", no_read, seek_pt);
	position += (UInt64)no_read;
	for (int qq=0;qq<no_read; qq++)
	{
  	//    Console.WriteLine("buffer copy {0}", qq);
   	    buffer[qq+seek_pt] = (byte)(rx & 0xFF); rx = rx >> 8; 
	}
	//        Console.WriteLine("Stream read {0} at {1} copied", no_read, seek_pt);
        return no_read;
    } 

    public int get_Length()
    {
      UInt64 r2 = KiwiFiles.KiwiRemoteStreamServices.perform_op(KiwiFiles.fs_protocol_cmd_t.Kiwi_fs_length, (UInt64)file_handle << 56);

      return (int)r2;
    }

    public void Close()
    {
      Console.WriteLine("Stream close fd={0}", file_handle);
      /*Int64 r = */KiwiFiles.KiwiRemoteStreamServices.perform_op(KiwiFiles.fs_protocol_cmd_t.Kiwi_fs_close, (UInt64)file_handle << 56);
    }

  }


  public class FileStream : Stream
  {

    public FileStream(string filepath, FileMode mode, FileAccess access) // Constructor
    {

//      System.Diagnostics.Debug.Assert(mode==FileMode.Open, "TODO ASSERT11");
//      System.Diagnostics.Debug.Assert(access==FileAccess.Read, "TODO ASSERT22");
//      System.Diagnostics.Debug.Assert(access==FileAccess.Write, "TODO ASSERT33");

      KiwiFS_Utils.send_filename(filepath);
      UInt64 access_code = (access==FileAccess.Write) ? 0x40000uL: 0x30000uL;
      UInt64 r2 = KiwiFiles.KiwiRemoteStreamServices.perform_op(KiwiFiles.fs_protocol_cmd_t.Kiwi_fs_op_open, access_code);

      file_handle = (int)(r2 >> 56);
      // TODO other open modes
      position = 0;
      Console.WriteLine("FileStream Open'd for read {0} fd={1}", filepath, file_handle);
    }

  }

  public class StreamReader
  {


  }

  public class StreamWriter
  {
    bool disposed = false;     // Flag: Has Dispose already been called? 
    int file_handle;
    UInt64 buffer;
    uint no_in_buffer;

    public StreamWriter(string filepath) // Constructor.
    {
      KiwiFS_Utils.send_filename(filepath);
      // Open for writing
      UInt64 r2 = KiwiFiles.KiwiRemoteStreamServices.perform_op(KiwiFiles.fs_protocol_cmd_t.Kiwi_fs_op_open, 0x40000L);
      int file_handle = (int)(r2 >> 56);
      no_in_buffer = 0;
    }

    public void Dispose()
    {
      Dispose(true);
      // FOR now  GC.SuppressFinalize(this);           
    }

   // Protected implementation of Dispose pattern. 
   protected virtual void Dispose(bool disposing)
   {
      if (disposed)return; 

      if (disposing)
      {
         /*Int64 r = */KiwiFiles.KiwiRemoteStreamServices.perform_op(KiwiFiles.fs_protocol_cmd_t.Kiwi_fs_close, (UInt64)file_handle << 56);
         // Free any other managed objects here. 
         //
      }

      // Free any unmanaged objects here. 
      //
      disposed = true;
   }



  }


  public class TextWriter
  {
    bool disposed = false;     // Flag: Has Dispose already been called? 
    int file_handle;
    UInt64 buffer;
    uint no_in_buffer;

    public void Close()
    {

    }

    public void WriteLine(string fmt)  // Please redirect to KiwiConsoleIO.WriteLine as needed...
    {

    }

    public void WriteLine(string fmt, Object arg0)
    {

    }


    public TextWriter(int fd) // Constructor.
    {
      no_in_buffer = 0;
      file_handle = fd;
    }

    public void Dispose()
    {
      Dispose(true);
      // FOR now  GC.SuppressFinalize(this);           
    }

   // Protected implementation of Dispose pattern. 
   protected virtual void Dispose(bool disposing)
   {
      if (disposed)return; 

      if (disposing)
      {
         /*Int64 r = */KiwiFiles.KiwiRemoteStreamServices.perform_op(KiwiFiles.fs_protocol_cmd_t.Kiwi_fs_close, (UInt64)file_handle << 56);
         // Free any other managed objects here. 
         //
      }

      // Free any unmanaged objects here. 
      //
      disposed = true;
   }



  }


  public class KiwiFS_Utils
  {

    public static void send_filename(string filename)
    {
      char [] name = filename.ToCharArray();
      UInt64 r0 = KiwiFiles.KiwiRemoteStreamServices.perform_op(KiwiFiles.fs_protocol_cmd_t.Kiwi_fs_op_open, 0x10000L);
      for (int xo = 0; xo< name.Length; xo++)
      {
//         char cc  = name[xo];

	 byte cc1 = (byte)filename[xo];
//	 Console.WriteLine("Compare {0:x} with {1:x}", cc, cc1);
         UInt64 rc = KiwiFiles.KiwiRemoteStreamServices.perform_op(KiwiFiles.fs_protocol_cmd_t.Kiwi_fs_op_open, (UInt64)(cc1) + 0x20000);
      }    
    }


  }

  public partial class File
  {
   
    public static TextReader OpenText(string filename)
    {
 // TODO make thread safe
      Console.WriteLine("Request to open TextReader {0}", filename);
      KiwiFS_Utils.send_filename(filename);
      UInt64 r2 = KiwiFiles.KiwiRemoteStreamServices.perform_op(KiwiFiles.fs_protocol_cmd_t.Kiwi_fs_op_open, 0x30000L);

      int file_handle = (int)(r2 >> 56);
      Console.WriteLine("File Open'd for read {0} fd={1}", filename, file_handle);
      return new TextReader(file_handle);

    }

    public static StreamReader OpenStream(string filename)
    {
 // TODO make thread safe
        Console.WriteLine("Opening {0} not implemented!", filename);
       return new StreamReader();
      
    }

   public static bool Exists (string filename)
   {
 // TODO make thread safe
      Console.WriteLine("Check exists {0}", filename);
      KiwiFS_Utils.send_filename(filename);
      UInt64 r2 = KiwiFiles.KiwiRemoteStreamServices.perform_op(KiwiFiles.fs_protocol_cmd_t.Kiwi_fs_op_open, 0x50000L);
      bool ans = (r2 & 1) != 0;
      Console.WriteLine("Check exists {0} -> {1}", filename, ans);
      return ans;
   }

  }
}

namespace KiwiFiles
{

  // Intended to be a generic input/output dispatch for binary and ASCII data.
  // Console devices experimented with so far include : framestore, UART or LCD display, network and filesystem.
  // but we need to define Kiwi_fs_putchar or something like it to make the configuration and routing easier.

  // TODO: pointer to where we explain how to use this with various substrates and NoCs. ...
  


 enum fs_protocol_cmd_t
  {
     Kiwi_fs_op_nop,
     Kiwi_fs_op_open,     //1 
     Kiwi_fs_op_console,  //2
     Kiwi_fs_read_word,   //3
     Kiwi_fs_write_word,//  4
     Kiwi_fs_test_eof,//    5
     Kiwi_fs_close,     //  6
     Kiwi_fs_length,    //  7
   };

  class KiwiRemoteStreamServices
  {
    // Replaced with an external implementation since attributed with KiwiRemote.
    [Kiwi.Remote("protocol=HFAST1;externally-instantiated=true")]
    public static UInt64 perform_op(fs_protocol_cmd_t cmd, UInt64 a2)
    {  
      return '*'; // Replaced with an external implementation.
    }

  }
}

// eof
