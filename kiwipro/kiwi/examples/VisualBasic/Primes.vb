' Kiwi Scientific Acceleration - Visual Basic Demo

' Primes using Sieve of Eratosthenes
' See http://www.cl.cam.ac.uk/research/srg/han/hprls/orangepath/kiwic-demos/kiwi-visual-basic/

' For Kiwi use, since this has no inputs or output, compile with something like -bevelab-default-pause-mode=bblock,  or
' put a Kiwi.Pause call near the start, otherwise the whole program will be run at compile time!


Imports KiwiSystem

Module Primes

   Private Const MyLimit = 10000

   Dim flags(MyLimit-1) as Boolean

   Private Sub Setter()
      Console.WriteLine("Setting All Flags")
      Dim p1 as Integer
      p1 = 0
      While (p1 < MyLimit)
            Flags(p1) = true
      	    p1 = p1 + 1
      End While
   End Sub


   Private Sub CrossingsOff()
      Console.WriteLine("Crossings Off Flags")
      Dim ii, jj as Integer
      ii = 2
      While (ii < MyLimit)
            jj = ii*2
            While (jj < MyLimit)
                Flags(jj) = false
                jj = jj + ii
            End While
            ii = ii + 1
      End While
   End Sub


   Private Sub Printer()
      Console.WriteLine("Printing out the primes:")
      Dim count, p3 as Integer
      p3 = 0
      count = 0
      While (p3 < MyLimit)
            if Flags(p3) 
               Console.WriteLine("  {0} is prime", p3)
               count = count + 1
               End If
      	    p3 = p3 + 1
      End While
      Console.WriteLine("There are {0} primes below {1}.", count, MyLimit)
   End Sub

   Public Sub Main()
      Console.WriteLine("Primes Start:")
      Kiwi.Pause() 
      Setter()
      CrossingsOff()
      Printer()
      Console.WriteLine("Primes End:")
   End Sub

End Module
' eof
