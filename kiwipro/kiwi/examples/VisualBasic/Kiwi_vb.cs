// Kiwi_vb.cs
// Kiwi Scientific Acceleration
// Additional bindings needed for a trival Visual Basic program to compile with KiwiC.
namespace Microsoft
{
  namespace VisualBasic
  {
    namespace Devices
    {
      public class Computer
      { //= "My.Computer";
	
      }
    }


    namespace ApplicationServices
    {

      public class ApplicationBase
      {
	
      }

      public class User
      {
	
      }
    }
  }
}
// eof