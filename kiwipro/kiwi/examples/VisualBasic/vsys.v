//
// Kiwi Scientific Acceleration  
// University of Cambridge, Computer Laboratory.
//
// vsys.v - A test wrapper for RTLSIM execution environment
//
`timescale 1ns/1ns

module SIMSYS();
   reg clk, reset;
   initial begin reset = 1; clk = 0; #33 reset = 0; end
   always #5 clk = !clk;

   initial begin # (1000 * 100 * 1000) $display("Finish HDL simulation on timeout %t.", $time); $finish(); end
   initial begin $dumpfile("vcd.vcd"); $dumpvars(); end

   wire [7:0]  ksubsAbendSyndrome;
   wire        finished = (ksubsAbendSyndrome != 0) && (ksubsAbendSyndrome != 255);

   wire done;
   wire [31:0] codesent;
   Primes the_primes(.clk(clk), 
	       .reset(reset),
	       .hpr_abend_syndrome(ksubsAbendSyndrome)
	       );

   always @(posedge clk) begin
      if (finished) begin
	 $display("Exit on finished syndrome 0x%02h reported after %d clocks.", ksubsAbendSyndrome, $time/10);
	 @(posedge clk)	#5000  $finish;
	 end
//      $display("%1t,  pc=%1d, codesent=%h" , $time, the_cordic1.xpc10nz, 0);
   end
   
endmodule
// eof


