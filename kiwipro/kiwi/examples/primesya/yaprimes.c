// yaprimes.c - primes sieve C Version for comparison.
// Kiwi Scientific Acceleration
// $Id: primesya.cs,v 1.7 2011/06/08 17:45:10 djg11 Exp $
// 

#include <stdio.h>
#include <stdlib.h>

//
// Correct output is one of:
//   There are 27 primes below the natural number 100.
//   There are 48 primes below the natural number 200.
//   There are 80 primes below the natural number 400.
//   There are 111 primes below the natural number 600.
//   There are 170 primes below the natural number 1000.
//   There are 305 primes below the natural number 2000.
//   There are 671 primes below the natural number 5000.
//   There are 1231 primes below the natural number 10000.
//   There are 9594 primes below the natural number 100000.

#if LARGESIZE
static int limit = 100  * 1000;
#else
static int limit = 1000 * 1;
#endif

typedef unsigned char bool;
static int evariant = 0;
static bool *PA;

    /*    static uint [] PY = new uint[limit];
    static int [] PZ1 = new int[limit];
    static int [] PZ2 = new int[limit];
    */


static unsigned int count = 0;
static unsigned int count1 = 0;
static int elimit = 0;
const bool true = 1;
const bool false = 0;
static bool start;
static bool finished = 0;

//    [Kiwi.HardwareEntryPoint()]
int kiwitest(bool verbosef)
{
  elimit = limit;
  if (verbosef) printf("Primes Up To %i\n", limit);
  PA = (bool *)(malloc(sizeof(bool)*limit));
  // Clear array
  count = 0; 
  int woz;
  for (woz = 0; woz < limit; woz++) 
    { 
      PA[woz] = 1; 
      //printf("Setting initial array flag to hold : addr={0} readback={1}", woz, PA[woz]); // Read back and print.
    }

  int i, j;

  for (i=2;i<limit; i++)
    {
      // Cross off the multiples - optimise by skipping where the base is already crossed off.
      if (evariant == 0) j = i*2;     
      else 
	{
	  if (evariant == 1)     
	    {
	      bool pp = PA[i];
	      //Console.WriteLine(" tnow={2}: check back {0} = {1} ", i, pp,  Kiwi.tnow);
	      if (!pp) continue;
	      count1 += 1;
	    }
	  // Can further optimise by commencing the cross-off of the factor squared.
	  j = i;
	  if (evariant == 2)  j = i*i;
	}
      for ( ; j<limit; j+=i) 
	{
	  //printf("Cross off %i %i\n", i, j);
	  PA[j] = false; 
	}
    }

  if (verbosef) printf("Now counting.\n");
  // Count how many there were and store them consecutively in the output array.
  int w;
  for (w = 0; w < limit; w++) 
    { 
      if (PA[w]) 
	{
	  count += 1; 
	  /*
	    {
	      PY[count] = (unsigned int)w;
	      PZ1[count] = w;
		     PZ2[count] = w;
              }
	  */
          //printf("Tally counting %i %i\n", w, count);
	}
    }
  if (verbosef) printf("There are %i (%i) primes below the natural number %i.\n", count, count1, limit);
  finished = true;
  return 0;

}


int main(int argc, const char *argv[])
{
  if (argc >= 2) evariant = atoi(argv[1]);
  int runs=1000*1000;
  int r;
  for (r=0; r<runs; r++) kiwitest(r==0);
  printf("ev=%i: Completed %i runs\n", evariant, runs);
  return 0;
}
// eof
