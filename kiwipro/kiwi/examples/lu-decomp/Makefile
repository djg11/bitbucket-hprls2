#//
#// Kiwi Scientific Acceleration Example - Lower/Upper Decomposition and Equation Solving.
#// (C) 2014 DJ Greaves, University of Cambridge, Computer Laboratory.
#//

#
#
include ../Makefile.examples.inc

ANAME=lu-decomp


# -bevelab-default-pause-mode=soft   -bevelab-soft-pause-threshold=30 \



# -sim

RES_SMALL= \
    -max-no-int-divs=1 \
    -max-no-fp-divs=1 \
    -max-no-int-muls=1 \
    -max-no-fp-muls=1 
#    -int-fl-limit-mul=20

# Finished at 120us on 100 MHz clock with resources constrained.


RES_BIG= \
    -max-no-int-divs=10 \
    -max-no-fp-divs=10 \
    -max-no-int-muls=10 \
    -max-no-fp-muls=10 
#    -int-fl-limit-mul=20

# soft 20 - 250 RTL arcs  - restructure in - 197 resumes and 251 arcs  683 new pc values
# soft 10, Kiwi L/U demo - L/U decomposition demo complete at 10, 140375.
# soft 20 Kiwi L/U demo - L/U decomposition demo complete at  20, 137765.
# soft 30 Kiwi L/U demo - L/U decomposition demo complete at  30, 121675


KIWICFLAGS= -vnl-resets=synchronous \
		-kiwic-cil-dump=combined \
		-bondout-loadstore-port-count=0 \
		-vnl-roundtrip=disable \
		$(RES_BIG) \
		-bevelab-default-pause-mode=soft \
		-bevelab-soft-pause-threshold=5 \
		-kiwic-register-colours=1 

#-kiwic-kcode-dump=enable \
#-restructure2=disable


all: killer  isim
# systemc

killer:
	rm -f $(ANAME).v

$(ANAME).exe:$(ANAME).cs
	$(CSC) $(ANAME).cs -r:$(KIWIDLL) 
#----------------------------------------------------------------
# RTL
$(ANAME).v:$(ANAME).exe
	$(KIWIC) $(KIWICFLAGS) -vnl-rootmodname=DUT -vnl=$(ANAME).v $(ANAME).exe 
	echo "KiwiC returned to Make"

isim:$(ANAME).v
	iverilog $(ANAME).v vsys.v $(CV_FP_ARITH_LIB) $(CV_INT_ARITH_LIB)
	./a.out
	cp vcd.vcd ~/Dropbox

#----------------------------------------------------------------
# SystemC

CXXFLAGS=   -O0 -g


systemc: $(ANAME).sysc.cpp
	$(CXX) -c -O2 $(CXXINCLUDES) $(ANAME).sysc.cpp


$(ANAME).sysc.cpp:$(ANAME).exe
	$(KIWIC) $(KIWICFLAGS) -vnl-rootmodname=DUT -cgen2=enable -cgen2-filename=$(ANAME) $(ANAME).exe 

#----------------------------------------------------------------
monorun:$(ANAME).exe
	MONO_PATH=$(KIWI)/support mono $(ANAME).exe

#----------------------------------------------------------------

work:
	vlib work

# eof
