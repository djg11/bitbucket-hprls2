//
// Kiwi Scientific Acceleration  
// University of Cambridge, Computer Laboratory
//
// vsys.v - A test wrapper for simulating very simple tests with clock and reset.
//
`timescale 1ns/1ns

module SIMSYS();

   reg clk, reset;
   initial begin reset = 1; clk = 0; #33 reset = 0; end
   always #5 clk = !clk;

   initial begin # (200 * 10 * 1000) $display("Finish HDL simulation on timeout %t.", $time); $finish(); end

   initial begin $dumpfile("vcd.vcd"); $dumpvars(); end
   wire [7:0]  ksubsAbendSyndrome;
   wire        finished = (ksubsAbendSyndrome != 0) && (ksubsAbendSyndrome != 255);

   //wire [31:0] codesent;
   wire done;
   
   DUT the_dut(.clk(clk), 
	       .reset(reset),
	       .hpr_abend_syndrome(ksubsAbendSyndrome)
	       //.codesent(codesent)
	       );

  always @(posedge clk) begin
      if (finished) begin
	 $display("Exit on finished syndrome 0x%2h reported after %d clocks.", ksubsAbendSyndrome, $time/10);

	 #5000  $finish;
	 end
     //$display("%1t,  pc=%1d, codesent=%h" , $time, the_dut.xpc10nz, codesent);
  end
   

endmodule
// eof


