//
// Kiwi Scientific Acceleration Example - Lower/Upper Decomposition and Equation Solving.
// (C) 2014 DJ Greaves, University of Cambridge, Computer Laboratory.
//
//  Simultaneous Equations Solving using L/U decomposition. 
//  This is the single-threaded version.  
//  Parallel speedup is gained here through automatic unwind of the inner loops.
//  An explictly parallel implementation is in another demo.

using System;
using System.Text;
using KiwiSystem;
using System.Diagnostics;

//http://halcyon.usc.edu/~pk/prasannawebsite/papers/ERSAvikashd_gg.pdf
//Efficient Floating-point based Block LU Decomposition on FPGAs Vikash Daga


public class SimuSolve
{
    double [,] tmp;
    double [] yy;
    double [] xx;
    int problemSize;
    public SimuSolve(int ps) // constructor
    {
      problemSize = ps;
      tmp = new double [problemSize,problemSize];
      yy = new double[problemSize];
      xx = new double[problemSize];
    }


    
    public void LUdecompose(double [,] LL, double [,] Adata, bool pivot_enable)
    {
        //int problemSize = yy.Length;

        for (int k=0; k<problemSize; k++) { LL[k,k] = 1.0; }
        for (int k=0; k<problemSize; k++)
            {
                for (int z=0; z<problemSize; z++) { LL[z,z] = 1.0; }
/*              if (pivot_enable)
                    {
                        double p = 0.0;
                        int k1 = 0;
                        for(int i=k; i<problemSize; i++)
                            {
                                if (Math.Abs(Adata[i,k]) > p) // Pivoting : find largest 
                                    { p = Math.Abs(Adata[i,k]);
                                      k1 = k;
                                    }
                            }           
                        //Console.WriteLine("Pivot %d %f", k1, p);
                        double [] t = Adata[k]; Adata[k] = Adata[k1]; Adata[k1] = t;
                    }
*/
                for (int i=k+1; i<problemSize; i++)
                    {
                        Debug.Assert(Adata[k,k] != 0.0); // Singular matrix!
                        double mu = Adata[i,k]/Adata[k,k];
                        //Console.WriteLine("mu at {0} {1} is {2}", i, k, mu);
                        Adata[i,k] = 0.0;
                        LL [i,k] = mu; // This simple store is all you need to form L.
                        for (int j=k+1; j<problemSize; j++)
                            {
                                Adata[i,j] = Adata[i,j] - mu * Adata[k,j];
                            }
                    }

            }
    }


    // Decompose to Lower and Upper. Upper is done in-place in G.
    public void DecomposeVerbose(double [,] LL, double [,] G)
    {
        Console.WriteLine("L/U Decomposition - single-threaded version.\n");
        Console.WriteLine("Initial Coefficient Matrix Pre L/U Decomposition:\n"); MatrixLib.printa(G);
        LUdecompose(LL, G, true);
        Console.WriteLine("UU="); MatrixLib.printa(G);
        Console.WriteLine("LL="); MatrixLib.printa(LL);
        Console.WriteLine("Recombine LL and RR: Should result in the original:"); MatrixLib.mpx(tmp, LL, G); MatrixLib.printa(tmp);
    }

    public double [] FwdsSubst(double [,] LL, double [] b)    // Forwards substitution to solve Ly=b
    {
        yy[0] = b[0]; 
        for (int i=1; i < problemSize; i++)
            {
                double sum = 0.0;
                for (int j=0; j < i; j++) sum += LL[i,j] *  yy[j];
                yy[i] = b[i] - sum;
            }
        return yy;
    }

    public double [] BackSubst(double [,] UU, double [] yy)    // Back substitution to solve Ux=y
    {
        for (int i=problemSize-1; i >= 0; i--)
            {
                double sum = 0.0;
                for (int j=i+1; j < problemSize; j++) sum += UU[i,j] * xx[j];
                xx[i] = (yy[i] - sum)/UU[i,i];
            }
        return xx;
    }

    public double [] SolveVerbose(double [,] LL, double [,] UU, double [] rhs)
    {
        double [] yy = FwdsSubst(LL, rhs); 
        Console.Write("After fwds subst="); MatrixLib.printa(yy);
        double [] xx = BackSubst(UU, yy);
        Console.Write("After back subst="); MatrixLib.printa(xx);
        return xx; // Return the final solution.
    }
}


class MatrixLib // It would be more OO to apply these methods to matrix instances.
{
    //------------------------------------------------------------------------
    // Matrix support functions now follow (there is similar code in java.utils).

    public static void printa (double [,] A)
    {
        for (int i=0; i<A.GetLength(0); i++)
            {
                Console.Write("    ");
                for (int j=0; j<A.GetLength(1); j++)
                    {
                        //if (A[i,j]==0.0) Console.Write("-.------ ", A[i,j]); else 
			Console.Write(" {0:f} ", A[i,j]);
                    }
                Console.WriteLine("");
  	        Kiwi.Pause();
            }
        Console.WriteLine("");
    }

    public static void printa (double [] AA)
    {
        Console.Write("{");
        for (int j=0; j<AA.Length; j++)
            {
            //          if (AA[j]==0.0) Console.Write("-.------ ", AA[j]);         else
                Console.Write("{0:f} ", AA[j]);
            }
	    Kiwi.Pause();
        Console.WriteLine("}");
    }


    public static double [] mpx(double [] Ans, double[,] AA, double[] BB) // Matrix multiply oblong array by column array.
    {
        for (int i=0; i<BB.Length; i++) 
            {
                double ss = 0.0;
                for (int j=0; j<BB.Length; j++) ss += AA[i,j] * BB[j];
                Ans[i] = ss;
            }
        return Ans;
    }

    public static double [,] mpx(double [,] Ans, double[,] AA, double[,] BB) // Matrix multiply oblong arrays.
    {
        //Console.WriteLine(" mpx %d,%d with %d,%d\n", AA.Length, AA[0].Length, BB.Length, BB[0].Length);       
        //Debug.Assert(AA.GetLength(1) == BB.GetLength(0));
        for (int i=0; i<AA.GetLength(0); i++)
            for (int k=0; k < BB.GetLength(1); k++)
                {
                    double sum = 0.0;
                    for (int j=0; j<AA.GetLength(1); j++) 
                        {
                            //Console.WriteLine(" mpx %d,%d with %d,%d %d %d %d\n", AA.Length, AA[0].Length, BB.Length, BB[0].Length, i, k, j); 
                            sum += AA[i,j] * BB[j,k];
                        }
                    Ans[i,k] = sum;
                }
        return Ans;
    }

    public static double[] [] transpose(double[] [] Ans, double[] [] AA)
    {
        for (int i=0; i<AA.Length; i++) for (int j=0; j<AA[0].Length; j++) Ans[j] [i] = AA[i] [j];
        return Ans;
    }

    public static double [,] copy2d(double[,] copy, double [,] original) // A deep clone - can we not just do a structure assign?.
     {
        for(int i = 0; i < original.GetLength(0); i++)
            {
                for (int j=0; j<original.GetLength(1); j++) copy[i,j] = original[i,j];
            }
        return copy;
    }
}


public class luTest
{
  const int problemSize = 8;
  static double [] target_rhs = new double [problemSize];
  static double [] res = new double [problemSize];
  static double [,] coefficients = new double [problemSize, problemSize];
  static double [,] LLtop = new double [problemSize, problemSize];
  static double [,] UUtop = new double [problemSize, problemSize];


  static void generate_example_rhs(int no, bool printme)
  {
     for (int i=0; i<problemSize; i++) target_rhs[i] = 1.0 + 2.0 * (double)i + 10.0 * (double)no;
     target_rhs[problemSize-1] = 2.71;
     if (printme) for (int i=0; i<problemSize; i++)
     {   
       Console.WriteLine("  test={2}  target_rhs [{0}] == {1}", i, target_rhs[i], no);
     }
  }

  static void generate_example_coefficients()
  {
        double pp = 10.0;
        for (int rr=0; rr<problemSize; rr++)
	    for (int cc=0; cc<problemSize; cc++)
		 { coefficients[rr,cc] = pp; pp *= 1.1; } 
        for (int i=0; i<problemSize; i++)  // += requires Array.Address backdoor to work
        { double p = coefficients[i,i];
          coefficients[i,i] = p + 10.0;
        }

        Console.WriteLine("Kiwi L/U demo - L/U decomposition target_rhs generated.");
  }




  [Kiwi.HardwareEntryPoint()]
  public static void Main()
    {
       Console.WriteLine("Kiwi Demo - L/U decomposition");
       SimuSolve ssolve = new SimuSolve(problemSize);
       Kiwi.KppMark(1, "Start");

       generate_example_coefficients();
       MatrixLib.copy2d(UUtop, coefficients);   
       ssolve.DecomposeVerbose(LLtop, UUtop);
       Console.WriteLine("Kiwi L/U demo - coefficient matrix decomposed.");
       Kiwi.KppMark(2, "Coefficients Created");

       for (int test=0; test<3; test++)
        {
           Console.WriteLine("\nKiwi L/U demo - L/U decomposition test with rhs no {0}.", test);
           generate_example_rhs(test, true);


           double [] sol = ssolve.SolveVerbose(LLtop, UUtop, target_rhs);

           // Now see if it works as a solution
           Console.Write("Substitute back - rhs given by solution is: y="); 
           MatrixLib.printa(MatrixLib.mpx(res, coefficients, sol));     

        }
       Kiwi.KppMark(3, "ThreeTestsFinished");
      Console.WriteLine("Kiwi L/U demo - L/U decomposition demo complete at {0}.", Kiwi.tnow);
      Kiwi.Pause();
      Kiwi.ReportNormalCompletion();
   }
}
// eof


