// $Id: vsys.v,v 1.2 2010/08/11 08:51:35 djg11 Exp $
//
// (C) 2010-15 DJ Greaves, University of Cambridge Computer Laboratory.
//
// vsys.v - A test wrapper for simulating very simple tests with clock and reset.
//

//

`timescale 1ns/10ps

module SIMSYS();
   reg clk, reset;
   initial begin reset = 1; clk = 0; # 400 reset = 0; end
   always #100 clk = !clk;



   wire [31:0] count, vol;
   wire finished;  
   reg start; 

   initial begin # (1000 * 1000) $display("Finish HDL simulation on timeout %t.", $time); $finish(); end

   initial begin $dumpfile("vcd.vcd"); $dumpvars(); end

   always @(posedge clk) begin
//	$display("pc, %d thread0 = %d", $time, streamwrapper.dut.xpc10nz);
	end

   STREAMWRAPPER streamwrapper(.clk(clk), .reset(reset),
			       
			       .finished(finished)
			       );
   
endmodule
// eof
