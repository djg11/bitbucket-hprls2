// Kiwi Scientific Acceleration: Streaming Example Demo
//
// (C) 2015 DJ Greaves, University of Cambridge Computer Laboratory.

module STREAMWRAPPER(
		     output finished,
		     input  clk, 
		     input  reset);

  
   wire [127:0] 	    streamingInPort_data;
   wire streamingInPort_rdy_in, streamingInPort_rdy_out;


    STESTGEN128 stream_src(.clk(clk), .reset(reset),
		    .streamingOutPort_data(streamingInPort_data),
		    .streamingOutPort_rdy_in(streamingInPort_rdy_out),
		    .streamingOutPort_rdy_out(streamingInPort_rdy_in)
		    );


    DUT dut(.clk(clk), .reset(reset),
	   .streamingInPort_data0(streamingInPort_data[63:0]),
	   .streamingInPort_data1(streamingInPort_data[127:64]),      
	   .streamingInPort_rdy_in(streamingInPort_rdy_in),
	   .streamingInPort_rdy_out(streamingInPort_rdy_out),
	   .finished(finished)
	   );

   
endmodule // STREAMWRAPPER

module  STESTGEN128(
		    input clk, 
		    input reset,
		    output [127:0] streamingOutPort_data,
		    input streamingOutPort_rdy_in,
		    output streamingOutPort_rdy_out
		    );

   reg [14:0] 		   prbs;
   reg [127:0] 		   gen;

   wire 		   go = streamingOutPort_rdy_in && streamingOutPort_rdy_out;
   
   always @(posedge clk)
      if (reset) begin
	 prbs <= 1;
	 gen <= 0;
	 end
      else
	if (go) begin
	   prbs <= (prbs >> 1) ^ ((prbs[0] ^ prbs[1]) ? (1<<14):0);
	   gen <= (gen << 1) ^ { 127'd0, prbs };
	end

   // Standard test sequence or else PRBS.
   assign streamingOutPort_data = (1) ? { 32'd1000, 32'd2000, 32'd3000, 32'd4000 } : gen;
   assign streamingOutPort_rdy_out = 1;

endmodule

// eof
