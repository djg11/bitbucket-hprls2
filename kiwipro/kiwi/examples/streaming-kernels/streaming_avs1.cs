// 
// Streaming Kernels Demo
// Kiwi Scientific Acceleration
// 
// (C) 2010-15 - DJ Greaves - University of Cambridge Computer Laboratory
//

using System;
using KiwiSystem;
using KiwiStreaming;


class streaming_avs1
{
  [Kiwi.OutputWordPort(31, 0)][Kiwi.OutputName("average")] static uint average = 0;
  [Kiwi.OutputWordPort(31, 0)][Kiwi.OutputName("dmon")] static int dmon = 0;
  [Kiwi.OutputBitPort("finished")] static bool finished = false;

  static void mainloop()
  {
    long counter = 0;
    int last = 0;
    while (true)
      {
	counter = counter + 1;
	Kiwi.Pause();
        int nd = KiwiBinaryStreamSrc.get_int32();
	average  =  (uint) ((nd+last)/2);
	last = nd;
	dmon = nd;
	Console.WriteLine("Average at point {0} {1} is {2}", Kiwi.tnow, counter,  average);
      }
  }


  [Kiwi.HardwareEntryPoint()]
  public static void Main()
  {
    bool kpp = true;
    Kiwi.KppMark(0, "START", "INITIALISE");  // Waypoint
    Console.WriteLine("Stream ");
    KiwiBinaryStreamSrc.start();
    mainloop();
    System.Diagnostics.Debug.Assert(false, "Never Reached");
    Kiwi.Pause();
    finished = true;
    Kiwi.Pause();
    Kiwi.KppMark(1, "FINISH");  // Waypoint
    Kiwi.Pause();
  }
}


// eof
