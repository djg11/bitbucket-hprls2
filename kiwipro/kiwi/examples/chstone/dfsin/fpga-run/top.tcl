
# /usr/groups/ecad/xilinx/vivado_sdk_installed/Vivado/2015.1/bin/vivado -source ~/tcl-xilinx-script1.tcl -mode batch

#STEP#1: define the output directory area.
#
set outputDir /tmp/dfsin/fpga-run


set cvtech1 $env(HPRLS)/hpr/cvgates.v
set cvtech2 $env(HPRLS)/hpr/cv_fparith.v
set design_dir $env(HPRLS)/kiwipro/kiwi/examples/chstone/dfsin/fpga-run


file mkdir $outputDir


#
# STEP#2: setup design sources and constraints
#


read_verilog  $cvtech1 $cvtech2 
read_verilog  $design_dir/top.v 
read_verilog  ../dfsin-floatnat.v


# spare examples
#read_verilog [ glob ./Sources/hdl/usbf/*.v ]
#read_vhdl -library bftLib [ glob ./Sources/hdl/bftLib/*.vhdl ]


# Constraints
read_xdc $design_dir/zed1a.xdc



#
# STEP#3: run synthesis, write design checkpoint, report timing,
# and utilization estimates
#

# VC 707:
# synth_design -top top -part xc7vx485tffg1761-2

# Zynq
synth_design -top ZEDTOP1 -part xc7z020clg484-1

#write_checkpoint -force $outputDir/post_synth.dcp
report_timing_summary -datasheet -file $outputDir/post_synth_timing_summary.rpt
report_utilization -file $outputDir/post_synth_util.rpt

# Run custom script to report critical timing paths
# reportCriticalPaths $outputDir/post_synth_critpath_report.csv


#
# STEP#4: run logic optimization, placement and physical logic optimization,
# write design checkpoint, report utilization and timing estimates
#
opt_design
#reportCriticalPaths $outputDir/post_opt_critpath_report.csv
place_design
report_clock_utilization -file $outputDir/clock_util.rpt
#
# Optionally run optimization if there are timing violations after placement
if {[get_property SLACK [get_timing_paths -max_paths 1 -nworst 1 -setup]] < 0} {
puts "Found setup timing violations => running physical optimization"
phys_opt_design
}
#write_checkpoint -force $outputDir/post_place.dcp
report_utilization -file $outputDir/post_place_util.rpt
report_timing_summary -file $outputDir/post_place_timing_summary.rpt
#
# STEP#5: run the router, write the post-route design checkpoint, report the routing
# status, report timing, power, and DRC, and finally save the Verilog netlist.
#
route_design
#write_checkpoint -force $outputDir/post_route.dcp
report_route_status -file $outputDir/post_route_status.rpt
report_timing_summary -file $outputDir/post_route_timing_summary.rpt
report_power -file $outputDir/post_route_power.rpt
report_drc -file $outputDir/post_imp_drc.rpt
write_verilog -force $outputDir/topfpga_impl_netlist.v -mode timesim -sdf_anno true
#
# STEP#6: generate a bitstream
#
write_bitstream -force $outputDir/topfpga.bit
