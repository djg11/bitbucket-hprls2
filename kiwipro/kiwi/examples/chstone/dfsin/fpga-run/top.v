// Kiwi Scientific Acceleration
//
// Top - for batch Xilinx use on Xilinx vc707 card. Part=xc7vx485tffg1761-2
//


module top(
    input CLK_sys_clk_p,
    input CLK_sys_clk_n,

    input GPIO_SW_S,
    input GPIO_SW_N,
    input GPIO_SW_W,
    input GPIO_SW_E,
    input GPIO_SW_C,
    input CPU_RESET,
    output [7:0] leds
       );
    
    //
    // SYSCLK
    // 
    IBUFDS IBUFDS_inst_sys_clock(
            .O(sys_clock_out), // Buffer output
            .I(CLK_sys_clk_p), // Diff_p buffer input (connect directly to top-level port)
            .IB(CLK_sys_clk_n) // Diff_n buffer input (connect directly to top-level port)
    );
       
    BUFG BUFG_inst_sys_clock (
          .O(sys_clock_bufg), // 1-bit output: Clock output
          .I(sys_clock_out)
                    );

    wire cbgclk200 = sys_clock_bufg;
    wire reset = CPU_RESET;

    wire [63:0] arg, result;


   wire[31:0] codesent;
//module dfsin(output reg done, input [31:0] dfsin_arg, output [31:0] dfsin_result, input clk, input reset);
  // Instance of the Kiwi-generated design
  dfsin  the_dfsin(  
	       .reset(reset),   
	       .clk(cbgclk200),   
	       .dfsin_arg(arg),
	       .dfsin_result(result)
   );


   wire       rr = (^(result)); // Hash parity


    assign leds[0] = codesent[0];
    assign leds[1] = codesent[1] ^ GPIO_SW_W;
    assign leds[2] = codesent[2] ^ GPIO_SW_E;
    assign leds[3] = codesent[3] ^ GPIO_SW_N;
    assign leds[4] = codesent[4];
    assign leds[5] = codesent[5];
    assign leds[6] = codesent[6];
    assign leds[7] = GPIO_SW_S ^ (codesent != 0);



//  Free-running counter.
    reg[4:0] boz;
    reg[23:0] vp;
    always @(posedge cbgclk200) begin
        vp <= vp + 1;
	if (vp==0) boz <= boz+1;
        end

   assign arg = { 10{boz}};

   assign codesent = result ^ { boz[2], rr, result[32] };   

endmodule
