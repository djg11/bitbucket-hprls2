//
// Kiwi Scientific Acceleration FloatingPointKmeans.cs
//
// K-Means Clustering demo - Lloyds Algorithm on FPGA.
// (C) 2009 DJ Greaves - University of Cambridge, Computer Laboratory
//


using System;
using KiwiSystem;



public class FloatingPointKmeans
{
  public const int LNPoints = 5;
  public const int NPoints = (1<<LNPoints);
  public const int NMeans = 3;

  // Experimenting with manual markup of unwind factors here: Kiwi should do this using its soft threshold usually.
  const int manual_unwind_factor = 1;
  const bool verbosef = false;
  public struct  kpoint
  {
    public float xx, yy;

    public float m2(kpoint other)
    {
      float dx = other.xx - xx, dy = other.yy - yy;
      if (verbosef)
	{
	  Console.Write(" Compare ({0}, {1}) ", xx, yy);
	  Console.Write(" with ({0}, {1}) ", other.xx, other.yy);
	  Console.WriteLine(" distances: dx={0} dy={1}", dx, dy);
	}
      return dx * dx + dy * dy;
    }
  }

  public kpoint [] Points = new kpoint [NPoints];
  public int [] Mapping = new int [NPoints];
  public kpoint [] Centroids = new kpoint [NMeans];
  public bool [] Deads = new bool [NMeans];

  TempRandom rg = new TempRandom(1);

  public void GenerateData(bool randf, int seeder)
  {
    Kiwi.Pause();

    randf = true;

    for (int i=0; i<NPoints; i++)
      {
	int xx = (randf ? rg.randy(): 3 + i);
	int yy = (randf ? rg.randy(): 3 - i);
	int qq = rg.randy();
	Kiwi.Pause();
	kpoint pp;
	pp.xx = 0.01f * (float)xx;
	pp.yy = 0.01f * (float)yy;
	Points[i] = pp;
	Kiwi.Pause();
      }
    
    Kiwi.Pause();
  }

  public void GenDummyData(int seeder1)
  {
    Kiwi.Pause();

    GenerateData(true, seeder1);
    Console.WriteLine("K-Means Floating-Point Data Generated. NPoints={0} NMeans={1}", NPoints, NMeans);
    Kiwi.KppMark(2, "DATA GENERATED");

  }

  // The data array is immutable in the K-Means problem. So printing once is all that is needed.
  public void PrintData()
  {
    Kiwi.Pause();
    for (int i=0; i<NPoints; i++)
      {
	Kiwi.Pause();
	kpoint pp = Points[i];
	Console.WriteLine("     Data print {0}   ({1}, {2})", i, pp.xx, pp.yy);
      }
    Kiwi.Pause();
  }

  public void SeedCentroids(int seeder)
  {
    for (int m=0; m<NMeans; m++) 
      {
	Kiwi.Pause();
	int pt = (m * seeder) % NPoints;
	Centroids[m] = Points[pt];
      }
    Kiwi.Pause();
  }


  public void PrintCentroids()
  {
    Kiwi.Pause();
    for (int m=0; m<NMeans; m++) 
      {
	Kiwi.Pause();
	kpoint pp = Centroids[m];
	Console.Write("     Centroid print {0} ", m);
//	if (Deads[m]) 	  Console.WriteLine("     Dead");
//	else 
	Console.WriteLine("     ({0}, {1})", pp.xx, pp.yy);
      }
    Kiwi.Pause();
  }


  float AllocateCentresToPoints(int pnt)  // Aim to be pauseless within this body
  {
    int centre = 0;
    float best_value = 0.0f; // mcs requires these clears.
    Kiwi.Pause();
    for (int m=0; m<NMeans; m++)
      {
	Kiwi.Pause();
	float dx = Points[pnt].m2(Centroids[m]);

	if (m==0 || dx < best_value)
	  {
	    centre = m;
	    best_value = dx; 
	  }
      }
    Mapping[pnt] = centre;
    if (verbosef) Console.WriteLine(" chose {0} for point {1}", centre, pnt);
    return best_value;
  }

  public float AllocateCentresToPointsAll()
  {
    float errorsum = 0.0f;
    for (int i=0; i<NPoints; i++)
      {
	Kiwi.Pause();
	errorsum += AllocateCentresToPoints(i);
      }
    return errorsum / (float)NPoints;
  }


  void AdaptCentre(int centre)   // Aim to do each call to this in parallel.
  {
    int count = 0;
    float avx = 0.0f, avy = 0.0f;
    int pnt = 0;
   
    while (pnt<NPoints)
      {
	Kiwi.Pause(); 
	for (int pt=0; pt < manual_unwind_factor; pt ++)
	  {
	    if (Mapping[pnt] == centre)
	      {
		
		count += 1;
		avx += Points[pnt].xx;
		avy += Points[pnt].yy;
		if (verbosef) Console.WriteLine("     asummer  ({0}, {1})", avx, avy);
	      }
	    pnt++;
	  }

      }

    Kiwi.Pause();
    if (count == 0)
      {
	if (!Deads[centre]) Console.WriteLine("Centre {0} now dead.", centre);
	Deads[centre] = true;
      }
    else
      {
	float fc = (float) count;
	kpoint qq;
	qq.xx = avx / fc;
	qq.yy = avy / fc;
	Console.WriteLine("   Adapt  ({0}, {1})", qq.xx, qq.yy);
	Centroids[centre] = qq;
      }
  }

  public void AdaptCentresAll()
  {
    for (int m=0; m<NMeans; m++) AdaptCentre(m);
  }


  public float SingleThreadedIteration()
  {
    float error = AllocateCentresToPointsAll();
    AdaptCentresAll();
    return error;
  }

  public void RunKmeans(int seeder2, int count)
  {
    Kiwi.Pause();
    //   PlotData();
    SeedCentroids(seeder2);
    Console.WriteLine("Seeded centroids with an initial guess.");
    SingleThreadedIteration();
    //    PlotCentroids();

    Kiwi.Pause();
    Console.WriteLine("Start iterations.");
    for (int iteration = 0; iteration < count; iteration ++)
      {
	Kiwi.Pause();
	PrintCentroids();
	float error  = SingleThreadedIteration();
	Console.WriteLine("Iteration {0}, error={1}", iteration, error);
      }
  }

}

// eof


