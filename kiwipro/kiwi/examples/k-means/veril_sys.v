//
// Kiwi Scientific Acceleration  
// University of Cambridge, Computer Laboratory.
//
// vsys.v - A test wrapper for simulating very simple tests with clock and reset.
// (C) 2010-16 DJ Greaves, University of Cambridge.
// 
//
//
`timescale 1ns/1ns

module VERI_SIMSYS(input clk, input reset);

   reg [25:0] counter;
   integer old_counter;
   reg [639:0] old_KppWaypoint;
   wire [639:0] KppWaypoint0, KppWaypoint1;   
   wire 	done;


   wire 	rtc50 = counter[15]; // Not as slow as 50Hz for a sensible sim.
   
//   initial begin $dumpfile("top.vcd"); $dumpvars(1, the_dut.bevelab10nz, old_KppWaypoint, KppWaypoint0, reset, done); end

   wire [31:0] 	mon0, mon32;
   reg 		rdel, adel;

   wire 	KiwiFormGraphics_setget_pixel_ack;
   wire 	KiwiFormGraphics_setget_pixel_req;

   wire [7:0] 	KiwiFormGraphics_setget_pixel_return;
   wire [31:0] 	KiwiFormGraphics_setget_pixel_axx;
   wire [31:0] 	KiwiFormGraphics_setget_pixel_ayy;
   wire 	KiwiFormGraphics_setget_pixel_readf;
   wire [7:0] 	KiwiFormGraphics_setget_pixel_wdata;

   wire [31:0] 	bevelab10nz_pc_export   ;
   always @(posedge clk) if (reset) begin
      rdel <= 0;
      adel <= 0;
   end
   else begin
      rdel <=  KiwiFormGraphics_setget_pixel_req;
      adel <=  KiwiFormGraphics_setget_pixel_ack;

      if (KiwiFormGraphics_setget_pixel_req) $display("      (verilator)   Plot point %1d %1d   col %1d", KiwiFormGraphics_setget_pixel_axx, KiwiFormGraphics_setget_pixel_ayy, KiwiFormGraphics_setget_pixel_wdata);
   end
   
   DUT the_dut(.clk(clk), 
	       .mon0(mon0),
	       .mon32(mon32),
	       .rtc50(rtc50),
	       .bevelab10nz_pc_export(bevelab10nz_pc_export),
	       .reset(reset),
	       //.done(done),
	       .KppWaypoint0(KppWaypoint0),
	       .KppWaypoint1(KppWaypoint1),
	       
               .KiwiFormGraphics_setget_pixel_ack(1), // KiwiFormGraphics_setget_pixel_ack_del),
               .KiwiFormGraphics_setget_pixel_req(KiwiFormGraphics_setget_pixel_req),
               .KiwiFormGraphics_setget_pixel_return(KiwiFormGraphics_setget_pixel_return),
               .KiwiFormGraphics_setget_pixel_axx(KiwiFormGraphics_setget_pixel_axx),
               .KiwiFormGraphics_setget_pixel_ayy(KiwiFormGraphics_setget_pixel_ayy),
               .KiwiFormGraphics_setget_pixel_readf(KiwiFormGraphics_setget_pixel_readf),
               .KiwiFormGraphics_setget_pixel_wdata(KiwiFormGraphics_setget_pixel_wdata)

	       
	       );


     always @(posedge clk) begin
	if (reset) begin
	  counter <=0;
	end
	if (done) begin
	   $display("Kiwi done at time %1t.  %1d cycles", $time, counter);
	   $finish;
	end
	
	counter <= counter + 1;
	if (old_KppWaypoint === 640'bx || old_KppWaypoint !=  KppWaypoint0) begin
	   old_KppWaypoint <=  KppWaypoint0;
	   old_counter <= counter;
	   if (KppWaypoint0) $display("%1t, delta=%1d pc=%1d, new waypoint -->%1s", $time, counter-old_counter, the_dut.bevelab10nz, KppWaypoint0);
	   end
	//if ((counter & 32'hFFFF)==0) $display("%1t, pc, thread_pc10=%1d:  v3=0x%1h", $time, the_dut.xpc10nz, 3);
	end

endmodule
// eof
