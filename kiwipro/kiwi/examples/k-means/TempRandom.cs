//
// Kiwi Scientific Acceleration TempRandom.cs
//
// K-Means Clustering demo - Lloyds Algorithm on FPGA.
// (C) 2009 DJ Greaves - University of Cambridge, Computer Laboratory
//
using System;
using KiwiSystem;

public class TempRandom
{


  int prbv;

  public TempRandom(int seed)
  {
    prbv = seed;
  }

  public int randy()  // Do not need this - use standard library that should redirect to Kiwi.Random soon.
  {
    Kiwi.Pause();
    if (false && prbv ==0) prbv = 123456; 
    int nv = 1 & ((prbv >> 14) ^ (prbv >> 13));
    prbv = ((prbv << 1) + nv) & 0x7FFF;
    //Console.WriteLine(" prbv {0:X} {0}", prbv);
    return prbv;
  }
}


// eof
