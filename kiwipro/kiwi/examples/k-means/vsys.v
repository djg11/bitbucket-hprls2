//
// Kiwi Scientific Acceleration  
// University of Cambridge, Computer Laboratory.
//
// vsys.v - A test wrapper for RTLSIM execution environment
//
`timescale 1ns/1ns

module SIMSYS();
   reg clk, reset;
   reg 	     rtc50;  
   initial begin rtc50 = 0; reset = 1; clk = 0; #33 reset = 0; end
   always #5 clk = !clk;


   always #10000 rtc50 = !rtc50;

   
   initial begin # (100 * 1000 * 1000) $display("Finish HDL simulation on timeout %t.", $time); $finish(); end
   initial begin $dumpfile("vcd.vcd"); $dumpvars(); end

   wire [7:0]  ksubsAbendSyndrome;
   wire        finished = (ksubsAbendSyndrome != 0) && (ksubsAbendSyndrome != 255);

   wire [31:0] codesent;

   initial clkdiv = 15;
   
   reg 	     rdel, adel;
   reg [4:0]	     clkdiv;
   always @(posedge clk)       clkdiv <= clkdiv+1;

   always @(posedge clk) if (reset) begin
      rdel <= 0;
      adel <= 0;

   end
   else begin



   end
   wire slowclk = clkdiv[4];
   wire [31:0] mon0, mon32;

//---------------------------------------------------------------------------------------
`ifdef SPARE
              /* portgroup=ReadStick abstractionName=AUTOMETA_JoyStickUnit_ReadStickSQ1_S32 */
   wire        ReadStick_req;
   wire        ReadStick_ack;
   wire [31:0] ReadStick_return;
   wire [31:0] ReadStick_stick_no;


      /* portgroup=LinkStatus abstractionName=AUTOMETA_DuplexInterFPGALink_LinkStatusSQ0_UNIT */
   wire        serdes_LinkStatus_req;
   wire        serdes_LinkStatus_ack;
   wire [31:0] serdes_LinkStatus_return;

      /* portgroup=Read64 abstractionName=AUTOMETA_DuplexInterFPGALink_Read64SQ0_UNIT */
   wire        serdes_Read64_req;
   wire        serdes_Read64_ack = 1;
   wire [63:0] serdes_Read64_return = 0;

      /* portgroup=Write64 abstractionName=AUTOMETA_DuplexInterFPGALink_Write64SQ1_U64 */
   wire        serdes_Write64_req;
   wire        serdes_Write64_ack = 1;
   wire [63:0] serdes_Write64_writed;

   wire [7:0]  setget_pixel_return_client0_raw;
   wire [31:0] setget_pixel_axx_client0_raw, setget_pixel_ayy_client0_raw;
   wire        setget_pixel_readf_client0_raw;
   wire [7:0]  setget_pixel_wdata_client0_raw;
   wire        setget_pixel_req_client0_raw;
   wire [31:0] pc_export;

   JoyStickUnit the_joystim(
			    .clk(clk), 
			    .reset(reset),
			    
			    .ReadStick_req(ReadStick_req),
			    .ReadStick_ack(ReadStick_ack),
			    .ReadStick_return(ReadStick_return),
			    .ReadStick_stick_no(ReadStick_stick_no));           
`endif   


//---------------------------------------------------------------------------------------
   
//    
   KKMeans the_dut(.clk(clk), 
		   .reset(reset),
		   .hpr_abend_syndrome(ksubsAbendSyndrome),
		   .kiwiTMAINIDL400PC10nz_pc_export(pc_export)
//	       .mon0(mon0),
//	       .mon32(mon32)

	       
	       );

   always @(posedge clk) begin

      //$display("kkmeans pc=%1d", pc_export); //  only printing lsb?
      if (finished) begin
	 $display("Exit on finished syndrome 0x%02h reported after %d clocks.", ksubsAbendSyndrome, $time/10);
	 @(posedge clk)	#5000  $finish;
	 end
   end
   
endmodule
// eof


