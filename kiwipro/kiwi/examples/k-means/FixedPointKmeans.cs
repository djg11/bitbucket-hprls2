//
// Kiwi Scientific Acceleration FixedPointKmeans.cs
//
// K-Means Clustering demo - Lloyds Algorithm on FPGA.
// (C) 2009 DJ Greaves - University of Cambridge, Computer Laboratory
//


using System;
using KiwiSystem;


public class FixedPointKmeans
{
  public const int LNPoints = 6;
  public const int NPoints = (1<<LNPoints);
  public const int NMeans = 3;

  const bool verbose = false;

  // Experimenting with manual markup of unwind factors here: Kiwi should do this using its soft threshold usually.
  const int manual_unwind_factor = 1;

  public struct  kpoint
  {
    public int xx, yy;

    public long m2(kpoint other)
    {
      long dx = other.xx - xx, dy = other.yy - yy;
      if (verbose)
	{
	  Console.Write(" Compare ({0}, {1}) ", xx, yy);
	  Console.Write(" with ({0}, {1}) ", other.xx, other.yy);
	  Console.WriteLine(" distances: dx={0} dy={1}", dx, dy);
	  
	}
      return dx * dx + dy * dy;
    }
  }

  public kpoint [] Points = new kpoint [NPoints];
  public int [] Mapping = new int [NPoints];
  public kpoint [] Centroids = new kpoint [NMeans];
  public bool [] Deads = new bool [NMeans];

  TempRandom rg = new TempRandom(1);

  public void GenerateData(bool randf, int seeder)
  {
    Kiwi.Pause();
 

    randf = true;

    for (int i=0; i<NPoints; i++)
      {
	int xx = (randf ? rg.randy(): 3 + i);
	int yy = (randf ? rg.randy(): 3 - i);
	int qq = rg.randy();
	Kiwi.Pause();
	kpoint pp;
	pp.xx = xx;
	pp.yy = yy;
	Points[i] = pp;
	Kiwi.Pause();
      }
    
    Kiwi.Pause();
  }


  public void GenDummyData(int seeder1)
  {
    Kiwi.Pause();
    GenerateData(true, seeder1);
    Console.WriteLine("K-Means Fixed-Point Data Generated. NPoints={0} NMeans={1}", NPoints, NMeans);
    Kiwi.KppMark(2, "DATA GENERATED");
  }


  // The data array is immutable in the K-Means problem. So printing once is all that is needed.
  public void PrintData()
  {
    Kiwi.Pause();
    for (int i=0; i<NPoints; i++)
      {
	Kiwi.Pause();
	kpoint pp = Points[i];
	Console.WriteLine("     Data print {0}   ({1}, {2})", i, pp.xx, pp.yy);
      }
    Kiwi.Pause();
  }

  public void SeedCentroids(int seeder)
  {
    for (int m=0; m<NMeans; m++) 
      {
	Kiwi.Pause();
	int pt = (m * seeder) % NPoints;
	Centroids[m] = Points[pt];
      }
    Kiwi.Pause();
  }


  public void PrintCentroids()
  {
    Kiwi.Pause();
    for (int m=0; m<NMeans; m++) 
      {
	Kiwi.Pause();
	kpoint pp = Centroids[m];
	Console.Write("     Centroid print {0} ", m);
	if (false && Deads[m]) 	  Console.WriteLine("     Dead");
	else Console.WriteLine("     ({0}, {1})", pp.xx, pp.yy);
      }
    Kiwi.Pause();
  }


  long AllocateCentresToPoints(int pnt)  // Aim to be pauseless within this body
  {
    int centre = 0;
    long best_value = 0; // mcs requires these clears.
    Kiwi.Pause();
    for (int m=0; m<NMeans; m++)
      {
	Kiwi.Pause();
	if (verbose) Console.WriteLine("Mean m={0} pnt={1}", m, pnt);
	long dx = Points[pnt].m2(Centroids[m]);

	if (m==0 || dx < best_value)
	  {
	    centre = m;
	    best_value = dx; 
	  }
      }
    Mapping[pnt] = centre;
    if (verbose) Console.WriteLine(" chose {0} for point {1}", centre, pnt);
    return best_value;
  }

  public long AllocateCentresToPointsAll()
  {
    long errorsum = 0;
    for (int i=0; i<NPoints; i++)
      {
	Kiwi.Pause();
	errorsum += AllocateCentresToPoints(i);
      }
    return errorsum / NPoints;
  }


  void AdaptCentre(int centre)   // Aim to do each call to this in parallel.
  {
    int count = 0;
    long avx = 0, avy = 0;
    int pnt = 0;
   
    while (pnt<NPoints)
      {
	Kiwi.Pause(); 
	for (int pt=0; pt < manual_unwind_factor; pt ++)
	  {
	    if (Mapping[pnt] == centre)
	      {
		count += 1;
		avx += Points[pnt].xx;
		avy += Points[pnt].yy;
		if (verbose) Console.WriteLine("     asummer pnt={2} ({0}, {1})", avx, avy, pnt);
	      }
	    pnt++;
	  }
      }

    Kiwi.Pause();
    int fc = count;
    if (fc == 0)
      {
	if (!Deads[centre]) Console.WriteLine("Centre {0} now dead.", centre);
	Deads[centre] = true;
      }
    else
      {
	kpoint qq;
	qq.xx = (int)(avx / fc);
	qq.yy = (int)(avy / fc);
	Console.WriteLine("   Adapt  ({0}, {1})", qq.xx, qq.yy);
	Centroids[centre] = qq;
      }
  }

  public void AdaptCentresAll()
  {
    for (int m=0; m<NMeans; m++) AdaptCentre(m);
  }


  public long SingleThreadedIteration()
  {
    long error = AllocateCentresToPointsAll();
    AdaptCentresAll();
    return error;
  }

  public void RunKmeans(int seeder2, int count)
  {
    Kiwi.Pause();
    //   PlotData();
    SeedCentroids(seeder2);
    Console.WriteLine("Seeded centroids with an initial guess.");
    Kiwi.KppMark(1, "SEEDED");
    
    SingleThreadedIteration();
    //    PlotCentroids();

    Kiwi.Pause();
    Console.WriteLine("Start find");
    for (int iteration = 0; iteration < count; iteration ++)
      {
	Kiwi.Pause();
	PrintCentroids();
	long error  = SingleThreadedIteration();
	Console.WriteLine("Iteration {0}, error={1}", iteration, error);
      }
  }

}

// eof


