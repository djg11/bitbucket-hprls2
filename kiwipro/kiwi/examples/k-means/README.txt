//
// Kiwi Scientific Acceleration KKMeans.cs
//
// K-Means Clustering demo - Lloyds Algorithm on FPGA.
// (C) 2009 DJ Greaves - University of Cambridge, Computer Laboratory
//



We have both a fixed-point and a floating-point version in here, but neither is a parallel version at the moment.  We instead just rely on KiwiC to infer parallelism.

Also, there are some unwind factors manually coded in the CSharp, which is certainly not the intended way of using Kiwi.  But this provides a way to manually explore the basic time/space fold.

//
// Let's explore the parallelism in a straightforward (naive) implementation of Lloyd's Algorithm for K-Means.  
//
// This selects NMeans central points that best fit NPoints data points
// which are in a multi-dimensional space.  Typically NPoints is at least two order of magnitude larger than NMeans. In this article we use 2-D space.
//

//  There are two mutable data structures needed. One is just a vector of means that is initialised with the seeds and returned as the final result. The other is
// is an array of size NPoints where the entries range over 0..NMeans-1 saying which group a point is currently in.

// Some points about the algorithm are:


//  1. It is non-deterministic, in the sense that the answer can depend on the initial seeding of the central points.
//  2. It is iterative and will decrease an error metric on each iteration. Some number of iterations, such as 5 to 50, is useful in practice.
//  3. The input data needs to be frequently accessed and so can be replicated to increase read bandwidth.
//  4. There is considerable opportunity for data parallelism which we hope an HLS tool can automatically find.
//  5. There is also task-level parallelism when we make several attempts from different starting seeds, but we do not discuss that here since it raises no further points of interest.
 
// Each iteration of the algorithm runs two phases, one after the other, and these have different data access patterns.
// Question: Is there a good layout of data that will boost performance. We need to consider three
// scales of operation according to the amount of data:

//  1. Small data size, up to a megabyte or so, that can be stored in SRAM on an FPGA.
//  2. Larger data that can be held in a DRAM bank
//  3. Big data that must be streamed from the file-server to the compute engine as few times as possible.

// The phases are allocateCentresToPoints and adaptCentres.

// The allocateCentresToPoints loop nest computes a norm for each data point with respect to the current vector of centres. It compares all data items with all centres and so has NP*NM
// cost. It is embarrassingly parallel in the data direction and for each datum it is a map reduce where the reduction operator is the associative and commutative 'keep the best so far'  operator.  
//  The obvious ideal implementation is to farm a mirror of the centres to each of N units that each handles NP/N data points.  
//
// The adaptCentres loop again has cost NP*NM but with the parallelism paradigms interchanged.  It is embarrassingly parallel in the centres direction and is a map reduce over the data dimension where the associative reduction operator is the addition intrinsic to computing the arithmetic mean. 
