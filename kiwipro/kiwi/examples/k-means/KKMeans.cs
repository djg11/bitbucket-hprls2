//
// Kiwi Scientific Acceleration KKMeans.cs
//
// K-Means Clustering demo - Lloyds Algorithm on FPGA.
// (C) 2009 DJ Greaves - University of Cambridge, Computer Laboratory
//


using System;
using KiwiSystem;

public static class KKMeans
{

  public static void Main()
  {
    RunHW();
  }

  [Kiwi.OutputWordPort("result_lo")] static int mon32;  // Debug monitoring - we need an output if FPGA tools are not to remove all.

  static void runDemo()
  {
    Console.WriteLine("Kiwi K-Means Clustering demo - Lloyds Algorithm on FPGA --- Start");
    Kiwi.KppMark(0, "START");

    // Same bench should work for fixed and floating versions of this.
    // Need to define an abstract class please!     
    var kmd = /* (true) ? new FixedPointKmeans(): */ new FloatingPointKmeans();
    kmd.GenDummyData(1);
    kmd.PrintData();

    Console.WriteLine("Kiwi K-Means Clustering demo - synthetic data generated.");

    Kiwi.KppMark(1, "MAINSTART");
    kmd.RunKmeans(1, 6);
   //    Console.WriteLine("Kiwi K-Means Clustering demo - Plotted");
    Kiwi.KppMark(2, "PRINTRESULT");    
    Console.WriteLine("Final result is:");
    kmd.PrintCentroids();
  }

  [Kiwi.HardwareEntryPoint()]
  public static void RunHW()
  {
    runDemo();
    Kiwi.KppMark(3, "FINISH");
    Console.WriteLine("K-Means Demo Finish");
    Kiwi.ReportNormalCompletion();
    Kiwi.Pause();
  }
}


// eof


