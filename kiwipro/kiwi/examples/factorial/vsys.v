// $Id: vsys.v,v 1.2 2010/08/11 08:51:35 djg11 Exp $
//# Kiwi Scientific Acceleration: Satnams Simple Factorial Example
//
// (C) 2010-15 DJ Greaves, University of Cambridge Computer Laboratory.
//
// vsys.v - A test wrapper for simulating very simple tests with clock and reset.
//

`timescale 1ns/10ps

module SIMSYS();
   reg clk, reset;
   initial begin reset = 1; clk = 0; # 400 reset = 0; end
   always #50 clk = !clk;

   wire [7:0]  ksubsAbendSyndrome;
   wire        finished = (ksubsAbendSyndrome != 0) && (ksubsAbendSyndrome != 255);


   initial begin # (1000 * 1000) $display("Finish HDL simulation on timeout %t.", $time); $finish(); end

   initial begin $dumpfile("vcd.vcd"); $dumpvars(); end

   wire done;
   wire [31:0] fac;
   wire [31:0] n = 10;

   SimpleFactorial the_dut(.clk(clk),
			   .reset(reset),
			   .hpr_abend_syndrome(ksubsAbendSyndrome),	       
			   .SimpleFactorial_n(n),
			   .result_lo(fac)
			   );
   
   always @(posedge clk) begin
      if (finished) begin
	 $display("Done at time %1t: factorial %1d is %1d", $time, n, fac);
	 #300
	 $finish;
	 end
//	$display("pc, %d thread0 = %d", $time, dut.xpc10nz);
	end

   
endmodule
// eof
