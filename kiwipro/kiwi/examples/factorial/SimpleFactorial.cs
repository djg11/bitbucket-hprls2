﻿// $Id: Factorial.cs 726 2011-07-21 16:23:55Z satnams $
// (C) 2010-15 DJ Greaves, University of Cambridge Computer Laboratory.
//
//

// Correct output is
// Done at time               295000: factorial         10 is    3628800

// See Satnam's blog:
// http://blogs.msdn.com/b/satnam_singh/archive/2010/10/15/compiling-c-programs-into-fpga-circuits-factorial-example.aspx


// Note we should use the Kiwi.Remote mechanism to wrap this up as an IP block ... manual polling of ready and request nets is no longer needed.

using System;
using KiwiSystem;

class SimpleFactorial
{
    [Kiwi.InputWordPort(31, 0)]
    static uint n; // Input to factorial circuit

    [Kiwi.OutputWordPort("result_lo")] static uint fac = 1; // Result of factorial circuit

    static void FactorialCircuit()
    {
        uint i = n; // On reset capture the input n ... TODO demonstrate programmed I/O here...
        bool done = false;
        while (!done)
        {
            if (i > 1)
                fac = fac * i;
            i--;
            if (i == 1)
            {
                Console.WriteLine("Factorial is {0}", fac);
                done = true;
            }
            Kiwi.Pause();
        }
    }

    [Kiwi.HardwareEntryPoint]
    static void FactorialServer()
    {
      FactorialCircuit();
      Kiwi.ReportNormalCompletion();
    }

    public static void Main()
    {
        n=5;
        FactorialCircuit();

    }
}

// eof
