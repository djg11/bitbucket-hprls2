#
# Kiwi Scientific Acceleration - Examples folder.
#
# This is Makefile.examples.inc
# Set up local env to compile and run the examples

# CSharp Compiler
CSC     ?=mcs

# Verilator converts from Verilog to C++ for very fast simulation.
VERILATOR_ROOT ?=/usr/share/verilator

# Please set KDISTRO env var to where your K-distro is installed and user Makefiles should key all libraries off that in the future.

KDISTRO  ?=$(HPRLS)/kiwipro/kiwic/kdistro
KIWIC    ?=$(KDISTRO)/bin/kiwic
KIWIDLL  ?=$(KDISTRO)/support/Kiwi.dll

CV_INT_ARITH_LIB =$(KDISTRO)/lib/hpr_ipblocks/cvip0/cvgates.v
CV_FP_ARITH_LIB  =$(KDISTRO)/lib/hpr_ipblocks/cvip0/cv_fparith.v


# TODO get from distro:
KIWI              =$(HPRLS)/kiwipro/kiwi
KIWIFS            =$(KIWI)/support/filesystem/kiwifs_bev.v
KIWI_LARGERAMS    =$(KIWI)/support/rams/generic_large_kiwi_rams.v



CVLIBS=$(CV_INT_ARITH_LIB) $(CV_FP_ARITH_LIB)

LOGDIR=/tmp/kiwilogs

#------------------------------------
# SystemC compiles
BSYSTEM        ?= $(HPRLS)/bsystem
CXX            ?=g++
CXXINCLUDES    ?= -I$(SYSTEMC)/include -I$(BSYSTEM)  -DHPR_USE_SYSTEMC -std=c++11 -DSC_DEFAULT_WRITER_POLICY=SC_MANY_WRITERS
# CXXINCLUDES  ?= -I$(SYSTEMC)/include -I$(HPRLS)/hpr/hprls_cpp_shims -std=c++03
ARCH           ?=x86_64


#eof
