//
// Kiwi Scientific Acceleration
// FFT
// (C) 2009 DJ Greaves - University of Cambridge, Computer Laboratory
//

using System;
using KiwiSystem;

// using IntFFT256; // as Coefs;


public struct RECX
{
  public int re, im;
}

public class IntFFTKernel256
{
   /*
     struct coefs_s
     { 
     public double re, im;
     }
     
     // Surely we do not need to store this for HLS - KiwiBitSwap for a low-cost built-in implementation.
     
     void recalc_sincos()
      {
	int i;
	static int adone = 0;
	if (adone) return;
	aadone = 1;
	for (i=0;i<128; i++)
	{
	double a = (double)i * 3.14159 / 128.0;
	coefs[i].re =  cos(a);
	coefs[i].im =  sin(a);
	}
      }
  */
  
  void butterfly_radix2(bool fwd, int phase, ref RECX top, ref RECX bot)
    {
      RECX a, b;

      int [] coefs_re = IntFFT256.coefs_lhs;
      int [] coefs_im = IntFFT256.coefs_rhs;
      const int denom = IntFFT256.coef_denom;
      a.re = top.re;
      a.im = top.im;
      if (fwd)
	{
	  /* clockwise -j transform */
	  b.re = (bot.re * coefs_re[phase]/denom + bot.im * coefs_im[phase]/denom);
	  b.im = (bot.im * coefs_re[phase]/denom - bot.re * coefs_im[phase]/denom);
	}
      else
	{
	  /* normal +j anticlockwise */
	  b.re = (bot.re * coefs_re[phase]/denom - bot.im * coefs_im[phase]/denom);
	  b.im = (bot.im * coefs_re[phase]/denom + bot.re * coefs_im[phase]/denom);
	}
      const int ndenom = 1;
      
      //Console.WriteLine(" butterfly res {0} {1}", b.re, b.im);
      bot.re = (a.re - b.re)/ndenom;
      bot.im = (a.im - b.im)/ndenom;

      top.re = (a.re + b.re)/ndenom;
      top.im = (a.im + b.im)/ndenom;
    }


  void permute(RECX [] drdata) // Perform the bit swap - for HLS is it better to inline this on the RAM reads when  pass is 0.
  {
    for (int i=0;i<IntFFT256.n_swaps;i++) 
	{    Kiwi.Pause();
	  int [] lhs = IntFFT256.swappers2_lhs;
	  int [] rhs = IntFFT256.swappers2_rhs;
	  RECX t = drdata[lhs[i]];
	  drdata[lhs[i]] = drdata[rhs[i]];
	  drdata[rhs[i]] = t;
	}
  }

    /*
     * This FFT will have a natural gain of IntFFT256.PTS/2 when coherently
     * detecting a single sine wave.  
     *
     */
  public void executeFFT(bool fwd, RECX [] drdata)
    {
      int w = 1;
      int phase_delta = IntFFT256.PTS/2;

      // int d = 1;
      // recalc_sincos();
      permute(drdata);
      Kiwi.KppMark(20, "DATA SWAPPED");  Console.WriteLine("Data swapped.");
      for (int pass=0; pass<IntFFT256.LPTS; pass++)
	{    Kiwi.Pause();
          Console.WriteLine(" FFT PASS {0} START", pass);
	  int t = 0;
	  while (t<IntFFT256.PTS)
	    { Kiwi.Pause();
	      int s = 0;
	      int phase = 0;
	      while (s < w)
		{    Kiwi.Pause();
		  int a = s + t;
		  int b = a + w;
		  /*  printf("phase = %i  %i %i\n", phase, a, b); */
		  butterfly_radix2(fwd, phase, ref drdata[a], ref drdata[b]);
		  phase += phase_delta;
		  s = s + 1;
		}
	      t = t + w + w;
	    }
	  w = w + w;
	  phase_delta = phase_delta >> 1;

//          if (pass==4) break; // for now

	}
      Kiwi.KppMark(21, "FFT COMPLETE");  Console.WriteLine("FFT Complete.");
    }


  static public uint bitswap(uint r, uint n) // Implement (direct wiring) bitswap function.
  {  

    for (uint m = n>>1; ((r^=m)&m) != 0; m >>= 1 ) continue;
    return r;
  }

  
}



public static class FFTBench
{


  static RECX [] drdata = new RECX [IntFFT256.PTS];  

  static void GenDummyData(int seeder, RECX [] drdata)
  {
    int NPoints = IntFFT256.PTS;  
    Kiwi.Pause();
    for (int i=0; i< NPoints; i++)
      { Kiwi.Pause();
	RECX vx;
	vx.re = (i==11) ? 1000:0;
	vx.im = 0;
	drdata[i] = vx;
      }
    
    Console.WriteLine("FFT Data Generated. NPoints={0} seed={1}", NPoints, seeder);
    Kiwi.KppMark(22, "DATA GENERATED");
  }

  static void PrintData(RECX [] drdata)
  {
    int NPoints = IntFFT256.PTS;  
    Kiwi.Pause();
    for (int i=0; i<NPoints; i++)
      {     Kiwi.Pause();
	RECX vx = drdata[i];
	Console.WriteLine("  data {0}  re={1} im={2}", i, vx.re, vx.im);
      }
    
    Console.WriteLine("FFT Data PRINTED. NPoints={0}", NPoints);
    Kiwi.KppMark(23, "DATA PRINTED");
  }
  

  static void BitSwapTest_fancy()
  {
    uint r = 0;
    for (uint x = 1; x<32-1; x++)
      {
        r = IntFFTKernel256.bitswap(r, 32);
        Console.WriteLine("Swap {0} gave {1}", x, r);
        Kiwi.Pause();
      }
  }

  static uint bitswap (uint p, uint lpts)
  {
    uint r=0;
    for(int x=0;x<lpts;x++)
      {
        r <<= 1;
        r |= (p & 1);
        p >>= 1;
      }
    return r;
  }
  
  static void BitSwapTest()
  {
    for (uint x = 1; x<32-1; x++)
      {
        uint r = bitswap(x, 5);
        Console.WriteLine("Swap {0} gave {1}", x, r);
        Kiwi.Pause();
      }
  }


  static void Debug4(ref RECX datum404)
  {
    RECX p = datum404;
    p.re += 1;
    datum404 = p;
  }

  [Kiwi.HardwareEntryPoint()]
  public static void Main()
  {
    Console.WriteLine("Kiwi FFT on FPGA Demo  --- Start");
    Kiwi.KppMark(1, "START");
    Kiwi.Pause();
    
    
    if (false)
      {
        BitSwapTest();
      }
    else 
      {
        var fft = new IntFFTKernel256();
        //    for (int i =0; i<2; i++)    Debug4(ref drdata[i]);
        GenDummyData(1, drdata);
        Kiwi.KppMark(10, "MAINSTART");
        fft.executeFFT(true, drdata);
        Kiwi.KppMark(11, "FFT FINISH");
        PrintData(drdata);
        Kiwi.KppMark(12, "PRINT FINISH");
      }
     Console.WriteLine("Kiwi FFT Demo Finish");
     Kiwi.Pause();
     Kiwi.ReportNormalCompletion();
     Kiwi.Pause();
  }

}


// eof

