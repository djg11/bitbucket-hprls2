//
// Kiwi Scientific Acceleration  
// University of Cambridge, Computer Laboratory.
//
// vsys.v - A test wrapper for RTLSIM execution environment
//
`timescale 1ns/1ns

module SIMSYS();
   reg clk, reset;
   initial begin reset = 1; clk = 0; #33 reset = 0; end
   always #5 clk = !clk;

   wire [7:0]  ksubsAbendSyndrome;
   wire        finished = (ksubsAbendSyndrome != 0) && (ksubsAbendSyndrome != 255);

  
   initial begin # (100 * 1000 * 1000) $display("Finish HDL simulation on timeout %t.", $time); $finish(); end
   initial begin $dumpfile("vcd.vcd"); $dumpvars(); end


   wire [31:0] codesent;

   initial clkdiv = 15;
   
   reg 	     rdel, adel;
   reg [4:0]	     clkdiv;
   always @(posedge clk)       clkdiv <= clkdiv+1;

   always @(posedge clk) if (reset) begin
      rdel <= 0;
      adel <= 0;

   end

   wire [31:0] mon0, mon32;


//---------------------------------------------------------------------------------------
//   wire [31:0] 	       bevelab10nz_pc_export;
//    
   IntFFTKernel the_fft(.clk(clk), 
			.reset(reset),
			.hpr_abend_syndrome(ksubsAbendSyndrome)

			//.bevelab10nz_pc_export(bevelab10nz_pc_export)
			//	       .mon0(mon0),
			//	       .mon32(mon32)
	       
	       );
   always @(posedge clk) begin
      if (finished) begin
	 $display("Exit on finished syndrome 0x%02h reported after %d clocks.", ksubsAbendSyndrome, $time/10);
	 @(posedge clk)	#5000  $finish;
	 end
   end
      //$display("%1t,  pc=%1d, codesent=%h" , $time, bevelab10nz_pc_export, 0);

   
endmodule
// eof


