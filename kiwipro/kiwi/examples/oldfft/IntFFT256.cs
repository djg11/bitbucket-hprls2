//
// Kiwi Scientific Acceleration
// FFT Coefficients Table
// (C) 2009 DJ Greaves - University of Cambridge, Computer Laboratory
//

using System;
using KiwiSystem;


public class IntFFT256
{
  
  public const int PTS = 256;
  public const int LPTS = 8;

  /* Inplace bit swap - there are 120 swap pairs for a 256 point FFT. */
  public const int n_swaps = 120;

  /*
  static readonly swapper_s [] swappers = new swapper_s [] 
    { { 1, 128},
      { 2, 64},
      { 3, 192},
      { 4, 32},
      { 5, 160},
      { 6, 96},
      { 7, 224},
      { 8, 16},
      { 9, 144},
      { 10, 80},
      { 11, 208},
      { 12, 48},
      { 13, 176},
      { 14, 112},
      { 15, 240},
      { 17, 136},
      { 18, 72},
      { 19, 200},
      { 20, 40},
      { 21, 168},
      { 22, 104},
      { 23, 232},
      { 25, 152},
      { 26, 88},
      { 27, 216},
      { 28, 56},
      { 29, 184},
      { 30, 120},
      { 31, 248},
      { 33, 132},
      { 34, 68},
      { 35, 196},
      { 37, 164},
      { 38, 100},
      { 39, 228},
      { 41, 148},
      { 42, 84},
      { 43, 212},
      { 44, 52},
      { 45, 180},
      { 46, 116},
      { 47, 244},
      { 49, 140},
      { 50, 76},
      { 51, 204},
      { 53, 172},
      { 54, 108},
      { 55, 236},
      { 57, 156},
      { 58, 92},
      { 59, 220},
      { 61, 188},
      { 62, 124},
      { 63, 252},
      { 65, 130},
      { 67, 194},
      { 69, 162},
      { 70, 98},
      { 71, 226},
      { 73, 146},
      { 74, 82},
      { 75, 210},
      { 77, 178},
      { 78, 114},
      { 79, 242},
      { 81, 138},
      { 83, 202},
      { 85, 170},
      { 86, 106},
      { 87, 234},
      { 89, 154},
      { 91, 218},
      { 93, 186},
      { 94, 122},
      { 95, 250},
      { 97, 134},
      { 99, 198},
      { 101, 166},
      { 103, 230},
      { 105, 150},
      { 107, 214},
      { 109, 182},
      { 110, 118},
      { 111, 246},
      { 113, 142},
      { 115, 206},
      { 117, 174},
      { 119, 238},
      { 121, 158},
      { 123, 222},
      { 125, 190},
      { 127, 254},
      { 131, 193},
      { 133, 161},
      { 135, 225},
      { 137, 145},
      { 139, 209},
      { 141, 177},
      { 143, 241},
      { 147, 201},
      { 149, 169},
      { 151, 233},
      { 155, 217},
      { 157, 185},
      { 159, 249},
      { 163, 197},
      { 167, 229},
      { 171, 213},
      { 173, 181},
      { 175, 245},
      { 179, 205},
      { 183, 237},
      { 187, 221},
      { 191, 253},
      { 199, 227},
      { 203, 211},
      { 207, 243},
      { 215, 235},
      { 223, 251},
      { 239, 247}
    };
    



  static readonly coefs_s[] coefs = new coefs [] 
    { 
      { 2048, 0 },
      { 2047, 50 },
      { 2045, 100 },
      { 2042, 150 },
      { 2038, 200 },
      { 2032, 250 },
      { 2025, 300 },
      { 2017, 350 },
      { 2008, 399 },
      { 1998, 448 },
      { 1986, 497 },
      { 1973, 546 },
      { 1959, 594 },
      { 1944, 642 },
      { 1928, 689 },
      { 1910, 737 },
      { 1892, 783 },
      { 1872, 829 },
      { 1851, 875 },
      { 1829, 920 },
      { 1806, 965 },
      { 1781, 1009 },
      { 1756, 1052 },
      { 1730, 1095 },
      { 1702, 1137 },
      { 1674, 1179 },
      { 1644, 1219 },
      { 1614, 1259 },
      { 1583, 1299 },
      { 1550, 1337 },
      { 1517, 1375 },
      { 1483, 1412 },
      { 1448, 1448 },
      { 1412, 1483 },
      { 1375, 1517 },
      { 1337, 1550 },
      { 1299, 1583 },
      { 1259, 1614 },
      { 1219, 1644 },
      { 1179, 1674 },
      { 1137, 1702 },
      { 1095, 1730 },
      { 1052, 1756 },
      { 1009, 1781 },
      { 965, 1806 },
      { 920, 1829 },
      { 875, 1851 },
      { 829, 1872 },
      { 783, 1892 },
      { 737, 1910 },
      { 689, 1928 },
      { 642, 1944 },
      { 594, 1959 },
      { 546, 1973 },
      { 497, 1986 },
      { 448, 1998 },
      { 399, 2008 },
      { 350, 2017 },
      { 300, 2025 },
      { 250, 2032 },
      { 200, 2038 },
      { 150, 2042 },
      { 100, 2045 },
      { 50, 2047 },
      { 0, 2047 },
      { -50, 2047 },
      { -100, 2045 },
      { -150, 2042 },
      { -200, 2038 },
      { -250, 2032 },
      { -300, 2025 },
      { -350, 2017 },
      { -399, 2008 },
      { -448, 1998 },
      { -497, 1986 },
      { -546, 1973 },
      { -594, 1959 },
      { -642, 1944 },
      { -689, 1928 },
      { -737, 1910 },
      { -783, 1892 },
      { -829, 1872 },
      { -875, 1851 },
      { -920, 1829 },
      { -965, 1806 },
      { -1009, 1781 },
      { -1052, 1756 },
      { -1095, 1730 },
      { -1137, 1702 },
      { -1179, 1674 },
      { -1219, 1644 },
      { -1259, 1614 },
      { -1299, 1583 },
      { -1337, 1550 },
      { -1375, 1517 },
      { -1412, 1483 },
      { -1448, 1448 },
      { -1483, 1412 },
      { -1517, 1375 },
      { -1550, 1337 },
      { -1583, 1299 },
      { -1614, 1259 },
      { -1644, 1219 },
      { -1674, 1179 },
      { -1702, 1137 },
      { -1730, 1095 },
      { -1756, 1052 },
      { -1781, 1009 },
      { -1806, 965 },
      { -1829, 920 },
      { -1851, 875 },
      { -1872, 829 },
      { -1892, 783 },
      { -1910, 737 },
      { -1928, 689 },
      { -1944, 642 },
      { -1959, 594 },
      { -1973, 546 },
      { -1986, 497 },
      { -1998, 448 },
      { -2008, 399 },
      { -2017, 350 },
      { -2025, 300 },
      { -2032, 250 },
      { -2038, 200 },
      { -2042, 150 },
      { -2045, 100 },
      { -2047, 50 }
    };

*/

 public static readonly int [] swappers2_lhs = new int [] 
    { (1) ,
      (2) ,
      (3) ,
      (4) ,
      (5) ,
      (6) ,
      (7) ,
      (8) ,
      (9) ,
      (10) ,
      (11) ,
      (12) ,
      (13) ,
      (14) ,
      (15) ,
      (17) ,
      (18) ,
      (19) ,
      (20) ,
      (21) ,
      (22) ,
      (23) ,
      (25) ,
      (26) ,
      (27) ,
      (28) ,
      (29) ,
      (30) ,
      (31) ,
      (33) ,
      (34) ,
      (35) ,
      (37) ,
      (38) ,
      (39) ,
      (41) ,
      (42) ,
      (43) ,
      (44) ,
      (45) ,
      (46) ,
      (47) ,
      (49) ,
      (50) ,
      (51) ,
      (53) ,
      (54) ,
      (55) ,
      (57) ,
      (58) ,
      (59) ,
      (61) ,
      (62) ,
      (63) ,
      (65) ,
      (67) ,
      (69) ,
      (70) ,
      (71) ,
      (73) ,
      (74) ,
      (75) ,
      (77) ,
      (78) ,
      (79) ,
      (81) ,
      (83) ,
      (85) ,
      (86) ,
      (87) ,
      (89) ,
      (91) ,
      (93) ,
      (94) ,
      (95) ,
      (97) ,
      (99) ,
      (101) ,
      (103) ,
      (105) ,
      (107) ,
      (109) ,
      (110) ,
      (111) ,
      (113) ,
      (115) ,
      (117) ,
      (119) ,
      (121) ,
      (123) ,
      (125) ,
      (127) ,
      (131) ,
      (133) ,
      (135) ,
      (137) ,
      (139) ,
      (141) ,
      (143) ,
      (147) ,
      (149) ,
      (151) ,
      (155) ,
      (157) ,
      (159) ,
      (163) ,
      (167) ,
      (171) ,
      (173) ,
      (175) ,
      (179) ,
      (183) ,
      (187) ,
      (191) ,
      (199) ,
      (203) ,
      (207) ,
      (215) ,
      (223) ,
      (239) 
    };
    
  public const int coef_denom = 2048;

  public static readonly int[] coefs_lhs = new int [] 
    { 
      (2048) ,
      (2047) ,
      (2045) ,
      (2042) ,
      (2038) ,
      (2032) ,
      (2025) ,
      (2017) ,
      (2008) ,
      (1998) ,
      (1986) ,
      (1973) ,
      (1959) ,
      (1944) ,
      (1928) ,
      (1910) ,
      (1892) ,
      (1872) ,
      (1851) ,
      (1829) ,
      (1806) ,
      (1781) ,
      (1756) ,
      (1730) ,
      (1702) ,
      (1674) ,
      (1644) ,
      (1614) ,
      (1583) ,
      (1550) ,
      (1517) ,
      (1483) ,
      (1448) ,
      (1412) ,
      (1375) ,
      (1337) ,
      (1299) ,
      (1259) ,
      (1219) ,
      (1179) ,
      (1137) ,
      (1095) ,
      (1052) ,
      (1009) ,
      (965) ,
      (920) ,
      (875) ,
      (829) ,
      (783) ,
      (737) ,
      (689) ,
      (642) ,
      (594) ,
      (546) ,
      (497) ,
      (448) ,
      (399) ,
      (350) ,
      (300) ,
      (250) ,
      (200) ,
      (150) ,
      (100) ,
      (50) ,
      (0) ,
      (-50) ,
      (-100) ,
      (-150) ,
      (-200) ,
      (-250) ,
      (-300) ,
      (-350) ,
      (-399) ,
      (-448) ,
      (-497) ,
      (-546) ,
      (-594) ,
      (-642) ,
      (-689) ,
      (-737) ,
      (-783) ,
      (-829) ,
      (-875) ,
      (-920) ,
      (-965) ,
      (-1009) ,
      (-1052) ,
      (-1095) ,
      (-1137) ,
      (-1179) ,
      (-1219) ,
      (-1259) ,
      (-1299) ,
      (-1337) ,
      (-1375) ,
      (-1412) ,
      (-1448) ,
      (-1483) ,
      (-1517) ,
      (-1550) ,
      (-1583) ,
      (-1614) ,
      (-1644) ,
      (-1674) ,
      (-1702) ,
      (-1730) ,
      (-1756) ,
      (-1781) ,
      (-1806) ,
      (-1829) ,
      (-1851) ,
      (-1872) ,
      (-1892) ,
      (-1910) ,
      (-1928) ,
      (-1944) ,
      (-1959) ,
      (-1973) ,
      (-1986) ,
      (-1998) ,
      (-2008) ,
      (-2017) ,
      (-2025) ,
      (-2032) ,
      (-2038) ,
      (-2042) ,
      (-2045) ,
      (-2047) 
    };

// Second term

  public static readonly int [] swappers2_rhs = new int [] 
    { 128,
      64,
      192,
      32,
      160,
      96,
      224,
      16,
      144,
      80,
      208,
      48,
      176,
      112,
      240,
      136,
      72,
      200,
      40,
      168,
      104,
      232,
      152,
      88,
      216,
      56,
      184,
      120,
      248,
      132,
      68,
      196,
      164,
      100,
      228,
      148,
      84,
      212,
      52,
      180,
      116,
      244,
      140,
      76,
      204,
      172,
      108,
      236,
      156,
      92,
      220,
      188,
      124,
      252,
      130,
      194,
      162,
      98,
      226,
      146,
      82,
      210,
      178,
      114,
      242,
      138,
      202,
      170,
      106,
      234,
      154,
      218,
      186,
      122,
      250,
      134,
      198,
      166,
      230,
      150,
      214,
      182,
      118,
      246,
      142,
      206,
      174,
      238,
      158,
      222,
      190,
      254,
      193,
      161,
      225,
      145,
      209,
      177,
      241,
      201,
      169,
      233,
      217,
      185,
      249,
      197,
      229,
      213,
      181,
      245,
      205,
      237,
      221,
      253,
      227,
      211,
      243,
      235,
      251,
      247
    };
    
  public static readonly int[] coefs_rhs = new int [] 
    { 
      0,
      50,
      100,
      150,
      200,
      250,
      300,
      350,
      399,
      448,
      497,
      546,
      594,
      642,
      689,
      737,
      783,
      829,
      875,
      920,
      965,
      1009,
      1052,
      1095,
      1137,
      1179,
      1219,
      1259,
      1299,
      1337,
      1375,
      1412,
      1448,
      1483,
      1517,
      1550,
      1583,
      1614,
      1644,
      1674,
      1702,
      1730,
      1756,
      1781,
      1806,
      1829,
      1851,
      1872,
      1892,
      1910,
      1928,
      1944,
      1959,
      1973,
      1986,
      1998,
      2008,
      2017,
      2025,
      2032,
      2038,
      2042,
      2045,
      2047,
      2047,
      2047,
      2045,
      2042,
      2038,
      2032,
      2025,
      2017,
      2008,
      1998,
      1986,
      1973,
      1959,
      1944,
      1928,
      1910,
      1892,
      1872,
      1851,
      1829,
      1806,
      1781,
      1756,
      1730,
      1702,
      1674,
      1644,
      1614,
      1583,
      1550,
      1517,
      1483,
      1448,
      1412,
      1375,
      1337,
      1299,
      1259,
      1219,
      1179,
      1137,
      1095,
      1052,
      1009,
      965,
      920,
      875,
      829,
      783,
      737,
      689,
      642,
      594,
      546,
      497,
      448,
      399,
      350,
      300,
      250,
      200,
      150,
      100,
      50
    };
}


/* eof */
