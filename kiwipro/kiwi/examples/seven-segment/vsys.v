//
// Kiwi Scientific Acceleration  
// University of Cambridge, Computer Laboratory
//
// vsys.v - A test wrapper for simulating very simple tests with clock and reset.
// 
//
//
`timescale 1ns/10ps

module SIMSYS();

   reg clk, reset;
   initial begin reset = 1; clk = 1; # 43 reset = 0; end
   always #5 clk = !clk;
   initial begin # (100 * 1000) $display("Finish HDL simulation on timeout %t.", $time); $finish(); end

   wire [7:0]  ksubsAbendSyndrome;
   wire        finished = (ksubsAbendSyndrome != 0) && (ksubsAbendSyndrome != 255);

   initial begin $dumpfile("vcd.vcd"); $dumpvars(); end

   wire clear = 0;   
   wire [7:0] segments;
   wire [3:0] strobes;

   DUT the_dut(.clk(clk), .reset(reset), 
	       .clear(clear), 
	       .strobes(strobes), 
	       .hpr_abend_syndrome(ksubsAbendSyndrome),
	       .segments(segments));

     always @(posedge clk) begin
      if (finished) begin
	 $display("Exit on finished syndrome 0x%02h reported after %d clocks.", ksubsAbendSyndrome, $time/10);

	 #5000  $finish;
	 end
     //$display("%1t,  pc=%1d, codesent=%h" , $time, the_dut.xpc10nz, codesent);
  end


endmodule
// eof
