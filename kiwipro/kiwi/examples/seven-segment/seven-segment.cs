//
// Kiwi Scientific Acceleration - Simple 7-segment display driver - net level output demo.
// (C) 2008 D J Greaves, University of Cambridge Computer Laboratory.

// 
// Scan-multiplexed LED display driver simple demo.
// 
// One display digit is illuminated at a time according to which segement strobe is active.
// The human eye integrates this and does not see any flicker provided the scanning is sufficiently fast (> 100 Hz).



using System;
using System.Text;
using KiwiSystem;


public static class seven_segment
{
  const int n_digits = 4;
  
   
  [Kiwi.InputBitPort("clear")] static bool clear;
  [Kiwi.OutputWordPort("segments")] static uint segments;
  [Kiwi.OutputWordPort("strobes")] static uint strobes;

  // Digit data in an array
  static int [] digit_data = new int [n_digits];

  // Seven segment character set ROM
  // Digits 0-9 and a minus sign. Bit g is lsb, zero for on.
  static int [] cbg_seven_seg = new int [] { 0x01, 0x4f, 0x12, 0x06, 0x4c, 0x24, 0x60, 0x0f, 0x00, 0x0c, 0x7E };


  static int seven_seg_encode_data(int d)
  {
    return cbg_seven_seg[d];
  }


  static int digit_on_display = 0;


  static void scan()
  {
    digit_on_display = (digit_on_display + 1) % n_digits;
    strobes = 1u << digit_on_display;
    segments = (uint)(seven_seg_encode_data(digit_data[digit_on_display]));
    for (int delay=0;delay<4; delay++) Kiwi.Pause();
  }


  static public void reset_data()
  {
    for (int i=0; i < n_digits; i++)
      {
	digit_data[i] = 0;	  
      }
  }

  static void increment_bcd()
  {
    for (int d=0; d<n_digits; d++)
      {
	int v = digit_data[d] + d;
	if (v==10) v = 0;
	digit_data[d] = v;
	if (v!=0) break; 
      }
  }

  
  [Kiwi.HardwareEntryPoint()]
  public static void Main()
  {
    int jj = 0;  
    reset_data();
    while(true)
      {
	if (clear) reset_data();
	jj = jj + 1;
	scan();
	if (jj == 10)
	  {
	    increment_bcd();
	    jj = 0;
	  }
      }
    }
}


// eof



