
// sim_main.cpp

#include "VVERISYS.h"
#include "verilated.h"
#include <stdio.h>

static int tnow;

int main(int argc, char **argv, char **env) 
{
  Verilated::commandArgs(argc, argv);
  VVERISYS* top = new VVERISYS;
  tnow = 0;
  top->reset = 1;
  top->clk = 0;
  top->eval(); tnow += 5;
  top->clk = 1;
  top->eval(); tnow += 5;
  top->reset = 0;


  while (!Verilated::gotFinish()) 
    {
      top->clk = 1;
      top->eval(); tnow += 5;
      top->clk = 0;
      top->eval(); tnow += 5;
    }
  delete top;
  printf("Finished sim_main.cpp\n");
  exit(0);
}

double sc_time_stamp()
{
  return (double)tnow;
}

//  EOF

