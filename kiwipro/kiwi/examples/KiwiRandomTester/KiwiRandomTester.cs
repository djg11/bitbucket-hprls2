// Kiwi Scientfic Acceleration.
// Random Number Generator Statistical Survey Demo.
// (C) 2016, DJ Greaves, University of Cambridge Computer Laboratory.


using KiwiSystem;
using System;


public abstract class RandomGeneratorIF
{
  
  // Generic interface for random number generators of various designs.
  public virtual void Seed(int s)
  {
    
  }

  public virtual int Next()
  {
    return 100;
  }

}



public class ParkMillerGenerator : RandomGeneratorIF
{
  int seed;
  public override void Seed(int s)
  {
    seed = s;
  }
  
  // PRGEN Park and Miller CACM October 1988
  public override int Next()
  {
    const int a = 2147001325;
    const int b = 715136305;
    seed = a * seed + b;
    return seed;
  }

}

public class KiwiPaddedGenerator : RandomGeneratorIF
{
  KiwiSystem.Random dg = new KiwiSystem.Random();
  public override void Seed(int s)
  {
    dg.Reset(s);
  }
  
  public override int Next()
  {
    return dg.Next();
  }

}

public class Randu: RandomGeneratorIF
{
  int seed;
  public override void Seed(int s)
  {
    seed = s;
  }

  /* Randu : v = (65539 * v) mod (2^31)
               != v + (v<<1) + (v<<16) 
     It's good to add another one to avoid degenerate all-zero case.
  */ 
  
  public override int Next()
  {
	seed = seed + (seed << 1) + (seed << 16) + 1;
	return seed;
  }

}

public class KiwiRandomTester
{

 //  RandomGeneratorIF dg;  
  KiwiPaddedGenerator dg;  

  public KiwiRandomTester(int gen_no_) // constructor
  {
    dg = new KiwiPaddedGenerator();  // Instantiate one of the units, depending on gen_no in future.
  }


  public void RunOnce()
  {
    int [] stats = new int[32];
    Kiwi.Pause();
    Console.WriteLine();
    int trials = 10000;
    int midpt = trials/2;
    for (int kk=0; kk<trials; kk++)
      {
	int v = dg.Next();
	for (int q =0; q<32; q++)  // Tally bits that are set.
	  {
	    if (((v >> q) & 1) == 1) stats[q] += 1;	    
	  }

      }

    KiwiStringIO.Write("Test D "); 
    KiwiStringIO.Write(86420012);
    KiwiStringIO.NewLine();

    for (int rr=0; rr<32; rr++)
      {
	KiwiStringIO.Write(" res "); 
	KiwiStringIO.Write(rr);
	KiwiStringIO.Write(" tally="); 
	KiwiStringIO.Write(stats[rr]);
	KiwiStringIO.Write(" dev= "); 
	KiwiStringIO.Write(Math.Abs(stats[rr]-midpt));
	KiwiStringIO.NewLine();
      }
    KiwiStringIO.WriteLine("Kiwi Random Demo Done"); 
  }


}




public class main
{

  // Xilinx GPIO push buttons on ML605 and VC707 cards.
  [Kiwi.InputBitPort("GPIO_SW_N")] static bool GPIO_SW_N;
  [Kiwi.InputBitPort("GPIO_SW_W")] static bool GPIO_SW_W;
  [Kiwi.InputBitPort("GPIO_SW_S")] static bool GPIO_SW_S;
  [Kiwi.InputBitPort("GPIO_SW_E")] static bool GPIO_SW_E;
  [Kiwi.InputBitPort("GPIO_SW_C")] static bool GPIO_SW_C;

  [Kiwi.InputBitPort("FPGA")] static bool FPGA;
  


  static KiwiRandomTester kl;


  static void mainWork()
  {
    // On FPGA can drive via GPIO push switches.
    // disable for diosim    if (GPIO_SW_N)  
      {
	kl.RunOnce();
      }
  }

  static void open()
  {
    kl = new KiwiRandomTester(0);
  }


  // We need to support three execution environments via our top level code: WS, RTLSIM, FPGA
  //
  // [WD  --- Rapid development of applications on the workstation with performance prediction.
  // [RTLSIM] --- Verilog simulation (verilator is fastest) in case of KiwiC bugs and for performance calibration when interacting with RTL models of other system components.
  // [FPGA] --- high-performance execution on the FPGA.

 

  [Kiwi.HardwareEntryPoint()]   // For the RTLSIM and FPGA execution environments.
  public static void HwProcess()
  {
    Console.WriteLine("KiwiRandomTester Demo Start");
    Kiwi.KppMark(0, "START");
    open();
    Kiwi.KppMark(1, "KiwiRandomTester Constructed");
    
      {
	mainWork();
	Kiwi.Pause();
      }
    Console.WriteLine("KiwiRandomTester Demo Finished");
    Kiwi.Pause();
    Kiwi.KppMark(2, "END");
    Kiwi.ReportNormalCompletion();

  }


  // For [WD] execution, Software entry point.
  public static int Main()
  {
    open();
    for (int run=0; run<10; run++)
      {
	GPIO_SW_C = false;
	GPIO_SW_N = true;
	mainWork();
	GPIO_SW_N = false;
	// We simulate some pressing of the buttons here.
	for (int it=0; it<1; it++)
	  {	    
	    Console.WriteLine("RunOnce generation: run={0}, it={1}", run, it);
	    mainWork();
	  }
      }
    return 0;
  }

}


// eof
