//
// Kiwi Scientific Acceleration  
// University of Cambridge, Computer Laboratory
//
// vsys.v - A test wrapper for FPGA simulation using verilator.
//
`timescale 1ns/1ns

module VERISYS(
	      input clk,
	      input reset);


//   initial begin # (1000 * 100 * 1000) $display("Finish HDL simulation on timeout %t.", $time); $finish(); end

//   initial begin $dumpfile("/tmp/vcd.vcd"); $dumpvars(); end

   wire 	    FPGA = 0; // Tie this high in FPGA top-level but low for RTLSIM execution environment.
   wire start = 1;
   wire busy;
   wire [31:0] codesent;
   DUT the_dut(.clk(clk), 
	       .reset(reset),
	       .done(done),
	       .GPIO_SW_N(1'b1),
	       .FPGA(FPGA)
//..	       .xpc10_start(start),
//	       .xpc10_busy(busy),
//	       .codesent(codesent)
	       );

   always @(posedge clk) begin
      if (done) begin
	 $display("Exit on done asserted.");
	 $finish;
	 end
      //$display("%1t,  pc=%1d, codesent=%h" , $time, the_dut.xpc10nz, codesent);
   end
   
endmodule
// eof


