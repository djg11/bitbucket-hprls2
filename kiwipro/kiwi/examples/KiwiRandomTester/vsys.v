//
// Kiwi Scientific Acceleration.
// (C) 2010 DJ Greaves, University of Cambridge, Computer Laboratory.
//
// vsys.v - A test wrapper for FPGA test 
//
`timescale 1ns/1ns

module SIMSYS();

   reg clk, reset;
   initial begin reset = 1; clk = 0; #33 reset = 0; end
   always #5 clk = !clk;

   initial begin # (1000 * 1000) $display("Finish HDL simulation on timeout %t.", $time); $finish(); end

   initial begin $dumpfile("/tmp/vcd.vcd"); $dumpvars(); end
   wire [7:0]  ksubsAbendSyndrome;
   wire        finished = (ksubsAbendSyndrome != 0) && (ksubsAbendSyndrome != 128);


   wire busy;
   wire [31:0] codesent;

   wire 	    FPGA = 0; // Tie this high in FPGA top-level but low for RTLSIM execution environment.
   DUT the_dut(.clk(clk), 
	       .reset(reset),
	       .ksubsAbendSyndrome(ksubsAbendSyndrome),
	       .GPIO_SW_N(1'b1), // Start it with a press to N
	       .FPGA(FPGA)
//..	       .xpc10_start(start),
//	       .xpc10_busy(busy),
//	       .codesent(codesent)
	       );

   always @(posedge clk) begin
      if (finished) begin
	 $display("RTLSIM: Exit on finished asserted after %t cycles.", $time/10 - 3); // Adjust for clock freq and reset duration in the cycles printed.
	 #5 $finish;
	 end
      //$display("%1t,  pc=%1d, codesent=%h" , $time, the_dut.xpc10nz, codesent);
   end
   
endmodule
// eof


