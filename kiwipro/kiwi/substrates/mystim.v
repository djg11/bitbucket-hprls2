`timescale 1ns / 1ps
//
// Kiwi Scientific Acceleration
// (C) 2013-15 DJ Greaves, University of Cambridge, Computer Laboratory.
//
// A test wrapper for designs with one a wide DRAM-style off-chip port but which still uses the HFAST protocol.
// Bytelanes are used and back-to-back cycles are supported.

module SIMSYS();
   reg clk, reset_n;
   initial begin reset_n = 0; clk = 0; # 43 reset_n = 1;  end

   always #5 begin
     if (finished) begin
          $display("Finishing cleanly - finish driven high");
     	  //$finish;
         end
     clk = !clk;
     end

   wire [31:0] count;
   wire [23:0] pcmon;
   wire [23:0] runtime0;
   wire finished;
   ksubstrate_1 Ksubs_i(.clk(clk), 
            .reset_n(reset_n),
         .finished(finished),
	 .runtime0(runtime0),
	 .pcmon(pcmon),
	 .count(count)
   );


   initial begin #(1 * 1000 * 1000 ) $display ("vsys cbg timeout and finish"); $finish(); end

  initial begin $dumpfile("vcd.vcd"); $dumpvars(); end

endmodule
// eof
