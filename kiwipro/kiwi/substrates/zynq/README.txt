Draft notes


There is a git repo elsewhere that now contains Kiwi-ksubs3

--------------------

ZedBoard

UART: under lsubs appears as  04b4:0008 Cypress Semiconductor Corp. 
 ls /dev/ttyACM0
sudo chmod a+rw /dev/ttyACM0 
stty < /dev/ttyACM0 115200
cu -s 115200 -l /dev/ttyACM0

Zedra10 00:0A:35:00:01:22  Zedra10	ZedBoard	D772998

Terminal connection gives direct login
zynq> uname -a
Linux (none) 3.3.0-digilent-12.07-zed-beta #2 SMP PREEMPT Thu Jul 12 21:01:42 PDT 2012 armv7l GNU/Linux
zynq> df
Filesystem           1K-blocks      Used Available Use% Mounted on
none                    249100         0    249100   0% /tmp

mount /dev/mmcblk0p1 /mnt

Two penguins on the HDMI outputq

Diligent module@ Appears as 
 Bus 003 Device 042: ID 0403:6014 Future Technology Devices International, Ltd FT232H Single HS USB-UART/FIFO IC
----------------------------



Zynq AXI slave and tcl script to be put here for Vivado suite.

part is xc7z010clg400-1

Recreate this design with a MAKE

./kiwi-substrate-2m.srcs/sources_1/ipshared/xilinx.com/generic_baseblocks_v2_1/da89d453/hdl/verilog:


You need a bitswapped binary

mknod /dev/xdevcfg c 259 0
cat /sys/devices/amba.0/f8007000.devcfg/prog_done       (if returns 1 then PL is programmed)
cat top.bit.bin > /dev/xdevcfg


fpga_image:
{
   system_stub.bit
} 

and run

$ bootgen -image fpga.bif -split bin -o I fpga.bin
 once used:

promgen -b -w -p bin -data_width 32 -u 0 system_stub.bit -o system_stub.bit.bin

 

Not tested with Linux, but bare-metal with the devcfg driver was successful.

The advantage is, promgen reports the length which can be set for the dma.

OLD: Since the Zynq interface is 32bits, you need the bytes swapped. So the promgen command is:

 promgen -w -b -p bin -o top.promgen.bin -u 0 top.bit -data_width 32



--------------------------------------
