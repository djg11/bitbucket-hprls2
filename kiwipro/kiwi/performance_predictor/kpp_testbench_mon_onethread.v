// Kiwi Scientific Acceleration of C# on FGPA
// ------------------------------------------
// (C) 2015 David J Greaves and University of Cambridge Computer Laboratory.
//                      
// Kiwi: Single-threaded profile collector - temporary code.   
// Generates an xml stats file from an RTL simulation that is read in as a data point (postulate) by the Kiwi Performance Predictor

   parameter max_profile_pc = (1<<15);
   parameter max_no_waypoints = 16;
   integer kpp_pc_profiles [max_no_waypoints*max_profile_pc-1:0];
   integer kpp_hwm, kpp_pcv;
   integer kpp_waypoint;
   integer dram_stats_activates [max_no_waypoints-1:0];
   integer dram_stats_writeops4 [max_no_waypoints-1:0];
   integer dram_stats_writeops8 [max_no_waypoints-1:0];
   integer dram_stats_readops4  [max_no_waypoints-1:0];
   integer dram_stats_readops8  [max_no_waypoints-1:0];
   integer kpp_waypoint_pcs     [max_no_waypoints-1:0];
   reg  [639:0] kpp_waypoint_names [max_no_waypoints-1:0];
   integer kpp_waypoint_entry_pc;
   reg [639:0] kpp_current_KppWaypoint0;
   reg [639:0] kpp_current_KppWaypoint1;   

   initial begin
      kpp_current_KppWaypoint0 = 0;
      kpp_current_KppWaypoint1 = 0;
      kpp_waypoint = 0;
      kpp_hwm = 0;
      kpp_pcv = 0;
      kpp_waypoint_names[0] = "AnonStartup";
      kpp_waypoint_pcs[0] = 0; // Assumed.      
      while (kpp_pcv < max_profile_pc * max_no_waypoints) begin
	 kpp_pc_profiles[kpp_pcv] = 0;
	 kpp_pcv = kpp_pcv + 1;
      end
   end

   integer kpp_fd;
   time    reset_time;
   integer kpp_wp;

   task copy_dram_stats;
      begin
      dram_stats_activates[kpp_waypoint] = controller_sans_cache.stats_activates;
      dram_stats_writeops4[kpp_waypoint] = controller_sans_cache.stats_writeops4;
      dram_stats_writeops8[kpp_waypoint] = controller_sans_cache.stats_writeops8;
      dram_stats_readops4[kpp_waypoint] = controller_sans_cache.stats_readops4;
      dram_stats_readops8[kpp_waypoint] = controller_sans_cache.stats_readops8;
	 end
   endtask // 
   
   always @(posedge clk) begin
      kpp_pcv = dut.xpc10nz; 
      if (kpp_current_KppWaypoint0 != KppWaypoint0) begin
	 kpp_current_KppWaypoint0 = KppWaypoint0;
	 kpp_current_KppWaypoint1 = KppWaypoint1;
	 copy_dram_stats;
	 kpp_waypoint = kpp_waypoint + 1;
	 kpp_waypoint_pcs[kpp_waypoint] = kpp_pcv;
	 kpp_waypoint_names[kpp_waypoint] = KppWaypoint0;
	 $display("%t: pc=%d: new waypoint monitored %s", $time, kpp_pcv, KppWaypoint0);	 
      end
      
      //$display("Profile capture pc=%d", kpp_pcv);
      if (kpp_pcv > kpp_hwm) kpp_hwm = kpp_pcv;
      if (kpp_pcv >= max_profile_pc) begin
	 $display("pc is outside verilog profile array size %d", kpp_pcv);
	 $finish;
      end

      if (reset) reset_time = $time;

      if (finished) begin
	 copy_dram_stats;
	 $display (" Kiwi run finished at %t after %d clocks", $time, ($time-reset_time)/2/`half_period);
	 $display (" PC visit count report kpp_hwm=%d", kpp_hwm);	 
	 kpp_fd = $fopen("primes.profile.xml", "w");
	 $fwrite(kpp_fd, "<?xml version='1.0'?>\n");
	 $fwrite(kpp_fd, "<kiwiProfile> \n");
	 $fwrite(kpp_fd, "  <creator> \"RTL Simulation Shim - Kiwi Profiling Workbench\" </creator>\n");	 
	 $fwrite(kpp_fd, "  <index> %d </index>\n", elimit);	 
	 $fwrite(kpp_fd, "  <design> %d </design>\n", edesign);	 
	 $fwrite(kpp_fd, "  <variant> %d </variant>\n", evariant);	 
	 $fwrite(kpp_fd, "  <title> \"primesya\" </title>\n");
	 
	 $fwrite(kpp_fd, " <clocksUsed> %d </clocksUsed>\n", ($time-reset_time)/2/`half_period);
	 kpp_wp = 0;
	 while (kpp_wp <= kpp_waypoint) begin
	    $fwrite(kpp_fd, "  <waypoint no=%d> \n", kpp_wp);
	    $fwrite(kpp_fd, "  <no> %d </no> \n", kpp_wp);
	    $fwrite(kpp_fd, "  <regionname> %s </regionname> \n", kpp_waypoint_names[kpp_wp]);
  	    $fwrite(kpp_fd, "  <entrypc> %d </entrypc>\n", kpp_waypoint_pcs[kpp_wp]);   
	    $fwrite(kpp_fd, " <dramStats> <name> dram0bank </name> <activates> %d </activates>\n", dram_stats_activates[kpp_wp]);
	    $fwrite(kpp_fd, "    <reads4> %d </reads4> <writes4> %d </writes4> \n", dram_stats_readops4[kpp_wp], dram_stats_writeops4[kpp_wp]);
	    $fwrite(kpp_fd, "    <reads8> %d </reads8> <writes8> %d </writes8> \n", dram_stats_readops8[kpp_wp], dram_stats_writeops8[kpp_wp]);
	    $fwrite(kpp_fd, " </dramStats>\n");
	    $fwrite(kpp_fd, "    <pcvisits> \n");	 
	    kpp_pcv = 0;
	    while (kpp_pcv <= kpp_hwm) begin
		if (kpp_pc_profiles[kpp_pcv+kpp_wp*max_profile_pc]>0) $display (" wp=%d, pc visit counts, %d, %d", kpp_wp, kpp_pcv, kpp_pc_profiles[kpp_pcv+kpp_wp*max_profile_pc]);
		$fwrite(kpp_fd, "      <visit> %d %d </visit>\n", kpp_pcv, kpp_pc_profiles[kpp_pcv+kpp_wp*max_profile_pc]);
		kpp_pcv = kpp_pcv + 1;
	     end
	     $fwrite(kpp_fd, "    </pcvisits>\n");
	    $fwrite(kpp_fd, "  </waypoint>\n");	 
	    kpp_wp = kpp_wp + 1;
	    end
	 $fwrite(kpp_fd, " </kiwiProfile>\n");	 
	 $fclose (kpp_fd);
	 $finish;
      end
      kpp_pc_profiles [kpp_pcv + kpp_waypoint*max_profile_pc] = kpp_pc_profiles[kpp_pcv + kpp_waypoint*max_profile_pc] + 1;
   end // always @ (posedge clk)

   
