// 
// KiwiStreaming - high performance, dataflow-style input and output support.
//
// Kiwi Scientific Acceleration
// 
// (C) 2010-15 - DJ Greaves - University of Cambridge Computer Laboratory
//
using System;
using KiwiSystem;


namespace KiwiStreaming
{

  public class KiwiBinaryStreamSrc
  {

// This is using net-level operations and hard pause mode.
// The new version will use Kiwi.Remote with an HFAST variant and be a NoC ring with multiple-client support.

    [Kiwi.InputBitPort("streamingInPort_rdy_in")]   static bool rdy_in;
    [Kiwi.OutputBitPort("streamingInPort_rdy_out")] static bool rdy_out = false;

    const int staticPortByteWidth = 16;

    [Kiwi.InputWordPort(63,0)][Kiwi.InputName("streamingInPort_data0")] static long din_data0;
    [Kiwi.InputWordPort(63,0)][Kiwi.InputName("streamingInPort_data1")] static long din_data1;

    static long [] streamingInPort_buffer = new long [staticPortByteWidth/8]; 

    static int logged_counter = 0;
    static long byteCount = 0;

    public static void start()
    {
      Console.WriteLine("Kiwi streaming input start\n");

    }

    public KiwiBinaryStreamSrc() // constructor or reset.
    {
      logged_counter = 0;
      rdy_in = false;
    }

    static public void get_word() // will round up to xfer size - some spare input on the end
    {   
      rdy_out = true;
      Kiwi.Pause();
      while (!rdy_in) Kiwi.Pause();
      rdy_out = false;
      streamingInPort_buffer[0] = din_data0;
      streamingInPort_buffer[1] = din_data1;
      byteCount += staticPortByteWidth;      
    }

    static public int get_int32()
    {
      if (logged_counter == 0) get_word();
      logged_counter = (logged_counter + 32/8) % staticPortByteWidth;
      long rv = streamingInPort_buffer[logged_counter / 8]; // Little-endian
      int rv1 = (int)(rv >> ((logged_counter % 8) * 8));
      return rv1;
    }

  }



  public class Generator
  {	
    
    public void soare()	
    {	  

    }
  }


}

// eof
