// Kiwi Scientific Acceleration of C# on FGPA
// ------------------------------------------
// (Kiwi Project:  DJ Greaves and S Singh: Compilation of parallel programs written in C# to H/W)
// HPR L/S library.
// (C) 2015 David J Greaves and University of Cambridge Computer Laboratory.
//
// The models in this file ddr2-models.v may be freely used and copied for any purpose by anybody but no warranty is provided or liablity accepted.
//
// Contents:
//   arbiter2 
//   cache 
//   A high-level model of a DRAM bank.
//
//-------------------------------------------------------------------
//
//  When two or more threads share one DRAM bank, arbiters like this are needed. Or else they are connected to separate AXI
// ports and the arbitration happens below these ports.
// The arbiter is associative, so more than two inputs is achieved by making a tree or list of them.
// There is no support for load-linked here - atomic operations in the shared resource (DRAM).
//
module HFAST_ARBITER2
  (input clk, 
   input 			     reset, 

   // Front-side interface0
   input 			      fs0_rwbar, 
   output noLanes*laneSize-1:0]       fs0_rdata,
   input [noLanes*laneSize-1:0]       fs0_wdata, 
   input [addrSize-1:0] 	      fs0_wordAddr, 
   output  			      fs0_oprdy, 
   input 			      fs0_opreq, 
   output reg 			      fs0_ack,   
   input [noLanes-1:0] 	              fs0_lanes, 

   // Front-side interface1
   input 			      fs1_rwbar, 
   output  [noLanes*laneSize-1:0]     fs1_rdata,
   input [noLanes*laneSize-1:0]       fs1_wdata, 
   input [addrSize-1:0] 	      fs1_wordAddr, 
   output  			      fs1_oprdy, 
   input 			      fs1_opreq, 
   output  			      fs1_ack,   
   input [noLanes-1:0] 	              fs1_lanes, 

   // Back-side interface
   output  			      bs_rwbar, 
   input [noLanes*laneSize-1:0]       bs_rdata,
   output  [noLanes*laneSize-1:0]     bs_wdata, 
   output  [addrSize-1:0] 	      bs_wordAddr, 
   input 			      bs_oprdy, 
   output  			      bs_opreq, 
   input 			      bs_ack,   
   output  [noLanes-1:0] 	      bs_lanes
   );

   reg 				      sel1;
   reg 				      busy0;
   reg 				      busy1;
   reg 				      sel1_last;
   reg 				      busy0_last;
   reg 				      busy1_last;

   // Need a pipelined arbiter - the ack and rdata of the previous operation overlaps with the next request.
   always @(posedge clk) (if reset) begin
      sel1 = 0;
      busy0 = 0;
      busy1 = 0;
      sel1_last = 0;
      busy0_last = 0;
      busy1_last = 0;
      else begin
	 if (!busy0 && !busy1) begin
	    if (fs_opreq0 && fs_oprdy0) begin
	       sel1 = 0;
	       busy0 = 1;
	    end
	    else if (fs_opreq1 && fs_oprdy1) begin
	       sel1 = 1;
	       busy1 = 1;
	    end


	    sel_last <= sel1;
	    busy0_last <= busy0;
	    busy1_last <= busy1;

	    ... unfinished ...
      end

	 assign fs0_oprdy =  ;
	 assign fs1_oprdy =   ;
      assign  bs_rwbar = (sel1) ? fs1_rwbar: fs0:rwbar;
      assign  bs_wdata = (sel1) ? fs1_wdata: fs0:wdata;   
      assign  bs_lanes = (sel1) ? fs1_lanes: fs0:lanes;
      assign  bs_wordAddr = (sel1) ? fs1_wordAddr: fs0:wordAddr;   
      assign  bs_opreq = (sel1) ? fs1_opreq: fs0:opreq;
      
      assign  bs_rwbar = (sel1) ? fs1_rwbar: fs0:rwbar;   

      assign fs0_ack = !sel1_last & bs_ack;
      assign fs1_ack = sel1_last & bs_ack;
      assign fs0_rdata = bs_rdata;
      assign fs1_rdata = bs_rdata;


endmodule

//
//
//
module cache256_hf1
  (input clk, 
   input 			      reset, 

   // Front-side interface
   input 			      fs_rwbar, 
   output reg [noLanes*laneSize-1:0]  fs_rdata,
   input [noLanes*laneSize-1:0]       fs_wdata, 
   input [addrSize-1:0] 	      fs_wordAddr, 
   output  			      fs_oprdy, 
   input 			      fs_opreq, 
   output reg 			      fs_ack,   
   input [noLanes-1:0] 	              fs_lanes, 

   // Back-side interface
   output reg 			      bs_rwbar, 
   input [noLanes*laneSize-1:0]      bs_rdata,
   output reg [noLanes*laneSize-1:0] bs_wdata, 
   output reg [addrSize-1:0] 	      bs_wordAddr, 
   input 			      bs_oprdy, 
   output reg 			      bs_opreq, 
   input 			      bs_ack,   
   output reg [noLanes-1:0] 	      bs_lanes
   );

   parameter dram_dwidth = 256;          // 32 byte DRAM burst size or cache line.
   parameter laneSize = 8;
   parameter noLanes = dram_dwidth / laneSize; // Bytelanes.

   parameter addrSize = 22;
   parameter ways = 4;

   reg [noLanes*laneSize-1:0]    dataplane  [ways-1:0];
   reg [noLanes-1:0] 		  validLanes [ways-1:0];
   reg [noLanes-1:0] 		  dirtyLanes [ways-1:0];
   reg [addrSize-1:0] 		  tags       [ways-1:0];      
   integer 			  lru;
   integer 			  qx, ql, way, ne;
`define ST_idle 0
`define ST_evict 1
`define ST_read_miss 2
`define ST_read_miss2 3

   integer					state;
   integer 					laneMask = (1<<laneSize)-1;
   reg 					nxt_fs_ack, nxt_bs_opreq;   

   integer 				stats_hits, stats_misses;


   
   reg 					pend_rwbar, pendingOp;
   reg [noLanes*laneSize-1:0] 		pend_wdata;
   reg [addrSize-1:0] 			pend_wordAddr;
   reg [noLanes-1:0] 			pend_lanes;
   reg 					flushFinished;
   
   parameter traceLevel = 0;

   assign fs_oprdy = !pendingOp && flushFinished;
   
   always @(posedge clk) begin
      if (reset) begin
	 state = `ST_idle;
	 qx = 0;
	 flushFinished <= 0;
	 pendingOp <= 0;
	 stats_hits = 0;
	 stats_misses = 0;	 
	 while (qx < ways) begin
	    dirtyLanes[qx] = 0;
	    validLanes[qx] = 0;	    
	    tags[qx] = -1; // Set all bits and hope this tag is never used/matches.
	    qx = qx + 1;
	 end
	 fs_ack <= 0;
	 lru = 0;
	 bs_opreq <= 0;
      end
      nxt_fs_ack =  0;
      nxt_bs_opreq =  0;      

      flushFinished <= 1; // flush is instant at the moment.

      if (state == `ST_read_miss) begin
	 state = `ST_read_miss2;
      end
      else if (state == `ST_read_miss2 && bs_ack) begin
	 ql = 0;
	 while (ql < noLanes) begin
	    if (!dirtyLanes[way][ql]) dataplane[way] = (dataplane[way] & ~(laneMask << (ql * laneSize))) | (bs_rdata & (laneMask << (ql * laneSize)));
	    validLanes[way][ql] = 1; // (a word store infact)
	    ql = ql + 1;
	 end
	 tags[way] = (pendingOp) ? pend_wordAddr: fs_wordAddr;
	 if (traceLevel > 1) $display("%t, %m: Line fill on word 0x%h, way %d, bs_rdata=%h", $time, tags[way], way, bs_rdata);
	 state = `ST_idle;
      end

      else if (state == `ST_evict && bs_ack) begin // dont need to wait for bs_ack - just post it.
	 ql = 0;
/*	 while (ql < noLanes) begin
	    validLanes[way][ql] = 0; // (a word store infact)
	    dirtyLanes[way][ql] = 0; // (ditto)
	    ql = ql + 1;
	 end
*/	 tags[way] = (pendingOp) ? pend_wordAddr: fs_wordAddr;	 
	 if (traceLevel > 0) $display("%t, %m: Eviction finished on way %d", $time, way);
	 state = `ST_idle;
      end

      
      else if (state == `ST_idle && ((fs_opreq && fs_oprdy) || pendingOp)) begin
	 if (!pendingOp) begin // chose a way to use
	    qx = 0;
	    way = ways; // invalid setting
	    while (qx < ways) begin // search hit
	       if (tags[qx] == (pendingOp ? pend_wordAddr: fs_wordAddr)) way = qx;
	       qx = qx + 1;
	    end
	    if (way != ways) stats_hits = stats_hits + 1; else stats_misses = stats_misses + 1;

	    if (way == ways) begin // if way not set, chose lru victim.
	       if (lru >= ways) lru = 0;
	       way = lru;
	       if (traceLevel > 1) $display("%t, %m: tag miss - use lru way %d", $time, way);
	       lru = lru + 1;
	    end
	    if (traceLevel > 0) $display("%m, %t, hits=%d misses=%d, add=0x%h, way=%d, rwbar=%d", $time, stats_hits, stats_misses, fs_wordAddr, way, fs_rwbar);
	 end // if (!pendingOp)
	 
	 if (tags[way] != (pendingOp? pend_wordAddr:fs_wordAddr) && (dirtyLanes[way] != 0)) begin // Way is dirty and needs to change tag
	    // Copy back if dirty and we need to evict. -- TODO no on pending!
	    state = `ST_evict;
	    if (!pendingOp) begin pend_wdata <= fs_wdata; pend_lanes <= fs_lanes; pend_rwbar <= fs_rwbar; pend_wordAddr <= fs_wordAddr; pendingOp <= 1; end
	    bs_wdata <= dataplane[way];
	    bs_lanes <= dirtyLanes[way];
	    nxt_bs_opreq = 1;
	    bs_wordAddr <= tags[way];
	    dirtyLanes[way] = 0;
	    validLanes[way] = 0;
	    bs_rwbar <= 0; // Write.
	    if (traceLevel > 0) $display("%t, %m: Initiate copyback to 0x%h on way %d", $time, tags[way], way);
	 end
	 else begin
	    if (pendingOp ? pend_rwbar: fs_rwbar) begin // Read operation - serve now from dataplane - we do not have fs_lanes on read but in future we could check miss under read mask.
	       //$display("%t, %m: now compare tag 0x%h with 0x%h", $time, tags[way], pendingOp ? pend_wordAddr: fs_wordAddr);
	       if (tags[way] == (pendingOp ? pend_wordAddr: fs_wordAddr) && validLanes[way] == ((1<<noLanes)-1)) begin	
		  fs_rdata <= dataplane[way]; 
		  nxt_fs_ack = 1;
		  pendingOp <= 0;
		  if (traceLevel > 0) $display("%t, %m: READ wordAddr=0x%h rdata=0x%h", $time, (pendingOp ? pend_wordAddr: fs_wordAddr), dataplane[way]);
		  if (traceLevel > 1) $display("%t, %m: Read hit on word 0x%h on way %d", $time, tags[way], way);
	       end
	       else begin // Read operation - block waiting for backside service
		  state = `ST_read_miss;
		  nxt_bs_opreq = 1;
		  bs_wordAddr <= pendingOp ? pend_wordAddr: fs_wordAddr;
		  bs_rwbar <= 1; // Read.
		  if (traceLevel > 0) $display("%t, %m: Read miss on word 0x%h on way %d", $time, tags[way], way);
		  if (!pendingOp) begin pend_lanes <= fs_lanes; pend_wdata <= fs_wdata; pend_rwbar <= fs_rwbar; pend_wordAddr <= fs_wordAddr; pendingOp <= 1; end
	       end
	    end
	    else begin // Write operation
	       if (traceLevel > 0) $display("%t, %m: WRITE lanes=%x wordAddr=0x%h wdata=0x%h", $time, pendingOp ? pend_lanes: fs_lanes, pendingOp ? pend_wordAddr: fs_wordAddr, pendingOp ? pend_wdata: fs_wdata);
	       if (traceLevel > 1) $display("%t, %m: write way=%d tag=%x wanted=%x valid=%x dirty=%x", $time, way, tags[way], pendingOp ? pend_wordAddr: fs_wordAddr, validLanes[way], dirtyLanes[way]);
	       if ((tags[way] != (pendingOp ? pend_wordAddr: fs_wordAddr)) && validLanes[way]==0 && dirtyLanes[way]==0) // Direct claim of empty cache line
		 begin
		    tags[way] = pendingOp ? pend_wordAddr: fs_wordAddr; // Direct claim of free cache line if not already done.
		    if (traceLevel > 1) $display("%t, %m: direct write claim empty way %d", $time, way);
		 end
	       
	       if (tags[way] == (pendingOp ? pend_wordAddr: fs_wordAddr)) begin	
		  dirtyLanes[way] = dirtyLanes[way] | (pendingOp ? pend_lanes: fs_lanes);
		  validLanes[way] = validLanes[way] | (pendingOp ? pend_lanes: fs_lanes);
		  ql = 0;
		  while (ql < noLanes) begin
		     if (pendingOp ? pend_lanes[ql]: fs_lanes[ql]) dataplane[way] = (dataplane[way] & (~(laneMask << (ql * laneSize)))) | ((pendingOp ? pend_wdata:fs_wdata) & (laneMask << (ql * laneSize)));
		     ql = ql + 1;
		  end
		  //if (traceLevel > 1) $display("%t, %m: write way=%d tag=%x wanted=%x valid=%x dirty=%x afterwards", $time, way, tags[way], pendingOp ? pend_wordAddr: fs_wordAddr, validLanes[way], dirtyLanes[way]);
		  pendingOp <= 0;
		  nxt_fs_ack = 1;
	       end // if (tags[way] == (pendingOp) ? pend_wordAddr: fs_wordAddr)
	       else begin // Write miss operation - eviction already initiated above.
		  if (traceLevel > 1) $display("%t, %m: Write miss on word 0x%h on way %d", $time, tags[way], way);
	       end
	    end // else: !if(fs_rwbar)
	 end
      end // if (state == `ST_idle && opreq && oprdy)

      fs_ack <= nxt_fs_ack;
      bs_opreq <= nxt_bs_opreq;
   end
      

endmodule


//
// A high-level model of a DRAM bank, currently without full timing detail.
// Use KIWI_DDR2_CONTROLLER for a more-detailed model instead
//
module membank256_hf1
  (input clk, 
   input 			     reset, 
   input 			     ctr_rwbar, 
   output reg [noLanes*laneSize-1:0] ctr_rdata,
   input [noLanes*laneSize-1:0]      ctr_wdata, 
   input [addrSize-1:0] 	     ctr_wordAddr, 
   output reg 			     ctr_oprdy, // ready for new request   - a new cmd is presented for every clock cycle where rdy and req both hold.
   input 			     ctr_opreq, // new op request          -
   output reg 			     ctr_ack, // output from last cycle is available.
   input [noLanes-1:0] 		     ctr_lanes
   );

   parameter noLanes=32;
   parameter laneSize=8;
   parameter memsize = 100 * 1000 * 8; 
   parameter addrSize = 22;
   parameter traceLevel = 0;
   
   reg [laneSize-1:0] 		     data [0:(memsize-1)];
   wire [31:0] 			     byteAddr = wordAddr * noLanes;
   reg [noLanes*laneSize-1:0] rans, wd;
   integer     wvv,   rvv;
  

   wire   start = ctr_opreq & ctr_oprdy;

   //always @(posedge clk) $display(" HFAST1 req=%d ack=%d rwbar=%d  pc=%d" , start, ack, rwbar, dut.xpc10);
   
   always @(posedge clk) begin
      if (start & ctr_rwbar) begin
	 rvv = noLanes - 1;
	 rans = 0;
	 while (rvv >= 0) begin
	    rans = (rans << laneSize) | ((byteAddr+rvv < memsize) ? data[byteAddr+rvv]:32'bx);
	    //$display("%m: lane read req=%d wordAddr=%d addr=%d laneData=%h rwbar=%d rd=$%x", req, wordAddr, byteAddr+rvv, data[byteAddr+rvv], rwbar, rans);
	    rvv = rvv - 1;

	 end
	 if (traceLevel > 0) $display("%t, %m: read-word start=%d w=[$%h] b=[$%h], rwbar=%d rd=$%x", $time, start, ctr_wordAddr, byteAddr, ctr_rwbar, rans);
	 ctr_rdata <= rans;
      end
   end


   always @(posedge clk) begin
     //$display("%m clk : req=%d addr=%d rwbar=%d", start, wordAddr, rwbar);

      ctr_ack <= 1;
      ctr_oprdy <= !reset;
      
     if (ctr_opreq && ctr_oprdy && !ctr_rwbar) begin
	wvv = 0;
	wd = ctr_wdata;
	if (traceLevel > 0) $display("%t, %m write-word lanes=$%x w=[$%h] b=[$%h] := $%h", $time, lanes, ctr_wordAddr, byteAddr + wvv, wd);	
	while (wvv < noLanes) begin
	   if (byteAddr+wvv < memsize && ctr_lanes[wvv]) begin
	      //$display("%m write byte [%d]=%d   q=%h" , byteAddr + wvv, wd & 8'hFF, wd);
	      data[byteAddr+wvv] <= wd & 8'hFF;
	   end
	   wd = wd >> 8;
	   wvv = wvv + 1;
	end

	if (traceLevel > 0) begin
       	   rvv = noLanes - 1;
	   rans = 0;
	   while (rvv >= 0) begin
	      rans = (rans << laneSize) | ((byteAddr+rvv < memsize) ? data[byteAddr+rvv]:32'bx);
	      //$display("%m: lane read wordAddr=%d addr=%d laneData=%h rwbar=%d rd=$%x", wordAddr, byteAddr+rvv, data[byteAddr+rvv], rwbar, rans);
	      rvv = rvv - 1;
	   end
	   $display("%t, %m write-word post lane mask w=[$%h] b=[$%h] := $%h", $time, wordAddr, byteAddr, rans);
	end
     end
     //else if (req && !ack && !rwbar)  $display("%m write out of range %d", addr);

  end


endmodule

// eof
