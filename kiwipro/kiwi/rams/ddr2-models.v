// Kiwi Scientific Acceleration of C# on FGPA
// ------------------------------------------
// (Kiwi Project:  DJ Greaves and S Singh: Compilation of parallel programs written in C# to H/W)
// HPR L/S library.
// (C) 2014 David J Greaves and University of Cambridge Computer Laboratory.
//
// The models in this file ddr2-models.v may be freely used and copied for any purpose by anybody but no warranty is provided or liablity accepted.
//
// Contents:
//   DDR2 controller testbench
//   DDR2 controller
//   DDR2 DRAM model
//
//-------------------------------------------------------------------
//
//  


//
// Behavioural model of a simple DDR controller - could be made synthesisable with a small code tidyup.
//
module KIWI_DDR2_CONTROLLER
  (input clk, 
   input 			     reset, 
   input 			     ctr_rwbar, 
   output reg [noLanes*laneSize-1:0] ctr_rdata,
   input [noLanes*laneSize-1:0]      ctr_wdata, 
   input [addrSize-1:0] 	     ctr_wordAddr, 
   output 			     ctr_oprdy, // ready for new request   - a new cmd is presented for every clock cycle where rdy and req both hold.
   input 			     ctr_opreq, // new op request          -
   output reg 			     ctr_ack, // output from last cycle is available.
   input [noLanes-1:0] 		     ctr_lanes
   );

   parameter addrSize = 22;
   parameter noLanes = 32;
   parameter laneSize = 8;
   parameter log2_internal_banks = 3;
   parameter BURST_SIZE = 8;       // No of dwidth=noLanes*laneSize words accessed in one go.
   parameter LOG2_ROW_SIZE = 15;   // Log_2 number of words per RAS
   parameter LOG2_COL_SIZE = 10;   // Log_2 number of words per CAS   
   parameter PRECHARGE_LATENCY = 11;
   parameter ACTIVATE_LATENCY = 11;
   parameter CAS_LATENCY = 11;
   parameter ddr_awidth = LOG2_ROW_SIZE;         // Address bus to memory chips, width in bits - word addressed.  
   parameter ddr_dwidth = 32;                    // Data bus width to memory chips.
   
   reg [1:0] traceLevel; // Set me to zero for normal (quiet) use.
   initial traceLevel = 0;
   // DRAM burst size - can be dynamically encoded in high-order CAS address. Currently fixed at 32 bytes.  With a 32 bit data bus (64 after doubling for DDR) this requires 4 clocks to transfer the burst of 8.
   parameter burstSize = 4;

   reg 			     ddr_ras;
   reg 			     ddr_cas;
   reg 			     ddr_rwbar;
   reg [log2_internal_banks-1:0] ddr_ibank;
   reg [ddr_awidth-1:0] 	  ddr_addr;
   reg [2*ddr_dwidth-1:0] 	  ddr_wdata; // The wdata and rdata busses are here twice their width in reality corresponding to data moving on both clock edges.
   wire [2*ddr_dwidth-1:0] 	  ddr_rdata;
   reg [2*ddr_dwidth/8-1:0] 	  ddr_dm;

   // DRAM controller.  External view is 32 bytes wide and designed to run at 1/6th clock rate of the DDR and its controller.
   //
   // A DRAM row is 2^15 words of 32 bits (ie 2^17 bytes).
   //

   reg 					pend_rwbar, pending_op;  // This is a one-place buffer for pending word - so we can only serve one request at a time.
   reg [noLanes*laneSize-1:0] 		pend_wdata;              // Better would be multiple buffers and a clever prioritiser that maximises throughput on the ddr_wdata/rdata lines.
   reg [noLanes-1:0] 			pend_lanes;
   reg [addrSize-1:0] 			pend_wordAddr;
   reg 					pendingOp;
   reg                                  active_rwbar; 					


   reg [noLanes*laneSize-1:0] 		active_wdata;
   reg [noLanes-1:0] 			active_lanes;

`define CST_idle 0
`define CST_activating 1
`define CST_active 2
`define CST_precharge 3

   reg [LOG2_ROW_SIZE-1:0] 	  bankRasAddress [(1<<log2_internal_banks)-1:0]; // 
   integer 			  bankCST [(1<<log2_internal_banks)-1:0];          // status enumeration
   integer 			  bankTimer [(1<<log2_internal_banks)-1:0];        // Counts down during delays and upwards during commands.

   integer bx;
   integer row;
   integer col;
   integer addr;
   reg    logged;
   integer idx;
   reg [noLanes*laneSize-1:0] rdata_buffer;


   integer 		      stats_writeops4;   // A burst read or write.
   integer 		      stats_readops4; 
   integer 		      stats_writeops8;
   integer 		      stats_readops8; 
   integer 		      stats_activates;  // A row change is an activate


   reg [3:0] 		      warmup;
   assign ctr_oprdy = !pendingOp && warmup == 15;
   
   always @(posedge clk) if (reset) begin
      ddr_ras <= 1;
      ddr_cas <= 1;
      warmup <= 0;
      stats_writeops4 = 0;
      stats_readops4 = 0;
      stats_writeops8 = 0;
      stats_readops8 = 0;
      stats_activates = 0;
      pendingOp <= 0;
      bx = 0;
      ctr_ack <= 0;
      while (bx<(1<<log2_internal_banks)) begin
	 bankCST[bx] = `CST_idle;
	 bx = bx + 1;
	 end
   end
   else begin

      if (warmup < 15) warmup <= warmup + 1;
      bx = 0;

      while (bx<(1<<log2_internal_banks)) begin
	 if (bankCST[bx] == `CST_precharge) begin
	    if (bankTimer[bx] == 0) begin
	       bankCST[bx] = `CST_idle;
	    end
	    else bankTimer[bx] = bankTimer[bx] -1;
	 end
	 else if (bankCST[bx] == `CST_activating) begin
	    if (bankTimer[bx] == 0) begin
	       bankCST[bx] = `CST_active;
	       bankTimer[bx] = 0;
	    end
	    else bankTimer[bx] = bankTimer[bx] -1;
	 end
	 bx = bx + 1;
      end

      ctr_ack <= 0;
      ddr_cas <= 1;
      ddr_ras <= 1;
      logged = pendingOp || (ctr_opreq && ctr_oprdy);
      if (logged) begin
	 addr = pendingOp ? pend_wordAddr: ctr_wordAddr; // Split out address bits - this determines bank interleave strategy.
	 col = (addr>>0) & ((1<<LOG2_COL_SIZE)-1);
	 bx =  (addr>>(LOG2_COL_SIZE)) & ((1<<log2_internal_banks)-1);
	 row = (addr>>(LOG2_COL_SIZE+log2_internal_banks)) & ((1<<LOG2_ROW_SIZE)-1);
	 
	 if (bankCST[bx] == `CST_active && bankRasAddress[bx] != row) begin // RAS change:  Need to deactivate the current row in this bank.
	    bankCST[bx] = `CST_precharge;
	    $display("%m, Change row");
	    bankTimer[bx] = PRECHARGE_LATENCY;
	    ddr_ras <= 0;
	    ddr_rwbar <= 1; // RAS with WR high to deactivate.
	    ddr_ibank <= bx;
	    if (!pendingOp) begin
	       pend_wdata <= ctr_wdata; pend_lanes <= ctr_lanes; pend_rwbar <= ctr_rwbar; pend_wordAddr <= ctr_wordAddr; pendingOp <= 1; 
	       if (!ctr_rwbar) begin
		  if (traceLevel > 0) $display("%t,%m : write ack site 0", $time);
		  ctr_ack <= 1; // Acknoweldge a write here - earliest possible site.
	       end
	    end
	    if (traceLevel > 0) $display("%t, %m: row swap from $%h to $%h: Initiate precharge/deactivate on bank %d", $time, bankRasAddress[bx], row, bx);
	 end
	 else
	   if (bankCST[bx] == `CST_idle) begin // Activate
	      bankCST[bx] = `CST_activating;
	      bankTimer[bx] = ACTIVATE_LATENCY;
	      ddr_ibank <= bx;
	      ddr_ras <= 0;
	      bankRasAddress[bx] = row;
	      ddr_rwbar <= 0; // RAS with WR low to activate.
	      ddr_addr <= row;
	      stats_activates = stats_activates + 1;
	      if (!pendingOp) begin 
		 pend_wdata <= ctr_wdata; pend_lanes <= ctr_lanes; pend_rwbar <= ctr_rwbar; pend_wordAddr <= ctr_wordAddr; pendingOp <= 1; 
		 if (!ctr_rwbar) begin
		    if (traceLevel > 0) $display("%t,%m : write ack site 1", $time);
		    ctr_ack <= 1; // Acknoweldge write as early as possible.
		 end
	      end

	      if (traceLevel > 0) $display("%t, %m: row activate  $%h on bank %d", $time, row, bx);
	   end
	 else
	   if (bankCST[bx] == `CST_active) begin // Execute cmd
	      ctr_rdata <= 0; // Clear this for tidier display;
	      //$display(" bank %d timer %d", bx, bankTimer[bx]);
	      if (bankTimer[bx] == 0) begin

		 bankTimer[bx] = 1; // Start the sequencer for this cmd's data xfer.
		 ddr_cas <= 0;
		 ddr_ibank <= bx;
		 active_rwbar = pendingOp ? pend_rwbar: ctr_rwbar;
		 active_lanes = pendingOp ? pend_lanes: ctr_lanes;
		 active_wdata = pendingOp ? pend_wdata: ctr_wdata;

		 if (burstSize == 4 && !active_rwbar)      stats_writeops4 = stats_writeops4 + 1;
		 else if (burstSize == 4 && active_rwbar)  stats_readops4  = stats_readops4 + 1;		 
		 else if (burstSize == 8 && !active_rwbar) stats_writeops8 = stats_writeops8 + 1;
		 else if (burstSize == 8 && active_rwbar)  stats_readops8  = stats_readops8 + 1;		 
		 ddr_rwbar <= active_rwbar;
		 ddr_addr <= col;
		 ctr_rdata <= 0;
		 rdata_buffer <= 0;
		 if (!pendingOp) begin
		    pend_wdata <= ctr_wdata; pend_lanes <= ctr_lanes; pend_rwbar <= ctr_rwbar; pend_wordAddr <= ctr_wordAddr; pendingOp <= 1;
		    if (!ctr_rwbar) begin
		       if (traceLevel > 0) $display("%t,%m : write ack site 2", $time);
		       ctr_ack <= 1; // Acknoweldge a write here if not already.
		    end
		 end
		 if (traceLevel > 0 && active_rwbar) $display("%t, %m: col READ cmd  raddr=$%h on bank %d", $time, col, bx);
		 if (traceLevel > 0 && !active_rwbar) $display("%t, %m: col WRITE cmd lanes=$%h waddr=$%h on bank %d wdata=%h", $time, active_lanes,  addr, bx, pendingOp ? pend_wdata: ctr_wdata);
		 if (active_lanes==0 && !active_rwbar) $display("%t, %m: *** WRITE cmd NO LANES SET waddr=$%h on bank %d wdata=%h", $time, addr, bx, pendingOp ? pend_wdata: ctr_wdata);
	      end
	      else if (bankTimer[bx] == CAS_LATENCY+burstSize+1 && active_rwbar) begin
		 ctr_rdata <= rdata_buffer;
		 if (traceLevel > 0) $display("%t, %m: col READ complete on bank %d, addr=%h rdata=%h", $time,  bx, col, rdata_buffer);		    
		 ctr_ack <= 1;
		 pendingOp <= 0;
		 bankTimer[bx] = 0; // Read command finished - set timer back to zero bank remains active (not autoclose).
		 end
	      else if (bankTimer[bx] == CAS_LATENCY+burstSize-1 && !active_rwbar) begin
		 pendingOp <= 0;
		 bankTimer[bx] = 0; // Write command finished - set timer back to zero bank remains active (not autoclose).
	      end
	      else begin
		 if (bankTimer[bx] > 0) bankTimer[bx] = bankTimer[bx] + 1;
	      end
	   end // if (bankCST[bx] == `CST_active)
      end

      //$display(" active_rwbar=%d bank=%d ctr=%d ST=%d", active_rwbar, bx, bankTimer[bx], bankCST[bx]);
      // Dataplane serdes.
      if (!active_rwbar) begin // Write command
	 if (bankCST[bx] == `CST_active && bankTimer[bx] >= CAS_LATENCY && bankTimer[bx] < CAS_LATENCY + burstSize) begin
	    idx = bankTimer[bx] - CAS_LATENCY;
	    ddr_wdata <= active_wdata >> (idx * 2 * ddr_dwidth);
	    //$display("xtmas set active_wdata: raw=%h, shift=%d, ans=%h", active_wdata, (idx * 2 * ddr_dwidth), active_wdata >> (idx * 2 * ddr_dwidth));
	    //$display("%t,%m: set pend_lanes: raw=%h, shift=%d, ans=%h", $time, active_lanes, (idx * 2 * ddr_dwidth / 8), pend_lanes >> (idx * 2 * ddr_dwidth/ 8));	    
	    ddr_dm <= active_lanes >> (idx * 2 * ddr_dwidth / 8);
	 end
      end
      else begin // Read command
	 if (bankCST[bx] == `CST_active && bankTimer[bx] >= CAS_LATENCY+2 && bankTimer[bx] < CAS_LATENCY+2 + burstSize) begin
	    idx = bankTimer[bx] - CAS_LATENCY - 2;
	    rdata_buffer <= rdata_buffer | (ddr_rdata << (idx * 2 * ddr_dwidth));
	 end
      end // else: !if(bankCST[bx] == `CST_active && bankTimer[bx] >= CAS_LATENCY+1 && bankTimer[bx] < CAS_LATENCY+1 + burstSize)
   end
   
   // The SIMM or DIMM (all the chips of the bank) is modelled with one RTL module.
   DDR_DRAM_BANK ddr(
		     .clk(clk), // DDR Clock - 800 MHz typically the net on the PCB has half this frequency)
		     .reset(reset),
		     .ddr_ras(ddr_ras),
		     .ddr_cas(ddr_cas),
		     .ddr_ibank(ddr_ibank),
		     .ddr_rwbar(ddr_rwbar),
		     .ddr_wdata(ddr_wdata), // The wdata and rdata busses are here twice their width in reality owing to DDR.
		     .ddr_rdata(ddr_rdata),
		     .ddr_dm(ddr_dm),
		     .ddr_mux_addr(ddr_addr)		     
		     );

endmodule // DDR DRAM_CONTROLLER

//===========================================================================================================================================
// (C) 2010-14 DJ Greaves. 
// Verilog RTL DDR2 behavioural model - fairly high level.
// The SIMM or DIMM (all the chips of the bank) is modelled with one RTL module.
module DDR_DRAM_BANK(
		 input 				 clk,    // DDR Clock - 800 MHz typically. We use one edge only and double the datapath width.
		 input 				 reset,  // Active high synchronous reset
		 input 				 ddr_ras, // Active low row address strobe
		 input 				 ddr_cas, // Active low col address strobe
		 input [log2_internal_banks-1:0] ddr_ibank,// Internal bank select
		 input 				 ddr_rwbar,// On CAS: 1=read, 0=write. On RAS 1=precharge, 0=activate.
		 input [2*dwidth-1:0] 		 ddr_wdata, // The wdata and rdata busses are here twice their width in reality owing to DDR.
		 input [awidth-1:0] 		 ddr_mux_addr, // Multiplexed address bus
		 input [2*dwidth/8-1:0] 	 ddr_dm,   // Lanes: Separate nets here for +ve and -ve edges instead of combined.
		 output reg [2*dwidth-1:0] 	 ddr_rdata); // Read data bus.

   // Note: DDR command encoding - RAS low with rwbar high is activate. RAS low with rwbar low is precharge (deactivate).
   // 
   parameter log2_dwidth = 5;
   parameter dwidth = (1<<log2_dwidth);         // Word width in bits - we actually have twice this to achieve/simulate double data rate.
   
   parameter DRAM_STYLE = 0;
   parameter READ_LATENCY_ = 1;

   // FOR DRAM style
   // E.g.   MT41K256M32-125 DDR3 @ 800 MHz/1.25ns RCD-RP-CL=11-11-11 Arch=32M x 32 bits x 8 banks = 8Gb = 1GB.  Row bits=15, Col=10, Bank=3.
   // DRAM burst size - can be dynamically encoded in high-order CAS address. Currently fixed at 32 bytes.  With a 32 bit data bus (64 after doubling for DDR) this requires 4 clocks to transfer the burst.
   parameter burstSize = 4;
   parameter LOG2_ROW_SIZE = 15;   // Log_2 number of words per RAS
   parameter LOG2_COL_SIZE = 10;   // Log_2 number of words per CAS   
   parameter PRECHARGE_LATENCY = 11;
   parameter ACTIVATE_LATENCY = 11;
   parameter CAS_LATENCY = 11;
   parameter log2_internal_banks = 3;
   parameter awidth = LOG2_ROW_SIZE;         // Address width in bits - word addressed.  
   parameter log2_blockSize = 16;    // Internal sparse representation of 4GB (only 1GB addressable with above pinout). 
   parameter maxblocks = 256;        // Internal sparse representation parameter: 64k virtual blocks of 64kB with only this number maximally populated.

   reg traceLevel = 0; // Set me to 0 for normal use
   initial traceLevel = 0;
   integer 				blockmapping [(1<<log2_blockSize)-1:0];
   integer 				next_free_block = 0;
   reg [7:0] 				memory_blocks [maxblocks*(1<<log2_blockSize)-1:0]; 

   reg [LOG2_ROW_SIZE-1:0] 		bankRasAddresses [(1<<log2_internal_banks)-1:0]; // RAS lines
   reg 					row_active [(1<<log2_internal_banks)-1:0]; // active flags.

   wire [LOG2_COL_SIZE-1:0] 		col_addr = ddr_mux_addr & ((1<<LOG2_COL_SIZE)-1);
   wire [LOG2_ROW_SIZE+LOG2_COL_SIZE+log2_internal_banks-1:0] dram_word_addr = { bankRasAddresses[ddr_ibank], ddr_ibank, col_addr }; // Repack for internal sparse store. Interleave does not really matter here - but ideally we want the most spatial locality for the sparse covers rather than the most bank diversity which is the packing/interleave aim within the controller design.
   parameter scaling = (1 << (1 + log2_dwidth - 3));
   integer 						      dram_byte_addr;
   reg [15:0] 						      block, offset;
   
   integer 						      zaddr;
   integer 						      clr;
   initial begin
      clr = 0;
      while (clr < maxblocks) begin
	 blockmapping[clr] = -1; // Sparse actual data store.
	 clr = clr + 1;
      end
   end

   integer burst_t_offset;
   integer burst_offset, burst_block;   
   reg 	   burst_rwbar;
   integer dmi, idx;
   reg [2*dwidth-1:0] rdata;
   parameter noPhysLanes = 2*dwidth/8;
   always @(posedge clk) if (reset) begin
      /* */
   end
   else begin
      if (!ddr_ras) begin
	 if (!ddr_rwbar) bankRasAddresses[ddr_ibank] <= ddr_mux_addr;
	 row_active[ddr_ibank] <= !ddr_rwbar;
	 if (!ddr_rwbar) $display("%t,%m: activate bank %d on row 0x%h", $time, ddr_ibank, ddr_mux_addr);
      end
      
      if (!ddr_cas) begin
	 if (!row_active[ddr_ibank]) $display("%t, %m: DDR PROTOCOL ERROR - run DDR read or write on inactive BANK %d", $time, ddr_ibank);
	 if (burst_t_offset > 0) $display("%t, %m: DDR PROTOCOL ERROR - issue DDR read or write cmd while previous cmd is mid burst", $time);	 
	 // TODO check for precharge time violation as well
	 // if ( ) $display("%t, %m: DDR PRECHARGE NOT COMPLETE - Activate command issued too early on bank %d", $time, ddr_ibank);	 
	 dram_byte_addr = dram_word_addr * scaling;
	 block = dram_byte_addr[31:log2_blockSize];
	 offset = dram_byte_addr[log2_blockSize-1:0];   
	 burst_t_offset <= 1;
	 burst_block <= block;
	 burst_offset <= offset;	 	 
	 burst_rwbar <= ddr_rwbar;
	 if (blockmapping[block] < 0 || blockmapping[block] === 'bx) begin
	    blockmapping[block] = next_free_block;
	    if (burst_rwbar) $display("%m: +++ Warning - read of uninitialised block at address %h", dram_byte_addr);
	    $display("%m: Allocated DRAM %m spare block %d to cover address %h (block=%h)", next_free_block, dram_byte_addr, block);
	    next_free_block = next_free_block + 1;
	    if (next_free_block == maxblocks) begin
	       $display("%m: Too much memory in use for RTL simulation: non sparse %h/%h", dram_byte_addr, maxblocks);
	       $finish;
	    end
	 end
      end

      if (burst_t_offset > 0) begin // Data burst running
	 ddr_rdata <= 0; // For tidy waveforms, clear this.
	 zaddr = blockmapping[burst_block] * (1<<log2_blockSize) + burst_offset * noPhysLanes;
	 burst_t_offset <= burst_t_offset + 1;	    
	 if (burst_t_offset >= CAS_LATENCY-1) begin
	    idx = burst_t_offset - CAS_LATENCY+1;	 	 
	    if (!burst_rwbar) begin // Write data xfer.
	       if (traceLevel>1) $display("%t, %m: offset=%d  XFER-WRITE lanes=%h wd=$%h  zaddr=$%h", $time, idx, ddr_dm, ddr_wdata, zaddr);
	       dmi = 0;
	       while (dmi < noPhysLanes) begin
		  if (ddr_dm[dmi]) memory_blocks[zaddr+idx*noPhysLanes+dmi] = ddr_wdata >> (8 * dmi);
		  if (traceLevel>2) $display("%t, %m: %d  XFER-WRITE wd=%h dm=%d zaddr=%h  dmi=%h", $time, idx, ddr_wdata >> (8 * dmi), ddr_dm[dmi], zaddr, dmi);
		  dmi = dmi + 1;
		  end
	    end
	    else begin // READ data xfer.
	       dmi = 0;
	       rdata = 0;
	       while (dmi < noPhysLanes) begin
		  rdata = rdata | (memory_blocks[zaddr+idx*noPhysLanes+dmi] << (8 * dmi));
		  dmi = dmi + 1;
		  if (traceLevel>2) $display("%t,%m: offset=%d  XFER-READ ASSEMBLE zaddr=$%h dmi=%d d=%h ", $time, idx, zaddr, dmi, rdata);
		  end
	       if (traceLevel>1) $display("%t,%m:   XFER-READ dram_word_addr=$%h, dram_byte_addr=$%h, zaddr=$%h", $time, dram_word_addr, dram_byte_addr, zaddr);
	       if (traceLevel>1) $display("%t,%m:   XFER-READ offset=%d d=%h  addr=%h  ", $time, idx, rdata, dram_byte_addr);
	       ddr_rdata <= rdata;
	    end

	    if (idx == burstSize-1) begin
	       burst_t_offset <= 0; // End of burst
	       if (!burst_rwbar && traceLevel > 1) begin // Hex dump of what was written
		  dmi = 0;
		  while (dmi < 64) begin
		     if ((dmi % 32) == 0) $write("%h:%h:  ", burst_block, burst_offset*noPhysLanes + dmi);		     
		     $write(" %h", memory_blocks[zaddr+dmi]);
		     if ((dmi % 32) == 31) $display();
		     dmi = dmi + 1;
		  end
	       end
	    end
	 end
	 
      end
   end

endmodule

// eof
